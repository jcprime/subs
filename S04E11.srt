﻿1
00:00:01,992 --> 00:00:03,223
Oh, goody, you're all here.

2
00:00:03,343 --> 00:00:06,079
Now, I know I'm not supposed to talk
about my campaign in the office,

3
00:00:06,199 --> 00:00:09,230
but last night I've made a decision
about who should be my campaign manager.

4
00:00:09,350 --> 00:00:11,167
This is a huge job.

5
00:00:11,185 --> 00:00:13,403
This is going to require
a lot of late night,

6
00:00:13,438 --> 00:00:15,355
one-on-one jam sessions
with me,

7
00:00:15,390 --> 00:00:16,873
and we're going to bounce ideas
off of each other,

8
00:00:16,908 --> 00:00:18,024
you're going to have to take
calls for me,

9
00:00:18,059 --> 00:00:19,810
any time, day or night.

10
00:00:19,844 --> 00:00:24,815
The person I have chosen is,
drum roll please,

11
00:00:24,849 --> 00:00:28,869
[imitates drum roll]

12
00:00:28,920 --> 00:00:30,287
Ann Perkins! Yay!
Give it up for Ann!

13
00:00:30,321 --> 00:00:33,323
[Applause] Good choice.

14
00:00:33,341 --> 00:00:35,325
Leslie, I don't know the first
thing about running

15
00:00:35,359 --> 00:00:37,360
a political campaign.

16
00:00:37,395 --> 00:00:39,329
Ann, you beautiful tropical fish.

17
00:00:39,347 --> 00:00:42,549
You're smart as a whip
and you're cool under pressure.

18
00:00:42,600 --> 00:00:44,768
You've resuscitated a human
heart in your bare hands.

19
00:00:44,802 --> 00:00:46,203
No, I haven't.

20
00:00:46,237 --> 00:00:47,771
- You haven't?
- No!

21
00:00:47,805 --> 00:00:49,806
You will.
You're that good of a nurse.

22
00:00:49,841 --> 00:00:51,057
What about Ben?

23
00:00:51,109 --> 00:00:53,026
I mean, he ran for mayor
at 18 and won.

24
00:00:53,060 --> 00:00:55,345
Ben is poison in my campaign.

25
00:00:55,363 --> 00:00:58,014
Our relationship is the reason
why my advisors pulled out.

26
00:00:58,049 --> 00:00:59,399
- I--
- Ann,

27
00:00:59,450 --> 00:01:01,351
don't listen to your head
or your heart.

28
00:01:01,385 --> 00:01:03,186
Just look at my eyes
and say "yes."

29
00:01:03,204 --> 00:01:04,854
- Okay, yes.
- Yes!

30
00:01:04,872 --> 00:01:06,689
- I believe in you, Ann.
- Thank you.

31
00:01:06,707 --> 00:01:08,375
And your first job
as my campaign manager

32
00:01:08,409 --> 00:01:10,160
is to start dressing like one.

33
00:01:10,194 --> 00:01:11,912
I don't want to have this
conversation again.

34
00:01:11,963 --> 00:01:14,697
Again? You just hired me eight
seconds ago.

35
00:01:14,715 --> 00:01:16,333
Wow. You're doing
a really bad job.

36
00:01:25,941 --> 00:01:36,467
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

37
00:01:36,587 --> 00:01:40,073
- William, Elizabeth.
- Leslie, hi.

38
00:01:40,107 --> 00:01:42,759
Hi. What are you--
Are you coming to see me?

39
00:01:42,793 --> 00:01:44,628
Did you hear that I'm
relaunching my campaign?

40
00:01:44,662 --> 00:01:46,029
Actually, no, sorry.

41
00:01:46,063 --> 00:01:48,298
We weren't here to see you.
We've been meeting with

44
00:01:53,871 --> 00:01:55,605
you find someone else and run theirs?

45
00:01:55,640 --> 00:01:57,541
Yes, that's our job.

46
00:01:57,575 --> 00:02:00,277
I know.
Good luck, but, uh,

47
00:02:00,311 --> 00:02:02,612
I just had a big meeting
with my new advisory board,

48
00:02:02,647 --> 00:02:05,448
and they're brilliant and amazing.

49
00:02:05,483 --> 00:02:07,250
They're real killers.

50
00:02:07,285 --> 00:02:09,803
Leslie, I tried to make
ramen in the coffee pot

51
00:02:09,854 --> 00:02:11,471
and I broke everything.

52
00:02:11,522 --> 00:02:13,523
Thank you, Andy.
I'll be right in.

53
00:02:13,558 --> 00:02:15,976
Well, uh, good luck,
Leslie. Honestly.

54
00:02:16,027 --> 00:02:19,262
Well, we don't need luck.
We are a rocket ship.

55
00:02:19,297 --> 00:02:21,631
We are relaunching,
and we're going to blast past

56
00:02:21,666 --> 00:02:23,867
your cann--
Huh, they're gone.

57
00:02:23,901 --> 00:02:26,870
It's true--I no longer have
highly-trained,

58
00:02:26,904 --> 00:02:29,873
professional campaign managers.
So what?

59
00:02:29,907 --> 00:02:32,409
Are most murders committed by
highly-trained,

60
00:02:32,443 --> 00:02:34,461
professional assassins?
No.

61
00:02:34,495 --> 00:02:38,481
They're committed by friends
and coworkers.

62
00:02:38,516 --> 00:02:40,517
That analogy was way better
in my head.

63
00:02:46,007 --> 00:02:47,924
Hi.

64
00:02:47,959 --> 00:02:52,846
Champion? Oh my gosh,
there you are, puppy.

65
00:02:52,897 --> 00:02:55,865
- That's a good boy.
- That is a three-legged dog.

66
00:02:55,900 --> 00:02:59,653
His name is Champion, because
he's the dog world champion.

67
00:02:59,687 --> 00:03:01,304
Okay, I have to ask this, I'm sorry,

68
00:03:01,339 --> 00:03:03,806
but how many legs did that dog
have when you found him?

69
00:03:03,824 --> 00:03:05,475
Three! That's what
makes him the best.

70
00:03:05,509 --> 00:03:08,361
He can do more with three legs
than most dogs can do with four.

71
00:03:08,412 --> 00:03:10,530
Except for digging.
He's really bad at digging.

72
00:03:10,581 --> 00:03:11,698
And you remember what you said

73
00:03:11,749 --> 00:03:13,033
about making decisions in the house?

74
00:03:13,084 --> 00:03:16,086
You want to be involved.
We get that, so...

75
00:03:16,120 --> 00:03:17,487
You just say the word,

76
00:03:17,505 --> 00:03:19,289
and Champion goes back to the pound,

77
00:03:19,323 --> 00:03:21,458
where he can be put down
and killed forever.

78
00:03:21,492 --> 00:03:23,843
I'm not gonna send
a three-legged dog to his death.

79
00:03:23,878 --> 00:03:25,195
Yes!

80
00:03:25,229 --> 00:03:27,130
But I'm not gonna
take care of him for you.

81
00:03:27,164 --> 00:03:29,683
Well, it would be nice
if you helped a little.

82
00:03:29,717 --> 00:03:32,168
Because, unlike you,
Andy and I have jobs.

83
00:03:32,186 --> 00:03:35,038
Cruel, but fair.

84
00:03:35,072 --> 00:03:38,441
So Leslie and I have
come up with the theme

85
00:03:38,476 --> 00:03:40,644
for the campaign relaunch rally.

86
00:03:40,678 --> 00:03:42,445
"The Comeback Kid."

87
00:03:42,480 --> 00:03:44,898
Everyone loves
a good comeback story. <i>Rocky.</i>

88
00:03:44,949 --> 00:03:46,199
Robert Downey, Jr.

89
00:03:46,233 --> 00:03:49,202
Terminator said,
"I'll be back," and he was.

90
00:03:49,236 --> 00:03:52,455
Also making a comeback--
the casual Hawaiian shirt.

91
00:03:52,490 --> 00:03:54,741
Well, well, well,
look, who's ahead of the curve.

92
00:03:54,792 --> 00:03:56,042
I was joking.

93
00:03:56,077 --> 00:03:58,545
You should soak that
in bleach and burn it.

94
00:03:58,579 --> 00:04:00,380
Okay, so, the rally's
going to be held tomorrow

95
00:04:00,414 --> 00:04:01,965
at the Pawnee Sports Building.

96
00:04:01,999 --> 00:04:04,084
April, you finalized a rental, right?

97
00:04:04,135 --> 00:04:05,502
[Dog voice] Shut up, Ann.

98
00:04:05,536 --> 00:04:07,270
I told you never to talk to me.

99
00:04:07,305 --> 00:04:08,922
[Gasps] That was Champion.

100
00:04:08,973 --> 00:04:10,640
Oh, my God, I'm sorry.
Bad dog!

101
00:04:10,675 --> 00:04:12,309
Yes, everything's fine
with the rental.

102
00:04:12,343 --> 00:04:13,943
Ron, you're constructing the stage?

103
00:04:13,978 --> 00:04:15,428
Yes, ma'am.

104
00:04:15,479 --> 00:04:19,816
Leslie wanted to hire
a contractor to build a stage.

105
00:04:19,850 --> 00:04:21,551
I don't want to paint
with a broad brush here,

106
00:04:21,585 --> 00:04:24,554
but every single contractor
in the world

107
00:04:24,588 --> 00:04:27,590
is a miserable, incompetent thief.

108
00:04:27,625 --> 00:04:33,530
Seal the edges by crimping
the fork--aw, crap.

109
00:04:33,564 --> 00:04:34,964
[Knocking] Ed!

110
00:04:34,999 --> 00:04:36,366
What's up, Chris?
Come on in, man.

111
00:04:36,400 --> 00:04:37,901
I already did.

112
00:04:37,935 --> 00:04:39,536
So, uh, how you been?
How are you doing?

113
00:04:39,570 --> 00:04:41,338
- How are you?
- Great, actually.

114
00:04:41,372 --> 00:04:45,642
I'm just learning how to
make a "cali-zoin-za."

115
00:04:45,676 --> 00:04:48,812
Or, as you Americans
like to say, calzones.

116
00:04:48,846 --> 00:04:52,716
- Do you want one?
- No, I find <i>calzone</i> fatty,

117
00:04:52,750 --> 00:04:54,084
and unnecessary.

118
00:04:54,118 --> 00:04:56,019
So, you've hit a bit of a rough patch,

119
00:04:56,053 --> 00:04:57,754
and I care about you,

120
00:04:57,788 --> 00:05:00,290
so I just want to make sure
you're doing okay.

121
00:05:00,324 --> 00:05:03,259
Chris, honestly, I'm great.

122
00:05:03,294 --> 00:05:07,063
I'm just exploring whatever fun
activity pops into my brain.

123
00:05:07,098 --> 00:05:08,398
Like, check this out.

124
00:05:08,432 --> 00:05:13,186
I'm teaching myself how to do
Claymation videos.

125
00:05:13,237 --> 00:05:15,405
Isn't this just so cool?

126
00:05:15,439 --> 00:05:16,706
It is so cool!

127
00:05:16,741 --> 00:05:20,377
Ben is massively depressed,
and he needs my help.

128
00:05:20,411 --> 00:05:22,145
You look like a real campaign manager.

129
00:05:22,163 --> 00:05:25,314
Oh, thanks, that's because
I googled "campaign manager"

130
00:05:25,332 --> 00:05:27,917
and noticed that they wear
a lot of dark colors.

131
00:05:27,952 --> 00:05:29,352
See, there's more things
to look at on the Internet

132
00:05:29,387 --> 00:05:32,589
- besides naked guys, Ann.
- What?

133
00:05:32,623 --> 00:05:34,891
I need a team update,
campaign manager.

134
00:05:34,925 --> 00:05:38,661
Okay. Pistol Pete Disellio
will be here in five minutes.

135
00:05:38,696 --> 00:05:40,196
I still can't believe
that you got him.

136
00:05:40,231 --> 00:05:42,665
Ann got local hero Pistol
Pete Disellio

137
00:05:42,700 --> 00:05:44,367
to endorse me at the rally.

138
00:05:44,402 --> 00:05:46,970
In 1992, Pistol Pete's
last-second dunk

139
00:05:47,004 --> 00:05:50,056
against Eagleton High clinched
a comeback victory

140
00:05:50,107 --> 00:05:52,575
for the Pawnee Central
drunken savages.

141
00:05:52,610 --> 00:05:55,078
Team mascot later changed.

142
00:05:55,112 --> 00:05:58,347
Akash, buddy, of course
I came to you first.

143
00:05:58,365 --> 00:06:00,150
You're the best carpet man in Pawnee.

144
00:06:00,184 --> 00:06:01,618
But here's my question--

145
00:06:01,652 --> 00:06:03,653
Do your carpets match your drapes?

146
00:06:03,687 --> 00:06:06,406
[Laughing]

147
00:06:06,457 --> 00:06:07,874
How do you make any event classy
on a budget?

148
00:06:07,908 --> 00:06:09,559
Red carpet.

149
00:06:09,593 --> 00:06:11,327
My entire apartment is red carpet.

150
00:06:11,362 --> 00:06:13,997
On top of that,
leading into my bedroom,

151
00:06:14,031 --> 00:06:15,298
a second red carpet.

152
00:06:15,332 --> 00:06:18,635
Oh, what's this in my shoe?

153
00:06:18,669 --> 00:06:20,220
Red carpet insole.

154
00:06:20,254 --> 00:06:23,940
Everywhere I go,
I'm walking on red carpet.

155
00:06:25,709 --> 00:06:27,577
Is there even enough room
for everyone?

156
00:06:27,611 --> 00:06:29,262
- Here, sit on my lap.
- No, that's humiliating.

157
00:06:29,313 --> 00:06:30,713
Can't I at least sit on Andy's lap?

158
00:06:30,731 --> 00:06:32,432
No, that's Champion's spot.
He called it.

159
00:06:32,483 --> 00:06:34,317
Tom, we're already late.

160
00:06:34,351 --> 00:06:36,486
Now, be a man, and sit
on that girl's lap.

161
00:06:36,520 --> 00:06:38,688
Yes, sir.

162
00:06:41,692 --> 00:06:42,926
Hi, Pete Disellio.

163
00:06:42,960 --> 00:06:44,110
We can call you Pistol, right?

164
00:06:44,161 --> 00:06:45,445
I prefer Pete.

165
00:06:45,496 --> 00:06:47,730
Oh. Okay, got it.
I'm Leslie Knope.

166
00:06:47,748 --> 00:06:49,632
We overlapped a year
at Pawnee Central.

167
00:06:49,667 --> 00:06:50,834
You probably remember my voice

168
00:06:50,868 --> 00:06:52,035
from the morning announcements.

169
00:06:52,069 --> 00:06:53,870
This is my campaign manager,
Ann Perkins.

170
00:06:53,904 --> 00:06:55,755
Hi, nice to meet you.
Come on in.

171
00:06:55,790 --> 00:06:57,640
Oh, thanks.

172
00:06:57,675 --> 00:07:00,543
- I'm excited to be here.
- Ah! We're excited to have you.

173
00:07:00,578 --> 00:07:04,097
Ann is killing it.
My campaign team is unstoppable.

174
00:07:04,131 --> 00:07:06,466
This rally is going to be awesome.

175
00:07:06,517 --> 00:07:08,268
Oh, my God, I'm feeling it!

176
00:07:08,302 --> 00:07:12,972
I'm gonna break dance!

177
00:07:13,023 --> 00:07:14,090
Leslie?

178
00:07:14,108 --> 00:07:16,109
Coming! Thank God.

179
00:07:22,004 --> 00:07:25,423
Oh, Champion is a way better
kisser than you are, babe.

180
00:07:25,458 --> 00:07:26,758
- Nuh-uh!
- Yes, he is.

181
00:07:26,792 --> 00:07:29,549
He's even a better kisser
than me. Here, try some.

182
00:07:29,669 --> 00:07:31,880
Okay, stop it.
He's drooling on me!

183
00:07:31,931 --> 00:07:33,348
Oh!
[Truck horn honks]

184
00:07:33,382 --> 00:07:35,517
- This is unsafe.
- Ha ha!

185
00:07:35,551 --> 00:07:36,685
Oh, we should do it for the kids.

186
00:07:36,719 --> 00:07:38,436
Kids love it, Ron.
Just for the kids.

187
00:07:38,471 --> 00:07:42,224
[Horn honking]
Hi, kids! Hey!

188
00:07:42,275 --> 00:07:43,608
[Police siren] Oh, look.

189
00:07:43,643 --> 00:07:45,644
The police even love it.
They're sirening back to us.

190
00:07:45,678 --> 00:07:47,479
[Gasps] That's awesome, I was
going to tell them that we--

191
00:07:47,513 --> 00:07:51,116
[Truck horn]
Heard ya, bud!

192
00:07:51,150 --> 00:07:54,236
And then, you come out,
and you dunk the ball,

193
00:07:54,287 --> 00:07:57,622
and you say, "Voting for Leslie
Knope is a slam dunk!"

194
00:07:57,657 --> 00:07:58,757
You can still dunk, right?

195
00:07:58,791 --> 00:08:02,627
- Oh, I can, but I won't.
- Sorry?

196
00:08:02,662 --> 00:08:03,828
Look, everywhere I go,

197
00:08:03,863 --> 00:08:05,363
everybody wants me to
talk about that dunk,

198
00:08:05,381 --> 00:08:06,531
and that game.

199
00:08:06,566 --> 00:08:07,799
It feels like I'm living in the past.

200
00:08:07,833 --> 00:08:11,720
But the past is great.
The jitterbug, stagecoaches,

201
00:08:11,754 --> 00:08:12,904
Herman Munster.

202
00:08:12,939 --> 00:08:14,472
Look, Leslie, I read up on you.

203
00:08:14,507 --> 00:08:16,074
You've done great stuff
for our parks system

204
00:08:16,108 --> 00:08:18,276
and I will happily endorse you...

205
00:08:18,311 --> 00:08:19,911
As Peter Disellio,

206
00:08:19,946 --> 00:08:21,646
regional distributor
for Derwin ham loafs,

207
00:08:21,681 --> 00:08:25,150
but if this is about who I was,
or what I did when I was 17,

208
00:08:25,184 --> 00:08:26,952
I'm out of here.

209
00:08:26,986 --> 00:08:28,737
Let's not talk about dunking anymore.

210
00:08:28,771 --> 00:08:29,887
Let's talk about what you want to do.

211
00:08:29,906 --> 00:08:31,656
- Okay.
- I think you want to dunk.

212
00:08:31,691 --> 00:08:33,124
I'm not going to dunk the ball.

213
00:08:33,159 --> 00:08:35,327
What about a layup?

214
00:08:35,361 --> 00:08:37,746
Officer, I've been operating
heavy machinery

215
00:08:37,780 --> 00:08:40,165
since I was eight years old.

216
00:08:40,199 --> 00:08:43,118
Now, I respect you
and your service to this town

217
00:08:43,169 --> 00:08:44,302
and your country,

218
00:08:44,337 --> 00:08:48,006
but what laws are we breaking exactly?

219
00:08:48,040 --> 00:08:49,941
Well, you got four people
in the front seat,

220
00:08:49,976 --> 00:08:51,776
nobody's wearing a seat belt,

221
00:08:51,811 --> 00:08:53,295
you were speeding
and blasting your horn

222
00:08:53,346 --> 00:08:54,779
through the hospital zone,

223
00:08:54,814 --> 00:08:57,966
the rear of the vehicle's open,
debris has been falling out,

224
00:08:58,017 --> 00:09:00,151
and you don't have a commercial
license to drive a truck.

225
00:09:00,186 --> 00:09:03,722
Okay. Well, we have
a philosophical difference

226
00:09:03,756 --> 00:09:05,974
on what constitutes a law.

227
00:09:06,025 --> 00:09:07,258
I need to see your hands

228
00:09:07,276 --> 00:09:09,427
and could you step out of
the vehicle, please?

229
00:09:09,445 --> 00:09:12,948
Hi. I just wanted you to know
we're about to be arrested.

230
00:09:12,982 --> 00:09:14,499
Oh my God, April, that's horrible.

231
00:09:14,533 --> 00:09:16,151
Where are you?

232
00:09:16,202 --> 00:09:17,953
"My mother's butt," really?
That's really helpful.

233
00:09:17,987 --> 00:09:19,371
Let me talk to her.

234
00:09:19,405 --> 00:09:22,207
April, this is city council
candidate Leslie Knope.

235
00:09:22,241 --> 00:09:23,608
Do not make any trouble.

236
00:09:23,626 --> 00:09:26,795
Sit tight.
I'm on my way.

237
00:09:26,829 --> 00:09:28,630
- Whoa. You just hung up on her?
- She'd already hung up on me.

238
00:09:28,664 --> 00:09:30,482
Okay, I'm going to go down
there and get them out,

239
00:09:30,516 --> 00:09:32,017
because men in uniform love me.

240
00:09:32,051 --> 00:09:34,653
You have to get Pistol Pete
to make that shot.

241
00:09:34,687 --> 00:09:36,421
Okay, but if I don't,
it's no big deal, right?

242
00:09:36,455 --> 00:09:38,757
- You'll just make your speech?
- He's our surprise headliner.

243
00:09:38,791 --> 00:09:40,792
The whole town loves him.
I'm polling at 1%.

244
00:09:40,826 --> 00:09:42,644
He must dunk, Ann.

245
00:09:42,678 --> 00:09:44,346
Do whatever it takes.

246
00:09:44,397 --> 00:09:45,963
You know, anything short
of sexual favors.

247
00:09:45,982 --> 00:09:47,432
- What?
- I do not--

248
00:09:47,466 --> 00:09:49,067
I repeat, I do not

249
00:09:49,101 --> 00:09:51,036
want you to tempt him
with sexual favors.

250
00:09:51,070 --> 00:09:52,537
- I wasn't going to.
- Good, I wouldn't either.

251
00:09:52,571 --> 00:09:53,905
That's where I draw the line.

252
00:09:53,939 --> 00:09:55,140
Although, I am a little offended

253
00:09:55,174 --> 00:09:56,157
that you wouldn't do that for me.

254
00:09:56,192 --> 00:09:57,692
- Go.
- Right.

255
00:09:57,743 --> 00:10:01,946
- So, Ben...Why <i>Calzone?</i>
<i>- Glad you asked, Chris.</i>

256
00:10:01,980 --> 00:10:04,315
You know, there's fast food
hamburgers.

257
00:10:04,333 --> 00:10:05,483
There's fast food Mexican.

258
00:10:05,501 --> 00:10:06,984
There's fast food Chinese.
Blah blah blah.

259
00:10:07,003 --> 00:10:08,420
Have you ever wondered

260
00:10:08,454 --> 00:10:10,755
why there isn't a fast food
option for Italian food?

261
00:10:10,790 --> 00:10:12,123
What about pizza?

262
00:10:12,157 --> 00:10:14,659
Pizza?
Never heard of it.

263
00:10:14,677 --> 00:10:16,428
That's what people will be
saying in 20 years,

264
00:10:16,462 --> 00:10:18,830
because pizza is old news, Chris.

265
00:10:18,848 --> 00:10:21,716
Pizza is your grandfather's calzone.

266
00:10:21,767 --> 00:10:23,218
Never thought of it that way.

267
00:10:23,269 --> 00:10:25,020
What I'm talking about is
a portable, delicious meal,

268
00:10:25,054 --> 00:10:26,438
that is it's own container.

269
00:10:26,472 --> 00:10:29,574
It's a whole new spin
on Italian fast casual dining.

270
00:10:29,608 --> 00:10:31,443
Amazing.

271
00:10:31,477 --> 00:10:33,812
And you of all people will like this.

272
00:10:33,846 --> 00:10:34,813
I'm gonna use
low-fat ingredients.

273
00:10:34,847 --> 00:10:36,114
Game-changer.

274
00:10:36,148 --> 00:10:39,150
And I will call my new
Italian fast casual eatery

275
00:10:39,184 --> 00:10:43,738
"The Low-cal Calzone Zone."

276
00:10:43,789 --> 00:10:46,825
That idea is literally
the greatest idea

277
00:10:46,859 --> 00:10:47,892
I've ever heard in my life.

278
00:10:47,927 --> 00:10:50,061
That idea is terrible.

279
00:10:50,096 --> 00:10:51,863
Glen, you're killing me.

280
00:10:51,881 --> 00:10:53,998
They broke about 50 laws, Knope.

281
00:10:54,032 --> 00:10:56,668
And that girl, she tried to get
that gimp dog to bite me.

282
00:10:56,702 --> 00:10:57,919
[Champion barks]

283
00:10:57,970 --> 00:11:00,037
Look, I could sit here
and fill out all the paperwork,

284
00:11:00,056 --> 00:11:02,540
but you and I both know that
you'd rather go home to Debra,

285
00:11:02,575 --> 00:11:05,209
have a nice home-cooked meal,
and do what comes naturally.

286
00:11:05,227 --> 00:11:06,511
That's not appropriate.

287
00:11:06,545 --> 00:11:08,680
You know that I'm gonna
pay all the fines.

288
00:11:08,714 --> 00:11:12,934
I'm in the middle of a campaign
rally, and this is my team.

289
00:11:12,985 --> 00:11:14,936
- I need them, please.
- Fine.

290
00:11:14,987 --> 00:11:16,788
But unless one of you
has a commercial license,

291
00:11:16,822 --> 00:11:19,724
you can't take that truck.

292
00:11:19,759 --> 00:11:22,610
It would just mean so much to us.

293
00:11:22,661 --> 00:11:24,696
I mean, you're a living legend.

294
00:11:24,730 --> 00:11:27,799
They still air that game every
Friday night on cable access.

295
00:11:27,833 --> 00:11:29,134
Yeah, well, being a living legend is

296
00:11:29,168 --> 00:11:30,585
sort of a double-edged sword.

297
00:11:30,619 --> 00:11:32,470
Everybody in this town
still calls me "Pistol."

298
00:11:32,505 --> 00:11:35,457
Tell me more about that, Peter.

299
00:11:35,508 --> 00:11:38,793
I mean, yes, that dunk,
it made me famous,

300
00:11:38,844 --> 00:11:42,213
but sometimes...

301
00:11:42,247 --> 00:11:46,184
Sometimes life dunks you.

302
00:11:46,218 --> 00:11:48,252
Hey, man, you want to go for a jog?

303
00:11:48,270 --> 00:11:50,054
Just, sort of, kick out the cobwebs,

304
00:11:50,088 --> 00:11:51,940
get some endorphins going?

305
00:11:51,974 --> 00:11:53,608
Oh, no, thanks, Chris.

306
00:11:53,642 --> 00:11:55,660
Kind of tearing this Claymation video

307
00:11:55,694 --> 00:11:57,428
a new one right now.

308
00:11:57,463 --> 00:11:59,063
You know, Ben, I really think

309
00:11:59,097 --> 00:12:00,482
you need to take a step back here.

310
00:12:00,533 --> 00:12:02,433
I think getting some
perspective would be good.

311
00:12:02,451 --> 00:12:03,968
What are you talking about?

312
00:12:04,003 --> 00:12:05,370
I've known you a long time.

313
00:12:05,404 --> 00:12:09,774
And, right now, you need help.

314
00:12:09,809 --> 00:12:12,610
- With my Claymaish?
- With your life.

315
00:12:12,628 --> 00:12:16,881
You are wildly, insanely depressed.

316
00:12:16,916 --> 00:12:18,316
Depressed?

317
00:12:18,350 --> 00:12:20,051
I'm the furthest thing from depressed.

318
00:12:20,085 --> 00:12:21,953
I mean, look at what
I've accomplished.

319
00:12:21,971 --> 00:12:23,838
Do you see him?

320
00:12:23,889 --> 00:12:25,840
Do you think a depressed
person could make this?

321
00:12:25,891 --> 00:12:29,344
No.

322
00:12:29,395 --> 00:12:31,629
- That's all we can fit in here.
- No problem.

323
00:12:31,647 --> 00:12:33,565
This is going to be fine.
We're all going to be fine,

324
00:12:33,599 --> 00:12:34,966
because the team is still together,

325
00:12:34,984 --> 00:12:36,734
and there's nothing the team can't do.

326
00:12:36,769 --> 00:12:40,071
Wait, I think Champion
has to go to the bathroom.

327
00:12:40,105 --> 00:12:43,675
- Oh! He shall do it in the car!
- Answer your phone, Ann.

328
00:12:43,709 --> 00:12:45,176
That's what life is like

329
00:12:45,211 --> 00:12:47,345
in a strict Roman Catholic household.

330
00:12:47,379 --> 00:12:48,947
[Phone vibrates] Of course, my father,

331
00:12:48,981 --> 00:12:50,031
he was more stick than carrot

332
00:12:50,082 --> 00:12:51,749
when it came to matters of discipline.

333
00:12:51,784 --> 00:12:54,953
I'm sorry, but the Ben Wyatt
that I know,

334
00:12:54,987 --> 00:12:57,555
I just don't think he'd be
happy sitting here

335
00:12:57,590 --> 00:12:59,174
faffing around.

336
00:12:59,208 --> 00:13:00,508
I'm not faffing around.

337
00:13:00,543 --> 00:13:02,343
I've sunk myself into my hobbies.

338
00:13:02,378 --> 00:13:05,930
Here, I'll show you my
Claymation project, okay?

339
00:13:05,965 --> 00:13:08,066
Now, I've been working
pretty hard on this,

340
00:13:08,100 --> 00:13:10,768
and...I think
it's really good.

341
00:13:10,803 --> 00:13:13,104
So just hang on to your hat, okay?

342
00:13:13,138 --> 00:13:15,023
Here it goes.

343
00:13:15,057 --> 00:13:17,008
[Intro to <i>Stand</i> playing]

344
00:13:17,026 --> 00:13:18,676
Oh, how great.

345
00:13:23,883 --> 00:13:26,217
♪ Stand in the place
where you-- ♪

346
00:13:29,889 --> 00:13:34,125
- Did you pause it?
- No, I--hang on.

347
00:13:36,161 --> 00:13:38,496
♪ Stand in the place
where you-- ♪

348
00:13:43,936 --> 00:13:47,805
Oh, my God.
That's the whole thing.

349
00:13:47,840 --> 00:13:50,275
- That's three weeks of work.
- You're gonna be okay.

350
00:13:50,309 --> 00:13:53,845
No, no, no, no, I'm not.
You see, in my head,

351
00:13:53,879 --> 00:13:56,481
I thought that was really,
really cool.

352
00:13:56,515 --> 00:13:59,083
I emailed Leslie two days ago

353
00:13:59,118 --> 00:14:01,286
and I compared it to <i>Avatar,</i>

354
00:14:01,320 --> 00:14:03,688
Chris, and how can it not be longer?

355
00:14:03,722 --> 00:14:06,491
Okay, look, what you're
feeling, right now

356
00:14:06,525 --> 00:14:09,160
is regret and shame.

357
00:14:11,330 --> 00:14:13,364
Okay, no word from Ann.

358
00:14:13,399 --> 00:14:14,916
April, I need a status report.

359
00:14:14,950 --> 00:14:17,835
- How's it looking out there?
- Perfect, but just one thing.

360
00:14:17,870 --> 00:14:19,437
It's not a basketball court any more.

361
00:14:19,471 --> 00:14:20,788
It's an ice skating rink.

362
00:14:20,839 --> 00:14:22,340
Wait, what? Why?
What happened?

363
00:14:22,374 --> 00:14:24,509
The stupid guy I called to
book this place didn't tell me

364
00:14:24,543 --> 00:14:26,544
they took off the basketball
floor for a hockey game.

365
00:14:26,578 --> 00:14:28,880
Or maybe he did tell me,
but he was so stupid and boring

366
00:14:28,914 --> 00:14:30,098
that I wasn't listening,

367
00:14:30,132 --> 00:14:31,249
and either way, it was his fault,

368
00:14:31,283 --> 00:14:33,051
because he was stupid and I hated him.

369
00:14:33,085 --> 00:14:34,485
Ron, how's the stage coming?

370
00:14:34,520 --> 00:14:38,222
Well, since we had to
jettison the bulk of the wood,

371
00:14:38,256 --> 00:14:39,524
this is the biggest I could make it.

372
00:14:39,558 --> 00:14:41,092
Oh, my God.

373
00:14:41,126 --> 00:14:44,228
Good lord, what happened
to the rest of my face?

374
00:14:44,262 --> 00:14:45,930
We had to "Jetsons"
most of the poster too,

375
00:14:45,948 --> 00:14:47,148
but I kind of like it,

376
00:14:47,199 --> 00:14:50,702
'cause windows
are the eyes to the house.

377
00:14:50,736 --> 00:14:52,370
Wow.

378
00:14:52,404 --> 00:14:55,239
- Okay, I got Pistol Pete.
- Good, where is he?

379
00:14:55,273 --> 00:14:57,241
Right now, he's curled up
in the back seat of my car,

380
00:14:57,275 --> 00:15:00,178
talking about his father,
who is a piece of work.

381
00:15:00,212 --> 00:15:02,213
I actually think he did the best
he could for a single father,

382
00:15:02,247 --> 00:15:03,915
but I may be too close
to the situation.

383
00:15:03,949 --> 00:15:05,667
Anyway, the important news
is that he's here.

384
00:15:05,718 --> 00:15:07,218
Good. Is he going to
dunk for me?

385
00:15:07,252 --> 00:15:09,587
I'm not sure, I couldn't
hear through all the crying.

386
00:15:09,621 --> 00:15:11,288
- He's crying?
- No, I was crying.

387
00:15:11,307 --> 00:15:12,557
It's been a stressful day,

388
00:15:12,591 --> 00:15:13,758
and he's had a really rough life.

389
00:15:13,792 --> 00:15:15,193
Oh, my God.

390
00:15:15,227 --> 00:15:16,427
We should cancel it, right?
Maybe we should cancel it?

391
00:15:16,462 --> 00:15:17,662
- Yeah.
- Jerry...

392
00:15:17,696 --> 00:15:19,180
You were in charge of getting a crowd.

393
00:15:19,231 --> 00:15:20,765
Please tell me that you pulled
a Jerry, and no one's here.

394
00:15:20,799 --> 00:15:22,100
Okay, well, first of all,

395
00:15:22,134 --> 00:15:23,568
I don't like it when you guys
use that term.

396
00:15:23,602 --> 00:15:25,770
But for the record, I came through.

397
00:15:25,804 --> 00:15:28,139
There are almost
a hundred people out there!

398
00:15:28,157 --> 00:15:31,025
Oh, damn it, Jerry, you just
had to do your job, didn't you?

399
00:15:31,076 --> 00:15:32,977
Yeah, can't you do anything
wrong, Jerry?

400
00:15:33,012 --> 00:15:34,979
I've been looking
at our utter and complete lack

401
00:15:34,997 --> 00:15:37,332
of experience as a positive thing,

402
00:15:37,366 --> 00:15:38,833
but I'm starting to think

403
00:15:38,867 --> 00:15:40,651
it actually might be a problem.

404
00:15:44,013 --> 00:15:45,384
Okay, guys, everybody listen up.

405
00:15:45,504 --> 00:15:47,180
I just wanted to say thank you.

406
00:15:47,300 --> 00:15:49,828
You've all volunteered
your time and, no matter what,

407
00:15:49,862 --> 00:15:51,663
I am eternally grateful for it.

408
00:15:51,697 --> 00:15:52,831
Now, I'm going to go out there,

409
00:15:52,865 --> 00:15:54,666
and I'm going to
announce the relaunch,

410
00:15:54,700 --> 00:15:57,268
and I'm going to muddle through
this thing as best I can.

411
00:15:57,303 --> 00:15:59,470
You should all leave,
and when this thing is over,

412
00:15:59,505 --> 00:16:00,805
I'll meet you at the nearest bar.

413
00:16:00,840 --> 00:16:02,774
That would be Hurley's.

414
00:16:02,808 --> 00:16:04,125
- Oh, it's karaoke tonight!
- Yes!

415
00:16:04,160 --> 00:16:07,529
No, no, no, guys.
No. We're not leaving, okay?

416
00:16:07,563 --> 00:16:09,080
We're the reason
Leslie's in this mess.

417
00:16:09,114 --> 00:16:11,983
It's our mess.
We're going to stay here

418
00:16:12,017 --> 00:16:14,102
and we're going to
go out there as a team.

419
00:16:14,153 --> 00:16:18,907
Or, we go as a team to Hurley's.

420
00:16:18,941 --> 00:16:20,692
No, Ann's right.

421
00:16:20,726 --> 00:16:23,962
We're a team.
We're all going out together.

422
00:16:23,996 --> 00:16:25,129
Let's give this crowd a show.

423
00:16:25,164 --> 00:16:27,749
All: Go, team!

424
00:16:27,783 --> 00:16:30,702
[Cheering] 
<i>[Get On Your Feet</i> plays]

425
00:16:31,837 --> 00:16:35,573
♪ Get up and make it happen ♪

426
00:16:35,591 --> 00:16:37,909
♪ get on your feet ♪

427
00:16:38,928 --> 00:16:40,445
Tom?

428
00:16:40,479 --> 00:16:41,930
Couldn't afford enough premium carpet

429
00:16:41,964 --> 00:16:43,414
to get us to the stage.

430
00:16:43,432 --> 00:16:44,549
I mean, it was a short walk,

431
00:16:44,583 --> 00:16:46,217
but it was pretty luxurious, right?

432
00:16:46,251 --> 00:16:48,252
Okay.

433
00:16:54,643 --> 00:16:55,977
Everybody smile and wave.

434
00:16:56,028 --> 00:16:59,447
[Music and cheering resumes]

435
00:16:59,481 --> 00:17:03,868
♪ Get up and make it happen ♪

436
00:17:03,903 --> 00:17:06,104
♪ get on your feet ♪

437
00:17:07,373 --> 00:17:09,908
Stop leaning on my arm, Andy.

438
00:17:11,210 --> 00:17:13,828
[Crowd gasps]

439
00:17:13,879 --> 00:17:15,413
I'm holding the dog.

440
00:17:15,447 --> 00:17:17,498
He is peeing.
He is now peeing.

441
00:17:17,549 --> 00:17:19,283
I am putting him down.

442
00:17:19,301 --> 00:17:21,052
No, Ron, he only has three legs!

443
00:17:21,086 --> 00:17:22,420
[Music resumes]

444
00:17:22,454 --> 00:17:25,023
- Get off me!
- No, I'm scared!

445
00:17:25,057 --> 00:17:26,891
♪ Get up and make it happen ♪

446
00:17:26,926 --> 00:17:29,494
Ron? Ron?
There's no stairs.

447
00:17:29,528 --> 00:17:31,128
How do I get onto the stage?

448
00:17:32,564 --> 00:17:33,998
Oh!

449
00:17:34,033 --> 00:17:36,301
♪ And take some action ♪

450
00:17:36,335 --> 00:17:38,069
[Music fades]

451
00:17:43,659 --> 00:17:46,160
♪ Get on your feet ♪

452
00:17:48,013 --> 00:17:51,182
♪ get up and make it happen ♪

453
00:17:51,216 --> 00:17:53,418
♪ get on your feet ♪

454
00:17:53,452 --> 00:17:56,020
Okay.
[Grunts]

455
00:17:56,055 --> 00:17:59,590
Uhh! Uhh!

456
00:17:59,625 --> 00:18:00,591
Uhh!

457
00:18:00,626 --> 00:18:01,592
[Pants]

458
00:18:01,627 --> 00:18:03,027
♪ Get on your feet ♪

459
00:18:03,062 --> 00:18:06,798
Mm-mm.
Here you go. Drink up,

460
00:18:06,832 --> 00:18:09,667
because it has every herb
in my herb belt.

461
00:18:09,685 --> 00:18:12,103
Oh. Tastes like a belt.

462
00:18:12,137 --> 00:18:15,306
I know, isn't it awful?
But it works wonderfully.

463
00:18:15,340 --> 00:18:16,891
So, tell me.

464
00:18:16,942 --> 00:18:19,577
Do you admit that you're depressed?

465
00:18:19,611 --> 00:18:21,546
How did you know?
I didn't even know.

466
00:18:21,580 --> 00:18:23,781
The Letters to Cleo t-shirt,
the unshaven face,

467
00:18:23,816 --> 00:18:26,684
the Doc Martins, and your hair
does not have that normal,

468
00:18:26,702 --> 00:18:30,521
uptight, rigid, inflexible
Ben Wyatt sense of fun.

469
00:18:30,539 --> 00:18:34,058
You can't hide
these things from friends.

470
00:18:34,093 --> 00:18:36,761
I think I'm feeling better.

471
00:18:36,795 --> 00:18:39,297
Herbal smoothie,
you have done it again.

472
00:18:39,331 --> 00:18:43,001
As a loyal pawneean, I've
always been proud of this town,

473
00:18:43,035 --> 00:18:45,069
and, I, oh, um, sorry.

474
00:18:45,104 --> 00:18:47,972
My cards got out of order here
when they fell.

475
00:18:48,007 --> 00:18:54,645
Together, we can defeat
obese children.

476
00:18:54,680 --> 00:18:57,765
I'm sure that was something
positive, originally.

477
00:18:57,816 --> 00:19:01,319
I'm sorry, okay, this is
just a disaster, isn't it?

478
00:19:01,353 --> 00:19:06,391
This is the worst political
event ever in history.

479
00:19:06,425 --> 00:19:10,728
Well, um, I can assure
you people in the bleachers

480
00:19:10,763 --> 00:19:13,831
that, if you follow my campaign,
it will be interesting.

481
00:19:13,866 --> 00:19:15,450
It sure will be!

482
00:19:15,501 --> 00:19:17,035
Ladies and gentlemen, I'm Pistol Pete,

483
00:19:17,069 --> 00:19:19,403
and around here, I'm a Pawnee legend!

484
00:19:19,421 --> 00:19:22,090
[Cheers and applause]

485
00:19:22,124 --> 00:19:25,259
I just wanted to say that
I fully endorse Leslie Knope.

486
00:19:25,294 --> 00:19:27,244
She's got a great team behind her,

487
00:19:27,262 --> 00:19:30,448
and a vote for her is a slam dunk.

488
00:19:30,482 --> 00:19:32,750
[Cheers and applause]

489
00:19:32,768 --> 00:19:34,318
Thanks, Pistol Pete!

490
00:19:34,353 --> 00:19:36,854
Wow, we're so happy to have you.

491
00:19:36,889 --> 00:19:39,524
Oh, hey. There he goes.

492
00:19:39,558 --> 00:19:40,525
Come on, Pete!

493
00:19:40,559 --> 00:19:41,943
Oh!
[Crowd gasps]

494
00:19:41,977 --> 00:19:44,645
Okay, stay still.
Stay down.

495
00:19:44,696 --> 00:19:48,132
Pistol Pete, everybody.
Still got it.

496
00:19:48,167 --> 00:19:49,784
[Music resumes]

497
00:19:49,818 --> 00:19:53,604
- Hey.
- Hey, how was the rally?

498
00:19:53,639 --> 00:19:55,506
- Oh, we nailed it!
- No, we didn't.

499
00:19:55,541 --> 00:19:58,176
Ben, my campaign manager
and I have made a decision.

500
00:19:58,210 --> 00:20:01,279
We've decided to fire
the campaign manager--me.

501
00:20:01,313 --> 00:20:04,982
- And hire you.
- But we've talked about this.

502
00:20:05,017 --> 00:20:07,418
I don't care if you're
poison to my campaign.

503
00:20:07,453 --> 00:20:11,122
This team has a lot of heart
and zero know-how.

504
00:20:11,156 --> 00:20:12,890
You're the only one that can save us.

505
00:20:12,925 --> 00:20:15,726
Please be my campaign manager.

506
00:20:15,761 --> 00:20:18,162
You know, Leslie, Ben's really
been going through something,

507
00:20:18,197 --> 00:20:21,165
and he's on a journey, so I
don't think he's gonna be--

508
00:20:21,200 --> 00:20:22,834
I'll do it. I'm in.

509
00:20:22,868 --> 00:20:25,403
I've done enough exploring.
I'm good.

510
00:20:25,437 --> 00:20:26,804
- Okay, great.
- Ann, you're fired.

511
00:20:26,822 --> 00:20:29,473
Oh, thank God.

512
00:20:29,491 --> 00:20:32,754
No, don't make out.
It's making Champion sad.

513
00:20:34,518 --> 00:20:41,257
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

514
00:20:41,377 --> 00:20:43,510
Surprise!

515
00:20:43,750 --> 00:20:46,886
We felt really bad about
the whole dredging up your past

516
00:20:46,920 --> 00:20:49,222
just to be publically
humiliated thing.

517
00:20:49,256 --> 00:20:52,225
- So, cookies.
- And balloons.

518
00:20:52,259 --> 00:20:55,294
Thanks, guys. It--
that's nice.

519
00:20:55,312 --> 00:20:56,562
We feel so bad.

520
00:20:56,597 --> 00:20:59,031
Is there anything else we can do?

521
00:20:59,066 --> 00:21:00,666
You could let me take you to dinner.

522
00:21:00,701 --> 00:21:02,535
Yes, Ann, yes.
You should do that.

523
00:21:02,569 --> 00:21:04,570
Oh, I can't, though,
because I'm married.

524
00:21:04,605 --> 00:21:07,190
You're not married.
She's not married.

525
00:21:09,409 --> 00:21:11,410
This is uncomfortable.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
