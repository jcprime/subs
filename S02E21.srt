1
00:00:01,376 --> 00:00:04,295
- Can I run something by you?
- Sure. I love having things run by me.

2
00:00:05,145 --> 00:00:06,811
I feel like you're being sarcastic.

3
00:00:06,936 --> 00:00:08,767
No, no. I'm not.
I genuinely love it.

4
00:00:08,892 --> 00:00:10,546
Run it by me.
Go ahead. Go go.

5
00:00:10,671 --> 00:00:12,602
So I've been worried
that Ann maybe thinks

6
00:00:12,727 --> 00:00:15,318
that our relationship
isn't moving forward fast enough.

7
00:00:15,443 --> 00:00:19,000
And so I'm wondering
if maybe I should ask her

8
00:00:19,125 --> 00:00:21,372
if she would like to move in
with me.

9
00:00:21,497 --> 00:00:23,397
Good idea? Bad idea?
You tell me.

10
00:00:23,847 --> 00:00:26,676
I would say that some women
won't consider moving in with someone

11
00:00:26,801 --> 00:00:29,280
unless they think marriage
is in the future, which I know is...

12
00:00:29,405 --> 00:00:31,005
I intend to marry her.

13
00:00:33,462 --> 00:00:34,492
For realskies?

14
00:00:34,660 --> 00:00:37,060
I'm not really quite sure
how I feel.

15
00:00:38,621 --> 00:00:41,404
It's a little weird.
I'm happy for them. They're my friends.

16
00:00:41,529 --> 00:00:42,713
I'm nauseous.

17
00:00:42,838 --> 00:00:46,038
If I could just sum it up
in one word, it would be...

18
00:00:55,806 --> 00:00:57,431
- Feygnasse Team -

19
00:00:57,775 --> 00:00:59,219
.:: La Fabrique ::.

20
00:01:00,161 --> 00:01:02,053
Episode 221
<i>94 Meetings</i>

21
00:01:03,323 --> 00:01:04,501
mpm - lestat

22
00:01:18,718 --> 00:01:19,918
Parks and rec.

23
00:01:22,588 --> 00:01:23,888
Confirming what?

24
00:01:25,396 --> 00:01:26,896
That's not possible.

25
00:01:35,429 --> 00:01:38,597
Why are there six people outside
who say they're waiting to meet with me?

26
00:01:38,765 --> 00:01:41,306
So you know how you love me
because you haven't had

27
00:01:41,431 --> 00:01:43,936
a single meeting with anyone
since I became your assistant?

28
00:01:44,355 --> 00:01:46,886
That's because
every time someone calls

29
00:01:47,011 --> 00:01:48,607
and requests a meeting with you,

30
00:01:48,732 --> 00:01:50,985
I always schedule it for march 31st.

31
00:01:51,524 --> 00:01:54,405
- Why?
- I didn't think march 31st existed.

32
00:01:55,258 --> 00:01:58,671
30 days has September,
April, March, and November.

33
00:01:58,994 --> 00:02:00,202
June and November.

34
00:02:01,462 --> 00:02:03,289
Today is March 31st.

35
00:02:03,708 --> 00:02:04,582
I know.

36
00:02:04,707 --> 00:02:07,457
So then how many meetings
do I have today?

37
00:02:08,263 --> 00:02:09,063
93.

38
00:02:13,231 --> 00:02:15,384
- You know what to do.
- Right.

39
00:02:15,751 --> 00:02:18,622
To me, this situation
is a blood-soaked,

40
00:02:18,747 --> 00:02:20,347
nightmarish hellscape.

41
00:02:21,304 --> 00:02:22,767
However, to Leslie Knope...

42
00:02:23,202 --> 00:02:24,350
How fun.

43
00:02:25,270 --> 00:02:27,807
Why don't you take as many as you can
and I'll cover the rest?

44
00:02:27,932 --> 00:02:28,898
I have an idea.

45
00:02:29,066 --> 00:02:31,601
Why don't I try to rustle up
seven more meetings

46
00:02:31,726 --> 00:02:34,278
'cause I think it would be cooler
if there were an even 100.

47
00:02:34,446 --> 00:02:36,530
Why don't we stick to these 93
and see how it goes.

48
00:02:37,886 --> 00:02:39,366
This is the perfect distraction.

49
00:02:39,712 --> 00:02:41,032
So I have everything I need.

50
00:02:41,157 --> 00:02:44,393
I have a fresh cup of coffee,
comfy, fur-lined boots.

51
00:02:44,665 --> 00:02:46,165
I'll need them later.

52
00:02:46,390 --> 00:02:49,198
Yeah, no problem. They're actually
a little narrow for me.

53
00:02:49,323 --> 00:02:50,669
Leslie, here's a guy.

54
00:02:50,837 --> 00:02:52,671
Hi, I'm Leslie Knope,
deputy director.

55
00:02:52,880 --> 00:02:55,111
Bill Haggardy,
Pawnee historical society.

56
00:02:55,236 --> 00:02:57,060
I'm supposed to be meeting
with Ron Swanson.

57
00:02:57,185 --> 00:02:58,844
I understand that. He's tied up.

58
00:02:59,012 --> 00:03:01,304
I've been trying
to meet with him three months

59
00:03:01,429 --> 00:03:02,936
and now he's not available.

60
00:03:03,061 --> 00:03:06,420
It's probably too late now,
but some rich woman, Jessica Wicks,

61
00:03:06,545 --> 00:03:08,916
has rented out Turnbill mansion
for a party

62
00:03:09,041 --> 00:03:10,523
and she's making alterations.

63
00:03:11,039 --> 00:03:12,239
"Alterations"?

64
00:03:12,600 --> 00:03:15,694
Turnbill mansion is one of the most
beloved historical sites in Pawnee.

65
00:03:16,093 --> 00:03:19,617
In 1867, the progressive
reverend Turnbill

66
00:03:20,149 --> 00:03:22,412
officiated a wedding
between a white woman

67
00:03:22,537 --> 00:03:24,893
and a Wamapoke Indian chief.

68
00:03:25,702 --> 00:03:28,921
The secret ceremony
was beautiful and romantic.

69
00:03:30,007 --> 00:03:33,042
But then word got out
and the reception was a bloodbath.

70
00:03:36,123 --> 00:03:38,573
Fortunately,
there were two survivors.

71
00:03:39,245 --> 00:03:41,428
Unfortunately,
they were both horses.

72
00:03:41,926 --> 00:03:44,726
Hold up. Former beauty queen
Jessica Wicks?

73
00:03:45,929 --> 00:03:48,269
Tom Haverford. I'm gonna
be running point on this, Bill.

74
00:03:48,437 --> 00:03:51,286
- Why didn't you try to stop her?
- Her husband is Nick Newport, Sr.

75
00:03:51,411 --> 00:03:54,108
From Sweetums. Everyone in this town
is afraid to say no to him.

76
00:03:54,411 --> 00:03:55,971
Everybody just relax.

77
00:03:56,403 --> 00:03:57,653
What are the alterations?

78
00:03:57,839 --> 00:04:01,537
Drilling holes. Painting.
Removing wainscoting.

79
00:04:01,885 --> 00:04:03,409
She's tearing down the gazebo.

80
00:04:03,641 --> 00:04:06,077
What? She's what?
No, she's not.

81
00:04:06,202 --> 00:04:08,052
Okay, Tom, go get the car.

82
00:04:09,632 --> 00:04:11,667
- Don't throw things at me.
- These are tight.

83
00:04:12,531 --> 00:04:14,003
I'll devise an action plan

84
00:04:14,171 --> 00:04:15,907
to help us coordinate better.

85
00:04:16,032 --> 00:04:18,132
Will you? Great.
Thanks for coming in.

86
00:04:18,555 --> 00:04:20,759
- Who's next?
- Tom, I'll meet you at the car.

87
00:04:21,114 --> 00:04:23,180
Where are you going?
We have 91 more meetings.

88
00:04:23,305 --> 00:04:25,044
Sorry.
As much as I would like to go

89
00:04:25,169 --> 00:04:27,480
for the all-time, city hall
single day meetings record,

90
00:04:27,605 --> 00:04:29,101
there is an emergency.

91
00:04:29,226 --> 00:04:31,576
Someone is trying to alter a gazebo.

92
00:04:37,025 --> 00:04:38,819
Sorry I can't hang out.
I have an emergency.

93
00:04:39,147 --> 00:04:41,083
I'm actually here to see Mark.

94
00:04:41,208 --> 00:04:43,157
Right. Thanks for the coffee.

95
00:04:43,325 --> 00:04:45,719
- That's also for Mark.
- I really need it though.

96
00:04:45,844 --> 00:04:48,068
But next time more sugar.
Okay, thanks. Bye!

97
00:04:48,705 --> 00:04:50,080
I postpone the rest?

98
00:04:50,379 --> 00:04:52,439
Or I could set myself on fire
and create a diversion.

99
00:04:53,164 --> 00:04:54,710
I'm gonna do all of these today.

100
00:04:54,934 --> 00:04:56,128
Round up whoever's free.

101
00:04:56,619 --> 00:04:58,819
I'm gonna need more Ron Swansons.

102
00:04:59,830 --> 00:05:01,550
Look at that.
It's gorgeous.

103
00:05:01,718 --> 00:05:04,574
What kind of monster
would want to change this?

104
00:05:05,981 --> 00:05:06,981
Pre-zit.

105
00:05:07,290 --> 00:05:09,536
Do you have any brown concealer
by any chance?

106
00:05:09,661 --> 00:05:11,222
I need to look good for Jessica.

107
00:05:11,347 --> 00:05:13,718
They may be rich,
but they can't just destroy history.

108
00:05:13,843 --> 00:05:15,596
- Let's go.
- Right behind you.

109
00:05:15,721 --> 00:05:18,701
I just need to spend a minute
on my cologne cloud.

110
00:05:20,092 --> 00:05:22,453
- What is that?
- Attack by Dennis Feinstein.

111
00:05:22,578 --> 00:05:25,199
When you want to attack the senses
of the lady you want to bed.

112
00:05:26,599 --> 00:05:28,200
Door! Door! Door! Door!

113
00:05:28,325 --> 00:05:29,325
Close it.

114
00:05:30,841 --> 00:05:33,126
So basically,
we're completely swamped.

115
00:05:33,251 --> 00:05:34,458
All hands on deck.

116
00:05:34,747 --> 00:05:36,058
I don't even work here.

117
00:05:36,183 --> 00:05:39,463
Don't care. I need anyone
with a pulse and a brain to pitch in.

118
00:05:40,177 --> 00:05:41,757
Do you need help with anything?

119
00:05:41,925 --> 00:05:44,593
No. We're good. Thanks.
In fact, you can head home early.

120
00:05:45,793 --> 00:05:48,514
Right now the four of us
are gonna divvy up all these meetings.

121
00:05:48,682 --> 00:05:50,408
Make 'em feel
like they've been heard.

122
00:05:50,533 --> 00:05:51,970
- Understood?
- Yes, sir.

123
00:05:52,095 --> 00:05:54,195
Bert Macklin, FBI, on the case.

124
00:05:56,142 --> 00:05:58,084
April, you take Leslie's office.

125
00:05:58,209 --> 00:05:59,581
Andy, the conference room.

126
00:05:59,706 --> 00:06:01,723
Ann, take your meetings
in the courtyard.

127
00:06:01,848 --> 00:06:05,009
Just sit there
and don't ruin the city.

128
00:06:05,946 --> 00:06:06,946
Dismissed.

129
00:06:09,082 --> 00:06:10,597
What are you doing?

130
00:06:10,722 --> 00:06:12,079
Can I get a badge?

131
00:06:14,039 --> 00:06:16,291
Brought you coffee.
Help you get through the day.

132
00:06:16,492 --> 00:06:17,751
Thanks, dude.

133
00:06:18,928 --> 00:06:20,721
No problem, lady.

134
00:06:25,977 --> 00:06:27,462
- I'm gonna go.
- Okay.

135
00:06:27,587 --> 00:06:29,138
April is the best.

136
00:06:29,503 --> 00:06:30,653
But she's 20.

137
00:06:31,276 --> 00:06:33,217
When April was born,
I was already in 3rd grade.

138
00:06:33,342 --> 00:06:37,077
So if we were friends back then,
I'd have been hanging out with a baby.

139
00:06:37,896 --> 00:06:40,446
I don't know anything
about infant care.

140
00:06:41,265 --> 00:06:42,943
My God, I could've killed her.

141
00:06:45,883 --> 00:06:47,383
Mr. Newport, sr.

142
00:06:47,587 --> 00:06:50,253
I've just been informed
that you plan on altering this mansion.

143
00:06:50,378 --> 00:06:52,386
And as a member
of the parks department,

144
00:06:52,511 --> 00:06:54,558
as a Pawneean, and an American,

145
00:06:54,683 --> 00:06:58,417
I ask you to refrain
from harming this historic structure.

146
00:06:59,611 --> 00:07:00,461
What?

147
00:07:00,795 --> 00:07:03,645
- I'll take care of this, love nut.
- Biscuit.

148
00:07:05,103 --> 00:07:08,010
I'm Jessica Wicks.
Miss Pawnee 1994.

149
00:07:08,223 --> 00:07:11,055
We've met before. We were both judges
at the pageant last year.

150
00:07:11,279 --> 00:07:14,027
Leslie Knorp.
Of course. How are you?

151
00:07:14,152 --> 00:07:16,771
Jessica Wicks! Hey, boo.

152
00:07:16,896 --> 00:07:19,104
Are you aging in reverse
'cause you look barely legal?

153
00:07:20,408 --> 00:07:22,316
If you're not the most
charming man in Indiana.

154
00:07:22,484 --> 00:07:25,246
And this must be
the luckiest man in Indiana.

155
00:07:25,371 --> 00:07:26,945
Sir, it is an honor to meet you.

156
00:07:28,801 --> 00:07:30,051
Come here, you.

157
00:07:32,303 --> 00:07:33,653
Tell me straight.

158
00:07:34,062 --> 00:07:35,407
Are you a Chinese?

159
00:07:37,033 --> 00:07:38,582
I'm one of the Indian people.

160
00:07:38,959 --> 00:07:42,668
That floppy old bag of money
is gonna be dead in, like, a month.

161
00:07:42,793 --> 00:07:46,343
And who's gonna comfort Jessica
and her millions of dollars?

162
00:07:47,169 --> 00:07:51,011
Yeah, Jessica's a gold digger,
but I'm a gold digger digger.

163
00:07:52,214 --> 00:07:54,523
So how did you and Mr. Newport meet?

164
00:07:54,648 --> 00:07:57,392
Well, I was doing a ribbon
cutting at the hospital.

165
00:07:57,560 --> 00:08:00,159
And he was there
because his blood doesn't work.

166
00:08:00,284 --> 00:08:01,948
And we started talking.

167
00:08:02,073 --> 00:08:04,173
And then I realized who he was.

168
00:08:05,131 --> 00:08:07,236
Oh, my gosh.
It was love at first sight.

169
00:08:07,690 --> 00:08:09,740
That sounds so romantic.

170
00:08:10,366 --> 00:08:13,022
I know you've rented out this house
for a party,

171
00:08:13,147 --> 00:08:16,175
but it's on the historical register
so we can't allow any changes.

172
00:08:16,300 --> 00:08:17,639
I'm so sorry.

173
00:08:18,176 --> 00:08:20,040
It's a little late for that,
sweetheart.

174
00:08:20,208 --> 00:08:22,084
I told them
what I was doing months ago.

175
00:08:22,603 --> 00:08:24,227
Where I come from there's a saying

176
00:08:24,352 --> 00:08:27,214
- "what's done is done".
- That's a saying everywhere.

177
00:08:27,382 --> 00:08:29,311
I've never heard it
it's a great saying.

178
00:08:29,436 --> 00:08:32,094
Maybe we can limit the damage.
How much have you done?

179
00:08:32,742 --> 00:08:34,668
You painted this?

180
00:08:35,645 --> 00:08:37,829
Miami teal
is Nicky's favorite color.

181
00:08:37,954 --> 00:08:40,030
We also replaced
some of the sconces,

182
00:08:40,155 --> 00:08:43,132
got rid of the furniture,
painted these floors black.

183
00:08:43,257 --> 00:08:45,254
These were
the original hardwood floors.

184
00:08:45,379 --> 00:08:48,248
They were all scratched up.
I did you a favor.

185
00:08:48,373 --> 00:08:50,320
I'll show you
what we did in the sitting room.

186
00:08:50,809 --> 00:08:53,309
After you.
I'd hate to miss the view.

187
00:08:53,434 --> 00:08:56,808
You little devil. I should just pay you
to follow me around.

188
00:08:56,933 --> 00:08:58,889
If that's a job offer, I accept.

189
00:09:00,113 --> 00:09:02,602
Why did you have to drill holes
in the wall?

190
00:09:02,727 --> 00:09:04,898
To hang my birthday present
to Nicky.

191
00:09:06,095 --> 00:09:07,284
My heavens.

192
00:09:07,468 --> 00:09:10,345
Isn't there a way that you...
Oh, my.

193
00:09:11,588 --> 00:09:13,022
History is important.

194
00:09:13,147 --> 00:09:15,811
You can't go around
changing everything all the time

195
00:09:15,936 --> 00:09:18,743
or else next thing you know
they'll be painting the White House...

196
00:09:19,623 --> 00:09:20,623
Not white.

197
00:09:22,259 --> 00:09:24,597
I'm so angry,
I can't think of another color.

198
00:09:25,593 --> 00:09:26,565
Green.

199
00:09:26,733 --> 00:09:29,370
So your department banned me
from attending games

200
00:09:29,495 --> 00:09:32,339
just because I yell
"you suck" at the players.

201
00:09:32,464 --> 00:09:35,991
According to the complaint,
you yelled it at five-year-old girls.

202
00:09:36,307 --> 00:09:37,326
Who suck!

203
00:09:37,867 --> 00:09:40,067
Why is that so hard to understand?

204
00:09:40,538 --> 00:09:42,831
I thought I was having
this meeting with Ron Swanson.

205
00:09:43,259 --> 00:09:46,376
I'm afraid that Ron Swanson's
currently dead.

206
00:09:47,295 --> 00:09:49,390
I'm his daughter.
April Swanson.

207
00:09:49,999 --> 00:09:53,967
And it's his last wish
that I have this meeting with you.

208
00:09:54,441 --> 00:09:56,136
I represent the ultimate
Frisbee league.

209
00:09:56,906 --> 00:09:58,510
Pawnee has an ultimate league?

210
00:09:58,635 --> 00:10:00,599
Yes, and we keep running
into conflicts...

211
00:10:00,767 --> 00:10:02,100
You won me over.

212
00:10:02,268 --> 00:10:03,477
I will join your team.

213
00:10:03,850 --> 00:10:05,687
I'm sorry, what we need...

214
00:10:05,855 --> 00:10:08,015
When does practice start?
You provide the jerseys?

215
00:10:08,140 --> 00:10:10,750
What color are the jerseys by the way?
What's our team name?

216
00:10:10,875 --> 00:10:12,168
Are we "the lightning"?

217
00:10:13,449 --> 00:10:16,457
Now, I have to tell you, I don't work
in the parks department.

218
00:10:16,582 --> 00:10:18,382
- I'm a nurse.
- Seriously?

219
00:10:19,576 --> 00:10:22,663
That's great news.
I have the weirdest thing on my arm.

220
00:10:24,249 --> 00:10:25,207
Can you see?

221
00:10:26,421 --> 00:10:27,626
Every time.

222
00:10:28,114 --> 00:10:31,070
Well, thank God you haven't
torn down the gazebo yet.

223
00:10:31,399 --> 00:10:32,358
Don't remind me.

224
00:10:32,612 --> 00:10:35,152
The demolition people couldn't
get here until this afternoon.

225
00:10:35,277 --> 00:10:37,594
I've had to look at that ugly thing
all day long.

226
00:10:38,103 --> 00:10:39,153
Ugly thing?

227
00:10:40,108 --> 00:10:42,975
Let me tell you a little something
about this ugly thing, ma'am.

228
00:10:43,143 --> 00:10:45,098
150 years ago, an interracial couple

229
00:10:45,223 --> 00:10:48,227
was married here
and slaughtered by their own families.

230
00:10:48,352 --> 00:10:50,691
It's one of the most beautiful stories
in Pawnee's history.

231
00:10:50,859 --> 00:10:54,333
- Why are you trying to destroy it?
- Don't tell me about Pawnee history.

232
00:10:54,458 --> 00:10:55,988
The Newports made this town.

233
00:10:56,517 --> 00:11:00,268
And tonight, we're gonna celebrate
my husband's 85th birthday

234
00:11:00,393 --> 00:11:02,953
without this mushy,
old, pile of rot.

235
00:11:03,121 --> 00:11:05,106
Your husband's a mushy,
old, pile of rot.

236
00:11:05,231 --> 00:11:08,123
You were a stick in the mud
when we judged that beauty pageant

237
00:11:08,248 --> 00:11:10,432
you're being a stick in the mud now!

238
00:11:10,670 --> 00:11:12,212
I am not a stick in the mud!

239
00:11:13,955 --> 00:11:16,258
I just want to stop a party
from happening.

240
00:11:17,510 --> 00:11:21,228
I have a gazebo update so ignore
all my previous voicemails and emails.

241
00:11:21,353 --> 00:11:23,724
Leslie, I haven't relieved myself
in five hours.

242
00:11:23,849 --> 00:11:25,499
So if you'll excuse me.

243
00:11:25,970 --> 00:11:28,653
I'm sorry, but this can't wait.
Jessica Wicks refuses...

244
00:11:28,778 --> 00:11:30,328
Leslie, what the?

245
00:11:31,691 --> 00:11:33,584
Councilman Howser!
Nice to see you again.

246
00:11:33,709 --> 00:11:35,602
Not that I saw anything
other than your face.

247
00:11:35,727 --> 00:11:38,078
And I would like to talk to you
about this gazebo thing.

248
00:11:38,203 --> 00:11:41,033
- You know, the p...
- Enough of this.

249
00:11:41,447 --> 00:11:43,827
Do whatever you want.
Alert the media. Call Fema.

250
00:11:44,184 --> 00:11:47,140
I don't care.
Do not bother me with this again.

251
00:11:51,583 --> 00:11:53,496
- Miss Knope.
- Councilman Howser.

252
00:11:53,801 --> 00:11:55,151
I saw your penis.

253
00:11:55,611 --> 00:11:57,966
There are eight swings
at ramsett park

254
00:11:58,134 --> 00:12:00,228
and every single one of them
is broken.

255
00:12:00,353 --> 00:12:02,095
Can you please just fix one of them?

256
00:12:02,973 --> 00:12:05,408
Ron told me I can't
say yes to anything.

257
00:12:05,533 --> 00:12:07,833
But it's such a reasonable request,

258
00:12:09,164 --> 00:12:10,395
I can't just say no.

259
00:12:12,440 --> 00:12:13,840
So that's a "yes"?

260
00:12:16,778 --> 00:12:19,407
Are you saying "yes"?
Just say out loud "yes" or "no".

261
00:12:25,126 --> 00:12:27,454
Yeah, they do look
a little bit swollen.

262
00:12:27,996 --> 00:12:29,783
Just follow the pen.

263
00:12:30,742 --> 00:12:32,334
Follow the pen t...

264
00:12:33,753 --> 00:12:35,045
Does that hurt?

265
00:12:37,465 --> 00:12:39,716
This ordinance can't go through.
Too much red tape.

266
00:12:40,316 --> 00:12:42,411
- This gridlock drives me nuts.
- Tell me about it.

267
00:12:42,536 --> 00:12:44,595
You're gonna have to make
an end run, you know?

268
00:12:44,720 --> 00:12:46,431
Go right to the commissioner.

269
00:12:46,599 --> 00:12:49,017
I hadn't thought of that.
That is a really great idea.

270
00:12:49,338 --> 00:12:50,738
I'm gonna do that.

271
00:12:51,229 --> 00:12:53,480
Your last resort is probably
gonna be city council.

272
00:12:53,831 --> 00:12:56,731
- Good luck there.
- Yeah, my thoughts exactly.

273
00:12:57,262 --> 00:12:58,985
I have no idea what I was saying.

274
00:12:59,268 --> 00:13:00,112
I ask you,

275
00:13:00,322 --> 00:13:02,406
is this too revealing
for a public pool?

276
00:13:02,878 --> 00:13:05,033
Kindly get your groin off my desk.

277
00:13:05,329 --> 00:13:07,202
So my body makes you
uncomfortable too?

278
00:13:10,848 --> 00:13:13,415
I'm kind of freaking out
about this historical mansion thing.

279
00:13:13,540 --> 00:13:16,128
- Can I talk to you?
- I'm headed right out the door.

280
00:13:16,916 --> 00:13:18,630
You're going on a date with Ann?

281
00:13:18,798 --> 00:13:20,757
I was gonna surprise her.
Take her to restaurant.

282
00:13:20,975 --> 00:13:23,225
- Don't worry. Have a good time.
- You sure?

283
00:13:24,524 --> 00:13:26,529
- How we doing?
- Pretty good.

284
00:13:26,654 --> 00:13:29,854
I may have promised
a new aquatic center to somebody.

285
00:13:30,774 --> 00:13:31,894
Is that a problem?

286
00:13:32,019 --> 00:13:34,521
I diagnosed two melanomas.
They're both benign.

287
00:13:35,016 --> 00:13:36,731
- How many more meetings?
- 20.

288
00:13:38,323 --> 00:13:40,152
April was supposed to be the moat

289
00:13:40,443 --> 00:13:42,988
that kept the citizen barbarians

290
00:13:43,156 --> 00:13:44,508
away from Swanson castle.

291
00:13:44,633 --> 00:13:48,061
Instead, she blew up the castle
and stabbed me in the face.

292
00:13:48,867 --> 00:13:51,246
I hired you to do one thing.

293
00:13:52,291 --> 00:13:53,291
Just one.

294
00:13:53,967 --> 00:13:55,625
I don't care that you text all day

295
00:13:55,793 --> 00:13:58,503
and sleep at your desk.
In fact, I encourage it.

296
00:13:58,671 --> 00:14:01,603
But only because you were doing
that one thing...

297
00:14:02,034 --> 00:14:04,217
Keeping this crap off my desk!

298
00:14:06,027 --> 00:14:08,627
Give me five minutes
before the next one.

299
00:14:14,604 --> 00:14:16,512
That guy is scary when he's angry.

300
00:14:16,637 --> 00:14:18,037
Yeah, God, I know.

301
00:14:18,195 --> 00:14:19,816
Hey, sorry, dude.

302
00:14:20,699 --> 00:14:22,549
I don't need your sympathy.

303
00:14:22,815 --> 00:14:23,612
Or yours.

304
00:14:24,166 --> 00:14:26,166
I wasn't offering my sympathy.

305
00:14:27,408 --> 00:14:30,789
- Thank you for meeting me here.
- Sure. What's up?

306
00:14:31,162 --> 00:14:34,206
- I need you to get this word for word.
- It's a tape recorder so it will.

307
00:14:35,755 --> 00:14:37,852
Gazebo?
More like "ga-zoinks-bo".

308
00:14:37,977 --> 00:14:39,571
She may be a former beauty queen,

309
00:14:39,696 --> 00:14:42,172
but today she's the king
of destroying history.

310
00:14:42,340 --> 00:14:44,000
Could you talk normally?

311
00:14:44,125 --> 00:14:45,866
Okay, fine.
Ga-zoinks-bo.

312
00:14:46,026 --> 00:14:49,458
Jessica Wicks is throwing a party
for her husband, Nick Newport, sr.,

313
00:14:49,583 --> 00:14:52,557
- at the Turnbill mansion tonight.
- I'm gonna stop you right there.

314
00:14:53,230 --> 00:14:55,852
Nick Newport, Sr.
Is the former CEO of Sweetums.

315
00:14:56,020 --> 00:14:56,883
So?

316
00:14:57,008 --> 00:14:59,131
Sweetums owns the Pawnee journal.

317
00:14:59,256 --> 00:15:00,706
Crap on the cob.

318
00:15:01,067 --> 00:15:03,235
I will take it into consideration.

319
00:15:04,529 --> 00:15:05,877
Thank God!

320
00:15:06,525 --> 00:15:08,532
- That was it, right?
- No, there's one more.

321
00:15:08,700 --> 00:15:09,658
Damn it to hell!

322
00:15:09,826 --> 00:15:12,786
It's with me. And it's right now.
And it's about me quitting.

323
00:15:13,913 --> 00:15:14,857
I quit.

324
00:15:18,162 --> 00:15:19,448
I have no other choice.

325
00:15:19,573 --> 00:15:22,826
This is the only rational way
I know to prevent this tragedy.

326
00:15:23,846 --> 00:15:25,757
Here they come.
Throw away the key!

327
00:15:26,071 --> 00:15:27,897
- Seriously?
- Yes, throw it!

328
00:15:29,280 --> 00:15:32,390
- Out of the way!
- Sir, sorry, but you're a little late.

329
00:15:32,515 --> 00:15:35,684
You're gonna have to turn around
'cause you're not getting in here.

330
00:15:38,592 --> 00:15:40,522
My God. Tom, stop him!
Jump in front of it!

331
00:15:40,751 --> 00:15:42,501
Sacrifice your tiny body!

332
00:15:47,905 --> 00:15:50,369
I really thought that gate
would open in the middle.

333
00:15:53,433 --> 00:15:55,243
The gazebo has been destroyed.

334
00:15:55,368 --> 00:15:57,968
And the thing that bothers me
the most...

335
00:15:58,813 --> 00:16:01,418
- Cut it out, Tom.
- It never gets old!

336
00:16:03,666 --> 00:16:05,101
Ann and Mark are coming!

337
00:16:05,226 --> 00:16:07,401
Guys, not so close.
You're gonna...

338
00:16:07,526 --> 00:16:09,276
Don't go past the sensor!

339
00:16:11,247 --> 00:16:12,137
Sensor.

340
00:16:12,874 --> 00:16:15,348
- Sorry if I ruined your evening.
- It's totally fine.

341
00:16:15,516 --> 00:16:16,558
I chained myself.

342
00:16:16,726 --> 00:16:18,268
- I can see that.
- Are you okay?

343
00:16:18,436 --> 00:16:20,336
Nothing's bruised but my ego

344
00:16:20,715 --> 00:16:23,356
and my arm a little
from the mechanized gate.

345
00:16:23,779 --> 00:16:25,915
Thanks again for letting me
take those meetings.

346
00:16:26,040 --> 00:16:29,070
I've never really had a meeting before.
They're awesome.

347
00:16:29,318 --> 00:16:31,424
- You're welcome.
- Have you seen April around?

348
00:16:31,549 --> 00:16:33,119
She comes by at the end of the day.

349
00:16:33,244 --> 00:16:34,902
You might want to check her house.

350
00:16:35,027 --> 00:16:36,036
She quit on me.

351
00:16:36,700 --> 00:16:37,662
What? Why?

352
00:16:38,298 --> 00:16:40,669
She screwed up my entire life today.

353
00:16:40,794 --> 00:16:43,501
That was one mistake.
She's perfect for you.

354
00:16:44,131 --> 00:16:47,088
There's no one in the world
who's gonna do a better job for you.

355
00:16:48,800 --> 00:16:50,780
Either you hire her back or I quit.

356
00:16:50,905 --> 00:16:53,637
- You don't work for me.
- And I never will, sir.

357
00:16:54,409 --> 00:16:55,621
Good day.

358
00:17:01,282 --> 00:17:03,175
So what'd you guys
talk about at dinner?

359
00:17:03,300 --> 00:17:05,357
Was it fun?
Any big decisions arrived at?

360
00:17:06,008 --> 00:17:08,068
- It was fine.
- Do you need a ride home?

361
00:17:08,438 --> 00:17:11,071
No, Tom's gonna take me.
You guys go on ahead.

362
00:17:11,246 --> 00:17:12,867
I'm glad you're okay.
Call me.

363
00:17:18,858 --> 00:17:21,926
Well, hello there.
You must be Ron Swanson.

364
00:17:22,092 --> 00:17:23,750
Welcome to castle Ludgate!

365
00:17:24,163 --> 00:17:26,461
- Hello, Mr. Ludgate.
- Call me Larry.

366
00:17:26,722 --> 00:17:29,130
- Larry, who is it?
- It's zuzu's boss.

367
00:17:30,967 --> 00:17:32,108
Rita Ludgate.

368
00:17:32,448 --> 00:17:35,198
You have no idea
how nice it is to meet you.

369
00:17:36,422 --> 00:17:39,515
Come in, for gosh sake.
What can I get you to drink?

370
00:17:40,500 --> 00:17:42,340
Could you take
your shoes off, please?

371
00:17:42,572 --> 00:17:43,603
Thank you so much.

372
00:17:45,256 --> 00:17:47,107
Nothing to drink.
Thanks.

373
00:17:47,376 --> 00:17:48,936
Is April around?

374
00:17:49,061 --> 00:17:50,617
Zuzu, you have a guest!

375
00:17:51,791 --> 00:17:55,454
I'll fetch her.
She's probably on the internet again.

376
00:17:55,954 --> 00:17:57,404
I'll come with you.

377
00:17:57,956 --> 00:17:59,669
You make yourself at home.

378
00:18:05,037 --> 00:18:06,584
You must be April's sister.

379
00:18:07,830 --> 00:18:08,830
I'm Ron.

380
00:18:14,259 --> 00:18:15,802
Nice to meet you.

381
00:18:16,491 --> 00:18:19,013
No offense, but I think
the normal grieving time

382
00:18:19,138 --> 00:18:21,782
for gazebo destruction has passed.

383
00:18:23,102 --> 00:18:25,770
Well, maybe that's not entirely
what I'm upset about.

384
00:18:28,123 --> 00:18:29,858
Mark and Ann are gonna get married.

385
00:18:30,171 --> 00:18:31,171
Really?

386
00:18:32,013 --> 00:18:35,239
Why does that bother you?
I should be the one that's upset.

387
00:18:35,364 --> 00:18:38,357
It was supposed to be me and Ann,
or me and Jessica,

388
00:18:38,482 --> 00:18:40,832
or Ann and Jessica with me watching.

389
00:18:42,351 --> 00:18:45,698
Every time a couple gets married,
two single people die.

390
00:18:46,894 --> 00:18:48,812
I'm about to lose two good friends.

391
00:18:48,937 --> 00:18:49,794
Come on.

392
00:18:50,354 --> 00:18:53,514
They drove over here
and cut you down from a gate.

393
00:18:53,833 --> 00:18:57,552
You don't have to worry about them
abandoning you once you get married.

394
00:18:57,889 --> 00:18:59,929
Yeah, I know that.
In my brain.

395
00:19:00,636 --> 00:19:03,600
- Can we go?
- I just need to go do one more thing.

396
00:19:07,596 --> 00:19:09,225
<i>Loving smiles</i>

397
00:19:10,618 --> 00:19:13,777
<i>And smiling tears</i>

398
00:19:15,173 --> 00:19:17,957
<i>The midnight of my heart</i>

399
00:19:18,732 --> 00:19:21,232
- <i>Is you</i>
- Okay, I'm sorry, everyone.

400
00:19:21,786 --> 00:19:24,161
Excuse me. Excuse me.
But I need to say something.

401
00:19:25,526 --> 00:19:26,771
Is that gate lady?

402
00:19:26,896 --> 00:19:28,527
I know you're probably thinking

403
00:19:28,652 --> 00:19:31,023
"There's that crazy gate lady
from outside back again."

404
00:19:31,148 --> 00:19:33,774
And to some extent, you're right.
I am the gate lady.

405
00:19:33,899 --> 00:19:36,674
But the only thing I'm crazy about
is leaving the past behind.

406
00:19:37,036 --> 00:19:40,621
So I would like to apologize
and raise a glass

407
00:19:41,401 --> 00:19:44,974
to change, and forgiveness,
and the future,

408
00:19:45,142 --> 00:19:47,352
and Ann and Mark, and happiness,

409
00:19:47,520 --> 00:19:49,070
and Nick Newport, Sr.

410
00:19:49,431 --> 00:19:52,440
May all your future gazebos
live long and prosper.

411
00:19:52,990 --> 00:19:54,140
Please leave.

412
00:19:55,050 --> 00:19:57,606
Is it weird if I ask
to take a piece of cake with me?

413
00:19:57,731 --> 00:19:58,981
- I won't.
- Go.

414
00:20:00,853 --> 00:20:02,203
Why are you here?

415
00:20:03,229 --> 00:20:04,806
Come back to work.

416
00:20:06,788 --> 00:20:07,788
Come back.

417
00:20:09,128 --> 00:20:10,458
I want you to come back.

418
00:20:10,834 --> 00:20:12,897
Well, I don't want to come back.

419
00:20:13,022 --> 00:20:16,130
- The end.
- I was talking to Andy about you.

420
00:20:16,816 --> 00:20:19,884
And he made me realize
that I need you back at work.

421
00:20:20,134 --> 00:20:22,220
- What did he say?
- Don't recall.

422
00:20:22,388 --> 00:20:24,597
- Fine. I'll come back.
- Good.

423
00:20:26,767 --> 00:20:27,867
I'm leaving.

424
00:20:29,243 --> 00:20:30,350
Bye, Zuzu.

425
00:20:31,293 --> 00:20:32,856
Bye, Duke Silver.

426
00:20:37,542 --> 00:20:39,392
My mom has all your albums.

427
00:20:39,761 --> 00:20:41,489
I recognized you the day we met.

428
00:20:42,097 --> 00:20:43,616
Have you told anyone?

429
00:20:45,487 --> 00:20:46,487
Good girl.

430
00:20:49,964 --> 00:20:50,964
Excuse me.

431
00:20:51,212 --> 00:20:53,209
I had a meeting
with Ron Swanson yesterday,

432
00:20:53,377 --> 00:20:56,640
- but I have a little car trouble.
- Sorry, he's busy right now.

433
00:20:57,477 --> 00:20:59,424
- Well, can I reschedule?
- Sure.

434
00:21:00,601 --> 00:21:02,626
How about June 50th?

435
00:21:04,387 --> 00:21:06,812
Do you think you could
come back today at 2:65?

436
00:21:06,937 --> 00:21:09,350
- He's available then.
- What is going on?

437
00:21:09,768 --> 00:21:12,461
The only other day he has open
is march-tember one-teenth.

438
00:21:12,586 --> 00:21:13,996
Does that work, sir?

439
00:21:19,058 --> 00:21:20,399
www.sous-titres.eu

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
