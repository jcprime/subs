1
00:00:00,300 --> 00:00:07,000
Sync by honeybunny
www.addic7ed.com

2
00:00:12,239 --> 00:00:13,573
- You are getting
a rare glimpse

3
00:00:13,641 --> 00:00:16,342
At this exclusive,
government-only event.

4
00:00:16,410 --> 00:00:18,898
Each December, one department
puts on a show

5
00:00:18,965 --> 00:00:20,366
That spoofs what happened
in our town.

6
00:00:20,434 --> 00:00:22,465
Prepare to laugh
your asnov!

7
00:00:24,868 --> 00:00:27,666
Sid asnov is a former
city councilman.

8
00:00:27,734 --> 00:00:29,804
Some of the jokes are
sort of inside.

9
00:00:29,872 --> 00:00:30,978
Councilman dexhart,

10
00:00:31,045 --> 00:00:33,386
You're involved in
yet another scandal?

11
00:00:33,453 --> 00:00:37,067
That's crazier than
mayor gunderson's dog rufus.

12
00:00:40,408 --> 00:00:43,484
- Yes, there is a new one,
and it's a doozy.

13
00:00:43,551 --> 00:00:45,085
I don't want to go
into the details,

14
00:00:45,153 --> 00:00:48,527
But let me just say that
it involves multiple women,

15
00:00:48,595 --> 00:00:54,037
A love child, nurses,
rabbis, priests.

16
00:00:56,407 --> 00:00:57,641
Well, I hope you can all
join me

17
00:00:57,709 --> 00:00:59,377
At my fundraiser tonight.

18
00:00:59,445 --> 00:01:01,079
I am being supported,
of course,

19
00:01:01,147 --> 00:01:03,717
By the glitter factory
and 1-800-mattress.

20
00:01:05,590 --> 00:01:08,026
Ha ha ha ha! Classic.

21
00:01:08,094 --> 00:01:10,129
- Ã¢ÂTÂª pawnee is a city
that ain't very pretty Ã¢ÂTÂª

22
00:01:10,196 --> 00:01:12,300
Ã¢ÂTÂª and good government is
our goal Ã¢ÂTÂª

23
00:01:12,368 --> 00:01:14,303
- Ã¢ÂTÂª we may not be big
and our mayor wears a wig Ã¢ÂTÂª

24
00:01:14,371 --> 00:01:16,205
Ã¢ÂTÂª but at least the raccoon
infestation Ã¢ÂTÂª

25
00:01:16,273 --> 00:01:17,373
Ã¢ÂTÂª is under control Ã¢ÂTÂª

26
00:01:17,441 --> 00:01:22,915
-
Ã¢ÂTÂª it's the most wonderful time Ã¢ÂTÂª

27
00:01:22,983 --> 00:01:29,458
Ã¢ÂTÂª in pawnee Ã¢ÂTÂª

28
00:01:33,697 --> 00:01:41,737
Ã¢ÂTÂª Ã¢ÂTÂª

29
00:01:51,014 --> 00:01:53,984
- Lot 48 was once a horrible
and disgusting pit.

30
00:01:54,052 --> 00:01:57,656
And now it's the site
of pawnee's winter wonderland.

31
00:01:57,724 --> 00:02:00,025
This could not be more perfect
if I had planned it myself.

32
00:02:00,093 --> 00:02:02,596
Which I did.
And it's awesome.

33
00:02:02,664 --> 00:02:03,931
So let's talk schedge, guys.

34
00:02:03,999 --> 00:02:06,367
The kids are gonna come
at 9:00 a.M. On Saturday,

35
00:02:06,435 --> 00:02:09,238
And everyone's gonna do santa
for two hours.

36
00:02:09,305 --> 00:02:10,606
So I would just
advise everyone

37
00:02:10,674 --> 00:02:12,041
Before you get
in your costume

38
00:02:12,109 --> 00:02:14,377
To go to the bathroom so we can
avoid what happened last year.

39
00:02:14,445 --> 00:02:16,179
It was just farts.

40
00:02:16,247 --> 00:02:18,316
- 6:00 p.M.,
caroling with the youth choir.

41
00:02:18,384 --> 00:02:20,886
And I am proud to announce,
for the first time ever,

42
00:02:20,954 --> 00:02:22,788
This year
our tree-lighting ceremony

43
00:02:22,856 --> 00:02:24,858
Will be simulcast
on internet radio.

44
00:02:24,925 --> 00:02:27,394
Thank you.

45
00:02:27,462 --> 00:02:30,064
- That's a really big deal.
- It is. Thank you.

46
00:02:30,131 --> 00:02:32,199
- Listening to that tree
lighting's gonna be dope!

47
00:02:32,267 --> 00:02:34,969
- Okay, any questions,
direct them to me--dismissed.

48
00:02:35,037 --> 00:02:39,375
- Hey, uh, there was
a message for you

49
00:02:39,443 --> 00:02:40,510
On the work voicemail.

50
00:02:40,577 --> 00:02:42,178
Councilman dexhart wants
to meet with you

51
00:02:42,246 --> 00:02:44,849
At 9:00 p.M.--
The boardwalk lounge.

52
00:02:44,917 --> 00:02:46,517
- Shut up.
- No.

53
00:02:46,585 --> 00:02:49,988
- I'm not scared.
I got nothing to apologize for.

54
00:02:50,055 --> 00:02:52,024
I could have been
a lot harder on him.

55
00:02:52,091 --> 00:02:53,025
He got off easy.

56
00:02:53,092 --> 00:02:54,493
And often.

57
00:02:56,163 --> 00:02:58,197
Thank you.
I'll be here all week.

58
00:02:58,265 --> 00:03:01,200
- You gotta tell me about him.
I don't know anything about him.

59
00:03:01,268 --> 00:03:03,335
- Okay, he's 23,
he's kinda my boyfriend.

60
00:03:03,403 --> 00:03:04,804
He's gay.

61
00:03:04,871 --> 00:03:06,772
Last year we got drunk and
he took me to the ice capades,

62
00:03:06,840 --> 00:03:08,507
And I didn't get him anything.

63
00:03:08,575 --> 00:03:09,942
And he's gay?
- Yeah.

64
00:03:10,010 --> 00:03:11,310
<i>- Brokeback mountain dvd.</i>

65
00:03:11,379 --> 00:03:12,346
- No.
- Fellas love that.

66
00:03:12,413 --> 00:03:13,447
No.

67
00:03:13,515 --> 00:03:14,781
- Does he already have,
like, chaps?

68
00:03:14,849 --> 00:03:16,950
Like, assless chaps?
- You know what, forget it.

69
00:03:17,018 --> 00:03:18,953
- Okay, uh,
why are you asking me?

70
00:03:19,021 --> 00:03:22,224
- Because, I don't know,
there's no one else to ask.

71
00:03:22,291 --> 00:03:23,759
And at least you're
kind of young.

72
00:03:23,827 --> 00:03:24,961
- All right,
I'll tell you what.

73
00:03:25,029 --> 00:03:26,763
I'm gonna think about it.

74
00:03:26,831 --> 00:03:28,398
Through the course of all day
I'm gonna mull it over.

75
00:03:28,466 --> 00:03:30,267
It's all I'm gonna think about.
But you're in good hands.

76
00:03:30,335 --> 00:03:31,903
I'm gonna come up with something
really, really good.

77
00:03:31,970 --> 00:03:32,904
- Really?
- Yeah.

78
00:03:32,972 --> 00:03:33,972
- All right.
- You got that?

79
00:03:34,039 --> 00:03:37,442
Yeah. Thanks.

80
00:03:37,510 --> 00:03:39,578
- Councilman dexhart,
I'm leslie knope.

81
00:03:39,646 --> 00:03:42,749
- Thank you for meeting me.
- It's my pleasure.

82
00:03:42,817 --> 00:03:44,684
I am assuming this is about
my performance

83
00:03:44,752 --> 00:03:47,054
In the government follies
last night.

84
00:03:47,121 --> 00:03:49,623
I'd just like to say that
I have nothing to apologize for.

85
00:03:49,691 --> 00:03:52,326
Except for perhaps being
too incisive.

86
00:03:52,394 --> 00:03:54,695
- I don't want you
to apologize.

87
00:03:54,763 --> 00:03:56,630
I want to know who told you.

88
00:03:56,698 --> 00:03:57,965
- Who told me?
- Mm-hmm.

89
00:03:58,033 --> 00:04:01,735
- Nobody.
Nobody told me nuttin'!

90
00:04:01,803 --> 00:04:03,471
What are we talking about?

91
00:04:03,538 --> 00:04:04,738
Well, based on that skit,

92
00:04:04,806 --> 00:04:06,240
I know you've heard about
the new scandal

93
00:04:06,308 --> 00:04:07,341
That's about to break.

94
00:04:07,409 --> 00:04:10,077
Who told you?

95
00:04:10,145 --> 00:04:11,312
Was it the babysitter?

96
00:04:11,379 --> 00:04:13,014
Was it the nurse
who delivered our love child?

97
00:04:13,081 --> 00:04:14,349
- What?
- Oh, stop playing dumb.

98
00:04:14,416 --> 00:04:16,251
You know damn well
what happened.

99
00:04:16,319 --> 00:04:18,086
I got the babysitter pregnant.

100
00:04:18,154 --> 00:04:19,688
Then, when she was
in the delivery room,

101
00:04:19,756 --> 00:04:21,523
I had sex with not one

102
00:04:21,591 --> 00:04:24,192
But four nurses
in a supply closet.

103
00:04:24,260 --> 00:04:26,061
As well as a woman
whose husband

104
00:04:26,129 --> 00:04:28,363
Was getting
a liver transplant.

105
00:04:28,431 --> 00:04:29,364
Hmm.

106
00:04:29,432 --> 00:04:32,300
Now, which one of them
told you?

107
00:04:32,368 --> 00:04:33,601
Was it the liver lady?

108
00:04:33,669 --> 00:04:36,971
- Well, I--no one--
I haven't--

109
00:04:37,039 --> 00:04:40,175
I haven't heard any of this ever
in my whole life.

110
00:04:40,243 --> 00:04:41,343
Oh.

111
00:04:41,411 --> 00:04:42,878
- Believe me, I would have
remembered this.

112
00:04:42,946 --> 00:04:45,547
- Okay. Well,
in that case,

113
00:04:45,615 --> 00:04:48,917
Everything I just told you
was just a funny prank.

114
00:04:54,723 --> 00:04:57,291
- Oh, hi.
How was your meeting?

115
00:04:57,359 --> 00:04:59,360
It was fine.

116
00:04:59,428 --> 00:05:00,795
Pretty straightforward.

117
00:05:00,862 --> 00:05:02,796
Short meeting.
You know, he's a busy guy.

118
00:05:02,864 --> 00:05:04,798
- Yes, very busy.
Look at this article.

119
00:05:04,865 --> 00:05:07,066
No!

120
00:05:07,134 --> 00:05:12,204
What the--

121
00:05:12,272 --> 00:05:15,574
- The story of this story is
that it won't stop developing.

122
00:05:15,642 --> 00:05:17,911
The mystery woman who met with
councilman dexhart last night

123
00:05:17,978 --> 00:05:21,847
Appears to be a government
employee named leslie norp.

124
00:05:21,915 --> 00:05:23,649
- Oh, my god.
Oh, god.

125
00:05:23,717 --> 00:05:26,152
<i>- According to unconfirmed
reports in the pawnee sun,</i>

126
00:05:26,220 --> 00:05:29,121
The two bent an elbow
at this local watering hole.

127
00:05:29,189 --> 00:05:30,256
And although they left
separately,

128
00:05:30,324 --> 00:05:33,393
No one knows where
they woke up together.

129
00:05:33,461 --> 00:05:36,797
Perd hapley,
channel 4 news.

130
00:05:40,175 --> 00:05:42,742
- All I did was write and
perform in one amazing skit.

131
00:05:42,810 --> 00:05:44,411
<i>- Leslie,
it's the pawnee sun.</i>

132
00:05:44,479 --> 00:05:45,412
It's a tabloid.

133
00:05:45,480 --> 00:05:46,847
Nobody else has written
about it.

134
00:05:46,914 --> 00:05:48,115
- Yeah, no one believes
that garbage.

135
00:05:48,182 --> 00:05:49,850
Nobody even reads
that thing.

136
00:05:49,917 --> 00:05:52,585
- Leslie, what?
Nice work, girl.

137
00:05:52,653 --> 00:05:54,153
Oh, god.

138
00:05:54,221 --> 00:05:56,521
How do I fight back?
Give me some options.

139
00:05:56,589 --> 00:05:59,725
- Do you want me
to seduce perd hapley?

140
00:05:59,792 --> 00:06:01,060
How would that help?

141
00:06:01,127 --> 00:06:02,794
I don't know.

142
00:06:02,862 --> 00:06:04,930
I just want to see
if I can do it.

143
00:06:04,998 --> 00:06:06,498
- April, I appreciate that,
but I don't think it's something

144
00:06:06,566 --> 00:06:08,300
Worth losing
your virginity over.

145
00:06:08,367 --> 00:06:11,502
- Leslie, I don't know if
this is important or not,

146
00:06:11,870 --> 00:06:14,471
But I have heard a lot of very,
very interesting chatter

147
00:06:14,539 --> 00:06:15,639
Around the shoeshine stand
today.

148
00:06:15,706 --> 00:06:16,973
Oh, my god, really?

149
00:06:17,040 --> 00:06:18,207
What have you heard?
- A lot of things.

150
00:06:18,275 --> 00:06:20,609
Uh, like,
"can you believe it?"

151
00:06:20,676 --> 00:06:22,777
And "the parks lady
boned dexhart."

152
00:06:22,845 --> 00:06:25,712
And "of course she did.
She's totally good to go."

153
00:06:25,780 --> 00:06:28,047
And one guy was like,
"I wouldn't hit that."

154
00:06:28,115 --> 00:06:29,682
And the other guy was like,
"me neither."

155
00:06:29,750 --> 00:06:31,384
Then this third guy
was like, "I would."

156
00:06:31,451 --> 00:06:34,688
So I don't know
if that's helpful at all.

157
00:06:34,755 --> 00:06:37,757
- Well, keep your ear
to the ground.

158
00:06:37,825 --> 00:06:39,459
Okay. I'll just--

159
00:06:39,527 --> 00:06:41,995
- I'm not gonna let
these people intimidate me.

160
00:06:42,062 --> 00:06:44,197
I'm gonna do my job.

161
00:06:44,264 --> 00:06:45,998
I would be lying if I said
that I never thought

162
00:06:46,066 --> 00:06:48,134
I would be involved
in an incident like this.

163
00:06:48,201 --> 00:06:50,870
Except I always assumed that
I would be the politician,

164
00:06:50,938 --> 00:06:53,606
And the man would be accused
of sleeping with me.

165
00:06:53,674 --> 00:06:56,409
And that man would be
the vice president.

166
00:06:56,477 --> 00:06:58,812
And I would be the president.

167
00:06:58,879 --> 00:07:01,281
Okay, all right, well,
you look good.

168
00:07:01,348 --> 00:07:02,615
You need to put
some tights on, though.

169
00:07:02,683 --> 00:07:04,250
And I want you to make sure
you keep your neckline

170
00:07:04,318 --> 00:07:05,485
Nice and high.

171
00:07:05,552 --> 00:07:07,753
<i>- Leslie knope?
Alexa softcastle, pawnee sun.</i>

172
00:07:07,821 --> 00:07:09,421
Can I ask you a few questions?

173
00:07:09,489 --> 00:07:10,522
Don't say anything.

174
00:07:10,590 --> 00:07:12,458
- I'm not afraid
to say anything, okay?

175
00:07:12,526 --> 00:07:13,926
I have nothing to hide.

176
00:07:13,994 --> 00:07:16,595
I've never even met councilman
dexhart before yesterday,

177
00:07:16,663 --> 00:07:18,230
And that's all I want to say
about the matter.

178
00:07:18,298 --> 00:07:19,765
- Some people are saying
this isn't the first time

179
00:07:19,832 --> 00:07:21,766
That you've had sex
with a married councilman.

180
00:07:21,834 --> 00:07:23,634
Who's saying that?

181
00:07:23,702 --> 00:07:25,336
Some people.

182
00:07:25,404 --> 00:07:28,139
Okay, this interview is over.

183
00:07:28,207 --> 00:07:31,142
- Go home, lay low,
the truth will come out.

184
00:07:31,210 --> 00:07:32,844
- I can't.
I have too much work to do.

185
00:07:32,911 --> 00:07:35,680
This department is not gonna
deputy-direct itself.

186
00:07:35,747 --> 00:07:37,148
I think we can manage.

187
00:07:37,216 --> 00:07:41,185
Just give me your schedule
and we'll cover for you.

188
00:07:41,253 --> 00:07:45,757
Okay, I've made copies
of leslie's daily work schedule.

189
00:07:45,825 --> 00:07:47,025
So we'll just split
into teams,

190
00:07:47,092 --> 00:07:51,763
And each team will take...

191
00:07:51,831 --> 00:07:55,767
Damn, ten items...
And we'll knock this out.

192
00:07:55,835 --> 00:07:57,369
- I swear to you,
I never even met the guy

193
00:07:57,436 --> 00:07:58,603
Before last night.

194
00:07:58,671 --> 00:08:00,705
- It's okay.
I believe you.

195
00:08:00,773 --> 00:08:02,040
- You do?
- Of course.

196
00:08:02,108 --> 00:08:05,210
I think I know you
pretty well.

197
00:08:05,278 --> 00:08:06,378
That doesn't sound
like you.

198
00:08:06,445 --> 00:08:07,812
Thank you.

199
00:08:07,880 --> 00:08:09,013
If I ever see that guy,

200
00:08:09,081 --> 00:08:10,281
I'm gonna punch him
right in the face.

201
00:08:10,348 --> 00:08:13,484
- Yeah, that's sweet.
Use your night stick.

202
00:08:13,551 --> 00:08:15,019
Which meeting is this?

203
00:08:15,087 --> 00:08:18,789
- The organization
of local auditing systems.

204
00:08:18,857 --> 00:08:20,958
Kill me.

205
00:08:21,026 --> 00:08:22,660
- All right,
first order of business...

206
00:08:22,727 --> 00:08:25,663
Uh, leslie--
where's leslie?

207
00:08:25,730 --> 00:08:28,932
- Um, I gave her the day off,
paul.

208
00:08:29,000 --> 00:08:31,234
I thought that was best.
- Oh, yeah. That's good.

209
00:08:31,302 --> 00:08:33,870
So you'll be making
the presentation then, ron.

210
00:08:33,938 --> 00:08:37,806
Yep. Yes.

211
00:08:37,874 --> 00:08:40,576
Hey, so, uh...

212
00:08:40,643 --> 00:08:42,744
That kind of brings us to what
I wanted to talk to you about.

213
00:08:42,812 --> 00:08:43,812
I, uh--

214
00:08:43,879 --> 00:08:45,713
You know, my--

215
00:08:45,781 --> 00:08:48,382
Army reserve unit---

216
00:08:48,450 --> 00:08:50,584
We got called up
to active duty.

217
00:08:50,652 --> 00:08:52,520
I ship out in four days.

218
00:08:52,587 --> 00:08:55,423
- Oh, my god.
Where are you going?

219
00:08:55,491 --> 00:08:56,858
San diego.

220
00:08:56,926 --> 00:08:58,093
Oh, my god!

221
00:08:58,161 --> 00:08:59,795
It's not dangerous.

222
00:08:59,862 --> 00:09:01,163
It's mostly maintenance.

223
00:09:01,230 --> 00:09:03,598
But I'm going for,
like, a year.

224
00:09:03,666 --> 00:09:05,467
Maybe 18 months.

225
00:09:05,535 --> 00:09:08,805
And I was kind of hoping
that maybe

226
00:09:08,872 --> 00:09:10,607
You'd want to come with--
with me.

227
00:09:10,674 --> 00:09:14,878
Like on a permanent-type
basis.

228
00:09:14,946 --> 00:09:16,513
I don't know, it's--

229
00:09:16,581 --> 00:09:18,783
It's kind of a weird question.

230
00:09:18,851 --> 00:09:22,354
I mean, I joined
to put myself through college,

231
00:09:22,422 --> 00:09:26,326
And, uh, it's, you know,
I'm just a desk jockey,

232
00:09:26,393 --> 00:09:28,561
But it's--
it's rewarding.

233
00:09:28,629 --> 00:09:33,800
So yeah, I guess I'm in love
with the army.

234
00:09:33,868 --> 00:09:35,368
Oh, leslie.
Well, yeah.

235
00:09:35,436 --> 00:09:38,105
Yeah, that makes
a lot more sense.

236
00:09:38,172 --> 00:09:41,074
Yeah, I'm definitely in love
with leslie.

237
00:09:41,142 --> 00:09:42,809
It's--it's affirmative.

238
00:09:42,877 --> 00:09:45,979
I--I know this seems
kind of fast,

239
00:09:46,047 --> 00:09:47,247
But I feel like we--

240
00:09:47,315 --> 00:09:48,648
I feel like we got
something here.

241
00:09:48,716 --> 00:09:50,817
I do too.

242
00:09:50,884 --> 00:09:52,652
- Yeah?
- Yeah.

243
00:09:52,719 --> 00:09:54,220
- Well, you don't have
to answer right away.

244
00:09:54,288 --> 00:09:57,590
But you kinda do.
I mean, I need to know...Soon.

245
00:10:00,693 --> 00:10:01,860
- Wow.
- Yeah.

246
00:10:01,928 --> 00:10:03,128
Are you gonna go?

247
00:10:03,196 --> 00:10:05,897
- This whole dexhart thing
is not making me feel

248
00:10:05,965 --> 00:10:07,499
Very attached
to pawnee right now.

249
00:10:07,567 --> 00:10:11,002
- San diego, jeez.
- Yeah.

250
00:10:11,069 --> 00:10:12,337
You could learn how to surf.

251
00:10:12,404 --> 00:10:13,704
I know how to surf.

252
00:10:13,772 --> 00:10:15,273
I took lessons
when I was a kid.

253
00:10:15,340 --> 00:10:18,811
I'm actually pretty good.
- I bet you are.

254
00:10:18,878 --> 00:10:21,580
I would miss leslie
like crazy.

255
00:10:21,648 --> 00:10:25,351
Aww...

256
00:10:25,419 --> 00:10:27,821
- All right, sorted out that
payroll issue--this is done.

257
00:10:27,889 --> 00:10:29,823
- "bring a case of beer
to sanitation." why?

258
00:10:29,891 --> 00:10:31,992
- Let's skip that one.
They can buy their own beer.

259
00:10:32,059 --> 00:10:33,726
I'm getting hungry.
Let's get something to eat.

260
00:10:33,794 --> 00:10:35,027
Hey, have you figured out

261
00:10:35,095 --> 00:10:36,228
What you're getting ann
for christmas?

262
00:10:36,296 --> 00:10:38,731
- Oh, I got her a pretty great
computer bag.

263
00:10:38,798 --> 00:10:40,833
- Yeah?
That's a terrible gift.

264
00:10:40,901 --> 00:10:42,768
- No, no, tom,
she needs one.

265
00:10:42,836 --> 00:10:44,437
She mentioned it to me
two months ago,

266
00:10:44,504 --> 00:10:45,871
And I wrote it down.

267
00:10:45,939 --> 00:10:48,207
That's what's called being
an amazing boyfriend.

268
00:10:48,275 --> 00:10:51,476
- Have you seen ann?
You know how hot she is?

269
00:10:51,544 --> 00:10:54,312
Men give women of that caliber
speed boats,

270
00:10:54,380 --> 00:10:57,580
Private jets...
Not computer bags.

271
00:10:57,648 --> 00:10:58,953
- Okay, what do you think
I should get her?

272
00:10:59,021 --> 00:11:00,555
- Diamonds.
Can't go wrong with diamonds.

273
00:11:01,023 --> 00:11:01,757
<i>- Diamonds?</i>

274
00:11:01,824 --> 00:11:03,525
- There isn't a woman alive
who doesn't love diamonds.

275
00:11:03,592 --> 00:11:04,954
Even the super-left-wing chicks

276
00:11:05,022 --> 00:11:06,188
<i>That saw blood diamond
and cried...</i>

277
00:11:06,656 --> 00:11:08,223
When they get a diamond,
they're, like,

278
00:11:08,291 --> 00:11:10,159
"yeah, bitch, gimme more
of them blood diamonds!

279
00:11:11,227 --> 00:11:12,028
Make 'em extra bloody."

280
00:11:12,895 --> 00:11:14,397
Trust me.

281
00:11:15,464 --> 00:11:17,532
- We are here looking
at a videotape.

282
00:11:17,600 --> 00:11:20,135
This is back in April, 2005.

283
00:11:20,203 --> 00:11:21,703
We see here we're looking
at councilman dexhart.

284
00:11:21,771 --> 00:11:22,904
Leslie, you should see this.

285
00:11:22,972 --> 00:11:25,240
- And he's now
about to shake hands

286
00:11:25,308 --> 00:11:28,210
With leslie knope,
the alleged sex toy.

287
00:11:28,277 --> 00:11:30,812
Oh, for cripes sake.

288
00:11:30,880 --> 00:11:33,315
- Look at the way
she's smiling at him.

289
00:11:33,382 --> 00:11:34,883
And then almost unconsciously

290
00:11:34,951 --> 00:11:36,484
Touching her hands
to her hips.

291
00:11:36,552 --> 00:11:38,854
See that? Right there.

292
00:11:38,921 --> 00:11:41,022
It's like she's
sending him a message

293
00:11:41,090 --> 00:11:42,090
That she's ready
for childbearing.

294
00:11:42,158 --> 00:11:44,130
- Wow.
- In a 24-hour news cycle,

295
00:11:44,198 --> 00:11:47,233
The tiniest story gets dissected
over and over again.

296
00:11:47,301 --> 00:11:51,037
In 2004, a kid from pawnee
went to the olympics,

297
00:11:51,104 --> 00:11:54,273
And it was reported on
for over a year.

298
00:11:54,341 --> 00:11:56,042
He wasn't even competing
or anything.

299
00:11:56,109 --> 00:11:59,312
He just was going, literally,
to watch the olympics.

300
00:12:06,353 --> 00:12:08,688
- It was way too easy
to get this guy here.

301
00:12:08,755 --> 00:12:11,624
- Councilman dexhart,
this has gone far enough.

302
00:12:11,692 --> 00:12:13,159
I want you to hold
a press conference

303
00:12:13,227 --> 00:12:14,160
And clear my name.

304
00:12:14,228 --> 00:12:16,696
- Mmm, no, thanks.
- Why not?

305
00:12:16,763 --> 00:12:17,897
What's in it for you?

306
00:12:17,965 --> 00:12:19,599
- Well, to be honest,
being linked to leslie

307
00:12:19,666 --> 00:12:21,267
Is a lot less damning
than the real story.

308
00:12:21,335 --> 00:12:24,370
You're like a glass
of whole milk.

309
00:12:24,438 --> 00:12:27,006
What are you, lutheran?
I love lutherans.

310
00:12:27,074 --> 00:12:29,475
- Councilman, with all due
respect, get a grip.

311
00:12:29,543 --> 00:12:30,843
If you don't hold
a press conference,

312
00:12:30,911 --> 00:12:32,912
I'm gonna come forward
and tell the whole story.

313
00:12:32,980 --> 00:12:34,280
It won't do you any good.

314
00:12:34,348 --> 00:12:37,416
Take it from me, denying
only makes things worse.

315
00:12:37,484 --> 00:12:38,784
Listen, I gotta go.

316
00:12:38,852 --> 00:12:40,820
I'm expected at two different
maternity wards.

317
00:12:40,888 --> 00:12:44,690
But before I go,
I'd be remiss if I didn't ask--

318
00:12:44,758 --> 00:12:46,525
Should we?

319
00:12:46,593 --> 00:12:48,427
Oh, my god!

320
00:12:48,495 --> 00:12:49,428
- People already think
we did it.

321
00:12:49,496 --> 00:12:50,496
You've got nothing to lose.

322
00:12:50,564 --> 00:12:52,331
I'm very good.
- Get outta here!

323
00:12:52,399 --> 00:12:53,332
All right, fine.

324
00:12:53,400 --> 00:12:56,002
- Shoe shine.
Shoe shine.

325
00:12:56,069 --> 00:12:57,737
Oh, hey.
- Hey.

326
00:12:57,804 --> 00:12:59,505
- I've been thinking about
your gay boyfriend all day.

327
00:12:59,573 --> 00:13:01,240
I have got
some awesome ideas.

328
00:13:01,308 --> 00:13:01,994
Okay.

329
00:13:01,432 --> 00:13:05,634
First idea:
Spray tan gift certificates.

330
00:13:06,202 --> 00:13:09,338
Uh...No.

331
00:13:09,405 --> 00:13:12,007
- Trip to germany.
Germany is awesome.

332
00:13:12,075 --> 00:13:13,742
And expensive.

333
00:13:13,810 --> 00:13:16,311
- Good call.
I didn't think of that.

334
00:13:16,379 --> 00:13:17,773
- Okay, you know how
people say

335
00:13:17,841 --> 00:13:20,009
That you should give gifts that
you would want to get yourself?

336
00:13:20,377 --> 00:13:21,777
What would you want?

337
00:13:21,845 --> 00:13:24,113
- Easy. Indianapolis colts
reggie wayne jersey--

338
00:13:24,180 --> 00:13:26,015
Number 87, double-xl,
home blue,

339
00:13:26,082 --> 00:13:28,017
Signed by reggie wayne right
after he catches a touchdown

340
00:13:28,084 --> 00:13:29,551
To win the super bowl.

341
00:13:29,619 --> 00:13:33,222
- Okay, never mind.
- No?

342
00:13:33,289 --> 00:13:35,991
<i>Hip-hop abs dance
fitness dvd.</i>

343
00:13:36,059 --> 00:13:39,628
- We just received
these exclusive photos.

344
00:13:39,696 --> 00:13:41,296
We have pictures
of city councilman dexhart...

345
00:13:41,364 --> 00:13:44,566
- Oh, my god!
I cannot believe it!

346
00:13:44,634 --> 00:13:48,003
- The big issue now is
who is this mystery woman?

347
00:13:48,071 --> 00:13:51,440
She and knope are standing
very close to each other.

348
00:13:51,508 --> 00:13:53,275
And anytime you see
two women

349
00:13:53,343 --> 00:13:55,277
Standing very close
to each other,

350
00:13:55,345 --> 00:13:56,545
You immediately assume...
- No, no, don't say it.

351
00:13:56,613 --> 00:13:58,313
Please don't say it.
- Lesbian.

352
00:13:58,381 --> 00:13:59,948
- Oh, of course.
- What?

353
00:14:00,016 --> 00:14:03,385
- It is a veritable storm
of information

354
00:14:03,453 --> 00:14:05,254
Coming in to the studios
here today.

355
00:14:05,321 --> 00:14:07,322
- Hi, this is leslie knope,
and I would like joan

356
00:14:07,390 --> 00:14:08,457
To clear all of her guests
tomorrow

357
00:14:08,525 --> 00:14:09,825
Because I'm coming on the show.

358
00:14:09,893 --> 00:14:11,793
If you think that you can drag
me and my friend

359
00:14:11,861 --> 00:14:13,495
Through the mud,
then you've got--

360
00:14:13,563 --> 00:14:14,663
Yes, I'll hold.

361
00:14:16,199 --> 00:14:17,199
- It's unbelievable.
- Hey.

362
00:14:17,267 --> 00:14:18,801
What happened?

363
00:14:18,869 --> 00:14:20,603
- Got a call from some panicky
morning joggers.

364
00:14:21,070 --> 00:14:23,705
Apparently sanitation didn't
empty this dumpster,

365
00:14:23,773 --> 00:14:25,908
To the raccoons' delight.

366
00:14:25,975 --> 00:14:28,043
- I thought raccoons were
supposed to be nocturnal.

367
00:14:28,111 --> 00:14:29,344
- Not in this town,
sweetheart.

368
00:14:29,412 --> 00:14:31,413
In this town,
they're 24/7.

369
00:14:31,481 --> 00:14:34,049
We can't have raccoons
for the christmas thing.

370
00:14:34,117 --> 00:14:36,718
They'll hunt the kids
for sport.

371
00:14:36,786 --> 00:14:38,387
Fess up, guys.
Who dropped the ball?

372
00:14:38,454 --> 00:14:41,423
- Bring a case of beer
to sanitation.

373
00:14:41,491 --> 00:14:43,091
Bring a case of--

374
00:14:43,159 --> 00:14:44,626
Yeah, this one's
our fault, ron.

375
00:14:44,694 --> 00:14:46,361
And we will take care
of this for you.

376
00:14:46,429 --> 00:14:49,398
- Okay good, because I
have to run a public forum,

377
00:14:49,465 --> 00:14:51,099
Supervise the maintenance crews,

378
00:14:51,167 --> 00:14:53,969
And teach crafts
at the senior center.

379
00:14:54,037 --> 00:14:55,537
Simuaneously.

380
00:14:55,605 --> 00:14:57,706
- Joan, I just wanted
to say thank you

381
00:14:57,774 --> 00:14:59,741
For letting me be
on your show.

382
00:14:59,809 --> 00:15:01,910
I'm really happy that I have
the chance to clear my name.

383
00:15:01,978 --> 00:15:04,146
- Sure. I see you brought
your girlfriend for support.

384
00:15:04,214 --> 00:15:06,915
That's nice.
- What?

385
00:15:06,983 --> 00:15:08,850
Oh, no, she's not--
- and in five, joan...

386
00:15:08,918 --> 00:15:09,985
- She's a--we're--
- four, three...

387
00:15:10,053 --> 00:15:11,420
You've got it wrong.

388
00:15:11,487 --> 00:15:16,147
- Sex. Drugs, possibly.
Rock and roll?

389
00:15:16,215 --> 00:15:18,449
<i>We'll find out
on pawnee today's</i>

390
00:15:18,517 --> 00:15:20,752
Exclusive interview
with the woman

391
00:15:20,819 --> 00:15:24,355
At the center of the dexhart
sex scandal, leslie knope.

392
00:15:24,423 --> 00:15:28,159
Leslie, my first question
has to be

393
00:15:28,227 --> 00:15:30,161
When did the affair start?

394
00:15:30,229 --> 00:15:32,664
- Joan, I spoke with
councilman dexhart

395
00:15:32,731 --> 00:15:34,132
For the first time
that night.

396
00:15:34,199 --> 00:15:36,668
We met for about
15 minutes,

397
00:15:36,735 --> 00:15:39,671
And then I went home alone,
and that's the whole story.

398
00:15:39,738 --> 00:15:41,439
- Well, leslie,
we all saw the tape

399
00:15:41,507 --> 00:15:43,107
From four years ago,

400
00:15:43,175 --> 00:15:46,044
And you were flashing
some serious "do me" eyes.

401
00:15:46,111 --> 00:15:47,278
That's just my opinion.

402
00:15:47,346 --> 00:15:49,714
- I don't understand why
I'm on trial here.

403
00:15:49,782 --> 00:15:52,417
You should be grilling
councilman dexhart.

404
00:15:52,484 --> 00:15:55,086
- Oh, you know what,
that's a really good idea.

405
00:15:55,154 --> 00:15:57,789
Uh, let's bring him out.
Councilman, come on out.

406
00:15:57,856 --> 00:15:59,691
Hi, sweetie.

407
00:15:59,758 --> 00:16:02,627
- Why didn't you tell me
he was gonna be here?

408
00:16:02,695 --> 00:16:04,462
- I thought it'd be
more exciting.

409
00:16:04,530 --> 00:16:06,230
You look great.

410
00:16:09,134 --> 00:16:10,935
It's like I'm invisible.

411
00:16:11,003 --> 00:16:12,637
Okay, my name is ron.

412
00:16:12,705 --> 00:16:15,073
You don't need to know
my last name.

413
00:16:15,140 --> 00:16:16,541
Whoever wants to talk,
go ahead,

414
00:16:16,609 --> 00:16:18,910
And we'll be out of here
in a tight 15.

415
00:16:18,978 --> 00:16:21,245
- I found a sandwich
in one of your parks,

416
00:16:21,313 --> 00:16:26,084
And I want to know why
it didn't have mayonnaise.

417
00:16:26,151 --> 00:16:27,752
What's so funny?
- Oh.

418
00:16:27,820 --> 00:16:28,987
- Yeah, I don't think kids
should be allowed

419
00:16:29,054 --> 00:16:30,321
On the playground equipment.

420
00:16:30,389 --> 00:16:31,812
Okay, we've been over this.

421
00:16:32,580 --> 00:16:33,347
If you're worried about
swine flu,

422
00:16:34,215 --> 00:16:35,882
Use hand sanitizer--

423
00:16:35,950 --> 00:16:36,950
- I'm not worried about
swine flu.

424
00:16:37,017 --> 00:16:38,084
I already had the swine flu.

425
00:16:38,152 --> 00:16:39,152
I'm worried about
the turtle flu!

426
00:16:39,220 --> 00:16:41,487
- The turtle flu...
- Turtle flu.

427
00:16:41,555 --> 00:16:44,090
Turtle flu.

428
00:16:44,158 --> 00:16:46,960
<i>- Joan, this whole
pawnee sun story</i>

429
00:16:47,027 --> 00:16:49,095
Is "gotcha journalism"
at its worst.

430
00:16:49,163 --> 00:16:51,164
Honey, let's just keep
our private life

431
00:16:51,232 --> 00:16:52,498
In the bedroom
where it belongs.

432
00:16:52,566 --> 00:16:54,534
- We do not have
a private life.

433
00:16:54,602 --> 00:16:56,436
Stop saying that we have
a private life.

434
00:16:56,503 --> 00:16:57,904
The fact is we never
slept together.

435
00:16:57,972 --> 00:17:00,373
And if we did,
you would have proof.

436
00:17:00,441 --> 00:17:02,141
And we would have seen it
by now.

437
00:17:02,209 --> 00:17:03,409
Councilman?

438
00:17:03,477 --> 00:17:05,745
I can prove it.

439
00:17:05,813 --> 00:17:08,982
She has a mole
on her right buttock.

440
00:17:09,049 --> 00:17:11,751
- What?
That is a total lie.

441
00:17:11,819 --> 00:17:14,254
You've never seen my butt.
What are you talking about?

442
00:17:14,321 --> 00:17:16,489
He has never seen my butt.

443
00:17:16,557 --> 00:17:17,991
-

444
00:17:18,058 --> 00:17:20,960
- Well, I guess it's your word
against his.

445
00:17:21,028 --> 00:17:22,428
I mean...
- That's right.

446
00:17:23,497 --> 00:17:25,265
Fine.

447
00:17:25,332 --> 00:17:28,134
Here, joan, why don't you look
for yourself?

448
00:17:28,202 --> 00:17:30,536
Is this happening?

449
00:17:30,604 --> 00:17:32,939
Wait...There's no mole.

450
00:17:33,007 --> 00:17:34,774
There is no mole!

451
00:17:34,842 --> 00:17:36,843
This exclusive story--

452
00:17:36,911 --> 00:17:39,679
There is no mole
on miss knope!

453
00:17:39,747 --> 00:17:40,680
- I can't believe
it's come to this.

454
00:17:40,748 --> 00:17:41,981
This is utterly humiliating.

455
00:17:42,049 --> 00:17:43,449
- Well, councilman,
care to make a comment

456
00:17:43,517 --> 00:17:45,718
About no-mole-gate?

457
00:17:45,786 --> 00:17:47,253
Yes.

458
00:17:47,321 --> 00:17:48,888
I really didn't think
that miss knope would

459
00:17:48,956 --> 00:17:50,379
Pull down her pants on tv.

460
00:17:51,047 --> 00:17:53,282
But since she did,

461
00:17:53,349 --> 00:17:56,785
I will admit that the rumors
of our affair

462
00:17:56,853 --> 00:17:58,921
Are indeed false.

463
00:17:58,988 --> 00:18:00,322
- Thank you.
- However...

464
00:18:00,390 --> 00:18:02,357
I have no plans to resign.

465
00:18:02,425 --> 00:18:04,259
- Oh, great.
Okay, councilman,

466
00:18:04,327 --> 00:18:05,427
You know what,
you're a class act.

467
00:18:05,495 --> 00:18:06,862
Thank you.

468
00:18:06,930 --> 00:18:08,697
I thought you did great.

469
00:18:08,765 --> 00:18:09,998
When you get all feisty
like that,

470
00:18:10,066 --> 00:18:12,301
It gets my heart racing.

471
00:18:12,368 --> 00:18:14,536
Like I'm on a stairmaster.

472
00:18:14,604 --> 00:18:20,876
Except more like
in a sexual...Context.

473
00:18:20,944 --> 00:18:24,746
- I can't go to san diego
with you.

474
00:18:28,051 --> 00:18:31,653
My life is here.
My friends, my career.

475
00:18:31,721 --> 00:18:33,589
- I guess I just thought,
with everything that happened,

476
00:18:33,656 --> 00:18:35,991
You'd want a fresh start.

477
00:18:36,059 --> 00:18:39,361
- I know, I--
I really care about you, dave.

478
00:18:39,429 --> 00:18:40,863
I just--

479
00:18:40,930 --> 00:18:43,732
I love this town.

480
00:18:43,800 --> 00:18:45,334
I'm sorry.

481
00:18:47,136 --> 00:18:48,570
You want your ticket back?

482
00:18:48,638 --> 00:18:51,240
- No. You know what,
keep it.

483
00:18:51,307 --> 00:18:54,943
You may want to
come visit me, right?

484
00:18:55,011 --> 00:18:57,112
There's a $75 change fee,

485
00:18:57,180 --> 00:18:59,615
But I could pay you that,
or I could reimburse you.

486
00:18:59,682 --> 00:19:01,316
There's a lot of ways
we could handle that.

487
00:19:11,983 --> 00:19:14,986
Oh, ho, that's, uh...

488
00:19:15,954 --> 00:19:18,722
Hey. Sorry I'm late.

489
00:19:18,790 --> 00:19:19,823
Hey!

490
00:19:19,891 --> 00:19:22,426
- Oh, honey,
I'm so proud of you.

491
00:19:22,493 --> 00:19:25,563
That took guts,
mooning joan callamezzo.

492
00:19:25,581 --> 00:19:27,548
Aww, honey...
- Thanks, mom.

493
00:19:27,616 --> 00:19:29,651
Oh, my god, is something wrong
with the tree?

494
00:19:29,718 --> 00:19:31,352
No, nothing wrong.

495
00:19:31,420 --> 00:19:32,353
- But it's almost 9:00.
It should have been lighted up

496
00:19:32,421 --> 00:19:33,454
An hour ago.

497
00:19:33,522 --> 00:19:34,989
We waited for you.

498
00:19:40,863 --> 00:19:43,131
Okay, everybody,
countin' down!

499
00:19:43,198 --> 00:19:47,468
-
five, four, three, two,

500
00:19:47,536 --> 00:19:48,770
One.

501
00:19:52,574 --> 00:19:55,810
- Ã¢ÂTÂª joy to the world
the lord is come Ã¢ÂTÂª

502
00:19:55,878 --> 00:19:59,781
Ã¢ÂTÂª let earth receive
her king Ã¢ÂTÂª

503
00:19:59,848 --> 00:20:02,617
Ã¢ÂTÂª let every heart... Ã¢ÂTÂª

504
00:20:02,685 --> 00:20:05,153
- It's gotten a lot harder
to work in government.

505
00:20:05,220 --> 00:20:07,388
You think winston churchill
ever had to pull his pants down

506
00:20:07,456 --> 00:20:08,456
And show his butt?

507
00:20:08,524 --> 00:20:10,925
No.
But would he have?

508
00:20:10,993 --> 00:20:12,293
Yes.

509
00:20:12,361 --> 00:20:13,962
Now, could he have?

510
00:20:14,029 --> 00:20:15,363
Maybe not towards
the end of his life,

511
00:20:15,431 --> 00:20:18,533
But he would have.

512
00:20:18,600 --> 00:20:20,135
Because he loved his job.

513
00:20:30,979 --> 00:20:36,016
- Ho ho!
Reggie wayne!

514
00:20:36,084 --> 00:20:37,818
Dude!

515
00:20:37,886 --> 00:20:40,954
You remembered me complaining
about my computer bag.

516
00:20:41,022 --> 00:20:43,657
- Do you like it?
- Yeah, it's perfect.

517
00:20:43,725 --> 00:20:45,826
It's way better than the gift
that I got you.

518
00:20:45,894 --> 00:19:47,917
It's pacers tickets.

519
00:20:48,085 --> 00:20:49,853
- Oh...
- They're not even good seats.

520
00:20:49,920 --> 00:20:51,387
Achh!
- This is awesome.

521
00:20:51,455 --> 00:20:52,455
It's not awesome.

522
00:20:52,523 --> 00:20:56,593
- I actually got you
a second gift.

523
00:20:56,660 --> 00:20:58,762
You don't have to come
to this game with me.

524
00:21:00,264 --> 00:21:02,766
You don't have to.

525
00:21:02,833 --> 00:21:03,933
- That's the nicest gift...
- Is that good? Is that good?

526
00:21:04,001 --> 00:21:05,835
- Anybody's ever gotten me
in my life.

527
00:21:07,705 --> 00:21:09,272
Oh, thank god.

528
00:21:09,340 --> 00:21:11,207
There's a bunch of messages
waiting for you

529
00:21:11,275 --> 00:21:14,644
About a bunch of things
I don't understand.

530
00:21:15,048 --> 00:21:21,050
Sync by honeybunny
www.addic7ed.com

531
00:21:21,118 --> 00:21:23,920
-

532
00:21:30,394 --> 00:21:33,783
- Hi, this is leslie knope
in the parks department.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
