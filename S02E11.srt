1
00:00:01,835 --> 00:00:03,589
- What's up?
- I'm gonna need you to go up

2
00:00:03,714 --> 00:00:06,088
to the bureau of motor vehicles
on the fourth floor.

3
00:00:06,367 --> 00:00:08,799
We got to talk ourselves out
of this late registration fee

4
00:00:08,967 --> 00:00:10,176
for the parks van.

5
00:00:10,454 --> 00:00:12,388
Come on. I don't want to go
to the fourth floor.

6
00:00:12,513 --> 00:00:14,714
That is the creepiest place
on earth.

7
00:00:14,839 --> 00:00:16,658
The fourth floor is awful...

8
00:00:17,194 --> 00:00:19,376
The DMV, divorce filings,

9
00:00:19,501 --> 00:00:20,901
probation offices.

10
00:00:22,436 --> 00:00:25,313
They put a popcorn machine up there
just to brighten things up,

11
00:00:25,489 --> 00:00:26,995
but they used the wrong kind of oil

12
00:00:27,120 --> 00:00:29,487
and a bunch of people
had to get their throats replaced.

13
00:00:29,988 --> 00:00:31,856
They'll only talk to you or me,

14
00:00:31,981 --> 00:00:33,231
and I can't go.

15
00:00:33,481 --> 00:00:34,700
Because I don't want to.

16
00:00:55,761 --> 00:00:57,461
Hey, boo, you're pretty.

17
00:00:58,037 --> 00:00:59,287
Thank you, sir.

18
00:00:59,789 --> 00:01:01,123
Are you on probation?

19
00:01:01,248 --> 00:01:03,556
I got clean urine.
You need female, I got female.

20
00:01:03,681 --> 00:01:06,456
- I'm good. Thank you.
- Hey, you clean? I buy too.

21
00:01:26,710 --> 00:01:27,909
Tom and Wendy?

22
00:01:28,657 --> 00:01:29,657
Popcorn?

23
00:01:30,424 --> 00:01:32,049
- Feygnasse Team -

24
00:01:32,217 --> 00:01:33,909
.:: La Fabrique ::.

25
00:01:34,849 --> 00:01:36,923
Episode 211
<i>The Fourth Floor</i>

26
00:01:37,973 --> 00:01:38,973
mpm

27
00:01:39,515 --> 00:01:40,701
lestat78

28
00:01:52,994 --> 00:01:54,768
So, Tom...

29
00:01:54,893 --> 00:01:57,657
how's everything in your life,
generally?

30
00:01:58,035 --> 00:01:58,968
Amazing.

31
00:01:59,093 --> 00:02:01,564
Took a risk,
bought some shoes online.

32
00:02:02,837 --> 00:02:05,137
Paid off handsomely, as you can see.

33
00:02:07,967 --> 00:02:11,065
And how are your institutions

34
00:02:11,593 --> 00:02:12,882
that you're a part of?

35
00:02:13,695 --> 00:02:16,692
- You heard about my divorce?
- I saw you on the fourth floor.

36
00:02:16,817 --> 00:02:18,763
I'm so, so sorry.

37
00:02:18,931 --> 00:02:20,431
Honestly, it's fine.

38
00:02:21,123 --> 00:02:24,310
Lasted longer than Avril Lavigne
and the guy from Sum 41. Am I right?

39
00:02:25,062 --> 00:02:26,500
I don't know what those are.

40
00:02:26,625 --> 00:02:27,500
Look...

41
00:02:28,649 --> 00:02:29,731
stop working.

42
00:02:30,753 --> 00:02:33,474
- Would have been nice to have saved it.
- You should have auto-saved that.

43
00:02:33,599 --> 00:02:35,418
That kind of feels like your fault.

44
00:02:36,105 --> 00:02:38,717
Look, I've heard your voice
when she calls you on the phone.

45
00:02:38,842 --> 00:02:41,463
I've seen you look at her ass
when she leaves the room.

46
00:02:41,588 --> 00:02:42,703
You love her.

47
00:02:43,024 --> 00:02:45,094
I really appreciate
what you're trying to do,

48
00:02:45,219 --> 00:02:46,369
but I'm fine.

49
00:02:46,517 --> 00:02:48,867
Look. Look at my face.
Are you watching?

50
00:02:50,993 --> 00:02:52,296
I did a little research,

51
00:02:52,510 --> 00:02:55,290
and divorce is the number two

52
00:02:55,415 --> 00:02:57,760
most stressful event
in a person's life.

53
00:02:58,124 --> 00:03:00,424
Of course marriage is number seven.

54
00:03:00,870 --> 00:03:02,930
So watch out, everyone.

55
00:03:03,928 --> 00:03:05,078
It's all bad.

56
00:03:07,922 --> 00:03:10,272
We have to step up.
For Tom's sake.

57
00:03:10,479 --> 00:03:12,038
I think Tom seems fine.

58
00:03:12,163 --> 00:03:13,848
Well, that is the problem.

59
00:03:13,973 --> 00:03:16,409
Tom always seems like
Mr. Slickster cool guy,

60
00:03:16,534 --> 00:03:18,031
but he's hiding his emotions

61
00:03:18,156 --> 00:03:20,407
underneath a very thick layer
of axe body spray.

62
00:03:21,103 --> 00:03:22,273
We have to help him.

63
00:03:22,398 --> 00:03:24,954
- We are his safe bubble.
- She's kind of right.

64
00:03:25,256 --> 00:03:26,891
None of his family lives here,

65
00:03:27,016 --> 00:03:29,208
and all of his friends
are frat guys and morons.

66
00:03:29,593 --> 00:03:31,502
This seems like
none of our business.

67
00:03:31,763 --> 00:03:32,920
Be supportive, okay?

68
00:03:33,130 --> 00:03:34,423
Don't be all like,

69
00:03:35,447 --> 00:03:36,749
"I don't want to.

70
00:03:36,874 --> 00:03:37,967
"I am a guy,

71
00:03:38,135 --> 00:03:39,260
"and I like fire

72
00:03:39,495 --> 00:03:42,054
"and playing hockey and eating meat.

73
00:03:42,179 --> 00:03:44,533
"No, no, says I."

74
00:03:45,839 --> 00:03:48,269
- That was a really good Ron.
- Thank you.

75
00:03:48,481 --> 00:03:50,791
- I don't know how to explain...
- Hey, Mark.

76
00:03:50,916 --> 00:03:53,660
The shoeshine stand still doesn't have
that syphilis medication

77
00:03:53,785 --> 00:03:55,385
you were asking about.

78
00:03:57,960 --> 00:03:59,113
What a coincidence.

79
00:03:59,578 --> 00:04:01,574
Are you in this office

80
00:04:01,742 --> 00:04:03,519
purely for business reasons?

81
00:04:03,644 --> 00:04:05,094
Pleasure, actually.

82
00:04:05,267 --> 00:04:07,367
My boyfriend, Mark, works here.

83
00:04:07,492 --> 00:04:09,540
- He's sitting right next to me.
- Hey, Andy.

84
00:04:10,008 --> 00:04:12,771
What a surprise running into you
all day every day

85
00:04:12,896 --> 00:04:14,420
every single place that we are.

86
00:04:14,878 --> 00:04:15,878
Likewise.

87
00:04:16,373 --> 00:04:18,674
Well, I guess
I'll see you guys around.

88
00:04:18,842 --> 00:04:19,919
See ya.

89
00:04:20,684 --> 00:04:22,342
Also, Mark, again,

90
00:04:22,467 --> 00:04:25,556
we don't have those extra-small
condoms you ordered.

91
00:04:26,145 --> 00:04:29,042
I called the factory.
It's gonna take a special order.

92
00:04:29,167 --> 00:04:32,972
Not just because of the size,
but because of the weird shape as well.

93
00:04:33,097 --> 00:04:35,691
Something they've never
dealt with before. We'll talk.

94
00:04:36,057 --> 00:04:38,650
Mark never asked me for any small,
weirdly-shaped condoms.

95
00:04:38,775 --> 00:04:40,025
I made that up.

96
00:04:41,148 --> 00:04:44,248
Every day I subtly chip away
at their relationship,

97
00:04:44,657 --> 00:04:46,535
weakening it little by little.

98
00:04:50,066 --> 00:04:53,541
Where's Tom Haverford?

99
00:04:56,124 --> 00:04:57,421
For ******** sake.

100
00:04:57,682 --> 00:05:00,117
<i>So you had a divorce</i>

101
00:05:00,242 --> 00:05:01,364
<i>Of course of course</i>

102
00:05:01,489 --> 00:05:03,636
<i>And no one enjoys
a divorce of course</i>

103
00:05:03,804 --> 00:05:06,388
I don't know what to do.
I can't be like, "Hey, Leslie,

104
00:05:06,818 --> 00:05:08,599
it's a green-card marriage.
I'm fine."

105
00:05:08,767 --> 00:05:10,599
Leslie thinks
you're a wounded animal,

106
00:05:10,724 --> 00:05:12,937
so her female instincts
are kicking in.

107
00:05:13,347 --> 00:05:14,593
Here's what you do.

108
00:05:14,718 --> 00:05:15,648
Act sad.

109
00:05:15,816 --> 00:05:19,336
Let her pull the thorn out of your paw
and wrap a bandage around it.

110
00:05:19,461 --> 00:05:22,071
That'll make her feel better.
She'll get off your jock.

111
00:05:22,356 --> 00:05:25,305
You still can't tell anyone
about the green card thing.

112
00:05:25,450 --> 00:05:28,010
Apparently you can get in trouble
even after divorce.

113
00:05:28,135 --> 00:05:29,226
Damn the man.

114
00:05:29,351 --> 00:05:30,871
Your secret's safe with me.

115
00:05:31,255 --> 00:05:32,623
Thanks, Rondoleezza Rice.

116
00:05:32,791 --> 00:05:34,216
Okay, everyone...

117
00:05:34,341 --> 00:05:36,913
The rumors are true.
Wendy and I are splitting up.

118
00:05:37,466 --> 00:05:38,796
Yeah, I'm really hurt.

119
00:05:39,129 --> 00:05:41,590
I'm so sorry
you feel that way, little friend.

120
00:05:42,130 --> 00:05:44,837
Why don't we go out tonight,
hit the town after work, huh?

121
00:05:44,962 --> 00:05:47,763
We'll make it Tom's night.
You can go anywhere you want. Our treat.

122
00:05:48,601 --> 00:05:50,660
There's a really fun documentary

123
00:05:50,785 --> 00:05:53,285
about tandem bicycles
at the art house.

124
00:05:54,217 --> 00:05:56,313
Supposed to be pretty unapologetic.

125
00:05:56,735 --> 00:05:58,962
- Or we could go to a restaurant.
- Strip club?

126
00:05:59,087 --> 00:06:00,816
- Did somebody say strip club?
- No.

127
00:06:00,941 --> 00:06:02,541
- Somebody say strip club?
- You did.

128
00:06:02,666 --> 00:06:04,702
I definitely heard
someone say strip club.

129
00:06:04,827 --> 00:06:06,511
- You did.
- Would the glitter factory

130
00:06:06,636 --> 00:06:08,534
be okay with whoever
suggested strip club?

131
00:06:08,702 --> 00:06:10,379
I don't think that's a good idea.

132
00:06:10,504 --> 00:06:13,998
There is a great
dinosaur-themed restaurant in Patterson.

133
00:06:14,123 --> 00:06:15,666
It is called Jurassic Fork.

134
00:06:17,878 --> 00:06:20,337
I have gone there three times a week
for the last 15 years.

135
00:06:20,844 --> 00:06:22,420
Dinosaurs, huh?

136
00:06:23,360 --> 00:06:24,648
All right.
Sounds okay.

137
00:06:24,773 --> 00:06:26,626
Okay! It's gonna be
a crazy night, guys.

138
00:06:26,751 --> 00:06:28,220
I will see you at Jurassic Fork

139
00:06:28,388 --> 00:06:29,538
at 5:45 p. M.

140
00:06:33,184 --> 00:06:35,180
Hi, welcome to Jurassic Fork

141
00:06:35,305 --> 00:06:37,903
where the only thing that'll be extinct
is your appetite.

142
00:06:38,801 --> 00:06:40,424
- You ready to order?
- Yes.

143
00:06:40,549 --> 00:06:43,599
I will have the Jamaican
jerk chicken veloci-wrap.

144
00:06:44,122 --> 00:06:46,238
I'm gonna get the tricera-chops,
please.

145
00:06:46,444 --> 00:06:48,221
How do you want that cooked?

146
00:06:48,346 --> 00:06:49,867
Medium roar.

147
00:06:51,341 --> 00:06:52,441
Medium rare?

148
00:06:52,566 --> 00:06:53,866
No, medium roar.

149
00:06:54,613 --> 00:06:58,167
For legal reasons, wecan't make puns
about the temperature of the meats.

150
00:06:58,456 --> 00:07:00,906
I'll have the surf and turf-osaurus,

151
00:07:01,390 --> 00:07:03,540
and a couple of bottles of wine?

152
00:07:04,071 --> 00:07:06,467
I'm gonna need a lot of wine,
so keep it flowing.

153
00:07:06,640 --> 00:07:08,376
I'm not gonna be drinking anything.

154
00:07:08,501 --> 00:07:10,429
Just wanted everybody to know that.

155
00:07:10,632 --> 00:07:12,247
I'm not a big fan of group dinners

156
00:07:12,372 --> 00:07:15,387
where everybody splits the bill
no matter what they get.

157
00:07:15,512 --> 00:07:19,021
I ordered a tyranno-ceasar salad,
and that's all I'm paying for.

158
00:07:20,984 --> 00:07:22,384
Hey, check it out.

159
00:07:22,509 --> 00:07:23,742
They got pool.

160
00:07:24,680 --> 00:07:26,098
You want to play?

161
00:07:26,223 --> 00:07:29,406
Finally settle that debate
about who's the better pool player.

162
00:07:29,845 --> 00:07:31,658
I've never had
that debate with anyone.

163
00:07:31,995 --> 00:07:33,692
We'll put a little money on it.

164
00:07:33,817 --> 00:07:35,704
- Make it a little more interesting.
- Please.

165
00:07:35,872 --> 00:07:37,756
No, I figured
because pool is all about angles,

166
00:07:37,881 --> 00:07:40,638
and he's a failed architect,
that he might want to play pool.

167
00:07:40,763 --> 00:07:43,074
- Let's do it.
- Really? That worked?

168
00:07:43,666 --> 00:07:46,173
How to hustle somebody in pool
by Andy Dwyer.

169
00:07:46,505 --> 00:07:48,312
Step 1, find the person
you want to hustle.

170
00:07:48,437 --> 00:07:49,874
Invite them to play pool.

171
00:07:49,999 --> 00:07:51,449
Should they accept,

172
00:07:51,809 --> 00:07:52,809
you're in.

173
00:07:53,488 --> 00:07:55,849
Here's what I think we should do.
Everyone should go around

174
00:07:56,017 --> 00:07:58,112
and say one thing
that they love about Tom.

175
00:07:58,237 --> 00:07:59,237
I'll start.

176
00:07:59,504 --> 00:08:01,855
- I really wish I could have your body.
- What?

177
00:08:02,523 --> 00:08:04,725
Like, tied up, naked,
in your basement?

178
00:08:05,026 --> 00:08:08,362
No, I mean, you're in good shape,
and you can eat whatever you want.

179
00:08:08,530 --> 00:08:10,030
Well, that was weird, Jerry.

180
00:08:10,474 --> 00:08:11,589
Ann, you go ahead.

181
00:08:11,714 --> 00:08:13,524
I think Tom is really nice.

182
00:08:13,649 --> 00:08:15,332
Also, you can give me
your credit cards,

183
00:08:15,457 --> 00:08:17,788
- I'll divide it up. I don't mind.
- Don't worry about it.

184
00:08:17,956 --> 00:08:20,415
The restaurant'll divide it evenly.
Just enjoy yourself.

185
00:08:20,969 --> 00:08:22,262
April, go ahead.

186
00:08:23,196 --> 00:08:25,087
Tom is the only cool person
in the office.

187
00:08:27,025 --> 00:08:28,132
Ridiculous. Donna?

188
00:08:28,313 --> 00:08:31,051
I love you, Tom.
You're my little prince.

189
00:08:31,371 --> 00:08:33,928
Just want to put you
in a little cape and a little hat

190
00:08:34,053 --> 00:08:35,753
and just fly you around.

191
00:08:36,343 --> 00:08:37,432
Thank you, Donna.

192
00:08:38,295 --> 00:08:39,395
Hey, garcon?

193
00:08:40,102 --> 00:08:41,604
I'm still feeling pretty sad.

194
00:08:41,729 --> 00:08:44,008
Can I get two creme brulees

195
00:08:44,133 --> 00:08:46,692
and another glass
of "bronto" grigio?

196
00:08:47,095 --> 00:08:49,218
I thought I had Tom all figured out,

197
00:08:49,571 --> 00:08:52,043
but it's almost like
he's faking being sad.

198
00:08:52,399 --> 00:08:53,824
Why would he do that?

199
00:08:56,892 --> 00:08:58,192
And that's game.

200
00:08:58,638 --> 00:09:00,488
I think you now owe me $25.

201
00:09:00,832 --> 00:09:01,762
Shoot.

202
00:09:02,135 --> 00:09:04,068
Someone had a pool table
growing up, huh?

203
00:09:04,193 --> 00:09:05,754
What do you say we play again?

204
00:09:05,879 --> 00:09:08,552
We'll make it a little more interesting.
Go double or nothing?

205
00:09:09,308 --> 00:09:10,308
Why not?

206
00:09:11,119 --> 00:09:11,967
Step two,

207
00:09:12,668 --> 00:09:15,977
lose to your opponent intentionally,
so they gain confidence.

208
00:09:16,658 --> 00:09:18,396
Step two has been completed.

209
00:09:18,679 --> 00:09:19,558
Easily.

210
00:09:20,281 --> 00:09:21,381
Very easily.

211
00:09:21,846 --> 00:09:23,746
Mark is pretty good at pool.

212
00:09:25,034 --> 00:09:26,934
So how are you feeling, Tom?

213
00:09:27,059 --> 00:09:28,770
Are you feeling okay?

214
00:09:28,895 --> 00:09:30,558
I like pretending to be sad.

215
00:09:30,683 --> 00:09:32,029
I now see why girls do it.

216
00:09:32,649 --> 00:09:34,518
So your arrangement with Wendy,

217
00:09:34,643 --> 00:09:36,890
it really was completely platonic?

218
00:09:37,427 --> 00:09:39,461
Yeah, never so much as even kissed.

219
00:09:39,690 --> 00:09:42,372
Except for a little peck at the wedding
for appearances.

220
00:09:43,192 --> 00:09:45,066
Now that you're getting divorced,

221
00:09:45,191 --> 00:09:46,791
I sort of feel like...

222
00:09:47,402 --> 00:09:50,297
there may be some potential
with me and Wendy.

223
00:09:51,555 --> 00:09:54,301
Would it be okay with you
if I was to ask her out

224
00:09:54,426 --> 00:09:56,276
once the fake dust settles?

225
00:09:58,928 --> 00:09:59,932
Why not?
Sure.

226
00:10:00,541 --> 00:10:02,863
Looking at her, I... I feel like...

227
00:10:03,254 --> 00:10:05,836
She might be the perfect
spooning size for me.

228
00:10:06,307 --> 00:10:07,814
I'm gonna take a leak.

229
00:10:16,202 --> 00:10:18,263
That would be $6,400.

230
00:10:19,448 --> 00:10:21,954
I accept checks
and most major credit cards.

231
00:10:23,504 --> 00:10:25,813
Mark is way better than me.
I'm gonna...

232
00:10:25,938 --> 00:10:28,463
I'm gonna say
that there is at least...

233
00:10:28,738 --> 00:10:29,788
a chance...

234
00:10:30,930 --> 00:10:33,680
that I didn't think
this through completely.

235
00:10:34,509 --> 00:10:36,051
I think I'm done, Andy.

236
00:10:36,834 --> 00:10:40,149
Forget money.
We'll play for something else.

237
00:10:40,557 --> 00:10:43,834
- You have nothing else to give me.
- A t-shirt I tackled Eddie Vedder in.

238
00:10:43,959 --> 00:10:45,269
It's literally priceless.

239
00:10:45,437 --> 00:10:46,958
Okay, how about this.

240
00:10:47,237 --> 00:10:50,691
If you win, you don't owe me any money,
but if I win, stop bugging me and Ann.

241
00:10:51,134 --> 00:10:53,353
- What are you talking about?
- No more comments.

242
00:10:53,478 --> 00:10:55,328
No more showing up
when we're hanging out.

243
00:10:55,453 --> 00:10:56,829
You leave us alone.

244
00:10:56,954 --> 00:10:59,454
That doesn't seem
like a very fair bet.

245
00:11:01,068 --> 00:11:02,427
So if I win,

246
00:11:02,820 --> 00:11:04,162
I also get Ann.

247
00:11:04,911 --> 00:11:05,822
Okay, fine.

248
00:11:05,947 --> 00:11:08,291
If you win, then you get Ann.

249
00:11:09,168 --> 00:11:10,197
Rack 'em up.

250
00:11:10,322 --> 00:11:11,920
Somebody punch someone.

251
00:11:12,916 --> 00:11:14,486
So how you doing, buddy?

252
00:11:14,611 --> 00:11:16,550
Want some steak? Champagne?

253
00:11:16,718 --> 00:11:19,037
Think of something
and we'll get it. What do you want?

254
00:11:19,162 --> 00:11:20,262
A cheese fountain?

255
00:11:20,814 --> 00:11:21,722
A ruby?

256
00:11:23,226 --> 00:11:24,725
A goose heart?

257
00:11:25,138 --> 00:11:27,647
How about a marriage?
How about a non-divorce?

258
00:11:28,605 --> 00:11:30,770
I'm sorry.
Maybe you need some time apart,

259
00:11:30,895 --> 00:11:32,816
to remember
how much you care about each other.

260
00:11:33,470 --> 00:11:35,944
Think about it. Does our marriage
really make sense to you?

261
00:11:36,448 --> 00:11:38,739
She's a tall, beautiful surgeon.

262
00:11:38,907 --> 00:11:41,658
I'm a short,
beautiful government employee

263
00:11:41,783 --> 00:11:43,243
slash club promoter.

264
00:11:43,536 --> 00:11:44,703
You're a club promoter?

265
00:11:44,871 --> 00:11:45,871
Aspiring.

266
00:11:47,558 --> 00:11:50,308
The point is,
I never meant anything to her.

267
00:11:50,800 --> 00:11:52,377
It wasn't even a real marriage.

268
00:11:53,002 --> 00:11:55,452
Now the sadness
is pouring out of Tom.

269
00:11:55,794 --> 00:11:59,301
Like blood from a pterodactyl
after it's attacked by a T-Rex.

270
00:11:59,602 --> 00:12:03,202
I have to take him to a place
where he can't possibly be sad.

271
00:12:07,319 --> 00:12:08,212
Crap.

272
00:12:08,937 --> 00:12:11,563
- We are going to the Glitter Factory.
- What?

273
00:12:11,871 --> 00:12:14,304
I can't go back there.
But if you see Jasmine,

274
00:12:14,429 --> 00:12:17,235
tell her she can keep Anthony,
but I want my microwave back.

275
00:12:18,385 --> 00:12:20,322
Good.
You're gonna take April home.

276
00:12:20,607 --> 00:12:22,449
No, I want to go
to the Glitter Factory.

277
00:12:22,617 --> 00:12:24,701
Drop out of school
and start doing meth.

278
00:12:25,066 --> 00:12:26,566
Let's go, everybody.

279
00:12:28,316 --> 00:12:30,082
I appreciate
what you're trying to do,

280
00:12:30,250 --> 00:12:31,541
but once you go in there,

281
00:12:31,709 --> 00:12:33,964
you will see things
you cannot un-see.

282
00:12:34,379 --> 00:12:36,710
I'm a feminist. I would never,
ever go to a strip club.

283
00:12:36,835 --> 00:12:39,599
I've gone on record
that if I had to have a stripper's name,

284
00:12:39,724 --> 00:12:41,274
it would be Equality.

285
00:12:41,553 --> 00:12:43,720
But I'm willing to sacrifice
all that I've worked for

286
00:12:43,888 --> 00:12:47,140
just to put a smile on your perverted
little face. So don't blow this.

287
00:12:47,308 --> 00:12:49,521
- All right.
- Lap dances are on me.

288
00:12:49,646 --> 00:12:52,668
I mean, I'm paying for them.
They're not gonna actually be on me.

289
00:12:52,793 --> 00:12:53,605
Got it.

290
00:12:53,919 --> 00:12:56,668
I kind of feel like Jane Goodall
studying the chimps.

291
00:12:56,793 --> 00:12:58,610
'Cause there are
some feminist scholars

292
00:12:58,778 --> 00:13:01,279
who say that stripping
is a feminist act.

293
00:13:10,643 --> 00:13:13,522
There is a girl here
that also works at Quiznos.

294
00:13:13,647 --> 00:13:15,085
She's really nice to me here

295
00:13:15,253 --> 00:13:17,390
but really mean to me at Quiznos.

296
00:13:18,920 --> 00:13:21,967
Go put these in places
I do not approve of.

297
00:13:22,582 --> 00:13:25,347
I'm gonna put these in places
you've never heard of.

298
00:13:26,089 --> 00:13:27,998
Yeah, I've been a little down.

299
00:13:28,123 --> 00:13:30,573
Totally natural.
I'm getting divorced.

300
00:13:30,986 --> 00:13:34,229
But now I'm ready to pull myself up
by some g-strings.

301
00:13:35,773 --> 00:13:36,857
It's rough in here.

302
00:13:37,025 --> 00:13:39,006
- Is it always like this?
- I wouldn't know.

303
00:13:39,131 --> 00:13:40,781
Don't like strip clubs.

304
00:13:41,002 --> 00:13:42,612
Smells like a wet mop in here.

305
00:13:43,061 --> 00:13:46,408
And I get the feeling that these women
is running a low-grade fever.

306
00:13:47,163 --> 00:13:48,577
You're one of the good ones.

307
00:13:49,291 --> 00:13:50,491
Wait a minute.

308
00:13:51,432 --> 00:13:52,782
Hello, beautiful.

309
00:13:54,709 --> 00:13:56,251
Strippers do nothing for me.

310
00:13:56,419 --> 00:13:58,514
I like a strong, salt of the earth,

311
00:13:58,639 --> 00:14:01,085
self-possessed woman
at the top of her field,

312
00:14:01,210 --> 00:14:03,008
your Steffi Grafs,
your Sheryl Swoopes.

313
00:14:04,390 --> 00:14:06,873
But I will take
a free breakfast buffet

314
00:14:07,262 --> 00:14:09,264
any time, any place.

315
00:14:16,397 --> 00:14:18,190
Tom cat.
Pull up a mouth.

316
00:14:18,358 --> 00:14:20,317
This buffet is unstoppable.

317
00:14:20,790 --> 00:14:22,040
I'm not hungry.

318
00:14:22,528 --> 00:14:24,946
You're doing a bang-up job
of looking sad about Wendy.

319
00:14:27,425 --> 00:14:28,783
Does she make scrambled eggs?

320
00:14:31,609 --> 00:14:32,788
Take it down a notch.

321
00:14:33,341 --> 00:14:35,859
You already won your Oscar,
DiCaprio.

322
00:14:40,926 --> 00:14:43,089
Ann, in case I don't sink this,

323
00:14:43,257 --> 00:14:45,449
it's been a real pleasure
dating you.

324
00:14:50,333 --> 00:14:51,807
Scratch on the eight!

325
00:14:55,206 --> 00:14:56,228
I'm victorious.

326
00:14:56,713 --> 00:14:59,550
I'm awesome at pool,
and I hustled your ass.

327
00:15:01,913 --> 00:15:03,276
Take a moment to say good-bye.

328
00:15:04,779 --> 00:15:06,988
I don't even know how to say this.
I am so sorry,

329
00:15:07,156 --> 00:15:09,691
and I will do my best
to visit you on holidays.

330
00:15:10,470 --> 00:15:11,618
Thanks. You tried.

331
00:15:11,786 --> 00:15:13,245
I guess you're his now.

332
00:15:13,413 --> 00:15:15,705
- Do you want to get out of here?
- I do.

333
00:15:16,018 --> 00:15:17,119
Bye, Andy.

334
00:15:17,347 --> 00:15:19,000
I know that legally

335
00:15:20,113 --> 00:15:22,127
Ann is now mine,

336
00:15:22,653 --> 00:15:25,867
but,
it weirdly doesn't feel that way.

337
00:15:29,470 --> 00:15:31,388
- This is Seabiscuit.
- Sierra.

338
00:15:31,846 --> 00:15:33,223
Sorry, it's loud in here.

339
00:15:33,391 --> 00:15:36,851
And I gave her money
to writhe around on your parts.

340
00:15:37,205 --> 00:15:38,395
I don't want to do that.

341
00:15:38,750 --> 00:15:41,265
Well, I already paid her.
Can I get my money back?

342
00:15:41,390 --> 00:15:42,774
So let her do her writhing.

343
00:15:43,966 --> 00:15:46,903
I just got to say, Sierra,
I don't get why this cheers men up,

344
00:15:47,071 --> 00:15:48,864
because it's very insincere
and fleeting,

345
00:15:49,032 --> 00:15:51,649
but go crazy.
Give my friend here the works.

346
00:15:51,774 --> 00:15:54,062
- Really grind the sorrow out of him.
- You got it.

347
00:15:54,187 --> 00:15:56,538
And then afterwards,
reconsider your profession, but for now,

348
00:15:57,006 --> 00:15:58,056
grind away.

349
00:16:03,254 --> 00:16:05,190
Stripper dancing action.

350
00:16:08,043 --> 00:16:09,593
I don't want to do this.

351
00:16:09,886 --> 00:16:12,721
Is it because you have a strong,
positive, female figure watching you?

352
00:16:13,027 --> 00:16:14,723
No, it's because I'm miserable.

353
00:16:15,698 --> 00:16:18,560
Never mind.
Thank you, Seabiscuit. That'll be all.

354
00:16:24,400 --> 00:16:26,026
I don't know what's wrong with me.

355
00:16:26,319 --> 00:16:28,987
This is honestly
the saddest I've ever been.

356
00:16:29,155 --> 00:16:30,280
Glitter bomb!

357
00:16:40,976 --> 00:16:44,306
I've been to the Glitter Factory
a million times.

358
00:16:44,962 --> 00:16:46,087
That girl up there,

359
00:16:46,404 --> 00:16:48,006
she's my emergency contact.

360
00:16:49,045 --> 00:16:50,247
But right now,

361
00:16:50,372 --> 00:16:51,622
I hate it here.

362
00:16:52,247 --> 00:16:54,012
And I just want to see Wendy.

363
00:16:54,710 --> 00:16:58,145
It's perfectly normal to feel devastated
when something's over.

364
00:16:58,270 --> 00:17:01,269
It's exactly how I felt
when that planet earth series ended.

365
00:17:01,645 --> 00:17:02,645
60 days!

366
00:17:03,340 --> 00:17:05,398
And then she's free
to marry Ron Swanson.

367
00:17:06,469 --> 00:17:07,484
What? Ron?

368
00:17:07,859 --> 00:17:09,736
He's gonna ask her out.
He told me.

369
00:17:10,208 --> 00:17:11,112
But who cares?

370
00:17:11,280 --> 00:17:14,130
If it's not him,
it'll just be some other guy.

371
00:17:18,548 --> 00:17:20,830
Did you tell Tom
that you were gonna ask out Wendy?

372
00:17:21,783 --> 00:17:22,791
It's complicated.

373
00:17:23,060 --> 00:17:24,551
What is wrong with you?

374
00:17:24,676 --> 00:17:27,099
I wish I wasn't alive
to hear myself say this,

375
00:17:27,224 --> 00:17:28,697
but I am ashamed

376
00:17:29,507 --> 00:17:31,883
to be your deputy.
I don't get men.

377
00:17:32,235 --> 00:17:34,398
If they're not wagering
their girlfriends in pool,

378
00:17:34,523 --> 00:17:36,832
then they're trying to steal
each other's wives.

379
00:17:36,957 --> 00:17:40,288
It makes you question the whole notion
of those bromance movies.

380
00:17:44,079 --> 00:17:45,313
Great.
Good. Okay.

381
00:17:45,481 --> 00:17:47,491
Could you carry him out of here?

382
00:17:47,616 --> 00:17:50,219
And, you know, also maybe
not have sex with his wife?

383
00:17:50,344 --> 00:17:51,344
Thanks.

384
00:17:53,209 --> 00:17:54,295
From the knees.

385
00:17:54,420 --> 00:17:56,163
He weighs eight pounds.

386
00:18:02,041 --> 00:18:04,582
We took Tom out tonight,
and he had a bit too much to drink.

387
00:18:08,239 --> 00:18:09,289
Come on in.

388
00:18:13,212 --> 00:18:14,212
My God.

389
00:18:14,337 --> 00:18:16,344
You insensitive little hussy.

390
00:18:16,512 --> 00:18:17,440
Excuse me?

391
00:18:18,966 --> 00:18:19,683
Halt.

392
00:18:20,057 --> 00:18:22,517
You're not divorced yet
and you're inviting other men over?

393
00:18:23,273 --> 00:18:24,227
You're married?

394
00:18:25,504 --> 00:18:27,067
Technically, yeah.

395
00:18:27,881 --> 00:18:29,190
That's my husband.

396
00:18:29,358 --> 00:18:30,483
Who's he carrying?

397
00:18:30,651 --> 00:18:32,360
No. That's his boss.

398
00:18:32,788 --> 00:18:35,362
My husband's the one that's
being carried.

399
00:18:35,487 --> 00:18:36,573
By his boss.

400
00:18:36,929 --> 00:18:38,173
How's it going?

401
00:18:38,298 --> 00:18:39,024
Good.

402
00:18:39,149 --> 00:18:40,326
You know what?

403
00:18:40,494 --> 00:18:42,664
You and Ron will be perfect
for each other.

404
00:18:42,789 --> 00:18:46,777
You should get married and start a club
for people who betray Tom Haverford.

405
00:18:49,431 --> 00:18:50,920
You have a lovely home.

406
00:18:54,500 --> 00:18:56,300
So where do you want this?

407
00:18:57,203 --> 00:18:58,344
So how you feeling?

408
00:18:59,162 --> 00:19:00,096
Rough morning?

409
00:19:00,264 --> 00:19:03,314
You know those hangover pills
you can order on TV?

410
00:19:03,439 --> 00:19:06,077
I threw up a bunch of them this morning
and feel much better.

411
00:19:06,202 --> 00:19:07,645
Last night was really rough,

412
00:19:07,813 --> 00:19:10,065
but all I want for you
is to get over this.

413
00:19:10,447 --> 00:19:11,900
I need to tell you something.

414
00:19:13,628 --> 00:19:16,492
The reason
I was acting weird yesterday was,

415
00:19:16,617 --> 00:19:19,117
Wendy and I have
a green-card marriage.

416
00:19:21,137 --> 00:19:22,619
My God, because you're Libyan.

417
00:19:23,234 --> 00:19:24,954
No, damn it.
Wendy's from Canada.

418
00:19:28,285 --> 00:19:29,375
Wait. Start again?

419
00:19:31,360 --> 00:19:32,276
This...

420
00:19:35,088 --> 00:19:37,133
You guys got a second?
It's about last night.

421
00:19:37,520 --> 00:19:40,929
I hope I'm not gonna have
to explain to you that you don't own me.

422
00:19:41,097 --> 00:19:42,084
Of course not.

423
00:19:42,209 --> 00:19:44,808
I never for one second
thought that that was for real.

424
00:19:47,320 --> 00:19:49,871
You don't have to worry
about me bothering you anymore.

425
00:19:49,996 --> 00:19:51,955
You clearly have something going,

426
00:19:52,080 --> 00:19:53,650
and I should respect that.

427
00:19:55,737 --> 00:19:57,904
If you say "psych",
I'm gonna be really pissed.

428
00:19:58,612 --> 00:20:01,167
You remembered how much I like
to say "psych".

429
00:20:02,038 --> 00:20:04,493
But no, this is no "psych".

430
00:20:06,531 --> 00:20:08,581
Thank you
for saying all that just now.

431
00:20:09,095 --> 00:20:09,965
Sure.

432
00:20:11,157 --> 00:20:12,833
Goodbye, A-cakes.

433
00:20:13,300 --> 00:20:14,514
Hello, Ann.

434
00:20:15,516 --> 00:20:17,203
Good-bye, Ann.

435
00:20:18,610 --> 00:20:19,551
Good-bye, Andy.

436
00:20:26,136 --> 00:20:29,561
I thought for a second you were gonna
chase after me, but you didn't.

437
00:20:29,883 --> 00:20:31,354
I meant every word I said.

438
00:20:33,800 --> 00:20:36,776
You like your wife?
That's a bummer.

439
00:20:37,358 --> 00:20:40,780
Anyone knows that you like your wife?
Your wife knows that you like your wife?

440
00:20:41,111 --> 00:20:43,111
I didn't know until yesterday.

441
00:20:43,372 --> 00:20:45,305
So don't tell anyone.
Don't tell Ron.

442
00:20:45,430 --> 00:20:46,452
It's my problem.

443
00:20:49,141 --> 00:20:51,499
How could there possibly
still be glitter on me?

444
00:20:51,900 --> 00:20:53,349
Takes forever to get off.

445
00:20:53,474 --> 00:20:55,628
My crotch looks like a disco ball.

446
00:21:11,469 --> 00:21:13,271
<i>Sorry she called you selfish
of course</i>

447
00:21:13,491 --> 00:21:15,648
<i>She just didn't know
the full story of course</i>

448
00:21:15,816 --> 00:21:18,574
<i>But now that she does
she's sorry of course</i>

449
00:21:19,445 --> 00:21:21,154
<i>She loves being your deputy</i>

450
00:21:24,241 --> 00:21:25,711
www.sous-titres.eu

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
