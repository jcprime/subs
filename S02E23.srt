1
00:00:12,668 --> 00:00:14,636
The place the Pawnee general
has called

2
00:00:14,704 --> 00:00:17,539
The sexiest,
most dangerous club in town.

3
00:00:17,607 --> 00:00:19,574
- That's not what they wrote.
- Fine.

4
00:00:19,642 --> 00:00:20,942
I added the word "sexiest,"

5
00:00:21,010 --> 00:00:22,454
But we've hired
better security.

6
00:00:22,522 --> 00:00:25,314
Yes, I am a good friend
for throwing the party.

7
00:00:25,382 --> 00:00:27,912
I'm also a genius because
I'm using the occasion

8
00:00:27,979 --> 00:00:30,649
To stock the club with
every available hottie I know.

9
00:00:30,717 --> 00:00:32,414
Call me a romantic,

10
00:00:32,482 --> 00:00:33,848
But I believe by
the end of the night,

11
00:00:33,915 --> 00:00:36,913
I will have between
one and four new girlfriends.

12
00:00:36,981 --> 00:00:38,114
Oh, that's great.

13
00:00:38,182 --> 00:00:39,681
I'd love to hear
all about your family vacations.

14
00:00:39,749 --> 00:00:41,081
- Hey.
- Oh, thank God.

15
00:00:41,149 --> 00:00:42,716
Get out, Kyle.

16
00:00:42,784 --> 00:00:46,416
Hello, birthday girl.

17
00:00:46,484 --> 00:00:47,720
Got you a birthday present.

18
00:00:47,788 --> 00:00:48,890
- Really?
- Yeah.

19
00:00:48,958 --> 00:00:51,027
I, uh, I wrote you a song.

20
00:00:51,095 --> 00:00:52,798
Score. What's it called?

21
00:00:52,865 --> 00:00:54,036
I'm not telling,
but I'll give you a clue.

22
00:00:54,104 --> 00:00:56,005
It's named after
a month out of the year.

23
00:00:56,073 --> 00:00:57,173
So April?

24
00:00:57,241 --> 00:01:00,147
No.

25
00:01:00,214 --> 00:01:01,382
That would have been
way better.

26
00:01:01,450 --> 00:01:03,317
Um...
Well, whatever.

27
00:01:03,385 --> 00:01:04,684
I can rewrite the lyrics.

28
00:01:04,752 --> 00:01:06,720
Um, are you coming to my party?

29
00:01:06,788 --> 00:01:08,521
Yeah, I wouldn't miss it
for the world.

30
00:01:08,589 --> 00:01:09,656
Are you kidding me?

31
00:01:09,724 --> 00:01:11,259
- Okay, cool.
- Okay.

32
00:01:11,327 --> 00:01:13,394
Um, well, we'll hang out then.

33
00:01:13,462 --> 00:01:14,428
Okay.

34
00:01:14,496 --> 00:01:15,929
It's gonna be fun.

35
00:01:15,997 --> 00:01:18,898
Yes, I am 21 years old today.

36
00:01:18,966 --> 00:01:21,600
Which is the age
that pretty much everyone agrees

37
00:01:21,668 --> 00:01:24,736
Makes you an adult.

38
00:01:27,773 --> 00:01:29,373
♪ ♪

39
00:01:44,865 --> 00:01:47,668
The yearly budget
and planning proposal for a city

40
00:01:47,736 --> 00:01:50,972
<i>Is called the master plan.</i>

41
00:01:51,040 --> 00:01:52,841
Isn't that just so awesome
you can't stand it?

42
00:01:52,908 --> 00:01:57,181
Oh, I shall now reveal to you
my master plan.

43
00:01:57,248 --> 00:01:58,715
Mwah-ha-ha-ha-

44
00:01:58,783 --> 00:02:00,317
Ee-ah-ah-ah-ah...

45
00:02:01,620 --> 00:02:03,154
Kind of sounded like
a chimp there at the end.

46
00:02:03,222 --> 00:02:05,356
Good morning, everybody.

47
00:02:05,424 --> 00:02:07,926
I know you all have
your budget presentations ready,

48
00:02:07,993 --> 00:02:09,427
But there's a change of plans.

49
00:02:09,495 --> 00:02:11,996
Due to the crippling gridlock
in city counsel,

50
00:02:12,064 --> 00:02:13,231
We are postponing

51
00:02:13,299 --> 00:02:15,601
All planning and spending
decisions indefinitely.

52
00:02:15,668 --> 00:02:17,502
Um, until when?

53
00:02:17,570 --> 00:02:18,704
Indefinitely.

54
00:02:18,772 --> 00:02:20,640
- And when will that end?
- Later than now.

55
00:02:20,708 --> 00:02:22,575
So this week probably?

56
00:02:22,643 --> 00:02:25,312
- Look, we are bordering on
a full-blown crisis, Leslie.

57
00:02:25,379 --> 00:02:27,748
The state government
is sending a team

58
00:02:27,816 --> 00:02:29,950
From Indianapolis
to try to solve

59
00:02:30,018 --> 00:02:31,285
This budget problem.

60
00:02:31,353 --> 00:02:32,853
Just calm down.

61
00:02:32,921 --> 00:02:34,622
You don't even know
what they're gonna do.

62
00:02:34,690 --> 00:02:36,124
Ron, they're state auditors.

63
00:02:36,191 --> 00:02:37,559
They're here to slash and burn.

64
00:02:37,626 --> 00:02:39,161
We gotta fight these guys.

65
00:02:39,229 --> 00:02:40,729
They've been sent by
the governor.

66
00:02:40,797 --> 00:02:42,498
They outrank everyone.

67
00:02:42,565 --> 00:02:44,300
There's no fight
to be had here.

68
00:02:44,367 --> 00:02:46,068
Yeah, you're right.
I'm not gonna fight them.

69
00:02:46,136 --> 00:02:47,503
Except that I am!

70
00:02:47,571 --> 00:02:49,439
Okay.

71
00:02:49,507 --> 00:02:50,740
Leslie.

72
00:02:50,808 --> 00:02:54,143
What do we do
when we get this angry?

73
00:02:54,211 --> 00:02:55,945
We count backwards
from 1,000 by sevens

74
00:02:56,012 --> 00:02:57,279
And we think of warm brownies.

75
00:02:57,347 --> 00:02:59,448
Go do that in your office.

76
00:02:59,516 --> 00:03:00,717
Go do it in your office.

77
00:03:00,784 --> 00:03:02,518
And we'll wait for
these guys to show up.

78
00:03:02,586 --> 00:03:04,987
I'll wait for you to show up.

79
00:03:05,055 --> 00:03:06,722
Andy!

80
00:03:06,790 --> 00:03:08,658
You coming out tonight,
part owner of the club?

81
00:03:08,725 --> 00:03:10,192
Uh, yes.

82
00:03:10,260 --> 00:03:11,660
Let me ask you
a quick question.

83
00:03:11,728 --> 00:03:13,195
What's the youngest
a girl can be

84
00:03:13,263 --> 00:03:15,430
That if, uh, we go out,
I'm not a total scumbag?

85
00:03:15,498 --> 00:03:17,065
You know the old rule.

86
00:03:17,133 --> 00:03:18,533
Half your age, plus seven.

87
00:03:18,601 --> 00:03:21,803
Half my age plus seven.
Oh, okay.

88
00:03:21,871 --> 00:03:24,372
Well, I'm 29 so half of...

89
00:03:24,439 --> 00:03:27,876
29...Add seven,

90
00:03:27,944 --> 00:03:32,414
That's only 20...Is--

91
00:03:32,481 --> 00:03:35,050
- 21--
- 20 years old.

92
00:03:35,118 --> 00:03:36,218
- 21.
- And a half.

93
00:03:36,286 --> 00:03:37,353
21 years old and a half.

94
00:03:37,420 --> 00:03:38,687
- Yup.
- Yup.

95
00:03:38,755 --> 00:03:40,957
We got the same thing
with the equation on that one.

96
00:03:41,024 --> 00:03:42,291
Here's the thing.

97
00:03:42,359 --> 00:03:44,294
What if she's slightly younger?

98
00:03:44,361 --> 00:03:45,562
Can I go out with
someone younger than that?

99
00:03:45,629 --> 00:03:47,397
Please!
You totally can.

100
00:03:47,465 --> 00:03:48,732
Yeah.

101
00:03:48,799 --> 00:03:52,536
Tom says it's okay.

102
00:03:52,603 --> 00:03:55,105
That probably means
it isn't okay.

103
00:03:55,173 --> 00:03:58,876
Thank you for meeting
with me again.

104
00:03:58,944 --> 00:04:01,412
I don't even know really
what I wanna say.

105
00:04:01,480 --> 00:04:02,780
But I guess
I just don't get it.

106
00:04:02,848 --> 00:04:04,883
You know, our relationship
was so great.

107
00:04:04,950 --> 00:04:07,986
Mark, honestly,
it was really nice.

108
00:04:08,054 --> 00:04:11,022
But maybe you thought
it was so amazing

109
00:04:11,090 --> 00:04:13,558
Because you've never dated
someone for so long before.

110
00:04:13,626 --> 00:04:16,661
Well, but, for example,
we barely ever fought.

111
00:04:16,729 --> 00:04:18,563
That's not a good sign.

112
00:04:18,631 --> 00:04:21,199
You're supposed to fight.
Sometimes.

113
00:04:21,266 --> 00:04:24,902
You're...Supposed to miss
the other person

114
00:04:24,970 --> 00:04:27,504
Even if they're only gone
for 20 minutes.

115
00:04:27,572 --> 00:04:29,440
And I just didn't feel that.

116
00:04:29,507 --> 00:04:31,575
Did you?

117
00:04:31,643 --> 00:04:34,511
Man, I, uh...

118
00:04:34,579 --> 00:04:37,748
I should have yelled at you
way more.

119
00:04:37,816 --> 00:04:39,850
Well, Paul just called.

120
00:04:39,918 --> 00:04:41,752
They're on their way.

121
00:04:41,820 --> 00:04:42,986
Oh, God, they're gonna
fire people, aren't they?

122
00:04:43,054 --> 00:04:45,088
Yeah, I am two years
from my pension.

123
00:04:45,156 --> 00:04:46,356
Relax, Jerry.
We don't know that.

124
00:04:46,424 --> 00:04:49,025
Maybe these people
are very helpful and pleasant--

125
00:04:49,093 --> 00:04:51,294
- Hello.
- Ha! Death!

126
00:04:51,362 --> 00:04:54,296
- Divorce filings?
- Fourth floor.

127
00:04:54,364 --> 00:04:56,365
- Hello, hello?
- Oh.

128
00:04:56,433 --> 00:04:58,166
Hey. How are you?
Hello there.

129
00:04:58,234 --> 00:04:59,367
Hi.

130
00:04:59,435 --> 00:05:01,103
Chris Traeger.
This is Ben.

131
00:05:01,170 --> 00:05:02,204
Hello, gents.
Ron Swanson.

132
00:05:02,272 --> 00:05:06,408
Ron...Swanson.

133
00:05:06,476 --> 00:05:07,609
Okay.

134
00:05:07,677 --> 00:05:09,177
I'm deputy director,
Leslie Knope.

135
00:05:09,245 --> 00:05:12,681
Leslie...Knope.

136
00:05:12,749 --> 00:05:13,849
It is fantastic to be here.

137
00:05:13,916 --> 00:05:15,017
Would you gentlemen
like a tour?

138
00:05:15,084 --> 00:05:16,184
Oh, there is quite literally

139
00:05:16,252 --> 00:05:17,653
Nothing I would rather have
in the world

140
00:05:17,720 --> 00:05:19,788
Than a tour of the parks
and recreation department

141
00:05:19,856 --> 00:05:23,725
Of the great city of Pawnee
led by Ron Swanson.

142
00:05:23,793 --> 00:05:25,293
And Leslie Knope.

143
00:05:25,361 --> 00:05:26,928
- Okay.
- Ben?

144
00:05:26,996 --> 00:05:27,996
I don't think
that's a great idea.

145
00:05:28,064 --> 00:05:29,331
- Let's do it.
- Okay.

146
00:05:29,398 --> 00:05:31,166
Chris is the most positive
state budget

147
00:05:31,233 --> 00:05:32,901
Auditing consultant
I've ever met.

148
00:05:32,968 --> 00:05:35,436
I mean,
I made eye contact with him,

149
00:05:35,504 --> 00:05:37,438
And it was like...

150
00:05:37,506 --> 00:05:39,573
Staring into the sun.

151
00:05:39,641 --> 00:05:40,741
People.

152
00:05:40,809 --> 00:05:42,743
We are here from
the state budget office

153
00:05:42,811 --> 00:05:44,612
From Indianapolis.

154
00:05:44,680 --> 00:05:45,980
Ooh! What does that mean?

155
00:05:46,048 --> 00:05:49,317
Well, simply, we are here
to tinker with your budget.

156
00:05:49,384 --> 00:05:52,653
Think of the government
as a broken down carousel.

157
00:05:52,721 --> 00:05:54,655
We're gonna slap on
a new coat of paint,

158
00:05:54,723 --> 00:05:56,857
We're gonna fix that broken
speaker system,

159
00:05:56,925 --> 00:05:58,258
And we are gonna get
those happy kids

160
00:05:58,326 --> 00:06:00,894
Back up on the horses
where they belong!

161
00:06:00,962 --> 00:06:02,062
Okay?

162
00:06:02,130 --> 00:06:04,064
Yeah.

163
00:06:04,132 --> 00:06:05,332
Okay?

164
00:06:06,868 --> 00:06:08,235
There it is.

165
00:06:08,303 --> 00:06:10,004
My, uh, partner Ben is gonna
stick around for a little bit.

166
00:06:10,071 --> 00:06:12,906
And I will see you all later.

167
00:06:12,974 --> 00:06:14,508
Adios.

168
00:06:14,576 --> 00:06:16,577
Do you have a second?

169
00:06:21,316 --> 00:06:23,283
I really like your shirt.

170
00:06:23,351 --> 00:06:24,751
So I'd like to talk about

171
00:06:24,819 --> 00:06:27,086
Where you think there's waste
within your department.

172
00:06:28,722 --> 00:06:30,724
- There is none.
- Where do I start?

173
00:06:30,791 --> 00:06:33,526
What exactly
will you be cutting?

174
00:06:33,594 --> 00:06:34,927
And how much of it,

175
00:06:34,995 --> 00:06:38,196
And can I watch you doing it
while eating pork cracklins?

176
00:06:38,264 --> 00:06:40,365
Okay, let's start
with personnel.

177
00:06:40,433 --> 00:06:43,101
What can you tell me about
Jerry Gergich?

178
00:06:43,168 --> 00:06:45,737
He is one of the best people
on the planet.

179
00:06:45,804 --> 00:06:47,405
He's universally adored here.

180
00:06:47,473 --> 00:06:50,207
If you fired him,
there would be a revolt.

181
00:06:50,275 --> 00:06:51,576
Okay, you need to understand

182
00:06:51,644 --> 00:06:53,412
That just to keep this town
afloat,

183
00:06:53,480 --> 00:06:55,114
We probably have to cut
the budget

184
00:06:55,182 --> 00:06:58,351
Of every department
by 40% or 50%, okay?

185
00:06:58,418 --> 00:07:00,853
Well, but Chris said that
you just had to,

186
00:07:00,920 --> 00:07:02,789
You know, tinker with things.

187
00:07:02,856 --> 00:07:05,358
Yeah, he said that because
that sounds a lot better than,

188
00:07:05,426 --> 00:07:06,726
"we're going to gut it with
a machete."

189
00:07:06,794 --> 00:07:09,996
Okay?

190
00:07:12,800 --> 00:07:14,635
- You're a jerk.
- I'm sorry?

191
00:07:14,702 --> 00:07:15,969
Easy.

192
00:07:16,037 --> 00:07:17,504
I'm sorry, these are
real people in a real town

193
00:07:17,571 --> 00:07:19,439
Working in a real building
with real feelings.

194
00:07:19,507 --> 00:07:21,341
This building has feelings?

195
00:07:21,408 --> 00:07:22,676
Maybe.

196
00:07:22,743 --> 00:07:24,644
There's a lot of history
in this one, maybe it does.

197
00:07:24,712 --> 00:07:26,012
How can you be
so blase about it?

198
00:07:26,079 --> 00:07:27,713
Because I didn't cause
these problems, miss Knope.

199
00:07:27,781 --> 00:07:29,849
Your government did.

200
00:07:29,916 --> 00:07:31,484
I'll get what I need
from the spreadsheets.

201
00:07:31,552 --> 00:07:34,320
Thanks.

202
00:07:37,524 --> 00:07:38,958
- What's a not-gay way
to ask him

203
00:07:39,026 --> 00:07:41,061
To go camping with me?

204
00:07:43,788 --> 00:07:44,854
- John-Ralphio.
- Lookin' hot, big t.

205
00:07:44,922 --> 00:07:46,189
♪ throw them bones ♪

206
00:07:46,257 --> 00:07:47,357
What?

207
00:07:47,424 --> 00:07:48,791
Damn, girl,
who you tryin' to impress?

208
00:07:48,859 --> 00:07:50,359
Just kidding, I know.

209
00:07:50,427 --> 00:07:53,529
- Walk away.
- Yep, you got it.

210
00:07:53,596 --> 00:07:54,963
Got your birthday shot.

211
00:07:55,031 --> 00:07:56,465
Oh, thanks.

212
00:07:56,532 --> 00:07:59,101
But now that it's legal,
I've kind of lost interest.

213
00:07:59,168 --> 00:08:02,803
Suit yourself.

214
00:08:04,773 --> 00:08:07,541
I guess our awesome new park
is on hold.

215
00:08:07,609 --> 00:08:09,309
Do you have any idea
what they're gonna cut?

216
00:08:09,377 --> 00:08:10,744
No.

217
00:08:10,812 --> 00:08:13,147
I don't know, this has been
a really crappy day.

218
00:08:13,214 --> 00:08:14,381
Where were you earlier
when I called?

219
00:08:14,449 --> 00:08:15,882
Talking to mark.

220
00:08:15,950 --> 00:08:19,252
- Again?
- Yeah.

221
00:08:19,319 --> 00:08:21,420
- You wanna get super drunk?
- I really do.

222
00:08:21,488 --> 00:08:22,655
- Yeah, where's our lady. Hey!
- Hey.

223
00:08:22,723 --> 00:08:25,525
I hope you all know,
I was instrumental

224
00:08:25,592 --> 00:08:27,259
In getting Trish crowned
miss Pawnee.

225
00:08:27,327 --> 00:08:28,427
What was your talent again?

226
00:08:28,495 --> 00:08:31,097
Oh, yeah.
Looking amazing.

227
00:08:34,202 --> 00:08:35,402
Hey, ladies.

228
00:08:35,470 --> 00:08:36,937
Is there anything
you desire at all besides me?

229
00:08:38,640 --> 00:08:41,842
So funny.
You are so funny.

230
00:08:41,910 --> 00:08:44,112
- I--I'm serious, though.

231
00:08:44,179 --> 00:08:45,412
Um, are you into me?

232
00:08:45,480 --> 00:08:46,947
What?

233
00:08:50,618 --> 00:08:53,953
Oh!

234
00:08:54,021 --> 00:08:56,421
Chug that bitch!
Oh, rules are rules.

235
00:08:56,489 --> 00:08:59,658
♪ ♪

236
00:08:59,725 --> 00:09:03,461
Hey. April.
Oh, my gosh.

237
00:09:03,528 --> 00:09:04,528
You look amazing.

238
00:09:04,596 --> 00:09:05,629
- Oh.
- Let me see.

239
00:09:05,697 --> 00:09:06,797
- No.
- Do a twirl.

240
00:09:06,865 --> 00:09:07,865
- No.
- This is beautiful.

241
00:09:07,933 --> 00:09:09,834
No, don't look at me.

242
00:09:09,902 --> 00:09:12,170
No, look. Look at me.
Happy Birthday.

243
00:09:12,238 --> 00:09:13,338
Thank you.

244
00:09:13,406 --> 00:09:14,439
Uh, can I get you a drink?

245
00:09:14,507 --> 00:09:18,144
Or--wait.

246
00:09:18,212 --> 00:09:20,446
Yeah, I can swing it.
Can I get you a drink?

247
00:09:20,514 --> 00:09:22,482
Whiskey. Neat.

248
00:09:22,550 --> 00:09:24,484
Wow, you're not messing around.

249
00:09:24,552 --> 00:09:27,120
No. I'm not.

250
00:09:27,188 --> 00:09:31,391
♪ ♪

251
00:09:31,459 --> 00:09:32,659
Barkeep!

252
00:09:32,727 --> 00:09:35,662
Andy Dwyer.
It is so awesome to see you.

253
00:09:35,730 --> 00:09:37,330
Hey, you too, how are you?

254
00:09:37,398 --> 00:09:38,631
Let me ask you a question.

255
00:09:38,699 --> 00:09:40,666
Was I a good girlfriend?

256
00:09:40,734 --> 00:09:43,036
Ann, you were
an awesome girlfriend.

257
00:09:43,104 --> 00:09:44,771
- Really?
- Yeah.

258
00:09:44,839 --> 00:09:47,340
- Aw.
- Yeah.

259
00:09:47,408 --> 00:09:48,808
I mean, I was the screwup.

260
00:09:48,876 --> 00:09:49,976
Yeah, you were.

261
00:09:51,178 --> 00:09:52,579
We had a good relationship,
right?

262
00:09:52,647 --> 00:09:54,214
God, I think so.

263
00:09:54,282 --> 00:09:57,718
I mean, we fought a lot.
I know. I know.

264
00:09:57,786 --> 00:09:59,887
I know that.
Let me ask you something.

265
00:09:59,955 --> 00:10:01,689
- Mm-hmm.
- I'm hot.

266
00:10:01,756 --> 00:10:02,923
Is that a question?

267
00:10:02,991 --> 00:10:05,192
- Um, when we were together--
I can't see.

268
00:10:05,260 --> 00:10:06,493
Here.

269
00:10:06,561 --> 00:10:07,661
- When we were together--
- Yes.

270
00:10:07,729 --> 00:10:09,829
Were there times
that you were like,

271
00:10:09,897 --> 00:10:12,398
"if I don't see Ann
in, like, ten seconds,

272
00:10:12,466 --> 00:10:13,733
I'm gonna die"?

273
00:10:13,800 --> 00:10:16,101
Yes. All the time.

274
00:10:16,169 --> 00:10:17,569
I loved Andy.

275
00:10:17,637 --> 00:10:19,070
Loved him.

276
00:10:19,138 --> 00:10:20,705
<i>Loved Andy.</i>

277
00:10:20,772 --> 00:10:24,041
He was a totally helpless baby
when we met.

278
00:10:24,109 --> 00:10:25,909
I dated him for three years.

279
00:10:25,977 --> 00:10:29,379
Now he's an adult with a job.

280
00:10:29,447 --> 00:10:33,283
And some other girl
is gonna reap the rewards

281
00:10:33,350 --> 00:10:36,519
Of my hard work?

282
00:10:36,587 --> 00:10:38,588
That's bull[bleep].

283
00:10:42,860 --> 00:10:45,428
I'm reading this, and I'm like,

284
00:10:45,496 --> 00:10:48,197
How could they
hurt those gorillas like that?

285
00:10:48,265 --> 00:10:51,967
They're such gentle,
magnificent creatures, you know?

286
00:10:52,035 --> 00:10:53,769
I'm an animal lover,
I don't know.

287
00:10:53,836 --> 00:10:57,305
Hey, I, um, want you to keep
this bottlecap.

288
00:10:57,372 --> 00:10:59,106
So you'll always remember me

289
00:10:59,174 --> 00:11:01,909
And the first time we met.

290
00:11:01,977 --> 00:11:04,645
Classic game.
Plant the seed.

291
00:11:04,713 --> 00:11:06,080
Harvest like a half hour later.

292
00:11:06,147 --> 00:11:09,717
Here you go.
Sorry that took forever.

293
00:11:09,785 --> 00:11:11,920
God.

294
00:11:11,988 --> 00:11:14,890
Hey, baby, you miss me?
'cause I missed you.

295
00:11:14,957 --> 00:11:16,058
Oh, look at this.
John-Ralphio.

296
00:11:16,125 --> 00:11:17,692
Oh, hey. Andy.

297
00:11:17,760 --> 00:11:18,794
Andy.
That's an all right name.

298
00:11:18,861 --> 00:11:20,128
That's an all right name.

299
00:11:20,196 --> 00:11:22,398
- Thank you.
- Yeah.

300
00:11:22,465 --> 00:11:25,201
- Are you guys--are you guys
hanging out together?

301
00:11:25,269 --> 00:11:26,602
Yeah, uh, maybe.

302
00:11:26,670 --> 00:11:28,004
You know, he asked me out
so many times.

303
00:11:28,071 --> 00:11:29,338
I guess he wore me down.

304
00:11:29,406 --> 00:11:31,340
I'm very persistent.

305
00:11:31,407 --> 00:11:32,541
Come over here.
Come over here.

306
00:11:32,608 --> 00:11:34,142
- I can hear you. I can--
- Come over here.

307
00:11:34,210 --> 00:11:35,877
One time, I waited outside
a woman's house

308
00:11:35,945 --> 00:11:38,613
For five days just to show her
how serious I was

309
00:11:38,680 --> 00:11:39,947
About wanting to drill her.

310
00:11:40,015 --> 00:11:41,815
Turns out,
it was the wrong house.

311
00:11:41,883 --> 00:11:43,283
She loved the story anyway.

312
00:11:43,350 --> 00:11:44,817
We got to third base.
Over the pants.

313
00:11:44,885 --> 00:11:46,986
- That's so cute.
- Yeah, super cute.

314
00:11:47,054 --> 00:11:48,321
- Wow.
- Right?

315
00:11:48,389 --> 00:11:49,956
Do you want a Remy Martin?
Probably good.

316
00:11:50,024 --> 00:11:51,291
Okay, I'll get you one.

317
00:11:51,359 --> 00:11:54,361
Two Remy Martins.
Here we go.

318
00:11:56,464 --> 00:11:57,665
You can have my Remy Martin.

319
00:11:57,732 --> 00:12:01,669
For your birthday.

320
00:12:01,737 --> 00:12:03,238
I thought she liked me.

321
00:12:03,306 --> 00:12:06,308
I guess I'm super bad
at picking up signals.

322
00:12:08,311 --> 00:12:11,346
But that Ralph Macchio guy's
a total douche.

323
00:12:11,414 --> 00:12:14,650
Did I give you a bottlecap?

324
00:12:14,717 --> 00:12:16,852
Excuse me.
Did I give you a bottlecap?

325
00:12:16,920 --> 00:12:18,453
- What?
- Ugh!

326
00:12:18,521 --> 00:12:21,523
Did I give any of you guys
a bottlecap?

327
00:12:21,591 --> 00:12:24,392
You sure?

328
00:12:24,460 --> 00:12:26,027
Who the [bleep]
did I give a bottlecap to?

329
00:12:26,095 --> 00:12:27,996
♪ fast money, fast cars ♪

330
00:12:28,063 --> 00:12:30,398
♪ fast women ♪
♪ That's how I'm livin' ♪

331
00:12:30,466 --> 00:12:32,133
♪ fast money, fast cars ♪

332
00:12:32,201 --> 00:12:34,135
♪ fast women ♪
♪ That's how I'm livin' ♪

333
00:12:34,203 --> 00:12:36,437
This party sucks.
Let's get out of here.

334
00:12:36,505 --> 00:12:38,506
- It's my birthday party.
- It is?

335
00:12:38,574 --> 00:12:41,142
- Yes.
- I'm sorry, boo.

336
00:12:41,210 --> 00:12:42,810
- You know what's thirsty?
- What?

337
00:12:42,878 --> 00:12:44,378
You know what's weird?

338
00:12:44,446 --> 00:12:45,780
How thirsty I get
when I'm weird.

339
00:12:45,848 --> 00:12:47,749
- Mm-hmm.
- When I'm drunk.

340
00:12:47,816 --> 00:12:50,018
Ben the jerk is here.

341
00:12:50,085 --> 00:12:51,620
- Who are?
- That one.

342
00:12:51,688 --> 00:12:53,722
The one coming over here.
Get ready.

343
00:12:53,790 --> 00:12:55,991
Okay, Leslie, be professional.

344
00:12:56,059 --> 00:12:57,193
Hi.

345
00:12:57,261 --> 00:12:59,596
Hello, Ben.

346
00:12:59,663 --> 00:13:02,332
Um, look, uh,

347
00:13:02,400 --> 00:13:05,068
I kind of feel like
we got off on the wrong foot. G

348
00:13:05,136 --> 00:13:07,704
So I just wanted to stop by
and--

349
00:13:07,772 --> 00:13:09,740
- Yeah, well, save your breath,
okay?

350
00:13:09,807 --> 00:13:12,375
Just get out of here.

351
00:13:12,442 --> 00:13:14,176
Because this is a party
with my friends

352
00:13:14,244 --> 00:13:15,678
And you're trying to fire
all my friends.

353
00:13:15,745 --> 00:13:17,045
- I--

354
00:13:17,113 --> 00:13:18,679
- Plus I just talked to
everybody in this bar

355
00:13:18,747 --> 00:13:20,214
And nobody wants you here.

356
00:13:20,282 --> 00:13:23,084
- Um, okay, then I'll--
I'll just see you tomorrow.

357
00:13:23,151 --> 00:13:25,018
- Mm-hmm.
- Sorry to bother you.

358
00:13:25,086 --> 00:13:28,355
Get out of here.

359
00:13:28,423 --> 00:13:31,992
Leslie,
that was so professional.

360
00:13:32,059 --> 00:13:33,360
I'm so proud of you.

361
00:13:33,427 --> 00:13:34,761
Thank you.

362
00:13:37,734 --> 00:13:39,334
- You have to help me.
- Ow.

363
00:13:39,402 --> 00:13:42,604
I know, me too.
But you have to help me.

364
00:13:42,672 --> 00:13:44,673
I think I may have made out
with someone last night.

365
00:13:44,741 --> 00:13:48,244
Oh, no. Who?

366
00:13:48,311 --> 00:13:49,312
I don't remember.

367
00:13:49,379 --> 00:13:50,780
I don't even know
if it happened,

368
00:13:50,847 --> 00:13:52,682
I just had this feeling
when I woke up this morning

369
00:13:52,750 --> 00:13:54,350
Like I definitely
kissed somebody.

370
00:13:54,418 --> 00:13:55,351
- No--

371
00:13:55,419 --> 00:13:57,987
- Hey. Ann.

372
00:13:58,055 --> 00:13:59,222
Hmm.

373
00:13:59,289 --> 00:14:01,457
Oh, that was a crazy night
last night.

374
00:14:03,293 --> 00:14:06,195
Yeah, crazy.

375
00:14:06,263 --> 00:14:09,832
I mean,
I left at 11:15

376
00:14:09,899 --> 00:14:11,567
And you were still raging
pretty hard.

377
00:14:11,635 --> 00:14:14,703
Oh, I was. I was.

378
00:14:14,771 --> 00:14:16,705
Good.

379
00:14:16,773 --> 00:14:17,940
Yeah.
So it's not Jerry.

380
00:14:18,008 --> 00:14:19,342
Help me.

381
00:14:19,410 --> 00:14:20,977
I'll keep an ear to the ground.

382
00:14:21,045 --> 00:14:22,879
But right now, I have to go
swallow my pride.

383
00:14:22,946 --> 00:14:25,014
Mm, mm.

384
00:14:25,082 --> 00:14:26,549
Leslie, Leslie.
Welcome, welcome, welcome.

385
00:14:26,617 --> 00:14:27,950
Hi, you have
a lot of bottles there.

386
00:14:28,018 --> 00:14:29,185
Oh, yeah, would you like
a vitamin?

387
00:14:29,253 --> 00:14:32,255
B12? Evening primrose oil?
Willow bark? Magnesium?

388
00:14:32,323 --> 00:14:33,823
- No, thank you.
- You sure?

389
00:14:33,891 --> 00:14:35,692
Really good for hangovers.
Okay, I'll take one.

390
00:14:35,760 --> 00:14:38,462
I take care of my body
above all else.

391
00:14:38,529 --> 00:14:41,499
Diet, exercise,
supplements, positive thinking.

392
00:14:41,567 --> 00:14:43,234
Scientists believe that
the first human being

393
00:14:43,302 --> 00:14:46,271
Who will live 150 years
has already been born.

394
00:14:46,339 --> 00:14:48,974
I believe I am
that human being.

395
00:14:49,042 --> 00:14:51,043
So. What brings you here
so early?

396
00:14:51,111 --> 00:14:53,178
I'm here to speak to Ben,
actually.

397
00:14:53,246 --> 00:14:54,346
Great.

398
00:14:54,414 --> 00:14:55,614
I'm gonna listen to
some ocean sounds

399
00:14:55,682 --> 00:14:58,617
And so some chin-ups.

400
00:15:01,521 --> 00:15:03,922
So I'd like to apologize
for yesterday.

401
00:15:03,990 --> 00:15:06,024
Don't worry about it.

402
00:15:06,091 --> 00:15:08,359
Now what I did
was out of line. Twice.

403
00:15:08,427 --> 00:15:11,362
And I was worked up
because obviously,

404
00:15:11,429 --> 00:15:12,863
You represent a threat
to my department.

405
00:15:12,930 --> 00:15:14,631
Your city counsel
and your Mayor

406
00:15:14,699 --> 00:15:16,166
Are the threats to
your department.

407
00:15:16,233 --> 00:15:17,366
We didn't do anything

408
00:15:17,434 --> 00:15:19,234
To get you into
this situation, okay?

409
00:15:19,302 --> 00:15:20,536
Okay, look, Ben,

410
00:15:20,604 --> 00:15:22,404
I don't appreciate
your callous attitude, okay?

411
00:15:22,472 --> 00:15:23,906
- Really?
- Yeah, really.

412
00:15:23,974 --> 00:15:25,207
Okay.

413
00:15:25,275 --> 00:15:27,776
You may hold my fate
in your hands like a small bird,

414
00:15:27,844 --> 00:15:31,713
But I still think
you're an ass.

415
00:15:31,781 --> 00:15:33,115
You wanna get a beer?

416
00:15:33,183 --> 00:15:35,217
It's, like,
10:30 in the morning.

417
00:15:35,285 --> 00:15:36,719
Yeah. You seem like
you could use a beer.

418
00:15:36,786 --> 00:15:38,453
Let's get a beer.

419
00:15:38,521 --> 00:15:39,621
- Well--

420
00:15:39,689 --> 00:15:41,891
- Oh, oh, oh, oh.
Don't keep him too long.

421
00:15:41,958 --> 00:15:44,426
I need someone to be here
when I take my multivitamin.

422
00:15:44,494 --> 00:15:46,529
There's a choking hazard.

423
00:15:46,596 --> 00:15:49,065
Okay, can we go?

424
00:15:49,132 --> 00:15:51,233
Oh, my God.

425
00:15:51,301 --> 00:15:52,835
You don't remember anything

426
00:15:52,902 --> 00:15:53,969
That happened last night,
do you?

427
00:15:54,037 --> 00:15:55,771
- No.
- Wow.

428
00:15:55,838 --> 00:15:59,207
For once, it's Ann
who blacked out drunk

429
00:15:59,275 --> 00:16:00,375
And not Andy.

430
00:16:00,443 --> 00:16:01,910
- Okay.
- Oh, my gosh.

431
00:16:01,978 --> 00:16:02,977
This is--
that's really dangerous.

432
00:16:03,045 --> 00:16:04,212
Please tell me.

433
00:16:04,280 --> 00:16:05,747
This is going to be so fun

434
00:16:05,815 --> 00:16:08,383
Letting you dangle
until I finally reveal

435
00:16:08,450 --> 00:16:10,718
That we did not make out.

436
00:16:10,786 --> 00:16:12,119
Oh, [bleep].

437
00:16:12,187 --> 00:16:13,721
Damn it.

438
00:16:13,789 --> 00:16:16,090
Good. See ya.

439
00:16:16,158 --> 00:16:18,626
All right.

440
00:16:18,693 --> 00:16:21,695
Hey, I need to settle my bill
from last night.

441
00:16:21,763 --> 00:16:23,130
Tom Haverford.

442
00:16:23,197 --> 00:16:25,932
Okay.

443
00:16:26,000 --> 00:16:28,835
Wow, you had 47 drinks
last night?

444
00:16:28,903 --> 00:16:31,805
Ten cosmos,
eight Smirnoff ices,

445
00:16:31,873 --> 00:16:33,941
And everything else starts with
the word pomegranate.

446
00:16:34,008 --> 00:16:35,609
Aw. Was it
your bachelorette party?

447
00:16:35,677 --> 00:16:37,311
No.

448
00:16:37,378 --> 00:16:39,746
I invited a bunch of girls here
last night,

449
00:16:39,814 --> 00:16:42,783
And they all put drinks
on my tab.

450
00:16:42,851 --> 00:16:44,851
And then I went home alone.

451
00:16:44,919 --> 00:16:46,019
I'm not sure what happened.

452
00:16:46,087 --> 00:16:47,320
You invited
a bunch of girls here

453
00:16:47,388 --> 00:16:48,788
And then you tried to hit on
all of them.

454
00:16:48,856 --> 00:16:51,024
Fair enough.

455
00:16:51,092 --> 00:16:52,292
I mean, what were you
expecting was gonna happen,

456
00:16:52,359 --> 00:16:53,459
A 43-way?

457
00:16:53,527 --> 00:16:56,329
That would have been
a little out of control.

458
00:16:56,396 --> 00:16:57,530
- But awesome.
- Yeah.

459
00:16:57,597 --> 00:16:59,298
The problem is
I only have 15 penises.

460
00:16:59,366 --> 00:17:01,367
So there would have been
28 girls there

461
00:17:01,434 --> 00:17:02,868
That were really upset with me.

462
00:17:06,673 --> 00:17:10,776
All right, well, thanks.

463
00:17:13,180 --> 00:17:16,348
Hey, uh, would you wanna get
a drink tomorrow night?

464
00:17:18,084 --> 00:17:20,219
- Does it have to be here?
- No.

465
00:17:20,287 --> 00:17:22,121
Then sure.
I'm Lucy.

466
00:17:22,189 --> 00:17:25,024
Let you give me
my phone number.

467
00:17:25,092 --> 00:17:27,794
Mm.

468
00:17:27,862 --> 00:17:28,962
Wow, that tastes really good.

469
00:17:29,030 --> 00:17:31,464
Mm. How's your head?

470
00:17:31,532 --> 00:17:34,200
Mushy.

471
00:17:34,268 --> 00:17:35,835
I'm sorry that I yelled at you.

472
00:17:35,903 --> 00:17:37,136
All three times.

473
00:17:37,204 --> 00:17:38,838
But I don't think
you know anything

474
00:17:38,905 --> 00:17:40,172
About my department.

475
00:17:40,240 --> 00:17:42,908
Have you ever been part of
a government body before?

476
00:17:42,975 --> 00:17:44,943
Uh, I have. Yeah.

477
00:17:45,011 --> 00:17:47,312
In a small town
called partridge, Minnesota.

478
00:17:49,381 --> 00:17:51,983
Why does that sound familiar?

479
00:17:54,654 --> 00:17:56,655
You're Benji Wyatt?
I am.

480
00:17:56,723 --> 00:17:57,990
When I was 18,

481
00:17:58,058 --> 00:18:01,928
I ran for Mayor of
my small town and won.

482
00:18:01,996 --> 00:18:05,532
Little bit of anti-establishment
voter rebellion, I guess.

483
00:18:05,600 --> 00:18:08,202
Here's the thing, though,
about 18-year-olds.

484
00:18:08,270 --> 00:18:10,071
They're idiots.

485
00:18:10,138 --> 00:18:11,772
So I pretty much
ran the place into the ground

486
00:18:11,840 --> 00:18:15,809
After two months
and got impeached.

487
00:18:15,877 --> 00:18:18,411
Worst part was,
my parents grounded me.

488
00:18:18,479 --> 00:18:20,247
Oh, my God, you were so cute.

489
00:18:20,314 --> 00:18:21,248
Oh, well, thank you.

490
00:18:21,315 --> 00:18:22,249
What was that song you played

491
00:18:22,316 --> 00:18:23,584
At the swearing-in ceremony?

492
00:18:23,651 --> 00:18:27,220
<i>Whoomp! There it is
is what it was, yeah.</i>

493
00:18:27,288 --> 00:18:28,788
God, I was so jealous of you.

494
00:18:28,856 --> 00:18:29,989
You shouldn't have been.

495
00:18:30,057 --> 00:18:31,691
I mean, it ended up
kind of ruining my life.

496
00:18:31,759 --> 00:18:33,826
I mean,
now I'm balancing budgets

497
00:18:33,894 --> 00:18:35,294
So I can show people
I'm responsible.

498
00:18:35,361 --> 00:18:37,896
And so I can
run for office again someday

499
00:18:37,964 --> 00:18:40,966
And not be laughed at,
you know?

500
00:18:41,033 --> 00:18:42,600
I mean, you wanna
run for office someday, right?

501
00:18:42,668 --> 00:18:46,070
Yeah. How'd you know?

502
00:18:46,138 --> 00:18:47,271
Well, you have to be able
to make decisions

503
00:18:47,339 --> 00:18:48,406
Like this, Leslie.

504
00:18:48,473 --> 00:18:50,207
You have to be harsh.
You know?

505
00:18:50,275 --> 00:18:51,708
No one's gonna elect you
to do anything

506
00:18:51,776 --> 00:18:54,411
If you don't show that
you're a responsible grown-up.

507
00:18:54,478 --> 00:18:56,412
Yeah.

508
00:18:56,480 --> 00:18:57,713
Well.

509
00:18:57,781 --> 00:19:00,116
Oh, no, please,
Mr. Mayor. Let me.

510
00:19:00,183 --> 00:19:01,950
♪ whoomp! There it is ♪

511
00:19:02,018 --> 00:19:03,218
Okay.

512
00:19:03,285 --> 00:19:06,621
Mm.

513
00:19:06,689 --> 00:19:08,990
Mm.

514
00:19:09,058 --> 00:19:11,493
- Everything okay?
- Hmm?

515
00:19:11,561 --> 00:19:12,994
Mm.

516
00:19:13,062 --> 00:19:15,063
No.

517
00:19:25,675 --> 00:19:27,977
- You ready?
- Yeah.

518
00:19:35,318 --> 00:19:38,386
Hey, Ron,
did we make out last night?

519
00:19:38,454 --> 00:19:39,987
Good God, woman, no.

520
00:19:40,055 --> 00:19:41,789
Okay.
Well, that's it.

521
00:19:41,856 --> 00:19:43,290
That's all the men that I know.

522
00:19:43,358 --> 00:19:45,659
You guys just knocked it
completely out of the park.

523
00:19:45,727 --> 00:19:49,563
And I appreciate it
and we will see you later.

524
00:19:51,265 --> 00:19:54,001
Hey. Ann Perkins.

525
00:19:54,068 --> 00:19:58,206
- Hey.
- Hey. Well.

526
00:19:58,273 --> 00:20:00,541
Uh, April's party?
April's party?

527
00:20:00,609 --> 00:20:02,210
Yeah, hey, man.
How's it going?

528
00:20:02,277 --> 00:20:04,779
- Hey.
- Hey.

529
00:20:04,847 --> 00:20:05,947
Come here.

530
00:20:06,015 --> 00:20:07,783
There's no reason
not to love him,

531
00:20:07,851 --> 00:20:08,984
But I--I didn't.

532
00:20:09,052 --> 00:20:14,189
♪ highway to the danger zone ♪

533
00:20:14,257 --> 00:20:15,824
I'm calling you a cab.

534
00:20:15,892 --> 00:20:17,894
- You know what,
I just--

535
00:20:17,961 --> 00:20:22,231
- Mm.

536
00:20:22,299 --> 00:20:24,333
Mm-mm-mm, mm-mm-mm.

537
00:20:24,401 --> 00:20:26,635
Mm.

538
00:20:26,703 --> 00:20:27,736
Okay, let's get in the cab.

539
00:20:27,804 --> 00:20:30,505
Go ahead.
Boo.

540
00:20:30,573 --> 00:20:31,573
Fancy this.

541
00:20:31,641 --> 00:20:34,109
Um, listen,
I have meetings all day,

542
00:20:34,176 --> 00:20:36,411
But I'd love to chat with you;
Can I call you?

543
00:20:36,479 --> 00:20:38,881
You have my phone number?

544
00:20:38,948 --> 00:20:41,149
No, you couldn't remember
your phone number.

545
00:20:41,217 --> 00:20:42,317
But you gave me your phone.

546
00:20:42,385 --> 00:20:44,052
Terrific.
That's great.

547
00:20:44,119 --> 00:20:45,186
We'll talk later.

548
00:20:45,254 --> 00:20:47,355
Okay.
♪ danger zone ♪

549
00:20:47,423 --> 00:20:50,625
Come on, guys.

550
00:20:50,693 --> 00:20:52,661
We'll talk later.

551
00:20:52,728 --> 00:20:57,333
This suggested plan reduces
our overall budget by 35%.

552
00:20:57,400 --> 00:20:59,402
And it contains very practical
deep cuts

553
00:20:59,469 --> 00:21:01,104
In many of our services.

554
00:21:01,171 --> 00:21:03,340
Uh, Leslie, this is amazing.

555
00:21:03,407 --> 00:21:04,541
But it's moot.

556
00:21:04,609 --> 00:21:05,576
Why?

557
00:21:05,643 --> 00:21:07,945
Our investigation has revealed

558
00:21:08,013 --> 00:21:10,247
That things in Pawnee
are much worse

559
00:21:10,314 --> 00:21:11,348
Than we anticipated.

560
00:21:11,416 --> 00:21:12,950
Meaning what?

561
00:21:13,017 --> 00:21:14,784
Well, effective
tomorrow morning,

562
00:21:14,852 --> 00:21:15,885
The entire government
will be shut down

563
00:21:15,953 --> 00:21:20,290
Until further notice.

564
00:21:20,358 --> 00:21:21,525
I'm sorry,
I just started hearing

565
00:21:21,592 --> 00:21:23,226
Really loud circus music
in my head.

566
00:21:23,294 --> 00:21:24,495
What did you say?

567
00:21:26,317 --> 00:21:29,186
♪ November,
your bangs are cute ♪

568
00:21:29,254 --> 00:21:32,490
♪ November,
your voice is a flute ♪

569
00:21:32,558 --> 00:21:34,225
♪ November ♪

570
00:21:34,293 --> 00:21:40,099
♪ let's pretend the sky
is for us ♪

571
00:21:40,167 --> 00:21:45,071
♪ let's spread our wings
and fly on a date ♪

572
00:21:45,139 --> 00:21:48,574
♪ I wanna go on a date
with you ♪

573
00:21:48,642 --> 00:21:50,609
♪ November ♪

574
00:21:50,677 --> 00:21:52,544
Sync by kuniva for addic7ed.com

575
00:21:52,612 --> 00:21:56,247
That song is about April.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
