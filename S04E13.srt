﻿1
00:00:00,406 --> 00:00:02,516
* a reduction in taxes

2
00:00:02,769 --> 00:00:04,103
For the small-business owner

3
00:00:04,137 --> 00:00:06,538
as well as a 7% surcharge

4
00:00:06,573 --> 00:00:10,109
on all nonlocal
business-related development.

5
00:00:10,143 --> 00:00:14,012
Together we will build
a better pawnee.

6
00:00:14,030 --> 00:00:16,615
So what we're looking for here

7
00:00:16,650 --> 00:00:19,485
are your overall impressions
of this candidate.

8
00:00:19,519 --> 00:00:22,204
What do you like about her?
What don't you like about her?

9
00:00:22,238 --> 00:00:24,623
Her ideas, her voice,
her clothes probably.

10
00:00:24,658 --> 00:00:26,892
Well, my campaign
has a little momentum,

11
00:00:26,926 --> 00:00:30,362
so we're trying to capitalize
on that by doing a focus group.

12
00:00:30,397 --> 00:00:33,865
Some things are very helpful.
Other things are not so helpful.

13
00:00:33,883 --> 00:00:37,836
All the things make me feel
a lot of feelings about myself.

14
00:00:37,870 --> 00:00:39,905
She's kinda short,
don't you think?

15
00:00:39,939 --> 00:00:41,206
Aggressively short almost.

16
00:00:41,224 --> 00:00:42,341
It's like she's throwing it
in my face.

17
00:00:42,375 --> 00:00:43,676
Insightful.

18
00:00:43,710 --> 00:00:46,478
You, sir, said you would not
vote for her, correct?

19
00:00:46,513 --> 00:00:49,181
Yeah. Yeah,
she seems a little uptight.

20
00:00:49,215 --> 00:00:50,566
She doesn't seem like

21
00:00:50,600 --> 00:00:53,652
the kind of person you could go
bowling with, you know?

22
00:00:53,687 --> 00:00:54,653
What?
That's ridiculous.

23
00:00:54,688 --> 00:00:57,356
I am excellent at bowling.
Ask Ron.

24
00:00:57,390 --> 00:00:59,425
Leslie, I know it's tough
to hear these things,

25
00:00:59,459 --> 00:01:02,244
but just try not to get
obsessed over one comment.

26
00:01:02,278 --> 00:01:04,863
You're totally right.
I'm not obsessing.

27
00:01:04,897 --> 00:01:06,064
Okay, what aou doing?

28
00:01:06,082 --> 00:01:07,533
I'm just reminding myself
to tell Ron

29
00:01:07,567 --> 00:01:08,751
to tell you how good I am
at bowling.

30
00:01:08,785 --> 00:01:09,901
All right.

31
00:01:09,919 --> 00:01:11,370
I actually thought
that she was smart,

32
00:01:11,404 --> 00:01:13,839
and I'd vote for her.
There you go.

33
00:01:13,873 --> 00:01:16,592
Although I once knew a Leslie,
and, uh, she was just awful.

34
00:01:16,626 --> 00:01:18,877
So now I hate all Leslies.

35
00:01:18,912 --> 00:01:21,046
Would she consider
changing her name?

36
00:01:21,073 --> 00:01:22,675
[Triumphant music]

37
00:01:22,689 --> 00:01:24,767
Sync and Corrections by <font color="#ffff00">APOLLO</font>
<font color="#00ff00"> www.addic7ed.com</font>

38
00:01:24,784 --> 00:01:30,777
♪

39
00:01:41,267 --> 00:01:43,202
Okay, here we go.

40
00:01:43,236 --> 00:01:45,471
71% said that you have
a strong command of the issues.

41
00:01:45,505 --> 00:01:48,440
That is good, but only 33%

42
00:01:48,475 --> 00:01:50,209
said that they would consider
voting for you.

43
00:01:50,243 --> 00:01:51,377
Mm, yeah,
that's really interesting stuff.

44
00:01:51,411 --> 00:01:52,444
Why did that guy say that he

45
00:01:52,479 --> 00:01:53,612
wouldn't want to go
bowling with me?

46
00:01:53,646 --> 00:01:55,748
You really have to let that go.

47
00:01:55,782 --> 00:01:57,483
I've never been very good
at letting things go.

48
00:01:57,517 --> 00:02:01,019
I can't tell you how many times
a fun tug-of-war with a dog

49
00:02:01,054 --> 00:02:03,288
over a chew toy
turned contentious.

50
00:02:03,323 --> 00:02:05,758
One of us always ends up mad.

51
00:02:05,792 --> 00:02:07,643
I did some research too about
the guy who said the thing

52
00:02:07,677 --> 00:02:09,645
about me and bowling,
and his name is Derek.

53
00:02:09,679 --> 00:02:11,063
Oh, my God.

54
00:02:11,097 --> 00:02:12,964
And get this... he's not
even that good of a bowler.

55
00:02:12,982 --> 00:02:14,132
His average is 132,

56
00:02:14,150 --> 00:02:15,768
according to the most recent
available data.

57
00:02:15,802 --> 00:02:18,637
Leslie, that comment

58
00:02:18,655 --> 00:02:21,106
wasn't really about whether
you're good at bowling...

59
00:02:21,141 --> 00:02:23,409
- which I am. Ask Ron.
- Which we all know you are.

60
00:02:23,443 --> 00:02:24,943
But some people
have the impression

61
00:02:24,978 --> 00:02:27,446
that you're a little elitist
or intellectual or something.

62
00:02:27,480 --> 00:02:29,948
That is so sexist.
It's just because I'm a woman.

63
00:02:29,983 --> 00:02:32,551
Would they deign
to say such things

64
00:02:32,585 --> 00:02:34,720
to Woodrow Wilson
or Benjamin Disraeli...

65
00:02:34,754 --> 00:02:36,288
okay, I see what
you're talking about.

66
00:02:36,322 --> 00:02:37,322
See, a lot of people
don't vote with their brains.

67
00:02:37,340 --> 00:02:38,690
They vote with their guts.

68
00:02:38,725 --> 00:02:40,092
I know you're fun,
and you can have a good time.

69
00:02:40,126 --> 00:02:42,461
The public needs to see that.

70
00:02:42,495 --> 00:02:46,298
Well, then maybe the campaign
should host a bowling night.

71
00:02:46,332 --> 00:02:48,133
- Hmm.
- It would be laid-back.

72
00:02:48,168 --> 00:02:49,935
Everyone having fun,
great photo op.

73
00:02:49,969 --> 00:02:51,103
That could actually work.

74
00:02:51,137 --> 00:02:52,204
Also I'm really good
at bowling.

75
00:02:52,238 --> 00:02:53,856
No, I don't think
I believe you.

76
00:02:53,890 --> 00:02:54,973
Is there anyone I can ask?

77
00:02:55,008 --> 00:02:56,608
Yeah.
Ask Ron.

78
00:02:56,643 --> 00:02:59,778
Oh, okay.
You're kidding.

79
00:02:59,813 --> 00:03:01,346
All right, it is time

80
00:03:01,381 --> 00:03:04,032
for the 12th Leslie Knope
Fundraising Phone Bank.

81
00:03:04,067 --> 00:03:05,717
Is everybody feeling good?

82
00:03:05,752 --> 00:03:07,736
Oh, I don't know, Jerry.
It's Sunday night,

83
00:03:07,787 --> 00:03:09,354
I'm making phone calls
to strangers,

84
00:03:09,389 --> 00:03:11,323
and you're in my house.

85
00:03:11,357 --> 00:03:12,658
My life couldn't be worse.

86
00:03:12,692 --> 00:03:13,992
Hey!

87
00:03:14,027 --> 00:03:15,694
Let's get
this phone party started.

88
00:03:15,728 --> 00:03:17,863
- No.
- Sorry I'm late, guys.

89
00:03:17,897 --> 00:03:19,197
I know this kind of thing

90
00:03:19,215 --> 00:03:20,916
can be a grind,
but Ben said that this

91
00:03:20,967 --> 00:03:23,702
is the most important thing
we can do to help Leslie.

92
00:03:23,736 --> 00:03:25,871
So tonight is going to be fun.

93
00:03:25,905 --> 00:03:28,974
You're probably thinking,
how could it possibly be fun?

94
00:03:29,008 --> 00:03:30,676
[Laughter]

95
00:03:30,710 --> 00:03:33,679
- No one's thinking that.
- I kinda was.

96
00:03:33,713 --> 00:03:36,014
I was not.

97
00:03:36,048 --> 00:03:37,916
Why are we laughing?

98
00:03:37,951 --> 00:03:40,385
Well, anyway, tonight
we are gonna spice things up

99
00:03:40,420 --> 00:03:41,887
with a little competition.

100
00:03:41,921 --> 00:03:43,856
Whoever raises the most money
by the end of the night

101
00:03:43,890 --> 00:03:47,860
will win two free movie passes
to the pawnee mono-plex.

102
00:03:47,894 --> 00:03:49,695
You really think
that's gonna motivate people...

103
00:03:49,729 --> 00:03:51,947
oh, my God.

104
00:03:51,998 --> 00:03:55,284
I could use that for a romantic
night with Millicent Gergich.

105
00:03:55,335 --> 00:03:56,902
Out of my way, suckers.

106
00:03:56,936 --> 00:03:59,204
As city manager,
I play no favorites.

107
00:03:59,239 --> 00:04:00,572
But as a private citizen,

108
00:04:00,590 --> 00:04:03,008
I am free to support
whomever I choose,

109
00:04:03,042 --> 00:04:05,043
and I choose to support
team Knope

110
00:04:05,077 --> 00:04:07,813
'cause they're the best.
Everybody's the best.

111
00:04:07,847 --> 00:04:11,917
We're all winners.

112
00:04:11,951 --> 00:04:13,452
- Do I look laid-back?

113
00:04:13,486 --> 00:04:16,254
- You look like you're trying
to look laid-back.

114
00:04:16,272 --> 00:04:17,990
How about now?

115
00:04:18,024 --> 00:04:20,559
I think just... maybe just stand
how you would normally stand.

116
00:04:20,593 --> 00:04:22,828
Okay.

117
00:04:22,862 --> 00:04:25,063
I forget now.
Hey, Ron.

118
00:04:25,097 --> 00:04:27,399
Thanks for coming, man.
Of course.

119
00:04:27,433 --> 00:04:30,335
This bowling alley has my
favorite restaurant in pawnee.

120
00:04:35,909 --> 00:04:37,776
Really?
You're not scared to eat here?

121
00:04:37,810 --> 00:04:41,713
When I eat,
it is the food that is scared.

122
00:04:41,748 --> 00:04:43,048
Why are you sitting weird?

123
00:04:43,082 --> 00:04:46,117
- Damn it.
- Who's ready to bowl so hard?

124
00:04:46,135 --> 00:04:48,887
Tommy's new bowling swag.

125
00:04:48,922 --> 00:04:50,389
You know what?
When you do it,

126
00:04:50,423 --> 00:04:51,757
you really do it.

127
00:04:51,791 --> 00:04:53,175
Good for you, man.

128
00:04:53,226 --> 00:04:55,761
Hello, I'm calling on behalf
of Leslie Knope,

129
00:04:55,795 --> 00:04:57,563
who is running
for city council.

130
00:04:57,597 --> 00:05:00,432
Is this Deirdre Splatterfork?

131
00:05:00,466 --> 00:05:01,633
That is literally
the most beautiful name

132
00:05:01,668 --> 00:05:03,018
I have ever heard.

133
00:05:03,069 --> 00:05:06,238
No, I don't think we can
accept donations over $50.

134
00:05:06,272 --> 00:05:07,272
I don't really know
why you would need

135
00:05:07,307 --> 00:05:09,341
my social security number,

136
00:05:09,375 --> 00:05:10,993
but... well, yeah, no.
You're right.

137
00:05:11,027 --> 00:05:13,328
I guess it couldn't hurt
to give it to you. It's 210...

138
00:05:13,363 --> 00:05:15,414
absolutely nothing.

139
00:05:15,448 --> 00:05:18,000
[Chuckles]
What are you wearing?

140
00:05:18,034 --> 00:05:19,918
Fine, if you don't wanna
donate, then don't.

141
00:05:19,953 --> 00:05:21,253
Oh, by the way...

142
00:05:21,287 --> 00:05:23,672
[Creepy voice] I'm calling
from inside your house.

143
00:05:23,706 --> 00:05:27,125
- Ha ha! $100.
- Whoa.

144
00:05:27,159 --> 00:05:30,712
This is unbelievable.
I cannot be stopped.

145
00:05:30,763 --> 00:05:32,664
♪ Come on now get some money ♪

146
00:05:32,699 --> 00:05:34,433
♪ come on, April
get an earpiece ♪

147
00:05:34,467 --> 00:05:35,834
♪ come on do it ♪

148
00:05:35,852 --> 00:05:37,302
I don't care about that prize,

149
00:05:37,337 --> 00:05:40,172
but I'm gonna win because I want
his happiness to go away.

150
00:05:40,206 --> 00:05:41,940
[Sighs]

151
00:05:41,975 --> 00:05:43,675
Type in "T Rex" for me.

152
00:05:43,693 --> 00:05:46,445
No, no, no.
Um, "t-boz."

153
00:05:46,479 --> 00:05:50,115
Wait, wait.
"Tommy Tsunami."

154
00:05:50,149 --> 00:05:51,533
No,
"ticky ticky Tom-Tom."

155
00:05:51,567 --> 00:05:53,902
No,
"fly guy."

156
00:05:53,953 --> 00:05:54,987
I wrote "tom."

157
00:05:55,021 --> 00:05:57,239
Classic.
Timeless.

158
00:05:57,290 --> 00:05:59,524
I love it.

159
00:05:59,559 --> 00:06:02,694
"Girl"?

160
00:06:02,712 --> 00:06:04,329
I saw your ad
from when you were a kid,

161
00:06:04,364 --> 00:06:05,831
but it's nice to have a beer
and get to know you.

162
00:06:05,865 --> 00:06:06,832
Aw, thanks, rich.

163
00:06:06,866 --> 00:06:09,001
Oh, here.
Smile.

164
00:06:09,035 --> 00:06:10,969
Maybe you'll get your picture
<i>the pawnee journal.</i>

165
00:06:11,004 --> 00:06:12,004
[Camera snaps]

166
00:06:12,038 --> 00:06:13,205
Hey, this is going really well.

167
00:06:13,239 --> 00:06:14,806
Pretty decent turnout.

168
00:06:14,841 --> 00:06:17,409
Yeah, you know, I feel very,
you know, casual, relaxed.

169
00:06:17,443 --> 00:06:21,313
Hey, is that the guy from
the... from the focus group?

170
00:06:21,347 --> 00:06:23,565
Huh.
I don't know.

171
00:06:23,599 --> 00:06:25,384
- I think it is.
- Is it?

172
00:06:25,418 --> 00:06:27,152
Excuse me, sir,
would you mind telling me

173
00:06:27,186 --> 00:06:29,221
how you heard
about the event tonight?

174
00:06:29,255 --> 00:06:31,823
Oh, uh, I got
a weird invitation in the mail.

175
00:06:31,858 --> 00:06:34,192
I didn't realize we were
sending out invitations.

176
00:06:34,226 --> 00:06:35,694
Oh.

177
00:06:35,728 --> 00:06:39,031
Hey, you're that girl from
the focus group thing I did.

178
00:06:39,065 --> 00:06:40,766
Oh, I wouldn't know.

179
00:06:40,800 --> 00:06:42,768
I'm never on the other side
of the glass with those things.

180
00:06:42,802 --> 00:06:44,086
Can I talk to you for a sec?

181
00:06:44,120 --> 00:06:48,407
Yep.

182
00:06:48,441 --> 00:06:50,575
Straight down the middle.

183
00:06:50,593 --> 00:06:53,679
No hook, no spin, no fuss.

184
00:06:53,713 --> 00:06:58,417
Anything more
and this becomes figure skating.

185
00:06:58,451 --> 00:07:00,102
Come on, big girl.

186
00:07:00,136 --> 00:07:03,689
Let's knock
these little pins down.

187
00:07:06,642 --> 00:07:08,393
Oh, my God, are you serious?

188
00:07:08,427 --> 00:07:11,430
Son, people can see you.

189
00:07:19,989 --> 00:07:22,574
Boom!
[Claps]

190
00:07:22,608 --> 00:07:25,444
That right there...
Tommy's strike.

191
00:07:25,478 --> 00:07:27,612
Leslie, what happened
to the big picture?

192
00:07:27,630 --> 00:07:28,964
I'm thinking
about the big picture.

193
00:07:28,998 --> 00:07:30,115
I'm trying to make
the picture bigger

194
00:07:30,133 --> 00:07:31,416
by including this guy in it.

195
00:07:31,451 --> 00:07:34,419
I can't do anything about
my gender or my height,

196
00:07:34,454 --> 00:07:38,924
but I can win that guy over
with a fun round of bowling.

197
00:07:38,958 --> 00:07:41,059
We need everyone here to see a
different side of Leslie Knope,

198
00:07:41,094 --> 00:07:42,461
not one specific person.

199
00:07:42,495 --> 00:07:44,262
Some people just
aren't gonna like you.

200
00:07:44,296 --> 00:07:46,965
Let it go.
Okay, I hear you.

201
00:07:46,983 --> 00:07:49,901
I'll treat him
just like everybody else.

202
00:07:49,936 --> 00:07:52,137
I'll just say hi to him,
shake his hand,

203
00:07:52,155 --> 00:07:54,156
buy a him a few beers,

204
00:07:54,190 --> 00:07:56,324
share a few laughs,
bowl a few frames,

205
00:07:56,359 --> 00:07:57,909
lose intentionally
to make him feel good,

206
00:07:57,944 --> 00:07:59,044
friend him on Facebook.

207
00:07:59,078 --> 00:08:02,214
And by the end of the night,
he will be mine.

208
00:08:05,479 --> 00:08:08,305
Whoa, "thar" she bowls.

209
00:08:08,323 --> 00:08:10,491
I'm Leslie Knope.

210
00:08:10,525 --> 00:08:11,942
You wanna bowl?
You wanna bowl together?

211
00:08:11,976 --> 00:08:12,943
You wanna be, like,
bowl buddies?

212
00:08:12,977 --> 00:08:13,978
Sure.

213
00:08:14,012 --> 00:08:16,530
Great,
just a casual game, you know?

214
00:08:16,581 --> 00:08:19,250
No biggie.
You okay?

215
00:08:19,284 --> 00:08:22,053
My sister has scoliosis, and I
think you might, you know...

216
00:08:22,087 --> 00:08:23,921
Oh, no. No, I'm just...
I'm just being cas.

217
00:08:23,955 --> 00:08:25,656
What are you rocking?
A 15 pounder?

218
00:08:25,674 --> 00:08:28,259
- 16.
- Whoa, hey, good for you.

219
00:08:28,293 --> 00:08:30,594
Oh, there they are. I just
ordered some wings for us.

220
00:08:30,629 --> 00:08:31,929
You like wings?

221
00:08:31,963 --> 00:08:33,997
- Love them.
- Really? Who knew?

222
00:08:34,016 --> 00:08:35,570
I knew.

223
00:08:42,641 --> 00:08:43,708
Damn it.

224
00:08:43,742 --> 00:08:45,309
Why are you mad?
You bowled a strike.

225
00:08:45,343 --> 00:08:47,912
That's how I motivate myself,
you know?

226
00:08:47,946 --> 00:08:49,196
Never good enough.
[Clears throat]

227
00:08:49,231 --> 00:08:51,032
Let me get you a beer.
Cool.

228
00:08:51,066 --> 00:08:55,119
These beers are as cold
as the Tuktoyaktuk Winter Road.

229
00:08:55,153 --> 00:08:57,555
You watch <i>Ice Road Truckers?</i>

230
00:08:57,589 --> 00:09:00,357
Yeah, dude.
It's my guilty pleasure.

231
00:09:00,375 --> 00:09:03,394
Tom, I'm asking you as a man
to stop this immediately.

232
00:09:12,370 --> 00:09:15,339
- Boom!
- What the [Bleep]?

233
00:09:15,373 --> 00:09:18,142
Hello,
is this Mrs. Gallivan?

234
00:09:18,176 --> 00:09:21,612
Well, my name is April,
and wouldn't you know it,

235
00:09:21,646 --> 00:09:24,915
I'm raising money for a city
council candidate I believe in.

236
00:09:24,950 --> 00:09:26,917
No, <i>mira, mira, mira,</i>
<i>mira, mira.</i>

237
00:09:26,952 --> 00:09:28,619
It's, like, whatever you want.

238
00:09:28,653 --> 00:09:29,987
Like, $10.
It don't matter.

239
00:09:30,021 --> 00:09:31,489
[Southern accent] Well,
I reckon it's just like

240
00:09:31,523 --> 00:09:33,991
grammy Martha told me
and my cousins,

241
00:09:34,025 --> 00:09:36,727
"you can't eat the biscuits
if you don't pay for the flour."

242
00:09:36,745 --> 00:09:39,530
[Laughs, mouths words]

243
00:09:39,564 --> 00:09:42,767
Put the phone down,
take a deep breath,

244
00:09:42,801 --> 00:09:45,286
and then you
are going to tell Steven

245
00:09:45,337 --> 00:09:49,306
that you will be treated
with respect, okay?

246
00:09:49,341 --> 00:09:51,742
And thank you
for your donation.

247
00:09:51,760 --> 00:09:53,844
Well, gee, I don't know, Fred.

248
00:09:53,879 --> 00:09:57,748
All I know is that
I just want to live in a world

249
00:09:57,783 --> 00:09:59,266
that's a better place,

250
00:09:59,301 --> 00:10:02,186
and your contribution will
definitely help us get there,

251
00:10:02,220 --> 00:10:05,856
to the...
[Sighs]

252
00:10:05,891 --> 00:10:10,728
Better-place world,
Fred.

253
00:10:10,762 --> 00:10:12,780
Yeehaw!

254
00:10:12,814 --> 00:10:15,866
Man, move over, pins.
There's a new Sheriff in town.

255
00:10:15,901 --> 00:10:19,570
You are so down with strikes,
they should call you Norma Rae.

256
00:10:19,604 --> 00:10:21,989
- Who's that?
- It's a...

257
00:10:22,040 --> 00:10:24,107
Sally Field movie about Unions.
Doesn't matter.

258
00:10:24,126 --> 00:10:25,409
The point is,
you're really good at this.

259
00:10:25,444 --> 00:10:27,912
It's your turn.
Try not to break a nail.

260
00:10:27,946 --> 00:10:30,498
Ah, classic Derek
with the Zings.

261
00:10:30,549 --> 00:10:32,299
Man, that's what bowling
is all about... hey.

262
00:10:32,334 --> 00:10:33,484
How's it going?

263
00:10:33,518 --> 00:10:34,969
- It's going really good.
- Oh, good.

264
00:10:35,003 --> 00:10:36,387
Um, we're just hanging out
and having a very good time.

265
00:10:36,421 --> 00:10:37,855
Okay, I just wanted
to remind you that

266
00:10:37,889 --> 00:10:40,558
when you're bowling, try not
to fixate on just one pin.

267
00:10:40,592 --> 00:10:43,828
You're trying to knock down
a lot of pins.

268
00:10:43,862 --> 00:10:46,063
I realize that,
but I will knock down this pin,

269
00:10:46,097 --> 00:10:47,731
and then this pin will knock
down all the other pins.

270
00:10:47,766 --> 00:10:48,899
- Really?
- Mm-hmm.

271
00:10:48,934 --> 00:10:51,168
Hey, could you get me
another one of these?

272
00:10:51,203 --> 00:10:52,803
I...
I don't work here.

273
00:10:52,838 --> 00:10:54,304
Classic Derek.

274
00:10:54,322 --> 00:10:56,440
I am taking
a probiotic tea break.

275
00:10:56,475 --> 00:10:57,942
And while that is happening,

276
00:10:57,976 --> 00:10:59,326
I thought I might share
some big news.

277
00:10:59,361 --> 00:11:03,047
I am going to ask Millicent
Gergich to move in with me.

278
00:11:03,081 --> 00:11:05,449
D-I... I mean, if that's okay
with you of course, Jerry.

279
00:11:05,483 --> 00:11:09,086
Uh, yeah.
[Chuckles] Of course. Sure.

280
00:11:09,120 --> 00:11:10,521
That's awesome, man.

281
00:11:10,555 --> 00:11:12,756
Hey, do you guys wanna live here
with us and Ben

282
00:11:12,791 --> 00:11:14,458
and champion,
the three-legged dog?

283
00:11:14,492 --> 00:11:17,595
- That is an amazing offer.
- Yes!

284
00:11:17,629 --> 00:11:19,797
But I think we're probably
gonna get our own place.

285
00:11:19,831 --> 00:11:21,765
I'm already working
with a Real Estate agent

286
00:11:21,800 --> 00:11:23,133
to find an octagonal house.

287
00:11:23,167 --> 00:11:25,436
I've done some reading,
and an octagon

288
00:11:25,470 --> 00:11:27,705
is the optimal shape for a home
in terms of energy flow.

289
00:11:27,739 --> 00:11:29,506
You know, Chris,
I hate to pull rank,

290
00:11:29,524 --> 00:11:31,141
but if you're going
to take a break,

291
00:11:31,175 --> 00:11:32,576
would you mind going
in the living room,

292
00:11:32,611 --> 00:11:35,012
just, you know, so it doesn't
disturb everybody else.

293
00:11:35,046 --> 00:11:37,948
Jerry, I love it
when you pull rank.

294
00:11:37,983 --> 00:11:39,116
You're being weird.
Why?

295
00:11:39,150 --> 00:11:41,218
Milli's gonna break up
with Chris.

296
00:11:41,253 --> 00:11:42,519
[Whispers] Shut up.

297
00:11:42,537 --> 00:11:43,521
Oh, my God,
that's gonna super weird

298
00:11:43,555 --> 00:11:45,055
when they move in together.

299
00:11:45,090 --> 00:11:48,542
Yeah, I don't know when, but
she is definitely gonna do it.

300
00:11:55,050 --> 00:11:58,052
Nice.

301
00:12:02,874 --> 00:12:04,642
Hey, Ron.
Were you trying to get a seven?

302
00:12:04,676 --> 00:12:06,343
Because if you were,
you did a great job.

303
00:12:06,377 --> 00:12:10,481
I am very angry right now.

304
00:12:10,515 --> 00:12:13,384
- [Whines]
- What?

305
00:12:13,418 --> 00:12:16,219
[Cries] My finger was in there.

306
00:12:16,238 --> 00:12:17,571
Ron crushed my finger.

307
00:12:17,606 --> 00:12:19,773
I think it might be broken.

308
00:12:19,824 --> 00:12:22,993
- Are you a female bird?
- It was an accident.

309
00:12:23,028 --> 00:12:25,763
Oh, my God,
it's already swollen.

310
00:12:25,797 --> 00:12:28,098
Tom, my God, do you have
any pride at all?

311
00:12:28,133 --> 00:12:30,701
You did this on purpose.
You're jealous of my gift.

312
00:12:30,735 --> 00:12:33,037
Okay, come on, Tweety Bird,
let's get you some ice.

313
00:12:33,071 --> 00:12:35,238
- It hurts.
- Come on.

314
00:12:35,257 --> 00:12:38,342
Well, congratulations, man.

315
00:12:38,376 --> 00:12:40,678
Fun match, fun game,
good times.

316
00:12:40,712 --> 00:12:42,279
Did you have fun?
Yeah.

317
00:12:42,314 --> 00:12:45,015
I'm not one to complain about
free beer and free bowling.

318
00:12:45,050 --> 00:12:46,416
See you around.
Okay.

319
00:12:46,434 --> 00:12:48,269
Uh, just one second.

320
00:12:48,303 --> 00:12:51,555
I'm running for city council,
as you know,

321
00:12:51,590 --> 00:12:54,024
and uh, just wondering,
do I have your vote?

322
00:12:54,059 --> 00:12:56,360
- No.
- [Chuckles] Derek.

323
00:12:56,394 --> 00:12:58,028
You old so-and-so.

324
00:12:58,063 --> 00:12:59,280
For reals,
can I count on your vote?

325
00:12:59,314 --> 00:13:01,966
Yeah, no, um, I don't think so.

326
00:13:02,000 --> 00:13:05,669
But we've been here
bowling all night,

327
00:13:05,704 --> 00:13:06,904
and we've been having fun.

328
00:13:06,938 --> 00:13:10,474
And you're still not
gonna vote for me? Why?

329
00:13:10,508 --> 00:13:13,744
- I don't like you, okay?
- Leslie, Leslie.

330
00:13:13,778 --> 00:13:15,779
Well, you're a crappy bowler,

331
00:13:15,797 --> 00:13:17,114
and I pretended to lose to you.

332
00:13:17,132 --> 00:13:19,133
Yeah, right.
I destroyed you.

333
00:13:19,167 --> 00:13:20,317
No, it's true.

334
00:13:20,352 --> 00:13:23,120
I am a really good bowler.
Ask Ron.

335
00:13:23,138 --> 00:13:24,722
I don't know who Ron is.

336
00:13:24,756 --> 00:13:26,223
But if you're so great,
let's play again.

337
00:13:26,257 --> 00:13:27,558
Good.
I'd love to.

338
00:13:27,592 --> 00:13:29,626
Tell you what, if I win,
I get your vote.

339
00:13:29,644 --> 00:13:31,895
If I win, you clean
my house for a month.

340
00:13:31,930 --> 00:13:33,314
- Done.
- Hey, Leslie.

341
00:13:33,348 --> 00:13:35,683
I'd like to introduce you
to my good friend, anyone else.

342
00:13:35,734 --> 00:13:36,984
Not now, Ben.

343
00:13:37,018 --> 00:13:39,236
I'd like to introduce
Derek's ass to my foot.

344
00:13:39,270 --> 00:13:41,271
Get me another beer.

345
00:13:41,305 --> 00:13:42,489
I don't work here.

346
00:13:42,524 --> 00:13:44,908
$20?
Thank you.

347
00:13:44,943 --> 00:13:46,994
Uh, you know, we're really
looking for donations

348
00:13:47,028 --> 00:13:49,747
more in the $10,000 range.

349
00:13:49,781 --> 00:13:51,415
- Hi, guys.
- Hey.

350
00:13:51,449 --> 00:13:53,283
Just one second.
Let me get off this call.

351
00:13:53,317 --> 00:13:58,088
Hi, I'm so sorry.
I'm back.

352
00:13:58,123 --> 00:14:01,208
[Clears throat]

353
00:14:01,259 --> 00:14:07,698
[Whistling]

354
00:14:07,732 --> 00:14:10,901
What, champion?
You need to go outside now?

355
00:14:10,935 --> 00:14:13,503
Come on.
That a boy.

356
00:14:13,521 --> 00:14:17,105
Sorry, he's...
hates awkward situations.

357
00:14:22,881 --> 00:14:26,817
Oh!

358
00:14:26,851 --> 00:14:28,869
- Terrible moonwalk.
- Don't care.

359
00:14:32,957 --> 00:14:34,524
Yeah, what's up now, huh?

360
00:14:34,542 --> 00:14:38,862
Aw, you got a spare.
That's so cute.

361
00:14:38,880 --> 00:14:40,497
Check out the scoreboard.

362
00:14:40,531 --> 00:14:44,168
So Millicent and I are going
to take a nice little stroll,

363
00:14:44,202 --> 00:14:45,569
and I'll be back soon.

364
00:14:45,603 --> 00:14:46,737
You know, if you want to take
the rest of the night off,

365
00:14:46,771 --> 00:14:47,971
you know, that would be fine.

366
00:14:48,006 --> 00:14:49,773
Well, I don't think
that will be necessary.

367
00:14:49,808 --> 00:14:52,576
Unless you also want
to go get dinner.

368
00:14:52,610 --> 00:14:54,845
Nope, this won't take too long.

369
00:14:54,879 --> 00:14:57,014
Chris, you might want
to take a jacket with you.

370
00:14:57,048 --> 00:14:59,216
It's about to get cold
out there.

371
00:14:59,250 --> 00:15:01,018
Thanks, Donna, but Millicent's
company will keep me warm.

372
00:15:01,052 --> 00:15:02,352
Take the jacket.

373
00:15:02,387 --> 00:15:05,689
I wished for his happiness
to go away.

374
00:15:05,724 --> 00:15:08,692
I might be a wizard.

375
00:15:10,695 --> 00:15:13,247
Hey, Derek.
What a defeat, man.

376
00:15:13,281 --> 00:15:15,499
I mean, I did not expect
to win by that much.

377
00:15:15,533 --> 00:15:17,835
But I warned you,
I am a good bowler.

378
00:15:17,869 --> 00:15:19,920
- Whatever.
- All joking aside,

379
00:15:19,954 --> 00:15:21,872
I wanna say
that I actually had fun.

380
00:15:21,906 --> 00:15:25,042
And I'm really looking forward
to your vote in the spring.

381
00:15:25,076 --> 00:15:27,127
Yeah, I'll just write
in "bitch."

382
00:15:27,178 --> 00:15:28,879
I'm sorry,
what did you just say?

383
00:15:28,913 --> 00:15:30,881
Just ignore him.
He's being a jerk.

384
00:15:30,915 --> 00:15:32,933
I said she's a bitch.

385
00:15:32,967 --> 00:15:35,252
- Oh, my God.
- What the hell?

386
00:15:35,270 --> 00:15:37,688
I'm so sorry.

387
00:15:37,722 --> 00:15:39,623
I'm so sorry.
That was awesome.

388
00:15:39,657 --> 00:15:43,393
I'm so sorry.

389
00:15:43,428 --> 00:15:46,613
Oh, look.
Here's that photo op you wanted.

390
00:15:46,648 --> 00:15:48,087
- Ow.
- Sorry.

391
00:15:48,556 --> 00:15:50,220
So... you know... that's
everything that happens

392
00:15:50,250 --> 00:15:52,135
so it was just what happened.
It was just like...

393
00:15:52,153 --> 00:15:53,970
[Imitates gunfire]

394
00:15:53,988 --> 00:15:55,689
- What's wrong with you?
- I don't know, I just...

395
00:15:55,740 --> 00:15:56,740
I have a lot of adrenaline
right now.

396
00:15:56,775 --> 00:15:59,276
Just take a few deep breaths,
okay?

397
00:15:59,310 --> 00:16:00,661
The guy said
he might press charges.

398
00:16:00,695 --> 00:16:02,162
Yeah, well, Randy,
I would like you to know

399
00:16:02,197 --> 00:16:03,614
that we will not
be pressing charges.

400
00:16:03,648 --> 00:16:05,165
Yeah, that really
wasn't an option.

401
00:16:05,200 --> 00:16:07,117
Come on, the guy
was being a total jerk.

402
00:16:07,151 --> 00:16:08,752
What Ben did was warranted

403
00:16:08,787 --> 00:16:10,587
and extremely awesome,
by the way.

404
00:16:10,622 --> 00:16:12,489
Did you write down
how awesome it was?

405
00:16:12,507 --> 00:16:13,857
When we write official reports,

406
00:16:13,892 --> 00:16:16,460
we refrain from using words
like "jerk" or "awesome."

407
00:16:16,494 --> 00:16:18,679
- Hey, here you go.
- Hey.

408
00:16:18,713 --> 00:16:20,597
Wow, you and me, huh?
Hurt "fingies."

409
00:16:20,632 --> 00:16:23,016
No, no,
two totally different injuries.

410
00:16:23,051 --> 00:16:25,853
Okay, you...
just hang in there, okay?

411
00:16:25,887 --> 00:16:27,955
Okay, love you too.
Bye-bye.

412
00:16:27,989 --> 00:16:30,674
Well, that was Milli.
It happened.

413
00:16:30,708 --> 00:16:32,009
She broke up with Chris.

414
00:16:32,043 --> 00:16:33,811
- Boo.
- Damn you, Jerry.

415
00:16:33,845 --> 00:16:36,029
It's not my fault.

416
00:16:36,064 --> 00:16:38,048
Well, I feel sorry for the man.

417
00:16:38,082 --> 00:16:40,117
Well, anyway, the fundraising
challenge is over,

418
00:16:40,151 --> 00:16:41,752
and the winner...
whoa.

419
00:16:41,786 --> 00:16:44,121
We have a surprise winner,
April.

420
00:16:44,155 --> 00:16:47,157
April wins two tickets
to the pawnee mono-plex.

421
00:16:47,192 --> 00:16:50,694
[Laughs] Yes.
That's my wife.

422
00:16:52,085 --> 00:16:54,030
Okay,
everything's settled here.

423
00:16:54,048 --> 00:16:55,833
Wanna head home?
No.

424
00:16:55,867 --> 00:16:58,702
We haven't finished yet.
Last frame, your turn.

425
00:16:58,736 --> 00:17:01,672
- He hurt his hand.
- Last frame, go.

426
00:17:01,706 --> 00:17:04,558
- Fine, I'll bowl one-handed,
like an idiot.

427
00:17:11,583 --> 00:17:14,401
Whoo!
Oh, come on.

428
00:17:14,435 --> 00:17:16,253
King Kong
ain't got nothing on me.

429
00:17:16,287 --> 00:17:18,071
Ow, my "fingie" still hurts.

430
00:17:18,106 --> 00:17:20,557
Oh, the bravery.
The perseverance.

431
00:17:20,575 --> 00:17:21,592
You're an American hero.

432
00:17:21,626 --> 00:17:23,610
And the best part is,
beautiful,

433
00:17:23,661 --> 00:17:26,747
you get to drive
the champion home.

434
00:17:26,781 --> 00:17:28,866
Well, the headline
I would've gone with is

435
00:17:28,900 --> 00:17:33,871
"no-strike-bowling bowler struck
by Knope's striking beau."

436
00:17:33,905 --> 00:17:35,005
Okay, one more time.

437
00:17:35,039 --> 00:17:36,807
Let me resign.

438
00:17:36,841 --> 00:17:38,275
It's the only thing
that truly protects you.

439
00:17:38,309 --> 00:17:40,244
- Non-starter.

440
00:17:40,278 --> 00:17:43,463
[Sighs] Okay.
Now when you take questions,

441
00:17:43,515 --> 00:17:46,016
I think you should be
brief and sincere.

442
00:17:46,050 --> 00:17:47,684
Don't try to justify
what I did.

443
00:17:47,719 --> 00:17:49,970
Just apologize again,
and stay on script.

444
00:17:50,021 --> 00:17:52,022
Are you sure that you don't need
more whipped cream?

445
00:17:52,056 --> 00:17:53,891
Not today.
I don't deserve it.

446
00:17:53,925 --> 00:17:58,695
I got us into this mess because
I was fixating on that guy.

447
00:17:58,730 --> 00:18:00,097
But this is ridiculous.

448
00:18:00,131 --> 00:18:01,798
I do need more whipped cream.
Ma'am.

449
00:18:01,833 --> 00:18:04,100
[Knock at door]

450
00:18:04,118 --> 00:18:05,619
Hi.

451
00:18:05,653 --> 00:18:08,956
April Ludgate.
How are you doing?

452
00:18:08,990 --> 00:18:12,793
Uh, whatever.
I'm fine.

453
00:18:12,827 --> 00:18:13,911
Um, how are you?

454
00:18:13,945 --> 00:18:16,797
Well, as you may know,
Millicent Gergich

455
00:18:16,831 --> 00:18:18,115
ended our relationship
last night,

456
00:18:18,149 --> 00:18:20,617
which was disappointing.

457
00:18:20,652 --> 00:18:23,487
But here's why

458
00:18:23,521 --> 00:18:27,057
it may be the greatest thing
that ever happened.

459
00:18:32,263 --> 00:18:36,166
Yeah, good point.

460
00:18:36,200 --> 00:18:38,068
Hadn't thought about it
that way.

461
00:18:38,102 --> 00:18:41,989
Um, here.

462
00:18:42,023 --> 00:18:43,156
Take these tickets.

463
00:18:43,191 --> 00:18:44,875
- Oh, no, no, no.
Y-you earned them.

464
00:18:44,909 --> 00:18:46,009
They're movie tickets, Chris.

465
00:18:46,044 --> 00:18:48,245
They're, like, 8 bucks.

466
00:18:48,279 --> 00:18:49,246
It's a gesture.

467
00:18:49,280 --> 00:18:50,364
There are three of them.

468
00:18:50,415 --> 00:18:51,415
I thought there were only two.

469
00:18:51,449 --> 00:18:53,250
Yeah, I know,
I bought another one

470
00:18:53,284 --> 00:18:55,886
because I thought
that maybe you, me, and Andy

471
00:18:55,920 --> 00:19:00,557
could go
to the movies sometime.

472
00:19:00,592 --> 00:19:02,793
Just take the stupid tickets.

473
00:19:02,827 --> 00:19:04,828
I'm just trying to be nice.

474
00:19:04,846 --> 00:19:09,366
- Thank you.
- There you go.

475
00:19:09,400 --> 00:19:12,469
Okay, bye.

476
00:19:12,503 --> 00:19:14,438
Leslie, are you going
to fire Ben Wyatt?

477
00:19:14,472 --> 00:19:16,106
Are you going
to suspend your campaign?

478
00:19:16,140 --> 00:19:18,525
I'd like to first start
by saying thank you for coming.

479
00:19:18,559 --> 00:19:20,310
And on behalf of Ben Wyatt

480
00:19:20,345 --> 00:19:22,279
and everyone involved
in my campaign,

481
00:19:22,313 --> 00:19:24,448
I'm very sorry
for what happened

482
00:19:24,482 --> 00:19:28,285
at the rock n' roll
bowling alley last night.

483
00:19:28,319 --> 00:19:31,722
You know what?
No, I'm not.

484
00:19:31,756 --> 00:19:32,956
[Scoffs] I...
I'm not sorry.

485
00:19:32,991 --> 00:19:36,359
This guy was drunk,
and he was aggressive.

486
00:19:36,377 --> 00:19:38,078
And he was rude,
and he was foul-mouthed.

487
00:19:38,129 --> 00:19:41,264
And he called me by my second
least favorite term for a woman,

488
00:19:41,299 --> 00:19:43,367
and my campaign manager
punched him.

489
00:19:43,401 --> 00:19:44,635
I do not condone violence,

490
00:19:44,669 --> 00:19:47,754
but I have to be honest,
it was awesome.

491
00:19:47,805 --> 00:19:51,475
And my campaign manager and I
made out a lot afterward.

492
00:19:51,509 --> 00:19:52,943
Ah, I probably shouldn't
have said that.

493
00:19:52,977 --> 00:19:55,178
But that's what happened.

494
00:19:55,213 --> 00:19:57,714
Derek hates me, and I don't
particularly like him.

495
00:19:57,749 --> 00:20:02,385
So what's the point,
right, Derek?

496
00:20:02,403 --> 00:20:04,438
I feel like you're being
kind of a bitch right now.

497
00:20:04,489 --> 00:20:06,823
See?
So I'm not going to apologize.

498
00:20:06,858 --> 00:20:09,026
And if people won't vote
for me because of that,

499
00:20:09,060 --> 00:20:10,327
well, there's nothing
I can do about it.

500
00:20:10,361 --> 00:20:11,995
But you should be warned.

501
00:20:12,030 --> 00:20:13,947
If you do not vote for me,

502
00:20:13,998 --> 00:20:16,566
my boyfriend might beat you up.

503
00:20:16,584 --> 00:20:18,335
Now if anyone has any questions

504
00:20:18,369 --> 00:20:22,405
about the issues facing our
city, I'm right here.

505
00:20:22,423 --> 00:20:25,242
Okay, what are your overall
impressions of this woman?

506
00:20:25,276 --> 00:20:28,762
What do you like about her?
What don't you like about her?

507
00:20:28,796 --> 00:20:29,913
- I like her.
- What do you like

508
00:20:29,931 --> 00:20:31,114
about her specifically?

509
00:20:31,149 --> 00:20:33,183
I don't know.
She's tough, I guess.

510
00:20:33,217 --> 00:20:34,417
I just like her.

511
00:20:34,435 --> 00:20:36,953
I like that that one guy
punched that other guy,

512
00:20:36,988 --> 00:20:39,639
and then I like
that she stood by him.

513
00:20:39,691 --> 00:20:41,391
See?
People vote with their gut.

514
00:20:41,425 --> 00:20:44,094
Yeah, that guy said
he didn't like my earrings.

515
00:20:44,112 --> 00:20:45,095
Go punch him.
Sure.

516
00:20:45,113 --> 00:20:46,113
[Chuckles]

517
00:20:47,048 --> 00:20:48,064
Here you go.
Lane eight.

518
00:20:48,622 --> 00:20:52,157
No, Lane 22.
The one at the very end.

519
00:20:58,608 --> 00:21:00,709
Son of a bitch.

520
00:21:00,744 --> 00:21:06,749
[Grunting]

521
00:21:13,657 --> 00:21:16,892
Hey, perfect game.
What's your name?

522
00:21:16,927 --> 00:21:18,194
Put it up on a wall.

523
00:21:18,196 --> 00:21:19,469
I was never here,

524
00:21:19,852 --> 00:21:21,962
and you will never
speak of this again

525
00:21:22,211 --> 00:21:24,570
Sync and Corrections by <font color="#ffff00">APOLLO</font>
<font color="#00ff00"> www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
