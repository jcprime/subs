1
00:00:02,936 --> 00:00:04,370
We're asking everyone
to do street parking.

2
00:00:04,404 --> 00:00:06,405
I'm not street parking
my Mercedes.

3
00:00:06,439 --> 00:00:08,240
Well, everyone is.
I'm doing it.

4
00:00:08,274 --> 00:00:10,376
'Cause nobody wants
to steal a Saturn.

5
00:00:10,410 --> 00:00:13,512
All right, uh,
moving on to recycling.

6
00:00:15,382 --> 00:00:17,316
- You okay, Ron?
- Just a little tooth pain.

7
00:00:17,350 --> 00:00:19,518
I'm fine.
Continue.

8
00:00:19,552 --> 00:00:21,887
Okay, each department
will be getting blue bins...

9
00:00:23,423 --> 00:00:25,457
Do you need to go
to the dentist, Ron?

10
00:00:25,492 --> 00:00:27,159
I don't like dentists.

11
00:00:27,193 --> 00:00:29,228
Just a second.

12
00:00:29,262 --> 00:00:30,229
- Hey.
- No, no.

13
00:00:30,263 --> 00:00:31,296
No!

14
00:00:31,331 --> 00:00:32,431
No, no, no, no!
No, no, no, no!

15
00:00:35,335 --> 00:00:36,969
Aah!

16
00:00:37,003 --> 00:00:39,304
- Oh!
- What the?

17
00:00:39,339 --> 00:00:40,639
Oh, my God!

18
00:00:40,673 --> 00:00:42,941
I'm sorry, everybody.
What were we talking about...

19
00:00:42,976 --> 00:00:44,943
Recycling?

20
00:00:44,978 --> 00:00:47,212
Dentist pulled
the tooth out yesterday.

21
00:00:47,247 --> 00:00:49,615
But it's always a good idea
to demonstrate

22
00:00:49,649 --> 00:00:51,750
to your co-workers
that you are capable

23
00:00:51,785 --> 00:00:54,553
of withstanding
a tremendous amount of pain.

24
00:00:54,587 --> 00:00:57,890
Plus, it's always fun
to see Tom faint.

25
00:01:03,296 --> 00:01:13,896
<font color="#ec14bd">Corrected by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

26
00:01:20,346 --> 00:01:23,082
Attention, everybody!

27
00:01:23,116 --> 00:01:24,349
Everybody, listen up.

28
00:01:24,384 --> 00:01:28,120
You are all officially invited
to a dinner party,

29
00:01:28,154 --> 00:01:29,455
7:00 P.M. tonight
at our home,

30
00:01:29,489 --> 00:01:31,657
which is officially
my friend Burly's home,

31
00:01:31,691 --> 00:01:33,425
because I don't have a home.

32
00:01:33,460 --> 00:01:34,793
That's it.
There's no more left.

33
00:01:34,828 --> 00:01:36,095
Give it to him.

34
00:01:36,129 --> 00:01:37,463
We've been dating
for almost a month,

35
00:01:37,497 --> 00:01:39,164
so we just decided

36
00:01:39,199 --> 00:01:41,467
that we wanted to do
something special.

37
00:01:41,501 --> 00:01:45,104
And dinner parties do combine
two of our favorite things...

38
00:01:45,138 --> 00:01:46,772
dinner and parties.

39
00:01:46,806 --> 00:01:48,307
- Parties.
- Yeah.

40
00:01:48,341 --> 00:01:52,611
It's like if you could have
a Xbox pancake.

41
00:01:52,645 --> 00:01:54,313
Your invitations
include requests

42
00:01:54,347 --> 00:01:56,014
for each of you to bring stuff,

43
00:01:56,049 --> 00:01:57,516
'cause we don't have
a whole lot.

44
00:01:57,550 --> 00:02:00,185
- Don't be late.
- Watch out.

45
00:02:00,220 --> 00:02:02,621
"Dearest Tom,
please bring silverware."

46
00:02:02,655 --> 00:02:05,491
- "Please bring cooked steak."
- This is ridiculous.

47
00:02:05,525 --> 00:02:06,758
They're young.
They're in love.

48
00:02:06,793 --> 00:02:08,560
They're ready to show it
to the world.

49
00:02:08,595 --> 00:02:09,761
It's very sweet.

50
00:02:09,796 --> 00:02:11,630
- And we're all going.
- What?

51
00:02:11,664 --> 00:02:13,866
There's a boy's sale
at Dillard's today.

52
00:02:13,900 --> 00:02:15,400
- Hey.
- Oh, hey.

53
00:02:15,435 --> 00:02:16,702
You going tonight?

54
00:02:16,736 --> 00:02:19,538
Yeah. Apparently they want me
to bring <i>Avatar</i>

55
00:02:19,572 --> 00:02:24,143
and 50 pairs of 3-d glasses
and a 3-d-capable television.

56
00:02:24,177 --> 00:02:25,544
- Mm.
- Yeah.

57
00:02:25,578 --> 00:02:27,479
Well, I will see you there,
with all of those things.

58
00:02:27,514 --> 00:02:28,814
I was actually coming
to see you.

59
00:02:28,848 --> 00:02:30,115
I wanted your advice
on something.

60
00:02:30,150 --> 00:02:32,217
- Oh, yeah?
- My boss in Indianapolis...

61
00:02:32,252 --> 00:02:34,586
He wants me back on the road
in a week.

62
00:02:34,621 --> 00:02:38,056
But then this morning
Chris offered me a job

63
00:02:38,091 --> 00:02:41,527
to stay here in Pawnee
and work for him.

64
00:02:41,561 --> 00:02:43,929
- Do you want to do that?
- Well, I don't know.

65
00:02:43,963 --> 00:02:45,964
I've been moving around so much
the past few years,

66
00:02:45,999 --> 00:02:48,333
it might be nice to stay
in one place for a while.

67
00:02:48,368 --> 00:02:51,603
- What do you think?
- Well, this is a great city.

68
00:02:51,638 --> 00:02:53,405
You know, it's definitely
the best city in Indiana,

69
00:02:53,439 --> 00:02:55,507
probably America,
possibly the world.

70
00:02:55,542 --> 00:02:57,042
Sure.

71
00:02:57,076 --> 00:02:59,077
But on the other hand,
you've put in 12 years

72
00:02:59,112 --> 00:03:01,079
with the state government.

73
00:03:01,114 --> 00:03:02,548
So you think
I should say no to Chris

74
00:03:02,582 --> 00:03:06,552
and head back
to Indianapolis or...?

75
00:03:06,586 --> 00:03:09,087
It's a tough call.
You know what I would do?

76
00:03:09,122 --> 00:03:11,023
You should make
a pros and cons list.

77
00:03:11,057 --> 00:03:13,258
- That always works for me.
- Yeah. Yeah, yeah.

78
00:03:13,293 --> 00:03:15,060
Okay, maybe I'll do that.

79
00:03:15,094 --> 00:03:17,029
Excellent.
Nice talking to you.

80
00:03:17,063 --> 00:03:18,630
- Good stuff.
- Yeah.

81
00:03:18,665 --> 00:03:20,232
Okay.

82
00:03:23,770 --> 00:03:25,370
So how does this work?

83
00:03:25,405 --> 00:03:26,972
All right, you just fill out
your name tag,

84
00:03:27,006 --> 00:03:29,041
- and you're all set.
- Okay.

85
00:03:29,075 --> 00:03:32,811
But I-I mean, do I just go up
and talk to anyone,

86
00:03:32,845 --> 00:03:35,714
or do I wait for them
to talk to me, or...?

87
00:03:35,748 --> 00:03:39,051
Are you asking me
how to flirt with men?

88
00:03:39,085 --> 00:03:41,386
Okay.
All right.

89
00:03:41,421 --> 00:03:42,788
It's a whole new Ann Perkins.

90
00:03:42,822 --> 00:03:45,457
I'm putting myself out there,
meeting some new people,

91
00:03:45,491 --> 00:03:47,826
having some casual fun,

92
00:03:47,860 --> 00:03:50,929
and it is...awkward.

93
00:03:50,964 --> 00:03:54,433
Gayle went out and got me
this new party shirt.

94
00:03:54,467 --> 00:03:56,802
So be honest...
What do you guys think?

95
00:03:56,836 --> 00:03:58,070
You know what, Jerry?

96
00:03:58,104 --> 00:04:01,673
I make fun of you a lot,
but credit where credit is due.

97
00:04:01,708 --> 00:04:05,410
You know, I like how the...

98
00:04:05,445 --> 00:04:09,848
Damn it. I was so close.
It's a terrible shirt.

99
00:04:09,882 --> 00:04:12,417
Hey, guys.
Okay, everybody listen up.

100
00:04:12,452 --> 00:04:15,087
April's friend Orin is here.

101
00:04:15,121 --> 00:04:17,322
He's very intense
and very weird.

102
00:04:17,357 --> 00:04:18,824
So if you end up
talking to him,

103
00:04:18,858 --> 00:04:20,325
just make sure you don't reveal

104
00:04:20,360 --> 00:04:22,094
anything personal
about yourself.

105
00:04:22,128 --> 00:04:23,829
- Hey, gang.
- Hey.

106
00:04:23,863 --> 00:04:26,632
- What'd you bring?
- I was in charge of the cake.

107
00:04:26,666 --> 00:04:29,368
To be fair, it's not a cake so
much as it is a vegetable loaf.

108
00:04:29,402 --> 00:04:31,136
You got your mushrooms,
your alfalfa sprouts,

109
00:04:31,170 --> 00:04:34,506
your spinach, and I had it
sweetened with fruit reduction.

110
00:04:34,540 --> 00:04:37,876
But did they ask you to bring
a vegetable loaf or a cake?

111
00:04:37,910 --> 00:04:40,178
No, a cake,
but this is so much healthier.

112
00:04:40,213 --> 00:04:42,514
So not only
does this thing exist,

113
00:04:42,548 --> 00:04:44,616
but now you have deprived
everyone of cake.

114
00:04:44,651 --> 00:04:47,152
- Take a walk, Ron.
- Yep.

115
00:04:52,425 --> 00:04:54,192
Donna.
Oh, my God.

116
00:04:54,227 --> 00:04:56,328
I am so excited to see you here.

117
00:04:56,362 --> 00:05:00,465
These things are horrible
when you're by yourself.

118
00:05:00,500 --> 00:05:01,667
What?

119
00:05:01,701 --> 00:05:03,869
Do you know
where you are right now?

120
00:05:03,903 --> 00:05:06,371
We're in the jungle.
There are no friends here.

121
00:05:06,406 --> 00:05:08,874
It's every woman for herself.

122
00:05:08,908 --> 00:05:11,643
- You're joking, right?
- Do I look like I'm joking?

123
00:05:11,678 --> 00:05:14,746
Dating is a zero-sum game.

124
00:05:14,781 --> 00:05:18,650
If you get a man,
I don't get that man.

125
00:05:18,685 --> 00:05:20,319
I'm here because of advice
that you gave me

126
00:05:20,353 --> 00:05:22,354
to be more adventurous
in my life.

127
00:05:22,388 --> 00:05:26,158
Here's some more advice...
Beat it.

128
00:05:26,192 --> 00:05:27,492
Hey.

129
00:05:27,527 --> 00:05:29,628
Ben, heard you
might be leaving Pawnee.

130
00:05:29,662 --> 00:05:32,931
I don't know.
I'm thinking about it.

131
00:05:32,965 --> 00:05:34,232
Well, you are a huge nerd.

132
00:05:34,267 --> 00:05:36,034
But I'd be sorry
to see you go, man.

133
00:05:36,069 --> 00:05:37,302
Thanks, Tom.

134
00:05:37,337 --> 00:05:38,937
Okay, this party needs
a little help.

135
00:05:38,971 --> 00:05:40,272
I'll be right back.

136
00:05:40,306 --> 00:05:42,641
So are you going on, like,
a year-long walking tour

137
00:05:42,675 --> 00:05:44,476
of the set
of <i>The Lord of the Rings</i>

138
00:05:44,510 --> 00:05:45,610
in New Zealand?

139
00:05:45,645 --> 00:05:47,612
To be honest with you,
I wasn't a fan

140
00:05:47,647 --> 00:05:49,448
of, uh, Peter Jackson's
interpretation.

141
00:05:49,482 --> 00:05:51,083
So you can put that one away.

142
00:05:51,117 --> 00:05:54,119
Andy, I think you maybe
should put some more food out.

143
00:05:54,153 --> 00:05:57,456
Totally agree. Just firing up
the bagel bites now.

144
00:05:57,490 --> 00:05:59,257
- Hey.
- Oh, my God.

145
00:05:59,292 --> 00:06:00,759
What do you think?

146
00:06:00,793 --> 00:06:02,260
April, you're not supposed
to let me see you.

147
00:06:02,295 --> 00:06:03,628
No, come on.
You have to look.

148
00:06:03,663 --> 00:06:05,964
This is important. You have
to tell me if I look good.

149
00:06:05,998 --> 00:06:07,466
Hey, guys, what's going on?

150
00:06:07,500 --> 00:06:10,035
Uh, it's supposed to be
a big surprise.

151
00:06:10,069 --> 00:06:11,436
But tonight

152
00:06:11,471 --> 00:06:15,173
me and April
are getting married...

153
00:06:15,208 --> 00:06:16,942
To each other.

154
00:06:19,579 --> 00:06:20,746
- Are you pregnant?
- Yes.

155
00:06:20,780 --> 00:06:21,913
- Yes?
- No.

156
00:06:21,948 --> 00:06:23,148
Are you sick?
Are you terminal?

157
00:06:23,182 --> 00:06:24,549
Is it like that movie
<i>A Walk to Remember?</i>

158
00:06:24,584 --> 00:06:25,550
- Yes.
- What?

159
00:06:25,585 --> 00:06:27,252
- No. God.
- Oh, God.

160
00:06:27,286 --> 00:06:28,920
Why are you doing this?

161
00:06:28,955 --> 00:06:31,223
Why is this great thing
happening?

162
00:06:31,257 --> 00:06:33,959
Well, I mean, it's actually
a really funny story.

163
00:06:33,993 --> 00:06:35,293
We were hanging out.

164
00:06:35,328 --> 00:06:37,963
And suddenly, I was like,

165
00:06:37,997 --> 00:06:39,664
"What if we got married
tomorrow?"

166
00:06:39,699 --> 00:06:42,768
And she was like...

167
00:06:42,802 --> 00:06:44,069
"Fine."

168
00:06:44,103 --> 00:06:46,505
- She's fine.
- That is a great story.

169
00:06:46,539 --> 00:06:48,440
But you guys haven't been
together very long,

170
00:06:48,474 --> 00:06:50,041
and you have no place to live.

171
00:06:50,076 --> 00:06:51,376
We've been together
long enough,

172
00:06:51,411 --> 00:06:52,644
and we'll find a place to live.

173
00:06:52,678 --> 00:06:53,845
Yeah.

174
00:06:53,880 --> 00:06:55,580
Like, a house
or, like, a condo or something.

175
00:06:55,615 --> 00:06:56,748
Probably a condo.

176
00:06:56,783 --> 00:06:59,050
It's... you can't just get
a condo.

177
00:06:59,085 --> 00:07:02,053
No, you're right...
house.

178
00:07:02,088 --> 00:07:04,256
I love Andy and April
as a couple.

179
00:07:04,290 --> 00:07:06,191
I want them
to stay together forever.

180
00:07:06,225 --> 00:07:08,727
But relationships
need planning.

181
00:07:08,761 --> 00:07:11,730
You just can't jump
into something without thinking.

182
00:07:11,764 --> 00:07:15,033
It took me four years
to find the right hairdresser,

183
00:07:15,067 --> 00:07:17,602
and we still fight
all the time!

184
00:07:17,637 --> 00:07:19,538
Andy and April
are gonna surprise people

185
00:07:19,572 --> 00:07:21,039
with a secret ceremony.

186
00:07:21,073 --> 00:07:23,442
Whoa.
Secret wedding.

187
00:07:23,476 --> 00:07:25,477
Julia Roberts
pulled the same move.

188
00:07:25,511 --> 00:07:28,013
Is that that toothy girl
from <i>Mystic Pizza?</i>

189
00:07:28,047 --> 00:07:30,248
Ron, please, I need you
to help me knock some sense

190
00:07:30,283 --> 00:07:31,416
into them.

191
00:07:31,451 --> 00:07:34,486
It's not really my place
or yours.

192
00:07:34,520 --> 00:07:37,189
- Hi. I'm Ann.
- Brian.

193
00:07:37,223 --> 00:07:38,857
What's your occupation?

194
00:07:38,891 --> 00:07:40,826
I'm a manager
at a sporting-goods store.

195
00:07:40,860 --> 00:07:43,128
- No way. Me too.
- Seriously? Which one?

196
00:07:43,162 --> 00:07:47,599
No, I'm not.
Um, I-I was just ribbing you.

197
00:07:47,633 --> 00:07:50,335
- What are you drinking?
- Yeah.

198
00:07:50,369 --> 00:07:52,838
- What?
- Oh, I don't know.

199
00:07:52,872 --> 00:07:54,673
I-I couldn't hear you.

200
00:07:54,707 --> 00:07:58,643
So you just laughed
and said, "Yeah"?

201
00:07:58,678 --> 00:08:01,613
- Yeah.
- Excuse us.

202
00:08:01,647 --> 00:08:03,482
That was the worst thing
I've ever seen in my life.

203
00:08:03,516 --> 00:08:05,016
Did you grow up in the woods?

204
00:08:05,051 --> 00:08:07,219
Are you Nell
from the movie <i>Nell?</i>

205
00:08:07,253 --> 00:08:09,821
I told you I'm rusty.

206
00:08:09,856 --> 00:08:11,456
All right, come with me.

207
00:08:11,491 --> 00:08:13,024
- Hey, Andy.
- Yeah.

208
00:08:13,059 --> 00:08:15,527
I know what's going down,
and I'm so happy for you guys.

209
00:08:15,561 --> 00:08:16,862
Thank you, Tom.

210
00:08:16,896 --> 00:08:18,330
Hey, something
just occurred to me.

211
00:08:18,364 --> 00:08:20,732
- Can I be your best man?
- Yeah, dude, totally.

212
00:08:20,766 --> 00:08:22,567
- Thank you.
- My best man.

213
00:08:23,769 --> 00:08:26,104
One of my life goals
is to be a best man.

214
00:08:26,138 --> 00:08:28,039
It's a baller position.

215
00:08:28,074 --> 00:08:29,574
You get drunk.
You make speeches.

216
00:08:29,609 --> 00:08:32,611
And you make love
to the prettiest bridesmaid,

217
00:08:32,645 --> 00:08:34,880
usually standing from behind.

218
00:08:34,914 --> 00:08:37,215
Look, I know that
I cannot tell you what to do.

219
00:08:37,250 --> 00:08:38,750
I know it doesn't work...

220
00:08:38,784 --> 00:08:41,219
From all the times I've tried
to tell you what to do before.

221
00:08:41,254 --> 00:08:42,554
But you know what would be fun?

222
00:08:42,588 --> 00:08:44,990
Maybe we should make
a pros and cons list.

223
00:08:45,024 --> 00:08:47,158
Let me take you home, and then
we'll go do that together.

224
00:08:47,193 --> 00:08:48,159
It'll be a blast.

225
00:08:48,194 --> 00:08:51,096
- Leslie, relax.
- You relax.

226
00:08:51,130 --> 00:08:53,532
You relax all the way home
and get into bed.

227
00:08:53,566 --> 00:08:56,067
- You need to go to bed.
- April.

228
00:08:56,102 --> 00:08:57,569
Girl talk.

229
00:08:57,603 --> 00:09:00,071
Hey, uh,
what's your middle name?

230
00:09:00,106 --> 00:09:02,040
The justice of the peace lady
needs to know.

231
00:09:02,074 --> 00:09:03,775
You don't know
each other's middle names?

232
00:09:03,809 --> 00:09:06,344
I don't want to say it
out loud.

233
00:09:06,379 --> 00:09:08,613
- Ew!
- Shut up.

234
00:09:08,648 --> 00:09:12,183
- Roberta!
- Such a loser.

235
00:09:12,218 --> 00:09:15,186
- Can't believe I'm marrying him.
- You don't have to.

236
00:09:15,221 --> 00:09:17,722
I'm going to.
And then I'm gonna divorce him.

237
00:09:17,757 --> 00:09:20,258
Then I'm gonna marry him again.

238
00:09:20,293 --> 00:09:23,929
One time, I fell madly in love
with a Civil War reenactor

239
00:09:23,963 --> 00:09:25,630
that I had only known
for six hours.

240
00:09:25,665 --> 00:09:27,732
And then I found out
he wore those clothes

241
00:09:27,767 --> 00:09:29,434
all the time.

242
00:09:29,468 --> 00:09:30,602
And he was married.

243
00:09:30,636 --> 00:09:33,305
But the clothes thing
really bothered me.

244
00:09:34,574 --> 00:09:36,441
You always want
to scribble the name.

245
00:09:36,475 --> 00:09:39,144
Make them read.
Draw them in.

246
00:09:39,178 --> 00:09:41,446
- Make them work for it.
- Right.

247
00:09:41,480 --> 00:09:42,948
Two shots of Jameson.

248
00:09:42,982 --> 00:09:45,717
Don't make
too much eye contact.

249
00:09:45,751 --> 00:09:48,720
You want to seem available,
but not too available.

250
00:09:48,754 --> 00:09:50,522
Oh, thanks.
I think I'm good for now.

251
00:09:50,556 --> 00:09:52,591
- I'm not gonna...
- Yeah.

252
00:09:54,860 --> 00:09:58,797
All right...
Let's do this.

253
00:09:58,831 --> 00:10:00,999
Ron, you are so amazing

254
00:10:01,033 --> 00:10:04,669
with me and April, stuff.

255
00:10:04,704 --> 00:10:06,504
I love you, man.

256
00:10:06,539 --> 00:10:08,573
Would you be one
of my best men?

257
00:10:08,608 --> 00:10:11,776
I'd be honored to, son.

258
00:10:11,811 --> 00:10:14,579
Chris, you helped me
get April back.

259
00:10:14,614 --> 00:10:16,414
Would you be my best man?

260
00:10:16,449 --> 00:10:19,918
Nothing else in my entire life

261
00:10:19,952 --> 00:10:22,320
would make me happier
as long as I live.

262
00:10:22,355 --> 00:10:26,024
Jerry. Ah, I'm so glad
you made it.

263
00:10:26,058 --> 00:10:27,192
This shirt is hilarious.

264
00:10:27,226 --> 00:10:30,629
Derek and Ben,
will you be my best men?

265
00:10:30,663 --> 00:10:33,298
Got a little competition
for the best man, huh?

266
00:10:33,332 --> 00:10:35,700
Well, you know what the best
best man does.

267
00:10:35,735 --> 00:10:37,936
Excuse me!

268
00:10:37,970 --> 00:10:40,305
Are there any strippers here?

269
00:10:40,339 --> 00:10:42,107
Former strippers?

270
00:10:42,141 --> 00:10:44,442
Non-dancers but you're feeling
a little bit drunk?

271
00:10:44,477 --> 00:10:45,844
What are you doing?

272
00:10:45,878 --> 00:10:47,912
I'm throwing Andy
an impromptu bachelor party.

273
00:10:47,947 --> 00:10:49,547
It's my duty as best man.

274
00:10:49,582 --> 00:10:51,116
The wedding's in four minutes.

275
00:10:51,150 --> 00:10:52,584
It's that kind
of negative thinking

276
00:10:52,618 --> 00:10:55,186
that makes you
a less good best man.

277
00:10:55,221 --> 00:10:56,721
Hello?

278
00:10:56,756 --> 00:10:57,822
Hi, Ann, it's Leslie...

279
00:10:57,857 --> 00:10:59,591
Leslie Knope
from the Parks Department.

280
00:10:59,625 --> 00:11:01,459
Yeah, I know.
Where are you?

281
00:11:01,494 --> 00:11:03,261
April and Andy
are getting married tonight.

282
00:11:03,295 --> 00:11:05,163
- What?
- Yeah, I know.

283
00:11:05,197 --> 00:11:06,598
I need you
to get down here, Ann.

284
00:11:06,632 --> 00:11:10,268
I need you to kiss him again
or kiss her or kiss somebody.

285
00:11:10,302 --> 00:11:13,071
I don't know, Ann!
Help me, please!

286
00:11:13,105 --> 00:11:15,140
Relationships
are like scuba diving.

287
00:11:15,174 --> 00:11:18,143
If you come up too fast,
you get the bends.

288
00:11:18,177 --> 00:11:19,477
And the reason why I know this

289
00:11:19,512 --> 00:11:20,879
is because I was dating
a guy once,

290
00:11:20,913 --> 00:11:22,047
and we were scuba diving,

291
00:11:22,081 --> 00:11:23,515
and I told him
that I loved him underwater,

292
00:11:23,549 --> 00:11:26,117
and he shot up really fast,
and he got the bends.

293
00:11:26,152 --> 00:11:30,121
No, Orin, I don't know
how I'm going to die.

294
00:11:30,156 --> 00:11:32,123
Wait. Are you asking me
or telling me?

295
00:11:32,158 --> 00:11:33,792
Hey, can I talk to you
for a second?

296
00:11:33,826 --> 00:11:34,893
Yes, please.

297
00:11:34,927 --> 00:11:36,961
April and Andy
are about to get married.

298
00:11:36,996 --> 00:11:41,132
Wow. My Brita filter is older
than their relationship.

299
00:11:41,167 --> 00:11:43,068
Wait a second.
Should I change my Brita filter?

300
00:11:43,102 --> 00:11:45,170
I don't know. It depends
on how frequently you use it.

301
00:11:45,204 --> 00:11:47,172
- It's true.
- Stay focused, okay? Please.

302
00:11:47,206 --> 00:11:49,741
The point is two boneheads
whom I love dearly

303
00:11:49,775 --> 00:11:51,309
are about
to sacrifice their future

304
00:11:51,343 --> 00:11:53,078
'cause they haven't thought
this thing through.

305
00:11:53,112 --> 00:11:54,546
And nobody seems concerned
about this.

306
00:11:54,580 --> 00:11:57,916
Attention, everybody.
Madames and...

307
00:11:57,950 --> 00:12:00,485
Mis...Wahs.

308
00:12:00,519 --> 00:12:03,655
If you would do me
the obligation

309
00:12:03,689 --> 00:12:08,993
of having your honor heretofore
in the room

310
00:12:09,028 --> 00:12:11,696
doth right over there,
uh, hence.

311
00:12:11,731 --> 00:12:12,831
What?

312
00:12:12,865 --> 00:12:14,833
Big event in that room,
15 minutes.

313
00:12:14,867 --> 00:12:16,735
Oh, my God.
This is really happening.

314
00:12:16,769 --> 00:12:20,605
I love April and Andy.
I want them to stay together.

315
00:12:20,639 --> 00:12:23,808
And that is why I have to stop
their wedding.

316
00:12:26,479 --> 00:12:27,946
Remember how I told you
there was going to be

317
00:12:27,980 --> 00:12:29,447
a surprise later?

318
00:12:29,482 --> 00:12:32,317
Well, the surprise is you're not
actually at a dinner party.

319
00:12:32,351 --> 00:12:35,620
You're at our wedding.

320
00:12:38,023 --> 00:12:41,159
All right.
Uh, let's do this.

321
00:13:04,450 --> 00:13:06,217
Ladies and gentlemen,

322
00:13:06,252 --> 00:13:07,786
we are gathered here today

323
00:13:07,820 --> 00:13:13,792
to marry Andrew Maxwell Dwyer
and April Roberta Ludgate.

324
00:13:13,826 --> 00:13:17,095
I guess I kind of hate
most things.

325
00:13:17,129 --> 00:13:21,099
But I never really seem
to hate you.

326
00:13:21,133 --> 00:13:23,668
So I want to spend
the rest of my life with you.

327
00:13:23,702 --> 00:13:25,770
- Is that cool?
- Yes.

328
00:13:27,573 --> 00:13:29,040
- Is it my turn?
- Yes.

329
00:13:29,074 --> 00:13:32,343
"April, you're the most
awesome person I have ever known

330
00:13:32,378 --> 00:13:33,411
"in my entire life.

331
00:13:33,445 --> 00:13:37,949
"I vow to protect you
from danger.

332
00:13:37,983 --> 00:13:40,151
"And I don't care
if I have to fight

333
00:13:40,186 --> 00:13:42,654
"an ultimate fighter
or a bear or him,

334
00:13:42,688 --> 00:13:45,089
"your mom...
I would take them down."

335
00:13:45,124 --> 00:13:46,424
Andy.

336
00:13:46,458 --> 00:13:48,526
I'm getting mad right now
even thinking about it.

337
00:13:48,561 --> 00:13:49,861
- I'm telling you.
- It's okay.

338
00:13:49,895 --> 00:13:52,997
I want to spend the rest
of my life...

339
00:13:53,032 --> 00:13:54,232
every minute...
with you.

340
00:13:54,266 --> 00:13:58,970
And I'm the luckiest man
in the galaxy.

341
00:13:59,004 --> 00:14:02,106
And now the rings.

342
00:14:05,077 --> 00:14:08,446
By the power vested in me
by the state of Indiana,

343
00:14:08,480 --> 00:14:11,683
I now pronounce you husband
and wife.

344
00:14:17,690 --> 00:14:20,391
- Go fly away.
- Fly. Fly.

345
00:14:20,426 --> 00:14:22,227
- Wings of love.
- Oh.

346
00:14:22,261 --> 00:14:23,461
Okay. All right.

347
00:14:23,495 --> 00:14:24,963
So that one is dead.
We know that.

348
00:14:37,576 --> 00:14:39,711
You wouldn't have been able
to stop it, you know.

349
00:14:39,745 --> 00:14:43,114
I could've yelled something
or tackled someone.

350
00:14:43,148 --> 00:14:45,583
But you didn't,
because deep down,

351
00:14:45,618 --> 00:14:47,252
you knew
it wouldn't have mattered.

352
00:14:47,286 --> 00:14:49,153
Those kids are gonna do
what they want to do.

353
00:14:49,188 --> 00:14:50,855
They may have just ruined
their lives

354
00:14:50,890 --> 00:14:52,724
on an impulse decision.

355
00:14:52,758 --> 00:14:54,726
Leslie, I got married twice.

356
00:14:54,760 --> 00:14:57,228
Both times, I was a lot older
than those two.

357
00:14:57,263 --> 00:15:00,965
And both marriages ended
in divorce...

358
00:15:01,000 --> 00:15:02,667
And a burning effigy.

359
00:15:02,701 --> 00:15:05,203
Who's to say what works?

360
00:15:05,237 --> 00:15:08,539
You find somebody you like,
and you roll the dice.

361
00:15:08,574 --> 00:15:11,342
That's all anybody can do.

362
00:15:11,377 --> 00:15:13,611
Wait. Weren't you married
three times?

363
00:15:13,646 --> 00:15:16,614
Oh, my God, you're right.

364
00:15:18,717 --> 00:15:20,685
I get to burn another effigy.

365
00:15:20,719 --> 00:15:23,354
The key to burning
an ex-wife effigy

366
00:15:23,389 --> 00:15:25,523
is to dip it in paraffin wax

367
00:15:25,557 --> 00:15:29,360
and then toss the flaming bottle
of isopropyl alcohol...

368
00:15:29,395 --> 00:15:31,162
From a safe distance.

369
00:15:31,196 --> 00:15:33,031
Do not stand too close

370
00:15:33,065 --> 00:15:35,733
when you light
an ex-wife effigy.

371
00:15:35,768 --> 00:15:39,337
My sister is lame,
but Andy's sort of cool.

372
00:15:39,371 --> 00:15:42,073
I guess I kind of see
why he'd marry her.

373
00:15:42,107 --> 00:15:44,809
Also, if anyone finds
my gray hoodie, I lost it.

374
00:15:44,843 --> 00:15:46,277
Thanks.

375
00:15:49,081 --> 00:15:50,748
That was beautiful.

376
00:15:50,783 --> 00:15:55,653
You two remind me
of me and my husband, George.

377
00:15:55,688 --> 00:15:57,555
I got to nail the speech.

378
00:15:57,589 --> 00:16:00,091
So I brought in an expert...
Jean-Ralphio.

379
00:16:00,125 --> 00:16:01,926
Can I throw something on you,
see if it feels good?

380
00:16:01,961 --> 00:16:03,294
Sure.

381
00:16:03,329 --> 00:16:04,495
Okay, this is what I would do.
I would start with a joke.

382
00:16:04,530 --> 00:16:07,498
Joke, Vince Vaughn quote,
obviously.

383
00:16:07,533 --> 00:16:10,134
<i>Swingers Crashers?</i>
<i>Fred Claus.</i>

384
00:16:10,169 --> 00:16:11,936
Talk about
Andy's ex-girlfriends.

385
00:16:11,971 --> 00:16:16,607
Quote from <i>Love Actually.</i>
<i>Hold back your tears. Pause.</i>

386
00:16:16,642 --> 00:16:18,309
Drop the microphone.
Get out of that bitch.

387
00:16:19,611 --> 00:16:22,180
I wish you the best of luck,
to both of you.

388
00:16:25,150 --> 00:16:27,118
Aw, thank you, grandma.

389
00:16:27,152 --> 00:16:29,954
How can anyone
ever possibly top that?

390
00:16:29,989 --> 00:16:31,689
Am I right?

391
00:16:31,724 --> 00:16:33,624
So thank you all
for all the talking.

392
00:16:33,659 --> 00:16:35,426
Let's just get
back to dancing, huh?

393
00:16:37,763 --> 00:16:41,232
Cool.
See you around maybe.

394
00:16:41,266 --> 00:16:44,168
- That went better, right?
- Yes, it did.

395
00:16:44,203 --> 00:16:47,305
However, he proudly told me
that he "beat herpes."

396
00:16:47,339 --> 00:16:49,540
I'm sorry, Donna.
I'm gonna go home.

397
00:16:49,575 --> 00:16:51,809
I just found out
Andy's getting married.

398
00:16:51,844 --> 00:16:55,146
- So?
- So that's my ex-boyfriend.

399
00:16:55,180 --> 00:16:56,748
We were together
for a really long time.

400
00:16:56,782 --> 00:16:59,283
- All right.
- What?

401
00:16:59,318 --> 00:17:02,987
What? Listen, you are a hot,
young doctor.

402
00:17:03,022 --> 00:17:05,056
- I'm a nurse, actually.
- Okay, I don't know you.

403
00:17:05,090 --> 00:17:07,358
But I do know
that you can fix your attitude.

404
00:17:07,393 --> 00:17:09,093
Do you want to go home
and feel sorry for yourself

405
00:17:09,128 --> 00:17:10,595
about a man you didn't want
to marry?

406
00:17:10,629 --> 00:17:12,196
Or do you want to go talk
to that cute boy

407
00:17:12,231 --> 00:17:14,699
who has been looking at you
and give him your number

408
00:17:14,733 --> 00:17:19,303
before I throw him in my Benz
for myself?

409
00:17:19,338 --> 00:17:22,106
All right.

410
00:17:22,141 --> 00:17:25,676
I just want to thank
my family, my friends,

411
00:17:25,711 --> 00:17:28,613
most importantly,
my <i>be</i>best man,

412
00:17:28,647 --> 00:17:30,114
Mr. Tom Haverford.

413
00:17:30,149 --> 00:17:32,683
Oh!
Coolest guy I know.

414
00:17:32,718 --> 00:17:34,619
And, ladies, he's single,
I think.

415
00:17:34,653 --> 00:17:35,887
You're not still married,
are you?

416
00:17:35,921 --> 00:17:39,190
- No.
- But you... you're straight?

417
00:17:39,224 --> 00:17:41,159
- Yes.
- Jean-Ralphio's just a friend?

418
00:17:41,193 --> 00:17:43,027
- Yeah.
- Yeah!

419
00:17:43,062 --> 00:17:46,931
Anyways, oh, my God,
I'm married!

420
00:17:46,965 --> 00:17:49,700
This is crazy.
What happened?

421
00:17:49,735 --> 00:17:53,671
But, seriously,
life is short, right?

422
00:17:53,705 --> 00:17:56,207
Uh, just... I really think
you should just do

423
00:17:56,241 --> 00:17:57,508
whatever makes you happy.

424
00:17:57,543 --> 00:18:00,144
That's what April and I did.

425
00:18:00,179 --> 00:18:01,746
We are in love.

426
00:18:01,780 --> 00:18:03,981
So we didn't overthink it.

427
00:18:04,016 --> 00:18:08,352
I mean, seriously,
I cannot emphasize

428
00:18:08,387 --> 00:18:10,321
how little we thought
about this.

429
00:18:10,355 --> 00:18:11,989
Am I right?

430
00:18:12,024 --> 00:18:15,660
All right, well, this is a song
I wrote for you,

431
00:18:15,694 --> 00:18:18,396
April Roberta Ludgate.

432
00:18:18,430 --> 00:18:21,099
I love you.

433
00:18:42,121 --> 00:18:44,055
Exactly. Mortality.

434
00:18:44,089 --> 00:18:45,790
See, I like to take each day
at a time.

435
00:18:45,824 --> 00:18:49,227
There's nothing in the world
we can't accomplish if we try.

436
00:18:49,261 --> 00:18:50,495
I have to go.

437
00:18:50,529 --> 00:18:53,631
It's been great talking to you.

438
00:18:54,700 --> 00:18:56,334
- Hey.
- Don't worry.

439
00:18:56,368 --> 00:18:57,902
I'm not gonna say another word.

440
00:18:57,936 --> 00:18:59,036
This is your day.

441
00:18:59,071 --> 00:19:02,974
I just wanted to say that...

442
00:19:03,008 --> 00:19:05,576
I'm really glad you're here.

443
00:19:05,611 --> 00:19:08,513
I think you're awesome,
and I love you.

444
00:19:10,249 --> 00:19:11,249
Oh.

445
00:19:14,520 --> 00:19:16,320
What's the deal
with that hot girl, April?

446
00:19:16,355 --> 00:19:18,256
- What's the deal with her?
- Yeah.

447
00:19:18,290 --> 00:19:20,758
Like, she good to go?
Like, she down to clown?

448
00:19:20,792 --> 00:19:24,095
She's married.

449
00:19:24,129 --> 00:19:25,630
She got married,
like, 20 minutes ago.

450
00:19:25,664 --> 00:19:28,566
Oh, my God. That's the same
girl from the thing.

451
00:19:28,600 --> 00:19:30,601
From the wedding ceremony,
yeah.

452
00:19:30,636 --> 00:19:33,104
All the good ones
are taken, huh, bro?

453
00:19:35,707 --> 00:19:37,842
Yeah.

454
00:19:42,981 --> 00:19:44,182
Hey.

455
00:19:46,652 --> 00:19:49,420
You should stay.

456
00:19:49,454 --> 00:19:52,623
Don't go back to Indianapolis.
You should stay here.

457
00:19:52,658 --> 00:19:55,793
- You think?
- Yeah. Yes.

458
00:19:55,827 --> 00:19:57,728
It's a great city here,
you know?

459
00:19:57,763 --> 00:19:59,030
And there are great people.

460
00:19:59,064 --> 00:20:00,531
And you've made a lot
of friends.

461
00:20:00,566 --> 00:20:01,899
And what are you gonna do...

462
00:20:01,934 --> 00:20:05,770
Go back to your old job
and hack up people's budgets?

463
00:20:05,804 --> 00:20:08,606
Stay here.
Help us build something.

464
00:20:08,640 --> 00:20:11,375
I already accepted the job.

465
00:20:11,410 --> 00:20:15,346
- I'm, uh... I'm staying here.
- That's good.

466
00:20:15,380 --> 00:20:17,715
Yeah, I think so.

467
00:20:19,418 --> 00:20:21,953
- Orin's behind me, isn't he?
- Mm-hmm. Yeah.

468
00:20:21,987 --> 00:20:23,020
- Okay.
- Okay.

469
00:20:23,055 --> 00:20:24,288
- So just keep your eyes on me.
- Okay.

470
00:20:24,323 --> 00:20:25,656
Don't panic,
'cause he can smell fear.

471
00:20:25,691 --> 00:20:26,724
Just keep talking to me.

472
00:20:26,758 --> 00:20:28,059
- Hi.
- Hi.

473
00:20:28,093 --> 00:20:29,727
- How are you?
- Good. How are you?

474
00:20:29,761 --> 00:20:32,196
- How was the wedding...
- Run.

475
00:20:36,835 --> 00:20:38,169
"Hope we can get
together soon."

476
00:20:38,203 --> 00:20:40,338
Okay, now you write,
"Who is this?"

477
00:20:40,372 --> 00:20:41,606
Ah.

478
00:20:41,640 --> 00:20:43,641
Hey, you guys, we got a video
from April and Andy.

479
00:20:43,675 --> 00:20:44,742
- Oh. Okay.
- Whoo!

480
00:20:44,776 --> 00:20:46,010
Yeah!
How you guys doing?

481
00:20:46,044 --> 00:20:47,912
- Hey, we're on our honeymoon.
- It is awesome.

482
00:20:47,946 --> 00:20:52,683
We're at Burly's family
vacation house on Lake Michigan.

483
00:20:52,718 --> 00:20:54,018
We're having so much fun,

484
00:20:54,052 --> 00:20:55,419
except for, uh, Roberta here

485
00:20:55,454 --> 00:20:57,922
turns out sucks
at water-skiing so bad.

486
00:20:57,956 --> 00:20:58,956
I'm gonna divorce you.

487
00:20:58,991 --> 00:21:00,391
Andy sucks at driving a boat.

488
00:21:00,425 --> 00:21:03,160
And I'm gonna divorce him,
probably right now.

489
00:21:03,195 --> 00:21:06,063
Well, they're still together.
You owe me 20 bucks.

490
00:21:06,263 --> 00:21:16,463
<font color="#ec14bd">Corrected by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
