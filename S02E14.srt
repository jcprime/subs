1
00:00:00,638 --> 00:00:01,792
It's because of you,

2
00:00:01,960 --> 00:00:04,378
our teachers, that every one
of these recreation classes

3
00:00:04,846 --> 00:00:07,298
in this catalog shines like a jewel

4
00:00:07,466 --> 00:00:09,366
in Pawnee's beautiful crown.

5
00:00:11,862 --> 00:00:13,732
Unfortunately,
due to budget constraints,

6
00:00:13,857 --> 00:00:16,974
this year we will have to remove
five jewels from that crown.

7
00:00:17,344 --> 00:00:19,685
Excuse me?
Are you cutting our classes?

8
00:00:20,538 --> 00:00:24,291
The city took away
$1,000 from our budget.

9
00:00:24,416 --> 00:00:25,566
So I'm very sorry.

10
00:00:25,734 --> 00:00:28,361
How will you decide
which classes to cut?

11
00:00:28,639 --> 00:00:31,139
By attendance.
And student evaluations.

12
00:00:31,315 --> 00:00:33,741
So just make sure
your students leave with a smile.

13
00:00:34,006 --> 00:00:37,156
My class is called
"coping with terminal illness."

14
00:00:37,329 --> 00:00:38,829
Hopefully your attendance is good.

15
00:00:40,207 --> 00:00:41,207
Actually, no.

16
00:00:41,722 --> 00:00:43,172
Hopefully it's bad.

17
00:00:43,449 --> 00:00:45,294
Nobody's more upset about this
than me.

18
00:00:45,462 --> 00:00:48,492
I've been taking rec center classes
since I was in high school.

19
00:00:48,617 --> 00:00:52,385
It's where I learned hair braiding,
how to make biscuits, french kissing.

20
00:00:53,508 --> 00:00:55,721
It was just from a boy
in my biscuits class,

21
00:00:55,846 --> 00:00:57,807
but either way, lesson learned.

22
00:00:58,200 --> 00:00:59,842
- Feygnasse Team -

23
00:01:00,019 --> 00:01:01,753
. :: La Fabrique ::.

24
00:01:02,690 --> 00:01:04,767
�pisode 214
<i>Leslie's House</i>

25
00:01:05,753 --> 00:01:07,007
mpm

26
00:01:07,354 --> 00:01:08,611
lestat78

27
00:01:18,480 --> 00:01:19,370
My God.

28
00:01:19,538 --> 00:01:21,038
You've never been to carnival.

29
00:01:21,206 --> 00:01:23,614
- I wanna go.
- We have to get tickets in February

30
00:01:23,739 --> 00:01:25,292
to go to Rio De Janeiro.

31
00:01:26,678 --> 00:01:29,378
I am on a date with Justin
in Indianapolis.

32
00:01:29,670 --> 00:01:32,550
We've only been out a few times,
but every one of them has been amazing.

33
00:01:32,718 --> 00:01:33,577
Cheers.

34
00:01:33,702 --> 00:01:36,594
Last time,
we had a private tour and cocktails

35
00:01:36,719 --> 00:01:38,669
at the indianapolis aquarium.

36
00:01:38,974 --> 00:01:41,809
- That fish reminds me of my mom.
- Why?

37
00:01:42,785 --> 00:01:44,687
It's just being really withholding.

38
00:01:45,031 --> 00:01:47,481
Justin is the most interesting guy
I've ever dated.

39
00:01:47,903 --> 00:01:51,068
- All I did was a little paperwork.
- He's being modest.

40
00:01:51,528 --> 00:01:54,196
It is thanks to him
my family remains in this country.

41
00:01:54,364 --> 00:01:56,064
And it was all <i>pro bono</i>.

42
00:01:57,034 --> 00:01:58,159
You've got to try.

43
00:02:01,119 --> 00:02:03,873
- That's really good.
- Isn't it great? It's camel stomach.

44
00:02:04,630 --> 00:02:05,374
Is it?

45
00:02:05,880 --> 00:02:07,251
I'm teasing. It's chicken.

46
00:02:09,192 --> 00:02:10,671
Would you like to try it?

47
00:02:13,512 --> 00:02:14,383
This is me.

48
00:02:15,355 --> 00:02:17,094
So when am I gonna see you again?

49
00:02:18,591 --> 00:02:20,514
I'm gonna be near Pawnee
on Tuesday.

50
00:02:20,807 --> 00:02:22,975
Excellent.
I'll plan something exciting.

51
00:02:23,551 --> 00:02:24,310
In fact,

52
00:02:25,330 --> 00:02:28,105
Our date in Pawnee is gonna blow
Indianapolis out of the water.

53
00:02:28,273 --> 00:02:29,273
All right.

54
00:02:30,697 --> 00:02:32,651
- I'm gonna hold you to that.
- You better.

55
00:02:32,819 --> 00:02:33,776
I will.

56
00:02:36,644 --> 00:02:37,907
I've racked my brain

57
00:02:38,075 --> 00:02:39,992
and I can't come up
with one exciting idea.

58
00:02:40,582 --> 00:02:44,413
He once ate fried cockatoo
with a member of the Thal Royal Family.

59
00:02:45,104 --> 00:02:47,793
Maybe just have him over
and cook him some chicken

60
00:02:47,918 --> 00:02:49,960
and tell him it's Pawnee cockatoo.

61
00:02:51,171 --> 00:02:52,922
A dinner party.
That's genius.

62
00:02:53,417 --> 00:02:55,530
I'll invite
all my most interesting friends over,

63
00:02:55,655 --> 00:02:59,094
- we'll have this big rambling party...
- I meant more of a one-on-one thing.

64
00:02:59,262 --> 00:03:01,419
I got it,
but I took your idea and made it better.

65
00:03:01,544 --> 00:03:02,890
It's called a think tank.

66
00:03:03,058 --> 00:03:05,434
- What is?
- Our lunches. Like think thanks.

67
00:03:05,878 --> 00:03:07,468
Come over to my house at 2:00

68
00:03:07,593 --> 00:03:09,821
and help me get ready.
Make sure that Mark's coming.

69
00:03:09,946 --> 00:03:12,399
So much work to do.
You're my best friend, get out of here!

70
00:03:15,282 --> 00:03:17,154
How many courses will there be?

71
00:03:17,571 --> 00:03:18,280
Three.

72
00:03:19,408 --> 00:03:20,182
Four.

73
00:03:21,368 --> 00:03:23,410
- Not including dessert.
- So five courses.

74
00:03:24,608 --> 00:03:26,038
Now it will be five courses.

75
00:03:26,502 --> 00:03:30,358
If I wanted to bring
a large number of deviled eggs

76
00:03:30,483 --> 00:03:33,078
but I didn't want to share them
with anyone else,

77
00:03:33,203 --> 00:03:34,922
can you guarantee fridge space?

78
00:03:35,204 --> 00:03:36,804
Just be there at 8:00.

79
00:03:38,774 --> 00:03:40,734
I'm having an a-list dinner party
for Justin

80
00:03:40,859 --> 00:03:43,846
and you, out of all my friends, come
from the most distant and exotic land.

81
00:03:44,015 --> 00:03:45,264
South Carolina?

82
00:03:46,387 --> 00:03:48,894
Save it for the party.
Can you make it?

83
00:03:49,062 --> 00:03:51,067
For Justin, are you kidding?
I'm there.

84
00:03:51,192 --> 00:03:53,260
Just don't invite
any boring people like jerry.

85
00:03:53,385 --> 00:03:55,693
- What are you talking about?
- Don't worry about it.

86
00:04:01,157 --> 00:04:02,616
Check it out.
Just practicing.

87
00:04:02,784 --> 00:04:05,481
Isn't that polish gonna get
on people's butts when they sit down?

88
00:04:07,789 --> 00:04:10,582
I'm having a party. And I thought 
you could help me with serving

89
00:04:10,750 --> 00:04:12,167
and taking people's coats.

90
00:04:12,335 --> 00:04:13,669
Ten bucks an hour.

91
00:04:14,075 --> 00:04:17,131
My guardian angel, I would love to.
Who's gonna be there?

92
00:04:17,299 --> 00:04:18,477
Really fun people.

93
00:04:22,437 --> 00:04:25,139
- Great. What's the occasion?
- Justin's coming to town.

94
00:04:27,392 --> 00:04:28,756
Hey, it's me Justin.

95
00:04:28,881 --> 00:04:32,187
Take my coat, but please be careful.
I got it from the king of Africa

96
00:04:32,355 --> 00:04:35,355
when we were walking
on the Berlin wall together.

97
00:04:36,143 --> 00:04:38,943
Really, Justin.
What instruments do you play?

98
00:04:40,053 --> 00:04:41,103
Actually...

99
00:04:41,560 --> 00:04:43,610
He's a pretty sick keyboardist.

100
00:04:44,182 --> 00:04:45,826
I'm good to go.
That sounds great.

101
00:04:46,111 --> 00:04:47,711
- Great.
- I'll see you there.

102
00:04:48,615 --> 00:04:50,881
- Afternoon, Andy.
- Hi, Kyle.

103
00:04:51,324 --> 00:04:52,666
Right before you sat down,

104
00:04:52,920 --> 00:04:55,269
I noticed, did you have
some kind of stain on your ass

105
00:04:55,394 --> 00:04:57,618
- or something, what was that?
- What?

106
00:04:59,899 --> 00:05:00,999
You're good.

107
00:05:04,830 --> 00:05:06,205
Welcome to my house.

108
00:05:06,330 --> 00:05:08,930
I can't believe
you've never had me over.

109
00:05:17,025 --> 00:05:18,025
Now I can.

110
00:05:18,817 --> 00:05:21,612
I know, it's a little messy.
But I have a system.

111
00:05:22,230 --> 00:05:24,615
I need you to help me
put the finishing touches on everything.

112
00:05:25,902 --> 00:05:28,489
This newspaper's from November 1986.

113
00:05:28,614 --> 00:05:30,864
The first rumblings of Iran contra.

114
00:05:30,989 --> 00:05:32,206
Don't throw that out.

115
00:05:32,619 --> 00:05:34,416
I think I need to call
child services

116
00:05:34,584 --> 00:05:36,835
and have Leslie taken away
from herself.

117
00:05:39,722 --> 00:05:41,590
Leslie, don't take this
the wrong way,

118
00:05:42,192 --> 00:05:45,092
but your house
is like a crazy person's garage.

119
00:05:46,830 --> 00:05:48,222
What is this birdhouse for?

120
00:05:50,225 --> 00:05:51,539
Can we get rid of it?

121
00:05:52,233 --> 00:05:54,297
- I might need it.
- What about this one?

122
00:05:55,475 --> 00:05:57,189
If two birds come along.

123
00:05:58,132 --> 00:06:00,859
Andy's gonna come over soon.
He can help us with all this stuff.

124
00:06:03,121 --> 00:06:05,721
You need a team of professionals
to help.

125
00:06:10,531 --> 00:06:11,557
I have an idea.

126
00:06:13,711 --> 00:06:15,411
Is this Maria Portlesman

127
00:06:15,536 --> 00:06:17,542
from "Organize your life
with Maria Portlesman"?

128
00:06:18,526 --> 00:06:19,544
April, come here!

129
00:06:20,275 --> 00:06:21,701
- What?
- Shake my hand.

130
00:06:22,007 --> 00:06:22,756
Why?

131
00:06:22,924 --> 00:06:26,635
To know if I've exfoliated too much.
I want Justin to respect my handshake.

132
00:06:27,117 --> 00:06:28,220
Why do you care?

133
00:06:28,462 --> 00:06:29,662
Justin is hip.

134
00:06:29,911 --> 00:06:31,911
Pawnee is the opposite of hip.

135
00:06:32,281 --> 00:06:35,531
People in this town
are just now getting into Nirvana.

136
00:06:35,928 --> 00:06:39,678
I can't tell them what's going to happen
to Kurt Cobain in 1994.

137
00:06:43,346 --> 00:06:45,070
It's like touching raw chicken.

138
00:06:45,351 --> 00:06:47,823
Damn it.
Gonna have to re-foliate 'em.

139
00:06:49,561 --> 00:06:52,256
I'll need some help
moving the boxes,

140
00:06:53,172 --> 00:06:54,961
- but it's doable.
- Thank God.

141
00:06:55,086 --> 00:06:56,957
- How much do I owe you?
- Please.

142
00:06:57,125 --> 00:06:59,626
No charge.
I just appreciate the chance

143
00:06:59,794 --> 00:07:02,713
to demonstrate how valuable my class is
to the rec center.

144
00:07:04,009 --> 00:07:06,174
And I hope you'll keep that in mind.

145
00:07:07,933 --> 00:07:09,052
I insist on paying.

146
00:07:10,381 --> 00:07:12,181
I insist on demonstrating.

147
00:07:12,432 --> 00:07:14,182
This isn't going
to affect my decision.

148
00:07:14,515 --> 00:07:15,726
Of course not.

149
00:07:15,894 --> 00:07:17,044
Stop winking.

150
00:07:18,958 --> 00:07:20,647
What are you cooking?

151
00:07:20,815 --> 00:07:21,732
Five courses.

152
00:07:22,165 --> 00:07:22,941
Of what?

153
00:07:24,360 --> 00:07:25,152
Of what?

154
00:07:26,085 --> 00:07:27,070
God.

155
00:07:30,349 --> 00:07:32,367
So how's it looking?
It smells great.

156
00:07:32,646 --> 00:07:34,411
The appetizers are almost ready.

157
00:07:34,579 --> 00:07:37,789
They are a very good sampling
of what my level one class does.

158
00:07:38,702 --> 00:07:41,460
Which I hope you'll keep in mind
when you're making your decisions.

159
00:07:41,628 --> 00:07:44,504
- Tania, like I said over the phone...
- I know.

160
00:07:45,079 --> 00:07:47,713
It won't influence your decision.
It's just that learning to cook

161
00:07:47,838 --> 00:07:49,968
- is very important in this day and age.
- Is it?

162
00:07:54,891 --> 00:07:56,442
Are you gonna be all right?

163
00:07:56,567 --> 00:07:58,395
What?
'Cause of Mark and Justin?

164
00:07:58,743 --> 00:08:01,396
They are just guests
at a really awesome dinner party

165
00:08:01,521 --> 00:08:03,023
and I'll treat them as such.

166
00:08:03,191 --> 00:08:04,978
That sounds very professional.

167
00:08:05,318 --> 00:08:08,737
I promise I'll not spit in anyone's food
unless they should request that I do.

168
00:08:09,010 --> 00:08:10,697
- Good.
- Did I do this right?

169
00:08:11,623 --> 00:08:12,773
I don't know.

170
00:08:15,549 --> 00:08:18,393
- Welcome to my humble abode.
- Well, thank you.

171
00:08:19,144 --> 00:08:21,541
This is way cleaner
than the last time I was here.

172
00:08:21,709 --> 00:08:22,709
You've been here?

173
00:08:24,643 --> 00:08:25,588
Right.

174
00:08:25,713 --> 00:08:27,464
When was mark here before?

175
00:08:31,223 --> 00:08:31,973
Sex.

176
00:08:33,655 --> 00:08:35,828
I can't wait to talk
to Justin again.

177
00:08:36,182 --> 00:08:40,157
Last time he told me how he snorkeled
with whale sharks in Madagascar.

178
00:08:40,882 --> 00:08:44,732
That was after I told him
that I sometimes go swimming at the "Y".

179
00:08:46,507 --> 00:08:47,757
Thank you, son.

180
00:08:49,460 --> 00:08:51,904
I believe you've arranged accommodations
for these.

181
00:08:52,365 --> 00:08:53,101
I have.

182
00:08:54,044 --> 00:08:57,077
You didn't happen to hear about
the incident with mayor Gunderson's dog?

183
00:08:57,245 --> 00:08:59,359
- It was a blood bath.
- No talking.

184
00:08:59,539 --> 00:09:03,208
Everybody stop talking, until Justin
gets here. Don't use up your stories.

185
00:09:05,253 --> 00:09:07,420
Somebody from animal services
is gonna get canned.

186
00:09:07,588 --> 00:09:10,572
My God, what part of not talking
do you not understand?

187
00:09:11,249 --> 00:09:13,515
Please have a good time
and shut your mouth.

188
00:09:13,640 --> 00:09:15,303
I want this dinner party to go well.

189
00:09:15,568 --> 00:09:17,890
There are very few things
I've asked for in this world.

190
00:09:18,015 --> 00:09:21,197
To build a new park from scratch,
to eventually become president,

191
00:09:21,322 --> 00:09:23,569
and to one day
solve a murder on a train.

192
00:09:23,694 --> 00:09:25,817
I think it's fair
to add this to the list.

193
00:09:28,908 --> 00:09:30,699
Guys, Justin's here.
That's Justin.

194
00:09:30,824 --> 00:09:32,624
Everybody.
Start talking.

195
00:09:42,827 --> 00:09:44,249
- I'm late.
- That's okay.

196
00:09:44,663 --> 00:09:48,086
- Welcome to my very interesting party.
- Andy Dwyer.

197
00:09:48,708 --> 00:09:50,171
Can't wait to catch up with him.

198
00:09:50,339 --> 00:09:52,424
All right, Justin,
may I take your coat, sir?

199
00:09:52,592 --> 00:09:54,142
Why, yeah. Thank you.

200
00:09:56,095 --> 00:09:58,452
I finally get to hang out
with all your friends tonight.

201
00:09:58,890 --> 00:10:00,578
It's gonna be super fun.
A blast.

202
00:10:00,703 --> 00:10:03,431
Really, the best and most exciting night
of your life.

203
00:10:03,556 --> 00:10:05,456
I cannot overhype it enough.

204
00:10:06,647 --> 00:10:08,273
All right, let's get started.

205
00:10:08,441 --> 00:10:10,324
- What's up, J.?
- How are you doing?

206
00:10:10,449 --> 00:10:12,090
- Justin.
- I have the swine flu.

207
00:10:12,215 --> 00:10:13,153
She doesn't.

208
00:10:18,769 --> 00:10:21,734
- Is that Justin's coat?
- Yeah. Isn't it awesome?

209
00:10:21,936 --> 00:10:25,619
He got it in Cambodia
when he was hiking mount Everest.

210
00:10:25,744 --> 00:10:28,710
- Let's put up gum in his pockets.
- That's genius. But I can't.

211
00:10:29,049 --> 00:10:32,756
Leslie's being super cool to me.
Can't screw over her boyfriend's jacket.

212
00:10:33,224 --> 00:10:35,199
I'll do it
when you're not in the room then.

213
00:10:35,594 --> 00:10:37,294
That would be terrible.

214
00:10:39,684 --> 00:10:42,140
I'm being sarcastic.
That would be awesome.

1
00:10:42,664 --> 00:10:44,682
God, India is so amazing.
Let me tell you something.

2
00:10:44,807 --> 00:10:47,085
That is my absolute
favorite place to travel.

3
00:10:47,210 --> 00:10:49,104
Where did you say
your parents were from?

4
00:10:49,229 --> 00:10:50,453
The south part.

5
00:10:50,734 --> 00:10:52,978
The southern part's always
the best part of anything.

6
00:10:53,603 --> 00:10:56,311
Have you been to the Kanmian mosque,
down in Tamil Nadu?

7
00:10:56,436 --> 00:10:59,405
Are you kidding?
My uncle practically runs the place.

8
00:10:59,530 --> 00:11:01,034
I've prayed there.
It's sick.

9
00:11:01,374 --> 00:11:03,057
Tell me everything, right now.

10
00:11:03,182 --> 00:11:05,641
One sec, I just gotta hit the loo,

11
00:11:05,766 --> 00:11:07,874
as those bastard British imperialists
would say.

12
00:11:10,842 --> 00:11:13,328
Last time I was in India,
I was eight years old.

13
00:11:13,453 --> 00:11:15,937
And I stayed inside the whole time
playing video games.

14
00:11:16,062 --> 00:11:17,362
I gotta bone up.

15
00:11:18,219 --> 00:11:20,685
Fourth largest coal reserves
in the world.

16
00:11:21,097 --> 00:11:23,526
This is the best
old fashioned I have ever had.

17
00:11:23,651 --> 00:11:25,458
Where did you find this bartender?

18
00:11:26,580 --> 00:11:29,393
I think, I don't know.
Like maybe the yellow pages?

19
00:11:29,564 --> 00:11:31,798
There's no way
that you're paying him enough.

20
00:11:31,923 --> 00:11:34,633
If you want, I could teach you
the secret to great...

21
00:11:36,694 --> 00:11:37,987
Ann, get over here.

22
00:11:38,397 --> 00:11:39,939
- I'm here, already.
- Good.

23
00:11:40,064 --> 00:11:41,810
Do you think
this party's going well?

24
00:11:41,935 --> 00:11:43,910
Can you see Justin?
Is he having fun?

25
00:11:44,217 --> 00:11:47,204
Justin is over there
talking to Mark happily.

26
00:11:48,925 --> 00:11:50,357
Okay, good.
Yeah, good.

27
00:11:50,482 --> 00:11:52,732
I'm just gonna casually make my...

28
00:11:53,671 --> 00:11:56,464
So my face is literally on fire,

29
00:11:56,973 --> 00:12:00,267
but it's this weird chemical
so no one can see the flames.

30
00:12:00,845 --> 00:12:02,804
I'm dancing around, freaking out.

31
00:12:02,972 --> 00:12:05,993
I end up having to dunk my face
into a bucket of sand.

32
00:12:06,118 --> 00:12:07,770
- Unreal.
- Unreal?

33
00:12:07,895 --> 00:12:09,577
Oh, my God.
That is unbelievable.

34
00:12:09,702 --> 00:12:11,145
Isn't it, Ann?
Unbelievable.

35
00:12:11,636 --> 00:12:14,279
It didn't happen to me.
It was a kid I knew from camp.

36
00:12:15,491 --> 00:12:17,735
But a great story is a great story.

37
00:12:18,435 --> 00:12:20,974
Plus the kid can't tell it.
His mouth melted.

38
00:12:23,699 --> 00:12:25,201
Dinner is served.

39
00:12:26,176 --> 00:12:27,176
Excellent.

40
00:12:31,979 --> 00:12:33,629
I'm sorry, are we late?

41
00:12:35,326 --> 00:12:37,599
What the hell are you doing?

42
00:12:37,724 --> 00:12:40,198
How could you invite Wendy
when Ron's here?

43
00:12:40,323 --> 00:12:42,649
He's gonna wrap her up
in his moustache and take her home.

44
00:12:42,774 --> 00:12:45,020
We agreed to invite
interesting people for Justin,

45
00:12:45,145 --> 00:12:46,707
and Wendy is an interesting person.

46
00:12:46,832 --> 00:12:48,889
And I told you
that you should talk to her.

47
00:12:49,014 --> 00:12:51,135
- Now's your chance.
- That makes sense.

48
00:12:51,260 --> 00:12:54,130
No, it doesn't!
Not cool. At all!

49
00:12:56,574 --> 00:12:59,037
- He'll get over it.
- No, I won't!

50
00:12:59,786 --> 00:13:03,000
The first course
is a mexican bean soup.

51
00:13:06,160 --> 00:13:08,111
Are these peppers for consuming?

52
00:13:08,236 --> 00:13:10,236
I wouldn't.
They're very hot.

53
00:13:10,361 --> 00:13:12,007
I'm gonna give it a try.

54
00:13:14,786 --> 00:13:16,935
- Hot.
- You're very brave.

55
00:13:17,310 --> 00:13:19,528
Please. That was one of
the tiny ones.

56
00:13:19,653 --> 00:13:21,502
<i>This is how you eat it</i>

57
00:13:21,627 --> 00:13:22,967
Don't do that, Tom.

58
00:13:33,122 --> 00:13:34,408
This is something.

59
00:13:34,533 --> 00:13:36,022
Justin, Derek and Ben are gay.

60
00:13:36,625 --> 00:13:38,088
But often on occasion,

61
00:13:38,442 --> 00:13:40,688
April will have relations
with Derek.

62
00:13:41,367 --> 00:13:43,567
Right? Crazy stuff.
Discuss that.

63
00:13:44,155 --> 00:13:47,283
There's not much more to discuss.
You kind of explained it all.

64
00:13:49,683 --> 00:13:51,245
Who else is gay?

65
00:13:52,583 --> 00:13:54,415
Tom, I need your help.
There was a lull.

66
00:13:54,587 --> 00:13:55,971
I saw Justin yawn.

67
00:13:56,293 --> 00:13:58,903
Tell me this is a sign of excitement
in India or something.

68
00:13:59,028 --> 00:14:00,129
Tom, focus.

69
00:14:00,460 --> 00:14:01,839
God, you're no help.

70
00:14:02,247 --> 00:14:05,256
Yes, just to be clear,
this will in no way influence me.

71
00:14:05,381 --> 00:14:08,721
It won't affect my decision and it has
no bearing on the future of your class.

72
00:14:10,686 --> 00:14:12,642
Hey, little buddy.
Everything okay?

73
00:14:13,071 --> 00:14:14,531
That took a long time.

74
00:14:14,656 --> 00:14:16,604
Poor Tommy.
He has a very frail colon.

75
00:14:17,576 --> 00:14:19,089
I feel your pain, brother.

76
00:14:19,214 --> 00:14:22,594
Hottest thing I ever put into my mouth
was in this remote village in Brazil.

77
00:14:22,719 --> 00:14:23,862
This sounds good.

78
00:14:23,987 --> 00:14:26,197
They take a little bit
of scorpion poison

79
00:14:26,365 --> 00:14:27,988
and they serve it to you...

80
00:14:28,231 --> 00:14:29,575
I wonder who that is.

81
00:14:29,866 --> 00:14:31,412
- Who could that be?
- More people.

82
00:14:31,537 --> 00:14:33,037
A belly dancer?

83
00:14:34,721 --> 00:14:35,721
What?

84
00:14:37,464 --> 00:14:38,793
It's weird, but...

85
00:14:39,446 --> 00:14:40,962
she just showed up.

86
00:14:41,431 --> 00:14:43,548
I'm not the kind of person
that's gonna throw

87
00:14:43,716 --> 00:14:45,390
someone out or, you know,

88
00:14:45,676 --> 00:14:46,759
not let them in

89
00:14:46,927 --> 00:14:48,386
if they randomly show up.

90
00:14:49,680 --> 00:14:53,182
I don't know how they all got here.

91
00:14:53,689 --> 00:14:56,602
You know, people hear
about a killer party at Knope's house,

92
00:14:56,770 --> 00:14:58,710
at Knope's house,
they all come a-runnin'.

93
00:15:01,900 --> 00:15:03,568
You don't do origami every day,
do you?

94
00:15:04,290 --> 00:15:06,725
They brought in a little entertainment,
thank God.

95
00:15:06,850 --> 00:15:09,323
'Cause that Justin is so boring.

96
00:15:09,491 --> 00:15:11,902
What are you talking about?
He's incredible.

97
00:15:12,027 --> 00:15:13,416
Yeah, he's not gay,

98
00:15:13,541 --> 00:15:15,830
but if he were, I would totally
break up with Ben for him.

99
00:15:15,998 --> 00:15:17,472
Yeah, I would understand.

100
00:15:17,597 --> 00:15:19,709
- He's amazing, right, April?
- Right, April?

101
00:15:19,877 --> 00:15:21,277
Don't we love him?

102
00:15:22,628 --> 00:15:24,928
Yeah, he's actually kind of awesome.

103
00:15:27,869 --> 00:15:29,240
You're underage.

104
00:15:29,939 --> 00:15:31,913
I could lose my liquor license,
I'm sorry.

105
00:15:32,798 --> 00:15:34,140
Are you having fun?

106
00:15:34,425 --> 00:15:35,891
All I can say is this is not

107
00:15:36,059 --> 00:15:37,704
not what I was expecting.

108
00:15:38,545 --> 00:15:41,147
- I cannot believe there's more.
- There's not.

109
00:15:43,790 --> 00:15:44,840
Miss Knope.

110
00:15:45,295 --> 00:15:48,487
I understand that you're currently
evaluating the rec center teachers.

111
00:15:48,943 --> 00:15:50,340
You're doing what?

112
00:15:51,271 --> 00:15:52,964
I demand an evaluation.

113
00:15:54,676 --> 00:15:55,911
What course do you teach?

114
00:15:56,079 --> 00:15:58,828
When it comes to choosing
accounting software,

115
00:15:58,953 --> 00:16:01,083
there's no accounting for taste.

116
00:16:03,337 --> 00:16:05,261
The advantage of Quickbooks Pro

117
00:16:05,386 --> 00:16:08,424
is that it's easy to assemble
a list of vendors...

118
00:16:08,625 --> 00:16:10,125
Well, this is great.

119
00:16:10,622 --> 00:16:12,183
I'm heading home,

120
00:16:12,308 --> 00:16:14,895
so as not to have to be here
anymore.

121
00:16:15,408 --> 00:16:17,257
My office first thing tomorrow.

122
00:16:17,423 --> 00:16:20,355
Yeah, I think we're probably
gonna take off too.

123
00:16:20,646 --> 00:16:22,688
The demonstration
takes another 45 minutes.

124
00:16:23,102 --> 00:16:25,847
Yeah, guys, please.
Everybody, can you just stay, okay?

125
00:16:25,972 --> 00:16:27,472
Justin, would you...

126
00:16:31,517 --> 00:16:34,241
I promise you, Miss Knope,
this disciplinary committee will go over

127
00:16:34,409 --> 00:16:36,118
every detail of what happened
last night

128
00:16:36,286 --> 00:16:39,677
and we will determine whether you are
guilty of abusing your power.

129
00:16:40,076 --> 00:16:42,083
Do you have anything
to say before we begin?

130
00:16:43,634 --> 00:16:45,691
Just that I'm looking forward

131
00:16:45,816 --> 00:16:47,296
to the truth coming out.

132
00:16:47,464 --> 00:16:49,497
I would like to say that Leslie

133
00:16:49,622 --> 00:16:52,760
is incapable of using people
for her own gain.

134
00:16:54,177 --> 00:16:55,572
But I can't because

135
00:16:55,697 --> 00:16:57,859
Leslie loves using people
for her own gain.

136
00:16:58,760 --> 00:17:01,977
If I may, Phil, I'd like
to say something to Tom on the record.

137
00:17:02,207 --> 00:17:03,964
This has all been on the record.

138
00:17:04,089 --> 00:17:06,620
I'm very sorry
that I invited Wendy to the party.

139
00:17:06,745 --> 00:17:09,735
- Is Wendy one of the teachers?
- No, she's my soon-to-be ex-wife.

140
00:17:10,025 --> 00:17:12,180
And hopefully
then-to-be future wife.

141
00:17:12,711 --> 00:17:14,333
Look, I'm really sorry.

142
00:17:14,458 --> 00:17:16,867
I put a new romance
in front of an old friendship.

143
00:17:19,121 --> 00:17:20,637
I put a beautiful man

144
00:17:20,762 --> 00:17:22,623
in front of an adorable man child.

145
00:17:22,791 --> 00:17:23,817
You're ruining it.

146
00:17:23,942 --> 00:17:25,691
I put a big white stallion

147
00:17:25,816 --> 00:17:27,545
in front of a little brown pony.

148
00:17:27,973 --> 00:17:29,213
Totally uncalled for.

149
00:17:29,592 --> 00:17:31,215
Ron Swanson will be next.

150
00:17:31,619 --> 00:17:33,678
When it comes
to government hearings,

151
00:17:33,803 --> 00:17:36,234
the only type of witness
I enjoy being

152
00:17:36,359 --> 00:17:37,709
is a hostile one.

153
00:17:38,163 --> 00:17:39,791
That's why I intend to answer

154
00:17:39,916 --> 00:17:43,044
every one of their questions
with a question.

155
00:17:44,438 --> 00:17:46,967
Were you aware
that all of the entertainment and food

156
00:17:47,092 --> 00:17:48,524
was provided by teachers?

157
00:17:48,860 --> 00:17:51,360
Would I have stayed if I knew that?

158
00:17:51,528 --> 00:17:53,821
- I don't know. Would you have?
- Would <I>you</I> have?

159
00:17:54,442 --> 00:17:55,891
No, I wouldn't have.

160
00:17:56,075 --> 00:17:58,452
Did you hear Leslie
make any promises?

161
00:17:58,577 --> 00:18:00,577
What constitutes a promise?

162
00:18:01,048 --> 00:18:02,121
A <i>quid pro quo.</i>

163
00:18:02,994 --> 00:18:04,123
Do you know Latin?

164
00:18:07,746 --> 00:18:08,846
Are we done?

165
00:18:10,933 --> 00:18:12,392
Can I get a shine?

166
00:18:13,310 --> 00:18:15,110
No, I'm sorry.
We're closed.

167
00:18:15,235 --> 00:18:16,844
Due to betrayment.

168
00:18:17,347 --> 00:18:19,930
I'm sorry. But I don't know
what you're talking about.

169
00:18:20,733 --> 00:18:22,413
Why don't you ask
your new best friend?

170
00:18:22,538 --> 00:18:25,482
- Whose name is Justin.
- Justin?

171
00:18:28,090 --> 00:18:29,545
I would...

172
00:18:30,451 --> 00:18:32,236
but he's probably too busy...

173
00:18:32,832 --> 00:18:35,232
cleaning gum out
of his coat pockets.

174
00:18:40,010 --> 00:18:40,993
Unbetrayed.

175
00:18:42,543 --> 00:18:44,163
So you don't work
for the government,

176
00:18:44,331 --> 00:18:46,248
and you had no idea
that these people

177
00:18:46,417 --> 00:18:48,448
were teachers
from the local rec center.

178
00:18:48,945 --> 00:18:51,801
I was simply attending a dinner party
at my friend's house.

179
00:18:51,926 --> 00:18:54,621
- Why is he here, Ms. Knope?
- That will become clear.

180
00:18:54,746 --> 00:18:57,009
Mr. Anderson, could you please
describe that evening?

181
00:18:57,342 --> 00:18:59,601
Well, I had a very pleasant time.

182
00:18:59,726 --> 00:19:01,982
And from my perspective,
there was no abuse of power

183
00:19:02,107 --> 00:19:04,781
with respect to Ms. Knope
and any of her various guests.

184
00:19:04,906 --> 00:19:06,435
Could you define pleasant?

185
00:19:06,765 --> 00:19:09,024
Did you have a good time?
Did you have a great time?

186
00:19:09,149 --> 00:19:11,690
Be specific, and do you remember
that you're under oath?

187
00:19:12,153 --> 00:19:13,650
I had an amazing time.

188
00:19:14,130 --> 00:19:15,569
But you fell asleep.

189
00:19:15,890 --> 00:19:18,197
God, I'm so sorry.
No... Look, I... you know,

190
00:19:18,322 --> 00:19:21,380
I'd been given a case the night before.
So I had to stay up all night working.

191
00:19:21,505 --> 00:19:24,286
- I didn't want to cancel.
- You should have told me.

192
00:19:24,454 --> 00:19:26,955
Are you kidding me?
I had an awesome time.

193
00:19:28,458 --> 00:19:31,043
Which had nothing to do
with the rec center teachers.

194
00:19:31,801 --> 00:19:33,351
No further questions.

195
00:19:34,571 --> 00:19:35,917
We're gonna take a break,

196
00:19:36,234 --> 00:19:38,666
and we'll let you know
what we decide in about an hour.

197
00:19:39,224 --> 00:19:40,094
Great.

198
00:19:40,911 --> 00:19:42,596
I will see you later.

199
00:19:42,848 --> 00:19:45,099
I'm in the middle of official business
so I can't talk.

200
00:19:45,267 --> 00:19:46,809
- But yes, you will.
- Great.

201
00:19:51,650 --> 00:19:53,982
So did you get that?
He said "amazing" and "awesome".

202
00:19:54,247 --> 00:19:56,325
So I'm gonna drive up
and see him next weekend.

203
00:19:56,450 --> 00:19:58,757
There's some kabuki theater
festival happening.

204
00:19:58,882 --> 00:20:01,066
Great.
You're so relaxed.

205
00:20:01,191 --> 00:20:02,866
You're not freaking out at all?

206
00:20:04,681 --> 00:20:06,120
We've reached our decision.

207
00:20:07,034 --> 00:20:09,498
We're not going to take
any further action at this time.

208
00:20:10,919 --> 00:20:13,673
It really helped
that you asked for this hearing yourself

209
00:20:13,798 --> 00:20:16,791
and that you made financial restitution
out of your own pocket.

210
00:20:16,916 --> 00:20:19,716
But I think we both know
you skirted a line here.

211
00:20:22,290 --> 00:20:24,342
You turned yourself in?
Why?

212
00:20:24,467 --> 00:20:27,212
Well, I did something wrong
and I felt bad.

213
00:20:27,337 --> 00:20:30,643
So this morning, I donated
$1,000 to the rec center

214
00:20:30,768 --> 00:20:33,514
so everybody
could keep teaching their classes.

215
00:20:33,639 --> 00:20:36,692
- So why did you ask for the hearing?
- Are you kidding, Ann?

216
00:20:37,007 --> 00:20:38,608
It's every girl's dream
to ask a dude

217
00:20:38,733 --> 00:20:41,128
how their date went
under penalty of perjury.

218
00:20:41,253 --> 00:20:42,562
I'm not proud of my actions,

219
00:20:42,687 --> 00:20:45,996
but the most important thing is there is
an official government document

220
00:20:46,121 --> 00:20:48,646
that proves my dinner party
kicked ass.

221
00:20:52,475 --> 00:20:53,524
Tommy Hav.

222
00:20:53,649 --> 00:20:55,335
- J-train.
- What's up, buddy?

223
00:20:55,504 --> 00:20:58,015
This might be none of my business,
but...

224
00:20:58,848 --> 00:21:00,257
You need to ask Wendy out.

225
00:21:01,844 --> 00:21:03,151
For reals this time.

226
00:21:03,922 --> 00:21:05,387
Did Leslie tell you?

227
00:21:05,848 --> 00:21:07,252
She didn't need to.

228
00:21:08,248 --> 00:21:09,328
I saw it.

229
00:21:10,200 --> 00:21:11,265
I'm not gay,

230
00:21:11,622 --> 00:21:13,757
but you're the most incredible man
I've ever met.

231
00:21:13,882 --> 00:21:15,507
That doesn't sound gay at all.

232
00:21:16,527 --> 00:21:17,564
See you later.

233
00:21:17,689 --> 00:21:19,169
- Take care.
- Bye, brother.

234
00:21:19,294 --> 00:21:22,499
You sure you don't wanna come
hang out for a minute?

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
