1
00:00:03,792 --> 00:00:06,193
It's pretty great having Andy
working in the building.

2
00:00:06,318 --> 00:00:07,752
The guy is so much fun.

3
00:00:07,877 --> 00:00:09,102
His new thing...

4
00:00:09,227 --> 00:00:10,527
Piggyback rides.

5
00:00:10,969 --> 00:00:12,254
Anytime you want.

6
00:00:12,679 --> 00:00:14,229
Piggyback! Piggyback!

7
00:00:14,473 --> 00:00:15,473
Move!

8
00:00:15,724 --> 00:00:16,993
Piggyback. Bam!

9
00:00:26,796 --> 00:00:27,796
Giddyup!

10
00:00:30,855 --> 00:00:32,657
Councilman Howser.
Hello.

11
00:00:32,825 --> 00:00:34,533
- Hello.
- Did you get my proposal

12
00:00:34,858 --> 00:00:37,787
for the possible re-zoning
of lot 48?

13
00:00:38,082 --> 00:00:39,689
I have been busy.

14
00:00:39,874 --> 00:00:43,325
But I think it would be a really great
thing for the neighborhood.

15
00:00:43,450 --> 00:00:46,212
I don't doubt it, but it's really
a question of resource allocation.

16
00:00:46,562 --> 00:00:48,004
I completely understand that.

17
00:00:48,129 --> 00:00:50,174
But we know
that if we want to find the money,

18
00:00:50,342 --> 00:00:51,175
we can.

19
00:00:52,187 --> 00:00:53,177
I'm running late.

20
00:00:53,345 --> 00:00:54,553
I'll walk with you.

21
00:00:54,678 --> 00:00:57,098
See, the thing is,
when we allocate money for Parks...

22
00:00:57,266 --> 00:00:58,666
- Feygnasse Team -

23
00:00:58,791 --> 00:01:00,458
. :: La Fabrique ::.

24
00:01:01,105 --> 00:01:02,897
�pisode 210
<i>Hunting Trip</i>

25
00:01:04,700 --> 00:01:05,720
mpm

26
00:01:06,567 --> 00:01:07,677
lestat78

27
00:01:17,664 --> 00:01:19,837
Just a reminder,
tomorrow's a half day.

28
00:01:19,962 --> 00:01:21,918
Jerry, Mark, and I have to conduct

29
00:01:22,499 --> 00:01:24,417
the annual trail survey

30
00:01:24,585 --> 00:01:26,067
at Slippery Elm Park.

31
00:01:26,580 --> 00:01:28,587
I had the trail survey hats made

32
00:01:28,712 --> 00:01:30,636
to commemorate the trip.

33
00:01:31,091 --> 00:01:32,191
Nice, Jerry.

34
00:01:33,079 --> 00:01:35,176
And if you have
any questions about the details,

35
00:01:35,301 --> 00:01:37,430
feel free to shoot me an email.

36
00:01:41,613 --> 00:01:43,782
The only trails
he's gonna be surveying

37
00:01:43,907 --> 00:01:45,859
are trails of lies and deception.

38
00:01:46,090 --> 00:01:47,940
Ron has a special deal
with the park rangers.

39
00:01:48,406 --> 00:01:50,652
Every November,
they let him use their cabin,

40
00:01:50,777 --> 00:01:53,863
so he can go on a secret hunting trip
with all the guys in the office.

41
00:01:54,198 --> 00:01:56,073
Not all the guys.
He's never taken me.

42
00:01:56,334 --> 00:01:57,734
Fine, all the men.

43
00:01:58,455 --> 00:02:00,020
Ron, let's cut the bull.

44
00:02:00,145 --> 00:02:03,454
I want me, Tom, and all the other ladies
included on your hunting trip.

45
00:02:04,361 --> 00:02:05,511
Hunting trip?

46
00:02:05,903 --> 00:02:08,559
We're doing a trail survey, Leslie.

47
00:02:08,921 --> 00:02:10,921
You're literally listening
to turkey calls.

48
00:02:11,990 --> 00:02:13,420
Is this not rap?

49
00:02:13,992 --> 00:02:15,460
- Come on.
- All right, look.

50
00:02:15,585 --> 00:02:17,750
It's not just a hunting trip.

51
00:02:19,342 --> 00:02:20,723
It's a tradition.

52
00:02:20,891 --> 00:02:22,791
I am really good at hunting.

53
00:02:23,016 --> 00:02:25,561
And I'm even better
at being one of the guys.

54
00:02:26,395 --> 00:02:27,995
It's a work event,

55
00:02:29,047 --> 00:02:31,275
so, legally,
I can't stop you from coming.

56
00:02:31,400 --> 00:02:34,150
This is gonna be so fun!
I'll bring s'mores.

57
00:02:34,464 --> 00:02:35,914
And just like that,

58
00:02:37,031 --> 00:02:39,731
the one tiny aspect of government
I enjoyed

59
00:02:40,215 --> 00:02:42,515
was clubbed to death before my eyes.

60
00:02:43,038 --> 00:02:45,638
April, I need you
to do something for me.

61
00:02:45,763 --> 00:02:47,613
I'm going hunting tomorrow,

62
00:02:48,126 --> 00:02:50,645
so call the State Parks Office
and get verbal confirmation

63
00:02:50,770 --> 00:02:52,463
that our budget documentation is in.

64
00:02:53,134 --> 00:02:54,898
Can I tell you
the 16 tracking number,

65
00:02:55,023 --> 00:02:57,051
or do you want me to write it down?

66
00:02:57,423 --> 00:03:00,168
- Can you handle this?
- You want me to dial a number,

67
00:03:00,293 --> 00:03:02,222
and then read another number
out loud?

68
00:03:03,515 --> 00:03:04,892
Can you handle this?

69
00:03:08,557 --> 00:03:09,857
Well, try. Okay?

70
00:03:10,273 --> 00:03:11,544
And if you do it,

71
00:03:11,669 --> 00:03:13,816
I will name the first turkey I shoot
after you.

72
00:03:15,158 --> 00:03:16,158
Cool.

73
00:03:20,529 --> 00:03:22,304
Ann, ready to bag some birds?

74
00:03:22,429 --> 00:03:24,710
No, but I am ready
to relax by the fire

75
00:03:24,835 --> 00:03:27,085
and get my real simple magazine on.

76
00:03:27,541 --> 00:03:29,438
If you change your mind,
you are now officially

77
00:03:29,563 --> 00:03:31,126
a licensed Indiana hunter.

78
00:03:31,854 --> 00:03:32,854
Gross.

79
00:03:38,625 --> 00:03:41,807
<i>Hello, you have reached
the Indiana State Parks Department.</i>

80
00:03:41,932 --> 00:03:45,061
<i>Your call is very important to us.
Please stay on the line.</i>

81
00:03:55,558 --> 00:03:57,439
- Here we go.
- Here we are.

82
00:03:57,695 --> 00:03:59,029
There's Ron.

83
00:03:59,197 --> 00:04:01,615
It's pretty!
I thought it was gonna be gross.

84
00:04:05,161 --> 00:04:06,352
Holy cow.

85
00:04:07,456 --> 00:04:08,996
It is good to be back!

86
00:04:09,456 --> 00:04:10,606
Sneak attack!

87
00:04:12,654 --> 00:04:14,324
- Damn it!
- I'm the pants king!

88
00:04:14,449 --> 00:04:16,170
- Bow to me.
- Bow. I bow.

89
00:04:17,655 --> 00:04:19,155
I'm the pants queen!

90
00:04:20,908 --> 00:04:22,136
What the hell!

91
00:04:23,242 --> 00:04:24,892
Bow to the pants queen.

92
00:04:25,578 --> 00:04:28,482
When you're out with the boys,
you gotta be ready for a good pantsing.

93
00:04:28,719 --> 00:04:32,319
That's why I have suspenders
that connect my bra to my jeans.

94
00:04:33,642 --> 00:04:35,274
Ron "P. Diddy" combs.

95
00:04:35,683 --> 00:04:38,402
I have to admit,
this place is pretty tight.

96
00:04:39,005 --> 00:04:40,946
May I interest anyone in some chew?

97
00:04:41,282 --> 00:04:42,281
Nice touch.

98
00:04:43,327 --> 00:04:44,740
Let me get some of that.

99
00:04:44,910 --> 00:04:46,410
- You sure?
- Yeah.

100
00:04:46,535 --> 00:04:48,745
I would not have pegged you
as a user of mouth tobacco.

101
00:04:49,712 --> 00:04:51,240
I'm full of surprises, Ron.

102
00:04:51,365 --> 00:04:53,765
Man, my stomach's a little upset.

103
00:04:54,096 --> 00:04:55,461
I feel a little queasy.

104
00:04:55,629 --> 00:04:58,130
That might be the chew.
You could spit it out.

105
00:04:58,782 --> 00:04:59,798
I swallowed it.

106
00:04:59,966 --> 00:05:01,425
You're supposed to swallow it?

107
00:05:10,406 --> 00:05:11,406
All right.

108
00:05:13,146 --> 00:05:14,449
Poor little buddy.

109
00:05:14,574 --> 00:05:16,780
That's why they call it chew
and not swallow.

110
00:05:16,905 --> 00:05:19,234
- Am I right, Ron?
- Yes, you are right.

111
00:05:21,029 --> 00:05:22,779
All right, safety basics.

112
00:05:24,281 --> 00:05:26,700
Tell me why it's bad
to look down the barrel of your gun?

113
00:05:27,079 --> 00:05:28,544
Is that a trick question?

114
00:05:28,669 --> 00:05:30,197
No, Donna, don't!
Please.

115
00:05:30,663 --> 00:05:33,628
Rule number one,
do not point the weapon at a person.

116
00:05:33,753 --> 00:05:35,417
That includes your own face.

117
00:05:35,812 --> 00:05:38,347
Now, every year before we go
on our first hunt,

118
00:05:38,472 --> 00:05:40,472
we do a toast, so grab a beer.

119
00:05:44,764 --> 00:05:45,928
To the hunt.

120
00:05:46,096 --> 00:05:47,021
Hear, hear!

121
00:05:47,146 --> 00:05:48,596
And to the hunters.

122
00:05:49,525 --> 00:05:51,625
The only way to defeat the beast

123
00:05:51,913 --> 00:05:53,813
is to find the beast within.

124
00:05:54,187 --> 00:05:56,313
- Pretty good.
- Hear, hear!

125
00:05:56,606 --> 00:05:57,555
Right on!

126
00:05:57,680 --> 00:05:59,025
Ron, your toast sucked.

127
00:05:59,150 --> 00:06:00,484
The traditional toast

128
00:06:00,797 --> 00:06:02,323
is "to the hunt."

129
00:06:02,966 --> 00:06:04,705
And it is said by me.

130
00:06:05,448 --> 00:06:06,611
You all set, Mark?

131
00:06:06,736 --> 00:06:08,698
Maybe we could do mixed doubles.

132
00:06:08,823 --> 00:06:10,077
Boy/girl, boy/girl.

133
00:06:10,386 --> 00:06:12,663
You said
that we were gonna hunt together.

134
00:06:13,130 --> 00:06:16,416
I always forget because you're so pretty
you're not used to rejection.

135
00:06:17,143 --> 00:06:18,877
I have to hunt with Ron.

136
00:06:19,087 --> 00:06:21,302
- Ann, we'll go together.
- Perfect.

137
00:06:21,557 --> 00:06:25,307
I hope you're ready to discuss
some college bowl game scenarios.

138
00:06:26,198 --> 00:06:27,029
Bully.

139
00:06:37,426 --> 00:06:38,729
Can you come here?

140
00:06:39,232 --> 00:06:41,289
- What's up?
- I've been on hold for an hour.

141
00:06:41,414 --> 00:06:43,360
I really have to pee.
Can you sit here

142
00:06:43,570 --> 00:06:44,827
and just listen?

143
00:06:44,952 --> 00:06:45,988
Please? Great.

144
00:06:46,494 --> 00:06:50,094
And if they answer, can you just read
those numbers out loud?

145
00:06:53,627 --> 00:06:55,497
Now here's the female
adolescent turkey.

146
00:06:57,679 --> 00:07:00,356
Could you hear the difference?
The turkeys can.

147
00:07:06,699 --> 00:07:08,844
We do that the first time
one of us hits something.

148
00:07:09,012 --> 00:07:09,962
Cool.

149
00:07:10,087 --> 00:07:11,680
No. You missed it.

150
00:07:13,063 --> 00:07:14,904
Look, Ron, I know this weekend

151
00:07:15,029 --> 00:07:18,089
you were looking forward
to a lot of man on man on man action,

152
00:07:18,214 --> 00:07:21,927
but I wanted to say I'm very grateful
that you let me come along on this trip.

153
00:07:22,052 --> 00:07:23,152
That's fine.

154
00:07:23,277 --> 00:07:26,641
I'm just glad you didn't end up inviting
more of the motor...

155
00:07:27,799 --> 00:07:28,946
What the***?

156
00:07:29,783 --> 00:07:32,376
- What the hell? Give me some warning.
- I saw a quail.

157
00:07:32,640 --> 00:07:34,663
Sorry, man.
You snooze, you loose.

158
00:07:41,961 --> 00:07:44,838
I think this is gonna be a really good
bonding sesh for me and Ron.

159
00:07:45,006 --> 00:07:46,757
Guys love it when you can show them

160
00:07:46,882 --> 00:07:49,217
you're better than they are
at something they love.

161
00:07:50,824 --> 00:07:52,036
Check this out.

162
00:07:52,722 --> 00:07:54,342
<i>I am on hold</i>

163
00:07:54,597 --> 00:07:56,308
<i>with the State Parks Department</i>

164
00:07:57,087 --> 00:07:58,477
<i>I am on hold</i>

165
00:07:58,897 --> 00:08:00,853
<i>so suck on my butt</i>

166
00:08:01,148 --> 00:08:01,938
Nice.

167
00:08:06,027 --> 00:08:07,757
They didn't answer, obviously.

168
00:08:07,882 --> 00:08:09,013
Where is everyone?

169
00:08:09,138 --> 00:08:10,989
- Hunting trip.
- Hunting trip?

170
00:08:11,254 --> 00:08:12,240
Did Mark go?

171
00:08:12,892 --> 00:08:15,118
That's cool.
At least he's not with Ann.

172
00:08:15,286 --> 00:08:17,128
- No, Ann's there.
- God!

173
00:08:17,833 --> 00:08:20,024
How come he gets to do
all the things I want to do?

174
00:08:20,149 --> 00:08:21,315
Go hunting...

175
00:08:21,800 --> 00:08:22,800
Ann...

176
00:08:23,044 --> 00:08:24,669
Maybe a deer will eat him.

177
00:08:28,758 --> 00:08:29,846
That'd be awesome,

178
00:08:29,971 --> 00:08:32,177
but I don't think
that will happen, probably.

179
00:08:32,387 --> 00:08:35,338
You surprised that my breasts
didn't throw my aim off?

180
00:08:35,463 --> 00:08:36,713
Leslie, please.

181
00:08:37,085 --> 00:08:39,935
I don't care that you're a girl,
I just don't like change.

182
00:08:40,103 --> 00:08:43,188
I like the same place, the same people,
telling the same stories,

183
00:08:43,356 --> 00:08:45,249
and seeing who can bag
the most turkeys.

184
00:08:45,775 --> 00:08:48,652
You like hunting with the same people
because you can beat 'em.

185
00:08:51,113 --> 00:08:53,198
100 bucks says I bag
more birds than you.

186
00:08:53,684 --> 00:08:54,491
You're on.

187
00:08:55,087 --> 00:08:57,561
Let's split up.
I do it better alone.

188
00:08:57,787 --> 00:08:59,432
Yeah, you do. See?

189
00:08:59,860 --> 00:09:01,178
Just one of the guys.

190
00:09:03,357 --> 00:09:05,387
Your favorite cake
can't be birthday cake.

191
00:09:05,512 --> 00:09:08,315
That's like saying, your favorite
kind of cereal is breakfast cereal.

192
00:09:08,440 --> 00:09:09,923
I love breakfast cereal.

193
00:09:10,091 --> 00:09:12,634
Look, some kind of bird. Let's kill it.
You talkin' to me, bitch?

194
00:09:17,824 --> 00:09:19,426
What were you aiming at?

195
00:09:19,551 --> 00:09:20,454
Nothin'.

196
00:09:28,685 --> 00:09:30,485
I can understand
why people like that.

197
00:09:37,090 --> 00:09:38,615
And keeping one's eyes open

198
00:09:38,740 --> 00:09:40,662
is always a good rule
of thumb around guns.

199
00:09:41,785 --> 00:09:43,479
This is such a great day.

200
00:09:43,604 --> 00:09:47,304
See, at my house I've got a wife
and three beautiful daughters.

201
00:09:48,003 --> 00:09:50,603
But this trip,
it is the one time of year

202
00:09:51,052 --> 00:09:52,948
I get to pee standing up.

203
00:09:56,646 --> 00:09:58,046
I love that sound.

204
00:10:00,111 --> 00:10:01,361
I've been shot!

205
00:10:03,447 --> 00:10:04,697
I've been shot!

206
00:10:12,968 --> 00:10:14,918
Somebody shot me in the head!

207
00:10:21,954 --> 00:10:22,954
My God!

208
00:10:23,481 --> 00:10:24,481
Over here.

209
00:10:24,833 --> 00:10:26,249
Ron, I have your hat.

210
00:10:26,989 --> 00:10:30,099
- Are you in a lot of pain?
- I was shot in the head with a shotgun!

211
00:10:30,224 --> 00:10:31,776
Ron, it's not that serious.

212
00:10:31,901 --> 00:10:33,645
I just need you to stay calm.

213
00:10:33,966 --> 00:10:37,016
I'm just gonna stay angry.
I find that relaxes me.

214
00:10:37,470 --> 00:10:39,804
Ron, we called 911,
and they're gonna send a ranger.

215
00:10:40,260 --> 00:10:42,132
Damn! This is a mess.

216
00:10:42,470 --> 00:10:44,514
The rangers won't let us
come back next year.

217
00:10:44,639 --> 00:10:46,601
We're not gonna think
about that right now.

218
00:10:46,726 --> 00:10:48,605
Can you put him on the day bed

219
00:10:48,773 --> 00:10:50,847
- in the carcass room?
- Day bed?

220
00:10:53,277 --> 00:10:54,477
Is that Donna?

221
00:11:03,843 --> 00:11:04,993
Are you okay?

222
00:11:05,425 --> 00:11:06,790
What is it, your heart?

223
00:11:06,915 --> 00:11:08,798
Are you having trouble breathing?

224
00:11:08,923 --> 00:11:09,923
Someone...

225
00:11:16,149 --> 00:11:18,801
- Here's your scotch, Ron.
- Jerry's here. Here's your scotch.

226
00:11:18,969 --> 00:11:20,069
There we go.

227
00:11:20,396 --> 00:11:23,826
Hey, you know what is great?
Ann's gonna take care of you.

228
00:11:23,951 --> 00:11:26,383
And Ann is the best nurse
in north America.

229
00:11:26,508 --> 00:11:28,208
All right.
There we go.

230
00:11:29,705 --> 00:11:32,148
- What? You okay?
- Did you... Did you shoot me?

231
00:11:32,499 --> 00:11:33,346
What?

232
00:11:34,485 --> 00:11:38,029
There was a bird kind of near me,
and I know you want to prove yourself.

233
00:11:38,197 --> 00:11:39,738
No, I swear I didn't.

234
00:11:39,863 --> 00:11:41,783
I swear to God,
I've never shot anyone.

235
00:11:41,951 --> 00:11:43,831
You better find out who it was.

236
00:11:43,956 --> 00:11:45,974
And then purchase them a coffin

237
00:11:46,099 --> 00:11:48,038
because I'm gonna rip them apart!

238
00:11:50,459 --> 00:11:51,459
Marco.

239
00:11:52,658 --> 00:11:53,508
Polo.

240
00:12:07,834 --> 00:12:10,886
How are you feeling? Are you dizzy?
Are you light-headed?

241
00:12:11,011 --> 00:12:13,497
When I look at my palm,
I see a lady's mouth

242
00:12:13,622 --> 00:12:15,172
french kissing a dog.

243
00:12:15,676 --> 00:12:16,651
Is that normal?

244
00:12:16,819 --> 00:12:19,055
The pain medication
I gave you is pretty strong.

245
00:12:19,180 --> 00:12:21,890
Donna uses it for menstrual cramps.
How many did you take?

246
00:12:22,015 --> 00:12:23,723
Seve... Eight.

247
00:12:24,366 --> 00:12:26,662
But I washed 'em down
with plenty of fluids.

248
00:12:26,787 --> 00:12:29,220
You cannot drink scotch with this!

249
00:12:29,345 --> 00:12:32,045
You're gonna need to purge
right now, okay?

250
00:12:32,170 --> 00:12:35,295
- No, I'm not wasting 20-year scotch.
- Can you open his mouth?

251
00:12:35,420 --> 00:12:36,838
- What?
- Open his mouth.

252
00:12:37,364 --> 00:12:40,579
- Open his mouth!
- I'm not making myself throw up.

253
00:12:40,952 --> 00:12:43,052
Ron, I'm sorry.
We have to do this.

254
00:12:43,220 --> 00:12:45,071
- This is for your own good.
- I'll bite you!

255
00:12:45,196 --> 00:12:47,531
- Open your mouth.
- Leslie, grab his moustache.

256
00:12:47,656 --> 00:12:48,850
Open your mouth.

257
00:12:49,018 --> 00:12:50,367
Get his chest up.

258
00:12:50,492 --> 00:12:52,728
- Open your mouth!
- His shoulders!

259
00:12:55,974 --> 00:12:58,525
The good news
is Ron is resting comfortably.

260
00:12:58,991 --> 00:13:00,611
Is he okay?
Is he gonna live?

261
00:13:01,181 --> 00:13:02,357
I think so.

262
00:13:02,835 --> 00:13:05,419
Although I am hoping
that he has some memory loss.

263
00:13:05,544 --> 00:13:07,994
On a scale of one to Chris Brown,
how pissed is he?

264
00:13:08,384 --> 00:13:11,084
Well, he's very curious
about who shot him.

265
00:13:11,271 --> 00:13:13,665
So if you did it,
just say "I did it".

266
00:13:15,896 --> 00:13:17,545
Come on, person who shot Ron.

267
00:13:21,876 --> 00:13:23,332
I think it's a little weird

268
00:13:23,457 --> 00:13:26,345
that nobody wants to admit
that they shot Ron in the head.

269
00:13:26,767 --> 00:13:28,430
Maybe Ron shot himself.

270
00:13:28,599 --> 00:13:30,381
He has seemed
really depressed lately.

271
00:13:30,506 --> 00:13:32,376
He was shot in the back of the head.

272
00:13:32,501 --> 00:13:35,694
- Right, he loves the back of his head.
- He would never shoot himself there.

273
00:13:35,819 --> 00:13:38,274
It could have been someone else.
Someone not in our group.

274
00:13:38,676 --> 00:13:40,923
You think someone is hunting us?

275
00:13:41,158 --> 00:13:42,853
Man is the most dangerous game.

276
00:13:42,978 --> 00:13:44,278
To the predator.

277
00:13:44,681 --> 00:13:47,034
I did smell something out there,
and it wasn't human.

278
00:13:47,159 --> 00:13:48,468
That was pine trees.

279
00:13:48,593 --> 00:13:50,064
The predator can see heat.

280
00:13:50,189 --> 00:13:51,662
We should cover ourselves in mud.

281
00:13:51,830 --> 00:13:53,289
It could still be out there.

282
00:13:54,923 --> 00:13:57,251
- Did you hear that?
- Actually, I did hear something.

283
00:13:57,419 --> 00:13:59,128
- There's someone.
- I get my gun.

284
00:13:59,296 --> 00:14:01,380
Scare him off.
Shoot over his head.

285
00:14:01,636 --> 00:14:04,210
What's that gonna do?
I'm gonna shoot under its head.

286
00:14:04,335 --> 00:14:06,844
- Don't shoot anyone.
- What are you doing?

287
00:14:08,002 --> 00:14:09,013
Don't shoot!

288
00:14:10,059 --> 00:14:12,559
It's Craig from Reinhold Mercedes!

289
00:14:13,268 --> 00:14:14,894
Craig, I got you, dawg!

290
00:14:15,052 --> 00:14:16,336
Don't worry, I'm comin'!

291
00:14:16,461 --> 00:14:18,898
Attention, person
who shot me in the head.

292
00:14:19,357 --> 00:14:22,307
I'm gonna find you
and I'm gonna tear you apart!

293
00:14:23,660 --> 00:14:25,030
Bed. Now.

294
00:14:30,153 --> 00:14:32,003
That man wasn't my brother.

295
00:14:32,900 --> 00:14:34,300
He was my husband!

296
00:14:37,641 --> 00:14:40,419
- How was that? That wasn't good?
- Yeah, you can do better.

297
00:14:40,587 --> 00:14:43,297
- Gimme another one.
- I'm pregnant...

298
00:14:44,106 --> 00:14:45,591
With Josh Groban's baby!

299
00:14:49,168 --> 00:14:50,429
That was good.

300
00:14:50,870 --> 00:14:51,931
You do one.

301
00:14:54,551 --> 00:14:56,852
What do you mean the squirrel...

302
00:14:57,348 --> 00:14:59,707
took the nuts out of the...

303
00:15:00,730 --> 00:15:04,235
Out of that kid's backpack...
And ate 'em?

304
00:15:05,558 --> 00:15:07,738
You have to give me
a reason to spit.

305
00:15:08,372 --> 00:15:09,372
To spit.

306
00:15:10,775 --> 00:15:14,077
- Don't ask me a question.
- I won't. Okay, not a question.

307
00:15:14,826 --> 00:15:17,380
I've lived in Pawnee my whole life.

308
00:15:21,320 --> 00:15:22,570
That is a fact.

309
00:15:23,192 --> 00:15:24,865
A fact about me.

310
00:15:27,466 --> 00:15:28,634
You have...

311
00:15:34,349 --> 00:15:37,034
- Hey, how is he?
- He's, pretty out of it.

312
00:15:37,159 --> 00:15:39,978
- He's talking about you in his sleep.
- Good stuff?

313
00:15:40,650 --> 00:15:42,315
- How's it going out there?
- Terribly.

314
00:15:42,696 --> 00:15:45,609
No one will admit they shot Ron,
and no one saw it happen.

315
00:15:45,877 --> 00:15:47,111
I know what happened.

316
00:15:48,170 --> 00:15:49,030
You do?

317
00:15:50,760 --> 00:15:54,201
I heard about the accident,
and I need to know who is responsible.

318
00:15:54,892 --> 00:15:57,079
I don't know.
That's the problem.

319
00:15:57,247 --> 00:15:58,164
I am.

320
00:15:59,325 --> 00:16:00,634
I shot Ron Swanson.

321
00:16:01,126 --> 00:16:02,626
You shot my Mercedes?

322
00:16:02,794 --> 00:16:03,944
What? No. No!

323
00:16:07,429 --> 00:16:10,610
You can follow my light?
No, not your whole head, just the eyes.

324
00:16:13,630 --> 00:16:15,222
- What month is it?
- November

325
00:16:16,042 --> 00:16:17,933
Why are you taking
the blame for this?

326
00:16:18,724 --> 00:16:21,094
Don't worry about it.
I know what I'm doing.

327
00:16:21,219 --> 00:16:22,869
Just go look after Ron.

328
00:16:25,859 --> 00:16:26,984
So what happened?

329
00:16:27,152 --> 00:16:29,657
Did you forget
to check the entire field?

330
00:16:29,782 --> 00:16:32,740
I find a lot of women have
problems with tunnel vision.

331
00:16:34,261 --> 00:16:36,076
No, I'm an excellent hunter.

332
00:16:36,286 --> 00:16:38,037
How did you end up
shooting a guy, then?

333
00:16:38,567 --> 00:16:39,565
Fair enough.

334
00:16:39,690 --> 00:16:41,498
I was walking in the woods,

335
00:16:41,623 --> 00:16:44,123
and then I tripped
and my gun went off.

336
00:16:44,586 --> 00:16:48,343
- So you forgot to put the safety on.
- No, I always have the safety on.

337
00:16:50,025 --> 00:16:52,817
While I was tripping, I saw a quail,

338
00:16:52,942 --> 00:16:54,512
and I shot at it.

339
00:16:54,853 --> 00:16:55,805
In mid-trip?

340
00:16:56,288 --> 00:16:57,973
No. That's...

341
00:16:58,908 --> 00:17:00,156
Okay, fine.

342
00:17:01,593 --> 00:17:03,443
I got that tunnel vision...

343
00:17:03,648 --> 00:17:04,898
That girls get.

344
00:17:06,835 --> 00:17:09,517
- That's what happened. End of story.
- I think you're hysterical

345
00:17:09,642 --> 00:17:11,701
because of all the excitement,
obviously.

346
00:17:11,826 --> 00:17:14,240
So I'm just not following your story.
All right?

347
00:17:16,130 --> 00:17:18,356
I let my emotions
get the best of me.

348
00:17:18,481 --> 00:17:21,539
I just...
I cared too much, I guess.

349
00:17:21,787 --> 00:17:24,457
I was thinking with my lady parts.

350
00:17:24,582 --> 00:17:26,490
I was walking,
and I felt something icky.

351
00:17:26,615 --> 00:17:29,600
I thought there was gonna be chocolate.
I don't even remember.

352
00:17:29,797 --> 00:17:33,801
I'm wearing a new bra,
and it closes in the front.

353
00:17:33,926 --> 00:17:36,224
So it popped open
and it threw me off.

354
00:17:36,349 --> 00:17:38,035
All I want to do is have babies.

355
00:17:38,160 --> 00:17:41,101
Are you single?
I'm going through a thing right now.

356
00:17:41,226 --> 00:17:44,212
I guess when my life's incomplete,
I want to shoot someone.

357
00:17:44,337 --> 00:17:46,837
This would not happen
if I had a penis!

358
00:17:47,083 --> 00:17:49,079
What?
Bitches be crazy.

359
00:17:49,204 --> 00:17:51,068
I'm good at tolerating pain,

360
00:17:51,575 --> 00:17:54,835
I'm bad at math, and I'm stupid.

361
00:17:55,620 --> 00:17:57,199
I wonder what they're doing.

362
00:17:57,377 --> 00:18:00,277
Probably making out
on top of a deer carcass.

363
00:18:00,918 --> 00:18:02,168
Super-romantic.

364
00:18:03,494 --> 00:18:06,741
You know, if I gave you a hickey,
it would totally make Ann jealous.

365
00:18:09,983 --> 00:18:12,603
I don't know, I think that would...
That's pretty gross.

366
00:18:12,728 --> 00:18:14,228
Seems kind of weird.

367
00:18:14,686 --> 00:18:18,136
What's weird about one friend sucking
on another friend's neck?

368
00:18:21,215 --> 00:18:23,517
When you put it that way,
it doesn't sound that weird at all.

369
00:18:24,072 --> 00:18:26,894
Yeah, it's not. I gave my gay
boyfriend's boyfriend a hickey,

370
00:18:27,019 --> 00:18:29,355
and it totally made
my gay boyfriend jealous.

371
00:18:29,893 --> 00:18:30,893
Really?

372
00:18:33,195 --> 00:18:34,295
All right...

373
00:18:34,960 --> 00:18:36,205
awesome, I'm in.

374
00:18:36,330 --> 00:18:38,000
I'm gonna go sterilize my neck.

375
00:18:43,932 --> 00:18:44,712
What?

376
00:18:45,300 --> 00:18:47,674
You know, Leslie,
the Super Bowl's in a couple months.

377
00:18:47,799 --> 00:18:49,731
I usually watch it with my brothers.

378
00:18:49,856 --> 00:18:53,166
Maybe you could come by at halftime
and shoot me in the head.

379
00:18:54,101 --> 00:18:56,971
Ron, I'm really sorry
that I ruined your weekend.

380
00:18:57,096 --> 00:19:00,913
Or perhaps next time I'm enjoying
some alone time in the men's restroom,

381
00:19:01,038 --> 00:19:03,849
you could invite yourself into my stall
and shoot me in the head.

382
00:19:05,157 --> 00:19:07,728
Look, if there's anything I can do
to make it up to you...

383
00:19:07,853 --> 00:19:09,886
Sure.
How 'bout you shoot me in the head?

384
00:19:10,011 --> 00:19:11,523
Wait, you already did that!

385
00:19:13,570 --> 00:19:16,191
Hey, Tom, can I talk to you
for a second?

386
00:19:16,316 --> 00:19:17,781
Hold on.
This is amazing.

387
00:19:17,906 --> 00:19:20,012
Now. I need to talk to you now.

388
00:19:24,829 --> 00:19:26,630
Are we finally gonna do this?

389
00:19:27,173 --> 00:19:28,673
I saw you shoot Ron.

390
00:19:28,857 --> 00:19:30,354
Okay, Leslie covered for you,

391
00:19:30,479 --> 00:19:33,670
but I won'tlet her take more crap
from Ron on your sorry-ass behalf.

392
00:19:34,223 --> 00:19:37,719
Okay, for the record, I was gonna
come forward, and I'll do it right now.

393
00:19:37,844 --> 00:19:40,386
But afterwards can we come back here
and talk about us?

394
00:19:42,722 --> 00:19:45,892
Maybe the next time I'm at the doctor's
office getting my prostate examined,

395
00:19:46,017 --> 00:19:48,139
you could come by
and shoot me in the head.

396
00:19:48,264 --> 00:19:49,686
Excuse me, everyone.

397
00:19:49,854 --> 00:19:51,939
Ron, I have something to say.

398
00:19:52,107 --> 00:19:54,253
Hang on a minute.
I'm not done berating Leslie.

399
00:19:54,378 --> 00:19:56,067
It wasn't Leslie's fault.

400
00:19:56,813 --> 00:20:00,362
She was covering for me
because I didn't have a hunting license.

401
00:20:01,804 --> 00:20:04,201
- I was the one who shot you.
- You didn't get a license?

402
00:20:04,800 --> 00:20:07,204
What kind of moron
doesn't get a license?

403
00:20:07,372 --> 00:20:09,480
That's reckless endangerment,
my son.

404
00:20:09,605 --> 00:20:11,938
That's a $25,000 fine, minimum.

405
00:20:12,164 --> 00:20:13,544
And probably jail time.

406
00:20:13,972 --> 00:20:15,838
But she covered for me,

407
00:20:16,006 --> 00:20:17,406
and I'm in the clear.

408
00:20:17,531 --> 00:20:18,681
That's right.

409
00:20:19,151 --> 00:20:21,927
She kept her mouth shut,
and now you're in the clear.

410
00:20:22,095 --> 00:20:24,177
- You know, I couldn't let...
- I know.

411
00:20:25,374 --> 00:20:26,524
You did good.

412
00:20:30,072 --> 00:20:31,478
You're a real stand-up guy.

413
00:20:32,676 --> 00:20:34,189
I'm sorry I lost my temper.

414
00:20:34,878 --> 00:20:37,496
It was because I was shot
in the head by a moron.

415
00:20:37,621 --> 00:20:38,902
Dude, Ron, I'm so sorry.

416
00:20:39,070 --> 00:20:41,106
Apology not accepted, moron.

417
00:20:45,441 --> 00:20:46,535
Pants king.

418
00:20:47,332 --> 00:20:48,495
Pants queen.

419
00:21:03,054 --> 00:21:04,543
The turkeys are great.

420
00:21:09,793 --> 00:21:11,193
Welcome back, Ron.

421
00:21:12,431 --> 00:21:14,100
"Welcome back, Ron."

422
00:21:15,345 --> 00:21:17,595
That's terrific.
Thanks, you guys.

423
00:21:18,403 --> 00:21:19,359
Let's eat!

424
00:21:19,527 --> 00:21:21,685
www.sous-titres.eu

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
