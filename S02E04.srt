1
00:00:00,084 --> 00:00:02,835
<i>... And to my wife, I apologize.</i>

2
00:00:03,276 --> 00:00:06,457
<i>All I can say is,
I wasn't just having sex.</i>

3
00:00:07,062 --> 00:00:09,634
<i>I was making love
to a beautiful woman...</i>

4
00:00:10,928 --> 00:00:12,176
<i>and her boyfriend,</i>

5
00:00:12,301 --> 00:00:15,097
<i>and a third person
whose name I never learned.</i>

6
00:00:15,526 --> 00:00:18,076
<i>Furthermore,
it was wrong of me to say</i>

7
00:00:18,201 --> 00:00:20,679
<i>I was building houses
for the underprivileged</i>

8
00:00:20,804 --> 00:00:23,182
<i>when I was actually
having four-way sex</i>

9
00:00:23,307 --> 00:00:25,270
<i>in a cave in Brazil.</i>

10
00:00:25,537 --> 00:00:27,610
I bet cave sex is in-sane.

11
00:00:27,779 --> 00:00:30,246
- Why?
- The echoes and the humidity.

12
00:00:31,490 --> 00:00:34,467
<i>... In my defense,
it was my birthday,</i>

13
00:00:35,212 --> 00:00:37,119
<i>and I really wanted to do it.</i>

14
00:00:37,445 --> 00:00:38,954
I think it's a real shame

15
00:00:39,122 --> 00:00:42,035
when people focus
on the tawdry details of a scandal.

16
00:00:42,460 --> 00:00:46,284
Personally, all I care about
is councilman Dexhart's policies,

17
00:00:46,409 --> 00:00:48,963
not whether he was high
on nitrous and cocaine

18
00:00:49,347 --> 00:00:50,849
during the cave sex...

19
00:00:50,974 --> 00:00:52,802
which, by the way, I heard he was.

20
00:00:53,290 --> 00:00:55,377
<i>... One more shocking revelation
in a story</i>

21
00:00:55,502 --> 00:00:57,223
<i>that just won't stop unfolding.</i>

22
00:00:57,392 --> 00:01:00,518
<i>It turns out councilman Dexhart
may have also had sex</i>

23
00:01:00,686 --> 00:01:02,824
<i>with a prostitute in the limousine</i>

24
00:01:02,949 --> 00:01:06,249
<i>on the way to and from
the press conference</i>

25
00:01:06,374 --> 00:01:08,931
<i>where he apologized
for having an affair.</i>

26
00:01:09,641 --> 00:01:11,774
Why does anybody want to run
for public office?

27
00:01:11,899 --> 00:01:14,532
You're just asking to have
your entire life exposed.

28
00:01:14,806 --> 00:01:17,997
Well, if you're squeaky clean,
like I am, there's no problem.

29
00:01:18,537 --> 00:01:20,537
You're married, and you hit
on women constantly.

30
00:01:20,840 --> 00:01:23,424
Yeah, but I've never
sealed the deal.

31
00:01:23,850 --> 00:01:25,304
Just window shopping.

32
00:01:25,568 --> 00:01:27,772
You can fly to Brazil,
just don't enter the cave.

33
00:01:27,897 --> 00:01:28,897
Up top.

34
00:01:31,842 --> 00:01:33,250
- Feygnasse Team -

35
00:01:33,729 --> 00:01:35,368
.:: La Fabrique ::.

36
00:01:35,821 --> 00:01:38,153
Episode 204
<i>Practice Date</i>

37
00:01:38,651 --> 00:01:40,554
Synchro :
mpm, Albator1932, lestat78

38
00:01:52,265 --> 00:01:53,863
You think that you're clean,

39
00:01:54,031 --> 00:01:56,101
but I bet that we could find
something on you.

40
00:01:56,226 --> 00:01:58,893
I changed my name, which is legal,
and once in high school,

41
00:01:59,018 --> 00:02:01,328
a girl beat me in a wrestling match.
That's it.

42
00:02:01,864 --> 00:02:04,039
I bet anything I can find
worse stuff on all you.

43
00:02:04,209 --> 00:02:06,109
Are you suggesting a game?

44
00:02:06,711 --> 00:02:08,210
- I'm in.
- I'm in.

45
00:02:08,378 --> 00:02:10,838
- I'm out.
- Not an option, Jerry.

46
00:02:11,080 --> 00:02:13,841
- You're going down.
- Seriously, I don't want to play.

47
00:02:14,010 --> 00:02:16,515
No, seriously, you are playing.
We're gonna nail you.

48
00:02:17,284 --> 00:02:18,470
I will play, too.

49
00:02:18,721 --> 00:02:21,351
If only to prove
that I can find more dirt on you

50
00:02:21,476 --> 00:02:22,767
than you can on me.

51
00:02:23,163 --> 00:02:24,834
That's why we're all playing.

52
00:02:25,728 --> 00:02:27,354
Your desk is over there.

53
00:02:27,543 --> 00:02:29,815
I love games that turn people
against each other.

54
00:02:30,300 --> 00:02:33,547
You will never believe
what I just found on Jerry's Facebook.

55
00:02:33,672 --> 00:02:34,748
A friend.

56
00:02:34,873 --> 00:02:35,895
Burn.

57
00:02:36,283 --> 00:02:38,739
Okay, again,
I'd really rather not play.

58
00:02:39,117 --> 00:02:40,594
Hey, guys, what are you doing?

59
00:02:40,719 --> 00:02:42,538
Trying to see who has
the least dirt on them

60
00:02:42,663 --> 00:02:44,617
in case someone wants
to run for office.

61
00:02:44,998 --> 00:02:47,125
Well, it ain't Jerry,
that's for sure.

62
00:02:47,277 --> 00:02:50,252
He's got a couple of 3-5-9s on him.
Public urination.

63
00:02:50,780 --> 00:02:52,282
I don't like this game.

64
00:02:52,407 --> 00:02:53,857
Just don't like it.

65
00:02:56,370 --> 00:02:58,552
He's probably gonna go
anger pee in the courtyard.

66
00:03:01,074 --> 00:03:02,844
You crazy park people,

67
00:03:03,412 --> 00:03:04,610
I'm out of here.

68
00:03:04,965 --> 00:03:06,537
Leslie, I'll see you tomorrow.

69
00:03:06,662 --> 00:03:10,272
Tomorrow will be
our first official... first date.

70
00:03:10,583 --> 00:03:13,071
- Roger that.
- Copy. Over and out.

71
00:03:14,211 --> 00:03:16,612
Dave and I are going
on our first date tomorrow.

72
00:03:17,404 --> 00:03:19,339
I'm not nervous.
Why should I be nervous?

73
00:03:19,464 --> 00:03:21,715
We're just two people
going on a first date.

74
00:03:21,840 --> 00:03:23,717
There's nothing to be nervous about.

75
00:03:30,098 --> 00:03:31,498
That was my phone.

76
00:03:34,293 --> 00:03:36,230
What are you...
what are you doing here?

77
00:03:36,355 --> 00:03:39,051
Just having lunch with Leslie.
What are you up to?

78
00:03:39,355 --> 00:03:43,183
I'm looking for scandalous information
about my co-workers

79
00:03:43,722 --> 00:03:45,349
for a game that we're playing.

80
00:03:46,323 --> 00:03:48,018
My taxes pay your salary, right?

81
00:03:49,980 --> 00:03:50,883
Cool.

82
00:03:55,728 --> 00:03:57,069
And he looked up at me,

83
00:03:57,238 --> 00:03:59,068
and he said,

84
00:03:59,193 --> 00:04:00,865
"Thank you. You saved my life".

85
00:04:02,515 --> 00:04:05,113
Hey, listen, I'm really nervous
about this date tomorrow night.

86
00:04:05,238 --> 00:04:07,580
Do you have, like, a first date
outfit I could borrow?

87
00:04:07,748 --> 00:04:10,431
Like, I don't know,
pair of cargo pants.

88
00:04:10,556 --> 00:04:12,251
I wouldn't go with a cargo pant.

89
00:04:12,431 --> 00:04:14,059
What about, like, a sexy hat?

90
00:04:14,337 --> 00:04:16,819
- I don't know what that even is.
- Helping already.

91
00:04:17,064 --> 00:04:18,890
Do you want to come by
after work today?

92
00:04:19,015 --> 00:04:20,467
Sure, yeah. Tonight?

93
00:04:21,112 --> 00:04:22,618
Several hours from now?

94
00:04:26,460 --> 00:04:28,642
Or you could just come over now?

95
00:04:28,810 --> 00:04:30,603
I think that would better.
Let's go.

96
00:04:35,973 --> 00:04:37,401
What you doing, guys?

97
00:04:37,570 --> 00:04:39,183
Looking for dirt on me?

98
00:04:39,473 --> 00:04:40,473
No point.

99
00:04:40,723 --> 00:04:41,905
Give up now.

100
00:04:42,351 --> 00:04:44,496
Oh, wait, there is this.

101
00:04:45,731 --> 00:04:46,731
Everybody,

102
00:04:47,508 --> 00:04:49,371
I bought a croissan'wich
this morning.

103
00:04:49,860 --> 00:04:52,419
This isn't even a real receipt,
just a scrap of paper.

104
00:04:52,987 --> 00:04:54,268
Taliban robes!

105
00:04:54,522 --> 00:04:56,864
What? Where'd you get
that photo from?

106
00:04:58,864 --> 00:05:00,611
Nice taliban robes.

107
00:05:01,051 --> 00:05:03,461
Those aren't taliban robes.
That's from Halloween.

108
00:05:03,586 --> 00:05:04,873
I was dressed like a jedi.

109
00:05:04,998 --> 00:05:07,861
I'm sure that the voters
would be able to tell the difference.

110
00:05:08,074 --> 00:05:10,151
Looks to me
like you're in the taliban.

111
00:05:10,519 --> 00:05:13,062
Did everybody know
that Ron's ex-wife Tammy

112
00:05:13,231 --> 00:05:15,688
is actually his second ex-wife
named Tammy?

113
00:05:15,946 --> 00:05:18,923
That's right.
Ron has two ex-wives,

114
00:05:19,048 --> 00:05:21,178
each named Tammy.

115
00:05:22,699 --> 00:05:24,349
Both of them bitches.

116
00:05:24,951 --> 00:05:27,822
Yes, my mom's name is Tammy.

117
00:05:30,581 --> 00:05:31,898
What's your point?

118
00:05:32,125 --> 00:05:34,459
Tom! That was a jaeger-secret.

119
00:05:35,336 --> 00:05:37,325
You just breached a jaeger-secret.

120
00:05:38,214 --> 00:05:40,589
Damn!
This just heated up quick.

121
00:05:43,344 --> 00:05:44,927
- That looks great.
- Yeah?

122
00:05:45,095 --> 00:05:47,538
Will you be wearing it
out of the store today, madame?

123
00:05:47,663 --> 00:05:49,823
I think I will, good lady.

124
00:05:52,102 --> 00:05:54,227
- Can I ask you a question?
- Shoot.

125
00:05:54,417 --> 00:05:55,602
if I've been married?

126
00:05:55,773 --> 00:05:57,105
- Have you?
- No.

127
00:05:57,230 --> 00:06:00,004
- Then say that.
- But he'll wonder why I haven't been.

128
00:06:00,129 --> 00:06:02,049
You know what?
I'm gonna say that I was married,

129
00:06:02,174 --> 00:06:04,177
but the question is,
should I say that I have kids?

130
00:06:04,302 --> 00:06:06,233
Guy like girls
that have kids, right?

131
00:06:06,700 --> 00:06:08,868
What if I get drunk
and I talk about Darfur too much?

132
00:06:08,993 --> 00:06:10,994
Or not enough?

133
00:06:11,242 --> 00:06:12,370
Leslie, relax.

134
00:06:15,115 --> 00:06:18,877
I have a few more questions for you.
"What if he shows up with another woman?

135
00:06:19,133 --> 00:06:21,045
"What if one of my sleeves
catches on fire

136
00:06:21,214 --> 00:06:22,690
"and it spreads rapidly?

137
00:06:22,815 --> 00:06:25,941
"What if, instead of tic tacs,
I accidentally pop a couple of ambien,

138
00:06:26,066 --> 00:06:28,070
and I have to keep punching
my leg to stay awake?"

139
00:06:28,195 --> 00:06:30,074
Those are all insane hypotheticals,

140
00:06:30,199 --> 00:06:32,141
and I promise you they won't happen.

141
00:06:33,297 --> 00:06:34,690
They have happened.

142
00:06:35,665 --> 00:06:37,511
All of these have happened to me.

143
00:06:38,129 --> 00:06:39,271
No, there's more.

144
00:06:39,396 --> 00:06:42,433
One time, I accidentally drank
an entire bottle of vinegar.

145
00:06:42,558 --> 00:06:43,944
I thought it was terrible wine.

146
00:06:44,113 --> 00:06:47,484
Once, I went out with a guy
who wore 3-d glasses the entire evening.

147
00:06:47,832 --> 00:06:50,658
One time, I rode in a sidecar
on a guy's motorcycle,

148
00:06:50,783 --> 00:06:53,487
and the sidecar detached
and went down a flight of stairs.

149
00:06:53,955 --> 00:06:56,740
Another time, I went to a really
boring movie with a guy,

150
00:06:56,865 --> 00:06:59,714
and while I was asleep,
he tried to pull out one of my teeth.

151
00:07:00,431 --> 00:07:02,704
I literally woke up
with his hand in my mouth.

152
00:07:03,349 --> 00:07:05,466
We went out after that,
but then he got weird.

153
00:07:05,711 --> 00:07:07,361
I know it sounds crazy.

154
00:07:07,552 --> 00:07:10,888
I'm a grown woman,
but I'm just not good on first dates.

155
00:07:12,722 --> 00:07:15,324
You have a problem,
and this is how we're gonna fix it.

156
00:07:15,449 --> 00:07:17,358
I know what you're thinking.
I wear an earpiece,

157
00:07:17,483 --> 00:07:20,028
you sit at a table nearby,
you speak into a mike,

158
00:07:20,153 --> 00:07:22,841
you tell me what to say on the date,
but let me tell you, it never works.

159
00:07:23,443 --> 00:07:26,409
We are gonna go to a restaurant
and have a practice date.

160
00:07:26,534 --> 00:07:29,072
I will pretend to be Dave,
and you will practice on me.

161
00:07:29,830 --> 00:07:31,075
That's a way better idea.

162
00:07:33,754 --> 00:07:35,606
Hey, Dave, it's me.
It's Leslie.

163
00:07:35,790 --> 00:07:39,124
Hi, Leslie, it's good to see you.
You don't want to do that quite yet.

164
00:07:41,658 --> 00:07:43,295
Let's begin our conversation.

165
00:07:45,613 --> 00:07:47,463
What's on the note cards?

166
00:07:47,588 --> 00:07:49,411
They're possible topics
of conversation.

167
00:07:49,553 --> 00:07:52,536
Whales, parades, electricity...

168
00:07:53,834 --> 00:07:56,391
- And the rest are blank.
- I couldn't think of anything else.

169
00:07:56,744 --> 00:07:58,746
Leslie's in worse shape
than I thought.

170
00:08:03,125 --> 00:08:04,875
Is she practice laughing?

171
00:08:11,242 --> 00:08:13,570
The Danish call it
<i>oppe og nede apparat,</i>

172
00:08:13,695 --> 00:08:15,660
which literally translated means

173
00:08:15,830 --> 00:08:17,386
"the up and down machine".

174
00:08:18,456 --> 00:08:20,761
That's a thorough history
of the teeter-totter.

175
00:08:20,887 --> 00:08:24,303
Now I'm gonna talk about the local flora
and fauna found in Pawnee.

176
00:08:24,454 --> 00:08:26,782
You know what?
Just ask me a question.

177
00:08:26,907 --> 00:08:28,757
- Just try to get to know me.
- Okay.

178
00:08:31,663 --> 00:08:34,555
I can't think of anything to ask you.
I'm sorry. My mind is blank.

179
00:08:34,723 --> 00:08:36,556
Ask me the first thing
that comes to you.

180
00:08:36,725 --> 00:08:37,925
How big is it?

181
00:08:39,887 --> 00:08:41,577
- Really?
- Oh, my God.

182
00:08:44,393 --> 00:08:45,280
You ready?

183
00:08:45,405 --> 00:08:48,005
Yes, I am.
Just give me one second.

184
00:08:48,537 --> 00:08:50,756
Donna, let me ask you something.

185
00:08:51,352 --> 00:08:52,663
Do you hate black people?

186
00:08:52,788 --> 00:08:53,788
Excuse me?

187
00:08:53,913 --> 00:08:55,826
'Cause apparently in 1988,

188
00:08:56,166 --> 00:08:57,352
you donated money

189
00:08:57,477 --> 00:08:59,605
to the presidential campaign for...

190
00:08:59,730 --> 00:09:00,978
David Duke!

191
00:09:01,103 --> 00:09:02,106
The KKK guy?

192
00:09:02,231 --> 00:09:05,127
I got a phone call.
They said he would lower taxes.

193
00:09:06,435 --> 00:09:07,921
How's it feel to lose so hard?

194
00:09:08,921 --> 00:09:10,883
I'm sorry, honey.
Let's get coffee.

195
00:09:11,055 --> 00:09:13,699
Re-Ron, you remember
my wife, wendy Haverford.

196
00:09:13,824 --> 00:09:15,596
Of course.
How are things at the hospital?

197
00:09:15,893 --> 00:09:17,807
Very good, thank you.

198
00:09:17,932 --> 00:09:20,309
I just got a pediatric
surgery fellowship.

199
00:09:20,498 --> 00:09:21,375
Nice.

200
00:09:21,500 --> 00:09:23,499
We're celebrating,
'cause she's super rich

201
00:09:23,688 --> 00:09:25,448
and super hot also.

202
00:09:25,573 --> 00:09:28,441
- Okay, come on, Tommy. Cut it out.
- You're super hot.

203
00:09:28,631 --> 00:09:31,900
Everybody else has to deal with it.
Let's get out of here. See you later.

204
00:09:34,187 --> 00:09:36,818
I've established
a scientifically perfect

205
00:09:36,943 --> 00:09:39,035
ten-point scale of human beauty.

206
00:09:39,205 --> 00:09:41,079
Wendy is a 7.4,

207
00:09:41,204 --> 00:09:43,459
which is way too high for Tom,

208
00:09:43,584 --> 00:09:45,694
who is a 3.8.

209
00:09:46,210 --> 00:09:48,810
Ten is tennis legend Steffi Graf.

210
00:09:53,153 --> 00:09:56,220
You're late, and I can see your nipples
through your dress.

211
00:09:56,388 --> 00:09:57,930
What? No. Really?

212
00:09:58,098 --> 00:10:01,099
In nursing school, we took
a psych course on how to treat phobias

213
00:10:01,224 --> 00:10:03,604
with a method called
exposure therapy.

214
00:10:03,729 --> 00:10:07,417
So like if you were afraid of snakes,
they'd immerse you in a tank of snakes,

215
00:10:07,542 --> 00:10:11,546
so I am going to immerse Leslie
in a tank of bad date.

216
00:10:12,112 --> 00:10:14,181
You're 20 minutes late.
I almost left.

217
00:10:14,739 --> 00:10:15,957
I was...

218
00:10:17,174 --> 00:10:18,492
dropping my niece off.

219
00:10:18,660 --> 00:10:21,868
- What's your niece's name?
- Torple. What? I don't know.

220
00:10:22,757 --> 00:10:23,872
That's not a name.

221
00:10:24,092 --> 00:10:25,416
I don't have a niece.

222
00:10:26,185 --> 00:10:28,460
- My niece's name is Stephanie?
- Stop lying!

223
00:10:28,628 --> 00:10:30,486
Look, there's bread!
You want some...

224
00:10:31,089 --> 00:10:33,882
I got flowers in your soup.
I'm so sorry...

225
00:10:34,192 --> 00:10:36,792
Just... I have to go to the...

226
00:10:37,385 --> 00:10:39,321
whizz palace.
You know, Dave, the...

227
00:10:39,446 --> 00:10:40,888
the place where you...

228
00:10:41,013 --> 00:10:42,863
you know, the toilet thing.

229
00:10:46,734 --> 00:10:48,034
It's a bathroom!

230
00:10:48,159 --> 00:10:49,648
It's called a bathroom!

231
00:10:55,014 --> 00:10:56,465
A little birdy told me

232
00:10:56,590 --> 00:10:59,516
that you have
one unpaid parking ticket.

233
00:10:59,700 --> 00:11:01,531
That's funny,
because a little birdy told me

234
00:11:01,656 --> 00:11:05,018
that your adoptive mother
was arrested for marijuana possession.

235
00:11:05,659 --> 00:11:06,872
Snap!

236
00:11:07,212 --> 00:11:09,087
- What?
- You didn't know that, huh?

237
00:11:11,728 --> 00:11:13,461
I didn't know I was adopted.

238
00:11:18,720 --> 00:11:21,342
- I'm so sorry.
- I really didn't want to play.

239
00:11:23,873 --> 00:11:25,992
- That was not my intention.
- It's not your fault.

240
00:11:26,117 --> 00:11:28,687
He totally baited you
with that unpaid parking ticket.

241
00:11:29,308 --> 00:11:31,908
Tom, could you come into my office?

242
00:11:33,122 --> 00:11:34,568
Tom-mato sauce.

243
00:11:35,188 --> 00:11:36,468
Ron-tanamo bay.

244
00:11:36,946 --> 00:11:38,196
Do be seated,

245
00:11:38,972 --> 00:11:41,074
and congratulate me.

246
00:11:41,792 --> 00:11:43,702
- For what?
- Winning the game.

247
00:11:44,136 --> 00:11:47,137
I just found out, through some pretty
impressive investigating,

248
00:11:47,262 --> 00:11:49,532
that your wedding was a sham.

249
00:11:49,944 --> 00:11:52,034
- It's a green card marriage.
- That's crazy.

250
00:11:52,159 --> 00:11:54,645
I was born in south Carolina.
These colors don't run, baby.

251
00:11:54,770 --> 00:11:57,147
Yes, but Wendy was born in Ottawa...

252
00:11:57,272 --> 00:11:58,175
Canada!

253
00:11:59,019 --> 00:12:01,901
Her visa was set to expire
the day after you got married

254
00:12:02,026 --> 00:12:06,030
at the county courthouse
in front of 3 strangers and no family.

255
00:12:08,365 --> 00:12:11,271
We met in college.
She wanted to work in the states.

256
00:12:11,439 --> 00:12:12,981
She couldn't get a permit.

257
00:12:13,990 --> 00:12:16,443
I knew it. I knew you couldn't
get a wife as hot as her.

258
00:12:16,851 --> 00:12:19,452
Seriously, Ron, games aside,
you got to keep this between us.

259
00:12:19,577 --> 00:12:22,491
- Don't tell anybody, please.
- Come on. I'm not gonna turn you in.

260
00:12:23,044 --> 00:12:25,672
Just admit that when it comes
to digging up dirt,

261
00:12:25,797 --> 00:12:27,239
I bested you in this game.

262
00:12:27,364 --> 00:12:31,192
- Say it. Say "I bested you."
- Fine, you bested me. Is that all?

263
00:12:33,631 --> 00:12:35,962
- I'll have your wife tonight.
- What?

264
00:12:37,238 --> 00:12:38,490
I'm just kidding.

265
00:12:38,615 --> 00:12:39,673
Get out of here.

266
00:12:40,400 --> 00:12:41,759
Why would you say that?

267
00:12:42,000 --> 00:12:44,471
That dog was like my
best friend, and when she died...

268
00:12:46,379 --> 00:12:47,516
Hey, Tiffany.

269
00:12:47,943 --> 00:12:50,018
Yeah, I definitely
want to see you tonight.

270
00:12:51,042 --> 00:12:53,855
No, I can't really talk right now,
'cause I'm a date with this drip.

271
00:12:54,955 --> 00:12:55,933
I can ask.

272
00:12:56,058 --> 00:12:58,527
Do you want to watch
a porno after this with me and my wife?

273
00:12:59,016 --> 00:13:01,863
No, Dave, 'cause you're disgusting.

274
00:13:04,022 --> 00:13:06,901
- So it's definitely a no?
- Why you're being so terrible?

275
00:13:07,245 --> 00:13:10,340
We're 2 people trying to go on a date.
It's supposed to be fun. It's a date.

276
00:13:10,465 --> 00:13:11,852
You're right, it is.

277
00:13:11,977 --> 00:13:12,958
Well done.

278
00:13:13,337 --> 00:13:14,795
I had to get all medical on you,

279
00:13:14,920 --> 00:13:17,348
but now you see
that even if everything goes wrong,

280
00:13:17,473 --> 00:13:18,629
you'll survive.

281
00:13:23,206 --> 00:13:24,486
You coy bastard.

282
00:13:30,473 --> 00:13:31,810
You hate Ron, right?

283
00:13:32,425 --> 00:13:35,230
- No, I think Ron is fine.
- So we're on the same page.

284
00:13:35,738 --> 00:13:38,275
You got to help me take him down.
There's got to be something on him.

285
00:13:38,615 --> 00:13:41,205
I'm starting to feel
kind of gross about this game.

286
00:13:41,425 --> 00:13:44,055
It's not about the game anymore.
Ron has some serious dirt on me.

287
00:13:44,180 --> 00:13:46,622
I need to balance things out.
You must know how that feels.

288
00:13:46,747 --> 00:13:49,327
You got tons of dirt in your past.
Please, you gotta help me.

289
00:13:52,258 --> 00:13:54,602
There is a man named Duke Silver.

290
00:13:55,307 --> 00:13:57,294
He hangs out at a bar in eagleton.

291
00:13:58,073 --> 00:13:59,753
He's an old friend of Ron's.

292
00:13:59,923 --> 00:14:02,381
- Maybe you should ask him.
- Thanks, man.

293
00:14:02,577 --> 00:14:04,262
I'm gonna dig up so much dirt,

294
00:14:04,387 --> 00:14:06,987
there's gonna be worms
all over the place.

295
00:14:09,099 --> 00:14:10,849
It sounded snappier in my head.

296
00:14:11,581 --> 00:14:12,976
Ann is so awesome.

297
00:14:13,208 --> 00:14:15,298
I'm lucky to have a friend
who would spend a whole day

298
00:14:15,423 --> 00:14:16,690
being so mean to me.

299
00:14:18,817 --> 00:14:19,617
Catch.

300
00:14:19,742 --> 00:14:22,028
I don't need your dress.
I'm gonna wear my own stuff.

301
00:14:22,153 --> 00:14:22,968
Me power.

302
00:14:23,093 --> 00:14:26,114
Good.
You're officially first date-proof.

303
00:14:26,283 --> 00:14:27,378
Thank you, Dave.

304
00:14:28,129 --> 00:14:31,579
Listen... It is impossible
that he's not gonna like you.

305
00:14:31,704 --> 00:14:33,597
He's gonna freak out
about how awesome you are.

306
00:14:33,722 --> 00:14:36,432
- No, he's not.
- Yes, he is. He's gonna love you.

307
00:14:36,557 --> 00:14:39,940
You're cool, and you're sexy,
and you're funny, and you're smart.

308
00:14:40,338 --> 00:14:42,374
Any guy would be lucky to date you.

309
00:14:42,718 --> 00:14:45,549
- Yeah! Hell, yeah!
- Hell, yeah! I am awesome!

310
00:14:45,718 --> 00:14:47,219
- You are awesome.
- Yeah!

311
00:14:47,460 --> 00:14:48,710
And you are, too.

312
00:14:48,835 --> 00:14:50,305
- Thank you.
- You are, too.

313
00:14:50,473 --> 00:14:52,240
- Thank you.
- You are, too!

314
00:14:53,060 --> 00:14:54,518
You're awesome.

315
00:14:58,128 --> 00:14:59,282
Help, police!

316
00:14:59,566 --> 00:15:00,616
Help! Help!

317
00:15:04,539 --> 00:15:06,088
Look, I know today is...

318
00:15:06,562 --> 00:15:10,079
today... And it's not tomorrow
but I felt like you should know that

319
00:15:10,204 --> 00:15:12,536
I'm awesome,
and you're lucky to have me.

320
00:15:12,859 --> 00:15:14,829
And I think
our first date tomorrow's

321
00:15:14,998 --> 00:15:16,898
gonna go awesome, off the charts.

322
00:15:17,023 --> 00:15:18,083
Amazing. Up top.

323
00:15:20,371 --> 00:15:22,319
All right, let's do this bitch!

324
00:15:22,857 --> 00:15:24,107
I'm not scared.

325
00:15:24,421 --> 00:15:25,674
Can I come in?

326
00:15:25,843 --> 00:15:27,997
'Cause I walked here
'cause of the drinking.

327
00:15:28,122 --> 00:15:29,875
- I don't know.
- I do know.

328
00:15:30,000 --> 00:15:32,290
I'm coming inside, so move.
Okay?

329
00:15:32,932 --> 00:15:34,975
You make a better door than a guy.

330
00:15:37,895 --> 00:15:41,106
This is gonna sound weird.
I'm looking for a guy named Duke Silver.

331
00:15:41,821 --> 00:15:43,483
Yeah, he goes on in a second.

332
00:15:44,262 --> 00:15:47,453
<i>Ladies, ladies, ladies,
it's just about that time.</i>

333
00:15:47,578 --> 00:15:50,333
<i>It's with the jazziest pleasure
that I bring out for you</i>

334
00:15:50,458 --> 00:15:53,402
<i>my man, Mr. Duke Silver.</i>

335
00:15:58,695 --> 00:16:00,041
Thank you, Dwayne.

336
00:16:00,209 --> 00:16:02,422
As always, it is a thrill
to be here during

337
00:16:02,547 --> 00:16:04,671
this witching hour
with you lovely ladies.

338
00:16:05,345 --> 00:16:07,893
Now relax
and let the Duke Silver Trio

339
00:16:08,018 --> 00:16:10,552
take you on a little journey...
to yourself.

340
00:16:26,023 --> 00:16:28,033
Ann was helping me,

341
00:16:28,417 --> 00:16:30,739
because I was panicking
about tomorrow.

342
00:16:31,711 --> 00:16:33,861
Did I say something
to make you worry about to...

343
00:16:33,986 --> 00:16:34,951
No, just...

344
00:16:35,389 --> 00:16:36,828
the whole idea of first dates

345
00:16:36,996 --> 00:16:38,474
just kind of freak me out.

346
00:16:38,826 --> 00:16:39,748
Not anymore.

347
00:16:40,629 --> 00:16:43,967
I can't even believe that I was
scared to go on a date with you.

348
00:16:44,117 --> 00:16:45,712
You should be scared of me.

349
00:16:47,669 --> 00:16:49,632
I think I need
to return this sweater.

350
00:16:49,992 --> 00:16:52,677
I think it fused with my shirt
in the dryer or something.

351
00:16:53,081 --> 00:16:55,347
I think that's
a sweater-shirt combo.

352
00:16:57,834 --> 00:17:00,163
I think it's going
pretty well with Dave.

353
00:17:00,960 --> 00:17:03,693
He wants me.
I can totally tell that he wants me.

354
00:17:03,818 --> 00:17:06,215
I'm right here.
You know I'm here, right?

355
00:17:08,281 --> 00:17:09,610
Did you see my bra?

356
00:17:11,721 --> 00:17:13,865
I'm wearing the hot one tomorrow,
the black one.

357
00:17:14,248 --> 00:17:15,868
Can I use your bathroom?

358
00:17:16,450 --> 00:17:18,370
Are you impressed
that I know what it's called?

359
00:17:18,538 --> 00:17:21,156
- I ought to give you a lift home.
- Good. Well, in London,

360
00:17:21,281 --> 00:17:22,783
they call the elevators lifts,

361
00:17:22,908 --> 00:17:25,587
- you're gonna give me an elevator home?
- OK, let's go.

362
00:17:26,671 --> 00:17:28,924
Let's go down to the pub...

363
00:17:29,049 --> 00:17:30,548
- That's right.
- Get a pint.

364
00:17:30,673 --> 00:17:33,171
We'll put our knickers
in the beatle records.

365
00:17:33,296 --> 00:17:34,636
This is an English accent.

366
00:17:34,804 --> 00:17:37,494
What I cannot believe
is Dexhart's wife.

367
00:17:37,619 --> 00:17:39,371
I mean, how clueless can you get?

368
00:17:39,496 --> 00:17:42,158
How did she not know
this was going on?

369
00:17:42,436 --> 00:17:44,626
More importantly,
how does she stay with him

370
00:17:44,751 --> 00:17:46,001
after all this?

371
00:17:47,900 --> 00:17:50,508
Can I help you at...

372
00:17:50,633 --> 00:17:51,883
11:48 PM?

373
00:17:52,280 --> 00:17:54,698
OK, here's the whole thing.
Here are all my skeletons.

374
00:17:55,207 --> 00:17:57,909
When I was 16, I had sex
with a married woman.

375
00:17:58,078 --> 00:17:59,577
- Right.
- When I was in college,

376
00:17:59,745 --> 00:18:01,845
I smoked a decent amount of pot.

377
00:18:02,142 --> 00:18:04,586
- Nothing insane.
- What the hell are you doing?

378
00:18:04,711 --> 00:18:08,544
I'm trying to tell you that I've done
some stuff that I'm not very proud of,

379
00:18:08,914 --> 00:18:11,423
and I like you, so I would rather
you not find out about this

380
00:18:11,592 --> 00:18:13,466
from anyone but me.

381
00:18:13,591 --> 00:18:14,884
This was a bad idea.

382
00:18:15,873 --> 00:18:17,887
No, no, it's... It's fine.

383
00:18:18,361 --> 00:18:19,680
I get what this is

384
00:18:19,850 --> 00:18:21,247
and why you did it.

385
00:18:23,016 --> 00:18:25,478
Is there anythingyou would like
to share from your past

386
00:18:25,646 --> 00:18:27,326
to sort of balance the scales?

387
00:18:28,213 --> 00:18:30,350
One time, this guy rang
my doorbell at midnight

388
00:18:30,475 --> 00:18:32,986
to brag about getting laid
when he was 16, so I shot him.

389
00:18:33,155 --> 00:18:33,987
Good night.

390
00:18:35,791 --> 00:18:38,867
It's been a real gift making
sonic love to you tonight.

391
00:18:39,738 --> 00:18:42,863
If you want more of the Duke,
both my albums are for sale here...

392
00:18:42,988 --> 00:18:45,150
<i>Smooth as Silver</i> and <i>Hi ho, Duke.</i>

393
00:18:46,004 --> 00:18:47,995
And look for my new CD next month,

394
00:18:48,120 --> 00:18:50,220
<i>Memories of Now.</i>

395
00:18:51,118 --> 00:18:54,268
So come see me, come talk to me,
come love with me,

396
00:18:54,748 --> 00:18:57,259
and maybe we can
walk through fire together.

397
00:18:57,488 --> 00:18:58,750
Thank you.
Good night.

398
00:19:04,633 --> 00:19:05,453
Duke!

399
00:19:05,943 --> 00:19:07,166
Huge fan.

400
00:19:12,447 --> 00:19:15,694
Look, Tom, I imagine you'll want
to tell everyone about this,

401
00:19:15,864 --> 00:19:18,850
but I have worked pretty hard
to cultivate a certain authoritative

402
00:19:18,975 --> 00:19:20,800
or intimidating image
around the office.

403
00:19:20,925 --> 00:19:22,786
- Can I get a picture?
- Sure.

404
00:19:23,269 --> 00:19:26,174
- Say, "I bested you".
- I bested you.

405
00:19:27,469 --> 00:19:29,532
All right.
Thanks for lovin' the Duke.

406
00:19:30,378 --> 00:19:31,461
Lovely photo.

407
00:19:32,337 --> 00:19:34,172
- Truce?
- Truce.

408
00:19:36,061 --> 00:19:38,273
- You're kidding me.
- No, I'm not.

409
00:19:38,594 --> 00:19:41,757
You showed up at the guy's house,
in the middle of the night, drunk,

410
00:19:42,623 --> 00:19:43,972
you didn't sleep with him?

411
00:19:44,141 --> 00:19:46,165
- Should I have?
- It never hurts.

412
00:19:54,632 --> 00:19:57,484
You... You left quite a bit of stuff
at my place last night...

413
00:19:57,609 --> 00:20:00,573
purse and earrings and a shoe.

414
00:20:00,741 --> 00:20:02,367
I am so sorry about last night.

415
00:20:02,536 --> 00:20:06,219
On my list of embarrassing things
that I've done in my life, that was...

416
00:20:06,485 --> 00:20:08,128
numbers one through seven.

417
00:20:08,658 --> 00:20:11,292
- I understand if you want to cancel.
- It's okay.

418
00:20:11,460 --> 00:20:13,711
You can make it up to me tonight
on our second date.

419
00:20:14,719 --> 00:20:15,643
2nd?

420
00:20:15,941 --> 00:20:18,758
Last night was our first date,
so that would make tonight our second.

421
00:20:19,078 --> 00:20:20,559
I'm looking forward to it.

422
00:20:20,730 --> 00:20:21,645
8:00?

423
00:20:22,588 --> 00:20:23,471
8:00.

424
00:20:24,854 --> 00:20:26,474
- You like dancing?
- Yeah.

425
00:20:26,997 --> 00:20:29,561
- I don't like dancing.
- We don't have to go.

426
00:20:30,082 --> 00:20:32,814
Well, we went on our first date,
and I didn't even know it,

427
00:20:33,218 --> 00:20:34,467
a.k.a. I nailed it.

428
00:20:35,737 --> 00:20:37,728
No fires, no ambulances.

429
00:20:38,311 --> 00:20:42,042
Just good old-fashioned showing up
drunk at a guy's house late at night.

430
00:20:43,743 --> 00:20:45,960
<i>However, I want to be clear.</i>

431
00:20:46,878 --> 00:20:49,163
<i>I have no plans to resign.</i>

432
00:20:50,034 --> 00:20:53,920
Is it weird that my feelings are hurt
that no one's found any dirt on me yet?

433
00:20:55,352 --> 00:20:56,302
Hello!

434
00:20:56,775 --> 00:20:59,549
I drove a riding lawn mower
through a Nordstrom!

435
00:21:00,841 --> 00:21:02,641
There's video that I took!

436
00:21:03,088 --> 00:21:04,429
It's on the Internet.

437
00:21:07,341 --> 00:21:08,141
Nothing.

438
00:21:09,272 --> 00:21:11,262
Jerry, plastic surgery?

439
00:21:14,495 --> 00:21:16,345
I got hit by a fire engine.

440
00:21:17,748 --> 00:21:19,110
You are so lucky.

441
00:21:19,437 --> 00:21:20,331
How?

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
