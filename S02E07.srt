1
00:00:14,151 --> 00:00:15,723
Greg Pikitis.

2
00:00:17,324 --> 00:00:20,569
- You're the parks lady, right?
- That's right. I'm the parks lady.

3
00:00:21,896 --> 00:00:25,075
And I'm here to tell you
that this year, it ends.

4
00:00:25,375 --> 00:00:26,869
This kid makes me crazy.

5
00:00:27,084 --> 00:00:28,994
We got a history, Greg and I.

6
00:00:29,396 --> 00:00:31,890
He absolutely terrorizes
the park system.

7
00:00:32,549 --> 00:00:34,921
Every halloween
someone defaces the statue

8
00:00:35,046 --> 00:00:37,105
of mayor Percy in Ramsett park.

9
00:00:37,371 --> 00:00:40,873
And I know it's Greg Pikitis,
but I've never been able to prove it.

10
00:00:41,250 --> 00:00:43,762
He's like an invisible,
adolescent James Bond

11
00:00:43,887 --> 00:00:46,170
super-villain criminal mastermind.

12
00:00:48,198 --> 00:00:51,175
Or maybe someone else is doing it.
But I really feel like it's this kid.

13
00:00:51,343 --> 00:00:53,623
Got the entire parks department
watching you,

14
00:00:53,748 --> 00:00:55,104
my boyfriend's a cop.

15
00:00:55,493 --> 00:00:56,849
So don't even try it.

16
00:00:57,553 --> 00:00:59,813
- I don't understand.
- I think you do.

17
00:01:00,299 --> 00:01:01,435
It ends today.

18
00:01:01,982 --> 00:01:02,982
It ends...

19
00:01:03,542 --> 00:01:04,542
to...

20
00:01:05,467 --> 00:01:06,273
day.

21
00:01:09,550 --> 00:01:11,362
Thanks for stopping by.
You look great.

22
00:01:12,154 --> 00:01:13,154
Thank you.

23
00:01:13,735 --> 00:01:14,785
Ends today.

24
00:01:15,158 --> 00:01:16,575
- Feygnasse Team -

25
00:01:17,035 --> 00:01:18,661
.:: La Fabrique ::.

26
00:01:19,449 --> 00:01:21,490
Episode 207
<i>Greg Pikitis</i>

27
00:01:22,653 --> 00:01:23,707
mpm

28
00:01:24,334 --> 00:01:25,418
lestat78

29
00:01:36,367 --> 00:01:37,596
Hey, parks department.

30
00:01:38,249 --> 00:01:39,299
Ann's here.

31
00:01:40,610 --> 00:01:42,168
Just to remind you...

32
00:01:42,293 --> 00:01:45,246
Tonight, 7:00, my house,
in costume.

33
00:01:45,371 --> 00:01:46,564
Everybody in?

34
00:01:50,193 --> 00:01:52,402
Halloween is my favorite holiday.

35
00:01:52,527 --> 00:01:55,398
It's just the best.
And I don't have to work.

36
00:01:55,523 --> 00:01:58,392
Hey, slutty teenage girls
dressed as sexy kittens,

37
00:01:58,517 --> 00:02:00,711
pump your own stomachs this year.

38
00:02:01,413 --> 00:02:02,288
One thing.

39
00:02:02,562 --> 00:02:05,303
The people in this room now
are the people I invited.

40
00:02:05,428 --> 00:02:08,310
Plus Leslie and Donna,
so don't tell anybody.

41
00:02:08,435 --> 00:02:11,498
- Who's not invited, then?
- What's going on, cupcake?

42
00:02:11,623 --> 00:02:13,632
Excited about the party tonight.

43
00:02:15,192 --> 00:02:17,803
You're coming.
I was just about to tell you.

44
00:02:18,363 --> 00:02:19,963
Jerry already told me.

45
00:02:20,358 --> 00:02:23,219
Can't wait to see
how tiny your costume is.

46
00:02:24,456 --> 00:02:26,356
- What time does it start?
- 7:00.

47
00:02:26,696 --> 00:02:27,605
Perfect.

48
00:02:27,879 --> 00:02:29,329
Yeah, it's perfect.

49
00:02:30,317 --> 00:02:33,027
Check this out.
These are all the possible routes

50
00:02:33,301 --> 00:02:36,204
from Greg Pikitis' house
to the statue.

51
00:02:37,037 --> 00:02:40,326
That looks like something you would find
on the wall of a serial killer.

52
00:02:40,638 --> 00:02:42,510
In a way that's a compliment.

53
00:02:42,635 --> 00:02:43,985
Shows dedication.

54
00:02:44,507 --> 00:02:47,869
What's the big deal anyway?
I mean, a kid TPs some statue.

55
00:02:48,464 --> 00:02:50,248
And all the trees
and all the benches.

56
00:02:50,373 --> 00:02:52,743
It takes us, like, ten days
to clean his mess up.

57
00:02:52,868 --> 00:02:55,966
But more importantly, Pikitis sucks,
and I want to destroy him.

58
00:02:58,237 --> 00:03:00,451
If you destroy him early,
please come by.

59
00:03:00,576 --> 00:03:01,488
I will.

60
00:03:03,558 --> 00:03:05,836
How is someone gonna be able
to get through that door?

61
00:03:07,052 --> 00:03:08,102
What the...

62
00:03:08,406 --> 00:03:09,897
Are you kidding me?

63
00:03:10,065 --> 00:03:12,191
Jerry, relax. Here.

64
00:03:13,019 --> 00:03:14,068
William Percy.

65
00:03:14,634 --> 00:03:17,205
One of Pawnee's greatest mayors,
and a true hero.

66
00:03:17,330 --> 00:03:20,261
During the Pawnee bread factory
fire of 1922,

67
00:03:20,386 --> 00:03:22,451
he ran back into a burning building

68
00:03:22,619 --> 00:03:25,245
and saved the beloved secret recipe
for Pawnee pumpernickel.

69
00:03:26,068 --> 00:03:27,706
Didn't 30 people die in that fire?

70
00:03:28,860 --> 00:03:30,310
He wasn't superman.

71
00:03:30,808 --> 00:03:33,901
He looks like Ron Swanson.
Is that who this is based on?

72
00:03:34,026 --> 00:03:35,714
No, it's based on William Percy.

73
00:03:36,174 --> 00:03:37,678
Were you listening?

74
00:03:39,544 --> 00:03:40,874
Okay, Andy. Good news.

75
00:03:40,999 --> 00:03:43,191
You're officially
on the parks department payroll.

76
00:03:43,316 --> 00:03:44,338
$8 an hour.

77
00:03:45,350 --> 00:03:46,850
Commit this to memory.

78
00:03:47,150 --> 00:03:48,102
You see him,

79
00:03:48,270 --> 00:03:49,420
you stop him.

80
00:03:49,545 --> 00:03:52,147
- Knock his head off if you have to.
- Don't do that.

81
00:03:52,556 --> 00:03:54,697
But I give you permission
to use excessive force.

82
00:03:54,822 --> 00:03:56,819
- Don't use excessive force.
- Don't go overboard.

83
00:03:57,204 --> 00:03:59,488
Just stop him.
By any means necessary.

84
00:04:00,949 --> 00:04:02,866
Just stop him.
You know what I mean.

85
00:04:04,442 --> 00:04:06,739
A week ago, I had nothing.

86
00:04:07,380 --> 00:04:09,262
Now I have a part-time job,

87
00:04:09,679 --> 00:04:11,880
I'm gonna make 32 bucks tonight.

88
00:04:12,804 --> 00:04:14,904
I owe Leslie everything I have.

89
00:04:15,236 --> 00:04:17,636
Which after tonight
will be 39 bucks.

90
00:04:26,083 --> 00:04:28,142
- Am I early?
- Nope, you are right on time.

91
00:04:29,265 --> 00:04:30,436
I like your costume.

92
00:04:31,100 --> 00:04:32,460
Thanks.
Raggedy Ann.

93
00:04:32,585 --> 00:04:34,701
One of my favorite books
when I was a kid.

94
00:04:37,389 --> 00:04:39,139
- I'm a pirate.
- Right.

95
00:04:41,244 --> 00:04:42,898
Dr. Harris, how are you?

96
00:04:45,502 --> 00:04:48,060
This is Ron Swanson
from the parks department.

97
00:04:48,185 --> 00:04:50,430
I know him from that thing
that I'm doing.

98
00:04:50,555 --> 00:04:52,005
This is Dr. Harris.

99
00:04:52,429 --> 00:04:54,279
- You're a doctor.
- Yeah.

100
00:04:55,550 --> 00:04:57,850
- I meant your costume.
- I got it.

101
00:05:00,294 --> 00:05:01,767
Worlds colliding.

102
00:05:02,350 --> 00:05:03,600
Always awkward.

103
00:05:06,525 --> 00:05:08,247
8:22 p.m.

104
00:05:08,372 --> 00:05:10,851
Suspect still with friends
in parking lot.

105
00:05:11,864 --> 00:05:13,896
He looks obnoxious and irritating

106
00:05:14,064 --> 00:05:15,914
even from a great distance.

107
00:05:18,495 --> 00:05:19,443
Confirmed.

108
00:05:19,767 --> 00:05:22,339
Normally I hate working
on halloween, but...

109
00:05:22,781 --> 00:05:24,937
it's nice hanging out with Leslie.

110
00:05:25,062 --> 00:05:27,620
You know, she's focused,
she's committed,

111
00:05:28,353 --> 00:05:30,733
and she made out with me
in my cop car.

112
00:05:31,119 --> 00:05:32,373
It was pretty neat.

113
00:05:34,459 --> 00:05:37,463
I shouldn't have said that.
That's clear to me now.

114
00:05:46,672 --> 00:05:48,705
Remember my boyfriend, Derek?

115
00:05:49,076 --> 00:05:50,432
And his boyfriend, Ben?

116
00:05:52,433 --> 00:05:54,853
It's totally okay
that you didn't wear a costume.

117
00:05:55,496 --> 00:05:57,147
I did.
I'm a straight person.

118
00:05:59,010 --> 00:06:01,006
- Sorry I'm late.
- See?

119
00:06:03,405 --> 00:06:06,406
Sorry it's a little bit lame right now,
but we'll get it going.

120
00:06:06,727 --> 00:06:09,118
I passed up a gay
halloween party to be here.

121
00:06:09,243 --> 00:06:11,739
Do you know how much fun
gay halloween parties are?

122
00:06:11,864 --> 00:06:15,670
Last year I saw three Jonas Brothers
make out with three Robert Pattinsons.

123
00:06:15,795 --> 00:06:17,045
It was amazing.

124
00:06:17,710 --> 00:06:19,461
So you're the Mark
that Ann is dating.

125
00:06:20,226 --> 00:06:21,726
I suppose that I am.

126
00:06:22,006 --> 00:06:24,508
- We're very protective of Ann.
- We look out for her.

127
00:06:24,676 --> 00:06:27,260
So you kind of dropped the ball
with Andy, then, didn't you?

128
00:06:29,014 --> 00:06:31,208
I loved Andy.
Oh, such a sweetheart.

129
00:06:31,333 --> 00:06:34,077
- Is Andy coming tonight?
- You should get back with Andy.

130
00:06:34,202 --> 00:06:35,602
Who needs a drink?

131
00:06:35,987 --> 00:06:36,812
I do.

132
00:06:37,509 --> 00:06:39,891
Suspect laughing with friends

133
00:06:40,131 --> 00:06:42,442
and playing
with his stupid skateboard

134
00:06:43,313 --> 00:06:45,856
in a snide, turd-ish manner.

135
00:06:46,262 --> 00:06:47,244
"Turd-ish"?

136
00:06:48,217 --> 00:06:49,445
Like a turd.

137
00:06:49,570 --> 00:06:50,967
Like a little turd.

138
00:06:51,323 --> 00:06:53,870
Hey, listen.
Your scare tactics clearly worked,

139
00:06:54,794 --> 00:06:57,937
you got Andy guarding the statue,
why don't we go to Ann's party?

140
00:06:58,642 --> 00:06:59,882
Have a little fun.

141
00:07:01,561 --> 00:07:04,944
We wanted to cover that block anyway.
Wanted a police presence, so that's...

142
00:07:05,069 --> 00:07:07,655
This is within the bou...
confines of...

143
00:07:07,962 --> 00:07:08,802
Let's go.

144
00:07:09,990 --> 00:07:12,330
- Let's pick up my costume at my office.
- Got it.

145
00:07:12,455 --> 00:07:14,605
Can't we fire a warning shot
over his head?

146
00:07:14,730 --> 00:07:16,429
No, babe, don't touch that.

147
00:07:17,000 --> 00:07:18,145
Leaving location.

148
00:07:18,313 --> 00:07:19,966
Look at stupid suspect.

149
00:07:20,856 --> 00:07:21,940
I hate suspect.

150
00:07:23,187 --> 00:07:25,649
- Where are my keys?
- Looks like it's open.

151
00:07:25,774 --> 00:07:26,624
Really?

152
00:07:41,628 --> 00:07:43,904
We've been tailing
that kid for a couple hours.

153
00:07:44,029 --> 00:07:45,901
It must've been somebody else.

154
00:07:46,026 --> 00:07:47,276
It was Pikitis.

155
00:07:49,083 --> 00:07:50,333
Believe me now.

156
00:07:50,861 --> 00:07:52,846
That doesn't mean anything to me.

157
00:07:53,101 --> 00:07:54,264
This is a peach pit.

158
00:07:55,719 --> 00:07:58,270
He was eating a peach
when I went to go talk to him.

159
00:07:58,395 --> 00:08:00,750
This is his ace of spades.
This is his calling card,

160
00:08:00,875 --> 00:08:03,419
this is what he leaves all his victims.
And it's still warm.

161
00:08:03,544 --> 00:08:05,567
Go and arrest him
and send this to the lab.

162
00:08:05,735 --> 00:08:07,233
We don't have a lab.

163
00:08:10,165 --> 00:08:12,240
You're underage.

164
00:08:12,816 --> 00:08:13,828
You're not.

165
00:08:15,667 --> 00:08:17,038
I'm gonna leave.

166
00:08:18,289 --> 00:08:20,805
- This isn't that fun.
- Didn't need to tell me that.

167
00:08:21,042 --> 00:08:22,280
I take this home.

168
00:08:22,405 --> 00:08:24,550
Nobody was drinking it,
so I'm gonna take it.

169
00:08:25,088 --> 00:08:28,036
Gonna take this too.
Been meaning to read it.

170
00:08:32,595 --> 00:08:34,058
This isn't strictly legal.

171
00:08:34,183 --> 00:08:36,408
We consult him for an hour,
and I got to let him go.

172
00:08:36,533 --> 00:08:39,191
You talk to him first.
You have a scary face.

173
00:08:39,435 --> 00:08:41,311
No, in a good way.
Scary cute.

174
00:08:42,563 --> 00:08:43,654
Go talk to him.

175
00:08:45,307 --> 00:08:47,302
I didn't do this, man.
I promise.

176
00:08:47,427 --> 00:08:50,362
Son, I'm a police officer,
okay, you shouldn't lie to me.

177
00:08:50,678 --> 00:08:51,782
I'm not.

178
00:08:51,907 --> 00:08:54,699
I've never been here before.
And why would I do this?

179
00:08:55,103 --> 00:08:57,922
Because you've had run-ins
with Miss Knope in the past.

180
00:08:59,152 --> 00:08:59,996
A second.

181
00:09:02,408 --> 00:09:04,033
<i>Ask him about the peach pit.</i>

182
00:09:04,931 --> 00:09:07,504
This isn't one-way glass, you know.
I can see you.

183
00:09:09,623 --> 00:09:11,417
You know, I like Knope.

184
00:09:11,705 --> 00:09:13,822
I screw with her
'cause she gets all riled up.

185
00:09:13,947 --> 00:09:16,221
And her face
gets all scrunched up like this.

186
00:09:16,537 --> 00:09:18,515
But I like her.
She's all right.

187
00:09:20,344 --> 00:09:21,344
You see?

188
00:09:22,020 --> 00:09:22,943
What?

189
00:09:23,068 --> 00:09:25,311
I spent the whole day
decorating this place,

190
00:09:25,436 --> 00:09:27,286
and everybody's in the kitchen.

191
00:09:27,411 --> 00:09:30,443
- Why is everyone always in the kitchen?
- I think it's going fine.

192
00:09:31,107 --> 00:09:32,307
It's so quiet.

193
00:09:33,105 --> 00:09:34,975
Oh, my God, oh, my God, oh, my God.

194
00:09:35,100 --> 00:09:38,100
This is a total failure.
This party is a failure.

195
00:09:38,472 --> 00:09:39,522
Oh, my God.

196
00:09:39,704 --> 00:09:42,029
No, it's not a failure, come on.

197
00:09:42,778 --> 00:09:44,025
Do you have any mounds?

198
00:09:44,150 --> 00:09:46,394
Because all I can find
are almond joys.

199
00:09:46,519 --> 00:09:48,267
And almonds give me the squirts.

200
00:09:50,362 --> 00:09:51,131
I'm here.

201
00:09:51,447 --> 00:09:52,647
Statue's fine.

202
00:09:53,228 --> 00:09:55,757
"Crappy hallowieners".
That's hilarious.

203
00:09:55,882 --> 00:09:57,887
- Where's the kid?
- In the conference room,

204
00:09:58,055 --> 00:09:59,874
trying to hide his obvious guilt.

205
00:09:59,999 --> 00:10:02,603
I don't know what to tell you.
I don't think he did it.

206
00:10:02,894 --> 00:10:04,811
Yeah, he did,
and we need a confession.

207
00:10:06,951 --> 00:10:09,065
Andy and I are going to take a walk.

208
00:10:09,557 --> 00:10:12,694
And it's gonna be just you,
Pikitis, and these pliers.

209
00:10:13,104 --> 00:10:14,612
I'm not gonna torture this kid.

210
00:10:14,780 --> 00:10:16,103
Nobody said torture.

211
00:10:17,033 --> 00:10:18,491
Let me take a run at him, boss?

212
00:10:19,280 --> 00:10:21,880
In order to think
like one of these guys,

213
00:10:22,381 --> 00:10:24,122
you have to think like them.

214
00:10:24,438 --> 00:10:26,207
Yeah, he's never seen you before.

215
00:10:26,776 --> 00:10:28,746
So imply that you're
someone very important,

216
00:10:28,871 --> 00:10:31,045
and that you can make his life hell.

217
00:10:37,397 --> 00:10:39,179
Would you like
some boiling hot coffee?

218
00:10:39,597 --> 00:10:40,722
In the face?

219
00:10:44,767 --> 00:10:46,436
My name is Bert Macklin.

220
00:10:47,236 --> 00:10:49,436
I'm with the **** FBI.

221
00:10:50,942 --> 00:10:52,734
Are these the same kind of chips?

222
00:10:54,217 --> 00:10:56,279
- What's up, gorgeous?
- Perfect.

223
00:10:56,447 --> 00:10:58,407
Get your raggedy ass
over here, girl.

224
00:10:59,036 --> 00:11:00,116
Mad hatter?

225
00:11:00,284 --> 00:11:01,484
T- pain, baby.

226
00:11:01,776 --> 00:11:03,268
<i>She poppin', she rollin'</i>

227
00:11:03,393 --> 00:11:04,829
<i>She rollin'</i>

228
00:11:04,997 --> 00:11:06,623
<i>She climbin' the pole</i>

229
00:11:06,791 --> 00:11:09,159
<i>And I'm in love with the stripper</i>

230
00:11:09,764 --> 00:11:12,758
this isn't a stripper, it's my wife.
You remember her.

231
00:11:13,047 --> 00:11:14,881
- Thank you so much for inviting us.
- Sure.

232
00:11:15,049 --> 00:11:17,967
She's a surgeon at county general.
And she's super hot.

233
00:11:18,135 --> 00:11:21,596
Well, thank you for being a doctor
and not coming dressed as a doctor.

234
00:11:21,931 --> 00:11:24,781
Oh, my God, who'd be boring enough
to do that?

235
00:11:26,637 --> 00:11:28,333
What happened?
Did you tell everyone

236
00:11:28,458 --> 00:11:30,660
they're gonna have to get
a bone marrow transplant?

237
00:11:30,785 --> 00:11:32,788
They look miserable.
This party's a disaster.

238
00:11:32,913 --> 00:11:34,473
- No, it's not.
- Yes, it is.

239
00:11:34,598 --> 00:11:36,486
- Don't worry. We can fix this.
- Totally.

240
00:11:36,690 --> 00:11:37,946
Give us a second.

241
00:11:40,500 --> 00:11:44,035
Looks like Andy's wearing him down.
I'm gonna go finish him off.

242
00:11:45,104 --> 00:11:47,703
That is the dumbest thing
I have ever heard.

243
00:11:47,828 --> 00:11:49,450
Why don't you shut up?

244
00:11:49,575 --> 00:11:52,877
- What's going on in here?
- He's a jerk! He is being such a jerk!

245
00:11:53,254 --> 00:11:55,296
That's an awful thing
to say to a human being!

246
00:11:55,464 --> 00:11:57,937
- Are you crying?
- No, I'm... I am not crying, okay?

247
00:11:58,062 --> 00:12:00,762
- I'm allergic to jerks!
- Okay, Andy, out.

248
00:12:00,887 --> 00:12:02,428
Fine! I don't even care!

249
00:12:02,596 --> 00:12:03,696
It's stupid!

250
00:12:05,970 --> 00:12:07,504
The gloves are coming off.

251
00:12:09,764 --> 00:12:12,073
- What's that?
- I think you know what that is.

252
00:12:12,891 --> 00:12:15,815
When I came to visit you at the school,
you were eating a peach.

253
00:12:16,301 --> 00:12:19,861
Then my office gets vandalized and
somebody leaves a peach pit on my desk.

254
00:12:20,073 --> 00:12:22,673
I don't think that's a coincidence.

255
00:12:23,663 --> 00:12:24,908
You nailed me.

256
00:12:25,119 --> 00:12:28,221
There is no way
that two different people could've eaten

257
00:12:28,346 --> 00:12:30,664
the same fruit, in the same day,

258
00:12:30,833 --> 00:12:32,417
in the same area of Indiana.

259
00:12:32,803 --> 00:12:34,401
Never mind that I was

260
00:12:34,526 --> 00:12:36,268
actually eating a plum

261
00:12:36,515 --> 00:12:37,936
when you saw me earlier.

262
00:12:42,153 --> 00:12:44,345
I know you did this.
I don't care what you say.

263
00:12:44,513 --> 00:12:46,306
You were in the parking lot
all night,

264
00:12:46,754 --> 00:12:49,187
- you found a way to get in here.
- Wait.

265
00:12:49,477 --> 00:12:50,934
How did you know that?

266
00:12:51,103 --> 00:12:53,245
Because I followed you, genius!

267
00:12:53,906 --> 00:12:55,820
If you followed me all night,

268
00:12:55,945 --> 00:12:57,734
then you know that it wasn't me!

269
00:12:58,694 --> 00:13:00,111
What is your problem?

270
00:13:00,279 --> 00:13:03,105
I've been very civil,
but I will waterboard you.

271
00:13:03,365 --> 00:13:04,247
Nope.

272
00:13:05,579 --> 00:13:08,348
I don't think she would make a good cop.
I would be

273
00:13:08,676 --> 00:13:11,441
frightened to live in a town
that she's the cop of, frankly.

274
00:13:12,249 --> 00:13:13,374
What are you doing?

275
00:13:13,792 --> 00:13:16,428
- Do you want me to liven things up?
- Of course I do.

276
00:13:16,553 --> 00:13:19,704
Then relax. Turn off all the lights
that don't have colored bulbs.

277
00:13:23,165 --> 00:13:24,339
All right, y'all.

278
00:13:24,797 --> 00:13:26,923
Take the pigs in a blanket
and put them to sleep,

279
00:13:27,048 --> 00:13:28,681
it's time to get wild!

280
00:13:29,285 --> 00:13:32,226
Get this furniture out of here,
we're making a dance floor.

281
00:13:33,391 --> 00:13:34,363
Come on.

282
00:13:34,772 --> 00:13:37,588
Put the turkey wraps down.
We're about to get wild.

283
00:13:41,415 --> 00:13:43,315
Ron Swanson in the building!

284
00:13:48,744 --> 00:13:51,087
His mom's gonna be here soon.
Please, Dave, arrest him.

285
00:13:51,212 --> 00:13:53,414
- On what charge?
- Handcuff him to something.

286
00:13:54,021 --> 00:13:55,021
Greggy?

287
00:13:57,513 --> 00:13:58,377
Gregsy.

288
00:13:59,325 --> 00:14:00,447
Mrs. Pikitis.

289
00:14:00,572 --> 00:14:03,255
Hi, I'm Leslie Knope.
This is officer Sanderson.

290
00:14:03,380 --> 00:14:06,479
- Bert Macklin. F.B.I.
- What do you think you're doing?

291
00:14:06,604 --> 00:14:09,638
You can't just hold a 16-year-old kid
against his will.

292
00:14:09,807 --> 00:14:11,723
We were questioning him
about this vandalism.

293
00:14:11,892 --> 00:14:14,650
- Do you have any evidence?
- Yes. Two words.

294
00:14:14,775 --> 00:14:16,437
- No, we don't...
- Peach pit.

295
00:14:17,898 --> 00:14:20,190
- His D.N.A. is all over it.
- We don't know that.

296
00:14:20,315 --> 00:14:23,245
We can get it tested,
and if it matches your D.N.A.,

297
00:14:23,529 --> 00:14:25,908
- you're going away for a long time.
- Is this a joke?

298
00:14:28,201 --> 00:14:29,783
I'm calling your shift commander.

299
00:14:30,087 --> 00:14:32,077
Monday morning,
I am calling your boss.

300
00:14:32,560 --> 00:14:34,288
I don't know
who to call about you...

301
00:14:34,456 --> 00:14:36,453
- President of the U.S.A.
- Fine!

302
00:14:36,834 --> 00:14:38,599
If you ever come near my son again,

303
00:14:38,724 --> 00:14:41,461
I will sue you
and everyone in this building.

304
00:14:42,645 --> 00:14:43,726
Come on, honey.

305
00:14:46,436 --> 00:14:48,636
Let them think they've lost us.
Then...

306
00:14:49,279 --> 00:14:51,176
- Where you going?
- I got to tell my boss

307
00:14:51,301 --> 00:14:53,316
why I illegally detained a teenager.

308
00:14:54,245 --> 00:14:56,593
I shouldn't have gotten involved
with this. It's just...

309
00:14:57,254 --> 00:14:58,699
Just bad police work.

310
00:14:59,851 --> 00:15:01,901
It was bad F.B.I. work too.

311
00:15:02,724 --> 00:15:03,692
My bad.

312
00:15:11,577 --> 00:15:14,910
Tonight the "t" in T-pain
stands for "Tom Haverford"!

313
00:15:15,581 --> 00:15:17,038
Nurses versus doctors.

314
00:15:17,261 --> 00:15:18,311
On my Mark.

315
00:15:18,606 --> 00:15:19,416
Three,

316
00:15:19,992 --> 00:15:20,761
two,

317
00:15:21,271 --> 00:15:22,003
one,

318
00:15:22,417 --> 00:15:23,297
Vodka!

319
00:15:29,073 --> 00:15:29,872
Son.

320
00:15:30,137 --> 00:15:31,803
That is one hell of a costume.

321
00:15:33,829 --> 00:15:35,198
Damn, that's good.

322
00:15:36,078 --> 00:15:37,128
Nice party.

323
00:15:39,516 --> 00:15:40,979
Why would anyone do this?

324
00:15:41,732 --> 00:15:42,910
Kids are kids.

325
00:15:43,035 --> 00:15:45,692
Everybody does stupid stuff
like this in high school.

326
00:15:45,903 --> 00:15:46,777
I didn't.

327
00:15:46,902 --> 00:15:48,529
But you were probably a nerd?

328
00:15:49,797 --> 00:15:51,157
I mean that in a good way.

329
00:15:51,772 --> 00:15:54,284
Look at you.
You're the coolest person I've ever met.

330
00:15:55,370 --> 00:15:56,954
I even met John Mellencamp.

331
00:15:58,048 --> 00:16:00,432
Why don't you go home?
I'll finish cleaning this up.

332
00:16:00,557 --> 00:16:01,291
No way.

333
00:16:01,691 --> 00:16:04,154
You got me a job.
You're helping me turn my life around.

334
00:16:04,279 --> 00:16:05,588
I go home when you go home.

335
00:16:07,590 --> 00:16:09,049
It's city planning.

336
00:16:09,656 --> 00:16:12,269
It's mostly regulating things
and just making sure

337
00:16:12,394 --> 00:16:13,887
people obey the city code.

338
00:16:14,056 --> 00:16:16,222
You want to get out of here?
Go back to my place?

339
00:16:17,168 --> 00:16:17,955
What?

340
00:16:19,270 --> 00:16:20,520
I'm dating Ann.

341
00:16:21,199 --> 00:16:23,606
Good. That was a test.
You passed.

342
00:16:23,774 --> 00:16:27,079
Let's celebrate you passing the test,
over a drink at my place.

343
00:16:27,908 --> 00:16:29,278
No. Still no.

344
00:16:29,767 --> 00:16:31,383
You passed a second test.

345
00:16:31,745 --> 00:16:33,695
But we're still watching you.

346
00:16:38,694 --> 00:16:41,790
So you want to get out of here,
go back to my place?

347
00:16:49,826 --> 00:16:52,256
You and Tom really
turned the party around.

348
00:16:53,033 --> 00:16:55,054
We have a good time together.

349
00:16:55,983 --> 00:16:57,360
Tom told me

350
00:16:57,849 --> 00:17:00,449
that you know
about our green card marriage,

351
00:17:00,574 --> 00:17:02,227
and I just want to say thank you

352
00:17:03,349 --> 00:17:05,134
for not telling anyone.

353
00:17:06,712 --> 00:17:07,712
No worries.

354
00:17:07,962 --> 00:17:10,287
I'd hate for you
to have to go back to Canada.

355
00:17:13,316 --> 00:17:16,617
All that socialized medicine
up there.

356
00:17:17,258 --> 00:17:18,881
Wendylicious, where you been?

357
00:17:19,006 --> 00:17:21,206
I was just thanking Ron for,

358
00:17:21,331 --> 00:17:22,790
not telling anyone.

359
00:17:24,236 --> 00:17:25,431
That was cool of you.

360
00:17:25,556 --> 00:17:27,586
You won't have to keep it
a secret much longer

361
00:17:27,754 --> 00:17:30,736
because in a few months we can split up
without raising any eyebrows.

362
00:17:30,861 --> 00:17:33,511
And we'll invite you
to the divorce party.

363
00:17:35,698 --> 00:17:37,752
Maybe Greg is innocent.

364
00:17:38,299 --> 00:17:41,224
If that's the case, there's another kid
out there who's tormenting me.

365
00:17:41,670 --> 00:17:42,534
Maybe.

366
00:17:43,153 --> 00:17:46,305
I believe that you're innocent
until proven guilty in this country.

367
00:17:46,773 --> 00:17:48,606
That's the cornerstone of democracy.

368
00:17:48,818 --> 00:17:49,650
Sure.

369
00:17:51,540 --> 00:17:54,390
On the other hand,
Greg Pikitis is a little punk.

370
00:17:54,573 --> 00:17:55,947
I want to TP his house.

371
00:17:56,158 --> 00:17:57,258
Let's do it.

372
00:17:59,952 --> 00:18:02,245
Wait, this is fun, what do I do?

373
00:18:03,706 --> 00:18:06,374
You just grab the toilet paper,
unroll it a little bit,

374
00:18:06,543 --> 00:18:07,792
throw it over the tree.

375
00:18:08,420 --> 00:18:11,588
This is for mayor Percy,
this is for the Parks Department Office,

376
00:18:11,757 --> 00:18:13,756
this is for pooping
on the handball court!

377
00:18:18,619 --> 00:18:20,848
This is really fun,
but I don't condone it!

378
00:18:24,978 --> 00:18:27,450
Bye, guys.
You're the man, Swanson.

379
00:18:27,907 --> 00:18:29,205
Thanks for coming.

380
00:18:31,651 --> 00:18:34,320
Thank you so much.
I don't even know what to say.

381
00:18:34,759 --> 00:18:36,488
- Don't mention it.
- Also, Tom,

382
00:18:36,656 --> 00:18:38,590
your wife is super cool.

383
00:18:38,715 --> 00:18:39,908
So well done.

384
00:18:40,119 --> 00:18:41,373
She's pretty great.

385
00:18:41,498 --> 00:18:43,474
I don't know
how you landed that chick,

386
00:18:43,599 --> 00:18:45,288
but she's awesome.
Nice work.

387
00:18:45,499 --> 00:18:47,301
- You're lucky.
- You ready to go?

388
00:18:48,293 --> 00:18:50,291
Thank you so much.
We had so much fun.

389
00:18:51,171 --> 00:18:53,718
About to head home
and have crazy sex.

390
00:18:53,843 --> 00:18:55,214
That's cool. I don't...

391
00:18:59,655 --> 00:19:00,864
Five-0!

392
00:19:03,656 --> 00:19:04,975
I see you in there, Andy.

393
00:19:07,519 --> 00:19:08,705
What are you doing?

394
00:19:12,056 --> 00:19:13,900
That's them, officer.
Right there.

395
00:19:14,111 --> 00:19:14,977
My God.

396
00:19:16,380 --> 00:19:18,236
I'm so sorry.
We have the wrong house.

397
00:19:18,361 --> 00:19:19,999
Why are you doing this?

398
00:19:20,124 --> 00:19:22,321
It's hard to explain,
but we were getting revenge

399
00:19:22,446 --> 00:19:24,329
on this kid,
and we thought this was his house,

400
00:19:24,454 --> 00:19:26,871
- but I guess we got the address wrong.
- I'm Greg's mom.

401
00:19:28,051 --> 00:19:28,944
You are?

402
00:19:31,394 --> 00:19:34,004
Did he hire a fake mom again
to get him out of trouble?

403
00:19:34,476 --> 00:19:35,241
What?

404
00:19:35,366 --> 00:19:37,635
Whenever he gets in trouble
he goes on craigslist

405
00:19:37,760 --> 00:19:39,592
and hires a woman to play his mother

406
00:19:39,761 --> 00:19:40,840
and bail him out.

407
00:19:40,965 --> 00:19:42,462
That little s-o-b.

408
00:19:45,771 --> 00:19:46,706
I knew it!

409
00:19:46,831 --> 00:19:48,226
My God.

410
00:19:49,783 --> 00:19:51,355
That kid is amazing.

411
00:19:53,066 --> 00:19:54,274
He's not in his room.

412
00:19:54,693 --> 00:19:56,250
I don't know where he is.

413
00:19:57,142 --> 00:19:57,985
I do.

414
00:19:58,154 --> 00:19:59,436
Make sure you get that.

415
00:20:03,618 --> 00:20:05,327
Let them go!
Stay on the leader!

416
00:20:09,420 --> 00:20:11,320
I am gonna wring your neck!

417
00:20:11,752 --> 00:20:14,168
How did you get into
the Parks Department? I have to know.

418
00:20:14,337 --> 00:20:16,148
Maybe the F.B.I. can figure it out.

419
00:20:16,739 --> 00:20:18,715
I'm not even in the F.B.I., stupid.

420
00:20:20,142 --> 00:20:22,927
- You're amazing.
- You're going to jail for a long time.

421
00:20:23,096 --> 00:20:25,347
He's not gonna go to jail,
he's a minor.

422
00:20:25,515 --> 00:20:26,980
We'll let the jury decide.

423
00:20:27,350 --> 00:20:30,560
- It's not gonna be a jury, it's his...
- The judge will decide where he goes.

424
00:20:30,771 --> 00:20:33,700
- He's gonna do probation, he's a minor.
- Just let me have this.

425
00:20:35,442 --> 00:20:36,525
Nice work, Knope.

426
00:20:36,693 --> 00:20:37,901
That is amazing.

427
00:20:38,736 --> 00:20:41,737
But the thing that's driving me nuts is,
how did he do it?

428
00:20:41,906 --> 00:20:43,375
Halloween Day.
3:45pm

429
00:20:43,500 --> 00:20:45,675
If you destroy him early,
please come by.

430
00:20:45,800 --> 00:20:46,800
I will.

431
00:20:47,495 --> 00:20:49,784
How is someone gonna be able
to get through that door?

432
00:20:49,909 --> 00:20:51,707
What the...
Are you kidding me?

433
00:20:51,875 --> 00:20:54,251
We were here all th afternoon.
How did he get in?

434
00:20:55,879 --> 00:20:57,588
We always lock the doors

435
00:20:57,756 --> 00:20:58,881
when we leave.

436
00:20:59,466 --> 00:21:01,168
And Dave and I tailed Pikitis

437
00:21:01,293 --> 00:21:03,895
from the moment
he left his house at 7:00 p.m.

438
00:21:04,262 --> 00:21:05,969
to the moment we came back here.

439
00:21:07,017 --> 00:21:08,473
Does he have a look-alike?

440
00:21:09,393 --> 00:21:11,184
Did he have an accomplice?
Did he have help?

441
00:21:15,887 --> 00:21:17,567
How did that little turd do it?

442
00:21:18,365 --> 00:21:20,032
www.sous-titres.eu

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
