1
00:00:01,612 --> 00:00:02,941
Thank you old Gus,

2
00:00:03,066 --> 00:00:05,466
for brightening our lives,
our hearts

3
00:00:05,735 --> 00:00:07,423
and most importantly, our shoes.

4
00:00:09,676 --> 00:00:10,801
This truly is a day

5
00:00:10,969 --> 00:00:12,424
of mixed emotions for me.

6
00:00:12,933 --> 00:00:14,961
I'm sad that old Gus is leaving.

7
00:00:15,086 --> 00:00:17,874
But it allows me
to help Andy restart his life

8
00:00:17,999 --> 00:00:20,227
as pioneer hall's new shoeshine guy.

9
00:00:20,626 --> 00:00:23,060
I've been listening
to your boring speeches

10
00:00:23,185 --> 00:00:24,694
for the last 50 years.

11
00:00:25,050 --> 00:00:27,234
And now it's time for you
to listen to one of mine.

12
00:00:28,900 --> 00:00:32,148
You know, a day like this
makes a man reflect upon his life.

13
00:00:32,273 --> 00:00:36,270
And I've come to the conclusion
that I've completely wasted mine.

14
00:00:38,012 --> 00:00:39,747
And just for the record,

15
00:00:39,915 --> 00:00:42,342
I never ever liked
being called old Gus.

16
00:00:43,094 --> 00:00:44,460
I didn't understand it

17
00:00:44,628 --> 00:00:46,754
when I was in my 20's
and I sure hate it now.

18
00:00:47,189 --> 00:00:49,173
So go to hell,
every single one of you.

19
00:00:49,438 --> 00:00:52,214
Screw Pawnee
and screw your damn shoes.

20
00:00:55,329 --> 00:00:57,318
Old Gus is classic.

21
00:00:58,642 --> 00:01:00,267
- Feygnasse Team -

22
00:01:00,435 --> 00:01:02,125
.:: La Fabrique ::.

23
00:01:03,021 --> 00:01:04,868
Episode 208
<i>Ron and Tammy</i>

24
00:01:05,845 --> 00:01:07,162
mpm

25
00:01:07,797 --> 00:01:08,844
lestat78

26
00:01:19,180 --> 00:01:21,038
So, I propose that we change

27
00:01:21,206 --> 00:01:23,926
our name, from the Subcommittee
for Pit Beautification,

28
00:01:24,051 --> 00:01:26,571
to the Subcommittee
for Lot Beautification.

29
00:01:27,350 --> 00:01:29,296
April, stop that.
Who are you texting?

30
00:01:29,464 --> 00:01:30,339
You.

31
00:01:31,868 --> 00:01:33,159
She's texting me.

32
00:01:36,138 --> 00:01:37,263
Sorry you're bored.

33
00:01:39,873 --> 00:01:41,517
Hey, everyone.
Hey, Ann.

34
00:01:42,393 --> 00:01:43,561
Can I talk to you?

35
00:01:43,729 --> 00:01:46,647
Fine. Tom, you're in charge.
Keep the momentum going.

36
00:01:46,815 --> 00:01:48,875
- Keep these minds working.
- Absolutely.

37
00:01:50,381 --> 00:01:53,070
Would you rather be able to fly
or speak fluent French?

38
00:01:53,238 --> 00:01:54,864
- Donna, go.
- French.

39
00:01:55,032 --> 00:01:56,575
I don't wanna freak you out,

40
00:01:56,700 --> 00:01:59,452
but another department
filed a planning claim for lot 48.

41
00:02:00,372 --> 00:02:01,203
Who?

42
00:02:05,288 --> 00:02:06,288
Damn it!

43
00:02:06,727 --> 00:02:07,979
The library?

44
00:02:08,930 --> 00:02:11,130
Pawnee's Library Department
is the most diabolical,

45
00:02:11,440 --> 00:02:13,424
ruthless bunch of bureaucrats
I've ever seen.

46
00:02:13,698 --> 00:02:15,169
They're like a biker gang.

47
00:02:15,579 --> 00:02:17,636
But instead of shotguns
and crystal meth,

48
00:02:18,160 --> 00:02:19,089
they use

49
00:02:19,372 --> 00:02:20,569
political savvy.

50
00:02:20,817 --> 00:02:21,967
And shushing.

51
00:02:22,772 --> 00:02:25,352
It's gonna be okay.
It's not a done deal.

52
00:02:26,023 --> 00:02:27,188
You're right.

53
00:02:27,356 --> 00:02:28,939
Thanks for pulling me out.

54
00:02:29,189 --> 00:02:30,316
I need a good plan.

55
00:02:30,484 --> 00:02:32,443
I don't want to cause a panic.

56
00:02:32,611 --> 00:02:35,404
News flash, we're screwed.
We got a big problem with the library.

57
00:02:35,732 --> 00:02:37,262
Punk ass book jockeys.

58
00:02:37,485 --> 00:02:39,286
Why do we hate the library?

59
00:02:39,576 --> 00:02:41,786
It's the worst group of people
ever assembled.

60
00:02:41,954 --> 00:02:43,788
They're mean, conniving, rude,

61
00:02:43,956 --> 00:02:46,040
and well read,
which makes them very dangerous.

62
00:02:46,320 --> 00:02:49,126
And they're trying to steal lot 48
for a new branch.

63
00:02:49,294 --> 00:02:50,294
What?

64
00:02:51,239 --> 00:02:52,622
- On 48?
- Yeah!

65
00:02:53,006 --> 00:02:56,008
I actually think a library
would be nice that close to my house.

66
00:02:58,011 --> 00:03:00,012
But I'm not about
to say that in there.

67
00:03:00,180 --> 00:03:01,305
A new branch?

68
00:03:01,856 --> 00:03:03,136
I hate them.

69
00:03:04,901 --> 00:03:06,847
We need to make contact
with someone there.

70
00:03:06,972 --> 00:03:09,271
Does anybody know anybody
that works at the library?

71
00:03:10,060 --> 00:03:11,982
Well, we actually kind of do.

72
00:03:12,737 --> 00:03:16,017
The new deputy director
of the department is Tammy Swanson.

73
00:03:16,734 --> 00:03:17,934
Ron's ex-wife?

74
00:03:18,408 --> 00:03:20,798
That's terrific.
Or is that awful?

75
00:03:20,923 --> 00:03:22,792
He hates her, but he knows her.

76
00:03:23,148 --> 00:03:25,246
Everything's okay.
Or is it just the same?

77
00:03:25,725 --> 00:03:27,248
You're thinking out loud again.

78
00:03:27,416 --> 00:03:28,754
Am I? I am.

79
00:03:29,411 --> 00:03:30,251
Of course,

80
00:03:30,419 --> 00:03:33,746
that bitch of an ex-wife
is working for the library now.

81
00:03:33,871 --> 00:03:35,090
That is perfect.

82
00:03:35,215 --> 00:03:39,018
The worst person in the world
working at the worst place in the world.

83
00:03:39,311 --> 00:03:41,720
I have to go talk to her.
Give me something I can use.

84
00:03:42,106 --> 00:03:43,556
Does she have any weaknesses?

85
00:03:45,142 --> 00:03:47,059
What do you mean no?
Everybody has one.

86
00:03:47,227 --> 00:03:48,377
Not machines.

87
00:03:48,600 --> 00:03:50,513
I honestly believe
that she was programmed

88
00:03:50,638 --> 00:03:53,639
by someone from the future
to come back and destroy all happiness.

89
00:03:53,816 --> 00:03:54,525
I see.

90
00:03:54,693 --> 00:03:57,137
Just, I knew that you had
two ex-wives named Tammy.

91
00:03:57,432 --> 00:03:59,905
So, I was hoping that there was one
that you got along with.

92
00:04:00,205 --> 00:04:01,505
I hate 'em both.

93
00:04:02,617 --> 00:04:03,637
On my death bed,

94
00:04:03,762 --> 00:04:07,204
my final wish is to have
my ex-wives rushed to my side,

95
00:04:07,372 --> 00:04:08,991
so I can use my dying breath

96
00:04:09,116 --> 00:04:11,816
to tell them both to go to hell,
one last time.

97
00:04:12,419 --> 00:04:13,845
Would I get married again?

98
00:04:13,970 --> 00:04:14,909
Absolutely.

99
00:04:15,034 --> 00:04:17,423
If you don't believe in love,
what's the point of living?

100
00:04:18,754 --> 00:04:21,864
I'm just looking forward
to a thoughtful debate with Tammy,

101
00:04:21,989 --> 00:04:25,139
about the relative merits
of Parks versus Libraries.

102
00:04:25,807 --> 00:04:29,051
And in case something bad goes down,
I wore my sharpest rings.

103
00:04:29,928 --> 00:04:31,270
This one will tear you up.

104
00:04:37,157 --> 00:04:39,412
I'm Leslie Knope.
I called a little while ago.

105
00:04:40,102 --> 00:04:42,740
You have a lot of nerve
showing your face here.

106
00:04:43,162 --> 00:04:44,162
Excuse me?

107
00:04:44,287 --> 00:04:46,076
You have overdue book fees

108
00:04:46,244 --> 00:04:47,870
totaling 3 dollars, Missy.

109
00:04:48,444 --> 00:04:49,774
That is so typical.

110
00:04:49,899 --> 00:04:53,459
I should've known you'd use a low blow,
dirty pool, B.S. move like that.

111
00:04:53,928 --> 00:04:56,808
That's why everybody hates the library.
Here's your 3 dollars.

112
00:04:57,135 --> 00:04:58,255
See you in hell.

113
00:04:59,424 --> 00:05:01,800
I was just kidding, dear God.

114
00:05:01,968 --> 00:05:05,078
You did have 3 dollars worth of fines,
but I cleared them.

115
00:05:05,811 --> 00:05:09,058
We government gals have to watch
each other's backs, right?

116
00:05:09,712 --> 00:05:11,920
I know this is a trap
but I don't know how.

117
00:05:13,173 --> 00:05:14,688
Did you talk to Ron?

118
00:05:16,091 --> 00:05:18,108
Ron tends to exaggerate

119
00:05:18,276 --> 00:05:19,660
when it comes to me.

120
00:05:19,899 --> 00:05:22,099
I swear, I don't have cloven feet.

121
00:05:23,073 --> 00:05:24,073
Sit down.

122
00:05:26,161 --> 00:05:28,089
<i>Shine, shine, shine your shoes?</i>

123
00:05:28,348 --> 00:05:30,431
What do you say, sir?
Ma'am, shoeshine?

124
00:05:30,556 --> 00:05:32,081
I won't look up your skirt.

125
00:05:34,652 --> 00:05:35,876
My gosh...

126
00:05:36,044 --> 00:05:37,885
Crazy! What are you doing?
It's me.

127
00:05:38,733 --> 00:05:40,297
So, Ann, how you been?

128
00:05:40,596 --> 00:05:43,509
Good. Leslie told me
you were working here now.

129
00:05:43,677 --> 00:05:46,595
Just trying the rat race.
Chasing the cheese. Racing the rats.

130
00:05:46,763 --> 00:05:47,930
To get the cheese.

131
00:05:48,430 --> 00:05:50,307
Enough business talk.
You look ravishing.

132
00:05:51,035 --> 00:05:53,395
- Not a lot of customers.
- Business is a little slow.

133
00:05:53,520 --> 00:05:55,198
It is definitely due to...

134
00:05:55,963 --> 00:05:57,063
the economy.

135
00:05:58,523 --> 00:06:02,403
I've been hearing a lot of people
say that a lot, about a lot of stuff.

136
00:06:04,990 --> 00:06:06,782
You want those shoes shined?

137
00:06:06,950 --> 00:06:09,371
I'll give you a discount
because you're a friend of Ann's.

138
00:06:10,012 --> 00:06:12,246
- I'm more than a friend of Ann's.
- Not for long.

139
00:06:12,629 --> 00:06:14,081
- I'm sorry?
- I don't know.

140
00:06:14,301 --> 00:06:16,386
At least, he finally has a real job.

141
00:06:16,786 --> 00:06:18,615
When we were dating,
the only job he had

142
00:06:18,740 --> 00:06:21,588
was sending audition tapes
to <i>Survivor</i> and <i>Deal or no deal</i>.

143
00:06:22,104 --> 00:06:23,633
Hi, my name is Andy Dwyer,

144
00:06:23,758 --> 00:06:26,778
and I would be a perfect contestant
for deal or no deal.

145
00:06:29,439 --> 00:06:31,098
I want it to be a perfect park,

146
00:06:31,266 --> 00:06:32,725
with a state of the art,

147
00:06:32,893 --> 00:06:34,810
swing set and basketball courts.

148
00:06:34,978 --> 00:06:37,167
And off to the side,
a lovely sitting area

149
00:06:37,292 --> 00:06:39,855
for kids with asthma
to watch other kids play.

150
00:06:40,782 --> 00:06:42,948
If I'd had a park like that
when I was growing up,

151
00:06:43,073 --> 00:06:46,530
I probably wouldn't have gone through
such a prolonged mall slut phase.

152
00:06:49,018 --> 00:06:50,318
That's the goal.

153
00:06:50,474 --> 00:06:51,452
Listen,

154
00:06:51,620 --> 00:06:52,994
you got there first.

155
00:06:53,119 --> 00:06:57,018
I'm gonna withdraw my request,
as a professional courtesy to you.

156
00:06:57,700 --> 00:06:59,204
Government gals, right?

157
00:07:00,246 --> 00:07:01,546
Government gals.

158
00:07:03,503 --> 00:07:05,153
So, you talked to Tammy.

159
00:07:05,763 --> 00:07:08,836
What's it like to stare
into the eye of Satan's butt hole?

160
00:07:08,961 --> 00:07:11,180
She's changed.
She's a different person.

161
00:07:11,348 --> 00:07:12,598
It would be healthy

162
00:07:12,766 --> 00:07:15,074
for you to get a sense of closure.
Look at Mark and me.

163
00:07:15,199 --> 00:07:17,311
We slept together, talked about it.
We're friends.

164
00:07:18,297 --> 00:07:19,855
You slept with Brendanawicz?

165
00:07:22,400 --> 00:07:23,894
Tammy and I don't work.

166
00:07:24,019 --> 00:07:25,434
We are oil and water.

167
00:07:25,603 --> 00:07:27,905
Or oil and TNT and C-4

168
00:07:28,030 --> 00:07:30,230
and a detonator and a butane torch.

169
00:07:30,884 --> 00:07:32,242
Nothing good will come.

170
00:07:33,808 --> 00:07:34,808
Wait...

171
00:07:39,043 --> 00:07:40,584
She's here, isn't she?

172
00:07:53,105 --> 00:07:54,556
Good.
Good start.

173
00:07:56,578 --> 00:07:59,283
Sorry about this,
but I really want to talk to you.

174
00:07:59,408 --> 00:08:02,147
Couldn't we go have a cup of coffee?

175
00:08:05,023 --> 00:08:06,610
Let's get this over with.

176
00:08:09,614 --> 00:08:10,656
It means a lot.

177
00:08:10,934 --> 00:08:12,324
You're a miracle worker.

178
00:08:15,240 --> 00:08:17,270
- Do you know what you just did?
- Yes, Donna.

179
00:08:17,395 --> 00:08:19,665
I got two people to put aside
years of hostility

180
00:08:19,833 --> 00:08:21,959
and open a door
to the possibility of friendship.

181
00:08:22,127 --> 00:08:24,209
You were not here
when they got divorced. I was.

182
00:08:24,334 --> 00:08:25,834
Those two are crazy.

183
00:08:26,139 --> 00:08:27,798
And you opened the gates.

184
00:08:28,136 --> 00:08:29,336
To crazy town.

185
00:08:29,843 --> 00:08:32,245
Frankly,
I think Ron was acting like a baby.

186
00:08:32,370 --> 00:08:33,362
A little bit.

187
00:08:33,847 --> 00:08:35,139
What is he afraid of?

188
00:08:35,535 --> 00:08:36,976
Tammy's just a woman.

189
00:08:37,101 --> 00:08:38,251
A nice woman.

190
00:08:38,804 --> 00:08:40,686
You know, she's not a murderer.

191
00:08:40,854 --> 00:08:42,304
She's not a dragon.

192
00:08:42,687 --> 00:08:44,648
It's really good to see you, Ron.

193
00:08:45,845 --> 00:08:47,395
You've aged horribly.

194
00:08:48,359 --> 00:08:49,148
You...

195
00:08:49,601 --> 00:08:51,155
son of a bitch.

196
00:08:51,323 --> 00:08:54,199
- That didn't take long.
- My God! What's your problem?

197
00:08:54,660 --> 00:08:55,993
Nothing's changed.

198
00:08:56,161 --> 00:08:57,661
Who set the bed on fire?

200
00:09:00,002 --> 00:09:01,081
We're fine.

201
00:09:02,141 --> 00:09:03,125
Take it easy.

202
00:09:04,664 --> 00:09:06,815
When you meddle
in someone's personal life,

203
00:09:06,940 --> 00:09:08,189
it's just so...

204
00:09:08,624 --> 00:09:09,636
rewarding.

205
00:09:21,906 --> 00:09:22,686
Gun it!

206
00:09:23,327 --> 00:09:24,605
Usual place?

207
00:09:46,043 --> 00:09:49,026
Good morning, Jerry.
That is a beautiful sweater vest.

208
00:09:50,466 --> 00:09:52,398
You look like you could use $20.

209
00:09:52,523 --> 00:09:54,051
- Am I right?
- Why?

210
00:09:54,480 --> 00:09:56,553
'Cause you're a kid
and kids always need money.

211
00:09:56,893 --> 00:09:58,055
I need money.

212
00:09:58,443 --> 00:10:00,057
That's why you're my favorite.

213
00:10:00,225 --> 00:10:03,772
When Tiger Woods feels invincible,
he wears a red shirt and black pants.

214
00:10:03,897 --> 00:10:05,187
Good morning, deputy.

215
00:10:05,587 --> 00:10:07,564
Ron wears the same thing
after he had sex.

216
00:10:07,966 --> 00:10:09,316
I'm a simple man.

217
00:10:09,614 --> 00:10:12,200
I like pretty dark-haired women
and breakfast food.

218
00:10:12,325 --> 00:10:15,918
But this stock photo,
I bought at a framing store, isn't real.

219
00:10:16,066 --> 00:10:17,574
Today I got the real thing.

220
00:10:18,488 --> 00:10:21,288
A naked Tammy
made me breakfast this morning.

221
00:10:22,454 --> 00:10:24,471
I should've taken a picture of it.

222
00:10:26,751 --> 00:10:27,559
What's up?

223
00:10:27,684 --> 00:10:30,685
My girlfriend's ex is working
100 feet away from my desk.

224
00:10:30,810 --> 00:10:33,178
And he's been pretty clear
out wanting her back.

225
00:10:33,446 --> 00:10:34,974
What do I do with that?

226
00:10:35,468 --> 00:10:38,387
That's a tough one. Break up with her
and tell her to go out with me.

227
00:10:38,555 --> 00:10:42,034
Good. Can I ask you another question?
Why did I come to you for advice?

228
00:10:43,697 --> 00:10:45,894
You should play this one cool, man.
Be the grown-up.

229
00:10:46,119 --> 00:10:47,393
Take the high road.

230
00:10:47,979 --> 00:10:49,329
Ann's a classy chick.

231
00:10:49,454 --> 00:10:52,067
If you get down in the mud,
you're just gonna lose her respect.

232
00:10:53,209 --> 00:10:55,833
You've just made a surprising
amount of sense.

233
00:10:57,018 --> 00:10:58,831
I've never taken the high road.

234
00:10:58,956 --> 00:11:00,339
But I tell other people to.

235
00:11:00,464 --> 00:11:03,321
'Cause then there's more room for me
on the low road.

236
00:11:05,719 --> 00:11:08,119
So how did your coffee
with Tammy go?

237
00:11:08,418 --> 00:11:10,558
Leslie, I can't thank you enough

238
00:11:10,741 --> 00:11:12,671
for sticking your nose
where it didn't belong.

239
00:11:12,839 --> 00:11:13,915
I knew it.

240
00:11:14,040 --> 00:11:16,374
So what did you guys talk about?
Old times?

241
00:11:16,499 --> 00:11:18,231
I love talking about old times.

242
00:11:18,356 --> 00:11:20,429
New times too.
But there's something about old times.

243
00:11:20,597 --> 00:11:22,731
- You know what I mean?
- We didn't talk.

244
00:11:22,861 --> 00:11:23,890
We made love.

245
00:11:26,426 --> 00:11:28,586
Good.
Well, spare me the details.

246
00:11:28,711 --> 00:11:30,431
- I'm just happy...
- It was so intense.

247
00:11:30,556 --> 00:11:33,317
I didn't know where my flesh stopped
and hers began.

248
00:11:33,485 --> 00:11:35,035
You know what I mean?

249
00:11:36,209 --> 00:11:38,248
Our marriage was always
a complete disaster,

250
00:11:38,373 --> 00:11:40,949
but we did have that,

251
00:11:41,692 --> 00:11:42,789
the two of us.

252
00:11:42,914 --> 00:11:45,892
It's like doing peyote
and sneezing slowly

253
00:11:46,017 --> 00:11:47,205
for six hours.

254
00:11:47,373 --> 00:11:51,075
- This seems like a private matter.
- She knows her way around a penis.

255
00:11:53,639 --> 00:11:57,466
Well, I'm happy that it went well
and that you enjoyed your coffee.

256
00:11:58,082 --> 00:12:01,136
I truly believe that everyone
should be friends with their exes.

257
00:12:02,211 --> 00:12:05,612
I can't even tell you how many
of my exes' weddings I've been to.

258
00:12:11,744 --> 00:12:13,809
They're reading
their old divorce papers.

259
00:12:14,609 --> 00:12:16,285
Three fistfuls of hair?

260
00:12:16,410 --> 00:12:17,944
That's not even possible.

261
00:12:18,476 --> 00:12:21,657
Well, I'm too classy
to say I told you so, donna.

262
00:12:21,856 --> 00:12:23,756
So I wrote it on a post it.

263
00:12:29,326 --> 00:12:32,026
Why don't you take
the rest of the day off.

264
00:12:33,356 --> 00:12:36,370
I mean you spend so much time
worrying about this park.

265
00:12:36,495 --> 00:12:38,445
But really, who cares?

266
00:12:39,499 --> 00:12:40,377
I care.

267
00:12:40,502 --> 00:12:41,652
I care a lot.

268
00:12:42,124 --> 00:12:43,428
It's kinda my thing.

269
00:12:43,684 --> 00:12:45,565
But at the end of the day
what does it matter

270
00:12:45,690 --> 00:12:48,884
if the lot becomes a park
or a museum

271
00:12:49,086 --> 00:12:51,164
or a mega church?

272
00:12:51,646 --> 00:12:52,687
Or a library.

273
00:12:53,992 --> 00:12:55,215
Nobody said library.

274
00:12:56,237 --> 00:12:58,568
You've been talking to Tammy
about the lot?

275
00:13:01,155 --> 00:13:02,305
I swear on...

276
00:13:02,734 --> 00:13:03,573
a grave.

277
00:13:04,089 --> 00:13:05,104
Oh, my god!

278
00:13:05,655 --> 00:13:06,827
Tell me the truth.

279
00:13:07,025 --> 00:13:08,412
Are you giving her the lot?

280
00:13:09,532 --> 00:13:10,532
Not giving.

281
00:13:11,553 --> 00:13:13,083
We have discussed a trade.

282
00:13:13,534 --> 00:13:14,534
For what?

283
00:13:15,827 --> 00:13:16,670
Excuse me.

284
00:13:17,638 --> 00:13:18,638
More sex.

285
00:13:22,229 --> 00:13:24,553
Tammy, can I speak with you
for a second?

286
00:13:25,423 --> 00:13:27,736
I know what you're doing.
You don't care about ron.

287
00:13:27,861 --> 00:13:30,090
You're using him to get lot 48
for your library.

288
00:13:30,310 --> 00:13:31,869
Leslie, that's crazy.

289
00:13:32,367 --> 00:13:33,467
And correct.

290
00:13:33,850 --> 00:13:35,063
Why are you doing this?

291
00:13:35,430 --> 00:13:38,056
Les, there are two kinds
of women in this world.

292
00:13:38,181 --> 00:13:40,434
There are women who work hard

293
00:13:40,559 --> 00:13:42,687
and stress out about doing
the right thing

294
00:13:42,812 --> 00:13:44,614
and then there are women
who are cool.

295
00:13:45,527 --> 00:13:47,727
You could either be a Cleopatra

296
00:13:47,852 --> 00:13:50,279
or you could be an
Eleanor Roosevelt.

297
00:13:50,788 --> 00:13:52,585
I'd rather be Cleopatra.

298
00:13:53,207 --> 00:13:55,077
What kind of lunatic would
wanna be Cleopatra

299
00:13:55,202 --> 00:13:56,852
over Eleanor Roosevelt?

300
00:13:57,795 --> 00:13:59,895
Haven't you ever messed
with a man's head

301
00:14:00,020 --> 00:14:02,697
just to see what you could
get him to do for you?

302
00:14:02,822 --> 00:14:05,183
We do it all the time
in the library department.

303
00:14:05,308 --> 00:14:07,053
You should come join us sometime.

304
00:14:07,221 --> 00:14:09,389
I would never work
at the library department.

305
00:14:10,182 --> 00:14:11,408
I'm gonna tell Ron.

306
00:14:11,533 --> 00:14:13,469
And this little game
is gonna be over.

307
00:14:14,896 --> 00:14:16,224
Yeah, you better run.

308
00:14:16,728 --> 00:14:18,273
We're no longer government gals.

309
00:14:19,104 --> 00:14:20,603
Look, we need to talk.

310
00:14:22,553 --> 00:14:24,738
The planning department
reprioritization document?

311
00:14:24,906 --> 00:14:27,240
- Calm down. I haven't signed it yet.
- You're gonna.

312
00:14:27,705 --> 00:14:30,113
Because Tammy wants you to
and you do everything that she wants.

313
00:14:30,238 --> 00:14:32,537
She made some really good points
about libraries.

314
00:14:33,051 --> 00:14:36,059
Are you even listening to yourself?
You're defending the library now?

315
00:14:37,061 --> 00:14:38,010
The library.

316
00:14:38,135 --> 00:14:40,378
Of all the horrifying,
miserable things in the world.

317
00:14:40,546 --> 00:14:42,130
Some people like libraries.

318
00:14:42,795 --> 00:14:45,132
I can't even believe it.
Some people like libraries.

319
00:14:45,257 --> 00:14:46,927
Ron, she's in your head.

320
00:14:47,380 --> 00:14:50,156
You said she was a manipulative monster
and you were right.

321
00:14:51,101 --> 00:14:52,140
Tammy's changed.

322
00:14:52,325 --> 00:14:54,543
Look me in the eye
and tell me that she's changed.

323
00:14:56,055 --> 00:14:57,229
Look me in the eye.

324
00:15:00,895 --> 00:15:02,108
I'm so screwed now.

325
00:15:02,561 --> 00:15:04,398
- Her hooks are in my brain.
- Yep.

326
00:15:04,523 --> 00:15:07,123
She has all the power
and I have nothing.

327
00:15:08,248 --> 00:15:09,449
I'm so little.

328
00:15:21,295 --> 00:15:23,216
Why don't you just break up
with her?

329
00:15:23,736 --> 00:15:25,006
I don't think I can.

330
00:15:25,598 --> 00:15:27,103
We would just end up naked.

331
00:15:27,228 --> 00:15:29,678
And I'd give her your lot
and my house

332
00:15:29,910 --> 00:15:31,338
and God knows what else.

333
00:15:31,463 --> 00:15:33,113
I have no control over my actions.

334
00:15:33,238 --> 00:15:36,177
- Like she's crawled up inside me...
- God, ron.

335
00:15:36,302 --> 00:15:38,645
I don't want to hear about
your disgusting sex anymore.

336
00:15:39,079 --> 00:15:40,479
Okay, now sack up.

337
00:15:40,976 --> 00:15:44,109
You are Ron freakin' Swanson!

338
00:15:44,277 --> 00:15:45,860
Not around her I'm not.

339
00:15:46,250 --> 00:15:48,864
It took years of intense,
focused hatred

340
00:15:48,989 --> 00:15:50,129
to get over her.

341
00:15:50,254 --> 00:15:52,754
And now I've undone
all that great work.

342
00:15:54,500 --> 00:15:56,121
You've gotta help me to break up.

343
00:15:56,572 --> 00:15:58,415
I shouldn't get involved in this.

344
00:15:58,737 --> 00:16:01,322
Now you don't want to get involved?

345
00:16:01,600 --> 00:16:04,504
"It's just coffee, Ron. "
"She's changed, Ron. "

346
00:16:04,672 --> 00:16:06,965
"I let Mark nail me
and we're still friends. "

347
00:16:07,191 --> 00:16:09,083
I never... I would never
use those words.

348
00:16:09,208 --> 00:16:11,108
I never said... Point taken.

349
00:16:12,970 --> 00:16:15,056
Let's go dump your ex.

350
00:16:15,401 --> 00:16:16,401
Thank you.

351
00:16:18,090 --> 00:16:20,024
Would you like to be in the room
when I tell her

352
00:16:20,149 --> 00:16:22,126
- or would you rather wait outside?
- In the room.

353
00:16:22,251 --> 00:16:24,413
I don't want her to think
I'm a wimp.

354
00:16:24,924 --> 00:16:26,915
Here are the ground rules.
Don't talk to her.

355
00:16:27,040 --> 00:16:30,291
Do not make eye contact with her.
Don't believe anything she says.

356
00:16:30,416 --> 00:16:32,275
Just sit there like a potted plant.

357
00:16:32,400 --> 00:16:33,782
Can you do that?

358
00:16:34,545 --> 00:16:35,545
Come on.

359
00:16:40,239 --> 00:16:42,167
So how's the shoeshine game?

360
00:16:42,723 --> 00:16:43,793
I'm on a break.

361
00:16:43,918 --> 00:16:47,293
One of the many advantages
of owning your own business.

362
00:16:47,556 --> 00:16:49,653
You know that's not
your situation, right?

363
00:16:52,065 --> 00:16:53,122
Listen, Donna.

364
00:16:53,247 --> 00:16:54,262
Quick question.

365
00:16:54,430 --> 00:16:56,681
Ann is trying to decide
between Mark and myself.

366
00:16:57,097 --> 00:16:58,350
.- }If you had to choose,

367
00:16:58,518 --> 00:17:00,559
who would you choose?
Right now, on the spot.

368
00:17:00,684 --> 00:17:03,007
I'm not sure.
Why don't you spin around for me.

369
00:17:03,132 --> 00:17:04,814
Can we talk privately?

370
00:17:05,258 --> 00:17:06,961
- Sure.
- Just out here.

371
00:17:12,824 --> 00:17:15,144
So, Tammy, for that
and many other reasons

372
00:17:15,269 --> 00:17:17,869
Ron has decided to end
this relationship.

373
00:17:22,291 --> 00:17:23,216
Wait a minute.

374
00:17:23,341 --> 00:17:26,920
Ron brought you here
to break up with me for him?

375
00:17:27,235 --> 00:17:28,380
She volunteered.

376
00:17:31,763 --> 00:17:33,802
Ron doesn't want to break up
with me.

377
00:17:34,611 --> 00:17:36,235
What Ron wants to do

378
00:17:36,957 --> 00:17:39,281
is leave here right now.

379
00:17:40,044 --> 00:17:42,693
Go to the sleaziest motel in town

380
00:17:43,538 --> 00:17:46,741
and wrap himself around me
like a coiled snake.

381
00:17:47,569 --> 00:17:49,776
- No, he doesn't.
- I'm pretty sure he does.

382
00:17:52,737 --> 00:17:54,948
See? He's completely over you.

383
00:17:55,331 --> 00:17:57,136
Look, I understand

384
00:17:57,467 --> 00:18:00,201
that this Ann situation is awkward.

385
00:18:00,326 --> 00:18:03,194
But I like you a lot as a dude.

386
00:18:03,375 --> 00:18:05,190
And I hope that there is some way

387
00:18:05,315 --> 00:18:08,420
that we can both be mature
and maybe be friends.

388
00:18:09,104 --> 00:18:10,546
Yeah, I doubt it.

389
00:18:11,032 --> 00:18:13,285
I mean, I think
you're a cool dude too.

390
00:18:13,410 --> 00:18:15,009
I like you as well.

391
00:18:15,265 --> 00:18:17,365
But I'm still in love with Ann.

392
00:18:18,380 --> 00:18:20,890
I couldn't have been more upfront
about that.

393
00:18:22,143 --> 00:18:24,330
Is punching allowed
on the high road?

394
00:18:25,346 --> 00:18:26,746
I'm sorry, Leslie.

395
00:18:27,823 --> 00:18:28,823
She wins.

396
00:18:30,060 --> 00:18:31,409
I can't resist her.

397
00:18:31,534 --> 00:18:32,819
But, you have to.

398
00:18:32,987 --> 00:18:35,535
Stay out of this.
This is our relationship.

399
00:18:35,660 --> 00:18:38,742
He's my man and we have
something twisted and beautiful.

400
00:18:41,187 --> 00:18:43,705
You want Ron.
That's what this is all about.

401
00:18:43,980 --> 00:18:45,612
No, that's insane.

402
00:18:47,547 --> 00:18:49,797
Fine, I had one dream, but no, no.

403
00:18:49,922 --> 00:18:52,088
Baby, don't you see
what's happening here?

404
00:18:52,256 --> 00:18:55,499
She's manipulating you
because she's jealous of me

405
00:18:55,875 --> 00:18:58,720
and the things I get to do
to your body and face.

406
00:18:59,765 --> 00:19:01,810
I'm here because Ron is my friend.

407
00:19:01,935 --> 00:19:05,310
And I don't like seeing my friends
miserable. And you make him miserable.

408
00:19:05,710 --> 00:19:08,396
So, Ron, you can give her
the lot or don't. Whatever.

409
00:19:09,005 --> 00:19:10,346
Just please...

410
00:19:10,827 --> 00:19:12,275
find a way to be happy.

411
00:19:17,613 --> 00:19:18,700
No, wait.

412
00:19:18,825 --> 00:19:20,572
Ron, it's okay.
Just sign the thing.

413
00:19:21,114 --> 00:19:24,120
It's not okay. You just put my needs
in front of your own.

414
00:19:24,406 --> 00:19:26,331
No woman has ever done that
for me before.

415
00:19:26,499 --> 00:19:27,813
I'm sure that's not true.

416
00:19:27,938 --> 00:19:29,667
You see the kind of women
that I choose.

417
00:19:30,214 --> 00:19:32,090
Look, just wait for me downstairs.

418
00:19:32,215 --> 00:19:33,465
If I'm not down in 5 min,

419
00:19:33,590 --> 00:19:36,406
it's only because I'm receiving
a pleasure so intense...

420
00:19:41,159 --> 00:19:42,889
What the hell are you doing?

421
00:19:43,164 --> 00:19:45,391
Andy and Ann's family shoeshine?

422
00:19:45,559 --> 00:19:47,185
I thought it had a nice ring to it.

423
00:19:47,638 --> 00:19:51,272
Mark has been really cool about this.
And you're throwing it in his face.

424
00:19:51,440 --> 00:19:53,296
But I still have feelings for you.

425
00:19:53,786 --> 00:19:55,737
I said it.
What do you want me to do?

426
00:19:55,862 --> 00:19:58,029
Not mention those feelings
to your boyfriend?

427
00:19:59,490 --> 00:20:01,307
And you might wanna take
these pictures down.

428
00:20:01,432 --> 00:20:02,432
Oh, my God.

429
00:20:02,557 --> 00:20:05,380
- Take those pictures down.
- What? It's fine. What did I...

430
00:20:05,505 --> 00:20:06,935
- All of it!
- I'm gonna.

431
00:20:07,060 --> 00:20:08,060
Now!

432
00:20:09,756 --> 00:20:11,876
- Give me the bikini one.
- The teeny one?

433
00:20:12,001 --> 00:20:13,128
Bikini one.

434
00:20:21,996 --> 00:20:24,078
- You didn't give her.
- Let's get out of here.

435
00:20:24,203 --> 00:20:26,266
- Is part of your moustache missing?
- Yes.

436
00:20:26,434 --> 00:20:28,268
- Walk.
- There's a push pin in your face.

437
00:20:28,393 --> 00:20:30,583
Leave it in.
Can't you walk faster?

438
00:20:30,708 --> 00:20:32,808
My legs are shorter than yours.

439
00:20:38,427 --> 00:20:39,827
We had a good run.

440
00:20:43,946 --> 00:20:46,307
Thank you for saving my future park.

441
00:20:46,540 --> 00:20:48,204
It must have been hard for you.

442
00:20:48,908 --> 00:20:50,949
You didn't kill Tammy, did you?

443
00:20:51,197 --> 00:20:53,603
I'm afraid she can't be killed.

444
00:20:55,513 --> 00:20:56,504
To exes...

445
00:20:57,167 --> 00:20:59,299
may they always stay that way.

446
00:21:00,174 --> 00:21:01,224
Tammy is...

447
00:21:02,881 --> 00:21:04,179
a mean person.

448
00:21:04,716 --> 00:21:06,681
Come on,
you can do better than that.

449
00:21:07,648 --> 00:21:10,144
- She's a great A bitch.
- There we go.

450
00:21:10,853 --> 00:21:13,479
Every time she laughs,
an angel dies.

451
00:21:13,791 --> 00:21:15,690
Even telemarketers avoid her.

452
00:21:16,017 --> 00:21:19,220
Her birth was payback
for the sins of men.

453
00:21:19,345 --> 00:21:21,821
But, you know,
the worst thing about her,

454
00:21:22,132 --> 00:21:23,781
She works for the library.

455
00:21:24,162 --> 00:21:26,087
She works for the library.

456
00:21:26,494 --> 00:21:28,063
www.sous-titres.eu

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
