﻿1
00:00:01,138 --> 00:00:02,583
Excuse me.

2
00:00:03,120 --> 00:00:04,754
- Are you Leslie Knope?
- Yes.

3
00:00:04,788 --> 00:00:06,255
We are part of a group

4
00:00:06,289 --> 00:00:08,324
that tries to identify
potential candidates

5
00:00:08,358 --> 00:00:10,192
for political office
here in town.

6
00:00:10,227 --> 00:00:12,595
Are you theoretically interested
in running for office?

7
00:00:12,629 --> 00:00:14,163
Abso-toot-ly I am.

8
00:00:14,197 --> 00:00:16,098
Very sorry that I just used
that word.

9
00:00:16,133 --> 00:00:18,534
Before we go any further,
I need you to tell me

10
00:00:18,568 --> 00:00:19,935
is there a scandal out there?

11
00:00:19,953 --> 00:00:24,507
Is there anything at all you
need to tell us about your life?

12
00:00:24,541 --> 00:00:26,609
Nope.

13
00:00:30,330 --> 00:00:31,780
Who were the suits?

14
00:00:31,798 --> 00:00:34,583
They want me to run for office.

15
00:00:34,618 --> 00:00:37,136
Oh, my God!
Leslie, yea!

16
00:00:37,170 --> 00:00:38,521
I know, right?
Yea.

17
00:00:38,555 --> 00:00:39,722
- Yea!
- Yea!

18
00:00:39,756 --> 00:00:40,956
- Yea!
- Yea!

19
00:00:40,991 --> 00:00:42,358
Wait, what does that mean
about you and Ben?

20
00:00:42,392 --> 00:00:43,426
I don't know.

21
00:00:43,460 --> 00:00:44,727
I think
it's gonna be really bad.

22
00:00:44,761 --> 00:00:46,462
Uh-oh. You just wanna go back
to saying "yea"?

23
00:00:46,496 --> 00:00:47,630
Yes, please.

24
00:00:47,664 --> 00:00:51,233
- Okay, yea.
- Yea. Yea.

25
00:00:51,268 --> 00:00:52,852
- Knope, follow me.
- Just one second.

26
00:00:52,903 --> 00:00:54,437
- Now!
- Ron, hey!

27
00:00:54,471 --> 00:00:56,572
Whoa! Whoa! Whoa, whoa, whoa!
Ron! Ron, Ron, Ron, Ron, Ron!

28
00:00:56,606 --> 00:00:57,740
What's going on?

29
00:00:57,774 --> 00:00:59,341
My ex-wife Tammy is back.

30
00:00:59,376 --> 00:01:00,743
Yeah, I saw her
in the courtyard.

31
00:01:00,777 --> 00:01:03,479
No, my other ex-wife Tammy.
Tammy one.

32
00:01:03,497 --> 00:01:05,781
I have accrued 228
personal days.

33
00:01:05,815 --> 00:01:07,533
Starting right now,
I'm using all of them.

34
00:01:09,653 --> 00:01:13,155
While I'm gone,
you're in charge.

35
00:01:13,173 --> 00:01:16,542
Ah!

36
00:01:16,593 --> 00:01:20,212
Also, I keep a sizable supply
of ground chuck in my desk.

37
00:01:20,263 --> 00:01:22,932
Remove it or it will begin
to smell.

38
00:01:22,966 --> 00:01:24,600
Godspeed.

39
00:01:26,937 --> 00:01:28,003
- Hi, Ron.
- Move!

40
00:01:28,021 --> 00:01:30,473
Whoa!

41
00:01:30,507 --> 00:01:32,041
Ah, geez.

42
00:01:35,078 --> 00:01:43,285
<font color="#ec14bd">Sync & corrected by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

43
00:01:53,016 --> 00:01:55,487
Warning! High levels
of swagger coming through.

44
00:01:55,607 --> 00:01:57,666
Tommy Haverford back
in the Parks Department.

45
00:01:57,701 --> 00:02:00,317
Entertainment720 hockey jersey,
anybody?

46
00:02:01,404 --> 00:02:02,888
Branded mouse pad?

47
00:02:02,923 --> 00:02:05,874
Entertainment720 is just
a little multi-national

48
00:02:05,892 --> 00:02:08,093
entertainment company
that I started with my boy

49
00:02:08,145 --> 00:02:09,411
Jean-Ralphio.

50
00:02:09,446 --> 00:02:11,113
What exactly do we do?

51
00:02:11,148 --> 00:02:13,782
Let's just say it's too hard
to explain.

52
00:02:13,817 --> 00:02:16,085
Donna, you look amazing.
How are the kids?

53
00:02:16,119 --> 00:02:17,653
- I don't have kids.
- Wow.

54
00:02:17,687 --> 00:02:19,655
- How long has it been?
- Three weeks.

55
00:02:21,074 --> 00:02:22,241
So did you get a chance to look

56
00:02:22,275 --> 00:02:23,576
at my sample position papers?

57
00:02:23,610 --> 00:02:26,245
How do you feel about my stance
on Egyptian debt relief?

58
00:02:26,279 --> 00:02:28,113
Uh, well, it probably won't
come up

59
00:02:28,165 --> 00:02:30,749
in a local City Council
election.

60
00:02:30,784 --> 00:02:33,502
But your thoroughness,
as always, is impressive.

61
00:02:33,537 --> 00:02:34,787
Thank you.

62
00:02:34,838 --> 00:02:37,673
Now, we have three months.

63
00:02:37,707 --> 00:02:39,908
I think we should start
talking strategy.

64
00:02:39,926 --> 00:02:42,094
My strategy is to win.

65
00:02:42,128 --> 00:02:45,297
I've been dreaming about running
for public office my whole life.

66
00:02:45,348 --> 00:02:47,716
While other girls were playing
with Barbies,

67
00:02:47,750 --> 00:02:51,053
I was playing with a
Geraldine Ferraro action figure

68
00:02:51,087 --> 00:02:53,455
that I made myself from
a picture of her that I glued

69
00:02:53,490 --> 00:02:55,191
onto a popsicle stick.

70
00:02:55,225 --> 00:02:56,492
See you next week?

71
00:02:56,526 --> 00:02:57,526
Okay, I'm looking forward
to it.

72
00:02:57,561 --> 00:02:58,644
Hey! What--

73
00:02:58,695 --> 00:03:01,814
- Who was that?
- Uh...

74
00:03:01,865 --> 00:03:03,899
Lady--just ladies.

75
00:03:03,933 --> 00:03:06,202
Meeting for the ladies'
yacht club.

76
00:03:06,236 --> 00:03:07,736
- Anchors away, ladies.
- What?

77
00:03:07,770 --> 00:03:09,271
Just don't--

78
00:03:09,289 --> 00:03:10,839
Ladies' yacht club?

79
00:03:10,874 --> 00:03:12,825
I cannot believe you haven't
told him yet.

80
00:03:12,876 --> 00:03:14,777
After I tell him,
I'm running for office,

81
00:03:14,811 --> 00:03:16,161
we're gonna have to break up.

82
00:03:16,213 --> 00:03:18,464
Why can't you just keep
sneaking around?

83
00:03:18,498 --> 00:03:22,618
Ann, you beautiful, naive,
sophisticated, newborn baby.

84
00:03:22,636 --> 00:03:24,119
What?

85
00:03:24,154 --> 00:03:26,789
Before, if people found out,
we would maybe lose our jobs.

86
00:03:26,823 --> 00:03:29,892
Now, if people find out,
it's gonna be a major scandal.

87
00:03:29,926 --> 00:03:32,461
My campaign will be over
before it begins.

88
00:03:32,479 --> 00:03:34,263
- What are you gonna do?
- I have a plan.

89
00:03:34,297 --> 00:03:35,764
And it's really good.

90
00:03:35,799 --> 00:03:38,000
I'm gonna postpone telling him

91
00:03:38,034 --> 00:03:40,969
until you figure out
what I'm supposed to do.

92
00:03:40,987 --> 00:03:42,304
So get crackin'.

93
00:03:42,322 --> 00:03:43,656
How is this on me?

94
00:03:43,690 --> 00:03:46,825
Fine! I'll do it.
God, who keeps emailing me?

95
00:03:46,860 --> 00:03:49,495
- A penis.
- What?

96
00:03:49,529 --> 00:03:51,614
Oh, my God.

97
00:03:51,648 --> 00:03:53,382
Oh--

98
00:03:53,416 --> 00:03:54,717
It's emailing me too.

99
00:03:54,751 --> 00:03:57,386
Whoever this is sent it
to every woman

100
00:03:57,420 --> 00:04:00,005
in the entire government.

101
00:04:00,040 --> 00:04:02,324
Oh, my God.

102
00:04:02,359 --> 00:04:06,262
He's gonna be so embarrassed.
I mean, that's a crazy accident.

103
00:04:06,296 --> 00:04:10,032
No, Andy, he did it on purpose.

104
00:04:10,066 --> 00:04:12,201
You know what? Respect.
That's a baller move.

105
00:04:12,235 --> 00:04:13,369
Pun intended.

106
00:04:13,403 --> 00:04:14,720
Excuse me.

107
00:04:14,771 --> 00:04:15,938
Chris, Jerry's making us look
at dirty pictures

108
00:04:15,972 --> 00:04:17,056
on his computer.

109
00:04:17,107 --> 00:04:18,891
It is exactly because of
that lewd photo

110
00:04:18,942 --> 00:04:20,209
that I am here.

111
00:04:20,243 --> 00:04:21,894
Ben and I are launching a full
investigation.

112
00:04:21,945 --> 00:04:24,846
And I want to apologize
to all the women...

113
00:04:24,864 --> 00:04:26,248
And Jerry.

114
00:04:26,283 --> 00:04:29,351
If I could go back in time,
and cut your eyeballs out,

115
00:04:29,386 --> 00:04:30,352
I would.

116
00:04:30,387 --> 00:04:32,955
- Wow, that is so sweet.
- Thank you.

117
00:04:32,989 --> 00:04:35,791
We are getting a lot of media
requests about this.

118
00:04:35,825 --> 00:04:37,760
So Leslie, can you do
Perd Hapley?

119
00:04:37,794 --> 00:04:40,245
Ho! She can do Perd Hapley.

120
00:04:40,297 --> 00:04:42,164
Hi-oh!

121
00:04:42,198 --> 00:04:44,133
Seriously,
can you do his TV show?

122
00:04:44,167 --> 00:04:46,552
Oh, yeah.

123
00:04:46,586 --> 00:04:48,754
Andrew my man-drew,
what are you up to?

124
00:04:48,805 --> 00:04:51,373
I'm about to do an experiment
where I huck this at the wall

125
00:04:51,408 --> 00:04:53,559
and see what happens.

126
00:04:53,593 --> 00:04:55,210
But that can wait.
Shoe shine?

127
00:04:55,228 --> 00:04:56,562
Andy, I'm gonna be honest.

128
00:04:56,596 --> 00:04:58,547
I came here today
to talk to you.

129
00:04:58,565 --> 00:05:01,717
Now look, you're enthusiastic.
You're hard-working.

130
00:05:01,751 --> 00:05:03,152
You can lift heavy things.

131
00:05:03,186 --> 00:05:04,253
I think there might be a place
for you

132
00:05:04,287 --> 00:05:06,238
over at Entertainment720.

133
00:05:06,272 --> 00:05:08,157
Oh, my God.

134
00:05:08,191 --> 00:05:09,191
What is that?

135
00:05:09,225 --> 00:05:10,576
It's my company!

136
00:05:10,610 --> 00:05:12,060
I don't know, Tom.

137
00:05:12,078 --> 00:05:14,430
I mean, I'm just now getting
really good at shoe shining.

138
00:05:14,464 --> 00:05:16,432
And I mean, I'm still
pretty bad at it.

139
00:05:16,466 --> 00:05:21,136
Let me ask you this.
Are you living your dreams?

140
00:05:21,171 --> 00:05:23,739
- I don't know, Tom.
- Well, I'm living mine.

141
00:05:23,773 --> 00:05:28,293
And if you wanna live yours,
here's my card.

142
00:05:28,345 --> 00:05:31,747
- It's impossible to read.
- Black print, black background.

143
00:05:31,765 --> 00:05:33,298
It's the coolest possible
color scheme.

144
00:05:33,350 --> 00:05:35,267
- Yeah.
- It's also a strong magnet.

145
00:05:35,301 --> 00:05:37,686
So don't put it in your wallet.
It will erase your credit cards,

146
00:05:37,721 --> 00:05:39,772
guaranteed.

147
00:05:39,806 --> 00:05:43,525
Destroy my credit cards.
Debt and everything?

148
00:05:43,560 --> 00:05:47,229
Mr. Mayor, I promise you
that we will find the owner

149
00:05:47,263 --> 00:05:48,497
of that weiner

150
00:05:48,531 --> 00:05:51,033
and we will punish him.
Good-bye.

151
00:05:51,067 --> 00:05:52,434
- Ann Perkins!
- Hey.

152
00:05:52,469 --> 00:05:54,370
I didn't know who to come to
with this,

153
00:05:54,404 --> 00:05:57,990
but that picture, uh--there's
something very disturbing

154
00:05:58,041 --> 00:05:59,608
about the testicles.

155
00:05:59,626 --> 00:06:01,510
I've always felt
the same way myself.

156
00:06:01,544 --> 00:06:04,213
The testicles are like
the ears of the genital system.

157
00:06:04,247 --> 00:06:05,781
They serve a very
important function,

158
00:06:05,799 --> 00:06:07,716
but they're not that great
to look at.

159
00:06:07,751 --> 00:06:11,987
Okay, wow.
No, I was speaking medically.

160
00:06:12,021 --> 00:06:13,322
They're enlarged.

161
00:06:13,356 --> 00:06:15,841
I think that maybe that guy
is sick with something--

162
00:06:15,892 --> 00:06:18,460
perhaps, a hernia, or mumps.

163
00:06:18,495 --> 00:06:21,397
Ann Perkins!
You really know your testes.

164
00:06:21,431 --> 00:06:24,066
Thank you?

165
00:06:24,100 --> 00:06:26,318
For a female perspective
on this scandal,

166
00:06:26,352 --> 00:06:28,970
we turn to a woman,
Leslie Knope.

167
00:06:28,988 --> 00:06:30,522
I'm about to ask you a question
right now

168
00:06:30,573 --> 00:06:31,907
and that question is this.

169
00:06:31,941 --> 00:06:34,526
The lewd photo--
just how big a deal is it?

170
00:06:34,577 --> 00:06:37,079
Well, frankly, Perd,
it's not <i>that</i> big a deal.

171
00:06:37,113 --> 00:06:38,614
If you know what I mean.

172
00:06:38,648 --> 00:06:41,166
I don't know what you mean.

173
00:06:41,201 --> 00:06:43,585
But it had the cadence
of a joke.

174
00:06:43,620 --> 00:06:46,021
When men in government
behave this way,

175
00:06:46,055 --> 00:06:48,424
they betray the public's trust.

176
00:06:48,458 --> 00:06:50,843
Maybe it's time for more women
to be in charge.

177
00:06:50,877 --> 00:06:53,495
There you have it,
where "it" is the thing

178
00:06:53,530 --> 00:06:56,165
Leslie Knope just said
about this situation.

179
00:06:56,199 --> 00:06:57,399
Perd Hap--

180
00:06:57,434 --> 00:06:58,567
How was that?
Was that okay?

181
00:06:58,601 --> 00:07:01,136
Amazing.
That sound bite is gold.

182
00:07:01,171 --> 00:07:03,021
You're gonna get great
feedback on this,

183
00:07:03,056 --> 00:07:05,224
and we think we can capitalize.

184
00:07:05,275 --> 00:07:07,676
You're gonna announce
your candidacy on Friday.

185
00:07:07,710 --> 00:07:11,246
Friday.
In three months.

186
00:07:11,281 --> 00:07:14,049
No, this Friday.
Is that okay?

187
00:07:14,083 --> 00:07:15,984
No, yeah.
Yes, it is great.

188
00:07:16,018 --> 00:07:18,904
Okay, we'll do that then
now soon.

189
00:07:18,955 --> 00:07:21,240
And, um, I just have something
I've been postponing for a while

190
00:07:21,291 --> 00:07:23,192
that I need to do,
but I'll do it now.

191
00:07:23,226 --> 00:07:25,160
- Great.
- Great.

192
00:07:25,195 --> 00:07:26,695
Yea.

193
00:07:28,233 --> 00:07:29,097
I know it.

194
00:07:29,725 --> 00:07:31,254
- Ben?
- Uh, okay.

195
00:07:31,374 --> 00:07:33,111
Ben? I need to talk to you.

196
00:07:34,481 --> 00:07:36,663
That was my brother.
He just had a baby girl.

197
00:07:36,783 --> 00:07:39,417
Oh, my God, that's horrible.
Or good--full disclosure.

198
00:07:39,537 --> 00:07:40,837
I didn't listen to what
you just said.

199
00:07:40,871 --> 00:07:42,122
I need to talk to you
about something--

200
00:07:42,173 --> 00:07:43,506
Okay, well, before you
say anything,

201
00:07:43,541 --> 00:07:46,550
let me just show you
what I got at Doug's Donuts.

202
00:07:46,670 --> 00:07:49,457
It's an L-shaped eclair!

203
00:07:49,726 --> 00:07:52,716
The "L" is for Leslie...
For your name.

204
00:07:57,521 --> 00:07:59,422
Hey.

205
00:07:59,440 --> 00:08:01,641
- How are you?
- Bad, really bad.

206
00:08:01,692 --> 00:08:03,960
Oh, God.
How did the breakup go?

207
00:08:03,995 --> 00:08:07,163
Well, I started crying
because he gave me an eclair.

208
00:08:07,198 --> 00:08:09,032
And then we made out
and spent the night together.

209
00:08:09,066 --> 00:08:11,901
But this morning in the cold
light of day,

210
00:08:11,936 --> 00:08:14,270
I cooked breakfast
and we made out some more.

211
00:08:14,305 --> 00:08:16,373
Wow, you opposite-of-broke-up
with him.

212
00:08:16,407 --> 00:08:20,510
He gave me an eclair, Ann!

213
00:08:20,544 --> 00:08:23,613
Joe, I've determined that
the lewd picture was sent by you

214
00:08:23,647 --> 00:08:27,984
because you sent it from your
personal email address,

215
00:08:28,019 --> 00:08:33,857
stinkmaster69@alumni.sarahlawrencecollege.edu.

216
00:08:33,891 --> 00:08:35,592
Really?
Sarah Lawrence?

217
00:08:35,626 --> 00:08:37,861
Yeah. I wanted a small
college experience.

218
00:08:37,895 --> 00:08:40,814
You also bragged about it
to many of your co-workers.

219
00:08:40,848 --> 00:08:43,566
There's an old sewage
department saying.

220
00:08:43,601 --> 00:08:45,702
"If you've got a nice
drainpipe,

221
00:08:45,736 --> 00:08:47,337
there's no reason to hide it."

222
00:08:47,371 --> 00:08:48,538
I doubt that's a saying.

223
00:08:48,572 --> 00:08:50,973
We are terminating you
effective immediately.

224
00:08:50,991 --> 00:08:52,442
Is this because you're jealous?

225
00:08:52,476 --> 00:08:54,978
No.
It's because you broke the law.

226
00:08:55,012 --> 00:08:56,980
The law of having
an amazing package.

227
00:08:57,014 --> 00:08:58,481
Before we have him
forcibly removed,

228
00:08:58,499 --> 00:09:00,834
you should know this, that
one of our female employees,

229
00:09:00,868 --> 00:09:02,752
a nurse, examined the photo
you sent her--

230
00:09:02,787 --> 00:09:04,421
I bet she did.

231
00:09:04,455 --> 00:09:05,922
And you have the mumps.

232
00:09:05,956 --> 00:09:07,257
You might wanna get
that checked.

233
00:09:07,291 --> 00:09:08,625
Sweet.

234
00:09:08,659 --> 00:09:11,428
Someone's got mumps
on his lumps.

235
00:09:11,462 --> 00:09:13,680
Up high.
Anyone?

236
00:09:13,714 --> 00:09:15,198
Ron isn't here.

237
00:09:15,232 --> 00:09:19,436
His ex-wife Tammy came,
and he got scared and ran away.

238
00:09:19,470 --> 00:09:21,054
Well, as acting manager
of the Parks Department,

239
00:09:21,105 --> 00:09:24,173
I declare that everything
you are saying is stupid.

240
00:09:24,191 --> 00:09:25,742
I love you.

241
00:09:25,776 --> 00:09:28,678
Hey, so Tom offered me a job
again.

242
00:09:28,712 --> 00:09:31,247
As my manager,
what do you think I should do?

243
00:09:31,282 --> 00:09:33,182
Well, what do you wanna do?

244
00:09:33,200 --> 00:09:36,252
I think it'd be kinda cool
to work for Tom.

245
00:09:36,287 --> 00:09:38,121
So take it.

246
00:09:38,155 --> 00:09:41,791
But working at a start-up
can be kind of risky.

247
00:09:41,826 --> 00:09:42,959
So don't take it.

248
00:09:42,993 --> 00:09:44,360
But Tom told me to follow
my dreams.

249
00:09:44,395 --> 00:09:45,762
So take it.

250
00:09:45,796 --> 00:09:49,132
- But this isn't my dream.
- So don't take it.

251
00:09:49,166 --> 00:09:51,468
- I don't know what to do.
- Me neither.

252
00:09:51,502 --> 00:09:53,586
You give such good advice.

253
00:09:53,637 --> 00:09:55,588
Babe, I love you.

254
00:09:55,639 --> 00:09:57,874
You're welcome.

255
00:10:00,778 --> 00:10:02,211
Ann Perkins!

256
00:10:02,229 --> 00:10:04,264
Hey. Did you maybe tell people

257
00:10:04,315 --> 00:10:07,383
that I diagnosed that guy with
mumps based on his porn photo?

258
00:10:07,418 --> 00:10:08,918
I did!
I was so proud of you.

259
00:10:08,953 --> 00:10:11,321
Okay, because now I have
everyone in City Hall

260
00:10:11,355 --> 00:10:12,856
sending me pictures
of their junk,

261
00:10:12,890 --> 00:10:14,757
asking me if they have mumps.

262
00:10:14,792 --> 00:10:16,826
Oh, my God.

263
00:10:16,861 --> 00:10:19,562
Your inbox is literally filled
with penises.

264
00:10:19,597 --> 00:10:20,947
- Mm-hmm.
- I am so sorry.

265
00:10:22,199 --> 00:10:25,502
Oh, look.
Ed Miller from payroll.

266
00:10:25,536 --> 00:10:27,954
Before I do this,
is there any possible way that

267
00:10:28,005 --> 00:10:29,305
we could still keep dating?

268
00:10:29,340 --> 00:10:31,841
We've attacked this
from all angles.

269
00:10:31,876 --> 00:10:33,610
Either you don't run
for office--

270
00:10:33,644 --> 00:10:35,111
Which is out of the question.

271
00:10:35,146 --> 00:10:37,247
Or you break up with him
and avoid a potential scandal.

272
00:10:38,649 --> 00:10:40,266
- Do you need to get that?
- No, it's just penises.

273
00:10:40,301 --> 00:10:42,468
Leslie, I don't want you
to break up with him either.

274
00:10:42,520 --> 00:10:44,654
- There's just no other way.
- Yeah.

275
00:10:44,688 --> 00:10:46,589
Just be direct and honest.

276
00:10:46,607 --> 00:10:47,891
- Okay.
- Okay.

277
00:10:47,925 --> 00:10:49,292
Hmm.

278
00:10:49,326 --> 00:10:51,778
It's the entire Sanitation
Department for Muncie.

279
00:10:51,812 --> 00:10:52,829
Ooh.

280
00:10:54,498 --> 00:10:55,965
Would you like any wine
to start?

281
00:10:56,000 --> 00:10:58,868
Yes, and I'm gonna be direct
and honest with you.

282
00:10:58,903 --> 00:11:00,336
I would like a glass of red wine

283
00:11:00,371 --> 00:11:01,538
and I'll take
the cheapest one you have

284
00:11:01,572 --> 00:11:02,705
because I can't tell
the difference.

285
00:11:02,740 --> 00:11:04,657
- I'll have the same.
- Great.

286
00:11:04,708 --> 00:11:07,844
- Okay. Here's the deal.
- Okay.

287
00:11:07,878 --> 00:11:10,780
- I really, really like you.
- I like you too.

288
00:11:10,814 --> 00:11:12,582
And I think I know
what you're gonna say.

289
00:11:12,616 --> 00:11:15,785
- I highly doubt that.
- I think I do.

290
00:11:15,819 --> 00:11:18,054
- Oh, I don't.
- And, um...

291
00:11:18,088 --> 00:11:21,341
- I got you something.
- No. What is this?

292
00:11:21,392 --> 00:11:23,026
- Is this jewelry?
- It's not, just open it.

293
00:11:23,060 --> 00:11:26,128
No.
Oh, I need to go to...

294
00:11:26,146 --> 00:11:27,430
Another place.

295
00:11:27,464 --> 00:11:29,849
I'm gonna go to the bathroom--
the Whiz Palace,

296
00:11:29,900 --> 00:11:31,184
as we like to call it here--

297
00:11:31,235 --> 00:11:34,404
and I'm going to be back
and I'm not gonna split.

298
00:11:34,438 --> 00:11:37,264
I'm here for you because
we need to talk about stuff.

299
00:11:37,384 --> 00:11:40,142
- Okay?
- All right.

300
00:11:40,160 --> 00:11:41,911
Wait--

301
00:11:48,252 --> 00:11:51,245
- Ron.
- What are you doing here?

302
00:11:51,365 --> 00:11:53,708
Running away from my problems.

303
00:11:53,828 --> 00:11:55,611
Come on in.

304
00:12:02,092 --> 00:12:03,987
- Ok, so here's what's going on.
- I didn't ask.

305
00:12:04,107 --> 00:12:05,464
Friday, I'm supposed
to announce that I'm running

306
00:12:05,704 --> 00:12:07,238
for City Council,
and I haven't told Ben yet.

307
00:12:07,272 --> 00:12:09,273
I know I said that we broke up,
but we couldn't.

308
00:12:09,308 --> 00:12:10,508
We're still dating.

309
00:12:10,542 --> 00:12:11,776
But now I have to break up
with him for real,

310
00:12:11,810 --> 00:12:12,994
but I don't wanna break up
with him

311
00:12:13,045 --> 00:12:14,412
because he's so cute.
He's so nice.

312
00:12:14,446 --> 00:12:15,880
If you're gonna stay here,
there are three rules

313
00:12:15,914 --> 00:12:17,715
you need to follow.

314
00:12:17,749 --> 00:12:21,485
One, no talk about Tammy one.
Two, no talk about Ben.

315
00:12:21,520 --> 00:12:23,321
Three, no talk.

316
00:12:23,355 --> 00:12:24,722
I didn't even ask you
last night.

317
00:12:24,756 --> 00:12:26,123
What is going on
with Tammy one?

318
00:12:26,158 --> 00:12:28,559
You just violated rules
number one and three.

319
00:12:28,593 --> 00:12:30,127
You lose your coffee
privileges.

320
00:12:30,162 --> 00:12:32,146
Oh.

321
00:12:32,180 --> 00:12:37,134
Oh, you choose, Knope--
hunting, fishing, or drinking?

322
00:12:37,169 --> 00:12:39,503
I'd really love to shoot
a gun right now.

323
00:12:39,538 --> 00:12:40,905
Fishing it is.

324
00:12:42,407 --> 00:12:44,992
And so effective immediately,
all male employees

325
00:12:45,027 --> 00:12:48,362
must stop sending explicit
photos to Ms. Perkins.

326
00:12:48,413 --> 00:12:51,332
If I may, what if based
on empirical evidence,

327
00:12:51,366 --> 00:12:55,386
I truly believe there's a grave
and immediate medical emergency?

328
00:12:55,420 --> 00:12:59,490
Were that the case,
could I then show you my dong?

329
00:12:59,524 --> 00:13:00,791
No.

330
00:13:00,825 --> 00:13:03,227
- That would be harassment.
- Fair enough.

331
00:13:03,261 --> 00:13:05,162
Now completely different
scenario.

332
00:13:05,197 --> 00:13:07,832
Let's say I've been watching
a lot of women's golf

333
00:13:07,866 --> 00:13:09,741
and I've had some wine--

334
00:13:09,861 --> 00:13:10,935
How about this?

335
00:13:10,969 --> 00:13:14,153
I'm gonna get a male doctor to
come in and do some screenings.

336
00:13:14,273 --> 00:13:14,939
Perfect.

337
00:13:14,973 --> 00:13:17,067
Just as a backup,
I'm sending you some photos.

338
00:13:17,187 --> 00:13:18,901
- That will not be necessary.
- That's me.

339
00:13:20,671 --> 00:13:22,773
Brand-new day,
brand-new merch.

340
00:13:22,807 --> 00:13:25,876
I got you guys some awesome
Entertainment720 stuff.

341
00:13:25,910 --> 00:13:30,079
Donna, this is for you.
Entertainment720 umbrella.

342
00:13:30,098 --> 00:13:34,050
Entertainment720 fly swatters.
Rubik's cube with my face on it.

343
00:13:34,084 --> 00:13:35,302
♪ Breath mints ♪

344
00:13:35,353 --> 00:13:36,853
Fireplace bellows.

345
00:13:36,888 --> 00:13:38,488
And...drum roll please.

346
00:13:39,724 --> 00:13:41,625
Drum roll sound effect
key chain.

347
00:13:41,659 --> 00:13:44,528
Shouldn't you be working
at your new company, Tom?

348
00:13:44,562 --> 00:13:45,812
I am working, April.

349
00:13:45,863 --> 00:13:48,231
I'm making high-level
network contacts.

350
00:13:48,266 --> 00:13:50,283
You think Bethenny Frankel
sits behind a desk all day?

351
00:13:50,318 --> 00:13:53,437
She makes $100 million a year.
How much you make a year?

352
00:13:53,471 --> 00:13:54,638
$101 million.

353
00:13:54,672 --> 00:13:56,506
Well, I, for one,
am happy to see Tom.

354
00:13:56,541 --> 00:13:57,741
Thanks, Jerry!

355
00:13:57,775 --> 00:14:00,293
Here. Have an umbrella
and some breath strips.

356
00:14:00,328 --> 00:14:01,878
Whoa.

357
00:14:01,913 --> 00:14:03,213
All right, enough chitchat,
everyone.

358
00:14:03,247 --> 00:14:05,949
- Please get back to work.
- You're not our boss.

359
00:14:05,967 --> 00:14:08,118
- What'd you just say to me?
- Ma'am.

360
00:14:08,152 --> 00:14:11,788
So...did you get a chance
to think about my offer?

361
00:14:11,806 --> 00:14:13,957
Yeah, I've been thinking a lot
about what you said.

362
00:14:13,991 --> 00:14:16,493
I wanna do my dreams,
but I just don't think my dream

363
00:14:16,527 --> 00:14:18,795
is working for a company that
puts logos on stuff.

364
00:14:18,830 --> 00:14:20,530
That's not my company does.

365
00:14:20,565 --> 00:14:22,399
Although, maybe we'll start.

366
00:14:22,433 --> 00:14:25,035
Hey, what you think?

367
00:14:25,069 --> 00:14:26,353
Donna...that looks great.

368
00:14:26,404 --> 00:14:28,538
I could do without this one.

369
00:14:30,324 --> 00:14:32,475
So running for office.

370
00:14:32,493 --> 00:14:34,945
Now I'm gonna have to find
a replacement for you.

371
00:14:34,979 --> 00:14:37,681
- I might not win.
- You'll win.

372
00:14:37,715 --> 00:14:42,085
- I might not run.
- You should.

373
00:14:42,119 --> 00:14:44,004
What's our plan here, Knope?

374
00:14:44,038 --> 00:14:47,691
I figure we build a fire,
roast the fish we shot,

375
00:14:47,725 --> 00:14:49,392
and make s'mores.

376
00:14:49,427 --> 00:14:51,194
I don't have the material
for s'mores.

377
00:14:51,229 --> 00:14:52,629
I do.

378
00:14:52,663 --> 00:14:54,664
I always carry emergency
s'more rations in my car.

379
00:14:54,699 --> 00:14:57,217
Given your hunting abilities

380
00:14:57,268 --> 00:14:59,219
and my chocolate supply,

381
00:14:59,270 --> 00:15:03,523
I figure we could stay up here
for two...three years.

382
00:15:09,313 --> 00:15:10,847
You know, when I was 12,

383
00:15:10,882 --> 00:15:14,017
my brother shot me in
the pinky toe with a nail gun.

384
00:15:14,051 --> 00:15:16,686
Granted, it was
a hilarious prank,

385
00:15:16,721 --> 00:15:18,187
and we all had a good laugh.

386
00:15:18,206 --> 00:15:19,355
That's awful.

387
00:15:19,373 --> 00:15:21,190
But I avoided going
to the doctor.

388
00:15:21,209 --> 00:15:23,326
I hate paperwork.

389
00:15:23,361 --> 00:15:26,296
After a few weeks,
the toe just kinda fell off.

390
00:15:26,330 --> 00:15:27,797
You only have nine toes?

391
00:15:27,832 --> 00:15:30,500
I have the toes I have.

392
00:15:30,535 --> 00:15:32,102
Let's just leave it at that.

393
00:15:32,136 --> 00:15:35,639
The point is the doctor said
if I had come in right away,

394
00:15:35,673 --> 00:15:38,241
they might've saved the toe.

395
00:15:38,276 --> 00:15:40,544
You can't run away
from your problems.

396
00:15:40,578 --> 00:15:42,779
Especially if you only have
nine toes.

397
00:15:44,098 --> 00:15:48,151
Sorry.
That was uncalled for.

398
00:15:48,185 --> 00:15:51,655
I'm Ron Swanson.
And you're Leslie [bleep] Knope.

399
00:15:54,325 --> 00:15:57,160
You with me?

400
00:16:01,499 --> 00:16:03,400
Well, what is your dream?

401
00:16:03,434 --> 00:16:05,702
It's to be the biggest rock
star on the planet obviously.

402
00:16:05,736 --> 00:16:07,404
But that could take another
3-5 years.

403
00:16:07,438 --> 00:16:08,672
Don't get me wrong.

404
00:16:08,706 --> 00:16:11,975
Shoe shining has been a...
been a pretty wild ride.

405
00:16:12,009 --> 00:16:15,478
But is it possible there's
something more out there for me?

406
00:16:15,513 --> 00:16:18,315
Andy, there's tons of stuff
you can do.

407
00:16:18,349 --> 00:16:20,100
I'll help you figure it out.

408
00:16:20,134 --> 00:16:21,968
- Really?
- Yes.

409
00:16:22,019 --> 00:16:25,722
One year from now, you,
Andy Dwyer, will no longer be

410
00:16:25,756 --> 00:16:29,159
a shoe shinist.

411
00:16:29,193 --> 00:16:30,760
Kyle, beat it.

412
00:16:30,795 --> 00:16:32,963
I wanna make out
with my wife now.

413
00:16:32,997 --> 00:16:34,831
But you've only done half
of one of my shoes.

414
00:16:34,865 --> 00:16:39,819
Here.
Now I haven't done anything.

415
00:16:39,870 --> 00:16:42,939
Scram, kid.

416
00:16:45,209 --> 00:16:47,243
- Hello.
- Hey, Ron! Welcome--

417
00:16:47,277 --> 00:16:50,113
Stop.
First order of business.

418
00:16:50,147 --> 00:16:52,549
I promised Leslie
I'd hire her a new assistant

419
00:16:52,583 --> 00:16:55,451
so the department doesn't
completely shut down

420
00:16:55,469 --> 00:16:57,804
while she runs for office.

421
00:16:57,838 --> 00:17:00,624
- You're running for office?
- Yeah, City Council.

422
00:17:00,658 --> 00:17:02,626
- Leslie, that is great.
- Thank you.

423
00:17:02,660 --> 00:17:06,830
Will you pledge right now
not to raise taxes?

424
00:17:06,864 --> 00:17:10,967
- I think that's premature.
- No pledge, no vote.

425
00:17:10,985 --> 00:17:12,986
The point is
she needs an assistant.

426
00:17:13,020 --> 00:17:15,672
- Start asking around--
- Hire Andy.

427
00:17:15,706 --> 00:17:19,809
- Andy can do it.
- Sure, yeah.

428
00:17:19,844 --> 00:17:20,977
Done.

429
00:17:20,995 --> 00:17:23,980
Andy, you are now Leslie's
assistant.

430
00:17:23,998 --> 00:17:25,782
Honey.

431
00:17:25,816 --> 00:17:28,451
No--

432
00:17:28,486 --> 00:17:30,754
Leslie, I'm gonna work
my ass off for you.

433
00:17:30,788 --> 00:17:33,056
I'll do anything you ask me.
I will prove myself.

434
00:17:33,090 --> 00:17:34,357
You don't even have to pay me.

435
00:17:34,392 --> 00:17:35,792
No, honey, no.

436
00:17:35,826 --> 00:17:39,346
Tomorrow, there will be
a 300-page orientation booklet

437
00:17:39,380 --> 00:17:41,197
as well as a dozen
homemade cookies

438
00:17:41,232 --> 00:17:42,766
in the shape of your face.

439
00:17:42,800 --> 00:17:45,568
But today...

440
00:17:45,603 --> 00:17:49,739
There's something else
I have to do.

441
00:17:49,774 --> 00:17:54,744
If any of you need
anything at all, too bad.

442
00:17:54,779 --> 00:17:57,313
Deal with your problems
yourselves like adults.

443
00:17:57,347 --> 00:17:59,682
I'll be in my office,
waiting for Tammy one.

444
00:17:59,700 --> 00:18:00,984
You want me to find her?

445
00:18:01,018 --> 00:18:02,986
There's no need.
She'll find me.

446
00:18:03,020 --> 00:18:08,458
She has the tracking ability
and body odor of a bloodhound.

447
00:18:10,745 --> 00:18:13,129
Okay, so now you're sure that
everything is okay, you know,

448
00:18:13,164 --> 00:18:14,330
down there?

449
00:18:14,364 --> 00:18:17,167
You're perfectly healthy.

450
00:18:17,201 --> 00:18:20,804
That man has the largest penis
I have ever seen.

451
00:18:20,838 --> 00:18:22,472
I actually don't even know
if he has mumps.

452
00:18:22,506 --> 00:18:23,840
Forgot to look.

453
00:18:23,874 --> 00:18:29,429
I was distracted by the largest
penis I have ever seen.

454
00:18:32,483 --> 00:18:34,184
I know I've been acting
really weird lately,

455
00:18:34,218 --> 00:18:37,087
and, um, I really like you.

456
00:18:37,121 --> 00:18:38,822
What I'm about to say
is gonna contradict the idea

457
00:18:38,856 --> 00:18:40,423
that I really like you,
but that won't change--

458
00:18:40,458 --> 00:18:41,558
So just open the box.

459
00:18:41,592 --> 00:18:43,293
Please stop bringing out
the box, okay?

460
00:18:43,327 --> 00:18:44,728
- Leslie--
- You're being really nice,

461
00:18:44,762 --> 00:18:46,830
and what I'm about to say
is gonna make you hate me.

462
00:18:46,864 --> 00:18:48,998
Okay, then I'll just open
the box for you.

463
00:18:52,369 --> 00:18:54,938
Wow.

464
00:18:54,972 --> 00:18:56,740
You knew?

465
00:18:56,774 --> 00:18:58,792
I figured it out a while ago.

466
00:18:58,843 --> 00:19:01,044
I'm sorry.
I should've told you I knew.

467
00:19:01,078 --> 00:19:05,415
But I just--I wanted this
to last as long as possible.

468
00:19:05,449 --> 00:19:07,217
We have to break up.

469
00:19:07,251 --> 00:19:10,887
Why?
Why do we have to break up?

470
00:19:10,921 --> 00:19:14,224
Well, Leslie, everything
you've accomplished,

471
00:19:14,258 --> 00:19:16,226
you have earned
and you have worked for.

472
00:19:16,260 --> 00:19:18,928
I don't want anyone to think
that you got where you are today

473
00:19:18,963 --> 00:19:22,732
by sleeping with your boss.

474
00:19:22,766 --> 00:19:24,067
But I really like sleeping
with my boss.

475
00:19:24,101 --> 00:19:26,369
Yeah...Yeah.

476
00:19:26,403 --> 00:19:28,705
Okay, look. I'm gonna make this
real easy for you.

477
00:19:28,739 --> 00:19:34,010
Um, it's not you, it's me.
I'm not ready for a commitment.

478
00:19:34,044 --> 00:19:35,979
I just don't like you anymore.

479
00:19:36,013 --> 00:19:38,748
I know it's hard to hear that,
but...

480
00:19:38,783 --> 00:19:40,917
You're boring, and frankly
you disgust me.

481
00:19:40,951 --> 00:19:42,635
Echh!

482
00:19:45,055 --> 00:19:46,790
How did you figure it out?

483
00:19:46,824 --> 00:19:49,826
Leslie, there was a dude
in the ladies' yacht club.

484
00:19:49,860 --> 00:19:51,828
Yeah, but I covered that
pretty well.

485
00:19:51,862 --> 00:19:55,482
Also, you've been making
campaign speeches in your sleep.

486
00:19:55,516 --> 00:19:59,369
Granted, you always do that,
but they got really specific

487
00:19:59,403 --> 00:20:01,204
and moving.

488
00:20:01,238 --> 00:20:02,772
Did I have a good opening line?

489
00:20:02,807 --> 00:20:07,110
It was simple, but I liked it.

490
00:20:07,144 --> 00:20:10,914
Friends, honored guests,
Pawneeans.

491
00:20:10,948 --> 00:20:15,001
I am Leslie Knope, and
I am running for City Council.

492
00:20:17,654 --> 00:20:27,798
<font color="#ec14bd">Sync & corrected by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

493
00:20:28,843 --> 00:20:32,446
- Hello, Tammy.
- Ronald.

494
00:20:32,480 --> 00:20:34,915
That's enough small talk.
What do you want?

495
00:20:34,949 --> 00:20:37,539
You remember what I do
for a living, I trust?

496
00:20:37,889 --> 00:20:40,621
Yes.
You ruin people's lives.

497
00:20:42,703 --> 00:20:45,592
You're being audited, Ronald.

498
00:20:45,627 --> 00:20:46,727
I don't care.

499
00:20:46,761 --> 00:20:49,215
Then why is your mustache
trembling?

500
00:20:49,484 --> 00:20:51,498
I'm here as a friend.

501
00:20:51,532 --> 00:20:52,900
Call it nostalgia.

502
00:20:52,934 --> 00:20:55,669
Or perhaps guilt
for all the times

503
00:20:55,703 --> 00:20:57,671
I tried to smother you
in your sleep.

504
00:20:57,705 --> 00:21:00,925
- I don't need your help.
- Wrong. You do.

505
00:21:01,159 --> 00:21:03,832
But as you're so fond of saying,
it's a free country.

506
00:21:04,088 --> 00:21:05,357
Good luck.

507
00:21:05,812 --> 00:21:08,048
I hope you don't go to jail.

508
00:21:10,607 --> 00:21:13,421
Sit up straight.
You're not doing your breast any favors.

509
00:21:14,811 --> 00:21:16,663
Thank you.

510
00:21:17,732 --> 00:21:20,903
Hi. Your breasts are amazing.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
