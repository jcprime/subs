1
00:00:00,931 --> 00:00:04,065
Miss Leslie Knope,
I present to you Herb Scaifer.

2
00:00:04,083 --> 00:00:05,783
Again, Andy,
you don't need to bow.

3
00:00:05,834 --> 00:00:06,868
As you wish, ma'am.

4
00:00:06,902 --> 00:00:08,336
Okay, Herb,
what can I do you for?

5
00:00:08,371 --> 00:00:11,756
Well, please prepare yourself.
I have terrible news.

6
00:00:11,790 --> 00:00:12,641
You do?

7
00:00:12,675 --> 00:00:15,210
The world is going
to end tomorrow at dawn.

8
00:00:15,244 --> 00:00:17,795
Aw nuts.
You sure it's tomorrow?

9
00:00:17,846 --> 00:00:20,715
Afraid so.
Right at dawn.

10
00:00:20,749 --> 00:00:24,519
Zorp is going to be here
to free you from your flesh.

11
00:00:25,553 --> 00:00:27,354
For a while in the 1970s,

12
00:00:27,355 --> 00:00:31,254
our town was run by a freaky cult.
And every few years

13
00:00:31,255 --> 00:00:33,754
the remaining members predicted
the world's gonna end.

14
00:00:33,755 --> 00:00:35,723
And they have an all-night
vigil in the park.

15
00:00:36,257 --> 00:00:37,858
It's super annoying.

16
00:00:37,893 --> 00:00:39,894
Turns out when you think
the world's ending,

17
00:00:39,928 --> 00:00:43,097
you don't aim so carefully
in the port-a-potties.

18
00:00:46,134 --> 00:00:47,734
♪ ♪

19
00:00:48,134 --> 00:00:57,734
<font color="#ff8c00">Sync & corrections by Alice</font>
<font color="#ff8c00">www.addic7ed.com</font>

20
00:01:03,268 --> 00:01:06,420
Why does the cult call
themselves "The Reasonableists"?

21
00:01:06,455 --> 00:01:08,756
Well, they figure if
people criticize them

22
00:01:08,790 --> 00:01:09,857
it'll seem like they're

23
00:01:09,891 --> 00:01:11,192
attacking something
very reasonable.

24
00:01:11,226 --> 00:01:12,793
That's weirdly brilliant.

25
00:01:12,828 --> 00:01:14,228
Look, there's nothing
to worry about.

26
00:01:14,262 --> 00:01:16,297
They've said that the world
is going to end 15 times.

27
00:01:16,331 --> 00:01:17,648
And the only bad thing
that's ever happened

28
00:01:17,699 --> 00:01:19,233
on any of those dates is

29
00:01:19,267 --> 00:01:22,136
Lance Armstrong
dumping Sheryl Crow.

30
00:01:22,170 --> 00:01:23,955
- That was a tragic day.
- Hmm.

31
00:01:23,989 --> 00:01:25,840
Live strong.

32
00:01:25,874 --> 00:01:29,126
In any rate, I think Ben and I
should accompany you tonight.

33
00:01:29,160 --> 00:01:30,745
Oh, I don't think
we have to do that.

34
00:01:30,779 --> 00:01:32,279
No, no, no, I insist.

35
00:01:32,297 --> 00:01:35,149
These people
live on planet nutbrain.

36
00:01:35,183 --> 00:01:37,885
I live on planet nut bran.

37
00:01:37,919 --> 00:01:39,670
Bran and nuts are very
helpful for your colon!

38
00:01:39,721 --> 00:01:40,671
Well, it looks like
we're kind of forced

39
00:01:40,722 --> 00:01:42,289
to hang out with each other.

40
00:01:42,307 --> 00:01:44,175
Yeah, listen,
I'll--I'll come for a bit,

41
00:01:44,226 --> 00:01:46,494
but if it's okay with you,
I'm not going to stay.

42
00:01:46,528 --> 00:01:50,765
It's just, you know,
still kind of weird, right?

43
00:01:50,799 --> 00:01:52,600
Yeah, yeah.
Yeah, yeah, yeah, yeah.

44
00:01:52,634 --> 00:01:54,435
Yeah, yeah, yeah,
mm-hmm, totally get it.

45
00:01:54,469 --> 00:01:55,969
Definitely get it.

46
00:01:55,988 --> 00:01:57,488
Ben and I don't
hang out much these days.

47
00:01:57,522 --> 00:01:59,540
Big deal,
lots of people don't hang out.

48
00:01:59,574 --> 00:02:03,811
Jerry and April.
Obama and Madonna, probably.

49
00:02:03,845 --> 00:02:05,162
We're in good company.

50
00:02:05,697 --> 00:02:08,083
Hello!

51
00:02:08,617 --> 00:02:11,052
So it looks like we'll each
end up with about $5,000 apiece.

52
00:02:11,086 --> 00:02:12,987
Stop.

53
00:02:13,021 --> 00:02:14,705
We get five g's each?
That's amazing!

54
00:02:14,756 --> 00:02:17,925
You started out with 450,000.

55
00:02:17,959 --> 00:02:19,527
Entertainment720 is dead.

56
00:02:19,561 --> 00:02:21,062
It's up in company heaven.

57
00:02:21,096 --> 00:02:24,799
Along with pets.com,
Blockbuster and askjeeves.

58
00:02:24,833 --> 00:02:26,967
My company is no better
than a company where you ask

59
00:02:27,002 --> 00:02:30,071
a fake butler
to Google things for you.

60
00:02:30,105 --> 00:02:31,939
So what's next,
Tommy Davidson?

61
00:02:31,973 --> 00:02:33,774
I say we invest our ten large

62
00:02:33,809 --> 00:02:37,010
and then I "accidentally"
get run over by a city bus

63
00:02:37,029 --> 00:02:38,512
and we start our own
hip-hop label.

64
00:02:39,247 --> 00:02:40,881
We have this place
for one more night, right?

65
00:02:40,916 --> 00:02:42,616
We're outtie tomorrow
at noon.

66
00:02:42,651 --> 00:02:45,686
What if we took
every dime we had left,

67
00:02:45,721 --> 00:02:47,288
threw one last party.

68
00:02:47,322 --> 00:02:48,456
Made it the essence

69
00:02:48,490 --> 00:02:50,291
of everything we wanted
the company to be.

70
00:02:50,325 --> 00:02:52,626
A party...

71
00:02:52,661 --> 00:02:54,211
For the end of the world.

72
00:02:54,246 --> 00:02:57,364
Shh! You had me at
"every dime we have left."

73
00:02:57,399 --> 00:02:59,934
Because I'm in
like Lara Flynn...

74
00:02:59,968 --> 00:03:02,386
Boyle... from <i>The Practice.</i>

75
00:03:02,421 --> 00:03:06,974
Dilly dilly dilly
dilly dilly dilly dilly swag!

76
00:03:08,310 --> 00:03:09,561
What should we do tonight?

77
00:03:09,595 --> 00:03:11,429
I don't know, I just figure
we'll just order some pizza.

78
00:03:11,480 --> 00:03:12,813
You watch me play Xbox,

79
00:03:12,848 --> 00:03:14,115
and then I could watch you
make some prank phone calls?

80
00:03:14,149 --> 00:03:15,549
We get hammered?
Make out?

81
00:03:15,584 --> 00:03:17,418
We do that every night.

82
00:03:17,452 --> 00:03:20,621
That's because repetition
is the key to a good marriage.

83
00:03:21,355 --> 00:03:23,091
Let's do something weird.

84
00:03:23,125 --> 00:03:24,909
Come on, it could be
the last night on earth.

85
00:03:24,943 --> 00:03:26,894
We could do something
off my bucket list.

86
00:03:26,928 --> 00:03:28,295
You have a bucket list?

87
00:03:28,330 --> 00:03:30,898
Catch the winning touchdown
at the super bowl.

88
00:03:30,932 --> 00:03:33,400
Make the most amazing
grilled-cheese sandwich ever.

89
00:03:33,435 --> 00:03:35,119
Win the lottery.
Ride a unicycle.

90
00:03:35,170 --> 00:03:36,001
Invent something.

91
00:03:36,005 --> 00:03:38,472
I'd like to remake the movie
<i>Shazaam</i> with Shaquille O'Neal

92
00:03:38,507 --> 00:03:40,241
where he plays a genie
and I'd like to get it right.

93
00:03:40,275 --> 00:03:41,909
Go skydiving.
Outrun a hippo.

94
00:03:41,943 --> 00:03:43,611
Fly first class on plane.

95
00:03:43,645 --> 00:03:47,548
And when people are walking by,
be like this.

96
00:03:49,918 --> 00:03:51,519
All right, well, this
is what we're doing tonight.

97
00:03:51,553 --> 00:03:53,637
- Wait, which one?
- This one.

98
00:03:53,688 --> 00:03:56,256
800, 900, 1,000.

99
00:03:56,274 --> 00:03:58,159
And how much
is left in the bank account?

100
00:03:58,193 --> 00:03:59,760
18 dollars
and four cents.

101
00:03:59,795 --> 00:04:01,762
Whoa!
Still a lot left over.

102
00:04:01,797 --> 00:04:06,099
Okay, you wanted to hold
$1,000 cash in your hands,

103
00:04:09,805 --> 00:04:13,273
that's super disappointing.

104
00:04:13,291 --> 00:04:16,961
998, 999, 1000.

105
00:04:16,995 --> 00:04:18,412
Yes!

106
00:04:18,446 --> 00:04:21,165
Now this is what I imagined!

107
00:04:23,552 --> 00:04:26,754
Have you ever seen this much
cash in your entire life?

108
00:04:26,788 --> 00:04:28,305
I just handed it to you.

109
00:04:28,340 --> 00:04:30,624
Nickels!

110
00:04:30,642 --> 00:04:31,625
I want nickels.

111
00:04:31,660 --> 00:04:33,594
- A billion nickels!
- No, Andy.

112
00:04:33,628 --> 00:04:34,962
Evening, gentlemen.

113
00:04:34,996 --> 00:04:37,598
- Hey, Ron!
- Hail Zorp.

114
00:04:37,632 --> 00:04:39,650
It is a beautiful night
for the end of the world.

115
00:04:39,684 --> 00:04:43,103
Congratulations to all of you
for reaching the finish line.

116
00:04:43,138 --> 00:04:45,406
The Zorpies are ridiculous.

117
00:04:45,440 --> 00:04:47,491
But, like the founding fathers,

118
00:04:47,526 --> 00:04:50,277
I believe in absolute
freedom of religion.

119
00:04:50,312 --> 00:04:54,014
Also their ceremonies require
the playing of flutes.

120
00:04:55,049 --> 00:04:58,152
I happen to make flutes
in my wood shop.

121
00:04:58,186 --> 00:05:00,554
Flutes are $80 apiece.

122
00:05:00,589 --> 00:05:02,456
And recorders are 150.

123
00:05:02,490 --> 00:05:05,359
Hey, they're beautiful.

124
00:05:05,393 --> 00:05:06,660
Would you take a check?

125
00:05:06,678 --> 00:05:09,346
- You can cash it tomorrow.
- Sure.

126
00:05:12,551 --> 00:05:14,185
Welcome to E720's

127
00:05:14,219 --> 00:05:16,103
end of the world celebration.

128
00:05:16,137 --> 00:05:18,439
The entire party's a VIP area.

129
00:05:18,473 --> 00:05:20,057
There's also
a double VIP area.

130
00:05:20,108 --> 00:05:21,859
A triple VIP area.

131
00:05:21,893 --> 00:05:23,944
And the Centurion Club
elite VIP area.

132
00:05:23,979 --> 00:05:25,279
Sponsored by Sobe Lifewater.

133
00:05:25,313 --> 00:05:27,248
No one's allowed in there,
not even us.

134
00:05:27,282 --> 00:05:28,266
Uh- uh.

135
00:05:28,267 --> 00:05:30,751
We also hired ten huge, scary
bouncers with earpieces.

136
00:05:30,785 --> 00:05:32,519
- What up, Keith?
- Mmm.

137
00:05:32,537 --> 00:05:34,205
He actually once tossed me
out of a club three years ago.

138
00:05:34,239 --> 00:05:35,539
Water under the bridge.

139
00:05:35,574 --> 00:05:37,992
DJ Bluntz is mixing up
some new beats.

140
00:05:38,026 --> 00:05:40,211
Ballers and ballerettes,
fasten your seatbelts.

141
00:05:40,245 --> 00:05:41,829
The perfect party begins...

142
00:05:41,863 --> 00:05:44,298
Clink! Now.

143
00:05:46,301 --> 00:05:47,968
Thank God.

144
00:05:48,003 --> 00:05:50,504
No one shows up
to a good party on time.

145
00:05:50,538 --> 00:05:52,006
If anybody actually
showed up right now,

146
00:05:52,040 --> 00:05:53,140
the whole party
would have been ruined.

147
00:05:53,174 --> 00:05:54,225
It would have been
a disaster.

148
00:05:54,259 --> 00:05:55,709
I don't want
to go to that party.

149
00:05:55,744 --> 00:05:58,646
Does the city consider them
a threat to public safety?

150
00:05:58,680 --> 00:06:01,565
Oh, not at all,
Shauna Malwae-Tweep.

151
00:06:01,600 --> 00:06:03,484
So I shouldn't interpret
anything from the fact

152
00:06:03,518 --> 00:06:05,519
that there are two people from
the city manager's office here?

153
00:06:05,553 --> 00:06:08,221
Well, nothing except that
I had nothing better to do.

154
00:06:08,240 --> 00:06:09,823
Which probably says
more about me than them.

155
00:06:09,858 --> 00:06:11,725
Thanks.

156
00:06:11,760 --> 00:06:13,727
- Thanks.
- Headline idea.

157
00:06:13,762 --> 00:06:15,462
"It's the end of the world
as they know it,

158
00:06:15,497 --> 00:06:17,064
but Pawnee feels fine."

159
00:06:17,082 --> 00:06:19,833
- It's a little long.
- Okay, "Zorp Shmorp!

160
00:06:19,868 --> 00:06:21,535
"Doomsday prediction
falls flat

161
00:06:21,569 --> 00:06:23,337
"as citizens spend
pleasant evening

162
00:06:23,371 --> 00:06:25,339
enjoying one of Pawnee's
finest parks."

163
00:06:25,373 --> 00:06:26,707
Somehow longer.

164
00:06:26,741 --> 00:06:28,008
Right, okay let's
go with the first one.

165
00:06:28,043 --> 00:06:30,261
Great, um, hey, also...

166
00:06:30,295 --> 00:06:32,245
He's cute.
Is he single, do you know?

167
00:06:32,264 --> 00:06:33,580
Chris? No.

168
00:06:33,598 --> 00:06:35,215
He's actually
dating Jerry's daughter.

169
00:06:35,249 --> 00:06:37,318
Oh, no sorry, I meant Ben.
What's his story?

170
00:06:37,352 --> 00:06:39,286
- Who?
- Ben?

171
00:06:39,321 --> 00:06:41,388
I-I don't know.

172
00:06:41,422 --> 00:06:44,308
I--I'm not s--
I'm not sure.

173
00:06:44,359 --> 00:06:46,593
He's, um,
a man and he's a worker.

174
00:06:46,611 --> 00:06:50,698
And he is--uh, we've never
discussed sex, so...

175
00:06:50,732 --> 00:06:53,734
It's--we've always just been
very businesslike, so...

176
00:06:53,768 --> 00:06:57,437
Your guess is as good--
nay, better than mine.

177
00:06:57,455 --> 00:06:59,439
The end.
By Leslie Knope.

178
00:07:00,457 --> 00:07:02,710
Okay.
Cool, thanks.

179
00:07:02,744 --> 00:07:04,778
H--happy to help.

180
00:07:04,813 --> 00:07:06,315
What's happening?

181
00:07:07,749 --> 00:07:09,383
The world is ending.

182
00:07:11,549 --> 00:07:13,283
Oh boy, I know how this goes.

183
00:07:13,356 --> 00:07:14,840
I know how Shauna operates.

184
00:07:14,874 --> 00:07:16,508
She smiles,
and then they fall in love,

185
00:07:16,542 --> 00:07:17,859
and then they get married,

186
00:07:17,894 --> 00:07:20,078
and then she changes her name
to Shauna Malwae-Wyatt.

187
00:07:20,112 --> 00:07:21,696
Or he's going
to be really progressive

188
00:07:21,731 --> 00:07:23,715
and change his name to
Ben Wyatt-Malwae-Tweep.

189
00:07:23,749 --> 00:07:26,701
God, I am so annoyed that he
would hypothetically do that.

190
00:07:26,736 --> 00:07:27,986
I'm gonna remind you

191
00:07:28,020 --> 00:07:29,237
that the reason
you're not dating him

192
00:07:29,288 --> 00:07:31,189
is because you decided
to run for city council.

193
00:07:31,207 --> 00:07:33,024
Oh, so just because
I can't go out with him,

194
00:07:33,059 --> 00:07:34,359
someone else can?

195
00:07:34,377 --> 00:07:35,911
- Oh, boy, okay.
- Wow.

196
00:07:35,962 --> 00:07:39,047
Okay, I'm sure
this is nothing.

197
00:07:39,781 --> 00:07:41,583
Okay, well, that was
definitely something.

198
00:07:41,634 --> 00:07:42,968
- Oh. My God.
- I know that move.

199
00:07:43,002 --> 00:07:44,085
They're gonna
have sex in five minutes.

200
00:07:44,136 --> 00:07:45,470
I've gotta stop this.

201
00:07:45,504 --> 00:07:46,838
I've got to keep them
away from each other

202
00:07:46,872 --> 00:07:47,706
for the rest of their lives.

203
00:07:47,724 --> 00:07:50,559
- How are you gonna do that?
- Unclear!

204
00:07:50,593 --> 00:07:54,179
Oh, yeah, ten bucks!

205
00:07:54,213 --> 00:07:55,347
That counts as
winning the lottery.

206
00:07:56,581 --> 00:07:58,984
Okay, now is this
the most amazing

207
00:07:59,018 --> 00:08:01,052
grilled-cheese
sandwich ever made?

208
00:08:01,070 --> 00:08:03,188
Mmm, it's pretty awesome.

209
00:08:03,722 --> 00:08:05,056
Honey, my bucket list
doesn't say

210
00:08:05,091 --> 00:08:07,792
"make a pretty awesome
grilled-cheese sandwich."

211
00:08:07,827 --> 00:08:09,127
Gotta go back to the store.

212
00:08:09,161 --> 00:08:10,629
We're gonna get
a different kind of cheese.

213
00:08:10,663 --> 00:08:11,746
No, no, no, dude, seriously.

214
00:08:11,781 --> 00:08:13,131
This is the most amazing
grilled-cheese sandwich

215
00:08:13,165 --> 00:08:14,199
ever made, okay?

216
00:08:14,233 --> 00:08:16,201
Okay. What's next?

217
00:08:16,235 --> 00:08:19,371
Hey, there you are.
Wow, you two are still talking?

218
00:08:19,405 --> 00:08:20,939
Ben hasn't bored you
to death by now.

219
00:08:20,973 --> 00:08:23,074
- Hey.
- So boring.

220
00:08:23,092 --> 00:08:24,842
Actually, we were having
a really great talk.

221
00:08:25,377 --> 00:08:27,245
- Keep your pants on.
- What?

222
00:08:27,280 --> 00:08:28,847
I mean,
keep your pants on, girl!

223
00:08:28,881 --> 00:08:30,482
I mean, those are
really nice pants.

224
00:08:30,516 --> 00:08:32,083
I really like your pants.
Where'd you get them?

225
00:08:32,118 --> 00:08:33,935
Do you want to go
buy some more pants?

226
00:08:33,970 --> 00:08:35,420
Or, um, walk away from here?

227
00:08:35,438 --> 00:08:37,522
I might head over to
that end of the world party.

228
00:08:37,556 --> 00:08:39,224
Oh, that's where all
the losers are going.

229
00:08:39,258 --> 00:08:41,159
Yeah, what is that?
Tom emailed me.

230
00:08:41,193 --> 00:08:43,728
- I could check that out.
- Hurrrrr...

231
00:08:43,763 --> 00:08:46,431
Well, then you should
stay here then.

232
00:08:46,465 --> 00:08:48,166
Because you can't go.

233
00:08:48,200 --> 00:08:50,101
I mean, what if you're
not here when Zorp shows up?

234
00:08:50,136 --> 00:08:51,269
Boy, would your face be red.

235
00:08:51,304 --> 00:08:52,971
When Zorp shows up,

236
00:08:53,005 --> 00:08:57,575
your faces will be
melted off and used as fuel.

237
00:08:58,210 --> 00:08:59,477
You want to be here
for that.

238
00:08:59,512 --> 00:09:00,812
I think
I'll take my chances.

239
00:09:00,846 --> 00:09:03,465
Leslie, thanks as always.

240
00:09:03,499 --> 00:09:07,452
Wow. What an unbelievably
unpleasant person.

241
00:09:09,555 --> 00:09:10,839
Melissa, get in there

242
00:09:10,890 --> 00:09:12,257
- with your bad self.
- All right.

243
00:09:12,291 --> 00:09:13,892
- You ready?
- I'm ready.

244
00:09:13,926 --> 00:09:15,493
Anything you grab
is yours to keep.

245
00:09:15,528 --> 00:09:16,678
Hit it!

246
00:09:16,729 --> 00:09:18,463
Yes!

247
00:09:18,981 --> 00:09:21,399
The perfect party.
It's an elusive idea.

248
00:09:21,934 --> 00:09:23,351
People have to be
completely entertained

249
00:09:23,402 --> 00:09:26,271
from the moment they walk in,
to the moment they leave.

250
00:09:26,305 --> 00:09:27,906
It's a grand experiment.

251
00:09:27,940 --> 00:09:31,076
And I... am a party scientist.

252
00:09:31,110 --> 00:09:33,078
Welcome to my laboratory.

253
00:09:33,112 --> 00:09:35,163
♪ ♪

254
00:09:38,217 --> 00:09:39,817
♪ ♪

255
00:09:47,727 --> 00:09:50,495
That was <i>Symphony for
the Righteous Destruction</i>

256
00:09:50,529 --> 00:09:52,163
<i>of Humanity</i> in E minor.

257
00:09:52,198 --> 00:09:54,832
By the late Lou Prozotovich.

258
00:09:54,967 --> 00:09:58,003
Reminder. These flutes are
available for purchase.

259
00:09:58,037 --> 00:10:00,205
You can't take it
with you people.

260
00:10:00,239 --> 00:10:02,857
Herb, I just speed-read
both of your books.

261
00:10:02,892 --> 00:10:04,025
- Ooh.
- And, full disclosure,

262
00:10:04,060 --> 00:10:05,810
I think they're
bonkers-filled.

263
00:10:05,845 --> 00:10:07,812
But I did notice that you're
interested in reincarnation.

264
00:10:07,847 --> 00:10:09,347
Tell me about that.

265
00:10:09,382 --> 00:10:11,516
Well, it makes sense,
doesn't it?

266
00:10:11,550 --> 00:10:16,037
From the universe, we emerge.
Into the universe, we return.

267
00:10:16,072 --> 00:10:18,823
And there are infinite
forms we can take

268
00:10:18,857 --> 00:10:21,376
in infinite universes.

269
00:10:21,910 --> 00:10:23,027
What a lovely thought.

270
00:10:23,045 --> 00:10:27,198
Well, this morning at dawn,
you will take a new form.

271
00:10:27,233 --> 00:10:30,602
That of a fleshless,
chattering skeleton

272
00:10:30,636 --> 00:10:34,639
when Zorp the surveyor arrives
and burns your flesh off

273
00:10:34,673 --> 00:10:37,142
with his volcano mouth.

274
00:10:38,176 --> 00:10:39,611
That was very strange
what you just said.

275
00:10:39,645 --> 00:10:41,713
I don't like it as much
as that other thing you said.

276
00:10:41,747 --> 00:10:44,949
Oh.

277
00:10:44,984 --> 00:10:47,235
Hey, hey, where you going?
Where you headed off to?

278
00:10:47,269 --> 00:10:48,937
Do you want to play
a fun game with me

279
00:10:48,971 --> 00:10:50,522
that I made up
called chess-risk?

280
00:10:50,556 --> 00:10:53,558
It's half chess, half risk and
takes like 15 hours to play.

281
00:10:53,576 --> 00:10:55,260
Okay, yeah, I think
I'm gonna go.

282
00:10:55,294 --> 00:10:58,029
- I--okay.
- All right?

283
00:10:58,563 --> 00:11:00,465
- Oh, my God!
- What?

284
00:11:00,499 --> 00:11:02,450
I forgot I have a thing
I need to show you.

285
00:11:02,501 --> 00:11:04,202
Oh.

286
00:11:04,236 --> 00:11:06,004
I need to bring you there
right now it's so amazing.

287
00:11:06,038 --> 00:11:07,505
It's going to freak you out.

288
00:11:07,540 --> 00:11:09,674
It's, uh, it's something
that we need to

289
00:11:09,708 --> 00:11:12,243
get in my car and go to,
so let's do that.

290
00:11:12,261 --> 00:11:13,261
- Okay.
- Come on.

291
00:11:13,295 --> 00:11:14,213
Leslie,
where are you going?

292
00:11:14,214 --> 00:11:16,581
I'm going to the thing
that I told you about with Ben.

293
00:11:16,599 --> 00:11:18,016
No spoilers, Ann.
She's been there before.

294
00:11:18,050 --> 00:11:19,617
- Let's go.
- Okay.

295
00:11:20,970 --> 00:11:22,270
What's up, what's up?

296
00:11:22,304 --> 00:11:24,055
Are you guys having
a good time tonight?

297
00:11:25,758 --> 00:11:27,992
I think we've gotta take things
to the next level.

298
00:11:28,027 --> 00:11:29,127
Drum line, hit it!

299
00:11:32,148 --> 00:11:33,748
♪ ♪

300
00:11:47,779 --> 00:11:49,631
- Okay, I'm ready!
- Good.

301
00:11:49,665 --> 00:11:52,300
Wait!
I'm not ready. Super scared.

302
00:11:52,334 --> 00:11:54,252
Andy, you said you wanted
to be an action star.

303
00:11:54,786 --> 00:11:55,587
This is your chance.

304
00:11:55,621 --> 00:11:58,223
Okay.

305
00:11:58,757 --> 00:12:01,976
Okay, you, Natalie,
follow the action.

306
00:12:02,011 --> 00:12:03,394
What do I have to do?

307
00:12:03,429 --> 00:12:04,562
Just hold up this
butter knife to my throat

308
00:12:04,597 --> 00:12:05,763
and pretend like
you're holding me hostage.

309
00:12:05,797 --> 00:12:07,632
- What?
- Ready!

310
00:12:07,666 --> 00:12:10,151
I don't know.
It seems a little crazy.

311
00:12:10,986 --> 00:12:12,237
Just crazy enough to work.

312
00:12:16,408 --> 00:12:18,243
Aah!

313
00:12:18,277 --> 00:12:22,247
Ahh!
Oh, my face!

314
00:12:22,281 --> 00:12:23,281
My face!

315
00:12:23,315 --> 00:12:26,000
Is fine, Mikhail Petrov!

316
00:12:26,235 --> 00:12:28,453
Forget it, Macklin,
you're too late--

317
00:12:28,487 --> 00:12:30,488
- I'm sorry, guys.
- Jerry!

318
00:12:30,523 --> 00:12:32,023
I need my cheaters.
I cannot see anyth--

319
00:12:32,057 --> 00:12:33,341
you're doing really good,
you're doing really good.

320
00:12:33,375 --> 00:12:34,092
- Really?
- Yeah.

321
00:12:34,127 --> 00:12:36,294
Help me,
Agent Macklin, help me!

322
00:12:36,328 --> 00:12:38,796
He's stolen my jewels, and now
he's going to ravish my body

323
00:12:38,831 --> 00:12:40,715
and he stinks,
he really stinks!

324
00:12:40,766 --> 00:12:43,902
Okay, Janet Snakehole belongs
to mother Russia now.

325
00:12:43,936 --> 00:12:45,169
Oh, that's what you think!

326
00:12:45,187 --> 00:12:47,171
- No.
- I hope you like pain!

327
00:12:47,189 --> 00:12:48,690
- What are you doing?
- Pow!

328
00:12:48,724 --> 00:12:49,858
Say the line.

329
00:12:49,892 --> 00:12:51,943
Looks like this Siberian husky

330
00:12:51,977 --> 00:12:54,445
is going to be Russian...

331
00:12:54,480 --> 00:12:57,148
off to jail.

332
00:12:57,582 --> 00:12:59,851
Hey, know what, can I go?
Gayle is making a roast.

333
00:13:01,385 --> 00:13:01,886
Ugh.

334
00:13:02,421 --> 00:13:05,406
We've already been
down this street.

335
00:13:05,457 --> 00:13:06,857
Just out of curiosity,

336
00:13:06,876 --> 00:13:09,027
where's your favorite
place to go like ever?

337
00:13:09,061 --> 00:13:10,628
Well, I don't know.

338
00:13:10,663 --> 00:13:12,380
Tom's party
sounded kind of fun.

339
00:13:12,414 --> 00:13:13,998
Here we are!
We're at the special place!

340
00:13:15,885 --> 00:13:20,572
Well, here we are.
What do you think?

341
00:13:20,606 --> 00:13:22,907
Are you gonna murder me
and bury me at this gas station?

342
00:13:22,942 --> 00:13:25,310
No, this is
a great gas station.

343
00:13:25,344 --> 00:13:29,414
This gas station
was owned by... Mick Jagger.

344
00:13:29,448 --> 00:13:30,982
Uh-huh.

345
00:13:31,016 --> 00:13:32,233
I came across
some financial records

346
00:13:32,268 --> 00:13:33,735
when I was doing
some research for my book,

347
00:13:33,769 --> 00:13:35,253
and this gas station was owned

348
00:13:35,287 --> 00:13:37,755
by an M. Jagger, which at first
I thought was a woman.

349
00:13:37,790 --> 00:13:40,358
Maybe Meg or something,
but I think

350
00:13:40,392 --> 00:13:42,360
the most likely explanation

351
00:13:42,394 --> 00:13:45,597
is that legendary Rolling Stones
front man Mick Jagger

352
00:13:45,631 --> 00:13:48,900
owned this gas station
from 1951 to 1958.

353
00:13:49,934 --> 00:13:51,469
I know what you're doing,
Leslie.

354
00:13:51,503 --> 00:13:53,538
I'm showing you a part
of Rock and Roll history.

355
00:13:53,572 --> 00:13:56,808
You--you can't do this.
You know we broke up.

356
00:13:57,542 --> 00:14:00,011
And I kind of feel like we
shouldn't...

357
00:14:00,045 --> 00:14:03,181
Hang out together,
just the two of us.

358
00:14:03,215 --> 00:14:05,750
Because every time we do,
it just makes it harder.

359
00:14:05,784 --> 00:14:08,152
You know?

360
00:14:08,187 --> 00:14:09,520
Okay.

361
00:14:11,523 --> 00:14:13,191
Okay.

362
00:14:18,523 --> 00:14:20,051
I think we've done
everything on the list

363
00:14:20,052 --> 00:14:21,252
that we can actually do.

364
00:14:21,303 --> 00:14:22,887
What about this?

365
00:14:22,922 --> 00:14:25,006
Oh, my God,
that would be awesome.

366
00:14:25,440 --> 00:14:26,908
- It's impossible.
- No, it isn't.

367
00:14:27,442 --> 00:14:28,943
We have $1,000
in our bank account,

368
00:14:28,978 --> 00:14:30,178
and we're young
and irresponsible.

369
00:14:31,415 --> 00:14:32,414
I mean, our car
would break down.

370
00:14:32,448 --> 00:14:33,748
There's no way we
could even get there.

371
00:14:33,783 --> 00:14:36,050
So I'll steal my dad's car.

372
00:14:36,068 --> 00:14:39,187
Look, this is a stupid idea,
but right at this exact second,

373
00:14:39,221 --> 00:14:41,523
we have enough momentum
to do something stupid

374
00:14:41,557 --> 00:14:42,924
before we realize
how stupid it is.

375
00:14:42,958 --> 00:14:47,946
You are absolutely right.
No thinking, just stupid.

376
00:14:47,997 --> 00:14:49,564
Yes!

377
00:14:49,582 --> 00:14:51,833
Buddhists feel
that human beings

378
00:14:51,867 --> 00:14:54,536
are unlikely to be
reincarnated as human beings.

379
00:14:54,570 --> 00:14:57,005
So the problem
with reincarnation

380
00:14:57,039 --> 00:14:59,107
is that you could be
reborn as a pretzel.

381
00:14:59,741 --> 00:15:01,376
Or a socket wrench.

382
00:15:02,010 --> 00:15:03,011
Ron, if you could
be reincarnated,

383
00:15:03,045 --> 00:15:04,646
what would you like to be?

384
00:15:04,680 --> 00:15:07,482
Socket wrench sounds
pretty good, actually.

385
00:15:07,516 --> 00:15:09,584
What religion am I?

386
00:15:09,618 --> 00:15:13,388
Well, I'm a practicing none
of your [Bleep] Business.

387
00:15:13,422 --> 00:15:16,391
I think the danger in
believing in reincarnation

388
00:15:16,425 --> 00:15:18,226
is that you spend so much
time trying to figure out

389
00:15:18,260 --> 00:15:19,928
what you're going to be
in the next lifetime

390
00:15:19,962 --> 00:15:22,397
that you forget to enjoy
the one you're in now.

391
00:15:22,931 --> 00:15:26,301
Ann Perkins...
that was beautiful.

392
00:15:28,604 --> 00:15:31,939
Drum lines,
personalized gift bags, tigers.

393
00:15:31,957 --> 00:15:33,575
This is the best
party I've ever been to.

394
00:15:33,609 --> 00:15:34,776
And I'm not just saying that.

395
00:15:34,810 --> 00:15:36,661
For real, T.T.,
think about it, man.

396
00:15:37,212 --> 00:15:40,081
It's pretty great.
But it's not perfect.

397
00:15:40,615 --> 00:15:41,649
Tom.

398
00:15:41,684 --> 00:15:44,285
Hey, Lucy,
what are you doing here?

399
00:15:44,803 --> 00:15:45,953
Jean-Ralphio called me
and told me

400
00:15:45,971 --> 00:15:47,689
that this was
a very special occasion

401
00:15:47,723 --> 00:15:49,190
and I should come down.

402
00:15:49,725 --> 00:15:50,425
It's nice to see you.

403
00:15:50,459 --> 00:15:52,093
Yeah, good to see you too.

404
00:15:52,127 --> 00:15:53,228
Want to go dance?

405
00:15:53,262 --> 00:15:54,896
Yeah, yeah!
Uh, give me a minute.

406
00:15:54,930 --> 00:15:55,931
Okay.

407
00:15:57,465 --> 00:15:59,300
I can't believe
you called her for me.

408
00:15:59,835 --> 00:16:01,236
That's really nice
of you, man, for real.

409
00:16:01,270 --> 00:16:02,537
Thanks.

410
00:16:02,571 --> 00:16:04,305
No, no, no, thank you.

411
00:16:04,323 --> 00:16:07,609
I mean for everything.

412
00:16:12,364 --> 00:16:14,115
I actually forgot
they ever dated.

413
00:16:14,149 --> 00:16:16,951
I was trying to hit that.

414
00:16:16,985 --> 00:16:19,387
Dawn is nigh!

415
00:16:19,421 --> 00:16:22,457
Zorp the surveyor approaches,

416
00:16:22,491 --> 00:16:25,993
and all earthly debts
will be repaid.

417
00:16:26,511 --> 00:16:28,347
Last call for doughnuts.

418
00:16:29,381 --> 00:16:31,399
Can I speak with you
about a personal matter?

419
00:16:32,433 --> 00:16:34,536
Normally, no.

420
00:16:34,570 --> 00:16:36,855
But... given there's
only 20 minutes

421
00:16:36,889 --> 00:16:38,740
until the end
of human existence...

422
00:16:38,774 --> 00:16:40,341
Also, no.

423
00:16:40,376 --> 00:16:41,943
I lost my mind tonight.

424
00:16:41,977 --> 00:16:45,847
I tried to screw up
even the potential of Ben

425
00:16:45,881 --> 00:16:47,181
dating someone else.

426
00:16:47,216 --> 00:16:48,950
Did you not hear me
when I said no?

427
00:16:48,984 --> 00:16:51,119
Oh, we broke up
because of me.

428
00:16:51,653 --> 00:16:54,789
But I have to
tell you, Ron...

429
00:16:56,725 --> 00:16:59,193
If the world
was ending tomorrow,

430
00:16:59,928 --> 00:17:02,263
I'd want to be with him.

431
00:17:03,298 --> 00:17:06,134
Well, that's significant.

432
00:17:06,168 --> 00:17:10,138
Problem is, Leslie, the world's
not ending tomorrow.

433
00:17:10,172 --> 00:17:12,574
The sun's going to rise
right over there.

434
00:17:13,108 --> 00:17:15,343
It'll be a regular Friday,

435
00:17:15,377 --> 00:17:18,012
and you'll be in the exact same
position you were in before.

436
00:17:19,047 --> 00:17:20,081
I know.

437
00:17:20,115 --> 00:17:21,616
It's just the thought
of him with someone else

438
00:17:21,650 --> 00:17:22,951
is making me miserable.

439
00:17:23,485 --> 00:17:25,486
If it makes you feel
any better, Leslie,

440
00:17:25,521 --> 00:17:27,739
we'll all be dead
in 20 minutes.

441
00:17:28,773 --> 00:17:31,225
That does make me feel better.
Thank you, Herb.

442
00:17:42,788 --> 00:17:45,440
- We did it!
- Yay!

443
00:17:45,474 --> 00:17:46,958
This car is nice!

444
00:17:47,009 --> 00:17:48,293
How long you think
it'll take us?

445
00:17:48,344 --> 00:17:50,578
Uh, the map says 30 hours
but I drive really fast.

446
00:17:50,613 --> 00:17:51,980
- 30 hours?
- Mm-hmm.

447
00:17:52,014 --> 00:17:53,815
Oh, crap!
I didn't bring any music.

448
00:17:53,849 --> 00:17:55,550
No!

449
00:17:55,584 --> 00:17:57,085
I'll tell you what.

450
00:17:57,119 --> 00:17:58,920
You play whatever you want,

451
00:17:59,454 --> 00:18:01,322
I'm just going to take
a real quick snooze

452
00:18:01,357 --> 00:18:03,391
for like 12 hours
so I'm fresh.

453
00:18:06,428 --> 00:18:10,898
♪ ♪

454
00:18:11,432 --> 00:18:12,600
Thank you guys
so much for coming out.

455
00:18:12,635 --> 00:18:14,268
If you're too drunk to drive,

456
00:18:14,286 --> 00:18:16,621
please take
one of our shuttles home.

457
00:18:16,655 --> 00:18:19,173
Valet service will drop
your car off later today.

458
00:18:19,208 --> 00:18:21,108
If you're one of
our designated drivers,

459
00:18:21,126 --> 00:18:22,744
please take a "thank you" bag.

460
00:18:22,778 --> 00:18:26,664
And enjoy the giant bows
I put on your car.

461
00:18:26,715 --> 00:18:28,683
- Hey!
- Hey.

462
00:18:28,717 --> 00:18:30,051
- It's great seeing you.
- You too.

463
00:18:30,085 --> 00:18:32,503
Are, uh, you sticking around
for a bit, or...

464
00:18:32,554 --> 00:18:34,188
Nah, I'm heading to
Bloomington right now.

465
00:18:34,223 --> 00:18:35,890
- Oh.
- Grad school's fun,

466
00:18:35,924 --> 00:18:38,009
but I do miss this town.

467
00:18:38,560 --> 00:18:40,595
- Yeah.
- What's wrong?

468
00:18:41,629 --> 00:18:42,597
I don't know.

469
00:18:42,631 --> 00:18:44,849
After this is done,
I'm gonna be broke,

470
00:18:44,900 --> 00:18:46,701
and I'm not gonna
have a job.

471
00:18:46,735 --> 00:18:48,152
Well,
if it's any consolation,

472
00:18:48,687 --> 00:18:51,022
this was honestly the best
party I've ever been to.

473
00:18:51,873 --> 00:18:52,657
Hey, it was pretty great.

474
00:18:52,691 --> 00:18:55,777
I-I was really
aiming for something--

475
00:18:58,814 --> 00:19:02,784
I'll see you around.

476
00:19:02,818 --> 00:19:04,919
Whoo.

477
00:19:05,453 --> 00:19:07,789
You saw that.

478
00:19:08,123 --> 00:19:09,223
You saw that too.

479
00:19:13,696 --> 00:19:16,030
Shauna Malwae-Tweep
thinks you're cute.

480
00:19:16,565 --> 00:19:17,715
- What?
- That's why I took you

481
00:19:17,766 --> 00:19:19,166
to Mick Jagger's
abandoned gas station.

482
00:19:19,184 --> 00:19:20,601
Because she thinks you're
cute, and I was afraid

483
00:19:20,636 --> 00:19:21,936
you guys were gonna
make out and have babies,

484
00:19:21,970 --> 00:19:23,838
and I had no right
to do that to you

485
00:19:23,856 --> 00:19:25,857
or your future children.

486
00:19:25,891 --> 00:19:27,809
And it wasn't until just now

487
00:19:27,843 --> 00:19:30,144
that I realized
the romantic part

488
00:19:30,179 --> 00:19:32,914
of our relationship is over.

489
00:19:32,948 --> 00:19:34,782
So... I'm sorry.

490
00:19:34,817 --> 00:19:37,685
And, uh, why don't you tell
Shauna to make herself decent

491
00:19:37,720 --> 00:19:40,254
and I'll apologize
to her as well.

492
00:19:41,089 --> 00:19:45,326
Oh, well,
Shauna's not here.

493
00:19:46,060 --> 00:19:47,628
- Oh.
- See what I did?

494
00:19:47,663 --> 00:19:48,696
- I do.
- I'm very sneaky.

495
00:19:50,114 --> 00:19:51,532
Okay, okay, well....

496
00:19:51,567 --> 00:19:53,167
♪ ♪

497
00:20:25,400 --> 00:20:26,567
It's so much more beautiful

498
00:20:26,602 --> 00:20:28,102
than I could have
ever even imagined.

499
00:20:28,637 --> 00:20:30,238
Yeah.

500
00:20:30,672 --> 00:20:33,458
I'm trying to find a way
to be annoyed by it, but...

501
00:20:34,209 --> 00:20:35,510
Coming up empty.

502
00:20:35,944 --> 00:20:37,595
Thank you so much.

503
00:20:37,629 --> 00:20:40,381
I never would have ever
done this without you.

504
00:20:41,015 --> 00:20:41,983
Thank you.

505
00:20:46,438 --> 00:20:50,391
Where's all the faces?
Like the presidents.

506
00:20:52,184 --> 00:20:59,587
<font color="#ff8c00">Sync & corrections by Alice</font>
<font color="#ff8c00">www.addic7ed.com</font>

507
00:21:00,184 --> 00:21:03,587
All the scholarly work
that I did on these texts?

508
00:21:03,621 --> 00:21:06,423
Indicated that
the world would end.

509
00:21:06,457 --> 00:21:08,525
Oh, gosh, I'm as disappointed
as you are, Herb.

510
00:21:08,559 --> 00:21:11,728
Yeah, but I went home and
began to reevaluate the texts.

511
00:21:12,262 --> 00:21:13,730
You don't say.

512
00:21:13,764 --> 00:21:16,733
And I realized that
I'd made some crucial errors

513
00:21:16,767 --> 00:21:18,602
well, math is hard.

514
00:21:18,636 --> 00:21:21,488
Yes, well, the actual
end of the world is May 19.

515
00:21:21,522 --> 00:21:23,240
Okay, let's see
what we've got.

516
00:21:23,274 --> 00:21:25,742
Um, oh, on the 19th
we can't give you the park.

517
00:21:25,776 --> 00:21:28,695
We have a Spring spectacular
free ice cream giveaway.

518
00:21:28,746 --> 00:21:30,997
Heh! I misspoke.

519
00:21:31,032 --> 00:21:32,516
Yeah, it's May 20?

520
00:21:33,550 --> 00:21:34,518
That is free.

521
00:21:34,552 --> 00:21:35,952
- Ah!
- Okay.

522
00:21:35,986 --> 00:21:37,587
End of World, May 20th.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
