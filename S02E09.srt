1
00:00:00,696 --> 00:00:02,178
Okay, as everybody knows,

2
00:00:02,303 --> 00:00:05,880
<i>The Spirit of Pawnee</i>
was defaced again last night.

3
00:00:06,308 --> 00:00:08,299
- What was it this time?
- Chocolate pudding.

4
00:00:09,245 --> 00:00:10,134
That's new.

5
00:00:10,302 --> 00:00:14,042
The mural that normally resides here
is called <i>The Spirit of Pawnee</i>,

6
00:00:14,167 --> 00:00:15,781
and it's very controversial.

7
00:00:15,906 --> 00:00:17,808
We've had someone throw acid at it,

8
00:00:18,283 --> 00:00:19,352
tomato sauce.

9
00:00:19,520 --> 00:00:20,936
Someone tried to stab it once.

10
00:00:21,647 --> 00:00:23,997
We really need better security here.

11
00:00:24,441 --> 00:00:27,876
We also need...
better, less-offensive history.

12
00:00:28,529 --> 00:00:31,837
So the city council has decided
it should be changed

13
00:00:31,962 --> 00:00:34,575
to something
a little less horrifying.

14
00:00:34,864 --> 00:00:36,893
Now since the murals were made

15
00:00:37,079 --> 00:00:39,771
by government employees
back in the 1930s,

16
00:00:40,180 --> 00:00:42,500
we thought it might be fun
if each department

17
00:00:42,989 --> 00:00:45,169
submitted the design
for a new mural...

18
00:00:45,337 --> 00:00:48,188
And you'll submit your concepts
tomorrow afternoon.

19
00:00:48,463 --> 00:00:50,963
All righty, folks?
Thank you very much.

20
00:00:51,420 --> 00:00:53,886
Leslie, what's your design gonna be?
A tree?

21
00:00:54,054 --> 00:00:55,574
Joe, you work in sewage.

22
00:00:55,699 --> 00:00:58,257
Your department literally
specializes in crap.

23
00:00:58,382 --> 00:01:00,240
- You wanna do this?
- I told you before.

24
00:01:00,365 --> 00:01:01,811
Crap is a slang term,

25
00:01:01,979 --> 00:01:03,562
and I don't like that term.

26
00:01:03,856 --> 00:01:07,233
But at least we don't specialize
in losing... Like you guys.

27
00:01:07,401 --> 00:01:09,050
Sewage, let's roll.

28
00:01:09,552 --> 00:01:10,444
Damn!

29
00:01:10,763 --> 00:01:13,114
How does sewage
always get the hottest interns?

30
00:01:13,282 --> 00:01:15,157
- Feygnasse Team -

31
00:01:15,325 --> 00:01:16,948
.:: La Fabrique ::.

32
00:01:17,073 --> 00:01:19,113
Episode 209
<i>The Camel</i>

33
00:01:21,142 --> 00:01:22,205
mpm

34
00:01:23,578 --> 00:01:25,163
lestat78

35
00:01:43,650 --> 00:01:45,731
Guys, this department has the chance

36
00:01:45,856 --> 00:01:48,859
to design something
that could be in this building forever.

37
00:01:48,984 --> 00:01:50,418
This could be our legacy.

38
00:01:50,543 --> 00:01:53,404
I thought building a park on lot 48
was gonna be our legacy.

39
00:01:53,572 --> 00:01:54,710
You can have two.

40
00:01:54,835 --> 00:01:57,200
Look at Madonna...
Great singer, amazing arms.

41
00:01:57,325 --> 00:02:00,244
Look at O.J. Simpson...
Heisman trophy winner, naked gun.

42
00:02:00,824 --> 00:02:03,042
No offense, Leslie,
but I'm not an artist.

43
00:02:03,373 --> 00:02:05,458
That's not true.
I've seen your fingernails.

44
00:02:05,813 --> 00:02:07,727
I pay someone to do this.

45
00:02:08,037 --> 00:02:08,878
Really?

46
00:02:10,205 --> 00:02:11,737
Shoot.
Anyway...

47
00:02:12,049 --> 00:02:14,299
I'm ordering all of you
to design a mural.

48
00:02:15,177 --> 00:02:17,749
Only Ron can order
the whole department to do something.

49
00:02:17,874 --> 00:02:20,598
- Ron, order them to do this.
- Do whatever Leslie says.

50
00:02:21,401 --> 00:02:23,184


52
00:02:27,107 --> 00:02:29,899
And it needs to be breathtaking
and moving and historical

53
00:02:30,622 --> 00:02:33,695
and better than every department,
and you have one hour.

54
00:02:34,106 --> 00:02:36,858
Designers, make it work.

55
00:02:37,643 --> 00:02:38,643
Tim Gunn.

56
00:02:40,137 --> 00:02:41,994
You, my friend,
are ready to go dancing.

57
00:02:42,735 --> 00:02:43,704
Thank you, sir.

58
00:02:43,872 --> 00:02:45,039
All right, next.

59
00:02:45,268 --> 00:02:46,832
Andrew, looking good.

60
00:02:47,154 --> 00:02:48,558
Business is booming.

61
00:02:49,192 --> 00:02:51,941
Hey, Ron, how about you?
Need a little dog waxin'?

62
00:02:52,066 --> 00:02:54,089
It's 5 bucks,
and you'll cut in front of him.

63
00:02:54,257 --> 00:02:55,424
- What?
- Beat it.

64
00:02:55,740 --> 00:02:58,121
This is Ron Swanson
we're talking about.

65
00:02:58,479 --> 00:03:00,040
I'm impressed with Andy.

66
00:03:00,165 --> 00:03:02,306
Pulling himself up
by his bootstraps.

67
00:03:03,225 --> 00:03:04,558
Reminds me of me.

68
00:03:04,726 --> 00:03:07,843
I got my first job when I was nine.
Worked at a sheet metal factory.

69
00:03:07,968 --> 00:03:10,755
In two weeks,
I was running the floor.

70
00:03:12,478 --> 00:03:15,128
Child labor laws
are ruining this country.

71
00:03:18,282 --> 00:03:21,000
- Do you have a key in your shoe?
- No, I have a bunion

72
00:03:21,125 --> 00:03:22,871
that's practically its own toe.

73
00:03:22,996 --> 00:03:26,790
Normally, the pain howls through
my loafers like a banshee on the moors,

74
00:03:26,957 --> 00:03:29,099
but... for these past three minutes,

75
00:03:29,266 --> 00:03:31,293
it's been reduced to a faint growl.

76
00:03:33,769 --> 00:03:34,964
That was neat.

77
00:03:36,283 --> 00:03:38,275
We are all finished.

78
00:03:41,507 --> 00:03:43,854
That was great.
Thank you, son.

79
00:03:43,979 --> 00:03:44,979
No sweat!

80
00:03:45,851 --> 00:03:46,839
Next!

81
00:03:47,368 --> 00:03:49,937
Shouldn't let your friends cut in line.
It's not good business.

82
00:03:50,105 --> 00:03:52,056
You know what else
isn't good business?

83
00:03:52,712 --> 00:03:54,800
- That guy's my friend.
- Right.

84
00:03:55,558 --> 00:03:56,708
I'm saying...

85
00:03:56,833 --> 00:03:58,618
You shouldn't
let your friends cut in line.

86
00:03:58,743 --> 00:04:00,322
Sorry, kyle.
Here, give me this.

87
00:04:01,413 --> 00:04:02,324
Next!

88
00:04:02,807 --> 00:04:05,077
So what are you looking for exactly?

89
00:04:05,245 --> 00:04:06,564
I don't know, man.

90
00:04:06,786 --> 00:04:09,286
The spirit of Pawnee.
That's all I got.

91
00:04:10,156 --> 00:04:12,360
Here. Just gimme $20 worth of art.

92
00:04:12,485 --> 00:04:15,546
Just something that seems personal
that only I could have done.

93
00:04:15,714 --> 00:04:18,489
- Well, tell me about yourself.
- No. Just paint.

94
00:04:20,087 --> 00:04:22,728
I have no interest in art.
Let me clarify.

95
00:04:22,895 --> 00:04:25,728
I have no interest
in non-nude images.

96
00:04:26,814 --> 00:04:28,747
Dude, what the hell
kind of art is this?

97
00:04:28,872 --> 00:04:30,743
Looks like a lizard
puking up skittles!

98
00:04:30,868 --> 00:04:32,688
I'm an abstract expressionist.

99
00:04:32,856 --> 00:04:36,044
No, you're a con artist,
and I'm a guy that's out 20 bucks.

100
00:04:36,694 --> 00:04:37,628
Whatever.

101
00:04:37,753 --> 00:04:39,153
I'm a terrible artist.

102
00:04:39,500 --> 00:04:41,450
But the Parks department
has done so much for me

103
00:04:41,575 --> 00:04:44,264
that if I can help them out
in any way, I will.

104
00:04:45,420 --> 00:04:46,420
Oh, God.

105
00:04:47,548 --> 00:04:49,960
Maybe I should just give them
all free flu shots.

106
00:04:50,791 --> 00:04:53,293
I know everything about
this town and these murals,

107
00:04:53,418 --> 00:04:56,348
and that's why this is
a dream come true... Literally.

108
00:04:56,713 --> 00:04:59,215
I have had a dream
where I designed a mural.

109
00:04:59,486 --> 00:05:02,817
But then it turned into a nightmare
because the mural started talking,

110
00:05:02,942 --> 00:05:05,677
and it came alive, it was whispering,
and I couldn't hear what it was saying,

111
00:05:05,802 --> 00:05:07,858
so I leaned in close,
and then it ate me.

112
00:05:08,494 --> 00:05:10,894
At one point,
Gina Gershon was there.

113
00:05:11,614 --> 00:05:15,114
My piece is truly
gonna capture the spirit of Pawnee.

114
00:05:20,061 --> 00:05:22,535
Never understood
the term elbow grease.

115
00:05:22,906 --> 00:05:24,835
I guess it's not really grease.

116
00:05:24,960 --> 00:05:26,512
Just hard work.

117
00:05:30,271 --> 00:05:33,428
- Nice shiny shoes you got there.
- I was just...

118
00:05:34,214 --> 00:05:35,497
Oh, no... Shoot.

119
00:05:35,823 --> 00:05:37,878
Actually, it looks like
I scuffed this one.

120
00:05:40,048 --> 00:05:42,289
Do you need your money back?
'Cause I already spent it.

121
00:05:42,414 --> 00:05:43,864
Really?
How did...

122
00:05:44,793 --> 00:05:45,875
Never mind.

123
00:05:46,200 --> 00:05:49,242
I think I'll just take
another full polish.

124
00:05:49,690 --> 00:05:51,390
Can I cut in line again?

125
00:05:51,515 --> 00:05:53,394
I feel right at home as a shoeshine.

126
00:05:53,562 --> 00:05:55,043
I have no idea what I'm doing,

127
00:05:55,168 --> 00:05:58,388
but I know I'm doing it
really, really well.

128
00:05:59,301 --> 00:06:02,028
Okay, let's get goin'
with the spirit of Pawnee.

129
00:06:02,153 --> 00:06:03,958
- Ann, you go first.
- Okay.

130
00:06:08,915 --> 00:06:10,536
Since it's the Parks department,

131
00:06:10,704 --> 00:06:12,969
I thought I would design
a pretty park.

132
00:06:13,278 --> 00:06:16,711
With, uh, dogs playing.
I can't really draw,

133
00:06:16,836 --> 00:06:19,536
so I just cut some stuff
out of a magazine.

134
00:06:20,081 --> 00:06:23,299
That looks like something a death row
convict would make in art therapy.

135
00:06:24,260 --> 00:06:25,301
Dude, I tried.

136
00:06:25,819 --> 00:06:27,069
And you failed.

137
00:06:27,262 --> 00:06:29,189
It's dogs and people
playing in a park.

138
00:06:29,314 --> 00:06:31,372
- It's cute!
- It's okay, sweetheart.

139
00:06:31,497 --> 00:06:33,889
You can't make art
because you are art.

140
00:06:34,102 --> 00:06:36,678
You're beautiful...
But that sucks.

141
00:06:37,814 --> 00:06:39,023
Let's see yours!

142
00:06:39,191 --> 00:06:42,331
Fine. Mine is amazing.
It's gonna blow your mind.

143
00:06:49,131 --> 00:06:51,160
Some professional-ass
art right here.

144
00:06:51,341 --> 00:06:54,626
- I'm not sure what I'm looking at.
- It's abstract, Leslie.

145
00:06:55,210 --> 00:06:57,260
Over here, you got some shapes.

146
00:06:57,459 --> 00:06:59,793
And then you come
over to this side...

147
00:07:00,951 --> 00:07:03,701
You know, it's actually
kind of interesting.

148
00:07:04,008 --> 00:07:06,058
Each shape is its own thing...

149
00:07:06,731 --> 00:07:08,302
But then when it comes together,

150
00:07:09,687 --> 00:07:11,305
it really gives you a sense of...

151
00:07:13,743 --> 00:07:14,860
completion.

152
00:07:17,299 --> 00:07:20,599
A piece of art caused me
to have an emotional reaction.

153
00:07:20,794 --> 00:07:22,044
Is that normal?

154
00:07:22,901 --> 00:07:24,727
So it's the last supper

155
00:07:24,852 --> 00:07:26,946
but with famous people from Indiana.

156
00:07:28,323 --> 00:07:30,712
John Mellancamp, Larry Bird,

157
00:07:30,837 --> 00:07:32,306
Michael Jackson,

158
00:07:32,786 --> 00:07:35,454
David Letterman, Vivica A. Fox.

159
00:07:35,622 --> 00:07:37,998
Okay, so here's where
it gets a little dicey,

160
00:07:38,166 --> 00:07:42,321
'cause there's not that many celebrities
from Indiana, so... a NASCAR,

161
00:07:43,291 --> 00:07:46,745
my friend Becky, Ron Swanson...

162
00:07:47,906 --> 00:07:49,343
Who's the Jesus?

163
00:07:49,511 --> 00:07:51,361
That would be Greg Kinnear.

164
00:07:51,941 --> 00:07:55,319
- I didn't know he was from Indiana.
- Yeah, you know, I read that he was.

165
00:07:55,444 --> 00:07:57,619
Do you think
he's the best choice for Jesus?

166
00:07:57,744 --> 00:07:59,065
He was great on <i>ER</i>...

167
00:07:59,190 --> 00:08:01,188
- Greg Kinnear wasn't in <i>ER</i>.
- Yeah, he was.

168
00:08:01,356 --> 00:08:02,856
I don't think that he was.

169
00:08:03,931 --> 00:08:05,481
Who am I thinking of?

170
00:08:06,304 --> 00:08:07,444
Okay. Next.

171
00:08:12,826 --> 00:08:14,526
Really good, Jerry.

172
00:08:15,329 --> 00:08:16,412
For my murinal,

173
00:08:16,580 --> 00:08:18,636
I was inspired
by the death of my grandma...

174
00:08:18,761 --> 00:08:20,211
You said "murinal".

175
00:08:21,420 --> 00:08:24,044
- No, I didn't.
- You said "murinal". I heard it.

176
00:08:24,262 --> 00:08:25,838
- Anyway, she...
- Jerry...

177
00:08:26,006 --> 00:08:27,989
Why don't you put that murinal
in the men's room

178
00:08:28,114 --> 00:08:30,634
so people can murinate all over it?

179
00:08:32,137 --> 00:08:35,305
Go to the doctor. You might have
a murinary tract infection.

180
00:08:35,925 --> 00:08:39,017
- Just wanted to show you my art.
- Murinal, murinal, murinal!

181
00:08:39,846 --> 00:08:41,395
Disqualified!

182
00:08:42,841 --> 00:08:44,191
It's pointillism.

183
00:08:44,672 --> 00:08:47,568
And each dot is a photo
of a citizen of the town.

184
00:08:48,371 --> 00:08:50,490
No one cares.
At all.

185
00:08:52,629 --> 00:08:55,145
So this is a multimedia project.

186
00:08:55,367 --> 00:08:56,341
First...

187
00:08:56,697 --> 00:08:58,579
A bunch of rats made out of garbage.

188
00:08:59,305 --> 00:09:00,305
And...

189
00:09:00,624 --> 00:09:01,739
This is a tv screen.

190
00:09:01,864 --> 00:09:03,609
It'll be like a big flat screen tv,

191
00:09:03,734 --> 00:09:05,982
and it'll play looped video
of knee surgeries.

192
00:09:06,107 --> 00:09:09,965
And this is a human-sized hamster wheel
that will be next to the mural,

193
00:09:10,163 --> 00:09:12,048
if we can get one,
and it'll be spinning,

194
00:09:12,173 --> 00:09:14,155
and there'll be like
a fat guy in it all the time

195
00:09:14,280 --> 00:09:17,055
like screaming and like eating
raw beef and like bleeding,

196
00:09:17,383 --> 00:09:19,600
and like blood will come
out of his mouth and stuff.

197
00:09:19,834 --> 00:09:22,734
And that'll be like right next to...

198
00:09:23,325 --> 00:09:24,392
the mural.

199
00:09:24,893 --> 00:09:26,347
I have one question.

200
00:09:26,952 --> 00:09:27,823
Why?

201
00:09:28,447 --> 00:09:30,277
If you have to ask,
you don't get it.

202
00:09:30,630 --> 00:09:32,863
I don't think
they really got the assignment.

203
00:09:33,031 --> 00:09:34,516
If we're gonna beatthe others,

204
00:09:34,641 --> 00:09:37,952
we have to choose something that
will stand the test of time...

205
00:09:38,119 --> 00:09:39,519
Like the Mona Lisa

206
00:09:40,119 --> 00:09:41,696
or the music of Squeeze.

207
00:09:44,078 --> 00:09:45,959
June 8, 1922.

208
00:09:46,127 --> 00:09:48,462
The Pawnee bread factory
burned to the ground.

209
00:09:48,852 --> 00:09:51,034
We lost a lot of good bread
that day...

210
00:09:51,159 --> 00:09:53,259
As well as several human lives.

211
00:09:54,405 --> 00:09:56,553
And it also made the whole town
smell like toast,

212
00:09:57,024 --> 00:10:00,445
which one resident described
as "disturbingly enticing".

213
00:10:00,570 --> 00:10:01,600
But I digress.

214
00:10:01,768 --> 00:10:03,552
The point is, we rebuilt Pawnee

215
00:10:03,677 --> 00:10:05,479
to the wonderful town
that it is today.

216
00:10:06,012 --> 00:10:08,441
Why would you want a mural
that shows people dying

217
00:10:08,566 --> 00:10:10,740
when you could have shapes
that come alive?

218
00:10:10,865 --> 00:10:14,121
Because it's the most famous event
in our town's history,

219
00:10:14,246 --> 00:10:16,865
and people love voting for tragedy.

220
00:10:17,033 --> 00:10:19,944
Look at the oscars.
This is our holocaust movie.

221
00:10:20,991 --> 00:10:22,761
This is our <i>English Patient</i>.

222
00:10:23,274 --> 00:10:25,666
Sounds like you're
exploiting the tragedy.

223
00:10:25,854 --> 00:10:27,204
See? Ann gets it.

224
00:10:27,561 --> 00:10:28,987
Time to vote, everybody.

225
00:10:29,259 --> 00:10:30,410
Cast your votes!

226
00:10:30,535 --> 00:10:34,341
May the best,
most tragic project win.

227
00:10:34,509 --> 00:10:37,008
When we started this,
we were six different voices

228
00:10:37,133 --> 00:10:38,762
with six ideas for a mural.

229
00:10:39,465 --> 00:10:42,288
And then those six voices
came together as a team.

230
00:10:42,767 --> 00:10:44,407
There are no losers today.

231
00:10:45,061 --> 00:10:46,376
But there is one winner.

232
00:10:46,501 --> 00:10:47,303
Us.

233
00:10:47,881 --> 00:10:49,431
The Parks Department.

234
00:10:50,066 --> 00:10:52,017
Tom, the results
of the vote, please.

235
00:10:52,402 --> 00:10:55,112
One to one to one
to one to one to one.

236
00:10:56,250 --> 00:10:58,198
We all voted
for ourselves, didn't we?

237
00:11:05,187 --> 00:11:06,587
Third time, today.

238
00:11:09,158 --> 00:11:10,794
That's a good shoeshine.

239
00:11:32,095 --> 00:11:33,852
What the ****, man?

240
00:11:34,709 --> 00:11:36,559
I don't know what happened.

241
00:11:37,238 --> 00:11:38,238
Frankly.

242
00:11:38,716 --> 00:11:39,906
I emitted a noise.

243
00:11:41,233 --> 00:11:43,705
The noise was involuntary.

244
00:11:46,101 --> 00:11:48,851
Sometimes a sound
is just a sound, you know?

245
00:11:50,793 --> 00:11:54,212
We need to whittle these down.
Can we all agree on eliminating

246
00:11:54,380 --> 00:11:56,673
- any of these designs?
- Ann's blows.

247
00:11:56,937 --> 00:11:58,258
Don't hold back.

248
00:11:58,426 --> 00:12:01,303
It's a giant picture of a park.
That's not art.

249
00:12:01,746 --> 00:12:04,598
At least it's not a fat human hamster
eating meat.

250
00:12:04,766 --> 00:12:06,808
- You don't work here!
- You both have a point.

251
00:12:07,568 --> 00:12:09,956
Ann, yours was a little trite.

252
00:12:10,250 --> 00:12:13,750
And, April, yours was hellish
and might make someone vomit.

253
00:12:16,110 --> 00:12:19,549
There's something about those shapes.
There's some emotional art right there.

254
00:12:19,674 --> 00:12:21,857
- Any kid could do that.
- No kid could do that!

255
00:12:21,982 --> 00:12:23,325
Only god could do that!

256
00:12:23,493 --> 00:12:25,489
What is so great about the shapes?

257
00:12:25,912 --> 00:12:28,580
He likes the shapes, okay?
And he's part of the team.

258
00:12:28,748 --> 00:12:32,398
So here, take these scissors
and cut out your favorite shapes.

259
00:12:32,589 --> 00:12:35,617
And then we'll put them
on a new team mural.

260
00:12:37,111 --> 00:12:38,895
We're gonna make a new design

261
00:12:39,020 --> 00:12:42,219
that takes the best parts
of all of our designs.

262
00:12:42,719 --> 00:12:44,953
It's like if you got Michelangelo

263
00:12:45,078 --> 00:12:46,504
and Andy Warhol

264
00:12:46,629 --> 00:12:50,222
and Jackson Pollock
and Jim Davis from Garfield

265
00:12:50,347 --> 00:12:51,797
to do one painting.

266
00:12:52,704 --> 00:12:54,731
Imagine how good
that painting would be!

267
00:12:58,734 --> 00:13:00,484
I think it's really good.

268
00:13:04,640 --> 00:13:06,090
I'll be right back.

269
00:13:07,996 --> 00:13:09,577
We need you for something.

270
00:13:09,702 --> 00:13:11,957
Can it wait?
I am so swamped here.

271
00:13:12,651 --> 00:13:14,001
Ann's in trouble.

272
00:13:14,580 --> 00:13:15,961
We think it might be pills.

273
00:13:16,129 --> 00:13:17,087
- What?
- No.

274
00:13:17,735 --> 00:13:18,672
That's a lie.

275
00:13:19,799 --> 00:13:22,259
But this is as important.
Look at a piece of art.

276
00:13:23,633 --> 00:13:24,633
God.

277
00:13:26,490 --> 00:13:27,431
What is that?

278
00:13:28,007 --> 00:13:30,309
This is our entry
for the mural contest.

279
00:13:30,434 --> 00:13:33,019
We couldn't decide on one design,
so we took parts

280
00:13:33,187 --> 00:13:36,077
that we all liked of our designs
and then we just kind of...

281
00:13:36,457 --> 00:13:38,538
smooshed it into a thing.

282
00:13:40,319 --> 00:13:41,486
You made a camel.

283
00:13:43,278 --> 00:13:44,785
You've never heard that saying?

284
00:13:44,910 --> 00:13:47,701
A camel is actually a horse
designed by a committee,

285
00:13:47,869 --> 00:13:49,327
and what you guys have here

286
00:13:49,495 --> 00:13:50,995
is one ugly camel...

287
00:13:51,964 --> 00:13:53,564
featuring Bill Paxton.

288
00:13:53,875 --> 00:13:55,367
- Greg Kinnear.
- Right.

289
00:13:55,492 --> 00:13:57,210
Is Bill Paxton from Indiana?

290
00:13:57,378 --> 00:13:59,963
You have to save us.
You have to design something.

291
00:14:00,131 --> 00:14:02,674
- Leslie, I'm not an artist.
- Yes, you are.

292
00:14:02,884 --> 00:14:04,784
I've seen you sketch things.

293
00:14:05,345 --> 00:14:06,720
Like poles for stop signs.

294
00:14:06,888 --> 00:14:08,597
That everybody stops and looks at.

295
00:14:08,765 --> 00:14:10,390
By law, they're required to.

296
00:14:10,558 --> 00:14:13,435
Please. You're the only one of us
who's got any talent.

297
00:14:13,865 --> 00:14:15,645
I know you'll do something good.

298
00:14:15,813 --> 00:14:17,773
And I really want to win this.

299
00:14:17,941 --> 00:14:19,917
Please?
What are you gonna do tonight?

300
00:14:20,042 --> 00:14:22,027
I was gonna go to Arby's
and watch <i>Frontline</i>.

301
00:14:29,553 --> 00:14:30,603
Here we go.

302
00:14:33,873 --> 00:14:35,705
So, it is an old man

303
00:14:36,127 --> 00:14:37,873
feeding pigeons in a park.

304
00:14:39,116 --> 00:14:40,754
The spirit of Pawnee, maybe?

305
00:14:40,922 --> 00:14:43,536
- You gotta be kidding me.
- How is that better than mine?

306
00:14:43,661 --> 00:14:45,491
It isn't.
And that's saying something.

307
00:14:45,616 --> 00:14:47,766
And who's the man?
Is he famous?

308
00:14:48,186 --> 00:14:49,686
Is it Martin Landau?

309
00:14:49,889 --> 00:14:52,766
I have no dog in this fight.
I'm just saying that this will win.

310
00:14:53,427 --> 00:14:55,177
It's nothing.
It's mush!

311
00:14:55,365 --> 00:14:57,774
There's not even one shape in there!
Where are the shapes?

312
00:14:57,899 --> 00:14:59,481
I'm not saying this is any good.

313
00:14:59,649 --> 00:15:02,411
I'm saying that this'll win.
It's mass appeal. It's like

314
00:15:02,536 --> 00:15:03,986
what motels put up,

315
00:15:04,487 --> 00:15:08,237
and it hangs there for years,
and no one ever throws acid at it.

316
00:15:08,653 --> 00:15:10,992
- Who did this? I like it.
- Seriously?

317
00:15:12,662 --> 00:15:13,521
Sorry!

318
00:15:14,791 --> 00:15:15,872
It comforts me.

319
00:15:18,136 --> 00:15:19,084
That'll win.

320
00:15:20,232 --> 00:15:21,322
This is...

321
00:15:22,231 --> 00:15:24,192
garbage!
We've gotta go back to the shapes.

322
00:15:24,317 --> 00:15:26,437
This is boring.
Go back to the garbage.

323
00:15:26,562 --> 00:15:28,635
I would take the shapes over this.
Come on!

324
00:15:28,808 --> 00:15:32,241
I wouldn't take them over anything.
I'd take Jerry's Murinal over this.

325
00:15:32,366 --> 00:15:33,416
No, guys...

326
00:15:34,142 --> 00:15:35,546
- This is the one.
- What?

327
00:15:35,671 --> 00:15:36,878
Sorry. Mark's right.

328
00:15:37,003 --> 00:15:39,104
This is an outrage!
He's not  in the department.

329
00:15:39,272 --> 00:15:41,189
- Neither is Ann.
- But Ann's hot.

330
00:15:41,357 --> 00:15:42,411
And that counts.

331
00:15:42,536 --> 00:15:44,696
Whatever happened to,
"we're the parks department.

332
00:15:44,821 --> 00:15:47,070
"Rah, rah, rah.
Down with the Sewage Department"?

333
00:15:47,446 --> 00:15:49,781
We spent all day here for no reason!

334
00:15:49,949 --> 00:15:52,367
We have a reason! We're gonna win!
That's our reason!

335
00:15:52,535 --> 00:15:55,485
Our designs are not gonna win.
Mark's might win!

336
00:15:55,873 --> 00:15:57,455
How great would it be if we won.

337
00:15:57,709 --> 00:15:59,541
You'll feel a lot better,
I promise you,

338
00:15:59,709 --> 00:16:01,780
after you win.
We're going with this one.

339
00:16:02,145 --> 00:16:04,337
I'll see you
at the presentation tomorrow.

340
00:16:04,680 --> 00:16:07,132
That is an order, team.
Go, team!

341
00:16:08,775 --> 00:16:09,759
Team dismissed.

342
00:16:10,804 --> 00:16:13,305
Yes, we are a team,
but I am the team leader.

343
00:16:13,629 --> 00:16:15,429
So I made a bold decision.

344
00:16:15,669 --> 00:16:17,264
We're playing it safe.

345
00:16:27,287 --> 00:16:29,029
How's life
in the Parks De-fart-ment?

346
00:16:29,197 --> 00:16:31,656
Better than life
in the Sewage De-fart-ment.

347
00:16:31,781 --> 00:16:33,963
- Which makes more sense.
- Whatever!

348
00:16:34,702 --> 00:16:35,847
You are screwed.

349
00:16:35,972 --> 00:16:37,322
Get used to this.

350
00:16:38,776 --> 00:16:41,458
Because it's gonna be hanging on
the wall, right outside your door

351
00:16:41,799 --> 00:16:44,169
for the next 100 years!

352
00:16:48,569 --> 00:16:49,758
That was pretty good.

353
00:16:50,312 --> 00:16:52,061
You think they're gonna win?

354
00:16:52,629 --> 00:16:55,472
They went super-patriotic.
It's a classic mistake.

355
00:16:55,640 --> 00:16:57,738
It seems crowd-pleasing,
but it's a stance.

356
00:16:57,863 --> 00:17:00,477
And there's always someone
who'll oppose a stance.

357
00:17:01,006 --> 00:17:01,811
Us...

358
00:17:02,185 --> 00:17:04,585
Old man feeding pigeons...
No stance.

359
00:17:05,039 --> 00:17:07,150
Absolutely no point of view
whatsoever.

360
00:17:08,312 --> 00:17:09,903
No point of view.
Smart.

361
00:17:12,296 --> 00:17:13,796
I haven't been back.

362
00:17:14,462 --> 00:17:16,862
I wish him good luck
in his business.

363
00:17:17,745 --> 00:17:20,246
And, yes,
I will absolutely go back

364
00:17:20,579 --> 00:17:23,165
to get my shoes shined soon.

365
00:17:24,321 --> 00:17:26,004
I don't frankly see

366
00:17:26,129 --> 00:17:28,779
why this is a topic for discussion.

367
00:17:45,474 --> 00:17:46,474
What's up?

368
00:17:50,030 --> 00:17:51,980
You know the thing?

369
00:17:53,017 --> 00:17:55,767
- The other day?
- Other day... Other day...

370
00:17:56,450 --> 00:17:57,450
Yesterday?

371
00:17:57,727 --> 00:17:58,727
I am...

372
00:17:59,575 --> 00:18:00,537
That was...

373
00:18:01,368 --> 00:18:03,918
I'm okay.
You know, I'd be okay if we...

374
00:18:05,668 --> 00:18:07,752
If we never mentioned it again.

375
00:18:09,297 --> 00:18:10,463
Never mentioned what?

376
00:18:12,027 --> 00:18:14,132
The moan.
The weird moan you made

377
00:18:14,257 --> 00:18:16,344
that was super-weird?
Do you not remember that?

378
00:18:16,512 --> 00:18:18,847
I talked about it with the lady
who went after for 1/2 hour.

379
00:18:19,172 --> 00:18:21,057
She said she thought
it was an animal...

380
00:18:22,351 --> 00:18:24,151
It was just an odd moment.

381
00:18:24,276 --> 00:18:26,676
Let's just not
talk about it anymore.

382
00:18:27,057 --> 00:18:28,770
That's what I was trying to say.

383
00:18:28,977 --> 00:18:30,377
But, yeah... Okay.

384
00:18:31,027 --> 00:18:31,985
Good job.

385
00:18:40,283 --> 00:18:41,953
- Miss Knope.
- Chief Konner.

386
00:18:42,121 --> 00:18:43,288
What do you got?

387
00:18:43,944 --> 00:18:47,194
Well, we aren't really artists,
but we gave it a shot.

388
00:18:47,660 --> 00:18:49,728
It's your basic dogs playing poker,

389
00:18:49,853 --> 00:18:52,297
but,
with an everything's-on-fire theme.

390
00:18:53,215 --> 00:18:55,115
This is my nephew over here.

391
00:18:55,488 --> 00:18:59,095
And this is an attractive lady
with a hamburger for a head.

392
00:18:59,365 --> 00:19:00,930
Just some stuff we like.

393
00:19:01,224 --> 00:19:03,308
Anyway, we had a lot of fun
putting it together.

394
00:19:04,852 --> 00:19:06,019
Good luck!

396
00:19:14,523 --> 00:19:16,023
- Go ahead.
- What?

397
00:19:17,266 --> 00:19:19,016
The camel's way more fun.

398
00:19:20,812 --> 00:19:22,118
I want my team back.

399
00:19:22,286 --> 00:19:23,661
And my team made this

400
00:19:24,093 --> 00:19:25,693
hot, crazy camel mess.

401
00:19:26,286 --> 00:19:28,166
So this is what we're gonna submit,

402
00:19:28,334 --> 00:19:29,751
even if it means we lose.

403
00:19:30,184 --> 00:19:32,837
God, I hope we win.
But we're definitely gonna lose.

404
00:19:33,005 --> 00:19:33,971
Probably.

406
00:19:36,609 --> 00:19:38,482
And Ann!
How Ann gets it done!

407
00:19:39,471 --> 00:19:40,471
Let's go!

408
00:19:47,442 --> 00:19:48,603
What are the shapes?

409
00:19:48,778 --> 00:19:51,282
The shapes are awesome,
is what they are.

410
00:19:51,407 --> 00:19:52,649
You can't handle it.

411
00:19:52,817 --> 00:19:53,942
No, I like them.

412
00:19:54,678 --> 00:19:55,568
Forgive me.

413
00:19:56,015 --> 00:19:57,320
Is that Michael Jackson?

414
00:19:58,906 --> 00:20:01,866
- The pride of Indiana.
- That's right. So it's relevant.

415
00:20:02,069 --> 00:20:03,201
Who is he carrying?

416
00:20:03,523 --> 00:20:04,973
Jesus Greg Kinnear.

417
00:20:05,421 --> 00:20:09,082
It looks like he's carrying Kinnear
into the burning building.

418
00:20:10,039 --> 00:20:12,362
That's because he's <i>moonwalking</i>.

419
00:20:13,043 --> 00:20:14,043
So he...

420
00:20:14,928 --> 00:20:16,928
Should be goin' the other way.

421
00:20:17,472 --> 00:20:18,925
That did not occur to me.

422
00:20:22,780 --> 00:20:24,597
- Sorry.
- So there you go.

423
00:20:24,765 --> 00:20:26,315
The spirit of Pawnee.

424
00:20:27,018 --> 00:20:28,017
We didn't win.

425
00:20:28,339 --> 00:20:30,023
But neither did anyone else.

426
00:20:30,148 --> 00:20:33,231
They realized it was gonna cost
a ton of money, to hire a muralist.

427
00:20:33,508 --> 00:20:35,649
So they're just gonna restore
the old one.

428
00:20:35,774 --> 00:20:38,924
They're changing the title
to the diversity express.

429
00:20:44,740 --> 00:20:45,890
Look at that.

430
00:20:48,135 --> 00:20:51,749
- It's not as good as the other one.
- I don't know what you mean by good?

431
00:20:51,917 --> 00:20:53,032
Neither do i!

432
00:20:53,157 --> 00:20:54,657
Just do another one!

433
00:20:54,792 --> 00:20:57,788
I have actual assignments
that I have to finish for art school.

434
00:20:57,913 --> 00:21:00,133
Shut up and do more art for me.

435
00:21:02,892 --> 00:21:04,292
This one's racist.

436
00:21:05,363 --> 00:21:06,613
It's beautiful.

437
00:21:08,706 --> 00:21:11,256
I've looked at this
for five hours, now.

438
00:21:13,706 --> 00:21:16,806
I like the green one
and the red circle right here.

439
00:21:21,873 --> 00:21:23,373
I'm tearing up, man.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
