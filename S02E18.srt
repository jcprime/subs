1
00:00:02,865 --> 00:00:05,046
Ann Perkins,
what's happening, sweetheart?

2
00:00:05,433 --> 00:00:06,867
Just waiting for April.

3
00:00:06,992 --> 00:00:08,549
She's taking care of my house.

4
00:00:08,797 --> 00:00:11,150
That's great.
Happy belated valentine's day.

5
00:00:11,275 --> 00:00:14,388
Valentine's day was a month ago.
Why are you giving it to me now?

6
00:00:14,556 --> 00:00:18,560
Whatever. Happy early valentine's day.
I saw this and I thought of you.

7
00:00:18,910 --> 00:00:21,084
Hope you keep that
somewhere special.

8
00:00:21,209 --> 00:00:23,939
Okay, this is one of those nanny cam
teddy bears, isn't it?

9
00:00:24,107 --> 00:00:25,107
What?
No!

10
00:00:25,259 --> 00:00:27,385
It's a regular
camera-less teddy bear.

11
00:00:27,510 --> 00:00:30,161
Just put it in your bedroom,
don't even think about it.

12
00:00:30,940 --> 00:00:33,741
It's a robot bear.
It's programmed to snuggle.

13
00:00:35,024 --> 00:00:37,574
- I'll take it.
- Donna, there's a camera in it.

14
00:00:37,699 --> 00:00:38,699
I know.

15
00:00:40,374 --> 00:00:41,999
- Feygnasse Team -

16
00:00:42,167 --> 00:00:43,575
.:: La Fabrique ::.

17
00:01:00,635 --> 00:01:04,044
All right, that's basically it.
Water the plants, get the mail...

18
00:01:04,195 --> 00:01:06,148
- Can I read your mail?
- No, please don't.

19
00:01:06,854 --> 00:01:08,526
- Fine.
- And...

20
00:01:08,975 --> 00:01:10,778
Here are the keys, and remember...

21
00:01:10,946 --> 00:01:13,030
I know, don't let Tom make a copy.

22
00:01:13,198 --> 00:01:14,359
That's right, good.

23
00:01:14,484 --> 00:01:17,941
I'm paying April 50 bucks
to watch my house while I'm away.

24
00:01:18,245 --> 00:01:19,777
I would have asked Leslie,

25
00:01:20,269 --> 00:01:22,456
but I've seen the way
she takes care of her house.

26
00:01:22,889 --> 00:01:26,429
Plus, there's always been
a little distance between me and April.

27
00:01:26,554 --> 00:01:29,446
And I thought this would maybe,
give us a chance...

28
00:01:29,571 --> 00:01:31,549
to get a little closer,
maybe even...

29
00:01:31,717 --> 00:01:32,717
I don't like Ann.

30
00:01:33,232 --> 00:01:35,186
Become friends.
Who knows?

31
00:01:36,599 --> 00:01:40,016
Leslie, this is Evelyn Rowshlind
from the mayor's office.

32
00:01:40,655 --> 00:01:42,643
Hello, I've always dreamed of you.

33
00:01:42,811 --> 00:01:44,605
- Excuse me?
- Meeting you.

34
00:01:44,827 --> 00:01:47,520
- What can I do for you? Have a seat.
- I prefer to stand.

35
00:01:47,645 --> 00:01:49,460
I have kind of an odd favor to ask.

36
00:01:49,585 --> 00:01:51,277
Mayor Gunderson was playing golf

37
00:01:51,445 --> 00:01:53,946
at Pawnee municipal,
and he brought his dog...

38
00:01:54,114 --> 00:01:56,450
Rufus. We all know Rufus.
Everybody loves Rufus.

39
00:01:56,575 --> 00:01:58,325
He's a great dog.
Continue.

40
00:01:58,493 --> 00:02:01,669
They were out on the sixth hole
when suddenly, out of nowhere...

41
00:02:01,794 --> 00:02:03,247
Rufus was bitten by a possum?

42
00:02:03,470 --> 00:02:04,999
Yes, how did you know that?

43
00:02:05,788 --> 00:02:07,113
It was Fairway Frank.

44
00:02:07,238 --> 00:02:09,910
Fairway Frank is this awful possum

45
00:02:10,035 --> 00:02:12,298
who lives near the sixth hole
of the public golf course.

46
00:02:12,542 --> 00:02:16,093
He's number 3 on the parks department's
most wanted pests list.

47
00:02:16,261 --> 00:02:19,054
Right behind the bats
who like to poop on the bell tower,

48
00:02:19,222 --> 00:02:22,516
and poopy, the raccoon who poops
all over the high school cafeteria.

49
00:02:22,684 --> 00:02:24,810
Shouldn't you take this
up with animal control?

50
00:02:24,978 --> 00:02:26,021
Let's be honest,

51
00:02:26,313 --> 00:02:28,689
it's not the most effective
branch of our government.

52
00:02:28,857 --> 00:02:30,441
A bunch of burned-out morons.

53
00:02:30,609 --> 00:02:32,985
You have the reputation
as a person who gets things done.

54
00:02:33,153 --> 00:02:35,568
So we'd like
you to form a little task force,

55
00:02:35,693 --> 00:02:37,440
find the animal, and put it down.

56
00:02:37,565 --> 00:02:39,158
- Task force?
- Needless to say,

57
00:02:39,326 --> 00:02:41,370
the mayor will be very grateful
for your help.

58
00:02:41,495 --> 00:02:43,495
Ma'am, the next time we speak,

59
00:02:43,879 --> 00:02:45,873
we shall be dancing
on the grave of a possum.

60
00:02:46,412 --> 00:02:47,249
Great.

61
00:02:47,745 --> 00:02:49,495
Ron will show you around.

62
00:02:50,219 --> 00:02:52,630
Right this way is the exit.

63
00:02:52,916 --> 00:02:53,916
Yo, police.

64
00:02:55,412 --> 00:02:57,051
No, you're not.
Coffee?

65
00:02:57,498 --> 00:02:59,345
You always know what to bring me.

66
00:02:59,668 --> 00:03:01,180
I only ever bring you coffee.

67
00:03:02,342 --> 00:03:05,627
And it is my very favorite
non-alcoholic hot drink,

68
00:03:05,752 --> 00:03:07,142
except for hot tea.

69
00:03:07,267 --> 00:03:09,632
And hot orange juice,
weirdly delicious.

70
00:03:09,757 --> 00:03:11,449
Anyways, thank you so much.
I needed it.

71
00:03:11,574 --> 00:03:13,901
I got a really bad case
of shoe shine head today.

72
00:03:14,069 --> 00:03:16,119
Andy recently diagnosed himself

73
00:03:16,244 --> 00:03:19,281
with what he calls
"shoe shine head".

74
00:03:19,862 --> 00:03:21,909
It's when you shine too many shoes

75
00:03:22,077 --> 00:03:23,786
and the fumes create

76
00:03:23,954 --> 00:03:25,749
a thunderstorm in your brain.

77
00:03:26,361 --> 00:03:28,817
Cures include coffee,

78
00:03:29,584 --> 00:03:30,543
cheeseburgers,

79
00:03:30,798 --> 00:03:32,128
and napping on the floor.

80
00:03:37,053 --> 00:03:39,034
- Hey, Ron. What's up?
- I...

81
00:03:40,262 --> 00:03:42,637
have a wood shop
and I'm planning a little expansion.

82
00:03:42,762 --> 00:03:46,762
Need my buddy Mark, the city planner,
to sign off on the plans.

83
00:03:47,060 --> 00:03:49,015
Sure, I just need
to schedule an inspection.

84
00:03:49,140 --> 00:03:51,711
- That's not really necessary.
- Yes, it is, but you'll be fine

85
00:03:51,836 --> 00:03:54,193
as long as you
don't have any code violations.

86
00:03:54,318 --> 00:03:57,018
You don't have
any code violations, do you?

87
00:04:02,804 --> 00:04:03,804
Nope.

88
00:04:07,174 --> 00:04:08,174
Eugene!

89
00:04:08,299 --> 00:04:10,124
Boy, we have
a really important job for you.

90
00:04:10,292 --> 00:04:13,661
- We'll get to it first thing Monday.
- Today's Wednesday.

91
00:04:13,927 --> 00:04:16,380
- Look, this is not a request.
- We're acting under direct orders

92
00:04:16,727 --> 00:04:18,541
- from mayor Gunderson's dog.
- Office.

93
00:04:18,666 --> 00:04:22,261
I need your two best guys to join me
and be part of my task force.

94
00:04:22,542 --> 00:04:24,592
That would be Harris and Brett.

95
00:04:25,923 --> 00:04:27,349
But they're not here.

96
00:04:27,517 --> 00:04:29,226
- Isn't that them there?
- Nope.

97
00:04:31,042 --> 00:04:32,042
Dude!

98
00:04:32,230 --> 00:04:35,041
That stupid possum
is on the golf course again.

99
00:04:35,166 --> 00:04:38,152
Would you rather I capture it myself
and then you can come and pick it up?

100
00:04:42,179 --> 00:04:43,706
Cool!
You want your ball back?

101
00:04:43,831 --> 00:04:45,485
Want your ball back?
Come with me.

102
00:04:48,917 --> 00:04:50,372
How you know my name, homie?

103
00:04:50,540 --> 00:04:51,991
- Stop.
- Stop hanging yourself.

104
00:04:52,116 --> 00:04:54,546
- I'm gonna hang yourself.
- Stop hanging yourself.

105
00:04:54,671 --> 00:04:58,110
- Quite a crack team you've assembled.
- There's 5 of us and only one possum.

106
00:04:58,235 --> 00:04:59,309
How hard can it be?

107
00:04:59,434 --> 00:05:01,834
Excuse us,
task force coming through.

108
00:05:03,299 --> 00:05:05,179
Task force clear, resume golfing.

109
00:05:05,471 --> 00:05:08,670
I used to love Tiger Woods
because he was a great champion.

110
00:05:08,795 --> 00:05:11,595
But after that sex scandal,
the man is a God.

111
00:05:16,842 --> 00:05:18,275
Welcome to my haven.

112
00:05:18,910 --> 00:05:22,029
You're the first non-me to set foot
in this building in ten years.

113
00:05:23,672 --> 00:05:26,575
- None of this is up to code.
- Sure it is.

114
00:05:26,743 --> 00:05:28,937
It's up to the Swanson code.

115
00:05:29,496 --> 00:05:32,081
There's no drainage,
doesn't seem to be any ventilation,

116
00:05:32,249 --> 00:05:34,458
you've got hazardous chemicals
over here.

117
00:05:34,709 --> 00:05:36,243
Yeah, which only I am breathing.

118
00:05:36,368 --> 00:05:39,510
It's the same liberty that gives me
the right to fart in my own car.

119
00:05:39,635 --> 00:05:42,557
Are you gonna tell a man
that he can't fart in his own car?

120
00:05:42,682 --> 00:05:44,993
There is a basket of oil-soaked rags

121
00:05:45,118 --> 00:05:47,274
above a wood burning stove.

122
00:05:47,639 --> 00:05:49,392
Good thing
I've got a fire extinguisher.

123
00:05:49,517 --> 00:05:51,217
Which, I can assure you,

124
00:05:51,342 --> 00:05:53,776
is totally up to your precious code.

125
00:05:56,219 --> 00:05:59,269
This says it should be recharged
June of 1996.

126
00:05:59,568 --> 00:06:00,924
Those dates are arbitrary.

127
00:06:01,049 --> 00:06:03,209
They're like those expiration
dates that the government

128
00:06:03,334 --> 00:06:05,572
forces companies to put
on yogurt and medicine.

129
00:06:06,276 --> 00:06:08,156
Observe.
Watch yourself.

130
00:06:13,327 --> 00:06:14,727
I'll replace this.

131
00:06:15,219 --> 00:06:16,125
Happy?

132
00:06:17,071 --> 00:06:19,169
How long
it would take me to learn golf?

133
00:06:19,337 --> 00:06:21,430
I could teach you.
I have a 16 handicap.

134
00:06:21,555 --> 00:06:24,508
But it takes a lot of practice.
You have to get up early, hit the range,

135
00:06:24,676 --> 00:06:27,146
- practice reading greens.
- See, I don't wanna do all that.

136
00:06:27,271 --> 00:06:29,179
I just want
some of those dope pants.

137
00:06:31,333 --> 00:06:32,783
It's Fairway Frank!

138
00:06:34,282 --> 00:06:36,912
All right, Tom?
I want you to very slowly...

139
00:06:43,069 --> 00:06:45,489
25, 26, 27, 28...

140
00:06:45,822 --> 00:06:49,008
- violations.
- Come on, Brendanawicz. Relax.

141
00:06:50,076 --> 00:06:51,776
Let me make you a canoe.

142
00:06:53,371 --> 00:06:55,998
I don't need a canoe, Ron.
I need you to fix this place up.

143
00:06:58,413 --> 00:06:59,940
I'll give you 24 hours.

144
00:07:11,573 --> 00:07:13,491
We may only get
one chance to catch this thing,

145
00:07:13,616 --> 00:07:14,969
so we have to play it smart.

146
00:07:15,094 --> 00:07:17,120
You two flank the left,
I'll flank the right,

147
00:07:17,245 --> 00:07:18,912
Andy will come in through...

148
00:07:24,029 --> 00:07:25,029
I got it.

149
00:07:26,860 --> 00:07:28,390
- I got him.
- Task force! Engage!

150
00:07:28,599 --> 00:07:30,157
- He's got it.
- He's good.

151
00:07:30,325 --> 00:07:33,452
Come on, you cowards!
Lemme see his face.

152
00:07:33,620 --> 00:07:34,870
It's him, we got him!

153
00:07:35,218 --> 00:07:36,884
It's okay.
He's on my neck.

154
00:07:37,376 --> 00:07:39,296
- He's on my neck.
- It's all right.

155
00:07:39,421 --> 00:07:40,542
All right, it's okay.

156
00:07:40,823 --> 00:07:42,387
You're next, poopy.

157
00:07:42,685 --> 00:07:44,097
Very impressive, Leslie.

158
00:07:44,488 --> 00:07:46,254
Thank you.
I had a great task force.

159
00:07:46,379 --> 00:07:48,739
I was just the simple mastermind
who planned the whole operation

160
00:07:48,864 --> 00:07:50,260
and executed it to perfection.

161
00:07:50,595 --> 00:07:52,888
This is Andy Dwyer.
He actually caught the thing.

162
00:07:53,056 --> 00:07:54,429
- Mr. Dwyer...
- Please...

163
00:07:54,554 --> 00:07:56,703
My friends call me Andy radical.

164
00:07:57,369 --> 00:07:58,477
No, we don't.

165
00:07:58,659 --> 00:08:01,343
By day, Andy Dwyer, shoeshine-ist.

166
00:08:01,832 --> 00:08:04,772
By different time of day,
Andy radical, possum tackler.

167
00:08:05,264 --> 00:08:06,514
And by night...

168
00:08:07,879 --> 00:08:09,530
I do whatever I want, no job.

169
00:08:09,816 --> 00:08:12,138
We think that this
is a great P.R. opportunity

170
00:08:12,263 --> 00:08:14,321
so we've arranged for a reporter
from The Journal

171
00:08:14,446 --> 00:08:16,912
to stop by to interview,
get some pictures of the team.

172
00:08:17,080 --> 00:08:18,414
Task force.
Great.

173
00:08:18,737 --> 00:08:22,584
And next time you need a special favor
from the mayor's office, give me a call.

174
00:08:23,556 --> 00:08:26,420
Oh, my God, I will.
I need so many special favors.

175
00:08:26,921 --> 00:08:29,258
- Which one should I choose?
- You don't have...

176
00:08:29,426 --> 00:08:31,135
How about an extra recycling can?

177
00:08:31,303 --> 00:08:32,845
Laminated bus pass?

178
00:08:33,013 --> 00:08:35,889
What time do yougo to bed?
'cause my best ideas come at night.

179
00:08:36,057 --> 00:08:37,848
Let's stick
to business hours, shall we?

180
00:08:38,957 --> 00:08:40,370
See you in hell, buddy.

181
00:08:46,686 --> 00:08:49,203
Am I sure the possum
we caught is Fairway Frank?

182
00:08:50,000 --> 00:08:51,690
Am I quite sure?
No.

183
00:08:51,890 --> 00:08:53,499
Am I sure enough?
Maybe.

184
00:08:53,624 --> 00:08:56,078
If it wasn't Fairway Frank,
would I feel badly? Of course.

185
00:08:56,203 --> 00:08:58,528
- Could I live with myself?
- That depends.

186
00:08:58,955 --> 00:09:02,228
As a city official it's important
that I ask myself a lot of questions.

187
00:09:02,353 --> 00:09:04,835
Does doing so
help me make decisions?

188
00:09:07,611 --> 00:09:09,100
- Hey, Shauna.
- Hi, Leslie.

189
00:09:09,225 --> 00:09:12,217
Andy, you remember Shauna Malwae-Tweep
from The Pawnee Journal.

190
00:09:12,385 --> 00:09:14,669
How could I forget?
You wrote the article when I fell.

191
00:09:14,794 --> 00:09:16,430
And then afterwards
had sex with Mark,

192
00:09:16,598 --> 00:09:18,548
and everyone talked about it.

193
00:09:21,394 --> 00:09:24,193
So you're on the golf course,
and you see Fairway Frank.

194
00:09:24,318 --> 00:09:27,636
Well, we see a possum
that we believe to be Fairway Frank.

195
00:09:27,761 --> 00:09:30,402
So for right now,
let's just refer to it as a possum.

196
00:09:30,570 --> 00:09:34,156
Let me explain something, Tweep.
When you're in a situation,

197
00:09:34,958 --> 00:09:36,783
you don't have time to think.

198
00:09:36,951 --> 00:09:38,494
So I thought to myself,

199
00:09:40,188 --> 00:09:42,041
"Don't think, Andy. Act."

200
00:09:42,523 --> 00:09:44,625
So you weren't thinking.

201
00:09:44,922 --> 00:09:45,922
Not at all.

202
00:09:46,918 --> 00:09:49,505
I cannot emphasize enough
how little I was thinking.

203
00:09:49,799 --> 00:09:51,162
Were you scared?

204
00:09:51,287 --> 00:09:52,637
No, no, I wasn't.

205
00:09:52,784 --> 00:09:56,553
Well, I lived in the pit
for the better part of last year

206
00:09:57,237 --> 00:09:59,736
and made some vermin friends.

207
00:10:00,749 --> 00:10:02,520
You know what?
Friends sounds stupid.

208
00:10:02,645 --> 00:10:03,645
Colleagues.

209
00:10:04,766 --> 00:10:06,666
They are bad at sharing,

210
00:10:07,295 --> 00:10:09,135
but they are good at tag.

211
00:10:09,526 --> 00:10:11,235
Well, your family
must be very proud.

212
00:10:11,677 --> 00:10:13,862
What does your girlfriend think?
The nurse?

213
00:10:15,740 --> 00:10:16,990
I broke up with Ann.

214
00:10:18,132 --> 00:10:21,051
Shortly after she kicked me out and told
we wouldn't be together anymore.

215
00:10:21,705 --> 00:10:23,622
Maybe this article
will change her mind.

216
00:10:26,474 --> 00:10:27,474
What now?

217
00:10:28,127 --> 00:10:29,805
I hadn't even thought of that.

218
00:10:31,570 --> 00:10:34,612
Of all the things I thought
would bring us back together,

219
00:10:34,737 --> 00:10:36,718
catching a possum
was never on that list.

220
00:10:41,307 --> 00:10:43,203
Let's go take some pictures
of Fairway Frank.

221
00:10:43,328 --> 00:10:44,530
Of the possum.

222
00:10:46,682 --> 00:10:48,856
Seriously, man,
when you wear these clothes,

223
00:10:48,981 --> 00:10:52,067
you just feel better than everyone else.
You know?

224
00:10:54,307 --> 00:10:55,028
Busy?

225
00:10:55,196 --> 00:10:57,896
- No, what's up?
- Just wanted to tell you,

226
00:10:58,384 --> 00:11:02,202
I understand that city codes exist
and I know why they exist.

227
00:11:03,955 --> 00:11:06,164
And I understand
that you enforce them.

228
00:11:07,542 --> 00:11:10,252
Okay, good talk.
Can you sign off on my plans now?

229
00:11:11,617 --> 00:11:13,386
Did you get everything up to code?

230
00:11:19,406 --> 00:11:20,956
- Yup.
- No, you didn't.

231
00:11:21,162 --> 00:11:23,765
- You clearly didn't.
- It's my property,

232
00:11:23,933 --> 00:11:24,933
my land,

233
00:11:25,263 --> 00:11:26,263
my shop!

234
00:11:27,244 --> 00:11:29,167
Tummy needs a banana, you guys good?

235
00:11:29,292 --> 00:11:31,106
Ron, you're asking me
to do a bad job,

236
00:11:31,274 --> 00:11:33,924
at my job,
and I'm just not gonna do that.

237
00:11:34,662 --> 00:11:37,821
So if you don't mind,
would you please get out of my office?

238
00:11:40,032 --> 00:11:43,410
Silly me, there was this little pom-pom
on my glove

239
00:11:43,578 --> 00:11:45,037
and it fell off. You've seen it?

240
00:11:45,711 --> 00:11:48,649
Could you help me look for it?
It should be on the ground somewhere

241
00:11:48,774 --> 00:11:51,004
where I was puttin' it up.
Let's see.

242
00:11:52,462 --> 00:11:54,318
- So how's your hole?
- Excuse me?

243
00:11:54,443 --> 00:11:55,839
The pit. Lot 48?

244
00:11:56,007 --> 00:11:57,994
We're making progress on the park.

245
00:11:58,119 --> 00:12:01,130
In fact, there may be some big news
on that front coming soon.

246
00:12:01,255 --> 00:12:04,222
All because of this very important
business card.

247
00:12:05,324 --> 00:12:08,518
So this isn't really a big story, right?
Nobody cares about this very much.

248
00:12:08,686 --> 00:12:11,980
Fairway Frank did bite the mayor's dog,
so this could be on the front page.

249
00:12:12,576 --> 00:12:15,339
<i>Fairway Frank you're gonna die</i>

250
00:12:15,684 --> 00:12:17,338
<i>You're gonna fry</i>

251
00:12:19,446 --> 00:12:21,948
<i>You guilty son of a bitch</i>

252
00:12:23,278 --> 00:12:26,654
<i>You're gonna fry
when they flip that switch</i>

253
00:12:31,125 --> 00:12:32,751
Can I talk to you about the possum?

254
00:12:33,419 --> 00:12:36,046
Hypothetically, if you were going
on a mission to say

255
00:12:36,214 --> 00:12:37,965
catch a guilty whale.

256
00:12:38,285 --> 00:12:41,397
And while you were catching the whale,
you saw something else

257
00:12:41,522 --> 00:12:44,646
that may also be another whale,
you were like "what?", you thought :

258
00:12:44,771 --> 00:12:48,016
"Maybe it's not a whale? But a big fish,
a submarine with a face painted on it?"

259
00:12:48,719 --> 00:12:52,270
The point is, if I kill the first whale,
am I technically a murderer?

260
00:12:52,438 --> 00:12:54,731
- What you're talking about?
- I saw a second possum.

261
00:12:56,242 --> 00:12:57,992
There was another possum.

262
00:12:58,681 --> 00:13:02,280
Andy's in there talking to a reporter,
literally kissing his own biceps.

263
00:13:02,958 --> 00:13:05,033
You're telling me
he might not be a hero,

264
00:13:05,201 --> 00:13:07,801
but just some jerk that goes around
tackling random possums?

265
00:13:08,246 --> 00:13:11,252
I've gotta help Leslie find the truth.
Not because I'm pissed at Andy,

266
00:13:11,377 --> 00:13:12,495
which I'm not.

267
00:13:13,662 --> 00:13:16,162
Because I care so deeply
about possums.

268
00:13:18,250 --> 00:13:20,090
'Cause they're so adorable.

269
00:13:20,576 --> 00:13:23,015
April, did you see my photo shoot?

270
00:13:24,565 --> 00:13:25,618
I nailed it.

271
00:13:25,743 --> 00:13:29,217
I fell off the stool when I was trying
to look serious. But I'm okay.

272
00:13:29,696 --> 00:13:32,146
That's cool,
I'm really happy for you.

273
00:13:32,867 --> 00:13:33,867
You are?

274
00:13:36,553 --> 00:13:38,608
Our happy reactions
are super different.

275
00:13:39,297 --> 00:13:42,446
So, he said he's gonna email me
the photos in, like, six hours.

276
00:13:42,924 --> 00:13:45,460
- You wanna wait with me?
- That sounds good,

277
00:13:45,585 --> 00:13:48,910
but I can't because I have to go
do something that actually matters.

278
00:13:53,057 --> 00:13:54,499
Fairway Frank's been haunting

279
00:13:54,667 --> 00:13:56,793
the 6th hole for a while.
You must know him well.

280
00:13:57,118 --> 00:13:58,118
I sure do.

281
00:13:58,451 --> 00:14:01,541
I've chased that little sucker off
more times than I can count.

282
00:14:01,970 --> 00:14:04,132
Can you just take a look
at these photos

283
00:14:04,257 --> 00:14:06,428
and make sure
that it's Fairway Frank?

284
00:14:07,343 --> 00:14:08,393
That's him.

285
00:14:09,839 --> 00:14:13,552
Mr. Campopiano, those are photos
of three different possums.

286
00:14:16,489 --> 00:14:17,939
You want something to drink?

287
00:14:20,028 --> 00:14:21,318
Boss, there's a possum

288
00:14:21,486 --> 00:14:24,738
wandering around on the 14th green.
Should I call animal control again?

289
00:14:27,349 --> 00:14:28,349
My God,

290
00:14:28,951 --> 00:14:31,578
they're here from animal control.
They're gonna take it away.

291
00:14:31,746 --> 00:14:34,585
We need a little bit more time
to figure out the truth. I have an idea.

292
00:14:34,957 --> 00:14:37,751
I'm gonna distract everybody,
then you get that possum out of here.

293
00:14:37,876 --> 00:14:39,594
Can you do that, April?

294
00:14:39,719 --> 00:14:41,588
Get the possum out of here?
Sneak it out?

295
00:14:41,982 --> 00:14:43,885
Can you do it?
Tell me you can do this.

296
00:14:44,010 --> 00:14:45,964
Yes, I can do it. God.

297
00:14:46,089 --> 00:14:48,400
I can't kill the possum
because it might be innocent.

298
00:14:48,525 --> 00:14:51,040
I can't let the possum go
because it might be guilty.

299
00:14:51,227 --> 00:14:53,553
Can't make a good soup,
can't do a handstand in a pool,

300
00:14:53,678 --> 00:14:55,878
can't spell the word "lieutenant."

301
00:14:56,003 --> 00:14:58,104
There's a lot of "can'ts"
in my life right now.

302
00:14:58,397 --> 00:15:00,232
Let's make it happen, captain.

303
00:15:07,824 --> 00:15:09,449
Help, my arm is bleeding.

304
00:15:09,617 --> 00:15:10,811
Holy cow... Leslie!

305
00:15:10,936 --> 00:15:12,869
Look how much it's bleeding.
Look at it.

306
00:15:13,037 --> 00:15:14,545
Everybody look over here.

307
00:15:14,670 --> 00:15:17,374
- I need everyone's full attention...
- There's a lot of blood.

308
00:15:17,719 --> 00:15:18,944
It is so...

309
00:15:19,069 --> 00:15:20,661
- It's ketchup.
- Is it?

310
00:15:22,873 --> 00:15:24,023
It's ketchup.

311
00:15:32,480 --> 00:15:33,830
Do you know them?

312
00:15:34,858 --> 00:15:36,017
What do you mean,

313
00:15:36,185 --> 00:15:37,686
that Fairway Frank is not here?

314
00:15:37,854 --> 00:15:39,183
Here's the thing, Evelyn.

315
00:15:39,308 --> 00:15:43,176
We're not sure that the possum
we caught is, in fact, Fairway Frank.

316
00:15:44,133 --> 00:15:46,083
Whoever it is, it's a possum.

317
00:15:46,508 --> 00:15:47,832
And the sooner it's dead,

318
00:15:47,957 --> 00:15:50,240
the sooner the mayor
can do what he wants with it.

319
00:15:51,620 --> 00:15:54,160
Does he want to have sex
with a dead possum?

320
00:15:55,913 --> 00:15:58,039
He's not a monster.
He wants

321
00:15:58,207 --> 00:16:00,986
to stuff it and hang it above the urinal
in his office bathroom

322
00:16:01,111 --> 00:16:03,378
so that little flecks of pee
can get on it forever.

323
00:16:07,133 --> 00:16:08,516
You did a great job.

324
00:16:08,860 --> 00:16:11,219
And the mayor knows
that you did a great job.

325
00:16:12,430 --> 00:16:13,930
Now where is the possum?

326
00:16:15,434 --> 00:16:17,668
I'm sorry but he's somewhere.
You'll never find it.

327
00:16:17,793 --> 00:16:20,367
And he's gonna stay there
until the truth comes out.

328
00:16:20,492 --> 00:16:23,363
And I will not reveal his location,
no matter how much you ask me.

329
00:16:23,488 --> 00:16:26,443
I'm gonna stop talking now, I'm afraid
to say where it is. So please go.

330
00:16:32,556 --> 00:16:33,450
Interesting.

331
00:16:33,618 --> 00:16:35,660
They put down a raccoon

332
00:16:35,828 --> 00:16:38,663
that bit a jogger in a park
in Staten Island.

333
00:16:39,873 --> 00:16:42,375
And in Walnut Creek, California,
they put down a duck

334
00:16:42,543 --> 00:16:43,710
that bit a kid.

335
00:16:44,138 --> 00:16:46,379
But security footage later revealed

336
00:16:46,547 --> 00:16:48,321
that it was actually a goose.

337
00:16:49,408 --> 00:16:50,216
Great.

338
00:16:50,384 --> 00:16:53,568
No, it's awful, Tom. How would you feel
if you killed an innocent duck

339
00:16:53,693 --> 00:16:56,264
and let a vicious goose waddle free?

340
00:16:57,675 --> 00:16:59,517
- Tom Haverford.
- <i>Tom, it's April.</i>

341
00:16:59,685 --> 00:17:01,580
<i>I'm at Ann's house
and the possum's loose.</i>

342
00:17:01,705 --> 00:17:03,700
<i>I need your help.
It's chewing on everything!</i>

343
00:17:03,825 --> 00:17:05,724
- <i>Don't tell Leslie!</i>
- You got it.

344
00:17:07,439 --> 00:17:09,130
Possum got loose at Ann's house.

345
00:17:09,255 --> 00:17:10,255
My God.

346
00:17:11,680 --> 00:17:14,282
April, it's Leslie Knope
from the Parks department!

347
00:17:14,450 --> 00:17:16,546
Stop ringing the doorbell.
It's making him mad!

348
00:17:18,407 --> 00:17:19,996
My God, how did this happen?

349
00:17:20,623 --> 00:17:22,896
I let it out of its cage
because it needed water

350
00:17:23,021 --> 00:17:25,354
and I thought it would drink out
of the toilet, but it ran off

351
00:17:25,479 --> 00:17:26,999
and I couldn't get it back in.

352
00:17:28,249 --> 00:17:29,549
He did that too.

353
00:17:31,008 --> 00:17:33,529
Then I opened all the doors,
I thought it would go outside,

354
00:17:33,654 --> 00:17:36,711
but it won't leave the house.
It must love tacky pictures of flowers.

355
00:17:36,836 --> 00:17:37,973
I'm gonna fix this.

356
00:17:38,141 --> 00:17:39,975
Let me just think for a second.

357
00:17:40,698 --> 00:17:42,639
- What?
- Where is it? I can't see it!

358
00:17:43,563 --> 00:17:45,511
Where did it go?
Here's what we do.

359
00:17:45,636 --> 00:17:47,649
We just go outside.
Let's go outside...

360
00:17:47,953 --> 00:17:48,900
Get in here!

361
00:18:00,402 --> 00:18:03,081
- I haven't even started yet.
- I know.

362
00:18:04,252 --> 00:18:05,542
You could use some help.

363
00:18:05,995 --> 00:18:08,912
Those city planning guys
can be real pains in the ass.

364
00:18:12,880 --> 00:18:15,721
I just want you to know
that I still don't think city codes...

365
00:18:15,846 --> 00:18:17,896
Ron, shut up, would you please?

366
00:18:18,774 --> 00:18:20,263
I'm taking a half day off

367
00:18:20,388 --> 00:18:22,559
to come and help you out
because you're my friend.

368
00:18:22,727 --> 00:18:23,893
So just...

369
00:18:24,552 --> 00:18:25,552
shut up.

370
00:18:27,141 --> 00:18:28,648
I'm bringing my workshop up

371
00:18:28,816 --> 00:18:30,074
to the Swanson code.

372
00:18:30,314 --> 00:18:33,495
And if the Swanson code
happens to overlap

373
00:18:33,620 --> 00:18:35,720
with the city government code...

374
00:18:35,845 --> 00:18:36,845
Shut up!

375
00:18:38,618 --> 00:18:40,903
I know there's a door there,
but I kind of feel like

376
00:18:41,028 --> 00:18:44,355
it's gonna chew through the door
and jump on my face and bite me.

377
00:18:44,480 --> 00:18:47,198
- What if it's in here?
- What if it laid eggs in the bed?

378
00:18:48,291 --> 00:18:49,461
There are no eggs.

379
00:18:49,629 --> 00:18:51,546
We should have killed it,
it's so huge.

380
00:18:51,714 --> 00:18:53,111
I'm gonna call Andy.

381
00:18:53,591 --> 00:18:55,577
He tackled it once before.
He can do it again.

382
00:18:55,702 --> 00:18:58,011
No, please don't, okay?
He's gonna be pissed at me.

383
00:18:58,179 --> 00:18:59,823
I already ruined his big day,

384
00:18:59,948 --> 00:19:02,557
it's my fault that it's in Ann's house,
who he's still in love with.

385
00:19:03,878 --> 00:19:04,878
I just...

386
00:19:05,269 --> 00:19:06,269
I don't...

387
00:19:06,956 --> 00:19:08,730
want him to be mad at me, okay?

388
00:19:13,163 --> 00:19:14,319
He'll forgive you.

389
00:19:14,822 --> 00:19:15,653
You think?

390
00:19:17,230 --> 00:19:18,490
We don't call Andy.

391
00:19:19,102 --> 00:19:21,036
We can just call animal control.

392
00:19:24,671 --> 00:19:26,831
Anytime you want to talk
about boys...

393
00:19:26,999 --> 00:19:28,308
My God, stop.

394
00:19:50,632 --> 00:19:53,489
So you were mad at me yesterday,
and I don't know why.

395
00:19:53,614 --> 00:19:55,347
So I made a list of everything I did

396
00:19:55,472 --> 00:19:57,362
and I'll try not to do
any of them again.

397
00:19:58,117 --> 00:19:58,863
Also,

398
00:19:59,850 --> 00:20:01,324
I got you this coffee.

399
00:20:03,327 --> 00:20:05,453
And then there's this.
Fourth paragraph down.

400
00:20:06,214 --> 00:20:08,248
"But Dwyer had some help
catching the possum.

401
00:20:08,416 --> 00:20:10,917
"I couldn't do it
without an early morning caffeine boost

402
00:20:11,085 --> 00:20:12,799
"from the amazing April Ludgate.

403
00:20:12,924 --> 00:20:15,880
"She gave me the liquid courage
to wrestle that beast to the ground."

404
00:20:19,844 --> 00:20:21,594
I have to say I'm very disappointed.

405
00:20:21,900 --> 00:20:24,264
You didn't have to say that.
You'd have just thought it.

406
00:20:24,432 --> 00:20:26,057
What'd you do with the possum?

407
00:20:27,092 --> 00:20:28,431
I'm proud to say

408
00:20:28,556 --> 00:20:31,606
that it's somewhere
the mayor can never pee on it.

409
00:20:31,731 --> 00:20:32,730
It's okay.

410
00:20:33,136 --> 00:20:34,065
I mean, look,

411
00:20:34,618 --> 00:20:35,650
when I retire,

412
00:20:35,818 --> 00:20:37,277
and I'm attending some gala

413
00:20:38,297 --> 00:20:41,166
honoring the first three female
presidents in history,

414
00:20:41,291 --> 00:20:43,941
myself, and two other women
I've inspired,

415
00:20:44,066 --> 00:20:45,368
I wanna be looking back

416
00:20:45,536 --> 00:20:47,082
at my distinguished legacy,

417
00:20:47,207 --> 00:20:49,914
and not thinking that I owe my career
to some possum.

418
00:20:50,742 --> 00:20:53,334
And I wanna be wearing a huge,
beautiful blue hat.

419
00:20:54,593 --> 00:20:56,485
- Thank you so much.
- Sure.

420
00:20:56,610 --> 00:20:59,632
It made me feel so happy that I knew
you were here, taking care of my house.

421
00:20:59,800 --> 00:21:00,592
Cool.

422
00:21:00,760 --> 00:21:02,942
Did the neighbors
give you any problems?

423
00:21:03,067 --> 00:21:05,180
- Anything in...
- Possum. There was a possum.

424
00:21:05,348 --> 00:21:07,209
We captured a possum,
we brought it here,

425
00:21:07,334 --> 00:21:09,884
- it might have laid eggs in your bed.
- What?

426
00:21:10,102 --> 00:21:12,645
It went into your laundry, your kitchen,
and it touched all your bras.

427
00:21:12,813 --> 00:21:14,480
And I'm so sorry, it's our fault,

428
00:21:14,648 --> 00:21:17,126
we captured it, it got out, ran around
and it was a possum.

429
00:21:17,251 --> 00:21:19,519
Run, April!
Sorry, Ann! I love you!

430
00:21:22,468 --> 00:21:24,949
- I'm gonna go.
- I think that would be best.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
