﻿1
00:00:00,000 --> 00:00:02,244
Now, after we are given the certificate,

2
00:00:02,364 --> 00:00:04,252
who do you think
should speak first?

3
00:00:04,372 --> 00:00:06,060
I think it should be me
and then you.

4
00:00:06,180 --> 00:00:08,233
But, if you want,
it could be you and then me.

5
00:00:08,353 --> 00:00:10,059
Or it could go me, you, me.

6
00:00:10,110 --> 00:00:12,028
- What do you think?
- How about just you?

7
00:00:12,096 --> 00:00:13,196
Thank you, Ron. Yes.

8
00:00:13,247 --> 00:00:14,831
Tomorrow,
the Parks and Rec Department

9
00:00:14,899 --> 00:00:18,368
is receiving a commendation
at the Indiana Statehouse

10
00:00:18,435 --> 00:00:20,470
for bringing
the harvest festival back.

11
00:00:20,538 --> 00:00:22,772
And although
it's purely ceremonial,

12
00:00:22,840 --> 00:00:25,074
it's a huge deal for me
to go to the Statehouse.

13
00:00:25,125 --> 00:00:28,077
I couldn't care less
about the commendation.

14
00:00:28,145 --> 00:00:32,882
But Indianapolis is home to
Charles Mulligan's steakhouse,

15
00:00:32,933 --> 00:00:35,618
the best damn steakhouse
in the damn state.

16
00:00:35,686 --> 00:00:39,122
I have taken a picture of every
steak I've ever eaten there.

17
00:00:39,190 --> 00:00:40,590
June 2004.

18
00:00:40,658 --> 00:00:43,726
Porterhouse, medium rare,
bearnaise sauce.

19
00:00:43,794 --> 00:00:45,195
January 2000.

20
00:00:45,262 --> 00:00:47,030
They call this one
"the enforcer."

21
00:00:47,097 --> 00:00:48,698
February '96.

22
00:00:48,766 --> 00:00:52,535
The steak: Rib-eye.
The whiskey: Lagavulin 16.

23
00:00:52,603 --> 00:00:55,305
The lady next to me:
A bitch.

24
00:00:55,372 --> 00:00:57,407
Specifically,
my ex-wife Tammy.

25
00:00:57,474 --> 00:00:59,459
Okay, this is...
this is the first time

26
00:00:59,543 --> 00:01:00,944
I ever went there.

27
00:01:01,011 --> 00:01:04,981
Oh, look at me.
I'm just a kid.

28
00:01:08,052 --> 00:01:17,975
<font color="#ec14bd">Sync by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

29
00:01:25,152 --> 00:01:26,802
She's showing
some elephant pic...

30
00:01:26,854 --> 00:01:29,906
Oh, la la!
Look who's fancy!

31
00:01:29,973 --> 00:01:33,142
Oh, am I wearing an ascot?
I didn't notice.

32
00:01:33,193 --> 00:01:35,411
Big night
at the Snakehole Lounge.

33
00:01:35,479 --> 00:01:36,913
It's a launch party

34
00:01:36,980 --> 00:01:40,500
for Dennis Feinstein's new fragrance:
Allergic-For men.

35
00:01:40,584 --> 00:01:43,169
Pawnee's own Dennis Feinstein
is a real up-and-comer

36
00:01:43,254 --> 00:01:45,505
in the world of microbrewed
perfumes and body sprays.

37
00:01:45,589 --> 00:01:47,874
His ether-based perfume
"Blackout"

38
00:01:47,958 --> 00:01:50,059
was named one
<i>Maxim's</i> top 100 ways

39
00:01:50,127 --> 00:01:51,894
to trick someone into sex.

40
00:01:51,962 --> 00:01:54,397
The club opens at 6:00,
event starts at 9:00.

41
00:01:54,465 --> 00:01:55,515
Oh, no, no, way too late.

42
00:01:55,599 --> 00:01:57,216
I will be deep
into my bath by then.

43
00:01:57,301 --> 00:02:00,836
Don't let us picture that.
Ben, you gonna go?

44
00:02:00,888 --> 00:02:02,889
Oh, uh, I don't think so.

45
00:02:02,973 --> 00:02:04,240
Probably just gonna take it easy,

46
00:02:04,308 --> 00:02:05,725
but thank you.

47
00:02:05,809 --> 00:02:07,110
You know, they always ask me
to go out with them.

48
00:02:07,177 --> 00:02:08,394
They're just being polite.

49
00:02:08,479 --> 00:02:09,612
I move around a lot,

50
00:02:09,680 --> 00:02:11,030
so the friends I make
in these cities,

51
00:02:11,115 --> 00:02:13,950
they're like
Facebook friends, you know?

52
00:02:14,018 --> 00:02:18,854
"Hey, Doug from Bloomington is
thinking about buying a shirt."

53
00:02:18,906 --> 00:02:21,190
I mean, come on, Doug, who cares?

54
00:02:21,241 --> 00:02:23,092
Okay, Tom, I want you to take Ben

55
00:02:23,160 --> 00:02:25,078
and make him go
to that antihistamine party.

56
00:02:25,162 --> 00:02:26,862
It's "Allergic," and forget it.

57
00:02:26,914 --> 00:02:28,231
He doesn't know anybody in town.

58
00:02:28,299 --> 00:02:31,067
Come on, Tom, take him under
your tiny little wing.

59
00:02:31,135 --> 00:02:32,802
He's a fully grown man, Leslie.

60
00:02:32,870 --> 00:02:34,387
And tonight's just not
about pleasure.

61
00:02:34,471 --> 00:02:36,389
I'm schmoozing Dennis Feinstein

62
00:02:36,473 --> 00:02:39,041
so I can pitch him
my new cologne, "Tommy Fresh."

63
00:02:39,093 --> 00:02:40,543
Okay, well,
while you're doing that,

64
00:02:40,611 --> 00:02:42,545
I want you to picture Ben
in his motel room,

65
00:02:42,613 --> 00:02:44,714
all by himself with no friends,
staring at the wall.

66
00:02:44,765 --> 00:02:45,815
- Deal.
- Tom.

67
00:02:47,017 --> 00:02:48,651
So my band's working
on a new album.

68
00:02:48,719 --> 00:02:50,103
Oh, yeah?
What's it called?

69
00:02:50,187 --> 00:02:54,457
<i>April Ludgate Is</i>
<i>the Best Ever, Volume One.</i>

70
00:02:54,525 --> 00:02:56,426
Shut up.

71
00:02:56,493 --> 00:02:57,727
So, uh, what do you
want to do tonight?

72
00:02:57,778 --> 00:02:59,462
We could watch TV
at Burly's house

73
00:02:59,530 --> 00:03:01,698
or we could watch TV
at your house.

74
00:03:01,765 --> 00:03:03,583
Or, I mean, we could
watch TV at best buy.

75
00:03:03,667 --> 00:03:05,335
Why don't we go to Tom's thing?

76
00:03:05,402 --> 00:03:08,237
That weird cologne party thing
at the Snakehole?

77
00:03:08,305 --> 00:03:09,305
Oh.

78
00:03:09,373 --> 00:03:11,341
I kind of forgot
that you need money

79
00:03:11,408 --> 00:03:12,908
when you have a girlfriend.

80
00:03:12,960 --> 00:03:14,310
Eh!

81
00:03:14,378 --> 00:03:16,179
I want to treat April
like a queen,

82
00:03:16,246 --> 00:03:22,285
and queens deserve flowers
and massages,

83
00:03:22,353 --> 00:03:24,804
chocolate, booze,

84
00:03:24,888 --> 00:03:26,656
diamonds, rubies, emeralds,

85
00:03:26,724 --> 00:03:29,359
them treasure chests
full of scarves,

86
00:03:29,426 --> 00:03:31,260
different kinds of lubes
that warm up

87
00:03:31,311 --> 00:03:32,695
when you rub 'em on stuff.

88
00:03:32,763 --> 00:03:35,448
I'm gonna give her
all that stuff and more.

89
00:03:35,532 --> 00:03:38,434
Hmm, that sounds pretty awesome.
Yeah, let's do it.

90
00:03:38,502 --> 00:03:39,435
- Okay.
- Okay.

91
00:03:39,486 --> 00:03:41,437
- All right. Bye.
- Bye.

92
00:03:41,488 --> 00:03:44,807
I don't know.
I guess we're dating.

93
00:03:44,875 --> 00:03:50,446
It's new. Whatever.
I don't like labels.

94
00:03:50,497 --> 00:03:52,548
Go away.

95
00:03:52,616 --> 00:03:55,168
- This?
- That would be good.

96
00:03:55,252 --> 00:03:56,836
But does it say,
"Hello, general assembly,

97
00:03:56,920 --> 00:03:59,522
I've come to Indianapolis
to accept your commendation"?

98
00:03:59,590 --> 00:04:01,057
As much as any one dress could.

99
00:04:01,124 --> 00:04:02,225
Hmm.

100
00:04:02,292 --> 00:04:03,643
Leslie, I think Chris
is cheating on me.

101
00:04:03,727 --> 00:04:06,062
What?
That lying bastard.

102
00:04:06,129 --> 00:04:07,129
Wait.
How do you know?

103
00:04:07,181 --> 00:04:09,732
I don't have any actual proof.

104
00:04:09,800 --> 00:04:11,184
Oh, then I'm sure
he's not cheating on you.

105
00:04:11,268 --> 00:04:13,986
And if he is, he's a monster.

106
00:04:14,071 --> 00:04:16,072
And if he's not,
you guys are great together.

107
00:04:16,140 --> 00:04:17,473
But if he is, I will kill him.

108
00:04:17,524 --> 00:04:19,342
Well, we had
a really good conversation

109
00:04:19,410 --> 00:04:20,943
about our relationship last week,

110
00:04:21,011 --> 00:04:22,812
and he was very reassuring

111
00:04:22,880 --> 00:04:24,163
that we're headed
in a good direction.

112
00:04:24,248 --> 00:04:25,248
Great.

113
00:04:25,315 --> 00:04:27,183
- But then he got distant.
- Oh.

114
00:04:27,251 --> 00:04:29,118
And when I asked to come
visit him in Indianapolis,

115
00:04:29,186 --> 00:04:31,187
he was totally weird.

116
00:04:31,255 --> 00:04:33,689
He was weird.

117
00:04:33,757 --> 00:04:34,924
Look, it sounds like
you're just spiraling.

118
00:04:34,992 --> 00:04:36,225
But what if I'm not?
What if I'm not?

119
00:04:36,293 --> 00:04:38,294
What if he has a girl up there?

120
00:04:38,345 --> 00:04:40,162
I think he's cheating on me.

121
00:04:40,214 --> 00:04:41,431
Wow. Okay.

122
00:04:41,498 --> 00:04:43,566
Well, luckily,
I'm heading up there.

123
00:04:43,634 --> 00:04:45,134
I'll... I'll invite him
out to dinner

124
00:04:45,202 --> 00:04:46,269
and I'll poke around
a little bit.

125
00:04:46,336 --> 00:04:48,170
Okay.

126
00:04:48,222 --> 00:04:49,722
I mean, he's not gonna be able
to keep anything from me.

127
00:04:49,807 --> 00:04:52,442
In high school, they used to
call me Angela Lansbury.

128
00:04:52,509 --> 00:04:55,278
But that was because
of my haircut.

129
00:04:55,345 --> 00:04:56,562
Hey, champ.

130
00:04:56,647 --> 00:04:58,915
Hey, champ...ion.

131
00:04:58,982 --> 00:05:00,917
Listen, you should
come out tonight.

132
00:05:00,984 --> 00:05:02,418
The club's gonna be dope.

133
00:05:02,486 --> 00:05:04,036
Plus, I'm pretty sure you
have nothing else going on.

134
00:05:04,121 --> 00:05:05,588
Well, that's not totally true.

135
00:05:05,656 --> 00:05:07,523
The owner of the motel
I'm staying at

136
00:05:07,591 --> 00:05:10,042
said she was gonna screen
<i>Hope Floats</i> in the lobby.

137
00:05:10,127 --> 00:05:11,561
Asked me if I wanted to watch.

138
00:05:11,628 --> 00:05:13,930
Yeah. You should
probably get out of that.

139
00:05:13,997 --> 00:05:15,131
I think she's gonna murder you.

140
00:05:15,199 --> 00:05:16,499
Come on down to the Snakehole.

141
00:05:16,550 --> 00:05:18,468
I'm not gonna take no
for an answer.

142
00:05:18,535 --> 00:05:21,087
Okay. Yeah, no,
I'll... I'll stop by.

143
00:05:21,171 --> 00:05:22,088
- Thank you.
- Cool.

144
00:05:22,172 --> 00:05:23,239
Hey, you mind if I ask?

145
00:05:23,307 --> 00:05:24,223
What are you gonna wear tonight?

146
00:05:24,308 --> 00:05:25,475
Uh, you know,

147
00:05:25,542 --> 00:05:27,944
I was probably just
gonna stick with this.

148
00:05:28,011 --> 00:05:29,846
Come on, seriously.

149
00:05:29,913 --> 00:05:30,980
What are you...
What are you gonna wear?

150
00:05:31,048 --> 00:05:32,047
Oh, something totally different.

151
00:05:32,099 --> 00:05:33,316
Probably go home
and grab something.

152
00:05:33,383 --> 00:05:35,050
Cool.

153
00:05:36,520 --> 00:05:38,905
If you would be willing
to just take a brief pit stop,

154
00:05:38,989 --> 00:05:43,192
we could see Indiana's
second largest rocking chair.

155
00:05:43,260 --> 00:05:44,777
No.

156
00:05:44,862 --> 00:05:47,563
Then maybe we should take
a quick two-hour sojourn

157
00:05:47,614 --> 00:05:51,033
to Dame Gervin's Misshapen
Celebrity Castle.

158
00:05:51,084 --> 00:05:54,270
It's where Madame Tussaud sends
all of its failed wax figures,

159
00:05:54,338 --> 00:05:55,922
and if you can figure out
who it is,

160
00:05:56,006 --> 00:05:58,174
- you get to take it home.
- No.

161
00:05:58,242 --> 00:05:59,709
What's wrong with you,
grouchy pants?

162
00:05:59,760 --> 00:06:03,045
I've been fasting all day
to save room for Mulligan's.

163
00:06:03,096 --> 00:06:04,797
Fasting's not healthy
for you, Ron.

164
00:06:04,882 --> 00:06:06,215
Leslie, you need to understand,

165
00:06:06,283 --> 00:06:09,051
we are heading for the most
special place on earth.

166
00:06:09,102 --> 00:06:10,820
When I'm done eating
a Mulligan's meal,

167
00:06:10,888 --> 00:06:13,356
for weeks afterwards,
there are flecks of meat

168
00:06:13,423 --> 00:06:16,025
in my mustache,
and I refuse to clean it

169
00:06:16,093 --> 00:06:17,226
because every now and then,

170
00:06:17,294 --> 00:06:19,478
a piece of meat
will fall into my mouth.

171
00:06:19,563 --> 00:06:20,813
Oh, I forgot to tell you,

172
00:06:20,898 --> 00:06:22,532
Chris Traeger is joining us
for dinner tonight.

173
00:06:22,599 --> 00:06:24,116
Please tell me he's
meeting us at the restaurant.

174
00:06:24,201 --> 00:06:25,117
No.
We are picking him up.

175
00:06:25,202 --> 00:06:26,669
Damn it, woman!

176
00:06:28,238 --> 00:06:30,640
Welcome. I am so glad
you guys called.

177
00:06:30,707 --> 00:06:31,991
This is gonna be great.
Come on in, come on in.

178
00:06:32,075 --> 00:06:33,409
- Sure.
- Or we could just...

179
00:06:33,477 --> 00:06:36,796
Wow!
Look at your foyer.

180
00:06:36,880 --> 00:06:39,148
- It's like a spa in here.
- Exactly.

181
00:06:39,216 --> 00:06:40,716
The entranceway to your house

182
00:06:40,784 --> 00:06:42,385
sends a message to the world
about who you are.

183
00:06:42,452 --> 00:06:45,621
And the front door provides
energy flow and opportunity.

184
00:06:45,689 --> 00:06:46,973
You cannot limit that.

185
00:06:47,057 --> 00:06:48,841
Yeah. Ron doesn't even
have a front door.

186
00:06:48,926 --> 00:06:50,192
He won't even tell me
his address.

187
00:06:50,260 --> 00:06:52,562
- Ask him where he lives.
- Where do you live?

188
00:06:52,629 --> 00:06:54,030
Why?

189
00:06:54,097 --> 00:06:57,934
So this is some kind of
coat closet or something?

190
00:06:57,985 --> 00:07:01,303
Ah! Wow. There's a lot
of men's coats in here.

191
00:07:01,355 --> 00:07:02,705
I like coats.

192
00:07:02,773 --> 00:07:05,024
Could I use your bathroom
really quick?

193
00:07:05,108 --> 00:07:06,659
- It's right behind you.
- Great.

194
00:07:06,743 --> 00:07:09,495
Oh, the toilet seat's up.

195
00:07:12,115 --> 00:07:16,168
Hey, let me get a beer
and a Pawnee sunrise.

196
00:07:16,253 --> 00:07:17,954
Do you want to pay cash
or start a tab?

197
00:07:18,021 --> 00:07:20,590
I... it's o... Uh, I-I know Tom.

198
00:07:20,657 --> 00:07:23,259
- Tom who?
- Tom Hammen... Hammenstein.

199
00:07:23,326 --> 00:07:24,460
- Haverford.
- Haverford.

200
00:07:24,511 --> 00:07:26,012
He's, like, my best friend.

201
00:07:26,096 --> 00:07:28,764
Hey, Freddy, this guy
says he knows Tom

202
00:07:28,832 --> 00:07:30,049
and wants a drink for free.

203
00:07:30,133 --> 00:07:32,001
You, uh, you have money
to pay for those drinks?

204
00:07:32,069 --> 00:07:34,470
Yes.

205
00:07:34,538 --> 00:07:35,905
No.

206
00:07:35,973 --> 00:07:38,040
Make room for
the paying customers, huh?

207
00:07:41,612 --> 00:07:42,545
Let's go, Knope!

208
00:07:42,613 --> 00:07:44,046
Just one second.

209
00:07:44,114 --> 00:07:45,147
Hey, what's up?

210
00:07:45,215 --> 00:07:47,049
Hey, I think you might be right.

211
00:07:47,117 --> 00:07:50,519
I found a women's razor
in Chris' shower

212
00:07:50,570 --> 00:07:52,088
and a pink swimming cap.

213
00:07:52,155 --> 00:07:53,589
Are you serious?
What the hell?

214
00:07:53,657 --> 00:07:55,374
You know, it could be nothing,

215
00:07:55,459 --> 00:07:57,126
but it feels like something.
I don't...

216
00:07:57,194 --> 00:07:58,577
I'm coming up there.

217
00:07:58,662 --> 00:07:59,862
Wait.

218
00:08:00,964 --> 00:08:02,198
All right, okay.

219
00:08:02,265 --> 00:08:04,667
All right, let's go.

220
00:08:06,299 --> 00:08:08,074
Oh my God.

221
00:08:08,194 --> 00:08:10,262
They... they just forgot to
unlock the door, that's all.

222
00:08:11,813 --> 00:08:13,831
Ron, it looks like they're closed

223
00:08:13,915 --> 00:08:15,783
for some kind of
health code violation.

224
00:08:17,419 --> 00:08:19,653
- Aah!
- Ron, stop it! Ron?

225
00:08:21,957 --> 00:08:28,212
They just boarded her up like
she was some common warehouse.

226
00:08:28,296 --> 00:08:30,564
I should have been here.

227
00:08:30,632 --> 00:08:31,799
What happened to the steaks

228
00:08:31,867 --> 00:08:34,935
that were in there
when they closed?

229
00:08:35,003 --> 00:08:36,871
Do you think they got eaten?

230
00:08:36,938 --> 00:08:39,156
Why don't we go back to my place

231
00:08:39,224 --> 00:08:41,275
and I'll fire up the grill?

232
00:08:41,343 --> 00:08:43,844
You sure you're not expecting
company or something?

233
00:08:43,912 --> 00:08:45,546
No.

234
00:08:45,614 --> 00:08:46,730
What do you say, Ron?

235
00:08:46,815 --> 00:08:48,215
Go back to my place
and make you some grub?

236
00:08:48,283 --> 00:08:52,119
It'll be just as good as it
would have been here.

237
00:08:52,187 --> 00:08:53,571
- Okay.
- Okay.

238
00:08:53,655 --> 00:08:57,525
That man is a legend.

239
00:08:57,592 --> 00:08:59,093
Think about all the scents
he's created.

240
00:08:59,160 --> 00:09:04,215
Attack, yearning, thickening,
itch, coma, sideboob.

241
00:09:04,299 --> 00:09:06,400
Dennis Feinstein, though?
I don't know.

242
00:09:06,468 --> 00:09:07,635
He should probably
change his name

243
00:09:07,702 --> 00:09:08,969
to something a little more exotic

244
00:09:09,037 --> 00:09:11,555
if he wants to make it
big in perfume.

245
00:09:11,640 --> 00:09:13,541
Oh, his real name is Dante Fiero,

246
00:09:13,608 --> 00:09:15,175
but he changed it
to Dennis Feinstein

247
00:09:15,243 --> 00:09:16,810
'cause that's way
more exotic in Pawnee.

248
00:09:16,878 --> 00:09:18,879
Okay.
So what's your plan?

249
00:09:18,947 --> 00:09:22,733
Wait till he's alone,
then I walk up and spray him

250
00:09:22,817 --> 00:09:26,020
with Tommy Fresh,
and I say, "Uh-oh.

251
00:09:26,087 --> 00:09:27,855
Looks like you just
nailed your future."

252
00:09:27,923 --> 00:09:33,360
Yeah. That might
actually work.

253
00:09:33,428 --> 00:09:34,962
Is everything okay?

254
00:09:35,030 --> 00:09:39,300
Um, it kind of sucks
that I'm super broke

255
00:09:39,367 --> 00:09:40,868
and I want to buy you stuff

256
00:09:40,919 --> 00:09:43,370
and it's embarrassing
that I can't, but...

257
00:09:43,438 --> 00:09:45,873
I'm broke too,
and I don't want anything.

258
00:09:45,941 --> 00:09:47,575
I just want to hang out with you.

259
00:09:47,642 --> 00:09:49,793
Plus, I can get free drinks
anytime I want.

260
00:09:49,878 --> 00:09:51,045
How?

261
00:09:51,112 --> 00:09:54,348
Uh, I'm a girl in a sleazy club.

262
00:09:55,951 --> 00:09:56,951
Hey.

263
00:09:57,018 --> 00:09:58,118
Hey.

264
00:09:58,186 --> 00:09:59,653
I hate drinking alone.

265
00:09:59,721 --> 00:10:00,721
Can I get you a drink?

266
00:10:00,789 --> 00:10:02,623
Sure.
Triple whiskey.

267
00:10:02,691 --> 00:10:03,991
What's your name?

268
00:10:04,059 --> 00:10:06,060
- Oprah.
- I'm Kevin.

269
00:10:06,127 --> 00:10:08,829
Cool. I kind of
want to drink alone.

270
00:10:08,897 --> 00:10:10,114
- But...
- I said I want to drink alone.

271
00:10:10,198 --> 00:10:12,032
Thanks. Bye.

272
00:10:12,100 --> 00:10:13,701
Here. You take this one.

273
00:10:13,768 --> 00:10:19,640
I will, um, get myself
a Martini from that idiot.

274
00:10:19,708 --> 00:10:23,377
So, Chris, do you
have any sisters?

275
00:10:23,445 --> 00:10:25,212
No, I don't, Leslie.
Do you have sisters?

276
00:10:25,280 --> 00:10:27,848
Maybe. So how's your mom?
Is she visiting?

277
00:10:27,916 --> 00:10:29,183
No, she's home up in Wisconsin.

278
00:10:29,250 --> 00:10:30,684
- Is your mom visiting?
- Any aunts?

279
00:10:30,752 --> 00:10:32,419
- Nope. You have aunts?
- Girl cousins?

280
00:10:32,470 --> 00:10:34,805
- A youthful grandmother, perhaps?
- Nope.

281
00:10:34,889 --> 00:10:37,474
Did you forget
how to have a conversation?

282
00:10:37,559 --> 00:10:38,759
I am so happy you guys are here.

283
00:10:38,810 --> 00:10:41,562
I'm gonna go fire up the grill.

284
00:10:41,630 --> 00:10:43,964
- I'm gonna kill him, Ron.
- Why?

285
00:10:44,032 --> 00:10:47,201
Chris is cheating on Ann.
There's evidence everywhere.

286
00:10:47,268 --> 00:10:49,403
She's coming up here
so they can have it out.

287
00:10:49,470 --> 00:10:52,373
Ask her to bring
some garlic salt.

288
00:10:52,440 --> 00:10:55,776
I'm worried
Chris doesn't have any.

289
00:10:57,412 --> 00:10:59,363
I got this from a waiter.

290
00:10:59,447 --> 00:11:01,281
I told him I had
a pork deficiency.

291
00:11:01,333 --> 00:11:02,700
Sweet!
Pigs in a blanket.

292
00:11:02,784 --> 00:11:04,702
I always wondered
why they call them that.

293
00:11:04,786 --> 00:11:05,753
Look what I got.

294
00:11:05,820 --> 00:11:06,954
Look what I got
from the bathroom.

295
00:11:07,022 --> 00:11:09,923
Mints. Six of them.

296
00:11:09,991 --> 00:11:11,508
- Nice.
- Yeah.

297
00:11:11,593 --> 00:11:15,179
Okay, whoever gets
the most free stuff

298
00:11:15,263 --> 00:11:16,430
by the end of the night wins.

299
00:11:16,497 --> 00:11:18,832
Deal.

300
00:11:18,883 --> 00:11:20,768
Go!

301
00:11:20,835 --> 00:11:21,969
Ron, would you like some salad?

302
00:11:22,020 --> 00:11:25,639
Since I am not a rabbit,
no, I do not.

303
00:11:25,707 --> 00:11:27,057
Try it.
Salad's good for you.

304
00:11:27,142 --> 00:11:29,676
You got it.

305
00:11:29,728 --> 00:11:31,812
Mmm, delicious.

306
00:11:31,863 --> 00:11:33,647
So, Chris, what do you do up here

307
00:11:33,698 --> 00:11:34,848
in your spare time?

308
00:11:34,899 --> 00:11:38,351
Well, uh, I exercise,
and I exercise my mind.

309
00:11:38,403 --> 00:11:40,821
And I try to keep up
on current events.

310
00:11:40,889 --> 00:11:43,290
- Oh, that's what you call it.
- Sorry?

311
00:11:43,358 --> 00:11:44,291
How are things going with Ann?

312
00:11:44,359 --> 00:11:45,793
You know what's funny about Ann?

313
00:11:45,860 --> 00:11:46,994
She's my best friend,

314
00:11:47,062 --> 00:11:48,128
and anyone who'd hurt her

315
00:11:48,196 --> 00:11:50,030
is someone I would murder
probably.

316
00:11:50,098 --> 00:11:52,733
Oh, is that what's upsetting you?

317
00:11:52,801 --> 00:11:53,867
Hmm?

318
00:11:53,935 --> 00:11:55,235
This is very
uncomfortable for me.

319
00:11:55,303 --> 00:11:57,971
I don't know what to say.

320
00:11:58,039 --> 00:12:00,340
Watch the master work it.

321
00:12:00,392 --> 00:12:01,675
I'm the Yoda of networking.

322
00:12:01,726 --> 00:12:04,061
Well, Yoda wouldn't
actually need networking.

323
00:12:04,145 --> 00:12:05,245
I mean, his powers
were more spiritual.

324
00:12:05,313 --> 00:12:08,048
- Shut up, you nerd!
- I get it. Okay.

325
00:12:10,652 --> 00:12:13,620
Mr. Feinstein,
Tom Haverford.

326
00:12:13,688 --> 00:12:15,939
I'm the organizer of this soiree

327
00:12:16,024 --> 00:12:17,725
and I'm also a huge fan of yours.

328
00:12:17,792 --> 00:12:19,827
I use all your cologne,
sometimes two at once.

329
00:12:19,894 --> 00:12:22,362
I don't recommend that.
Are you from the FDA?

330
00:12:22,414 --> 00:12:24,631
You know, legally,
if you're from the FDA,

331
00:12:24,699 --> 00:12:25,666
you have to tell me.

332
00:12:25,734 --> 00:12:27,418
I wanted to talk to you

333
00:12:27,502 --> 00:12:29,870
because I actually created
my own scent.

334
00:12:29,938 --> 00:12:33,307
Oh, great. I'd love
to smell it, right?

335
00:12:34,642 --> 00:12:36,510
- I know you're a busy guy.
- Yeah, man.

336
00:12:36,578 --> 00:12:37,945
I'm crazy busy.

337
00:12:38,012 --> 00:12:40,798
But all day long, people
are pitching me colognes.

338
00:12:40,882 --> 00:12:42,950
That guy just pitched me
a cologne called "Sluts."

339
00:12:43,017 --> 00:12:44,318
I'm gonna tell you
what I told him.

340
00:12:44,385 --> 00:12:45,819
Not just anybody can do this.

341
00:12:45,887 --> 00:12:48,021
Although it's a great name
and I'm probably gonna steal it.

342
00:12:48,089 --> 00:12:49,757
Please, it'll take two seconds.

343
00:12:49,824 --> 00:12:51,925
If you like it, maybe we
go into business together,

344
00:12:51,993 --> 00:12:53,360
branch out in other states,

345
00:12:53,428 --> 00:12:55,329
move over to watches
and fancy raincoats,

346
00:12:55,396 --> 00:12:56,763
open up stores
in London and Paris.

347
00:12:56,815 --> 00:12:58,532
I don't know.
I haven't thought it through.

348
00:12:58,600 --> 00:12:59,700
But for now,

349
00:12:59,768 --> 00:13:03,937
I want to present Tommy Fresh.

350
00:13:07,675 --> 00:13:09,543
All right, I get it.

351
00:13:09,610 --> 00:13:12,079
It's a gag scent.
Hilarious.

352
00:13:12,147 --> 00:13:15,516
As in, "It's hilarious
how awesome it is"?

353
00:13:15,583 --> 00:13:16,583
It smells like somebody

354
00:13:16,651 --> 00:13:18,685
spilled Chinese food
in a bird cage.

355
00:13:18,753 --> 00:13:20,888
Horrible.
It's assaultive.

356
00:13:20,955 --> 00:13:23,924
It takes everything in my power
to not retch right now.

357
00:13:23,992 --> 00:13:26,460
Kid, you need to
find another game.

358
00:13:26,528 --> 00:13:29,596
Leave perfumery to the real men.

359
00:13:32,534 --> 00:13:33,901
Hey, Ann, if you get this,

360
00:13:33,968 --> 00:13:35,235
I don't know if you
should come up here.

361
00:13:35,303 --> 00:13:36,636
He's not worth it, okay?

362
00:13:36,688 --> 00:13:38,438
Just call me,
let me know where you are,

363
00:13:38,490 --> 00:13:40,574
- and if you have...
- Aah!

364
00:13:40,641 --> 00:13:41,708
Ron?

365
00:13:41,776 --> 00:13:43,810
What in the devil's name is this?

366
00:13:43,862 --> 00:13:45,078
Portobello mushrooms.

367
00:13:45,146 --> 00:13:47,414
- Where's the steak?
- Oh, there's no steak.

368
00:13:47,482 --> 00:13:49,583
That's a healthier option.
It's organically grown.

369
00:13:49,651 --> 00:13:51,952
Lord.

370
00:13:52,003 --> 00:13:53,670
Ron, are you okay?

371
00:13:53,755 --> 00:13:54,922
Whoa, whoa, whoa, okay.

372
00:13:54,989 --> 00:13:58,158
Could you get us
a cold compress or something?

373
00:14:01,212 --> 00:14:04,181
♪
♪

374
00:14:04,265 --> 00:14:07,868
Hey, uh, six beers
for, uh, table 12.

375
00:14:07,936 --> 00:14:09,887
- Do you work here?
- Yeah.

376
00:14:09,971 --> 00:14:12,306
My dad owns this place.
I'm Janet.

377
00:14:12,357 --> 00:14:13,874
Janet Snakehole.

378
00:14:13,942 --> 00:14:17,027
Hey! This round's
on the house!

379
00:14:17,111 --> 00:14:18,695
- All right!
- Whoo!

380
00:14:18,780 --> 00:14:19,980
How about some tips?

381
00:14:20,031 --> 00:14:22,783
- Uh, gum or mint, sir?
- Uh, gum.

382
00:14:22,851 --> 00:14:24,651
- All out, sir.
- Then mints, I guess.

383
00:14:24,719 --> 00:14:26,119
Don't have those either, sir.

384
00:14:26,187 --> 00:14:30,073
- Do you have anything?
- Out of everything, sir.

385
00:14:30,158 --> 00:14:31,792
This is so awesome!

386
00:14:31,860 --> 00:14:34,394
We are like Robin Hood.

387
00:14:34,462 --> 00:14:38,715
We steal from the club
and we give to ourselves.

388
00:14:38,800 --> 00:14:40,417
Look, we'll find
a 24-hour diner.

389
00:14:40,501 --> 00:14:42,252
Or we can get one of those cows

390
00:14:42,337 --> 00:14:43,871
that we saw on the way up here

391
00:14:43,938 --> 00:14:45,505
and we'll bring it back and
we'll make steaks out of that.

392
00:14:46,674 --> 00:14:48,675
Ann Perkins,
what are you doing here?

393
00:14:48,726 --> 00:14:50,427
Whose pink razor
is in your shower?

394
00:14:50,511 --> 00:14:51,762
Excuse me?

395
00:14:51,846 --> 00:14:53,013
Leslie found a pink razor

396
00:14:53,064 --> 00:14:55,048
and a pink swimming cap
in your shower.

397
00:14:55,116 --> 00:14:56,483
Whose is it?

398
00:14:56,550 --> 00:14:57,818
I guess you're talking
about my razor.

399
00:14:57,886 --> 00:14:59,386
I shave my legs for swimming,

400
00:14:59,454 --> 00:15:00,720
and women's razors work better.

401
00:15:00,772 --> 00:15:03,056
For whatever reason,
men's razor technology

402
00:15:03,124 --> 00:15:05,292
hasn't figured out how to
properly contour the shinbone.

403
00:15:05,360 --> 00:15:07,744
And... and the swimming cap?

404
00:15:07,829 --> 00:15:10,430
Indiana breast cancer
awareness triathlon 2009.

405
00:15:10,498 --> 00:15:12,416
Came in fourth.

406
00:15:12,500 --> 00:15:15,068
Well, I found concealer
in your medicine cabinet.

407
00:15:15,136 --> 00:15:16,169
What's that about?

408
00:15:16,237 --> 00:15:18,288
I'm a human being.

409
00:15:18,373 --> 00:15:20,424
Sometimes I get blemishes.
I'm not perfect.

410
00:15:20,508 --> 00:15:23,944
Oh, God.
I am so sorry, honey.

411
00:15:24,012 --> 00:15:25,262
I'm so embarrassed.

412
00:15:25,346 --> 00:15:28,081
I was scared that you
were cheating on me.

413
00:15:28,149 --> 00:15:31,318
No, I'm not cheating on you.

414
00:15:31,386 --> 00:15:34,254
But I'm also not dating you.

415
00:15:34,322 --> 00:15:36,957
We broke up last week.

416
00:15:40,540 --> 00:15:43,690
We talked at your house.
Do you not remember this?

417
00:15:43,810 --> 00:15:46,261
Of course I remember,
but we didn't break up.

418
00:15:46,329 --> 00:15:48,063
I'm sorry, Ann,
but I'm certain we did.

419
00:15:48,131 --> 00:15:50,466
Okay, you said that you
didn't want to leave Pawnee

420
00:15:50,533 --> 00:15:53,068
and that I was the most amazing
woman you had ever met.

421
00:15:53,136 --> 00:15:54,636
- Which you are.
- And then I said

422
00:15:54,704 --> 00:15:56,405
that I would move to
Indianapolis if I had to,

423
00:15:56,456 --> 00:15:58,107
and you said you didn't
want to make me do that,

424
00:15:58,174 --> 00:15:59,908
and then you said
that in a different world...

425
00:15:59,976 --> 00:16:03,479
Oh, my God.
You broke up with me.

426
00:16:03,546 --> 00:16:05,330
Oh, my God.

427
00:16:05,415 --> 00:16:07,716
Yeah, so here's what happened.

428
00:16:07,784 --> 00:16:10,752
Sweet and beautiful Ann
has never been dumped before,

429
00:16:10,820 --> 00:16:12,671
and Chris is such
a positive person,

430
00:16:12,755 --> 00:16:15,390
when he broke up with her,
she just didn't realize it.

431
00:16:15,458 --> 00:16:16,925
It's kind of understandable,

432
00:16:16,993 --> 00:16:18,694
although it does kind of
make you wonder

433
00:16:18,761 --> 00:16:20,963
how good of a nurse she is.

434
00:16:21,030 --> 00:16:22,297
Oh, my God.

435
00:16:22,348 --> 00:16:23,515
You left, and you said
you were gonna work late,

436
00:16:23,600 --> 00:16:24,833
and I kissed you?

437
00:16:24,901 --> 00:16:27,436
As enjoyable as that was,
I did find it odd.

438
00:16:28,738 --> 00:16:30,706
And then I wanted you
to meet my parents?

439
00:16:30,773 --> 00:16:32,641
I'm sure they're great people,
but strange.

440
00:16:32,709 --> 00:16:33,942
This is humiliating.

441
00:16:34,010 --> 00:16:36,662
I'm sorry, I gotta go.

442
00:16:36,746 --> 00:16:37,663
Hey, w...

443
00:16:39,415 --> 00:16:41,200
Hey, man.

444
00:16:41,284 --> 00:16:45,704
I, uh, got you another melontini.

445
00:16:45,788 --> 00:16:47,189
Thanks.

446
00:16:49,092 --> 00:16:52,461
Listen, that guy is a jerk,

447
00:16:52,529 --> 00:16:54,897
and if you ask me,
he smells like...

448
00:16:54,964 --> 00:16:57,132
Success.

449
00:16:57,200 --> 00:17:00,686
He smells like success.

450
00:17:00,770 --> 00:17:01,970
You know what I smell like?

451
00:17:02,021 --> 00:17:07,509
A teriyaki hairpiece?

452
00:17:07,577 --> 00:17:11,346
I smell like the guy
who's always coming up short.

453
00:17:11,414 --> 00:17:13,682
And you know what?
I'm just sick of it.

454
00:17:13,733 --> 00:17:17,653
Tommy Fresh was my dream.

455
00:17:17,704 --> 00:17:19,555
Now no one's ever
gonna smell it but me.

456
00:17:19,622 --> 00:17:22,191
I can smell your dreams, Tom.

457
00:17:22,258 --> 00:17:24,393
I can.

458
00:17:24,460 --> 00:17:26,728
And I can smell 'em from here.

459
00:17:26,796 --> 00:17:30,198
And honestly, they smell
ing terrible.

460
00:17:31,584 --> 00:17:34,503
But I like Tom.

461
00:17:34,571 --> 00:17:38,056
- I've got a jar of olives.
- Heh! Nice.

462
00:17:38,141 --> 00:17:40,842
A thing of toothpicks
that look like swords.

463
00:17:40,910 --> 00:17:44,012
And a year's supply
of toilet paper.

464
00:17:44,080 --> 00:17:50,018
I got... boom!...
$38 in tips.

465
00:17:50,069 --> 00:17:52,354
$180.

466
00:17:55,291 --> 00:17:57,259
We should give it back.

467
00:17:59,762 --> 00:18:01,563
- Follow me.
- Okay.

468
00:18:03,166 --> 00:18:04,900
Oh, my God.

469
00:18:11,257 --> 00:18:14,042
Wow.
Thank you.

470
00:18:15,845 --> 00:18:17,579
One time,
when I was in high school,

471
00:18:17,647 --> 00:18:21,617
a guy's mom called me
and broke up with me for him.

472
00:18:23,019 --> 00:18:25,621
There was another time
where I was on a date

473
00:18:25,688 --> 00:18:27,456
and I tripped
and broke my kneecap.

474
00:18:27,523 --> 00:18:29,791
And then the guy said
he wasn't "feelin' it,"

475
00:18:29,859 --> 00:18:33,829
so he left and I waited
for an ambulance.

476
00:18:33,896 --> 00:18:35,597
One time I was dating
this guy for a while,

477
00:18:35,665 --> 00:18:36,798
and then he got down on one knee

478
00:18:36,866 --> 00:18:38,467
and he begged me
to never call him again.

479
00:18:38,534 --> 00:18:39,801
One guy broke up with me

480
00:18:39,869 --> 00:18:41,737
while we were in
the shower together.

481
00:18:41,788 --> 00:18:43,805
Skywriting isn't always positive.

482
00:18:43,873 --> 00:18:47,643
Another time, a guy invited me
to a beautiful picnic

483
00:18:47,710 --> 00:18:48,910
with wine and flowers,

484
00:18:48,978 --> 00:18:50,412
and then,
when I tried to sit down,

485
00:18:50,480 --> 00:18:53,415
he said, "Don't eat anything.
Rebecca's coming."

486
00:18:53,483 --> 00:18:54,750
And then he broke up with me.

487
00:18:55,952 --> 00:18:58,820
- Who was Rebecca?
- Yeah. Exactly.

488
00:18:58,888 --> 00:19:01,890
Thanks for driving me.

489
00:19:01,958 --> 00:19:03,091
Wait.

490
00:19:03,159 --> 00:19:04,092
You're getting
your commendation tomorrow

491
00:19:04,160 --> 00:19:05,327
at the Statehouse.

492
00:19:05,395 --> 00:19:07,829
Oh, please.
It's just a goofy ceremony.

493
00:19:07,897 --> 00:19:08,830
I don't even care about it.

494
00:19:08,898 --> 00:19:09,898
Leslie.

495
00:19:09,965 --> 00:19:12,534
Besides, Ron is staying behind.

496
00:19:12,602 --> 00:19:13,802
He'll go.

497
00:19:13,870 --> 00:19:15,904
Is Ron gonna be okay?

498
00:19:18,408 --> 00:19:20,242
I honestly don't know.

499
00:19:25,281 --> 00:19:27,149
This isn't a steak.

500
00:19:27,216 --> 00:19:28,617
Why would you call it that
on your menu?

501
00:19:28,685 --> 00:19:29,985
I don't know what
to tell you, man.

502
00:19:33,172 --> 00:19:36,591
Just give me all the bacon
and eggs you have.

503
00:19:36,659 --> 00:19:37,843
Wait, wait.

504
00:19:37,927 --> 00:19:39,494
I worry what you just heard was,

505
00:19:39,545 --> 00:19:42,180
"Give me a lot
of bacon and eggs."

506
00:19:42,265 --> 00:19:43,799
What I said was,

507
00:19:43,850 --> 00:19:47,719
give me all the bacon
and eggs you have.

508
00:19:47,804 --> 00:19:49,371
Do you understand?

509
00:19:53,209 --> 00:19:54,843
It's called a tommytini.

510
00:19:54,894 --> 00:19:57,579
It's just vodka
and a bunch of cinnamon.

511
00:19:57,647 --> 00:19:59,247
That sounds gross.

512
00:19:59,315 --> 00:20:01,366
- Ben.
- Oh, Miller lite.

513
00:20:01,451 --> 00:20:03,151
- How'd you know?
- Everybody knows.

514
00:20:03,202 --> 00:20:04,152
That's your drink.

515
00:20:04,203 --> 00:20:05,620
- Tommy.
- Let's do this.

516
00:20:05,688 --> 00:20:07,372
♪ You know just what I need ♪

517
00:20:07,457 --> 00:20:09,658
♪ I need that fast money ♪

518
00:20:09,726 --> 00:20:11,827
♪ fast cars, fast women ♪

519
00:20:11,894 --> 00:20:13,161
Mmm!

520
00:20:13,212 --> 00:20:14,546
How's it going?

521
00:20:14,630 --> 00:20:16,998
Hey, look who's socializing!

522
00:20:17,050 --> 00:20:18,433
Yeah, and I'm having a good time.

523
00:20:18,501 --> 00:20:19,751
- Sit with us.
- Okay.

524
00:20:19,836 --> 00:20:22,036
Ann's in kind of a crappy mood

525
00:20:22,088 --> 00:20:23,588
because Chris dumped her.

526
00:20:23,673 --> 00:20:26,241
Ah, yeah, like a week ago, right?

527
00:20:26,309 --> 00:20:30,278
Yeah, but I only knew about it
an hour ago.

528
00:20:30,346 --> 00:20:32,614
Oh, so that's why
you tried to kiss him.

529
00:20:32,682 --> 00:20:35,400
We were all seriously
confused about that.

530
00:20:35,485 --> 00:20:37,919
Who's "we"? What are you...?
Oh, God.

531
00:20:38,039 --> 00:20:41,937
<font color="#ec14bd">Sync by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

532
00:20:42,057 --> 00:20:43,390
Did you keep the toilet paper?

533
00:20:43,442 --> 00:20:45,576
Yes. I feel bad,
but I need it.

534
00:20:45,661 --> 00:20:46,961
Ew!

535
00:20:48,580 --> 00:20:50,498
Hey, hey, that's gotta be
Feinstein's car.

536
00:20:50,565 --> 00:20:52,366
Give me...
Give me the Tommy Fresh.

537
00:20:52,417 --> 00:20:54,285
- Why?
- Gimme the Tommy Fresh.

538
00:21:00,375 --> 00:21:02,543
He's gonna smell your dreams now.

539
00:21:03,912 --> 00:21:05,947
Are you okay?

540
00:21:06,014 --> 00:21:08,216
- Is it that bad?
- Oh, my God, it's unbelievable.

541
00:21:08,267 --> 00:21:09,794
Oh!

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
