1
00:00:00,203 --> 00:00:01,220
Okay, time for gifts.

2
00:00:01,303 --> 00:00:05,020
We have gift certificates
for facials everybody,

3
00:00:05,321 --> 00:00:07,420
then you're gonna get
a needlepoint pillow

4
00:00:07,421 --> 00:00:09,502
with your face on it
and a news headline

5
00:00:09,503 --> 00:00:11,120
from a newspaper
on the day you were born.

6
00:00:11,138 --> 00:00:13,356
"Joseph Stalin dies"?

7
00:00:13,390 --> 00:00:14,991
That's right, Mom,
and you replaced him.

8
00:00:15,025 --> 00:00:17,260
Though not in terms
of genocide.

9
00:00:17,294 --> 00:00:19,178
Just in terms of,
you know, being on earth.

10
00:00:19,229 --> 00:00:22,481
February 14th, Valentine's Day,
is about romance,

11
00:00:22,516 --> 00:00:26,068
but February 13th,
galentine's day,

12
00:00:26,103 --> 00:00:27,803
is about celebrating
lady friends.

13
00:00:27,838 --> 00:00:30,640
It's wonderful, and it
should be a national holiday.

14
00:00:31,174 --> 00:00:32,658
It <i>should</i> be
a national holiday.

15
00:00:33,693 --> 00:00:36,479
Dear Congress,
it's Leslie again.

16
00:00:36,513 --> 00:00:38,748
Let's talk about
personal relationships.

17
00:00:38,782 --> 00:00:39,815
Who wants
to start? Donna?

18
00:00:39,850 --> 00:00:42,285
I have several men
in rotation.

19
00:00:42,319 --> 00:00:43,319
One's waiting for me
out in the car.

20
00:00:43,353 --> 00:00:44,486
[Chuckling]

21
00:00:44,504 --> 00:00:46,289
Don't worry,
I rolled down the window.

22
00:00:46,323 --> 00:00:48,391
April, you're
the old married broad here.

23
00:00:48,425 --> 00:00:49,491
- How's Andy?
- Well...

24
00:00:49,509 --> 00:00:51,627
Last week he was
supposed to buy gas,

25
00:00:51,661 --> 00:00:53,896
but instead he bought
novelty cookie cutters.

26
00:00:53,931 --> 00:00:55,865
Now everything we eat
is shaped like a dinosaur.

27
00:00:55,899 --> 00:00:57,434
He's amazing.

28
00:00:58,068 --> 00:01:00,469
Great. And mom, how's
it going with Steven?

29
00:01:00,503 --> 00:01:02,338
Well, I met him
on the Internet

30
00:01:02,356 --> 00:01:05,524
because of
my lovely daughter,

31
00:01:05,559 --> 00:01:09,028
and we are compatible.

32
00:01:09,062 --> 00:01:10,396
La, la,
la, la, la, la, la.

33
00:01:10,447 --> 00:01:12,848
Okay, no, no, no, no, no.
Ann, can you start talking?

34
00:01:12,883 --> 00:01:15,534
Well, I'm--I'm--I'm
in a bit of a lull right now,

35
00:01:15,569 --> 00:01:18,287
but I'm so glad that you all
have people in your lives

36
00:01:18,322 --> 00:01:20,489
that you care about,
so... Congrats!

37
00:01:21,992 --> 00:01:24,794
It's really hard to say congrats
without sounding sarcastic.

38
00:01:24,828 --> 00:01:29,198
Seriously, I really hope
you guys are all very happy--

39
00:01:29,233 --> 00:01:30,900
- Damn it.
- We are.

40
00:01:32,302 --> 00:01:35,304
[Triumphant music]

41
00:01:36,306 --> 00:01:40,906
<font color="#ff8c00">Sync & corrections by Alice</font>
<font color="#ff8c00">www.addic7ed.com</font>

42
00:01:52,489 --> 00:01:54,240
Ann, I just
wanted to say

43
00:01:54,274 --> 00:01:55,574
I'm sorry if I
put you on the spot

44
00:01:55,609 --> 00:01:57,059
when I asked you
about your love life.

45
00:01:57,077 --> 00:01:58,394
How are you doing?

46
00:01:58,412 --> 00:02:01,397
You know, I'm mostly
fine being single.

47
00:02:01,431 --> 00:02:04,066
It's just this time of year
with the hearts and roses,

48
00:02:04,084 --> 00:02:05,334
it just gets me
a little down.

49
00:02:06,369 --> 00:02:08,587
But galentine's day
made me feel better,

50
00:02:08,622 --> 00:02:10,589
so thanks.

51
00:02:10,624 --> 00:02:13,276
Oh, Ann,
you beautiful spinster.

52
00:02:13,310 --> 00:02:14,277
I will find you love.

53
00:02:14,311 --> 00:02:15,678
What? Did
you say something?

54
00:02:15,712 --> 00:02:17,146
Love you.

55
00:02:17,180 --> 00:02:19,815
Thank you all
for being here.

56
00:02:19,850 --> 00:02:20,816
Let's get started.

57
00:02:21,351 --> 00:02:23,469
Wow. Great attitude, Ron.

58
00:02:24,520 --> 00:02:27,556
Sorry, I was talking
to these ribs.

59
00:02:27,590 --> 00:02:29,892
Okay, so the
Valentine's Day dance

60
00:02:29,926 --> 00:02:31,661
is planned, prepped,
and under budget,

61
00:02:31,695 --> 00:02:34,146
but we have one final task.

62
00:02:34,197 --> 00:02:35,598
Ann Perkins is
currently attending

63
00:02:35,632 --> 00:02:39,135
the Valentine's Day dance alone,
so I need each one of you

64
00:02:39,169 --> 00:02:41,454
to bring an eligible bachelor
for her tomorrow.

65
00:02:41,488 --> 00:02:43,205
Ann's not totally hideous.

66
00:02:43,240 --> 00:02:44,290
Why does she need our help?

67
00:02:44,324 --> 00:02:45,708
Because that's
what friends do, April.

68
00:02:45,742 --> 00:02:47,943
They help friends
find happiness.

69
00:02:47,978 --> 00:02:49,679
Now the last guy
she liked was Chris,

70
00:02:49,713 --> 00:02:52,114
but she can't date him again
because he's her boss.

71
00:02:52,132 --> 00:02:54,300
So when we're thinking
of prospective sweethearts,

72
00:02:54,334 --> 00:02:58,287
we need to think of people
who are... Attractive

73
00:02:58,305 --> 00:03:00,690
and smart and kind.

74
00:03:00,724 --> 00:03:02,792
And if you're wondering what
kind of guy is right for Ann,

75
00:03:02,826 --> 00:03:04,477
all you need
to do is ask.

76
00:03:04,511 --> 00:03:07,129
Also you need to find
someone educated,

77
00:03:07,147 --> 00:03:08,297
and friendly,
and fun--

78
00:03:08,332 --> 00:03:10,299
- just tell us
the damn word.

79
00:03:10,317 --> 00:03:13,035
Effervescent, he needs
to be effervescent.

80
00:03:13,070 --> 00:03:14,570
Quick question
about Ann--

81
00:03:14,604 --> 00:03:17,540
does anyone know if she has
any indian in her?

82
00:03:17,574 --> 00:03:18,808
No one respond.

83
00:03:18,842 --> 00:03:19,909
- No one say anything.
- Why?

84
00:03:19,943 --> 00:03:22,078
I'm just
curious if Ann

85
00:03:22,112 --> 00:03:24,847
- has a little indian in her.
- Silence.

86
00:03:24,881 --> 00:03:26,648
- I don't think she does.
- Would she like some?

87
00:03:26,666 --> 00:03:28,000
All: Jerry!

88
00:03:28,266 --> 00:03:30,600
Happy Valentine's Day.

89
00:03:30,887 --> 00:03:34,256
- And...Ooh-ah!
- [Gasping]

90
00:03:34,291 --> 00:03:35,758
- Yachter Otter?
- Yup.

91
00:03:35,792 --> 00:03:37,860
Two months ago,
I have a dream

92
00:03:37,894 --> 00:03:39,762
about a playboy
otter lost at sea,

93
00:03:39,796 --> 00:03:41,030
and you
make him real?

94
00:03:41,064 --> 00:03:42,898
- Uh-huh.
- I love it.

95
00:03:42,933 --> 00:03:44,266
I thought we had agreed
that we weren't

96
00:03:44,301 --> 00:03:46,052
gonna get each other presents
'cause we were too busy

97
00:03:46,103 --> 00:03:47,720
with the campaign--
I got you something too.

98
00:03:48,722 --> 00:03:51,774
Oh, a... bomb?

99
00:03:51,808 --> 00:03:54,110
It is a cryptex, like in
that movie <i>The Da Vinci Code,</i>

100
00:03:54,144 --> 00:03:55,895
which was the first movie that
you and I ever watched

101
00:03:55,946 --> 00:03:59,065
- on Starz HD.
- Wow, that's specific.

102
00:03:59,116 --> 00:04:01,884
Inside it will tell you where
to meet me at 9:00 P.M. tonight,

103
00:04:01,918 --> 00:04:04,387
and the combination
is a five-letter code

104
00:04:04,421 --> 00:04:07,189
that captures the essence
of our third date.

105
00:04:07,207 --> 00:04:08,958
- Yeah...
- So I will see you tonight.

106
00:04:08,992 --> 00:04:11,460
Unless...You can't
crack the code.

107
00:04:12,495 --> 00:04:15,131
I think I can
crack the code.

108
00:04:17,417 --> 00:04:18,801
I have no idea
what the code is.

109
00:04:18,835 --> 00:04:20,369
Hey, man, you okay?

110
00:04:20,404 --> 00:04:22,138
Still thinking
about your ex?

111
00:04:22,172 --> 00:04:26,675
Millicent Gergich
has literally

112
00:04:26,709 --> 00:04:30,146
torn my heart from my body
and replaced it

113
00:04:30,180 --> 00:04:34,550
with a thick slab
of sadness.

114
00:04:34,584 --> 00:04:35,851
I may never smile again.

115
00:04:37,254 --> 00:04:40,356
Cool, so I found a DJ
for the dance,

116
00:04:40,390 --> 00:04:42,741
and his name is
DJ Bluntz.

117
00:04:43,776 --> 00:04:46,612
Tom, this is a publicly
funded couples dance.

118
00:04:46,663 --> 00:04:48,664
I don't think it's
appropriate for people

119
00:04:48,698 --> 00:04:50,950
to be getting
"wet with sound."

120
00:04:51,001 --> 00:04:52,134
Who's gonna do it, then?

121
00:04:52,169 --> 00:04:54,603
I will. I certainly
have no other plans

122
00:04:54,638 --> 00:04:58,407
for Valentine's Day,
because I am completely alone.

123
00:05:00,460 --> 00:05:02,478
All right,
I'll see you later, Chris.

124
00:05:03,513 --> 00:05:04,780
Aw, cool cryptex,
can I have it?

125
00:05:04,815 --> 00:05:06,098
Hey, no.
No, you can't--

126
00:05:06,133 --> 00:05:07,183
Where'd you get it?

127
00:05:07,217 --> 00:05:08,551
How do you know
what a cryptex is?

128
00:05:08,585 --> 00:05:10,636
I know what things are.

129
00:05:10,687 --> 00:05:13,255
Well, Leslie hid the location
of our Valentine's meeting place

130
00:05:13,290 --> 00:05:14,557
- in here.
- No.

131
00:05:14,591 --> 00:05:16,475
I tried every five-letter word
that has anything

132
00:05:16,526 --> 00:05:17,760
to do with
our third date.

133
00:05:18,794 --> 00:05:20,830
Have you
tried [Bleep]?

134
00:05:20,864 --> 00:05:22,231
That's a
four-letter word.

135
00:05:22,265 --> 00:05:23,983
Oh. Add an "s"?

136
00:05:24,034 --> 00:05:25,534
I really don't
think it's that.

137
00:05:25,569 --> 00:05:27,269
Uh, I wish I could help you,
bro, I don't know if I can.

138
00:05:27,287 --> 00:05:29,839
You're, like, the second
smartest guy I know.

139
00:05:29,873 --> 00:05:31,841
You should go to the first
smartest guy I know.

140
00:05:31,875 --> 00:05:33,459
Okay, so the clue
is inside,

141
00:05:33,493 --> 00:05:36,962
but it takes a five-letter code
to open it-- Andy!

142
00:05:37,497 --> 00:05:38,715
Did you try [Bleep]?

143
00:05:39,307 --> 00:05:39,915
Ha!

144
00:05:39,949 --> 00:05:42,251
Yes, why is that
everyone's first suggestion?

145
00:05:42,285 --> 00:05:43,335
Just smart people.

146
00:05:43,386 --> 00:05:45,421
I think I might
be able to help you.

147
00:05:45,455 --> 00:05:46,455
Told you.

148
00:05:50,494 --> 00:05:51,627
- Let me see it!
- Yeah, you're gonna see it.

149
00:05:51,645 --> 00:05:53,829
I'll show you.
I can unroll it.

150
00:05:53,864 --> 00:05:55,898
"The murals with
this heart you see,

151
00:05:55,932 --> 00:05:58,934
look to the first, and there
your next clue will be."

152
00:05:58,968 --> 00:06:00,686
The first letter
of the name of each mural

153
00:06:00,737 --> 00:06:03,706
with a heart on it
will spell out the clue.

154
00:06:03,740 --> 00:06:05,324
Or some--
or something.

155
00:06:05,358 --> 00:06:06,942
Let's check out
the murals.

156
00:06:06,976 --> 00:06:11,814
Happy Valentine's Day, Pawnee.
For me, it is not happy.

157
00:06:11,848 --> 00:06:15,518
But don't let my sadness
diminish your night.

158
00:06:15,552 --> 00:06:17,853
[Ominous music playing]

159
00:06:17,888 --> 00:06:19,955
Anyway, life is fleeting.

160
00:06:20,488 --> 00:06:22,555
Leslie.
Found a date for Ann.

161
00:06:22,659 --> 00:06:24,043
Jerry, well done.

162
00:06:24,094 --> 00:06:25,511
I put an ad
on Craigslist:

163
00:06:25,545 --> 00:06:28,464
"Man seeking man
for a night of casual fun."

164
00:06:28,498 --> 00:06:29,965
Enrico, he responded
right away.

165
00:06:29,999 --> 00:06:32,518
- I'll meet you inside, okay?
- Okay.

166
00:06:32,552 --> 00:06:34,403
You hired
a male escort.

167
00:06:34,437 --> 00:06:35,838
A what?

168
00:06:35,872 --> 00:06:37,306
Please get your gigolo
out of here.

169
00:06:37,340 --> 00:06:40,910
I-- uh--
oh, my God.

170
00:06:40,944 --> 00:06:42,478
Okay, so we've got
all the ones

171
00:06:42,512 --> 00:06:43,812
from this side
with the heart sticker.

172
00:06:45,847 --> 00:06:47,533
All right, the ones
from the other hallway are...

173
00:06:47,567 --> 00:06:51,520
"Cornfield Slaughter,"
"Lament of the Buffalo,"

174
00:06:51,538 --> 00:06:54,323
"Needless Slaughter,"
"Slaughter Gone Wrong,"

175
00:06:54,357 --> 00:06:57,593
"Eating the Reverend,"
"It's Raining Blood,"

176
00:06:57,627 --> 00:07:00,796
- and "Death Everywhere."
- Great. I got it from here.

177
00:07:00,830 --> 00:07:03,031
Thanks, guys. I hope I didn't
screw up your Valentine's Day too much.

178
00:07:03,049 --> 00:07:05,417
Oh, God, no.
April hates Valentine's Day.

179
00:07:05,468 --> 00:07:07,536
And brunch.
And outside.

180
00:07:07,554 --> 00:07:09,204
And smiling.
[Laughing]

181
00:07:09,222 --> 00:07:11,740
- She's weird.
- Wait. I figured it out.

182
00:07:11,775 --> 00:07:16,078
The letters unscrambled say,
"No food finer.

183
00:07:16,112 --> 00:07:18,547
Clue three
at J.J.'s Diner."

184
00:07:18,582 --> 00:07:20,182
How many clues
are there, exactly?

185
00:07:22,852 --> 00:07:23,886
Hey, you're here.

186
00:07:23,920 --> 00:07:26,522
And I brought
a bachelor.

187
00:07:26,556 --> 00:07:27,523
- Are you kidding me?
- No.

188
00:07:27,557 --> 00:07:29,191
You brought Orin?

189
00:07:29,225 --> 00:07:31,226
Ann is not some
weird, morose mummy.

190
00:07:31,244 --> 00:07:34,363
Offense intended, Orin.
Let this be a wake-up call

191
00:07:34,397 --> 00:07:36,065
about the way you present
yourself to the world.

192
00:07:36,099 --> 00:07:38,167
I think Ann and him
would be cute together.

193
00:07:38,201 --> 00:07:39,368
I ask you
to do one thing.

194
00:07:39,402 --> 00:07:40,836
Do you know how hard
I've been working

195
00:07:40,870 --> 00:07:42,972
to try to pull this together
before Ann gets here?

196
00:07:43,006 --> 00:07:44,206
- Hey.
- Hey, you're here.

197
00:07:44,240 --> 00:07:45,274
We weren't
talking about you.

198
00:07:45,308 --> 00:07:46,742
- How are you?
- Well...

199
00:07:46,776 --> 00:07:48,544
It's Valentine's Day,
and I'm single,

200
00:07:48,578 --> 00:07:49,912
and I'm at
a couples dance.

201
00:07:49,946 --> 00:07:51,747
I can't imagine
a more depressing place to be.

202
00:07:51,781 --> 00:07:54,550
What about a wedding where you
used to go out with the groom,

203
00:07:54,584 --> 00:07:56,118
and you're the only one
there without a date,

204
00:07:56,152 --> 00:07:58,721
so the bride makes you dance
to <i>Single Ladies</i> by yourself?

205
00:07:58,755 --> 00:08:00,022
Oh, my God,
did that happen to you?

206
00:08:00,056 --> 00:08:02,090
Maybe. Let's get a drink,
and then you and I

207
00:08:02,108 --> 00:08:04,660
are gonna have
some f-u-n.

208
00:08:04,694 --> 00:08:06,895
Well, at least the music
seems about right.

209
00:08:06,929 --> 00:08:10,232
[Tragic opera playing]

210
00:08:10,266 --> 00:08:12,618
There was supposed to be,
like, 20 guys here for Ann,

211
00:08:12,652 --> 00:08:14,903
and there's only four.
Who else do we have?

212
00:08:14,938 --> 00:08:16,905
Ben, taken.
Jerry, taken.

213
00:08:16,940 --> 00:08:20,175
Oh! My dentist...
Is 80, and he's gay,

214
00:08:20,210 --> 00:08:22,478
and he's taken.
Uh... what's this?

215
00:08:22,512 --> 00:08:24,013
P. Hut?
Oh, Pizza Hut!

216
00:08:24,047 --> 00:08:25,748
Hold on...

217
00:08:25,782 --> 00:08:28,300
Hi, how cute are you?

218
00:08:30,582 --> 00:08:32,900
Ann, this is my
lawyer friend, Alex.

219
00:08:32,957 --> 00:08:34,041
- Hi.
- Hi.

220
00:08:34,092 --> 00:08:35,092
- Nice to meet you.
- Nice to meet you.

221
00:08:35,126 --> 00:08:36,993
I've heard
a lot about you...

222
00:08:37,011 --> 00:08:38,378
- In the last hour.
- Yeah.

223
00:08:38,429 --> 00:08:40,330
Well, Alex gives
my campaign legal advice,

224
00:08:40,365 --> 00:08:43,000
and Ann is the greatest
human being ever invented.

225
00:08:43,034 --> 00:08:45,219
So I'm gonna-- what?
Uh, someone needs me.

226
00:08:45,270 --> 00:08:47,471
I'm gonna go, and you two
just hit it off now.

227
00:08:47,505 --> 00:08:49,673
- You crazy kids.
- Okay...

228
00:08:49,691 --> 00:08:51,408
Both: [Laughing nervously]

229
00:08:51,442 --> 00:08:54,978
Okay..."There
is no food finer."

230
00:08:55,012 --> 00:08:57,364
For Leslie, that means
whipped cream, or...

231
00:08:58,398 --> 00:08:59,849
No, it's
whipped cream.

232
00:08:59,868 --> 00:09:01,518
I'm gonna look
in the whipped cream cans.

233
00:09:02,336 --> 00:09:04,022
Oh, got some!

234
00:09:08,526 --> 00:09:10,027
Oh...

235
00:09:10,861 --> 00:09:12,412
Nothing yet.

236
00:09:12,463 --> 00:09:14,998
Okay, "something wicked
this way comes,"

237
00:09:15,033 --> 00:09:17,918
"whence you peruse a book
with your thumbs.

238
00:09:17,969 --> 00:09:20,003
"Go to the one
who succumbs.

239
00:09:20,037 --> 00:09:21,972
I'll be attached
with a little gum."

240
00:09:22,006 --> 00:09:23,223
Oh, come on, Leslie,
gimme a break.

241
00:09:23,258 --> 00:09:26,109
I hate riddles
and other such nonsense,

242
00:09:26,144 --> 00:09:28,228
I want that
on the record.

243
00:09:28,263 --> 00:09:30,681
But something wicked
with a book

244
00:09:30,715 --> 00:09:32,950
is my ex-wife,
from the library.

245
00:09:32,984 --> 00:09:34,451
Which means I'm the one
that succumbs.

246
00:09:34,485 --> 00:09:37,654
Uh..."I'll be attached
with a little gum."

247
00:09:37,689 --> 00:09:40,090
Good God.
That woman is good.

248
00:09:40,124 --> 00:09:42,226
"Follow me to
the sheltered snow.

249
00:09:42,260 --> 00:09:44,278
Only 22 clues
left to go."

250
00:09:44,329 --> 00:09:46,997
22?

251
00:09:47,031 --> 00:09:49,600
Well, this is the woman
I've chosen to love.

252
00:09:49,634 --> 00:09:51,034
Well, I guess
there's nothing to do

253
00:09:51,069 --> 00:09:52,452
but spend
the entire night

254
00:09:52,503 --> 00:09:54,104
methodically solving
this puzzle.

255
00:09:54,138 --> 00:09:56,006
There's no way
we'll finish in time.

256
00:09:56,040 --> 00:09:57,741
Okay, I'll just
make a list of places

257
00:09:57,759 --> 00:09:59,943
she and I have been together,
and hope we luck out

258
00:09:59,978 --> 00:10:01,211
and find
the last clue.

259
00:10:01,246 --> 00:10:02,879
We'll split up,
cover more ground.

260
00:10:02,913 --> 00:10:03,964
Move.

261
00:10:04,015 --> 00:10:08,552
[Ominous music playing]

262
00:10:08,586 --> 00:10:11,121
Hey, Kriss Kross,
can we change up the music?

263
00:10:11,155 --> 00:10:13,257
It kind of sounds like
the end of a movie

264
00:10:13,291 --> 00:10:14,608
about a monk
who killed himself.

265
00:10:14,642 --> 00:10:16,727
- It is.
- Listen, man...

266
00:10:16,761 --> 00:10:18,395
There's some
attractive women here.

267
00:10:18,429 --> 00:10:19,463
Why don't
you rebound?

268
00:10:19,497 --> 00:10:22,532
Nobody here
compares to Millicent.

269
00:10:22,567 --> 00:10:23,634
Except maybe Jerry.

270
00:10:23,668 --> 00:10:26,970
Technically they share 50%
of the same DNA.

271
00:10:34,178 --> 00:10:36,113
Stop staring
at Jerry like that.

272
00:10:40,668 --> 00:10:43,503
I don't know.
It's working now.

273
00:10:43,554 --> 00:10:44,954
Hey, how's it going?

274
00:10:44,973 --> 00:10:47,324
It's Valentine's Day,

275
00:10:47,358 --> 00:10:49,343
and I'm working the late shift
of the snow globe museum,

276
00:10:49,394 --> 00:10:50,811
so I'm right
where I wanna be.

277
00:10:50,845 --> 00:10:54,398
Okay, um, my girlfriend
left a clue here, I think.

278
00:10:54,432 --> 00:10:56,133
Like, for a
scavenger hunt.

279
00:10:56,167 --> 00:10:58,669
- Right. You're Ben.
- Yes.

280
00:10:58,703 --> 00:11:01,505
She said you'd
be here by noon.

281
00:11:01,539 --> 00:11:02,906
Not doing so good.
All right, well.

282
00:11:02,940 --> 00:11:04,541
Thanks, Kevin.
I'm supposed to tell you

283
00:11:04,575 --> 00:11:05,976
that it's under
one of the snow globes.

284
00:11:06,010 --> 00:11:09,012
But which one?
Ooh, I don't know.

285
00:11:11,666 --> 00:11:14,484
Nope. No.

286
00:11:14,519 --> 00:11:16,820
No. You really suck at this.
Okay, why don't you just...

287
00:11:16,838 --> 00:11:17,954
Tell me where it is
and I can get out of here?

288
00:11:17,988 --> 00:11:19,656
That would take
all the fun out of it.

289
00:11:19,691 --> 00:11:21,825
- Ah!
- Winner.

290
00:11:21,859 --> 00:11:23,493
Ah, ha, ha, ha.
I found it.

291
00:11:23,528 --> 00:11:25,829
Yep. Oh, wait.

292
00:11:25,863 --> 00:11:27,831
No, that might be
from another scavenger hunt.

293
00:11:27,849 --> 00:11:30,801
Ann, looks like
you already met Jeff.

294
00:11:30,835 --> 00:11:33,002
Well, you didn't tell me that
your friend was so beautiful.

295
00:11:33,021 --> 00:11:34,571
Aww, thanks, Jeff.

296
00:11:34,605 --> 00:11:36,239
Not as beautiful
as my sister,

297
00:11:36,274 --> 00:11:38,575
but you know the law.
[Laughing]

298
00:11:38,609 --> 00:11:40,377
- What?
- No, Jeff.

299
00:11:40,411 --> 00:11:43,030
How am I supposed
to find the love of Ann's life

300
00:11:43,064 --> 00:11:44,398
when it sounds
like a funeral in here?

301
00:11:44,449 --> 00:11:46,750
There's no changing it
as long as Chris is depressed.

302
00:11:46,784 --> 00:11:48,568
I'm gonna go talk to Chris.
You go help Ann.

303
00:11:48,619 --> 00:11:50,220
She's not responding
to my top candidates,

304
00:11:50,254 --> 00:11:52,039
so we're gonna have
to go with some wild cards.

305
00:11:52,073 --> 00:11:53,090
- Got it.
- And April...

306
00:11:53,124 --> 00:11:54,541
Thanks for nothing.

307
00:11:54,575 --> 00:11:55,742
Why should I do anything
to help her?

308
00:11:55,793 --> 00:11:57,361
Because despite the fact
that all you do

309
00:11:57,395 --> 00:12:00,030
is give her a hard time,
she would do this for you.

310
00:12:00,064 --> 00:12:02,599
Just go hide under the table
with your friend Orin.

311
00:12:02,633 --> 00:12:04,534
Yeah, I see you, weirdo.

312
00:12:07,271 --> 00:12:09,038
So Leslie dragged you

313
00:12:09,057 --> 00:12:11,274
into this sneaky little
fix-up scheme, huh?

314
00:12:11,309 --> 00:12:13,843
We're stuck at a Parks and Rec
community couples event.

315
00:12:13,878 --> 00:12:15,344
What else
you gotta do?

316
00:12:15,379 --> 00:12:17,314
What the hell.
You're right.

317
00:12:17,348 --> 00:12:18,398
Bring 'em on, Tom.

318
00:12:18,433 --> 00:12:20,550
I'm Harris.
Heard you were desperate

319
00:12:20,585 --> 00:12:23,186
for a man-piece.
We in business?

320
00:12:23,221 --> 00:12:25,922
He's 33 years old,
still lives with his parents,

321
00:12:25,957 --> 00:12:29,059
and he's been to at least
200 Phish concerts.

322
00:12:29,093 --> 00:12:30,794
Try 308.

323
00:12:30,828 --> 00:12:32,095
I'm gonna have
to pass right now.

324
00:12:32,130 --> 00:12:33,530
Your mistake, mama.

325
00:12:33,564 --> 00:12:38,335
So, Bill, this stunning woman
saves lives for a living.

326
00:12:38,369 --> 00:12:39,703
What do you
bring to the table?

327
00:12:39,737 --> 00:12:41,972
Uh, where to begin?
Uh, I'm an amateur juggler--

328
00:12:42,006 --> 00:12:43,239
Nope, you shouldn't
have begun there.

329
00:12:43,257 --> 00:12:46,409
Get out. No one's trying
to get with jugglers.

330
00:12:46,427 --> 00:12:48,295
Thanks for playing.

331
00:12:55,353 --> 00:12:58,188
Hello, my name
is Ron Swanson.

332
00:12:58,222 --> 00:13:00,757
I believe Leslie Knope
may have left some kind

333
00:13:00,775 --> 00:13:02,325
of scavenger hunt
clue here.

334
00:13:02,360 --> 00:13:04,728
She did.
One second.

335
00:13:09,648 --> 00:13:10,767
Gentlemen.

336
00:13:11,785 --> 00:13:13,638
Ah-- [Giggling].

337
00:13:13,785 --> 00:13:14,838
Enjoy your evening.

338
00:13:14,872 --> 00:13:16,490
[Laughing]

339
00:13:16,872 --> 00:13:20,890
Well... it always says
"Break glass in case of an emergency."

340
00:13:22,946 --> 00:13:23,747
Wait a second.

341
00:13:24,281 --> 00:13:26,967
[Chuckling]

342
00:13:27,001 --> 00:13:31,638
Well, there we go.

343
00:13:31,672 --> 00:13:34,290
Hey, Chris, what do you think
about changing the music?

344
00:13:34,308 --> 00:13:37,994
Sure.
[Ominous music plays]

345
00:13:38,029 --> 00:13:39,346
Ugh. Hey,
you know what?

346
00:13:39,397 --> 00:13:41,064
You should look
on the bright side.

347
00:13:41,098 --> 00:13:43,066
I'm sure that Millicent
couldn't keep up with you

348
00:13:43,100 --> 00:13:45,936
on your runs, so now
you can go full speed again.

349
00:13:45,970 --> 00:13:48,939
On the contrary,
she was faster than I was.

350
00:13:48,973 --> 00:13:52,943
I set many personal bests
just trying to keep up with her.

351
00:13:52,977 --> 00:13:55,745
Which is what
I'm worried about.

352
00:13:55,780 --> 00:13:58,715
What if she was
my personal best?

353
00:13:58,749 --> 00:14:01,051
No, your best
is still ahead of you.

354
00:14:01,085 --> 00:14:03,320
I am 44 years old.

355
00:14:03,354 --> 00:14:04,721
You don't look
a day over 30.

356
00:14:04,755 --> 00:14:06,022
Most people say 25.

357
00:14:06,057 --> 00:14:07,657
- Who says that?
- Lot of people.

358
00:14:07,692 --> 00:14:10,227
You don't think that
Millicent was my soul mate?

359
00:14:10,261 --> 00:14:12,429
There are a lot
of soul mates in the world.

360
00:14:12,463 --> 00:14:13,763
I mean, look at Ann.

361
00:14:13,798 --> 00:14:16,032
You dated her, and she's
a perfect human specimen,

362
00:14:16,067 --> 00:14:18,401
and you tossed her out
like day-old chowder.

363
00:14:18,436 --> 00:14:20,470
But it's
gonna be okay.

364
00:14:20,504 --> 00:14:21,471
Buck up.

365
00:14:21,505 --> 00:14:24,474
Thanks, I'll try.

366
00:14:31,549 --> 00:14:34,518
[Upbeat party music]

367
00:14:34,552 --> 00:14:36,853
♪

368
00:14:36,871 --> 00:14:38,872
- Hey.
- Hey.

369
00:14:38,906 --> 00:14:40,240
How is your
evening unfolding

370
00:14:40,291 --> 00:14:41,708
in terms of your
conversations with men?

371
00:14:41,742 --> 00:14:43,877
I have met a lot
of different guys tonight.

372
00:14:43,911 --> 00:14:46,746
How lucky that
that happened to you

373
00:14:46,797 --> 00:14:49,132
- on Valentine's Day.
- What's lucky

374
00:14:49,166 --> 00:14:52,102
is that I have a best friend
who spent her Valentine's Day

375
00:14:52,136 --> 00:14:53,103
trying to find
me a date.

376
00:14:53,137 --> 00:14:55,138
Thank you.
I'm gonna go home.

377
00:14:55,172 --> 00:14:56,640
No. Stay.

378
00:14:56,674 --> 00:14:58,675
I just-- I'm gonna
go home and watch TV.

379
00:14:58,709 --> 00:15:00,777
Okay, well,
can you just do me a favor

380
00:15:00,811 --> 00:15:02,112
and give me your car keys
for a second?

381
00:15:02,146 --> 00:15:03,947
There's something I need
to look at on your car keys.

382
00:15:03,981 --> 00:15:05,482
You're gonna throw them
on the roof again

383
00:15:05,516 --> 00:15:07,350
- so I can't leave.
- You know me too well.

384
00:15:07,384 --> 00:15:10,937
You're the best.

385
00:15:10,988 --> 00:15:13,222
It sucks being alone
on Valentine's Day,

386
00:15:13,241 --> 00:15:15,625
so I'm gonna take Ann out
for a drink, you know.

387
00:15:15,660 --> 00:15:18,895
Try to cheer her up.
And I can be late meeting Ben.

388
00:15:18,913 --> 00:15:22,432
He's not gonna be on time,
'cause those clues are hard.

389
00:15:22,466 --> 00:15:24,868
They're really, really hard,
and I'm really worried

390
00:15:24,902 --> 00:15:27,137
that he's not gonna be able
to figure it out.

391
00:15:34,411 --> 00:15:36,680
Ann, you tricky bastard.

392
00:15:36,714 --> 00:15:39,082
- Leslie, what are you doing?
- Look at Ann.

393
00:15:39,116 --> 00:15:41,851
She's putting on makeup.
She using the emergency

394
00:15:41,886 --> 00:15:43,186
mini curling iron
that I gave her.

395
00:15:43,220 --> 00:15:45,472
- She has a date.
- You don't know that.

396
00:15:45,523 --> 00:15:48,124
Why wouldn't
she tell me who it is?

397
00:15:48,659 --> 00:15:49,426
Because she doesn't
want me to know.

398
00:15:49,460 --> 00:15:51,761
Because it's someone
she shouldn't be dating.

399
00:15:51,796 --> 00:15:54,431
Wait, the music's better.
Have you seen Chris?

400
00:15:54,465 --> 00:15:56,366
I don't know, maybe he's
in the bathroom or something.

401
00:15:57,900 --> 00:15:59,469
She's going
to meet Chris.

402
00:16:03,864 --> 00:16:06,551
Hey, I got nine,
three and four.

403
00:16:06,864 --> 00:16:10,951
I have 11, 12, 13, 14,
15, 16, 17, 18, and 19.

404
00:16:10,985 --> 00:16:12,018
Whoa.

405
00:16:12,053 --> 00:16:13,703
I got--
I got lucky.

406
00:16:13,754 --> 00:16:15,856
And I love riddles.

407
00:16:15,890 --> 00:16:19,926
Yo, found some clues.
Also found this.

408
00:16:19,961 --> 00:16:22,162
Weird stick.
Might be a clue.

409
00:16:22,196 --> 00:16:23,630
- No.
- Okay.

410
00:16:23,664 --> 00:16:25,532
Did you find
the 25th clue?

411
00:16:25,566 --> 00:16:27,801
No, I got #8,
and #22.

412
00:16:29,854 --> 00:16:31,007
[Phone ringing]

413
00:16:32,854 --> 00:16:34,007
What do you want, April?

414
00:16:34,041 --> 00:16:35,876
Leave Ann alone.
This is none of your business.

415
00:16:35,910 --> 00:16:38,411
After all the hoops that
Ben and I had to jump through

416
00:16:38,446 --> 00:16:40,213
because of the
boss-employee thing,

417
00:16:40,248 --> 00:16:42,115
it is absolutely
my business.

418
00:16:42,149 --> 00:16:46,019
Chris needs to explain himself,
and Ann lied to me

419
00:16:46,053 --> 00:16:49,372
about this date.
I mean, so many injustices.

420
00:16:49,407 --> 00:16:51,958
Ann would never do anything
to piss you off, Leslie.

421
00:16:51,993 --> 00:16:54,160
You guys are such
close friends, it's lame.

422
00:16:54,194 --> 00:16:56,563
Just have a nice night with Ben,
and forget about this.

423
00:16:56,597 --> 00:17:00,333
Okay, fine.
I will let it go for now.

424
00:17:00,368 --> 00:17:02,335
But I want you to know
that I think there is some--

425
00:17:02,369 --> 00:17:04,504
[beep]

426
00:17:04,538 --> 00:17:06,208
Okay, she hung up on me.

427
00:17:08,242 --> 00:17:10,176
We failed.

428
00:17:10,211 --> 00:17:12,095
If I just had
a little more time.

429
00:17:12,146 --> 00:17:14,047
She's waiting for me
somewhere in Pawnee,

430
00:17:14,065 --> 00:17:16,399
- and I'm not gonna be there.
- Wait...

431
00:17:16,434 --> 00:17:19,219
Leslie loves romance,
but she also loves being right.

432
00:17:19,253 --> 00:17:21,955
Is there something you used
to disagree on,

433
00:17:21,989 --> 00:17:24,024
but you've since come around
to her way of thinking?

434
00:17:25,558 --> 00:17:27,243
Ron, you're a genius.

435
00:17:31,732 --> 00:17:33,249
Little Sebastian.

436
00:17:33,284 --> 00:17:34,901
At first you
did not understand

437
00:17:34,936 --> 00:17:37,337
what made this
tiny horse so special,

438
00:17:37,371 --> 00:17:38,571
and now you love him
more than I do.

439
00:17:38,589 --> 00:17:41,508
Yep, I miss him
every day.

440
00:17:41,542 --> 00:17:43,510
I really tried
to make that hard.

441
00:17:43,544 --> 00:17:45,111
I'm very impressed
with you.

442
00:17:45,146 --> 00:17:46,479
Eh, some of them
were kind of tough.

443
00:17:46,514 --> 00:17:48,481
- [Laughing]
- So are you ready to go

444
00:17:48,516 --> 00:17:50,450
have our first ever
Valentine's dinner?

445
00:17:50,484 --> 00:17:52,319
Yes, but I have
to tell you something.

446
00:17:52,353 --> 00:17:55,588
Chris and Ann
are on a secret date.

447
00:17:55,623 --> 00:17:57,791
Which is crazy
because he's her boss,

448
00:17:57,825 --> 00:17:59,492
which is the exact
situation we were in,

449
00:17:59,527 --> 00:18:01,895
except we were put
on trial for it.

450
00:18:01,929 --> 00:18:02,896
And I know I'm not
supposed to care,

451
00:18:02,930 --> 00:18:03,997
and I'm supposed
to let it go,

452
00:18:04,031 --> 00:18:05,765
and we should just
have a romantic dinner--

453
00:18:05,783 --> 00:18:07,667
No, we have to go
catch them in the act.

454
00:18:07,702 --> 00:18:09,169
- Right now.
- Really?

455
00:18:09,203 --> 00:18:11,488
Yeah. Screw
romantic dinners.

456
00:18:11,539 --> 00:18:13,073
Let's go rub it
in their face.

457
00:18:13,107 --> 00:18:15,241
God, I love you
so much.

458
00:18:15,275 --> 00:18:17,276
I cannot wait
to see the look

459
00:18:17,294 --> 00:18:19,445
on Chris' remarkably
youthful face

460
00:18:19,463 --> 00:18:21,665
when we march in there
and confront him--

461
00:18:21,716 --> 00:18:23,350
Whoa, whoa,
whoa, whoa, Leslie.

462
00:18:25,970 --> 00:18:27,587
What?

463
00:18:27,621 --> 00:18:29,589
Ahh!

464
00:18:29,623 --> 00:18:30,590
- Shh.
- [Muffled cries]

465
00:18:30,624 --> 00:18:32,625
- Shh.
- April.

466
00:18:32,643 --> 00:18:34,627
- Hey.
- April.

467
00:18:34,645 --> 00:18:37,163
- Tom and Ann are on a date.
- I know.

468
00:18:37,198 --> 00:18:38,798
What do you mean you know?
How do you know?

469
00:18:38,833 --> 00:18:42,635
Because...
I set them up.

470
00:18:43,833 --> 00:18:45,635
I was watching
Ann with Tom at the dance

471
00:18:46,033 --> 00:18:47,935
and she seemed to
be having a good time.

472
00:18:48,533 --> 00:18:49,935
So I went over
to talk to her.

473
00:18:50,544 --> 00:18:52,812
What are you looking for
in a date anyway?

474
00:18:52,830 --> 00:18:54,981
I don't know.
What does anyone want?

475
00:18:55,016 --> 00:18:59,719
Just a nice, funny guy
who likes me and treats me well.

476
00:18:59,754 --> 00:19:02,622
Tom's funny,
and he'd treat you well.

477
00:19:02,656 --> 00:19:05,892
- Ha, ha.
- I'm serious.

478
00:19:05,926 --> 00:19:07,360
You want a good date,
why not ask out

479
00:19:07,395 --> 00:19:09,462
the only guy that's
made you smile tonight?

480
00:19:09,497 --> 00:19:11,097
He's ridiculous.

481
00:19:11,132 --> 00:19:13,466
- All that dumb swagger.
- It's not.

482
00:19:13,501 --> 00:19:16,503
He's sweet. You should just
ask him out for a drink.

483
00:19:16,537 --> 00:19:19,039
I'm 1,000% sure
he'd say yes.

484
00:19:19,073 --> 00:19:21,641
I bet you guys
would have a good time.

485
00:19:21,675 --> 00:19:24,310
Man, I need to, like,
wrap my head around this.

486
00:19:24,345 --> 00:19:25,545
Why? It makes
sense to me.

487
00:19:25,579 --> 00:19:28,381
This is a small, loser town
with loser people,

488
00:19:28,416 --> 00:19:30,917
and Tom's, like,
at least semi-cool.

489
00:19:30,951 --> 00:19:32,252
You know
what I'm hearing?

490
00:19:32,986 --> 00:19:36,356
You tried to make Ann happy
for Valentine's Day.

491
00:19:36,390 --> 00:19:38,992
- Ugh.
- You're a very nice person.

492
00:19:39,026 --> 00:19:39,993
- No.
- Yes, you are.

493
00:19:40,027 --> 00:19:42,028
- Very nice person.
- Bye.

494
00:19:44,665 --> 00:19:47,050
I do not understand this.
This really confuses me.

495
00:19:47,084 --> 00:19:48,601
Well, you do
love them both.

496
00:19:48,636 --> 00:19:52,589
Yeah, I love passionate
speakers and Italian men.

497
00:19:52,640 --> 00:19:53,940
Doesn't mean
I love Mussolini.

498
00:19:53,974 --> 00:19:55,391
You love
Italian men?

499
00:19:55,426 --> 00:19:57,544
Not as much as...

500
00:19:57,578 --> 00:20:00,547
Irish... Scottish?

501
00:20:00,581 --> 00:20:04,017
White... whatever you are.

502
00:20:04,051 --> 00:20:06,753
This is the weirdest
Valentine's Day ever.

503
00:20:10,257 --> 00:20:13,559
So, Ann,
it's finally happening.

504
00:20:13,577 --> 00:20:15,562
Dude, this is so close
to falling apart.

505
00:20:15,596 --> 00:20:17,197
Okay, okay,
I'm sorry.

506
00:20:17,731 --> 00:20:21,117
Can I be honest, though?
I'm a little freaked out.

507
00:20:21,168 --> 00:20:23,803
Like, I don't really
understand what's going on.

508
00:20:24,338 --> 00:20:25,772
This is
what's going on--

509
00:20:25,806 --> 00:20:27,040
we're gonna
have a drink,

510
00:20:27,074 --> 00:20:30,410
and we're gonna talk,
get to know each other.

511
00:20:30,444 --> 00:20:33,546
And then I am going
to drive home... alone.

512
00:20:33,581 --> 00:20:35,548
Cool. Consider this
alternate plan--

513
00:20:35,582 --> 00:20:37,217
we have drinks here, get to know
each other, whatever.

514
00:20:37,251 --> 00:20:40,753
Then we go back to my place
and snuggle up...

515
00:20:40,788 --> 00:20:42,222
Like little bunnies.

516
00:20:42,256 --> 00:20:44,257
- Yeah, this was a mistake.
- No!

517
00:20:46,667 --> 00:20:49,519
Leslie, thanks to your
annoying prying last year,

518
00:20:49,553 --> 00:20:51,621
I'm sure you remember
my birthday's coming up.

519
00:20:51,655 --> 00:20:53,723
I know, I know.
No parties, you hate parties.

520
00:20:53,758 --> 00:20:56,259
Correct.

521
00:20:56,293 --> 00:20:59,129
Oh, you know that
wild goose chase

522
00:20:59,163 --> 00:21:00,797
you sent Ben on
for Valentine's Day?

523
00:21:01,331 --> 00:21:02,531
The scavenger hunt?

524
00:21:02,566 --> 00:21:06,135
- I also do not want that.
- Yep, I understand.

525
00:21:06,169 --> 00:21:08,504
I absolutely do not want
to solve a series

526
00:21:08,538 --> 00:21:11,674
of riddles and clues, each more
intricate than the last.

527
00:21:11,708 --> 00:21:12,675
You understand
what I'm saying?

528
00:21:12,709 --> 00:21:14,810
- Yeah, I got it, Ron.
- Good.

529
00:21:18,315 --> 00:21:19,649
[Whistling]

530
00:21:19,683 --> 00:21:23,536
Uh, I-I do want that.
Please do that for me.

531
00:21:23,537 --> 00:21:27,560
<font color="#ff8c00">Sync & corrections by Alice</font>
<font color="#ff8c00">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
