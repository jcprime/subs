﻿1
00:00:01,619 --> 00:00:03,865
Business at <i>Rent-a-Swag</i>
could not be better.

2
00:00:03,866 --> 00:00:06,246
Yesterday Jaden Smith came in,
and he was like,

3
00:00:06,247 --> 00:00:09,385
"Look, I want to quit the music/acting
business and work here with you."

4
00:00:09,386 --> 00:00:11,183
And I was like,
"Jaden, be serious.

5
00:00:11,184 --> 00:00:12,734
The world needs you.
You have a gift."

6
00:00:12,735 --> 00:00:14,989
Ever since my client opened
his store across the street,

7
00:00:15,024 --> 00:00:16,448
your sales have plummeted.

8
00:00:16,449 --> 00:00:19,153
It's only a matter of time
before you're out of business.

9
00:00:19,459 --> 00:00:21,793
But my client has had
a moment of weakness,

10
00:00:21,828 --> 00:00:24,620
something he
referred to as "sympathy"?

11
00:00:25,004 --> 00:00:26,464
He's made you a final offer...

12
00:00:26,465 --> 00:00:29,251
$40,000 for <i>Rent-a-Swag</i>
and all of its contents.

13
00:00:29,252 --> 00:00:30,752
I suggest you take it.

14
00:00:30,787 --> 00:00:33,981
Well, I've heard all the facts, and
it's pretty obvious what I have to do...

15
00:00:33,982 --> 00:00:37,592
Get a big old mug of hot chocolate, put
on my thinking PJs, and get back to you.

16
00:00:37,627 --> 00:00:40,195
The offer is valid
for 48 hours.

17
00:00:40,229 --> 00:00:42,938
We would also be interested
in acquiring your thinking PJs.

18
00:00:42,939 --> 00:00:45,832
Listen to me
very carefully.

19
00:00:45,833 --> 00:00:47,269
No matter what happens,

20
00:00:47,320 --> 00:00:50,005
you will <i>never</i> acquire

21
00:00:50,039 --> 00:00:51,560
my thinking PJs

22
00:00:51,561 --> 00:00:54,943
or my YouTube blazer...

23
00:00:54,944 --> 00:00:57,445
Nonnegotiable.

24
00:01:00,531 --> 00:01:04,404
♪ Season 6, Episode 7
"Recall Vote"

25
00:01:05,327 --> 00:01:08,327
Synced by Reef
www.addic7ed.com

26
00:01:18,349 --> 00:01:19,672
Sorry. Sorry.

27
00:01:19,673 --> 00:01:22,008
Larry, everyone else was able to make it
to the committee meeting on time.

28
00:01:22,042 --> 00:01:23,242
What's your excuse?

29
00:01:23,243 --> 00:01:25,244
I was voting for you
in the recall election.

30
00:01:25,278 --> 00:01:27,379
Lots of people wearing
Leslie Knope buttons.

31
00:01:28,532 --> 00:01:31,553
No discussion about the recall election
until the polls close at 8:00.

32
00:01:31,585 --> 00:01:33,386
Let's focus
on what we can control.

33
00:01:33,420 --> 00:01:36,038
In 24 hours, we are going
to transform City Hall

34
00:01:36,073 --> 00:01:38,524
into the best haunted house
Pawnee has ever seen.

35
00:01:38,542 --> 00:01:39,992
There's been a mistake.

36
00:01:40,010 --> 00:01:42,562
This says that I'm in charge
of the silly skeleton room,

37
00:01:42,596 --> 00:01:45,581
when really it should say
"the undead serial killer room."

38
00:01:45,632 --> 00:01:49,602
- Who do I talk to about changing this?
- It's recall day. I've made my case,

39
00:01:49,603 --> 00:01:52,071
and the latest polls have it
as a dead heat,

40
00:01:52,089 --> 00:01:54,528
so there's nothing
left to do but wait.

41
00:01:54,529 --> 00:01:58,048
I'm fine. I mean, I got stuff to do.
I'm planning a haunted house.

42
00:01:58,082 --> 00:02:00,067
Plus, um...

43
00:02:00,101 --> 00:02:02,517
It's Kevin Pollack’s birthday.

44
00:02:02,518 --> 00:02:04,171
That's something
to focus on today.

45
00:02:04,205 --> 00:02:07,785
Who cares about the recall?
It's Kevin's day.

46
00:02:08,226 --> 00:02:09,642
I don't know what to do...

47
00:02:09,661 --> 00:02:12,863
I could take the offer and break even,
but I've come so far.

48
00:02:12,897 --> 00:02:17,369
Ron, can you put some more tiny
marshmallow in my hot chocky?

49
00:02:18,433 --> 00:02:19,583
Thank you.

50
00:02:19,584 --> 00:02:22,189
If there was something
I could do for you, I would.

51
00:02:22,223 --> 00:02:23,991
Maybe you should try
taking a walk...

52
00:02:24,025 --> 00:02:25,325
Out of my office.

53
00:02:27,045 --> 00:02:30,647
- Ron! You're in <i>Bloosh!</i>
- What?

54
00:02:30,682 --> 00:02:33,500
- Ron's in <i>Bloosh?</i>
- Ron is in <i>Bloosh!</i>

55
00:02:35,202 --> 00:02:36,544
What is <i>Bloosh?</i>

56
00:02:36,610 --> 00:02:39,696
<i>Bloosh</i> is a weekly lifestyle
email written by Annabel Porter.

57
00:02:39,697 --> 00:02:41,970
She used to be the face
of the Eagleton phone book.

58
00:02:41,999 --> 00:02:43,583
Then she moved to Hollywood
to pursue her dream

59
00:02:43,634 --> 00:02:45,802
of becoming friends
with a bunch of celebrities.

60
00:02:45,836 --> 00:02:48,154
Then she moved back
to become a lifestyle guru.

61
00:02:48,189 --> 00:02:49,394
She's legit.

62
00:02:49,395 --> 00:02:52,090
She spent four months living
in Kate Bosworth's pool house.

63
00:02:52,091 --> 00:02:56,529
"A Ron Swanson-designed chair is
<i>the</i> must-have item of the season,"

64
00:02:56,564 --> 00:03:00,306
"along with red quinoa, wind therapy,
and buying an island."

65
00:03:00,326 --> 00:03:01,880
Annabel says
that buying an island

66
00:03:01,881 --> 00:03:05,730
is the only real way to know that
your goji-berry farm is pesticide-free.

67
00:03:05,773 --> 00:03:08,541
I've been trying to get
Rent-a-Swag in <i>Bloosh</i> for months.

68
00:03:08,592 --> 00:03:12,224
And now that you got in, I can get in too.
Congratulations, Ron! We did it!

69
00:03:12,225 --> 00:03:15,327
- Someone's getting a new leather jacket.
- I don't want a leather jacket.

70
00:03:15,361 --> 00:03:17,784
It's for me.
This is a great day!

71
00:03:18,281 --> 00:03:19,898
Ron's in <i>Bloosh!</i>

72
00:03:19,933 --> 00:03:21,934
- Ron's in <i>Bloosh!</i>
<i>- What?</i>

73
00:03:21,985 --> 00:03:23,118
Ron's in <i>Bloosh!</i>

74
00:03:23,153 --> 00:03:25,120
- Do it.
- Ron's in <i>Bloosh!</i>

75
00:03:27,533 --> 00:03:31,376
April, you seem depressed,
and I would know.

76
00:03:31,377 --> 00:03:34,162
I spent most of last year being
treated by Dr. Richard Nygard

77
00:03:34,163 --> 00:03:35,597
for my own
emotional problems.

78
00:03:35,632 --> 00:03:37,349
Yeah, I just miss Andy.

79
00:03:37,400 --> 00:03:40,452
Halloween was, like,
kind of our thing.

80
00:03:40,453 --> 00:03:43,496
Every year, we would dress up
like demons and egg Larry's house.

81
00:03:43,497 --> 00:03:45,390
- That was you?
- Larry, please.

82
00:03:45,425 --> 00:03:46,642
We're having
a private conversation.

83
00:03:46,676 --> 00:03:48,911
Well, I'd like to make Halloween
fun for you,

84
00:03:48,945 --> 00:03:51,288
like Andy would have,
starting right now...

85
00:03:51,289 --> 00:03:53,265
With scary,
scary monster claws!

86
00:03:53,267 --> 00:03:56,652
Okay, people, the grand
opening is in 20 hours.

87
00:03:56,686 --> 00:03:59,288
Larry, you had
the easiest job...

88
00:03:59,322 --> 00:04:00,489
Two triangles and a tooth.

89
00:04:00,507 --> 00:04:03,475
Nobody wants Gengurch-family-themed
Jack-o-lanterns.

90
00:04:03,476 --> 00:04:06,161
- Gayle likes them.
- Is any of this even good?

91
00:04:06,196 --> 00:04:08,480
Why didn't we call it
"City Hall-oween"?

92
00:04:08,515 --> 00:04:10,516
Okay, we need to change
every single banner.

93
00:04:10,550 --> 00:04:12,100
Leslie's been holding it
together pretty well,

94
00:04:12,134 --> 00:04:14,369
but today is when
she needs us the most.

95
00:04:14,404 --> 00:04:17,840
That's why we formed the Leslie
Knope emotional support task force.

96
00:04:17,874 --> 00:04:20,676
Anything she could possibly need
to get her through these final hours,

97
00:04:20,710 --> 00:04:22,411
we got it covered...

98
00:04:22,445 --> 00:04:26,348
Back rubs, YouTube videos of turtles
and birds becoming friends...

99
00:04:26,349 --> 00:04:30,285
A poster announcing
the new Lilith Fair concert.

100
00:04:30,286 --> 00:04:32,064
It's fake,
but it'll buy us an hour.

101
00:04:32,065 --> 00:04:35,020
We even have a secret hand signal
in case we think she needs help.

102
00:04:36,226 --> 00:04:38,777
It's loosely based
on the Klingon greeting salute.

103
00:04:38,828 --> 00:04:42,114
I just learned that. And if I had known,
I would not have agreed to it.

104
00:04:42,148 --> 00:04:46,694
Uh, hey, Leslie, how about instead of
slightly changing a lot of banners,

105
00:04:46,695 --> 00:04:49,697
we paint our toenails
to look like pumpkins?

106
00:04:49,739 --> 00:04:52,040
Fun!
Ann, you're a genius!

107
00:04:52,074 --> 00:04:55,044
Your brain is almost as perfect
as your face.

108
00:04:55,929 --> 00:04:58,929
Hey, Ron, baby, what are
the hot deets on <i>Bloosh?</i>

109
00:04:58,930 --> 00:05:01,216
Start over
and speak differently.

110
00:05:01,251 --> 00:05:03,719
Sorry. Did you call that P.R. guy?
What's the latest news?

111
00:05:03,753 --> 00:05:06,905
Apparently, Joan Callamezzo
wants me to go on <i>Pawnee Today</i>

112
00:05:06,940 --> 00:05:09,258
to discuss my chairs
with this Porter woman.

113
00:05:09,292 --> 00:05:10,509
I said no.

114
00:05:10,543 --> 00:05:13,095
Ron, you said if there was something
you could do to help me, you would.

115
00:05:13,129 --> 00:05:15,697
Getting in <i>Bloosh</i> is my last
chance to save my store.

116
00:05:15,731 --> 00:05:17,399
You have to go and take me
so I can meet her.

117
00:05:17,417 --> 00:05:18,834
- Please?
- Fine.

118
00:05:18,868 --> 00:05:21,086
If it's that important
to your company, I'll do it.

119
00:05:21,120 --> 00:05:22,437
Yes!

120
00:05:22,472 --> 00:05:26,942
Now, I know high-end, Internet-only
lifestyle magazine really isn't your scene,

121
00:05:26,960 --> 00:05:30,312
so Donna and I wrote up
some cocktail banter

122
00:05:30,346 --> 00:05:34,149
and some conversation snippets
for you to practice so you can fit in.

123
00:05:34,183 --> 00:05:38,887
"Annabel, could I be more <i>jels</i>
of your low-lights right now?"

124
00:05:38,922 --> 00:05:42,495
"I mean, O.M.Squee,
talk about bangs envy."

125
00:05:42,496 --> 00:05:44,631
Oof, you got a long way
to go, Swanson.

126
00:05:44,691 --> 00:05:48,067
- Let's go again, from the top.
- I regret everything.

127
00:05:48,157 --> 00:05:50,758
- Babe, you made it. It's 8:00.
- Thank God.

128
00:05:50,759 --> 00:05:53,594
Okay, everyone,
great work, as always.

129
00:05:53,595 --> 00:05:57,239
I'm gonna stay here and just kind
of wait for the results to pour in.

130
00:05:57,273 --> 00:05:58,957
Thank you.
I'll see you all tomorrow.

131
00:05:59,008 --> 00:06:01,190
We're not going anywhere.
We're staying with you.

132
00:06:01,210 --> 00:06:03,312
No, really, it's gonna be
hours before they call it.

133
00:06:03,346 --> 00:06:06,231
We're not leaving, woman.
Stop trying to get rid of us.

134
00:06:06,266 --> 00:06:09,067
Yeah, I made frappuccinos
for a little caffeine boost,

135
00:06:09,118 --> 00:06:10,619
and Larry went to get pizzas
for everyone.

136
00:06:10,653 --> 00:06:12,654
Aw, that's very sweet,
you guys.

137
00:06:12,688 --> 00:06:13,855
Thank you.

138
00:06:13,873 --> 00:06:16,875
All right, let's hunker down.
It's gonna be a long night.

139
00:06:19,662 --> 00:06:22,331
<i>Too close to call,
a real nail-biter,</i>

140
00:06:22,365 --> 00:06:23,732
<i>anybody's game...</i>

141
00:06:23,766 --> 00:06:26,034
<i>These are three phrases
you won't hear tonight,</i>

142
00:06:26,069 --> 00:06:28,470
<i>as Leslie Knope has been voted
out of office.</i>

143
00:06:28,504 --> 00:06:30,493
<i>What was expected to be
a close race...</i>

144
00:06:30,508 --> 00:06:31,524
What did he say?

145
00:06:31,525 --> 00:06:33,091
<i>...Has actually been
a not close race.</i>

146
00:06:33,142 --> 00:06:36,445
<i>Pawnee voters have decisively
removed Leslie Knope...</i>

147
00:06:36,479 --> 00:06:38,313
- Honey, I'm so sorry.
<i>- ...From the City Council.</i>

148
00:06:38,348 --> 00:06:41,183
♪ I got-a the pizza

149
00:06:41,217 --> 00:06:43,068
pizza time,
it's-a pizza time!

150
00:06:43,102 --> 00:06:45,237
Larry, everyone is
miserable,

151
00:06:45,271 --> 00:06:47,605
and you are
only making it worse.

152
00:06:50,341 --> 00:06:52,514
- Okay, Leslie's on her way in.
- Is she okay?

153
00:06:52,568 --> 00:06:53,866
I left her, like,
30 voice mails,

154
00:06:53,867 --> 00:06:57,770
and her outgoing message is just her
sobbing and burping and crazy laughing.

155
00:06:57,804 --> 00:07:00,745
Yeah, you know what?
She's actually handling it pretty well.

156
00:07:01,391 --> 00:07:03,225
What's up, fartwads?

157
00:07:03,260 --> 00:07:05,144
Are you eating
a paunch burger?

158
00:07:05,162 --> 00:07:07,446
Well, you know what?
If you can't beat 'em, join 'em.

159
00:07:07,464 --> 00:07:11,049
I mean, I've been boycotting them
ever since Pinewood and her goons

160
00:07:11,067 --> 00:07:13,886
started this whole recall business,
but who cares now, right?

161
00:07:13,904 --> 00:07:16,405
What does it matter?
Nothing matters anymore.

162
00:07:16,439 --> 00:07:18,524
Mmm. Oh, my God.
This is good.

163
00:07:18,558 --> 00:07:20,259
This is really good.

164
00:07:20,293 --> 00:07:21,944
Why did I ever fight this?

165
00:07:21,995 --> 00:07:23,995
Have you thought
about a concession speech?

166
00:07:23,997 --> 00:07:25,497
Oh, concession speech?

167
00:07:25,515 --> 00:07:28,239
Yeah, I wrote one.

168
00:07:29,119 --> 00:07:31,875
"Eat my shorts, Jabronies.
Knope, out."

169
00:07:31,876 --> 00:07:35,440
I thought it best to revisit
the concession speech at a later time.

170
00:07:36,614 --> 00:07:39,011
But we should focus
on the haunted house.

171
00:07:39,026 --> 00:07:41,213
And you know what I think
it needs, actually,

172
00:07:41,231 --> 00:07:45,150
is a papier-mâché
Li'l Sebastian!

173
00:07:45,168 --> 00:07:46,852
- What?
- Oh!

174
00:07:49,990 --> 00:07:52,925
I'm gonna go fall asleep
on a bench.

175
00:07:57,981 --> 00:07:59,865
I can honestly say

176
00:07:59,899 --> 00:08:03,233
today's guest is the only person
in this town

177
00:08:03,234 --> 00:08:06,201
who's a bigger deal than I am.

178
00:08:06,656 --> 00:08:10,893
Please welcome lifestyle guru
Annabel Porter.

179
00:08:10,944 --> 00:08:14,413
Hello, hello.
It's me. I'm here.

180
00:08:14,447 --> 00:08:16,498
- Yes, you are.
- Hi.

181
00:08:16,550 --> 00:08:18,500
Can I just say,
as a journalist,

182
00:08:18,535 --> 00:08:21,170
I feel like
we're best friends.

183
00:08:21,204 --> 00:08:23,172
Tell the audience
about yourself.

184
00:08:23,206 --> 00:08:28,160
Well, I'm just a simple former
phonebook model who found her calling.

185
00:08:28,195 --> 00:08:30,296
- You know, I'm not perfect.
- Oh.

186
00:08:30,330 --> 00:08:33,065
The average woman worries about
what she looks like in a bathing suit.

187
00:08:33,099 --> 00:08:36,001
So does my nanny.
So I get it.

188
00:08:36,036 --> 00:08:37,720
Annabel, what trends are...

189
00:08:37,754 --> 00:08:40,443
Pardon the pun, but...
Trending?

190
00:08:40,444 --> 00:08:44,943
First of all, Mozambique cashmere
is the new cast-iron stove.

191
00:08:44,961 --> 00:08:46,411
Of course.

192
00:08:46,429 --> 00:08:49,698
I have found some amazing new
conflict-free paella recipes,

193
00:08:49,733 --> 00:08:53,703
and, luckily, my favorite fishmonger
now makes house calls.

194
00:08:53,704 --> 00:08:55,579
- Thank God.
- Finally.

195
00:08:55,580 --> 00:09:01,010
But my fave new item
is a deliciously bold chair...

196
00:09:01,044 --> 00:09:03,484
- Ooh.
- By local artist Ron Swanson.

197
00:09:03,485 --> 00:09:04,935
Ron Swanson is here.

198
00:09:04,994 --> 00:09:09,301
He will be joining us after
the break, so stick around.

199
00:09:09,336 --> 00:09:12,504
Did that woman
call my chair "delicious"?

200
00:09:12,522 --> 00:09:14,657
Whoo, Happy Halloween.

201
00:09:14,708 --> 00:09:16,675
Oh, caterpillar!
Thank you.

202
00:09:16,710 --> 00:09:18,761
Hey, guys, come on in.

203
00:09:18,812 --> 00:09:22,331
- Welcome to the den of horrors.
- Where's all the scary stuff?

204
00:09:22,365 --> 00:09:24,133
Scary stuff
is invisible, Leah...

205
00:09:24,167 --> 00:09:27,552
Broken dreams, disappointment,
achieving your greatest goal

206
00:09:27,570 --> 00:09:29,071
and having it
all fall apart

207
00:09:29,122 --> 00:09:31,123
and knowing that you'll never
climb any higher.

208
00:09:31,157 --> 00:09:32,841
Can we have some candy?

209
00:09:32,876 --> 00:09:34,810
I'm gonna give you something
sweeter than candy, Ryan...

210
00:09:34,844 --> 00:09:35,961
The truth.

211
00:09:35,996 --> 00:09:39,097
It's very possible that some
of you have already peaked.

212
00:09:39,115 --> 00:09:41,216
It's all downhill
from here, turkeys.

213
00:09:41,251 --> 00:09:43,686
Okay, let's say good-bye
to Ms. Knope, kids.

214
00:09:43,737 --> 00:09:44,820
It is Ms. Knope.

215
00:09:44,904 --> 00:09:47,706
It's not city councilwoman Knope, because
that chapter of my life is already over.

216
00:09:47,740 --> 00:09:50,042
Just remember, kids,

217
00:09:50,060 --> 00:09:51,977
nothing gold can stay.

218
00:09:52,011 --> 00:09:53,696
Okay.

219
00:09:53,747 --> 00:09:57,716
Oh, my goodness, we are back
with America's greatest leader...

220
00:09:57,751 --> 00:09:59,318
Annabel Porter

221
00:09:59,352 --> 00:10:01,820
and her new favorite artisan,

222
00:10:01,855 --> 00:10:04,423
local woodworker
Ron Swanson.

223
00:10:04,457 --> 00:10:05,708
Hello.

224
00:10:05,742 --> 00:10:08,227
I discovered Ron's chairs
a few months ago,

225
00:10:08,261 --> 00:10:11,880
and what I absolutely love
about a Swanson

226
00:10:11,915 --> 00:10:14,066
is you can really use it
for anything.

227
00:10:14,100 --> 00:10:16,719
Yes, mostly you use it
for sitting.

228
00:10:16,770 --> 00:10:19,638
Make it a rustic accent piece
in your solarium.

229
00:10:19,656 --> 00:10:22,884
Even better, use it as a focal point
in your yoga tent.

230
00:10:22,885 --> 00:10:25,094
Put it by a table
and eat a meal.

231
00:10:25,145 --> 00:10:28,030
We don't do
<i>meals</i> in my home.

232
00:10:28,064 --> 00:10:32,067
No, every two hours, we eat
what I like to call a food tease,

233
00:10:32,102 --> 00:10:35,104
like an oat wedge
or a seaweed lozenge.

234
00:10:35,138 --> 00:10:36,355
Can I just say...

235
00:10:36,389 --> 00:10:38,507
And I think this is
really important,

236
00:10:38,541 --> 00:10:41,543
so I need everyone
to shut up...

237
00:10:41,594 --> 00:10:45,097
- I love your hair.
- Oh. Thank you.

238
00:10:45,131 --> 00:10:47,032
It's genetic
and unattainable.

239
00:10:50,870 --> 00:10:53,305
I am scaring you!

240
00:10:53,339 --> 00:10:54,890
I could see you.

241
00:10:54,941 --> 00:10:57,309
There's, like, a million mirrors
in front of me.

242
00:10:57,327 --> 00:10:59,913
And you're never gonna make me
not miss Andy, okay?

243
00:10:59,914 --> 00:11:03,915
What about this? I'm Bert Macklin,
friendly lifeguard.

244
00:11:03,933 --> 00:11:07,586
Bert Macklin is not a lifeguard.
He's an FBI agent.

245
00:11:07,620 --> 00:11:11,390
- Really? That's even harder to believe.
- Just stop, okay?

246
00:11:11,424 --> 00:11:14,226
Your lame attempts at trying
to cheer me up are not working.

247
00:11:14,260 --> 00:11:18,247
Now I'm sad that I miss Andy and sad
that I can't make fun of you to Andy.

248
00:11:18,298 --> 00:11:20,849
I understand. I'm sorry.

249
00:11:20,884 --> 00:11:24,521
I've failed you, both
as a scary monster and a friend.

250
00:11:34,948 --> 00:11:38,116
Hey, I am really sorry.
I know you must feel awful.

251
00:11:38,134 --> 00:11:39,635
I don't feel awful.

252
00:11:39,686 --> 00:11:42,271
You know, actually,
I feel like I can finally relax.

253
00:11:42,305 --> 00:11:44,556
Accepting that you've peaked
is very freeing.

254
00:11:44,574 --> 00:11:45,824
Yeah, I know the feeling.

255
00:11:45,858 --> 00:11:48,527
I was impeached when I was 18,
and it was brutal.

256
00:11:48,545 --> 00:11:51,313
But you just have to keep
climbing back up that hill.

257
00:11:51,347 --> 00:11:53,198
Or do you?

258
00:11:53,233 --> 00:11:56,585
Maybe instead of working
super hard to rebuild your life,

259
00:11:56,619 --> 00:12:01,039
you just said, "I peaked.
Wow. What a great run."

260
00:12:01,057 --> 00:12:03,859
"And now I'm going to spend
the rest of my life really chill."

261
00:12:03,910 --> 00:12:07,054
Well, sure, I mean, part of that
sounds very appealing.

262
00:12:07,055 --> 00:12:08,931
- Yeah.
- But, n-no.

263
00:12:08,982 --> 00:12:11,049
Uh, see, i-it was important
for me...

264
00:12:11,067 --> 00:12:13,518
See, I took a job in City
budget crisis management,

265
00:12:13,536 --> 00:12:16,941
and now I work
for a candy charity.

266
00:12:17,816 --> 00:12:19,641
Oh, God, did I peak
when I was 18?

267
00:12:19,692 --> 00:12:22,093
- There it is.
- Drink up.

268
00:12:24,948 --> 00:12:26,148
- Whoo!
- Shots!

269
00:12:28,268 --> 00:12:30,853
B-13 shots.

270
00:12:30,904 --> 00:12:32,654
Bird bath salts.

271
00:12:32,705 --> 00:12:35,657
Wha...?
Champagne decanters!

272
00:12:35,708 --> 00:12:37,926
This is heaven.

273
00:12:37,961 --> 00:12:39,561
I love your chair.

274
00:12:39,596 --> 00:12:41,613
I need 12 for my stepdaughter's
craft room.

275
00:12:41,648 --> 00:12:44,566
Well, I make two a year,
so maybe in six years.

276
00:12:44,567 --> 00:12:46,134
- Can you put me on the wait list?
- Me too...

277
00:12:46,160 --> 00:12:49,004
Unless your chair was ever touched
by someone who ate refined sugar.

278
00:12:49,055 --> 00:12:51,968
Then I can't buy any of your chairs,
and I protest this entire event.

279
00:12:51,969 --> 00:12:53,859
Okay, Ron,
Annabel’s coming over.

280
00:12:53,893 --> 00:12:57,288
When you talk me up, be sure
to mention that you really admire

281
00:12:57,314 --> 00:13:00,215
how I take big risks
with men's neck accessories.

282
00:13:00,250 --> 00:13:03,051
- There he is.
- Hello, hello.

283
00:13:03,102 --> 00:13:06,188
Thank you for your kind words
about my chairs, Miss Porter.

284
00:13:06,239 --> 00:13:07,439
No, thank us.

285
00:13:07,457 --> 00:13:10,342
You are about to make
a ton of money.

286
00:13:10,376 --> 00:13:11,927
I want to license
your designs.

287
00:13:11,978 --> 00:13:15,948
We are going to put Swanson chairs
in every six-bedroom home in the Midwest.

288
00:13:15,982 --> 00:13:19,051
If my chairs were mass-produced,
they wouldn't be Swanson chairs.

289
00:13:19,085 --> 00:13:21,620
Swanson chairs are handmade.

290
00:13:21,638 --> 00:13:24,723
Yes, and now they will be made
by thousands of tiny Chinese hands.

291
00:13:25,775 --> 00:13:29,611
Oh, Miss Porter, this is
my very good friend Tom Haverford.

292
00:13:29,646 --> 00:13:33,382
He's a big fan of your...
Whatever you call what you do.

293
00:13:33,416 --> 00:13:35,700
Please speak to each other
while I leave.

294
00:13:35,718 --> 00:13:39,209
Oh, no, I am not done with you,
you wicked little so-and-so.

295
00:13:39,210 --> 00:13:40,440
Terrific.

296
00:13:40,488 --> 00:13:43,325
You know what? I had my time, you know?
I did my thing.

297
00:13:43,359 --> 00:13:46,643
I wanted to be a city councilor, and
I did, and I was, and I was like, whoo!

298
00:13:46,644 --> 00:13:48,197
And the people were like,
pffft!

299
00:13:48,248 --> 00:13:52,084
- At least we have each other.
- I love you so much.

300
00:13:52,118 --> 00:13:53,785
I love you too.

301
00:13:53,803 --> 00:13:55,053
- This is real.
- I know.

302
00:13:55,088 --> 00:13:56,770
- You're my husband.
- You're my wife.

303
00:13:56,789 --> 00:13:59,224
- You married me in front of people.
- I did. I was there.

304
00:13:59,259 --> 00:14:00,509
We may have peaked,
but it doesn't matter.

305
00:14:00,560 --> 00:14:02,761
We need to do something big,
you know,

306
00:14:02,795 --> 00:14:04,162
something that really lets
people know

307
00:14:04,197 --> 00:14:05,931
that we're gonna love
each other forever.

308
00:14:05,965 --> 00:14:08,901
- Yeah! What is it?
- I got it.

309
00:14:08,935 --> 00:14:11,636
Hello, sir.
We are sober.

310
00:14:11,637 --> 00:14:14,172
We would like to get tattoos
on our bodies, 'cause we love each other.

311
00:14:14,190 --> 00:14:16,258
Tattoos?
This is a pawnshop.

312
00:14:16,292 --> 00:14:18,744
- But, yeah, sure, I can do that.
- Awww!

313
00:14:18,778 --> 00:14:20,746
Yes! It happened!

314
00:14:20,780 --> 00:14:23,532
Look what he's doing.
He's breaking pens.

315
00:14:23,583 --> 00:14:27,119
- This is the idea of the century.
- Right here.

316
00:14:28,821 --> 00:14:30,822
Come here.

317
00:14:32,665 --> 00:14:36,645
My store is called <i>Rent-a-Swag.</i>
It's a clothing rental service for teens.

318
00:14:36,679 --> 00:14:38,609
It's an interesting idea.
Are you guys doing well?

319
00:14:38,610 --> 00:14:40,406
- We're doing amazing.
- Mm.

320
00:14:40,407 --> 00:14:42,327
And it'd be perfect
for <i>Bloosh.</i>

321
00:14:42,335 --> 00:14:44,631
Why don't you take out whatever
you wrote about my chairs

322
00:14:44,666 --> 00:14:47,750
and put something in
about Rent-a-Swag instead?

323
00:14:47,751 --> 00:14:51,397
This month's trends are final.
I can't just throw a trend out.

324
00:14:51,559 --> 00:14:56,158
But my <i>Bloosh</i> scouts have started
trend-tracking for the February zine blast.

325
00:14:56,159 --> 00:14:59,078
- Good God, these words.
- I'll have them stop by your store.

326
00:14:59,112 --> 00:15:01,098
That'll be too late.
We'll be out of business by then.

327
00:15:01,106 --> 00:15:02,690
I thought you said
you were doing well.

328
00:15:02,741 --> 00:15:06,343
Ronnie, one last time, have you
reconsidered mass production?

329
00:15:06,378 --> 00:15:09,214
I can sell your chairs
on my site.

330
00:15:09,215 --> 00:15:10,349
No, thank you, ma'am.

331
00:15:10,383 --> 00:15:13,669
I'm not interested in letting
someone else sell my work.

332
00:15:13,704 --> 00:15:16,017
Well, that's too bad.

333
00:15:17,057 --> 00:15:20,977
This party is over.
It no longer is.

334
00:15:21,011 --> 00:15:22,144
It was.

335
00:15:22,179 --> 00:15:26,160
- It was...? That was my last shot.
- Sorry, Tommy.

336
00:15:26,161 --> 00:15:29,218
Let's just grab some of that
lychee body oil and bounce.

337
00:15:29,219 --> 00:15:31,470
Can you grab me some of those
pomegranate face masks too?

338
00:15:31,488 --> 00:15:33,151
Fo' sho'.

339
00:15:33,373 --> 00:15:35,007
Can I ask you a question?

340
00:15:35,042 --> 00:15:36,642
Are we the cutest couple
you've ever seen?

341
00:15:36,676 --> 00:15:37,943
Nope.

342
00:15:37,977 --> 00:15:40,092
Okay, you want the long needle
or the short needle?

343
00:15:40,093 --> 00:15:42,515
Doesn't matter. Can't find
a short needle. Long needle it is.

344
00:15:42,549 --> 00:15:43,882
I have a big idea.

345
00:15:43,900 --> 00:15:47,086
I want a portrait
of Eleanor Roosevelt,

346
00:15:47,120 --> 00:15:48,504
tastefully done,

347
00:15:48,538 --> 00:15:52,808
and then she has a very classy
tattoo of Pat Benatar on her arm.

348
00:15:52,859 --> 00:15:54,560
And that's very subtle.

349
00:15:54,578 --> 00:15:57,346
No, scratch that. Scratch all that.
Just write "Ben".

350
00:15:57,381 --> 00:15:58,964
Do you have anything
to numb the pain?

351
00:15:58,982 --> 00:16:01,900
Sure, take a scoop
out of the pill bucket.

352
00:16:01,918 --> 00:16:04,536
Okay, you said you wanted
"mouth queen"?

353
00:16:04,554 --> 00:16:07,023
No, stop. Stop it. Stop
everything that you're doing.

354
00:16:07,057 --> 00:16:09,858
- Stop it.
- Ann's here! Ann wants a tattoo!

355
00:16:09,860 --> 00:16:10,926
- Ann!
- No, no!

356
00:16:10,961 --> 00:16:12,211
- Ann!
- No!

357
00:16:12,229 --> 00:16:15,581
Shh! Ann does not want a tattoo.
No one wants a tattoo.

358
00:16:15,615 --> 00:16:17,116
Ann, relax.

359
00:16:17,134 --> 00:16:19,301
Be "respitable"
for a second, okay?

360
00:16:19,353 --> 00:16:21,037
What we are doing
is not nuts.

361
00:16:21,071 --> 00:16:22,321
We are merely getting tattoos

362
00:16:22,355 --> 00:16:25,057
so that everybody knows we love each
other and that we haven't peaked.

363
00:16:25,092 --> 00:16:28,444
- It makes a lot of sense.
- Okay, we're leaving, all right?

364
00:16:28,495 --> 00:16:30,396
So say good-bye
to the nice man

365
00:16:30,414 --> 00:16:32,998
and thank me for saving you
from being on <i>Dateline.</i>

366
00:16:33,032 --> 00:16:34,233
Okay, let's go.
Come on.

367
00:16:34,267 --> 00:16:36,902
- Good-bye, world traveler.
- Later, bro Heisen.

368
00:16:36,920 --> 00:16:39,088
- Go.
- Come on.

369
00:16:40,924 --> 00:16:43,159
I don't want to be
a failure again, Ron.

370
00:16:43,193 --> 00:16:45,327
There's no shame
in failure

371
00:16:45,379 --> 00:16:46,745
if you gave it
an honest effort.

372
00:16:46,763 --> 00:16:48,047
Easy for you to say.

373
00:16:48,081 --> 00:16:49,665
People are dying to give you
money for your business,

374
00:16:49,699 --> 00:16:50,849
and you didn't even take it.

375
00:16:50,867 --> 00:16:54,787
I didn't sell my chairs to that website
because I value my name.

376
00:16:54,821 --> 00:16:57,390
The only thing that's important
at the end of the day

377
00:16:57,424 --> 00:16:59,825
is what's on your gravestone...
Your name.

378
00:16:59,826 --> 00:17:01,794
My gravestone's gonna be
a 60-inch touchscreen

379
00:17:01,828 --> 00:17:03,145
with a hologram
of four mes

380
00:17:03,196 --> 00:17:06,682
singing <i>End of the Road</i>
by <i>Boyz II Men...</i>

381
00:17:06,716 --> 00:17:08,050
But point taken.

382
00:17:08,101 --> 00:17:12,571
Ben, what is the first rule of the Leslie
Knope emotional support task force?

383
00:17:12,606 --> 00:17:14,290
Don't let her get
a tattoo.

384
00:17:14,324 --> 00:17:16,415
- What the hell happened?
- I couldn't help it.

385
00:17:16,443 --> 00:17:19,978
She's so cute when she's coming up
with destructive ideas.

386
00:17:19,996 --> 00:17:22,498
Never send a husband
to do a best friend's job.

387
00:17:22,532 --> 00:17:23,966
Out, Wyatt.
Walk it off.

388
00:17:24,017 --> 00:17:26,569
- Walk it off.
- Fine.

389
00:17:27,797 --> 00:17:29,232
- You did it.
- Yep.

390
00:17:29,289 --> 00:17:31,340
- See you later, Ann.
- No, Knope, sit down.

391
00:17:32,676 --> 00:17:36,729
- You are avoiding your problems.
- What? No, I'm not.

392
00:17:36,763 --> 00:17:39,003
- Can we talk about this tomorrow?
- You were down in the polls.

393
00:17:39,004 --> 00:17:41,716
Your opponents were well funded
and well organized.

394
00:17:41,717 --> 00:17:43,652
You must have known
this would be a possibility.

395
00:17:43,653 --> 00:17:46,605
Intellectually, I knew
that I might be in trouble,

396
00:17:46,623 --> 00:17:48,757
but deep in my heart,
I never really thought...

397
00:17:48,792 --> 00:17:50,359
Ben and I tried
to help you,

398
00:17:50,393 --> 00:17:52,975
but as usual, you're
the best person for the job.

399
00:17:52,976 --> 00:17:55,545
You need to hear yourself
read this.

400
00:17:57,482 --> 00:18:00,068
"They held the recall
election, and I lost."

401
00:18:01,569 --> 00:18:03,620
"I was voted out of office."

402
00:18:03,638 --> 00:18:07,044
"In 30 days, I will no longer be
a Pawnee city councilor."

403
00:18:07,045 --> 00:18:08,662
Oh, it's so hard to read
when you're drunk.

404
00:18:08,680 --> 00:18:10,044
Keep going.

405
00:18:10,065 --> 00:18:11,773
"But I am Leslie Knope."

406
00:18:11,774 --> 00:18:14,309
"I am more
than a city councilor."

407
00:18:14,424 --> 00:18:17,726
"I am an unstoppable force
of energy."

408
00:18:17,727 --> 00:18:23,198
"And I will use those days
to work as hard as I can."

409
00:18:23,214 --> 00:18:25,696
These are all
your ongoing projects,

410
00:18:25,697 --> 00:18:28,053
everything you're currently
working on for Pawnee.

411
00:18:28,087 --> 00:18:30,055
You have things to do.

412
00:18:30,089 --> 00:18:32,224
You have a month left.
Use it.

413
00:18:32,258 --> 00:18:35,093
Ann, you poetic
and noble land mermaid,

414
00:18:35,144 --> 00:18:37,162
you're right once again.

415
00:18:37,163 --> 00:18:38,222
Thank you.

416
00:18:38,223 --> 00:18:41,749
I flushed my car keys
down the toilet.

417
00:18:41,768 --> 00:18:44,186
No problem.

418
00:18:45,934 --> 00:18:48,482
Eww... stop.

419
00:18:48,750 --> 00:18:50,467
Hello to you too, April.

420
00:18:50,502 --> 00:18:53,286
I'm sorry I said
you were lame, okay?

421
00:18:53,304 --> 00:18:54,838
You are, but I know

422
00:18:54,889 --> 00:18:56,289
you were just trying
to help me, so...

423
00:18:56,307 --> 00:18:58,308
Thank you, April.
Happy Halloween.

424
00:18:58,343 --> 00:19:01,753
And if you want to egg
Larry's house, come find me.

425
00:19:02,430 --> 00:19:03,764
You guys are cute.

426
00:19:03,798 --> 00:19:06,433
- Where'd that come from?
- I'm weak.

427
00:19:06,467 --> 00:19:09,570
I miss Andy,
and it's making me weak.

428
00:19:09,604 --> 00:19:11,772
Also, if Chris puts
that much effort

429
00:19:11,806 --> 00:19:14,141
into making
my stupid night better,

430
00:19:14,159 --> 00:19:17,528
I'm sure that he'll make
your lame life perfect.

431
00:19:17,579 --> 00:19:20,164
Or he's secretly
super in love with me.

432
00:19:20,198 --> 00:19:21,398
It's probably that.

433
00:19:21,416 --> 00:19:24,040
That's a funny joke, April.

434
00:19:24,041 --> 00:19:27,004
But the truth is, I'm very
much in love with Ann.

435
00:19:28,490 --> 00:19:31,334
Okay, gross.
The spell's broken.

436
00:19:32,156 --> 00:19:36,325
I've made my decision, and I'll sell you
Rent-a-Swag, but on my terms.

437
00:19:36,326 --> 00:19:37,631
Which are?

438
00:19:37,665 --> 00:19:40,167
$40,000 for the inventory, the website,
and everything else in the store,

439
00:19:40,185 --> 00:19:44,095
but I keep the name Rent-a-Swag.
It's mine, and I'm keeping it.

440
00:19:45,006 --> 00:19:47,941
The name is actually
an important part of the sale.

441
00:19:47,976 --> 00:19:50,210
- Really?
- Yes. My client likes the name.

442
00:19:50,228 --> 00:19:53,631
It's better than Tommy's Closet.
He needs the name.

443
00:19:54,783 --> 00:19:57,367
Well, I don't know
what to tell you, except...

444
00:19:57,402 --> 00:19:59,787
I'll throw in the name
in exchange for $20,000 more

445
00:19:59,821 --> 00:20:01,705
and 5%
of your client's business.

446
00:20:01,739 --> 00:20:05,409
I'll run it past my client,
but I think we have a deal.

447
00:20:05,460 --> 00:20:08,912
Ron said there's nothing more valuable
than my name, but he's not a businessman.

448
00:20:08,963 --> 00:20:10,164
I am.

449
00:20:10,198 --> 00:20:12,528
And now I have seed money
for my next venture.

450
00:20:12,529 --> 00:20:14,394
I sold out, baby.

451
00:20:15,753 --> 00:20:19,423
The votes have been counted,
and I have been recalled.

452
00:20:19,474 --> 00:20:21,384
I am, of course,
disappointed.

453
00:20:21,385 --> 00:20:24,544
But I am still your City
councilwoman for 30 more days,

454
00:20:24,562 --> 00:20:27,239
and I intend to spend
every second I have left

455
00:20:27,240 --> 00:20:29,592
working for you
and this great city.

456
00:20:29,882 --> 00:20:31,266
The thing
about being part

457
00:20:31,300 --> 00:20:33,485
of the Leslie Knope
emotional support task force

458
00:20:33,536 --> 00:20:35,958
is that
it's a very easy job.

459
00:20:35,959 --> 00:20:37,693
She's never down
for that long.

460
00:20:37,711 --> 00:20:39,695
And now that she's had
a little time to recharge,

461
00:20:39,713 --> 00:20:42,531
she is like a toddler
bouncing back from a nap.

462
00:20:42,549 --> 00:20:44,233
Hey!
There you guys are.

463
00:20:44,267 --> 00:20:46,719
Okay, Ben, I need some help
with the re-zoning thing.

464
00:20:46,753 --> 00:20:49,638
Also, I think we should paint our deck,
so I have some color samples for you.

465
00:20:49,673 --> 00:20:52,057
And to thank you both for being
there when I needed you,

466
00:20:52,092 --> 00:20:54,226
I carved your faces
into these Jack-o-lanterns.

467
00:20:54,261 --> 00:20:55,877
Ann, it was very hard
to capture your beauty,

468
00:20:55,896 --> 00:20:57,396
and, Ben, you make
a sexy pumpkin...

469
00:20:57,430 --> 00:20:58,514
No surprise.

470
00:20:58,548 --> 00:21:00,329
Love you both.
See you later.

471
00:21:00,330 --> 00:21:02,513
- Okay.
- Wow.

472
00:21:03,034 --> 00:21:04,668
This is amazing.

473
00:21:05,054 --> 00:21:06,522
Not bad.

474
00:21:06,687 --> 00:21:09,687
Synced by Reef
www.addic7ed.com

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
