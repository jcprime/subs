﻿1
00:00:02,836 --> 00:00:06,572
♪ Whoo-hoo! ♪

2
00:00:06,640 --> 00:00:08,040
Hey, ladies and gentlemen,

3
00:00:08,107 --> 00:00:09,841
for the first time ever
in her new part-time job

4
00:00:09,909 --> 00:00:11,509
in the Public Health Department
at City Hall,

5
00:00:11,576 --> 00:00:13,511
it's Ann Meredith Perkins!
Yay!

6
00:00:13,578 --> 00:00:15,612
Oh, Leslie, this is so nice!

7
00:00:15,680 --> 00:00:17,614
I put a poisonous gas
in one of these balloons,

8
00:00:17,682 --> 00:00:19,383
so if any of them pops,
you may die.

9
00:00:19,451 --> 00:00:21,718
No, April, we would all die.

10
00:00:21,786 --> 00:00:23,720
Gases fill the volume

11
00:00:23,788 --> 00:00:26,190
of whatever container
they're in.

12
00:00:26,258 --> 00:00:27,524
School.

13
00:00:27,591 --> 00:00:30,226
We have activities every hour
on your first day.

14
00:00:30,294 --> 00:00:32,195
10:00 A.M., Ann's first day
waffle explosion.

15
00:00:32,263 --> 00:00:34,864
11:00 A.M.,
the start-paperwork jamboree.

16
00:00:34,932 --> 00:00:36,199
And then 12:00 noon sharp

17
00:00:36,267 --> 00:00:39,169
is the inaugural
d-Ann-ce party.

18
00:00:39,236 --> 00:00:41,438
Welcome to City Hall, cupcake!

19
00:00:41,505 --> 00:00:43,105
How many of you are in here?

20
00:00:43,173 --> 00:00:45,107
There are seven.
And you have an office mate.

21
00:00:45,175 --> 00:00:47,142
His name is Stuart,
and he's kind of a grouch.

22
00:00:47,210 --> 00:00:48,643
I have an office mate?

23
00:00:48,711 --> 00:00:51,113
Get these [Bleep] balloons
out of here.

24
00:00:51,181 --> 00:00:53,549
Hi. I'm Ann.

25
00:00:53,616 --> 00:01:05,551
<font color="#FF8C00">Sync & corrections by Alice</font>
<font color="##FF8C00">www.addic7ed.com</font>

26
00:01:13,802 --> 00:01:14,868
- Hey.
- Hey.

27
00:01:14,936 --> 00:01:16,269
Just to let you know,

28
00:01:16,337 --> 00:01:17,938
I gave the senior center
project to Donna.

29
00:01:18,006 --> 00:01:19,773
Sounds good.
The meeting's at 2:00?

30
00:01:19,841 --> 00:01:21,274
Yes. Conference room "C".

31
00:01:21,342 --> 00:01:23,343
Okay.
I think at some point,

32
00:01:23,411 --> 00:01:26,013
you and I should probably
make out with each other.

33
00:01:26,081 --> 00:01:27,247
Yeah, good call.

34
00:01:27,315 --> 00:01:28,515
The best part
of any relationship

35
00:01:28,583 --> 00:01:29,516
is the beginning.

36
00:01:29,584 --> 00:01:31,485
No problems, no fights.

37
00:01:31,552 --> 00:01:34,386
Just white wine, cuddling,

38
00:01:34,454 --> 00:01:38,625
and crazy amounts of
History Channel documentaries.

39
00:01:38,692 --> 00:01:40,292
Hey, we should take
separate cars

40
00:01:40,360 --> 00:01:41,393
to the restaurant tonight.

41
00:01:41,461 --> 00:01:43,229
Chris is deadly serious
about this...

42
00:01:43,296 --> 00:01:44,363
I know.

43
00:01:44,431 --> 00:01:46,365
I just don't wanna talk
about it, okay?

44
00:01:46,433 --> 00:01:47,466
I'll see you tonight.

45
00:01:47,534 --> 00:01:49,267
Okay.
Oh, by the way,

46
00:01:49,335 --> 00:01:52,838
I have a meeting
with Marlene Griggs-Knope.

47
00:01:52,905 --> 00:01:55,140
Is that any relation?

48
00:01:55,207 --> 00:01:57,475
- No.
- Oh, okay.

49
00:01:57,543 --> 00:01:59,845
Just a coincidence.
We get it all the time.

50
00:01:59,912 --> 00:02:01,013
We laugh about it.

51
00:02:01,080 --> 00:02:02,547
But I've never met her.

52
00:02:02,615 --> 00:02:04,048
- She's Filipino.
- Mm.

53
00:02:04,116 --> 00:02:07,018
Okay, bye.

54
00:02:07,085 --> 00:02:09,153
Why don't you ju t tell Ben
she's your mother?

55
00:02:09,220 --> 00:02:10,354
Because Ben and I

56
00:02:10,421 --> 00:02:12,356
are in this amazing
little bubble right now,

57
00:02:12,423 --> 00:02:14,491
and there's no room in here

58
00:02:14,559 --> 00:02:16,493
for Chris' stupid rules
about us not dating

59
00:02:16,561 --> 00:02:17,862
or my ball-busting mother.

60
00:02:17,929 --> 00:02:19,363
Oh, Leslie, I'm sorry,

61
00:02:19,430 --> 00:02:21,031
but I don't think
you thought this through.

62
00:02:21,098 --> 00:02:22,899
You know what I should do?
I should get my mother

63
00:02:22,967 --> 00:02:24,901
a one-way ticket to London,
leaving today.

64
00:02:24,969 --> 00:02:26,536
And that way,
Ben never has to meet her

65
00:02:26,603 --> 00:02:28,204
and I could visit her
in London.

66
00:02:28,272 --> 00:02:29,839
Everybody wins.

67
00:02:29,907 --> 00:02:31,541
What if he sees
a picture of her on your desk?

68
00:02:31,608 --> 00:02:32,876
What if Marlene says,

69
00:02:32,943 --> 00:02:34,044
"Hey, have you met
my daughter Leslie?

70
00:02:34,111 --> 00:02:35,511
She works
in the Parks Department."

71
00:02:35,579 --> 00:02:38,580
Stuart, please, could you
give us like 45 minutes?

72
00:02:38,648 --> 00:02:40,849
- It's my office too.
- Stuart.

73
00:02:43,053 --> 00:02:44,186
Boy, that guy was rude.

74
00:02:44,253 --> 00:02:46,488
I feel that
some structural changes

75
00:02:46,556 --> 00:02:48,924
could really unlock
this department's potential.

76
00:02:48,992 --> 00:02:50,192
- Jerry.
- Hmm?

77
00:02:50,259 --> 00:02:52,593
I believe you are capable
of much more.

78
00:02:53,261 --> 00:02:54,595
- I'm not.
- Nonsense.

79
00:02:54,663 --> 00:02:56,164
- Look in the mirror.
- What?

80
00:02:56,231 --> 00:02:58,833
You are an intelligent,
charismatic,

81
00:02:58,901 --> 00:03:00,868
beautiful superhero.

82
00:03:00,936 --> 00:03:03,004
I'm making you
head of Public Relations,

83
00:03:03,072 --> 00:03:05,506
which means you'll be leading
the daily briefing sessions.

84
00:03:05,574 --> 00:03:07,041
Excellent idea.

85
00:03:07,109 --> 00:03:08,709
This is my favorite part

86
00:03:08,743 --> 00:03:10,711
about having
a new city manager.

87
00:03:10,778 --> 00:03:12,713
They always try
to shake things up,

88
00:03:12,780 --> 00:03:14,414
and their ideas are terrible,

89
00:03:14,481 --> 00:03:17,718
and it brings City Hall
to a grinding halt.

90
00:03:17,785 --> 00:03:19,385
I just grab a few donuts,

91
00:03:19,453 --> 00:03:21,588
sit back, and enjoy the show.

92
00:03:21,655 --> 00:03:24,456
April, you are too valuable

93
00:03:24,524 --> 00:03:26,291
to just be Ron's assistant.

94
00:03:26,359 --> 00:03:28,894
So from now on, you are
a multitasking executive aide,

95
00:03:28,962 --> 00:03:30,362
assisting the entire office.

96
00:03:30,430 --> 00:03:31,630
Is this a nightmare?

97
00:03:31,698 --> 00:03:33,733
April, wake up.

98
00:03:33,800 --> 00:03:35,101
Tom, I just wanna say

99
00:03:35,168 --> 00:03:37,603
that you are
a wonderful employee

100
00:03:37,670 --> 00:03:39,571
and a terrific human being.

101
00:03:39,639 --> 00:03:40,972
Meeting adjourned!

102
00:03:42,108 --> 00:03:43,208
Tom.

103
00:03:43,275 --> 00:03:44,876
I enjoy you.

104
00:03:44,944 --> 00:03:47,378
You know what else I enjoy?
Your entrepreneurial spirit.

105
00:03:47,446 --> 00:03:50,181
I did recently sell
my <i>Chronicles of Riddick</i> DVD

106
00:03:50,249 --> 00:03:51,916
on eBay for $10.

107
00:03:51,984 --> 00:03:53,918
Used the profits
to buy the Blu-Ray.

108
00:03:53,986 --> 00:03:56,186
That's terrific.
I have an amazing project

109
00:03:56,254 --> 00:03:57,320
for someone of your talents.

110
00:03:57,388 --> 00:03:59,156
You are going to digitize

111
00:03:59,224 --> 00:04:01,458
the entire city archives
up on the fourth floor.

112
00:04:01,525 --> 00:04:02,860
No! I hate
the fourth floor!

113
00:04:02,927 --> 00:04:04,327
Last time I was up there,

114
00:04:04,395 --> 00:04:06,429
I saw someone buy crystal meth
out of a vending machine.

115
00:04:06,497 --> 00:04:07,430
It's a bad place.

116
00:04:07,498 --> 00:04:09,166
You won't be alone.
Andy.

117
00:04:09,234 --> 00:04:10,500
Starting now,

118
00:04:10,568 --> 00:04:12,434
you are nobody's shoeshine boy.

119
00:04:12,502 --> 00:04:16,773
Starting now, you are
an administrative assistant.

120
00:04:16,840 --> 00:04:18,975
For three weeks.
Then back to shoes.

121
00:04:19,042 --> 00:04:20,977
Chris!

122
00:04:21,044 --> 00:04:22,478
I wasn't super paying attention

123
00:04:22,545 --> 00:04:23,746
to what you just said
that we'll be doing,

124
00:04:23,814 --> 00:04:26,482
but I will give 110%!

125
00:04:26,549 --> 00:04:27,816
Well, as soon
as you repeat yourself

126
00:04:27,850 --> 00:04:29,517
in a more interesting way.

127
00:04:29,585 --> 00:04:32,654
- Hey, Ben.
- Hey, what's up?

128
00:04:32,721 --> 00:04:34,689
Nothing much. Since I'm here,
I just wanted to let you know

129
00:04:34,690 --> 00:04:36,124
that Marlene Griggs-Knope
is my mom.

130
00:04:36,192 --> 00:04:38,860
What? Wh.. why?

131
00:04:38,928 --> 00:04:40,829
Also, she can be cruel
and difficult,

132
00:04:40,930 --> 00:04:42,330
and she make snap judgments
about people

133
00:04:42,398 --> 00:04:43,998
that she holds on to
for a very long time.

134
00:04:44,065 --> 00:04:45,199
And she's kind of a bully.

135
00:04:45,267 --> 00:04:46,934
Okay! Go in there
you're gonna be great.

136
00:04:46,968 --> 00:04:47,935
Oh God. Oh, my God.

137
00:04:48,003 --> 00:04:49,970
Oh, my God. I'm sorry.
Here... Sorry!

138
00:04:50,038 --> 00:04:52,173
Hello.

139
00:04:52,240 --> 00:04:53,974
I'm Mr. Ben Wyatt.

140
00:04:54,042 --> 00:04:55,776
Looks like you got some shirt
on your coffee.

141
00:04:55,844 --> 00:04:57,444
Yeah. You know
what happened?

142
00:04:57,512 --> 00:05:00,947
I spilled it,
but you know how it goes.

143
00:05:01,015 --> 00:05:02,816
Fun anecdote.

144
00:05:02,883 --> 00:05:04,417
The County School District

145
00:05:04,484 --> 00:05:06,085
needs four new school buses.

146
00:05:06,153 --> 00:05:07,854
I'm being stonewalled
by the board,

147
00:05:07,922 --> 00:05:10,156
and I'm hoping your office
can help us out.

148
00:05:10,224 --> 00:05:12,025
Okay.

149
00:05:12,092 --> 00:05:13,860
Really? That's it?

150
00:05:13,928 --> 00:05:17,362
Yeah.
Actually, no. It...

151
00:05:17,430 --> 00:05:19,298
it's probably not possible.

152
00:05:19,365 --> 00:05:21,300
Then I'm confused, Ben,

153
00:05:21,367 --> 00:05:22,868
because you
just told me it was.

154
00:05:22,936 --> 00:05:24,102
Five seconds ago.

155
00:05:25,371 --> 00:05:27,605
I got a second box of donuts,
if anybody...

156
00:05:28,942 --> 00:05:31,043
What the [Bleep] is this?

157
00:05:31,110 --> 00:05:33,677
This, Mr. Director,
is your new desk.

158
00:05:33,745 --> 00:05:35,112
Okay...

159
00:05:35,180 --> 00:05:36,613
This desk is the epitome

160
00:05:36,681 --> 00:05:39,350
of the Swedish concept
of <i>Jamstalldhet,</i>

161
00:05:39,417 --> 00:05:41,052
or "equality."

162
00:05:41,119 --> 00:05:42,453
Imagine someone
needs your attention.

163
00:05:42,520 --> 00:05:44,655
- Somebody say my name.
- Chris.

164
00:05:44,723 --> 00:05:46,157
Swivel!

165
00:05:46,225 --> 00:05:47,225
What is it, Jerry?

166
00:05:47,292 --> 00:05:48,691
You told me to say your name.

167
00:05:48,759 --> 00:05:50,293
And you did a great job,
superstar.

168
00:05:50,361 --> 00:05:51,862
Someone else say something.

169
00:05:51,929 --> 00:05:52,996
You look like a freak.

170
00:05:53,064 --> 00:05:54,331
Swivel!

171
00:05:54,398 --> 00:05:56,333
April, that is not
a very good attitude.

172
00:05:56,400 --> 00:05:58,835
I will keep my eye on you

173
00:05:58,903 --> 00:06:00,337
from my circular desk
where I can see everything.

174
00:06:00,404 --> 00:06:02,805
Tiny swivel.
See how it works?

175
00:06:02,874 --> 00:06:04,974
What about my office

176
00:06:05,041 --> 00:06:07,243
and its many walls?

177
00:06:07,310 --> 00:06:09,211
That becomes
a new public waiting room.

178
00:06:09,279 --> 00:06:12,314
You're gonna be
more accessible than ever.

179
00:06:17,453 --> 00:06:18,988
The fourth floor...
Small Claims Court,

180
00:06:19,055 --> 00:06:21,389
divorce filings,
state-ordered drug tests.

181
00:06:21,456 --> 00:06:23,858
It's somehow
both freezing <i>and</i> humid.

182
00:06:23,926 --> 00:06:25,726
There's a whole room
on the fourth floor

183
00:06:25,794 --> 00:06:27,896
where they store the knives
they've confiscated from people

184
00:06:27,963 --> 00:06:30,098
who went to the fourth floor
to stab someone.

185
00:06:32,001 --> 00:06:34,335
- You from parks and rec?
- Yeah.

186
00:06:34,403 --> 00:06:36,336
I'm here to show you around.

187
00:06:36,404 --> 00:06:38,105
Oh, nice to meet you, ma'am.
What's your name?

188
00:06:38,172 --> 00:06:41,241
Ethel Beavers.
Follow me.

189
00:06:42,476 --> 00:06:44,211
Okay, this is you.

190
00:06:44,279 --> 00:06:46,513
Whoa!

191
00:06:46,580 --> 00:06:48,982
Double computers?

192
00:06:49,050 --> 00:06:50,683
I get my own office phone.

193
00:06:50,751 --> 00:06:51,918
Hello, wall street.

194
00:06:51,985 --> 00:06:54,220
Buy more stocks. Now.

195
00:06:54,288 --> 00:06:55,754
This phone
kind of smells like a butt.

196
00:06:55,822 --> 00:06:57,256
Try not to move things around,

197
00:06:57,324 --> 00:06:58,657
'cause technically speaking,

198
00:06:58,725 --> 00:07:00,426
this is still a crime scene.

199
00:07:00,493 --> 00:07:02,428
Oh, here's your name tags.

200
00:07:02,495 --> 00:07:04,297
Here.

201
00:07:04,364 --> 00:07:06,832
Ethel, this shirt
is from theory.

202
00:07:06,900 --> 00:07:08,733
Name tags make holes.
I'm not wearing this.

203
00:07:08,801 --> 00:07:10,368
Well, do what you want.

204
00:07:11,436 --> 00:07:12,670
Bye, Ethel!

205
00:07:12,738 --> 00:07:13,871
Damn it.

206
00:07:13,940 --> 00:07:15,473
Remind me next time
to ask her where she was

207
00:07:15,541 --> 00:07:16,774
when Lincoln got shot.

208
00:07:16,842 --> 00:07:20,545
Remind... Tom...
Ask... something.

209
00:07:20,612 --> 00:07:22,147
You got it, boss.

210
00:07:22,214 --> 00:07:24,648
Hey! Somebody
made somebody a mix!

211
00:07:24,715 --> 00:07:26,650
And I definitely heard
your feedback from last time,

212
00:07:26,717 --> 00:07:28,652
so I only put five
Sarah Mclachlan songs

213
00:07:28,719 --> 00:07:30,654
- on this one.
- Cool.

214
00:07:30,721 --> 00:07:32,489
Should we talk about
how you claimed your mom

215
00:07:32,557 --> 00:07:34,491
was a Filipino woman
you'd never met?

216
00:07:34,559 --> 00:07:35,625
Should we?

217
00:07:37,362 --> 00:07:39,396
I was just trying
to delay the inevitable.

218
00:07:39,464 --> 00:07:41,931
If this turns
into something real,

219
00:07:41,999 --> 00:07:44,467
then we are gonna have
to deal with Chris' rule,

220
00:07:44,534 --> 00:07:47,337
and parents,
and annoying flossing habits,

221
00:07:47,404 --> 00:07:49,638
and a lot of un-fun stuff.

222
00:07:49,706 --> 00:07:51,040
But not yet.

223
00:07:51,108 --> 00:07:53,742
I just want to enjoy the bubble
for a little while.

224
00:07:54,810 --> 00:07:55,945
I'm sorry.

225
00:07:56,012 --> 00:07:57,279
Everything's good?

226
00:07:57,347 --> 00:07:58,447
No, it's not.

227
00:07:58,514 --> 00:07:59,948
I was completely flustered.

228
00:08:00,016 --> 00:08:01,450
I came off like an idiot.

229
00:08:01,517 --> 00:08:03,118
I mean, at one point,
for no reason,

230
00:08:03,186 --> 00:08:06,021
I just took off my shoes
and held them in my hand.

231
00:08:06,089 --> 00:08:07,189
I'm gonna go ask her
how it went,

232
00:08:07,256 --> 00:08:08,857
and I bet she loved you.

233
00:08:10,026 --> 00:08:11,659
I loved him!

234
00:08:11,727 --> 00:08:14,661
He's a total pushover.

235
00:08:14,729 --> 00:08:16,930
Very odd guy, though.

236
00:08:16,999 --> 00:08:20,434
He's like a goofy,
spineless jellyfish.

237
00:08:22,004 --> 00:08:23,504
Why do you ask, sweetheart?

238
00:08:23,571 --> 00:08:25,172
No reason.

239
00:08:26,471 --> 00:08:28,522
Don't worry about it.
It's totally fine.

240
00:08:28,523 --> 00:08:29,790
No, it's not fine.

241
00:08:29,858 --> 00:08:31,291
What if she starts going around
telling people

242
00:08:31,359 --> 00:08:32,359
I'm a pushover?

243
00:08:32,426 --> 00:08:33,526
No one's gonna believe that.

244
00:08:33,594 --> 00:08:34,694
When I first met you,

245
00:08:34,762 --> 00:08:36,563
I thought
you were a fascist hardass.

246
00:08:36,630 --> 00:08:38,565
- What?
- A cute fascist hardass,

247
00:08:38,632 --> 00:08:40,900
but still,
you give off a very...

248
00:08:40,968 --> 00:08:42,769
Education department

249
00:08:42,837 --> 00:08:45,705
now officially requesting
ten school buses.

250
00:08:45,773 --> 00:08:47,406
I thought
they only wanted four.

251
00:08:47,474 --> 00:08:50,576
Well, now that I'm a pushover,
why not ask for 10, or 20,

252
00:08:50,643 --> 00:08:53,579
or 100 new school buses
and a fleet of school yachts?

253
00:08:53,646 --> 00:08:54,847
- How about that?
- Hmm.

254
00:08:54,914 --> 00:08:55,848
Here's what we're gonna do.

255
00:08:55,915 --> 00:08:57,649
You are gonna tell me

256
00:08:57,717 --> 00:08:59,317
everything there is to know
about your mom,

257
00:08:59,386 --> 00:09:01,119
and I'm gonna have
another meeting with her,

258
00:09:01,187 --> 00:09:02,488
and we're gonna have it out.

259
00:09:02,555 --> 00:09:04,655
Or... we could go
to Belize

260
00:09:04,723 --> 00:09:05,790
and go scuba diving.

261
00:09:05,858 --> 00:09:07,191
And we could look
at the whale sharks.

262
00:09:07,259 --> 00:09:10,395
You're certified, right?
Let's get you certified.

263
00:09:13,265 --> 00:09:14,699
April, do you have
that usage report

264
00:09:14,767 --> 00:09:15,966
that I asked for?

265
00:09:19,404 --> 00:09:22,506
- Anything for you, Jerry.
- Thank you.

266
00:09:22,573 --> 00:09:25,008
Oh, come on!
Why?

267
00:09:25,075 --> 00:09:26,843
Excuse me.

268
00:09:26,911 --> 00:09:28,678
There's a sign at Ramsett Park

269
00:09:28,746 --> 00:09:30,980
that says "Do not drink
the sprinkler water,"

270
00:09:31,048 --> 00:09:32,416
so I made sun tea with it,

271
00:09:32,483 --> 00:09:34,418
and now I have an infection.

272
00:09:34,485 --> 00:09:36,251
Sir?

273
00:09:36,319 --> 00:09:37,920
Sir, are... are... are...

274
00:09:37,987 --> 00:09:39,422
are you listening to me, sir?

275
00:09:39,489 --> 00:09:42,090
Sir, I'm talking to you!

276
00:09:42,158 --> 00:09:43,258
Sir!

277
00:09:43,326 --> 00:09:45,428
Sir, are you aware

278
00:09:45,495 --> 00:09:47,763
that there is waste

279
00:09:47,831 --> 00:09:49,799
in your water system?

280
00:09:49,866 --> 00:09:53,000
Okay, you need to be strong,
powerful, decisive.

281
00:09:53,068 --> 00:09:54,803
This is not a meeting.
It's a battle.

282
00:09:54,870 --> 00:09:56,771
Normal meeting rules
do not apply.

283
00:09:56,839 --> 00:09:58,105
I'll be my mom,

284
00:09:58,173 --> 00:09:59,774
and I'm gonna be
very harsh with you,

285
00:09:59,842 --> 00:10:01,643
and it's only because
I like you a lot.

286
00:10:01,710 --> 00:10:03,945
- Okay.
- Go!

287
00:10:05,815 --> 00:10:07,181
- Hello.
- Wrong.

288
00:10:07,249 --> 00:10:08,515
- What?
- No preambles.

289
00:10:08,583 --> 00:10:10,851
No introductions.
Just walk in and start talking.

290
00:10:10,919 --> 00:10:14,087
I'd like to discuss
the new school bus.

291
00:10:14,155 --> 00:10:16,957
I'd like to discuss
your rhyming, Dr. Seuss.

292
00:10:17,024 --> 00:10:18,459
And you should be
sitting by now.

293
00:10:18,526 --> 00:10:20,327
- Uh, what?
- Just walk in and take a seat.

294
00:10:20,394 --> 00:10:21,662
Um...

295
00:10:21,729 --> 00:10:23,396
"Um" is the sound
in "dumb."

296
00:10:23,464 --> 00:10:24,964
That's what she says to people.

297
00:10:25,031 --> 00:10:26,732
And now you've crossed your legs
like a woman.

298
00:10:26,800 --> 00:10:28,067
God! Okay,
should we just start over?

299
00:10:28,134 --> 00:10:30,035
No. No, we need to put
a pin in this.

300
00:10:30,103 --> 00:10:31,471
Here is a list

301
00:10:31,538 --> 00:10:33,973
of my mother's top 100
favorite conversation topics,

302
00:10:34,040 --> 00:10:35,174
starting with Persian rugs,

303
00:10:35,241 --> 00:10:36,308
ending with Daniel Craig.

304
00:10:36,376 --> 00:10:38,711
You have ten minutes
to memorize it.

305
00:10:38,779 --> 00:10:40,912
- <i>Deliverance</i> the movie?
- Mm-hmm.

306
00:10:40,980 --> 00:10:42,213
Oh, God.

307
00:10:42,281 --> 00:10:44,916
So Jerry is,
like, basically my boss now...

308
00:10:44,984 --> 00:10:46,718
which I'm never
gonna work for him...

309
00:10:46,786 --> 00:10:49,053
Ron's trapped
in this weird desk prison,

310
00:10:49,121 --> 00:10:51,890
and I now work
at this traveling IV station.

311
00:10:51,958 --> 00:10:54,225
This Mort Jansen's office?

312
00:10:54,293 --> 00:10:55,460
Nope.

313
00:11:04,268 --> 00:11:07,505
Tell Mort
I said "your move."

314
00:11:10,141 --> 00:11:12,208
Oh, my God. I wanna work
up here with you guys.

315
00:11:12,275 --> 00:11:13,376
This is awesome.

316
00:11:13,443 --> 00:11:15,712
How many more of those stupid
documents do you have to scan?

317
00:11:15,779 --> 00:11:17,213
Just this one,

318
00:11:17,280 --> 00:11:20,049
and all the ones
in those boxes.

319
00:11:20,117 --> 00:11:21,985
I gotta get out of here pronto.

320
00:11:22,052 --> 00:11:24,153
Time to get those old ladies
to do my work for me.

321
00:11:24,221 --> 00:11:25,254
How?

322
00:11:25,322 --> 00:11:26,623
By shining down on 'em

323
00:11:26,690 --> 00:11:28,156
with the Haverford charm ray.

324
00:11:28,224 --> 00:11:29,758
Wah-wah wah-wah wah-wah...

325
00:11:30,826 --> 00:11:31,860
Well, well, well,

326
00:11:31,928 --> 00:11:33,862
if it isn't Ethel Beavers.

327
00:11:33,930 --> 00:11:35,697
What's up, beautiful?

328
00:11:35,765 --> 00:11:38,199
Julianne Moore just called.
She wants her hair back.

329
00:11:39,435 --> 00:11:42,370
Nobody named Julien called.

330
00:11:42,438 --> 00:11:43,838
Never mind.
I just need to ask you

331
00:11:43,906 --> 00:11:45,239
a couple of questions.

332
00:11:45,306 --> 00:11:48,776
One, can you help me out
with this project we're doing?

333
00:11:48,844 --> 00:11:51,779
And, two,
will you please invite me

334
00:11:51,847 --> 00:11:53,915
to your 30th birthday party?

335
00:11:56,451 --> 00:11:58,118
What are you doing?

336
00:11:58,186 --> 00:11:59,619
I'm making a test call
to your phone

337
00:11:59,687 --> 00:12:00,854
to make sure it's working

338
00:12:00,922 --> 00:12:03,189
so you don't miss
any more calls.

339
00:12:03,257 --> 00:12:04,357
You know what?
Don't bother.

340
00:12:04,425 --> 00:12:05,692
Maybe I'll just find
an open window

341
00:12:05,760 --> 00:12:08,695
- and plummet to my death.
- Okay.

342
00:12:10,364 --> 00:12:13,199
This...
isn't gonna work.

343
00:12:13,267 --> 00:12:15,835
Okay, you did not
just swivel away

344
00:12:15,836 --> 00:12:16,602
while I was talking to you.

345
00:12:16,670 --> 00:12:20,439
This spaceship keyboard
is driving me crazy.

346
00:12:20,506 --> 00:12:22,274
I'm down to one word a minute,

347
00:12:22,341 --> 00:12:23,809
and the word is
"perflipitusclub,"

348
00:12:23,877 --> 00:12:26,378
because I can't fly spaceships.

349
00:12:26,446 --> 00:12:28,681
Donna, you know as well as I do

350
00:12:28,748 --> 00:12:30,883
these city manager shakeups
always Peter out.

351
00:12:30,951 --> 00:12:33,585
- You just have to wait.
- Usually I'm with you.

352
00:12:33,652 --> 00:12:36,320
But this is Chris Traeger,
the Six Million dollar Man.

353
00:12:36,388 --> 00:12:37,655
He won't quit.

354
00:12:37,723 --> 00:12:39,824
So you need to swivel your ass
down to his office

355
00:12:39,892 --> 00:12:41,459
and have a word with him.

356
00:12:41,526 --> 00:12:42,794
Mommy.

357
00:12:42,861 --> 00:12:44,462
Oh!

358
00:12:44,529 --> 00:12:48,298
Oh, that is
so thoughtful of you.

359
00:12:48,365 --> 00:12:49,800
Of course!

360
00:12:49,867 --> 00:12:51,167
I just figured, hey,

361
00:12:51,235 --> 00:12:53,303
why not put my mother
in a good mood?

362
00:12:54,538 --> 00:12:55,739
So it's my understanding

363
00:12:55,807 --> 00:12:58,108
that despite what I said
in our meeting,

364
00:12:58,175 --> 00:13:00,443
you have somehow increased
your request

365
00:13:00,511 --> 00:13:01,578
for more school buses.

366
00:13:01,646 --> 00:13:02,913
Excuse me,

367
00:13:02,981 --> 00:13:05,180
but I'm having coffee
with my daughter.

368
00:13:05,248 --> 00:13:06,515
Maybe we could do this
another time.

369
00:13:06,583 --> 00:13:08,550
No, we're gonna talk
about this now.

370
00:13:09,886 --> 00:13:11,186
Or if you'd prefer,

371
00:13:11,254 --> 00:13:12,755
I could put a permanent freeze
on the education budget.

372
00:13:12,823 --> 00:13:14,423
It's your call.

373
00:13:14,491 --> 00:13:15,925
Wow, maybe you
should talk to him, mom.

374
00:13:15,993 --> 00:13:18,794
He seems
pretty self-assured.

375
00:13:19,895 --> 00:13:21,462
There's nothing left.

376
00:13:21,530 --> 00:13:22,797
It's over.

377
00:13:22,865 --> 00:13:24,465
Hey, hey, hey, don't say that.

378
00:13:24,533 --> 00:13:26,134
Now, come on.

379
00:13:26,201 --> 00:13:27,636
Get yourself together

380
00:13:27,703 --> 00:13:29,037
and go out there
and be the woman

381
00:13:29,105 --> 00:13:32,173
you always knew you could be.

382
00:13:32,241 --> 00:13:35,943
Thank you so much.

383
00:13:36,011 --> 00:13:37,444
Who was that?

384
00:13:37,512 --> 00:13:38,612
I don't know.

385
00:13:38,680 --> 00:13:41,148
I saw her crying,
and so I helped.

386
00:13:41,215 --> 00:13:43,450
What did you
wanna talk to me about?

387
00:13:43,518 --> 00:13:45,485
Look, Chris, I'm very flattered

388
00:13:45,553 --> 00:13:47,721
you thought of me
for this amazing opportunity,

389
00:13:47,789 --> 00:13:50,457
but I don't really know
if this gig is in my wheelhouse.

390
00:13:50,525 --> 00:13:52,992
I tend to work best
with young, attractive people.

391
00:13:53,060 --> 00:13:55,662
Tom, I have made you
a project leader.

392
00:13:55,729 --> 00:13:56,896
This is a big deal!

393
00:13:56,964 --> 00:13:58,998
You should keep an open mind.

394
00:13:59,066 --> 00:14:01,500
Let your brain unlock the door
to your heart's future.

395
00:14:01,568 --> 00:14:03,670
I made that expression up
when I was 14.

396
00:14:03,737 --> 00:14:05,672
Still in use today.
By me.

397
00:14:05,739 --> 00:14:07,906
Gotta jam.

398
00:14:07,974 --> 00:14:09,841
You know what?
It's just not possible.

399
00:14:09,909 --> 00:14:11,509
And don't even bother trying
to outflank me either,

400
00:14:11,577 --> 00:14:12,944
because you will fail.

401
00:14:13,012 --> 00:14:15,213
Where did all this confidence
come from all of a sudden?

402
00:14:15,281 --> 00:14:17,182
Hey, guys, come on.
Let's relax.

403
00:14:17,249 --> 00:14:19,051
- You know, Ms. Griggs-Knope...
- Marlene.

404
00:14:19,118 --> 00:14:20,318
There's probably
a compromise here.

405
00:14:20,386 --> 00:14:22,754
"Compromise is usually
a sign of weakness."

406
00:14:22,822 --> 00:14:24,488
Yes, it is,
according to Andrew Carnegie.

407
00:14:24,556 --> 00:14:26,891
But your demands remind me
of a different quote,

408
00:14:26,958 --> 00:14:28,325
from the great Bill Watterson...

409
00:14:28,393 --> 00:14:31,896
"you need a lobotomy.
I'll get a saw."

410
00:14:31,963 --> 00:14:34,565
I love <i>Calvin and Hobbes.</i>

411
00:14:34,632 --> 00:14:37,201
Me too!

412
00:14:37,268 --> 00:14:40,036
I gotta say,
this is some of my best work.

413
00:14:40,104 --> 00:14:41,470
This could not
have gone better.

414
00:14:42,539 --> 00:14:43,807
They are really hitting it off.

415
00:14:43,874 --> 00:14:46,910
Thank God.
Crisis averted.

416
00:14:48,374 --> 00:14:50,990
Well, I am very glad
you came back up here.

417
00:14:51,874 --> 00:14:52,990
Oh...

418
00:14:53,071 --> 00:14:54,071
Yeah, me too.

419
00:14:54,089 --> 00:14:55,690
- Yeah, uh...
- So you have a deal.

420
00:14:55,757 --> 00:14:57,358
Well, let me
mull everything over,

421
00:14:57,426 --> 00:15:01,262
and then we can reconnect
and hash things out.

422
00:15:01,330 --> 00:15:03,264
It might get rough,
but that can be fun.

423
00:15:03,332 --> 00:15:05,933
- Ben can handle it.
- Yup, I can.

424
00:15:06,001 --> 00:15:07,201
Okay, great.

425
00:15:07,235 --> 00:15:10,337
- It was a pleasure.
- Oh, mm-hmm.

426
00:15:12,006 --> 00:15:13,440
What's so great
is she was impressed by you too.

427
00:15:13,508 --> 00:15:15,275
I mean, you could tell instantly
that the way you guys

428
00:15:15,343 --> 00:15:16,443
were connecting
was really amazing.

429
00:15:16,511 --> 00:15:17,944
Now, what I'm thinking
is that maybe

430
00:15:18,012 --> 00:15:19,079
we should go over
to her house...

431
00:15:19,146 --> 00:15:21,381
you two are
just the cutest things

432
00:15:21,449 --> 00:15:22,448
on four legs!

433
00:15:22,516 --> 00:15:24,584
- Aw.
- But why so much black?

434
00:15:24,651 --> 00:15:26,385
It's like you're going
to a funeral.

435
00:15:26,453 --> 00:15:29,355
- She's got a point, babe.
- So marry her then.

436
00:15:29,423 --> 00:15:31,090
I wish!

437
00:15:31,157 --> 00:15:33,292
Hey, sorry to interrupt
the love fest,

438
00:15:33,360 --> 00:15:35,094
but can you hand me that
folder there, please, Ethel?

439
00:15:35,161 --> 00:15:37,530
Oh, great.

440
00:15:37,598 --> 00:15:39,364
Why you gotta be
like that, Ethel?

441
00:15:39,432 --> 00:15:41,533
Looks like someone got up
on the wrong side of the crib.

442
00:15:43,168 --> 00:15:44,402
Stop laughing, Muriel.

443
00:15:45,638 --> 00:15:47,071
Stop it!

444
00:15:47,139 --> 00:15:49,107
I gotta say, I'm a genius.

445
00:15:49,174 --> 00:15:50,942
The only thing my mom loves
more than music boxes

446
00:15:51,009 --> 00:15:52,544
is McSteamy
from <i>Grey's Anatomy.</i>

447
00:15:52,612 --> 00:15:54,045
So I made her...

448
00:15:54,113 --> 00:15:56,079
A McSteamy-style music box,

449
00:15:56,147 --> 00:15:57,348
and I thought maybe
you could give it to her.

450
00:15:57,415 --> 00:15:58,582
She would love you for it.

451
00:15:58,650 --> 00:16:01,051
I think maybe
that's not such a good idea.

452
00:16:01,119 --> 00:16:02,419
Why?

453
00:16:02,487 --> 00:16:04,288
I mean, it's not
the best picture of him, but...

454
00:16:04,356 --> 00:16:08,058
Your mom kind of...

455
00:16:08,126 --> 00:16:09,960
Um...

456
00:16:10,027 --> 00:16:12,161
Made a pass at me.

457
00:16:14,431 --> 00:16:16,666
So Webster's dictionary

458
00:16:16,734 --> 00:16:18,868
describes a pork as a...

459
00:16:18,935 --> 00:16:21,337
no, it's park... park.

460
00:16:21,405 --> 00:16:24,106
I'm sorry.
Geez, okay. So...

461
00:16:24,174 --> 00:16:26,175
You know what,
can we start over?

462
00:16:26,243 --> 00:16:28,510
Let's... let's go from
the first thing I said... pork...

463
00:16:28,578 --> 00:16:30,111
which, by the way,

464
00:16:30,179 --> 00:16:32,781
they have a great pork sandwich
in the cafeteria today.

465
00:16:32,848 --> 00:16:34,949
- Ron Swanson.
- Chris.

466
00:16:35,017 --> 00:16:37,452
You have come up with a plan

467
00:16:37,520 --> 00:16:39,521
so spectacularly horrible

468
00:16:39,589 --> 00:16:41,657
that it might ruin
the entire department.

469
00:16:41,724 --> 00:16:42,990
- Now, wait a minute.
- No, I mean that

470
00:16:43,057 --> 00:16:44,057
as a compliment.

471
00:16:44,125 --> 00:16:46,427
SO it pains me to say this...

472
00:16:46,495 --> 00:16:48,829
My department has to go back
to the way it was.

473
00:16:48,897 --> 00:16:50,498
Give 'em time.
They'll adjust.

474
00:16:50,565 --> 00:16:52,966
No, they won't.
They're miserable.

475
00:16:53,034 --> 00:16:56,370
Tom only performs
when there's someone to impress,

476
00:16:56,438 --> 00:16:58,138
so marooning him
on freak island

477
00:16:58,206 --> 00:16:59,539
isn't helping anyone.

478
00:16:59,607 --> 00:17:02,309
And you made April
assistant to everyone?

479
00:17:02,376 --> 00:17:04,477
You know who April hates?

480
00:17:04,545 --> 00:17:06,446
Everyone.

481
00:17:06,514 --> 00:17:09,048
And Jerry can only function
if no one's looking.

482
00:17:09,116 --> 00:17:10,317
You shine a light on him,

483
00:17:10,384 --> 00:17:13,085
and he shrinks up faster
than an eskimo's scrotum.

484
00:17:13,153 --> 00:17:14,787
Well, that's
very perceptive, Ron.

485
00:17:14,854 --> 00:17:16,657
And... very graphic.

486
00:17:17,025 --> 00:17:18,393
I understand your point,

487
00:17:18,425 --> 00:17:20,693
but there's no way that
I get just rolled-over on this.

488
00:17:23,095 --> 00:17:24,763
Okay.

489
00:17:24,831 --> 00:17:26,732
You won't ever hear me
say this again,

490
00:17:26,800 --> 00:17:29,067
so savor this moment.

491
00:17:30,636 --> 00:17:33,037
I may have a compromise.

492
00:17:33,104 --> 00:17:35,272
Look, she was just
flirting a little.

493
00:17:35,341 --> 00:17:37,742
I'm sure she's not
really interested in me.

494
00:17:37,810 --> 00:17:39,744
Oh, my God. I can't even
have this conversation.

495
00:17:39,812 --> 00:17:41,779
The whole thing
is screwed up now.

496
00:17:41,847 --> 00:17:43,815
There's so many ways
to destroy a bubble,

497
00:17:43,882 --> 00:17:46,916
but my mom flirting with you
is number one on the list.

498
00:17:46,984 --> 00:17:49,118
And I'm sorry,
I have to say this...

499
00:17:49,186 --> 00:17:51,087
Were you asking for it
in any way?

500
00:17:51,155 --> 00:17:52,456
No!

501
00:17:52,523 --> 00:17:53,923
- How were you dressed?
- Oh, my God.

502
00:17:53,991 --> 00:17:55,859
- I was wearing this.
- Here's what we do.

503
00:17:55,926 --> 00:17:57,461
You issue
a government-wide memo

504
00:17:57,528 --> 00:17:59,563
about sexual harassment,
and then you go to Mexico.

505
00:17:59,630 --> 00:18:02,398
Just for a couple of weeks.

506
00:18:02,466 --> 00:18:04,066
Where are you going?
Ben!

507
00:18:04,133 --> 00:18:06,301
Ben! Wait.
Just k...

508
00:18:06,370 --> 00:18:08,203
Hey.
Hi, mom.

509
00:18:08,271 --> 00:18:10,639
- Hi again.
- Leslie and I are dating.

510
00:18:11,207 --> 00:18:12,375
We haven't told anyone

511
00:18:12,442 --> 00:18:14,810
because there's a rule
against office relationships,

512
00:18:14,878 --> 00:18:16,445
but it's happening.

513
00:18:16,513 --> 00:18:18,780
I'm sorry we didn't tell you
before you touched my knee,

514
00:18:18,848 --> 00:18:20,448
and I trust
you won't tell anyone now,

515
00:18:20,516 --> 00:18:22,584
since it could get
your daughter fired.

516
00:18:29,792 --> 00:18:31,993
That is hilarious.

517
00:18:32,060 --> 00:18:34,628
That's what we thought too.

518
00:18:34,696 --> 00:18:37,130
Well...

519
00:18:37,198 --> 00:18:39,800
Four buses.

520
00:18:39,868 --> 00:18:42,603
- Two.
- Deal.

521
00:18:47,208 --> 00:18:48,308
I like this one.

522
00:18:48,876 --> 00:18:50,510
Me too.

523
00:18:50,577 --> 00:18:52,678
You keep your hands off him.

524
00:18:52,746 --> 00:18:56,348
Margaret's pecan squares.

525
00:18:56,417 --> 00:18:58,785
They are like crack.
I brought you one.

526
00:18:58,852 --> 00:19:00,286
How are you so happy
working here?

527
00:19:00,353 --> 00:19:01,654
I don't know, man.
It's not that bad.

528
00:19:01,722 --> 00:19:03,122
A year ago I lived in a pit.

529
00:19:03,189 --> 00:19:05,858
Now I got a job,
and a kickass wife,

530
00:19:05,926 --> 00:19:08,560
and my band is so good,

531
00:19:08,628 --> 00:19:11,262
and are you gonna eat
that pecan square?

532
00:19:14,834 --> 00:19:19,003
When life gives you lemons,
make lemonade.

533
00:19:19,071 --> 00:19:21,840
I read that once
on a can of lemonade.

534
00:19:21,908 --> 00:19:25,275
I like to think
that it applies to life.

535
00:19:27,278 --> 00:19:28,712
Here you go.

536
00:19:28,780 --> 00:19:30,948
Thank you.

537
00:19:35,720 --> 00:19:37,955
Kind of figured you'd
be back in your office by now.

538
00:19:38,022 --> 00:19:40,557
Well, you know
how new city managers are.

539
00:19:40,624 --> 00:19:43,192
Seems like everyone else
is back to normal.

540
00:19:44,628 --> 00:19:45,962
How long
do you have to do this?

541
00:19:46,029 --> 00:19:48,230
A week.

542
00:19:48,298 --> 00:19:50,900
Citizen request.
Swivel, swivel, swivel.

543
00:19:50,968 --> 00:19:53,736
Hello. I can help you
in here, sir.

544
00:19:53,804 --> 00:19:57,404
<font color="#FF8C00">Sync & corrections by Alice</font>
<font color="##FF8C00">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
