﻿1
00:00:00,000 --> 00:00:02,594
There is a crazy flu going around.

2
00:00:02,917 --> 00:00:05,445
Lots of miserable
dehydrated people

3
00:00:05,491 --> 00:00:07,087
with high fevers and diarrhea.

4
00:00:07,453 --> 00:00:08,633
And one of those

5
00:00:08,753 --> 00:00:11,718
dehydrated, high-fevered
miserable people

6
00:00:11,755 --> 00:00:12,726
is April...

7
00:00:13,274 --> 00:00:14,157
Who hates me.

8
00:00:17,891 --> 00:00:20,361
- I want another nurse.
- Well, there are none.

9
00:00:20,481 --> 00:00:21,732
We're stretched pretty thin
right now.

10
00:00:21,852 --> 00:00:24,023
Then I want a janitor.
They can do what you do, right?

11
00:00:24,474 --> 00:00:27,279
Yep, nurses and janitors
are totally interchangeable.

12
00:00:27,527 --> 00:00:29,180
Except no one dresses up
like a janitor

13
00:00:29,210 --> 00:00:30,110
when they want to be slutty.

14
00:00:30,449 --> 00:00:32,943
I get the sense that maybe
you're angry at me

15
00:00:33,063 --> 00:00:33,882
for kissing Andy...

16
00:00:34,002 --> 00:00:35,558
No, I'm not. What are you talking about?
That's crazy.

17
00:00:35,678 --> 00:00:38,911
And that you think it might be
fun to take it out on me.

18
00:00:39,372 --> 00:00:40,435
Please don't do that.

19
00:00:40,641 --> 00:00:42,572
"Do"?
I can't do anything.

20
00:00:43,080 --> 00:00:44,103
I'm sick.

21
00:00:49,089 --> 00:00:50,881
My blankets are on the floor.

22
00:00:55,620 --> 00:01:05,560
<font color="#ec14bd">Sync by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

23
00:01:12,670 --> 00:01:13,970
So, JJ, we wanted

24
00:01:14,021 --> 00:01:16,006
to talk to you
about the harvest festival.

25
00:01:16,073 --> 00:01:18,275
Yeah, I heard you were
bringing that back.

26
00:01:19,543 --> 00:01:21,328
Sorry, I haven't been
feeling myself.

27
00:01:22,747 --> 00:01:25,448
Well, I'm organizing
this huge harvest festival,

28
00:01:25,516 --> 00:01:28,518
and I need
at least 80 local businesses

29
00:01:28,586 --> 00:01:29,853
to participate.

30
00:01:29,921 --> 00:01:31,488
And normally, this is
the kind of thing

31
00:01:31,555 --> 00:01:32,756
I would love to do,

32
00:01:32,823 --> 00:01:35,692
but I'm just feeling
really tired.

33
00:01:35,760 --> 00:01:39,029
I think maybe my allergies
are acting up.

34
00:01:39,096 --> 00:01:43,900
I've already vomited,
like, five times today.

35
00:01:43,968 --> 00:01:45,969
We're having a meeting tonight
at the Chamber of Commerce,

36
00:01:46,020 --> 00:01:47,604
and we're gonna go over
all the details,

37
00:01:47,672 --> 00:01:49,673
and we'd just love if you
could come and join us.

38
00:01:49,740 --> 00:01:52,876
Sure, anything
for my favorite customer.

39
00:01:52,944 --> 00:01:54,861
I bet you say that
to all the girls.

40
00:01:54,946 --> 00:01:57,180
Oh, no, no.
Actually, you're my favorite.

41
00:01:57,248 --> 00:02:02,869
You spent over $1,000 last year
on waffles alone.

42
00:02:04,722 --> 00:02:05,755
Here.

43
00:02:05,823 --> 00:02:07,557
I didn't know what
to bring you,

44
00:02:07,625 --> 00:02:10,627
so I just got
some magazines and lipstick...

45
00:02:10,695 --> 00:02:11,745
woman stuff.

46
00:02:11,829 --> 00:02:13,163
Thanks.

47
00:02:13,214 --> 00:02:16,366
All my parents got me was that.

48
00:02:16,434 --> 00:02:17,884
Okay, well, I'm not very good

49
00:02:17,969 --> 00:02:19,869
at visiting people
in hospitals,

50
00:02:19,920 --> 00:02:22,005
so I'm going to go.

51
00:02:22,073 --> 00:02:25,708
Hey, if you see Andy,
will you not tell him I'm here?

52
00:02:25,760 --> 00:02:27,344
- Okay.
- Because of what happened,

53
00:02:27,411 --> 00:02:28,912
- I don't want him to think...
- Stop.

54
00:02:28,980 --> 00:02:31,081
Don't want to know.

55
00:02:31,148 --> 00:02:33,483
The less I know
about other people's affairs,

56
00:02:33,551 --> 00:02:34,651
the happier I am.

57
00:02:34,719 --> 00:02:37,721
I'm not interested
in caring about people.

58
00:02:37,788 --> 00:02:39,990
I once worked
with a guy for three years

59
00:02:40,057 --> 00:02:41,992
and never learned his name...

60
00:02:42,059 --> 00:02:44,694
Best friend I ever had.

61
00:02:44,762 --> 00:02:47,897
We still never talk sometimes.

62
00:02:50,401 --> 00:02:51,951
Mmm.

63
00:02:52,036 --> 00:02:54,921
So we've developed
a revenue-sharing formula

64
00:02:55,006 --> 00:02:57,374
that will hopefully appeal
to most business owners.

65
00:02:57,425 --> 00:02:58,608
Leslie!

66
00:02:58,676 --> 00:03:00,543
Go home.
You're sick.

67
00:03:00,611 --> 00:03:02,462
I'm not sick.
It's just allergies.

68
00:03:02,546 --> 00:03:04,747
Come on, guys.
Just let me in there.

69
00:03:04,799 --> 00:03:06,082
- No.
- You can't come in here.

70
00:03:06,133 --> 00:03:07,417
You're not coming in.

71
00:03:07,485 --> 00:03:09,386
Leslie, you look tired,
and you're all sweaty.

72
00:03:09,437 --> 00:03:11,137
You look tired, and you're
all sweaty all the time.

73
00:03:11,222 --> 00:03:12,889
What's your excuse?
You want to go there, Jerry?

74
00:03:12,940 --> 00:03:14,724
- No.
- Fine, I'm coming in.

75
00:03:14,775 --> 00:03:16,893
Donna, barricade the door, now!

76
00:03:16,944 --> 00:03:19,779
Donna, come on, just let me in.

77
00:03:19,864 --> 00:03:21,097
Un-uh.

78
00:03:21,165 --> 00:03:23,233
Either go home,
or go back into quarantine.

79
00:03:23,300 --> 00:03:24,934
I'm not going home.

80
00:03:25,002 --> 00:03:26,769
Get out of here, Leslie.
Go home.

81
00:03:26,821 --> 00:03:28,438
Hit the bricks.

82
00:03:32,309 --> 00:03:33,643
Oh, come on.
No, no, no, no, no.

83
00:03:33,711 --> 00:03:35,078
She's germing up all my stuff.

84
00:03:35,129 --> 00:03:37,647
Aw, yuck.
Leslie!

85
00:03:39,917 --> 00:03:41,968
- Ann Perkins.
- Hey.

86
00:03:42,053 --> 00:03:43,169
How was your run?

87
00:03:43,254 --> 00:03:44,921
Ended
with a 5 1/2-minute mile,

88
00:03:44,972 --> 00:03:46,139
my personal low.

89
00:03:46,223 --> 00:03:48,508
I think the pavement
in this town is soft.

90
00:03:48,592 --> 00:03:51,010
- What's with the mask?
- Flu prevention.

91
00:03:51,095 --> 00:03:53,329
My body is finely tuned,
like a microchip.

92
00:03:53,397 --> 00:03:55,065
And the flu is like
a grain of sand.

93
00:03:55,132 --> 00:03:57,517
It could literally shut down
the entire system.

94
00:03:57,601 --> 00:03:59,636
My body's like a chip too.

95
00:03:59,703 --> 00:04:01,871
Potato chip.

96
00:04:01,939 --> 00:04:02,989
No.

97
00:04:03,074 --> 00:04:04,157
Speaking of potato chips,

98
00:04:04,241 --> 00:04:06,142
you and I are on
for dinner tomorrow, yes?

99
00:04:06,210 --> 00:04:07,310
Yes, definitely.

100
00:04:07,361 --> 00:04:10,313
I'm super, super excited.

101
00:04:10,381 --> 00:04:11,498
Looking forward to it.

102
00:04:11,582 --> 00:04:13,316
Excellent.

103
00:04:15,836 --> 00:04:17,454
Way to go, buddy.
Way to go.

104
00:04:17,505 --> 00:04:19,089
We've been on a couple
of dates.

105
00:04:19,156 --> 00:04:20,340
I really like him.

106
00:04:20,424 --> 00:04:24,660
The problem is,
he's like a perfect human man.

107
00:04:24,712 --> 00:04:27,497
I can't find one flaw.

108
00:04:27,565 --> 00:04:30,666
There was one time
I thought he farted.

109
00:04:30,718 --> 00:04:32,852
But it was me.

110
00:04:34,538 --> 00:04:38,224
I need to find someone
to fill in for April.

111
00:04:38,309 --> 00:04:40,743
Now, I know I'm not
gonna find someone

112
00:04:40,811 --> 00:04:44,314
who's both aggressively mean
and apathetic.

113
00:04:44,381 --> 00:04:47,750
April really is
the whole package.

114
00:04:47,818 --> 00:04:49,719
But I think
I might know someone

115
00:04:49,787 --> 00:04:52,455
who will be
just as ineffective.

116
00:04:55,910 --> 00:04:58,545
Hello, Andrew.
What's new?

117
00:04:58,629 --> 00:05:00,630
Um, a whole lot.

118
00:05:00,697 --> 00:05:02,599
Check this out.
I just invented it...

119
00:05:02,666 --> 00:05:03,833
super straw.

120
00:05:03,884 --> 00:05:07,537
♪ ♪

121
00:05:07,588 --> 00:05:09,105
Ahh.

122
00:05:09,173 --> 00:05:12,342
So just sit here
and do your thing.

123
00:05:12,409 --> 00:05:14,010
Do I have to tuck my shirt in?

124
00:05:14,061 --> 00:05:15,912
Because, honestly,
that's kind of a deal breaker.

125
00:05:15,980 --> 00:05:17,096
Let 'er fly.

126
00:05:17,181 --> 00:05:18,481
Hey, wait.

127
00:05:18,549 --> 00:05:19,882
Where's April?

128
00:05:19,934 --> 00:05:21,451
Is she all right?

129
00:05:21,519 --> 00:05:24,887
Yeah. I just gave her
the day off.

130
00:05:24,939 --> 00:05:27,790
Would it be weird
if I asked for the day off?

131
00:05:27,858 --> 00:05:30,243
Yeah, no. It's okay.
I didn't think so.

132
00:05:30,327 --> 00:05:32,061
I'm sorry.
Are you leaving?

133
00:05:32,129 --> 00:05:33,730
- I thought we had a meeting.
- No, we do.

134
00:05:33,797 --> 00:05:35,765
It's just I think
it's a little chilly in here.

135
00:05:35,833 --> 00:05:37,200
Are you okay?
Your eyes are glassy.

136
00:05:37,251 --> 00:05:38,418
Oh, my God.
Oh, my...

137
00:05:38,502 --> 00:05:40,069
Is she... is she sick?
Are you sick?

138
00:05:40,120 --> 00:05:43,173
- No.
- Yeah, she's sick.

139
00:05:43,240 --> 00:05:44,807
That's why I'm wearing this

140
00:05:44,875 --> 00:05:47,076
and misting myself
with hand sanitizer.

141
00:05:47,127 --> 00:05:48,795
I am not sick.

142
00:05:48,879 --> 00:05:50,580
I just have allergies, okay?

143
00:05:50,631 --> 00:05:51,914
I took a Claritin,
and I threw that up.

144
00:05:51,966 --> 00:05:53,616
So I took another one.
I threw that up.

145
00:05:53,684 --> 00:05:55,602
And then I took a third,
and it stayed down.

146
00:05:55,686 --> 00:05:56,719
I'm getting better.

147
00:05:56,770 --> 00:05:58,154
All right, you're burning up.

148
00:05:58,222 --> 00:05:59,856
You're burning up.
What?

149
00:05:59,923 --> 00:06:01,057
I have to get out of here.

150
00:06:01,108 --> 00:06:02,942
I have 2.8% body fat.

151
00:06:03,027 --> 00:06:04,310
My body's like a microchip.

152
00:06:04,395 --> 00:06:06,946
A grain of sand
could destroy it.

153
00:06:07,031 --> 00:06:10,066
My body's a microchip.

154
00:06:10,117 --> 00:06:11,100
Leslie, go home.

155
00:06:11,151 --> 00:06:12,535
No, I can't.
I can't go home.

156
00:06:12,603 --> 00:06:14,120
I have to get ready
for the Chamber of Secrets.

157
00:06:14,205 --> 00:06:15,605
Commerce.

158
00:06:15,673 --> 00:06:17,106
If this meeting
does not go perfectly,

159
00:06:17,174 --> 00:06:19,108
then the harvest festival's
gonna be over before it began.

160
00:06:19,176 --> 00:06:20,443
I cannot go home.

161
00:06:20,494 --> 00:06:23,796
- Okay, then who is your doctor?
- Ann's my doctor.

162
00:06:23,881 --> 00:06:26,082
And she's the most beautiful
nurse in the world.

163
00:06:26,133 --> 00:06:27,917
- Come on.
- Oh, God.

164
00:06:27,968 --> 00:06:29,252
Now I'm hot.
Now it's really hot in here.

165
00:06:29,303 --> 00:06:31,087
Okay, well, that's your fever.

166
00:06:31,138 --> 00:06:33,756
Leslie, I typed your symptoms
into the thing up here,

167
00:06:33,807 --> 00:06:37,510
and it says you could have
network connectivity problems.

168
00:06:37,595 --> 00:06:39,979
104.1.

169
00:06:40,064 --> 00:06:42,265
Leslie, you're dehydrated.
I'm admitting you.

170
00:06:42,316 --> 00:06:44,267
If I was sick, could I do this?

171
00:06:47,438 --> 00:06:50,373
- What are you doing?
- Cartwheels.

172
00:06:50,441 --> 00:06:51,708
Am I not doing them?

173
00:06:51,775 --> 00:06:53,676
- No.
- Look, don't worry.

174
00:06:53,744 --> 00:06:55,678
I've done presentations
like this before,

175
00:06:55,746 --> 00:06:56,946
and Tom will be with me.

176
00:06:56,997 --> 00:06:58,481
So you're in good hands.

177
00:06:58,532 --> 00:07:01,451
It's not
that I don't trust Ben.

178
00:07:01,502 --> 00:07:04,787
It's that I don't have
faith in Ben.

179
00:07:04,855 --> 00:07:08,291
And also, I'm starting
to forget who Ben is.

180
00:07:11,263 --> 00:07:13,405
I'll handle the general outline
if you want to uh...

181
00:07:13,525 --> 00:07:14,842
- Look.
- Just jump right in.

182
00:07:14,910 --> 00:07:16,310
The worst thing you can do

183
00:07:16,378 --> 00:07:18,346
with an important presentation
like this is over-prepare.

184
00:07:18,413 --> 00:07:19,947
- Well...
- So...

185
00:07:19,998 --> 00:07:21,716
I think it's best
if I go to the spa.

186
00:07:21,783 --> 00:07:25,386
No, we promised Leslie
we'd prep for the meeting.

187
00:07:25,454 --> 00:07:26,721
No, Ben, you promised Leslie.

188
00:07:26,788 --> 00:07:29,006
See, I never promise
Leslie anything.

189
00:07:29,091 --> 00:07:30,791
That way,
I never disappoint her.

190
00:07:30,859 --> 00:07:31,943
I try to be considerate.

191
00:07:32,010 --> 00:07:33,361
Well...

192
00:07:33,428 --> 00:07:36,830
I am off for a soak
and a schvitz.

193
00:07:36,882 --> 00:07:39,817
"Arrivederch."

194
00:07:44,973 --> 00:07:48,175
Ron Swanson's office.

195
00:07:48,243 --> 00:07:49,277
Yes.

196
00:07:49,344 --> 00:07:51,279
I will transfer you.

197
00:07:55,751 --> 00:07:59,353
I just dropped another call.

198
00:08:03,742 --> 00:08:06,661
I thought you might like
a fresh set of pillows.

199
00:08:06,712 --> 00:08:08,095
Are you trying to smother me?

200
00:08:08,163 --> 00:08:10,364
Help!
The slutty nurse is trying

201
00:08:10,415 --> 00:08:12,333
to smother me to death
with a pillow!

202
00:08:12,384 --> 00:08:13,534
Okay, never mind.

203
00:08:13,585 --> 00:08:15,419
Stay back, slut.

204
00:08:15,504 --> 00:08:17,438
I know what she's trying to do.

205
00:08:17,506 --> 00:08:18,839
She's trying to get me
to break.

206
00:08:18,890 --> 00:08:20,841
And you know what?
I'm not gonna do it.

207
00:08:20,909 --> 00:08:22,093
I'm gonna be professional,

208
00:08:22,177 --> 00:08:23,945
and I'm gonna put on
a happy face,

209
00:08:24,012 --> 00:08:25,513
and then I'm gonna
go into a supply closet

210
00:08:25,580 --> 00:08:27,915
and snap a bunch
of tongue depressors.

211
00:08:27,983 --> 00:08:30,718
Okay, so let's, um,
talk about your opening remarks.

212
00:08:30,786 --> 00:08:32,453
Do you want me
to write you a rap?

213
00:08:32,521 --> 00:08:33,754
I'll write you a rap.
No, you know what?

214
00:08:33,822 --> 00:08:35,323
You're never gonna be able
to pull it off.

215
00:08:35,390 --> 00:08:36,440
You're too white.

216
00:08:36,525 --> 00:08:37,792
How about a show tune
or something?

217
00:08:37,859 --> 00:08:40,861
I was just, uh, planning
to introduce myself...

218
00:08:40,912 --> 00:08:42,196
Non-musically,

219
00:08:42,264 --> 00:08:44,432
and then get everyone
to refer to their packets,

220
00:08:44,499 --> 00:08:46,400
and, uh, blah, blah, blah.

221
00:08:46,468 --> 00:08:48,469
No.
No "blah, blah, blah," okay?

222
00:08:48,537 --> 00:08:50,571
You need to go over
every single detail with me.

223
00:08:50,639 --> 00:08:53,240
Leslie, I promise you,
I won't half-ass this, okay?

224
00:08:53,292 --> 00:08:54,675
Now get some rest.

225
00:08:54,743 --> 00:08:56,911
I have some great news.
I think my fever just broke.

226
00:08:56,979 --> 00:08:58,596
Good-bye.

227
00:09:05,053 --> 00:09:06,087
This came out.

228
00:09:06,154 --> 00:09:07,288
Eh.

229
00:09:07,356 --> 00:09:08,589
I am starving.

230
00:09:08,640 --> 00:09:10,191
I haven't had lunch
since yesterday.

231
00:09:10,258 --> 00:09:11,759
So I'm gonna head over
to Callahan's.

232
00:09:11,810 --> 00:09:13,060
Oh, no, no, no.
Don't go there.

233
00:09:13,128 --> 00:09:14,595
They totally skimp on pickles.

234
00:09:14,646 --> 00:09:16,430
Let me go
to Bighead Joe's for you.

235
00:09:16,481 --> 00:09:18,899
They have
the most insane burritos.

236
00:09:18,950 --> 00:09:20,835
I don't much go
for ethnic food.

237
00:09:20,902 --> 00:09:22,169
No, no, no.
Trust me.

238
00:09:22,237 --> 00:09:24,071
They have one that's called
the meat tornado.

239
00:09:24,139 --> 00:09:26,324
Literally killed a guy
last year.

240
00:09:26,408 --> 00:09:28,776
You had me
at "meat tornado."

241
00:09:32,497 --> 00:09:33,914
What are you doing?

242
00:09:33,982 --> 00:09:35,416
Hey, that flu medicine
really helped.

243
00:09:35,467 --> 00:09:37,118
I feel
a thousand percent better.

244
00:09:37,185 --> 00:09:38,552
Good as new.

245
00:09:38,620 --> 00:09:40,554
Does this scarf look okay?
I don't want to look stuffy.

246
00:09:40,622 --> 00:09:42,423
But I also don't want
to look too schlubby.

247
00:09:42,474 --> 00:09:44,342
- Get back in that bed.
- So no to the scarf?

248
00:09:44,426 --> 00:09:47,128
- Get back in the bed.
- No, I'm going to that meeting.

249
00:09:47,195 --> 00:09:48,696
Either you get back in the bed,

250
00:09:48,764 --> 00:09:50,164
or I will strap you down.

251
00:09:50,232 --> 00:09:52,933
I've done it before.
Don't test me.

252
00:09:57,856 --> 00:09:59,907
- Okay, okay, okay.
- All right.

253
00:09:59,975 --> 00:10:02,143
Now, what superpower
would you rather have?

254
00:10:02,210 --> 00:10:04,995
Would you rather be able to fly
or be invisible?

255
00:10:05,080 --> 00:10:06,313
Ed, go first.

256
00:10:06,365 --> 00:10:09,149
- Uh, fly, I guess.
- I forgot to tell you.

257
00:10:09,201 --> 00:10:10,951
You can only fly five feet
above the ground.

258
00:10:12,987 --> 00:10:15,489
I bring a certain panache
and spice

259
00:10:15,557 --> 00:10:17,358
to the spa experience
for these guys.

260
00:10:17,426 --> 00:10:19,794
Before I joined, they were just
three old white dudes

261
00:10:19,845 --> 00:10:22,012
quietly boiling in hot water.

262
00:10:22,097 --> 00:10:25,199
Would you rather live
in the pocket

263
00:10:25,267 --> 00:10:27,835
of a giant kangaroo

264
00:10:27,903 --> 00:10:31,439
or have a pocket
on your own stomach

265
00:10:31,506 --> 00:10:34,341
that has a tiny kangaroo
in it all the time?

266
00:10:34,393 --> 00:10:35,726
Preston.

267
00:10:35,811 --> 00:10:38,145
Tiny kangaroo
in my stomach pocket.

268
00:10:38,196 --> 00:10:41,182
Forgot to mention,
the tiny kangaroo is a racist.

269
00:10:43,118 --> 00:10:45,519
Hey, Ann.

270
00:10:45,570 --> 00:10:46,821
What happened?

271
00:10:46,872 --> 00:10:49,523
I just saw you a few hours ago,
and you were fine.

272
00:10:49,574 --> 00:10:51,492
Not like, "damn, you're fine,"

273
00:10:51,543 --> 00:10:53,127
but fine health-wise.

274
00:10:53,194 --> 00:10:55,713
My body has no fat
to protect itself from disease.

275
00:10:55,797 --> 00:10:57,798
Things happen very quickly.
Listen to me.

276
00:10:57,866 --> 00:11:00,367
It's very important
that you replenish my body

277
00:11:00,419 --> 00:11:01,886
with electrolytes

278
00:11:01,970 --> 00:11:04,221
after every involuntary loss
of bodily fluids.

279
00:11:04,306 --> 00:11:05,973
- Oh, boy.
- My body tends to lose

280
00:11:06,040 --> 00:11:08,809
a lot of fluids when it's sick.

281
00:11:08,877 --> 00:11:10,728
My brain is on fire.
I'm dying.

282
00:11:10,812 --> 00:11:13,714
Mm. Well, you definitely
have the flu.

283
00:11:13,782 --> 00:11:16,150
Oh, my God.

284
00:11:16,218 --> 00:11:18,352
The microchip
has been compromised.

285
00:11:18,420 --> 00:11:22,323
Pierre Garcon
was a sixth-round pick.

286
00:11:22,390 --> 00:11:25,159
Collie was the fourth round.

287
00:11:25,226 --> 00:11:27,912
Indianapolis Colts
know how to draft so well.

288
00:11:27,996 --> 00:11:29,163
They really do.

289
00:11:29,230 --> 00:11:32,466
Andy, this was delicious.

290
00:11:32,534 --> 00:11:33,701
It's awesome, huh?

291
00:11:33,752 --> 00:11:37,204
It's a whole new
meat delivery system.

292
00:11:37,272 --> 00:11:38,539
Thank you, son.

293
00:11:38,590 --> 00:11:40,407
What do you say
we go out to the parking lot,

294
00:11:40,475 --> 00:11:42,877
run a few pass plays
to burn off the calories?

295
00:11:42,944 --> 00:11:46,080
You are an unstoppable
good-idea machine!

296
00:11:47,215 --> 00:11:48,599
I like Andy.

297
00:11:48,683 --> 00:11:50,618
I'm surrounded
by a lot of women

298
00:11:50,685 --> 00:11:52,453
in this department.

299
00:11:52,521 --> 00:11:54,922
And that includes the men.

300
00:11:54,990 --> 00:11:58,726
Hup, hup, hup!

301
00:11:58,777 --> 00:12:00,444
Peyton Manning
reading the defense!

302
00:12:00,529 --> 00:12:01,762
Reggie Wayne, post corner!

303
00:12:10,372 --> 00:12:11,789
Andy, wait up!

304
00:12:14,476 --> 00:12:15,793
Hey, April.

305
00:12:15,877 --> 00:12:18,779
I saw that you called for me.
What can I do for you?

306
00:12:18,830 --> 00:12:22,082
- I need more flu medicine.
- Oh, an actual request.

307
00:12:22,150 --> 00:12:24,018
Well, you can't have any.

308
00:12:24,085 --> 00:12:26,253
That stuff is powerful...
no extra doses.

309
00:12:26,321 --> 00:12:27,972
I didn't take any.

310
00:12:28,056 --> 00:12:30,724
Leslie came in here
and stole it and left.

311
00:12:32,727 --> 00:12:33,978
Leslie?

312
00:12:34,062 --> 00:12:37,648
Leslie!

313
00:12:37,732 --> 00:12:39,200
Hey, have you seen Leslie?

314
00:12:39,267 --> 00:12:40,768
I had a dream

315
00:12:40,835 --> 00:12:42,970
that she came into this room,

316
00:12:43,021 --> 00:12:46,507
stole my flu medicine,
told me not to tell you,

317
00:12:46,575 --> 00:12:48,859
and then disappeared
through that hole in the wall.

318
00:12:48,944 --> 00:12:50,544
The door?

319
00:12:50,612 --> 00:12:53,313
Maybe we should get
a suite here for them, okay?

320
00:12:53,365 --> 00:12:55,216
Ben Wyatt!
Hello!

321
00:12:55,283 --> 00:12:56,650
Uh, hi, Leslie.

322
00:12:56,701 --> 00:12:58,152
- Good to see you.
- You too.

323
00:12:58,220 --> 00:13:00,504
Wow, you're really burning up.

324
00:13:00,589 --> 00:13:01,705
Can I get some money

325
00:13:01,790 --> 00:13:03,541
for the cab that I took
over here, please?

326
00:13:03,625 --> 00:13:04,558
Sure.
How much?

327
00:13:04,626 --> 00:13:05,559
I'm not sure.

328
00:13:05,627 --> 00:13:06,760
I looked at the meter,

329
00:13:06,828 --> 00:13:09,179
and it had
Egyptian hieroglyphics on it.

330
00:13:09,264 --> 00:13:10,831
- Wow.
- Do you know the exchange rate?

331
00:13:10,882 --> 00:13:12,716
So should we do this?

332
00:13:12,801 --> 00:13:15,035
Oh, boy.
Hold on.

333
00:13:15,103 --> 00:13:16,837
- Be careful.
- What?

334
00:13:16,905 --> 00:13:20,341
The floor and the wall
just switched.

335
00:13:20,408 --> 00:13:21,775
Okay.

336
00:13:21,843 --> 00:13:23,978
Walk very carefully.

337
00:13:31,958 --> 00:13:33,500
You know, I find it a little insulting

338
00:13:33,620 --> 00:13:34,954
that you don't trust me
to handle this.

339
00:13:35,005 --> 00:13:37,290
It's not that
I don't trust you, okay?

340
00:13:37,358 --> 00:13:40,059
It's just... This harvest festival
is my project.

341
00:13:40,127 --> 00:13:42,195
It's my career on the line.

342
00:13:42,263 --> 00:13:43,396
And I just need to make sure

343
00:13:43,464 --> 00:13:46,833
that I've done everything I can
to make it work.

344
00:13:46,901 --> 00:13:48,651
Okay.

345
00:13:48,736 --> 00:13:50,170
It's showtime.

346
00:13:50,237 --> 00:13:51,771
Okay.

347
00:13:51,822 --> 00:13:53,940
Good evening, everyone.
I'm Leslie Monster.

348
00:13:53,991 --> 00:13:55,775
And this is <i>Nightline.</i>

349
00:13:55,826 --> 00:13:57,160
Okay, I wouldn't open
with that.

350
00:13:57,244 --> 00:13:58,328
No?

351
00:13:58,412 --> 00:14:00,180
- Why don't we sit down. Okay?
- Yep.

352
00:14:00,247 --> 00:14:04,050
Libertarianism is all
about individual liberty,

353
00:14:04,118 --> 00:14:05,451
and it should never be defined

354
00:14:05,519 --> 00:14:07,837
by the terms "liberal"
or "conservative."

355
00:14:07,922 --> 00:14:10,540
And communism is
no good, right?

356
00:14:10,624 --> 00:14:11,541
That's right.

357
00:14:11,625 --> 00:14:13,359
Big swing and a miss.

358
00:14:13,427 --> 00:14:14,494
And what's the word

359
00:14:14,562 --> 00:14:17,030
for when a few clerics are
in charge?

360
00:14:17,097 --> 00:14:18,064
Religious oligarchy.

361
00:14:18,132 --> 00:14:20,466
Holy [bleep].

362
00:14:20,518 --> 00:14:22,852
- I can remember things.
- I guess.

363
00:14:22,937 --> 00:14:25,405
Mm, hey, keep mine rare.

364
00:14:25,472 --> 00:14:28,558
A man after my own heart.

365
00:14:30,377 --> 00:14:33,413
I vomited
somewhere in this room.

366
00:14:33,480 --> 00:14:35,882
I don't remember where, though.

367
00:14:35,950 --> 00:14:39,586
Wait. You might want to check
that drawer.

368
00:14:39,653 --> 00:14:42,422
Stop...

369
00:14:42,489 --> 00:14:43,890
Pooping.

370
00:14:43,958 --> 00:14:46,059
You have to get up
off the floor now.

371
00:14:46,126 --> 00:14:47,427
This floor is my friend.

372
00:14:47,494 --> 00:14:49,996
Nothing like a complete
physical breakdown

373
00:14:50,064 --> 00:14:52,799
to make a guy seem
less intimidating.

374
00:14:52,867 --> 00:14:54,434
I love the flu.

375
00:14:54,501 --> 00:14:55,919
And then I said,
"uh, you know what?

376
00:14:56,003 --> 00:15:00,340
I think we need to get
these bananas out of here."

377
00:15:00,407 --> 00:15:01,641
Have a seat, boys.

378
00:15:01,709 --> 00:15:03,610
Oh, hey, Tom,
glad you could make it.

379
00:15:03,677 --> 00:15:06,045
Uh, you have a nice day
with your spa buddies?

380
00:15:06,113 --> 00:15:07,730
Actually, yes, Ben, I did.

381
00:15:07,815 --> 00:15:09,415
And my spa buddies own

382
00:15:09,483 --> 00:15:11,234
a couple of car dealerships
in town,

383
00:15:11,318 --> 00:15:12,886
and they've agreed to donate

384
00:15:12,953 --> 00:15:15,255
a fleet of vehicles
for harvest festival...

385
00:15:15,322 --> 00:15:18,124
25 vans for the entire event.

386
00:15:18,192 --> 00:15:20,460
Okay, Tom, I'm ready.
Get up there and introduce me.

387
00:15:20,527 --> 00:15:21,828
You got it.

388
00:15:21,895 --> 00:15:23,663
I can't let you
do this, Leslie.

389
00:15:23,731 --> 00:15:26,699
Hey, this ain't
your call, McCluskey.

390
00:15:26,767 --> 00:15:30,086
Hey, Ron,
can I ask you a question?

391
00:15:30,170 --> 00:15:33,106
- Sure.
- It's about April.

392
00:15:33,173 --> 00:15:36,409
I don't like to get involved
in people's personal...

393
00:15:36,477 --> 00:15:39,629
I hurt her feelings,
and now she's pissed at me.

394
00:15:39,713 --> 00:15:41,247
I feel like
if I could just explain myself,

395
00:15:41,298 --> 00:15:44,250
she'd understand,
but she's avoiding me.

396
00:15:44,318 --> 00:15:46,486
It's making me miserable.

397
00:15:49,456 --> 00:15:52,091
April's in the hospital,
sick with the flu.

398
00:15:52,142 --> 00:15:53,109
What?

399
00:15:53,193 --> 00:15:54,560
I knew it!

400
00:15:54,612 --> 00:15:55,778
I didn't know that.

401
00:15:55,863 --> 00:15:57,730
- Is she okay?
- Yeah, she's fine.

402
00:15:57,781 --> 00:15:59,565
You can go visit her
tomorrow morning.

403
00:15:59,633 --> 00:16:01,117
But don't tell her I told you.

404
00:16:01,201 --> 00:16:03,503
I won't.

405
00:16:03,570 --> 00:16:06,272
No, no.
Nope.

406
00:16:06,323 --> 00:16:07,273
Come here.
Ugh.

407
00:16:07,324 --> 00:16:08,992
Ohhh!

408
00:16:09,076 --> 00:16:12,445
Before we get started,
a quick announcement...

409
00:16:12,513 --> 00:16:15,348
Every Thursday night
is ladies' night

410
00:16:15,416 --> 00:16:18,751
down at the Snakehole Lounge
over on Burnham Avenue.

411
00:16:18,819 --> 00:16:21,254
Ladies get two drinks
for the price of one.

412
00:16:21,305 --> 00:16:22,822
Oh, no, that can't be right.

413
00:16:22,890 --> 00:16:25,425
That's way too good of a deal.

414
00:16:25,476 --> 00:16:27,126
Nope.

415
00:16:27,194 --> 00:16:29,178
That is what it says.

416
00:16:29,263 --> 00:16:30,179
Wow.

417
00:16:30,264 --> 00:16:31,648
That sounds like a fun time.

418
00:16:31,732 --> 00:16:33,700
Anyway, uh,

419
00:16:33,767 --> 00:16:35,401
it is now my great honor
to welcome to the stage

420
00:16:35,469 --> 00:16:38,371
the woman responsible
for the entire harvest festival.

421
00:16:38,439 --> 00:16:40,473
Ladies and gentlemen,
Leslie Knope!

422
00:16:53,454 --> 00:16:54,820
Every one of you,

423
00:16:54,872 --> 00:16:57,206
just by showing
up here tonight,

424
00:16:57,291 --> 00:16:58,624
has already made history.

425
00:16:58,676 --> 00:17:01,527
In days past,
the harvest festival

426
00:17:01,595 --> 00:17:03,212
was the cornerstone

427
00:17:03,297 --> 00:17:05,848
of Pawnee's
small-business community...

428
00:17:05,933 --> 00:17:07,467
a weeklong showcase

429
00:17:07,518 --> 00:17:10,803
for everything from textiles
to small farms.

430
00:17:10,854 --> 00:17:13,022
By our estimates,
as many as 30,000 people

431
00:17:13,107 --> 00:17:14,674
might attend the festival,

432
00:17:14,742 --> 00:17:16,075
and the monetary value

433
00:17:16,143 --> 00:17:18,561
of that kind of direct
customer-to-business exposure

434
00:17:18,645 --> 00:17:21,014
is, frankly, incalculable.

435
00:17:23,200 --> 00:17:25,218
What?

436
00:17:25,285 --> 00:17:26,753
Three, two, one.

437
00:17:26,820 --> 00:17:28,204
And my shift's over.

438
00:17:28,288 --> 00:17:30,189
What the [bleep]
is your problem?

439
00:17:30,257 --> 00:17:31,858
Whoa, I thought
you weren't gonna lose it.

440
00:17:31,925 --> 00:17:33,993
While I was on duty, I didn't.
Now it's just me.

441
00:17:34,061 --> 00:17:36,429
I get that you're mad
that I kissed Andy, okay?

442
00:17:36,497 --> 00:17:37,830
But it was a moment
of confusion,

443
00:17:37,898 --> 00:17:39,198
and it was a mistake.

444
00:17:39,249 --> 00:17:40,700
And I'm very sorry.

445
00:17:40,768 --> 00:17:42,085
You want to hate me forever?

446
00:17:42,169 --> 00:17:43,553
- Okay.
- Fine!

447
00:17:43,637 --> 00:17:44,937
You know what?
I don't care.

448
00:17:45,005 --> 00:17:46,372
But you shouldn't take it out
on Andy,

449
00:17:46,440 --> 00:17:48,874
because he really likes you,
and he did nothing wrong.

450
00:17:48,926 --> 00:17:52,211
And just for the record,
I'm starting to hate you too.

451
00:17:54,948 --> 00:17:57,683
That's the most
I've ever liked Ann.

452
00:17:57,735 --> 00:17:59,769
The time is now.

453
00:17:59,853 --> 00:18:01,904
The place is Pawnee.

454
00:18:01,989 --> 00:18:04,223
Let's make history.

455
00:18:06,160 --> 00:18:08,745
That was amazing.

456
00:18:08,829 --> 00:18:10,396
That was a flu-ridden
Michael Jordan

457
00:18:10,464 --> 00:18:12,598
at the '97 NBA finals.

458
00:18:12,666 --> 00:18:14,233
That was...

459
00:18:14,284 --> 00:18:16,035
Kirk Gibson
hobbling up to the plate

460
00:18:16,103 --> 00:18:17,587
and hitting a homer
off of Dennis Eckersley.

461
00:18:17,671 --> 00:18:20,740
That was...

462
00:18:20,808 --> 00:18:23,476
That was Leslie Knope.

463
00:18:23,544 --> 00:18:25,745
Thank you so much.
Any questions?

464
00:18:25,813 --> 00:18:27,463
Yes, sir?

465
00:18:27,548 --> 00:18:30,149
Are we going to get
the same sales-tax incentives

466
00:18:30,217 --> 00:18:31,267
we used to?

467
00:18:31,351 --> 00:18:33,186
That's a very good
question, sir,

468
00:18:33,253 --> 00:18:36,255
and I would counter
with my own question, which is,

469
00:18:36,306 --> 00:18:38,808
why is half of your face
all swirly?

470
00:18:38,892 --> 00:18:40,293
Okay, um, unfortunately,

471
00:18:40,360 --> 00:18:43,312
Leslie has another very
important meeting right now.

472
00:18:43,397 --> 00:18:44,814
So if you have
any other questions,

473
00:18:44,898 --> 00:18:46,599
you can just direct them,
uh, towards me.

474
00:18:46,667 --> 00:18:47,817
Give it up, everybody,

475
00:18:47,901 --> 00:18:49,602
for Scott Bakula
from <i>Quantum Leap.</i>

476
00:18:49,653 --> 00:18:52,038
All right.
Okay, let's...

477
00:18:52,105 --> 00:18:53,906
Excuse me.

478
00:18:53,957 --> 00:18:56,509
Um, and to answer
your question, sir,

479
00:18:56,577 --> 00:18:58,411
there will be
sales-tax incentives this year.

480
00:18:58,479 --> 00:18:59,912
I think I should drive you
to the hospital.

481
00:18:59,980 --> 00:19:01,581
Was I wearing a tiara
when I came in here?

482
00:19:01,648 --> 00:19:03,116
Because if you happen upon it,

483
00:19:03,183 --> 00:19:04,717
will you have Lady Pennyface
retrieve it

484
00:19:04,785 --> 00:19:06,119
and send it posthence?

485
00:19:06,186 --> 00:19:07,220
Chocolate-covered popcorn.

486
00:19:09,956 --> 00:19:11,974
- Oh, hey.
- Hey, there.

487
00:19:12,059 --> 00:19:14,227
I got you, uh,
some waffles here,

488
00:19:14,294 --> 00:19:16,095
courtesy of JJ's diner,

489
00:19:16,146 --> 00:19:17,897
and chicken soup,
courtesy of me.

490
00:19:17,964 --> 00:19:19,816
- I'll take the waffles.
- Okay.

491
00:19:19,900 --> 00:19:22,235
Thank you.

492
00:19:22,302 --> 00:19:23,903
So how did the rest
of the meeting go?

493
00:19:23,970 --> 00:19:26,823
Um, well, you said you needed

494
00:19:26,907 --> 00:19:28,474
80 total businesses
to participate.

495
00:19:28,525 --> 00:19:30,076
Yeah.

496
00:19:30,143 --> 00:19:31,244
We have 110...

497
00:19:31,311 --> 00:19:32,612
And counting.

498
00:19:32,663 --> 00:19:34,980
So, uh, nice work, Leslie.

499
00:19:35,032 --> 00:19:36,999
- Nice work to you too.
- Oh.

500
00:19:37,084 --> 00:19:39,285
Uh, left the chicken soup there,
just in case.

501
00:19:39,336 --> 00:19:41,537
It's an old family recipe.

502
00:19:41,622 --> 00:19:44,123
It's not a big deal, but...

503
00:19:44,174 --> 00:19:46,292
- Thank you for that.
- Okay.

504
00:19:50,848 --> 00:19:52,164
Hi, April.

505
00:19:52,216 --> 00:19:54,050
It's me, Andy.

506
00:19:54,134 --> 00:19:56,519
Just stay sleeping.

507
00:19:56,603 --> 00:20:00,339
I am going to be here
when you wake up.

508
00:20:00,407 --> 00:20:02,441
I will not leave your side.

509
00:20:07,347 --> 00:20:11,734
You could be asleep for hours.
Maybe I'll come back later.

510
00:20:11,818 --> 00:20:14,019
Hope you feel better.
Okay.

511
00:20:14,071 --> 00:20:16,455
Oh, and I know you think
that I'm a jerk,

512
00:20:16,523 --> 00:20:19,158
but I hope you can forgive me.

513
00:20:22,746 --> 00:20:24,463
Gross.
Your forehead is all sweaty.

514
00:20:24,531 --> 00:20:26,048
That's gross.

515
00:20:28,135 --> 00:20:29,969
But I still like you.

516
00:20:30,036 --> 00:20:31,037
Okay.

517
00:20:31,104 --> 00:20:33,038
Oh, that's disgusting.

518
00:20:40,557 --> 00:20:41,674
Got your message.
What's up?

519
00:20:41,758 --> 00:20:43,476
I got a call
from the boys upstairs.

520
00:20:44,064 --> 00:20:45,726
And they have
a new assignment for us.

521
00:20:45,846 --> 00:20:46,890
- Okay.
- Yeah.

522
00:20:47,010 --> 00:20:50,387
And I feel like we should ask
for an extension to stay here.

523
00:20:50,706 --> 00:20:52,087
Yes, definitely.
You know...

524
00:20:52,207 --> 00:20:54,092
The festival thing's
getting pretty huge,

525
00:20:54,120 --> 00:20:56,717
and, uh, couple loose ends
that need tying up.

526
00:20:56,837 --> 00:20:59,077
Good, so I'll make the call,
then, to get the extension?

527
00:20:59,123 --> 00:21:00,580
- Yeah, they need our help.
- For the loose ends.

528
00:21:00,700 --> 00:21:01,644
- Great.
- Good.

529
00:21:01,695 --> 00:21:03,556
Okay, you need
a ride back to the office?

530
00:21:03,607 --> 00:21:05,576
No, no, I'm gonna go
for a light 15k.

531
00:21:05,696 --> 00:21:06,653
I missed yesterday.

532
00:21:08,121 --> 00:21:09,475
Way to go, buddy.
Way to go.

533
00:21:09,675 --> 00:21:19,875
<font color="#ec14bd">Sync by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
