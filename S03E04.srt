﻿1
00:00:00,741 --> 00:00:01,815
Ron?

2
00:00:02,076 --> 00:00:03,675
Jack Cooper, from the controller's office.

3
00:00:03,795 --> 00:00:04,962
Got a...got a second?

4
00:00:05,030 --> 00:00:06,481
Sure, what can I do you for?

5
00:00:06,565 --> 00:00:07,899
Frankly, this is
a little awkward.

6
00:00:07,966 --> 00:00:10,501
We've received this letter
from a collection agency

7
00:00:10,569 --> 00:00:11,836
about an outstanding debt

8
00:00:11,904 --> 00:00:13,971
from the Pawnee Public Library.

9
00:00:14,039 --> 00:00:15,206
It appears
you have an overdue book.

10
00:00:15,274 --> 00:00:16,607
Oh, do I?

11
00:00:16,675 --> 00:00:18,709
<i>It's Not the Size of the Boat:</i>

12
00:00:18,777 --> 00:00:22,713
<i>Embracing Life with a Micro-Penis.</i>

13
00:00:22,781 --> 00:00:24,415
Tammy.

14
00:00:24,483 --> 00:00:26,417
My ex-wife Tammy
likes to check in

15
00:00:26,468 --> 00:00:28,319
every so often and make sure

16
00:00:28,387 --> 00:00:29,821
I'm doing okay.

17
00:00:29,888 --> 00:00:33,691
And if I am, she tries
to [bleep] everything up.

18
00:00:33,759 --> 00:00:35,626
Okay, I pre-dialed 911,
so all you have to do

19
00:00:35,694 --> 00:00:36,694
is press send.

20
00:00:36,762 --> 00:00:38,129
You're worrying over nothing.

21
00:00:38,197 --> 00:00:39,764
If she binds your hands
and you can't reach your phone,

22
00:00:39,815 --> 00:00:41,566
just try to chew yourself free.

23
00:00:44,436 --> 00:00:45,970
Whale tale.
Whale tale.

24
00:00:46,038 --> 00:00:48,039
She's flashing a whale tale.
Abort. Abort.

25
00:00:48,107 --> 00:00:49,991
Hello, Tammy.

26
00:00:50,075 --> 00:00:52,176
Oh.
Hello, Ron.

27
00:00:52,244 --> 00:00:53,978
I didn't see you come in.

28
00:00:54,029 --> 00:00:56,013
I was just checking myself
for scoliosis.

29
00:00:56,081 --> 00:00:58,332
- And?
- Straight as an arrow.

30
00:00:58,400 --> 00:01:00,952
Just like somebody else I know.

31
00:01:01,019 --> 00:01:01,986
Jerky?

32
00:01:02,054 --> 00:01:03,888
Call off the dogs.

33
00:01:03,956 --> 00:01:05,356
You and I both know

34
00:01:05,424 --> 00:01:07,158
that in my entire adult life,

35
00:01:07,226 --> 00:01:10,695
I have never checked
a book out of the library.

36
00:01:10,762 --> 00:01:13,047
Oh, my God, she's amazing.

37
00:01:13,132 --> 00:01:16,017
Ooh...
Mmm.

38
00:01:16,101 --> 00:01:18,402
I admit there was a time
when that sort of behavior

39
00:01:18,470 --> 00:01:20,521
would've driven me wild.

40
00:01:20,606 --> 00:01:23,541
But I am in a healthy
relationship now, Tammy.

41
00:01:23,609 --> 00:01:26,477
A relationship?
With whom?

42
00:01:26,545 --> 00:01:28,579
A lovely, intelligent,
self-possessed

43
00:01:28,647 --> 00:01:29,947
pediatric surgeon named Wendy.

44
00:01:30,015 --> 00:01:32,450
Sounds like a real whore.

45
00:01:32,518 --> 00:01:34,852
Clear the late charges
and cut the crap.

46
00:01:34,920 --> 00:01:38,055
Good day.

47
00:01:38,123 --> 00:01:39,524
Good day, Leslie.

48
00:01:39,591 --> 00:01:42,410
Good... good day.

49
00:01:45,497 --> 00:01:55,421
<font color="#ec14bd">Sync by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

50
00:02:02,548 --> 00:02:03,748
Okay, so we're ordering them

51
00:02:03,815 --> 00:02:06,601
a total of 30 pizzas
so let's talk toppings.

52
00:02:06,685 --> 00:02:08,085
Sausage, onion, and peppers.

53
00:02:08,153 --> 00:02:10,321
Scientifically proven
to be the best toppings.

54
00:02:10,388 --> 00:02:11,606
Nice.

55
00:02:11,690 --> 00:02:13,224
Should we throw in some salads

56
00:02:13,292 --> 00:02:14,892
for a healthy option?

57
00:02:14,943 --> 00:02:17,495
Wow, don't be such a Jerry, Ben.

58
00:02:17,563 --> 00:02:19,430
Yeah, Ben, these guys
are cops, not ballerinas.

59
00:02:19,498 --> 00:02:21,098
So we are throwing
a little shindig

60
00:02:21,166 --> 00:02:23,434
for the police department
because we're asking them

61
00:02:23,502 --> 00:02:26,070
to volunteer as security
during the harvest festival.

62
00:02:26,121 --> 00:02:28,756
I don't know what it is
about big outdoor gatherings

63
00:02:28,840 --> 00:02:31,442
that makes everyone
want to urinate

64
00:02:31,510 --> 00:02:35,446
all over everything, but it does.

65
00:02:35,514 --> 00:02:36,948
And they do.

66
00:02:37,015 --> 00:02:40,101
Okay, how about some calzones?

67
00:02:40,185 --> 00:02:41,919
Calzones are like pizzas
but they're harder to eat.

68
00:02:41,987 --> 00:02:44,655
They're dumb
and so was that idea.

69
00:02:44,723 --> 00:02:47,992
- Seriously?
- This is embarrassing for you.

70
00:02:48,060 --> 00:02:50,194
Sorry to interrupt.
Ron, you ready?

71
00:02:50,262 --> 00:02:51,629
Absolutely, my dear.

72
00:02:51,697 --> 00:02:53,598
I am off to have
a mid-morning pre-lunch

73
00:02:53,665 --> 00:02:55,066
with my lady friend,

74
00:02:55,117 --> 00:02:56,817
but I will be back
in time for lunch.

75
00:02:56,902 --> 00:02:59,303
All right.

76
00:02:59,371 --> 00:03:00,638
Oh, Ron's girlfriend's pretty.

77
00:03:03,942 --> 00:03:05,293
Tom's ex-wife.

78
00:03:05,377 --> 00:03:08,045
Oh.

79
00:03:08,113 --> 00:03:09,330
I don't want to
see them together.

80
00:03:09,414 --> 00:03:10,715
It's like they're
rubbing it in my face.

81
00:03:10,782 --> 00:03:13,184
So all I can think about
is Captain Mustache

82
00:03:13,252 --> 00:03:14,452
plowing my ex-wife.

83
00:03:14,519 --> 00:03:15,586
And you imagine
he's wearing a cape

84
00:03:15,654 --> 00:03:17,138
while he's plowing her?

85
00:03:17,222 --> 00:03:18,522
What?

86
00:03:18,590 --> 00:03:20,007
No, just, Captain Mustache?

87
00:03:20,092 --> 00:03:22,059
I mean, if all you can think of
is Ron, you know......

88
00:03:22,127 --> 00:03:23,728
Maybe put him in some tights
and a cape

89
00:03:23,795 --> 00:03:25,096
and then... and then
it would be funny.

90
00:03:25,163 --> 00:03:29,466
Now, I'm imagining a cape.

91
00:03:29,518 --> 00:03:30,668
April?

92
00:03:30,736 --> 00:03:33,671
Did you call me?

93
00:03:33,739 --> 00:03:35,306
I did call you.
Good ears.

94
00:03:35,357 --> 00:03:39,010
Chris's assistant
went back to Indianapolis,

95
00:03:39,077 --> 00:03:42,480
so he's borrowing me
for a few days.

96
00:03:42,547 --> 00:03:44,015
Yay.

97
00:03:44,082 --> 00:03:45,650
I'd like you to get me
some more post-its.

98
00:03:45,717 --> 00:03:47,285
I'd like them in multiple colors.

99
00:03:47,336 --> 00:03:48,719
I'd like green.
I'd like yellow.

100
00:03:48,787 --> 00:03:50,688
Do not buy orange.

101
00:03:50,756 --> 00:03:52,657
I do not want orange.
I have plenty of orange.

102
00:03:52,724 --> 00:03:53,991
Got it.

103
00:03:54,042 --> 00:03:55,893
You want five million
orange post-its.

104
00:03:55,961 --> 00:03:58,879
That's hilarious.

105
00:03:58,964 --> 00:04:04,501
Oh, wait!
I have a post-it for you.

106
00:04:04,553 --> 00:04:07,138
- It says, "great job."
- Mmm.

107
00:04:07,205 --> 00:04:08,773
- Great job!
- Mmm.

108
00:04:08,840 --> 00:04:10,808
There you go.

109
00:04:13,779 --> 00:04:17,081
They're getting really old
and I'm an only child.

110
00:04:17,149 --> 00:04:19,183
I just feel like
the right thing to do

111
00:04:19,251 --> 00:04:22,386
is to move back home.

112
00:04:22,454 --> 00:04:24,755
I'm sorry to see you go.

113
00:04:24,823 --> 00:04:30,027
I've really come to think
of you as...

114
00:04:30,095 --> 00:04:33,497
A companion.

115
00:04:33,565 --> 00:04:38,002
Hey, I don't suppose
you'd want to move to Canada?

116
00:04:41,139 --> 00:04:43,474
Canada.

117
00:04:43,542 --> 00:04:47,094
No, I don't suppose I would.

118
00:04:47,179 --> 00:04:49,113
Hey, I hate to nag,

119
00:04:49,181 --> 00:04:50,598
but shouldn't
we be talking strategy,

120
00:04:50,682 --> 00:04:54,685
like, when to ask the chief
for this huge critical favor?

121
00:04:54,736 --> 00:04:56,553
Yes. I know exactly
when we should do it.

122
00:04:56,605 --> 00:04:58,489
Post pizza, pre ice cream,

123
00:04:58,557 --> 00:04:59,890
between his third
and fourth beer.

124
00:04:59,941 --> 00:05:01,859
He'll be full but not stuffed,
tipsy but not drunk.

125
00:05:01,910 --> 00:05:03,060
Should be around 9:00.

126
00:05:03,111 --> 00:05:05,296
Okay, so you've
thought this through.

127
00:05:05,364 --> 00:05:08,833
Just sit tight.
I'll get you a beer.

128
00:05:08,900 --> 00:05:11,786
Can I have two beers please?

129
00:05:11,870 --> 00:05:13,087
Hey, you okay?

130
00:05:13,171 --> 00:05:15,072
Wendy and I are over.

131
00:05:15,140 --> 00:05:16,907
She's moving back to Canada.

132
00:05:16,975 --> 00:05:18,742
It's too bad.

133
00:05:18,794 --> 00:05:21,379
I just taught her how to whittle.

134
00:05:21,430 --> 00:05:24,799
She made me this
tiny sharpened stick.

135
00:05:24,883 --> 00:05:27,551
Look, I'm gonna tell you
what I tell all my girlfriends

136
00:05:27,619 --> 00:05:29,487
when they get dumped.

137
00:05:29,554 --> 00:05:31,055
Men are dogs.

138
00:05:31,123 --> 00:05:32,973
Thank you, Leslie.

139
00:05:33,058 --> 00:05:35,893
That does not apply
to this situation at all.

140
00:05:35,944 --> 00:05:38,262
But thank you.
I appreciate it.

141
00:05:38,313 --> 00:05:39,997
This is a disaster.

142
00:05:40,065 --> 00:05:42,666
You're not into football?

143
00:05:42,734 --> 00:05:45,269
I knew there was something
wrong with you.

144
00:05:45,320 --> 00:05:47,605
I knew it.
I knew it, knew it, knew it.

145
00:05:47,672 --> 00:05:49,990
Honestly, I haven't felt
this good in years.

146
00:05:50,075 --> 00:05:51,826
And it's not just because
of the supplements

147
00:05:51,910 --> 00:05:53,994
he has me taking
and the soluble fiber

148
00:05:54,079 --> 00:05:55,446
and the increase in regularity.

149
00:05:55,497 --> 00:05:56,747
It's him.

150
00:05:56,815 --> 00:05:58,816
He's, uh, moving back
to Indianapolis

151
00:05:58,884 --> 00:06:01,285
in a couple of weeks,
which sucks.

152
00:06:01,336 --> 00:06:03,788
But if he asked me
to move with him,

153
00:06:03,855 --> 00:06:06,290
I think I would.

154
00:06:06,358 --> 00:06:07,691
I would like a local beer.

155
00:06:07,759 --> 00:06:09,510
I'd like it in a bottle.

156
00:06:09,594 --> 00:06:11,095
I'd like the bottle to be cold.

157
00:06:11,163 --> 00:06:13,013
I would like
a glass of white wine.

158
00:06:13,098 --> 00:06:14,682
I would like it to be chardonnay.

159
00:06:14,766 --> 00:06:16,467
And I would like that
with one ice cube.

160
00:06:16,518 --> 00:06:18,969
Thanks.

161
00:06:19,037 --> 00:06:21,405
- Chief Trumple?
- Hey, Knope.

162
00:06:21,473 --> 00:06:24,325
You know Ben Wyatt
from the State Budget Office.

163
00:06:24,409 --> 00:06:25,543
- Yeah, how's it going?
- Hello.

164
00:06:25,610 --> 00:06:27,027
Thanks for the party.
Good pizza.

165
00:06:27,112 --> 00:06:30,197
Great. So, yeah, the pizza's
dynamite, isn't it?

166
00:06:30,282 --> 00:06:32,049
I just said it was good pizza.

167
00:06:32,117 --> 00:06:34,718
It's good.
Pizza's good.

168
00:06:34,786 --> 00:06:38,622
You, uh, you know what I like?

169
00:06:38,690 --> 00:06:40,458
Calzones.

170
00:06:40,509 --> 00:06:42,326
What the hell
is wrong with this guy?

171
00:06:42,394 --> 00:06:44,011
It's all right.
We'll check in with you later.

172
00:06:44,095 --> 00:06:45,796
- Take it easy.
- Yeah, will do.

173
00:06:45,847 --> 00:06:46,931
What is wrong with you?

174
00:06:46,998 --> 00:06:48,015
I was getting somewhere
with that.

175
00:06:48,099 --> 00:06:49,183
Yeah, great small talk
over there.

176
00:06:49,267 --> 00:06:50,234
Okay, let me handle this.

177
00:06:50,302 --> 00:06:51,352
I get a little nervous with cops.

178
00:06:51,436 --> 00:06:54,638
- I'm in control.
- What's up?

179
00:06:54,706 --> 00:06:56,524
Tom Haverford's in the building.

180
00:06:56,608 --> 00:06:57,741
Whoo!

181
00:06:57,809 --> 00:06:59,843
Tom, what the hell are you doing?

182
00:06:59,895 --> 00:07:02,146
Same thing you're doing,
celebrating Pawnee's finest.

183
00:07:02,214 --> 00:07:04,148
I believe you know
my date, Tammy Swanson.

184
00:07:04,216 --> 00:07:05,149
Hiya, Ron.

185
00:07:05,217 --> 00:07:07,401
- Oh. Hiya, Ron.
- Hey, Ron.

186
00:07:07,486 --> 00:07:08,752
I'm sorry.

187
00:07:08,820 --> 00:07:11,322
Is seeing your ex-wife
on my arm

188
00:07:11,389 --> 00:07:12,490
making you uncomfortable?

189
00:07:12,557 --> 00:07:15,593
Gee, I can't imagine why.
Or can I?

190
00:07:15,660 --> 00:07:17,912
Ooh, I like this song.
Do you wanna dance, Tammy?

191
00:07:17,996 --> 00:07:18,996
Oh, yeah. Yeah.

192
00:07:19,047 --> 00:07:21,415
Ooh. Uh.

193
00:07:21,500 --> 00:07:23,834
I know Tammy seems scary.

194
00:07:23,885 --> 00:07:26,170
But really, she's just
a manipulative, psychotic,

195
00:07:26,238 --> 00:07:29,073
library-book-pedaling,
sex-crazed she-demon.

196
00:07:32,968 --> 00:07:34,702
- I think that's enough.
- Tammy.

197
00:07:34,770 --> 00:07:36,871
Uh. Oh, hi, Ron.

198
00:07:36,922 --> 00:07:39,090
I was just tasting
my new boyfriend, Glenn.

199
00:07:39,174 --> 00:07:40,425
Tom.

200
00:07:40,509 --> 00:07:41,776
You don't know
what you're mixed up in, son.

201
00:07:41,844 --> 00:07:44,078
This isn't about you.
It's about me.

202
00:07:44,146 --> 00:07:46,931
Typical Ron Swanson.
Always thinking about yourself.

203
00:07:47,016 --> 00:07:49,717
- Maybe we like each other.
- Yeah, maybe we do.

204
00:07:49,768 --> 00:07:51,986
Ah-eey! Okay.

205
00:07:52,054 --> 00:07:53,271
Let's take it easy.

206
00:07:53,355 --> 00:07:55,723
Tammy, leave him out of this.

207
00:07:55,791 --> 00:07:57,291
Oh, so it's okay for you
to have a girlfriend,

208
00:07:57,359 --> 00:07:58,726
but I'm not supposed
to see anybody?

209
00:07:58,777 --> 00:08:00,094
Damn it, woman.

210
00:08:00,162 --> 00:08:02,096
Just crawl back in to
the dank hole you came from

211
00:08:02,164 --> 00:08:03,564
and leave my friends alone.

212
00:08:03,615 --> 00:08:04,866
Oh! Okay, okay.
Let's settle down.

213
00:08:04,933 --> 00:08:06,284
What seems to be the problem?

214
00:08:06,368 --> 00:08:07,802
- What seems to be the problem?
- Is there a problem here?

215
00:08:07,870 --> 00:08:10,271
Basically what we had here was
a dispute of a domestic nature.

216
00:08:10,339 --> 00:08:13,041
- White male, 40, 45.
- Caucasian male.

217
00:08:13,092 --> 00:08:15,476
- Stocky build.
- Approximately 5'10".

218
00:08:15,544 --> 00:08:18,146
Verbal altercation transpired
at approximately 8:55 P.M.

219
00:08:18,213 --> 00:08:19,414
with a female unsub.

220
00:08:19,481 --> 00:08:21,082
Appears to be slightly
intoxicated.

221
00:08:21,133 --> 00:08:22,784
Claims to be an ex-spouse.

222
00:08:22,851 --> 00:08:24,419
Real piece of work.

223
00:08:24,486 --> 00:08:25,920
- Real piece of work.
- Real piece of work.

224
00:08:25,988 --> 00:08:27,488
Real piece of work.

225
00:08:27,556 --> 00:08:28,923
I know why you're doing this,

226
00:08:28,974 --> 00:08:30,141
and you're making a huge mistake.

227
00:08:30,225 --> 00:08:31,592
Fight fire with fire, Leslie.

228
00:08:31,660 --> 00:08:33,027
He dates my ex, I date his.

229
00:08:33,095 --> 00:08:34,762
Ron and Wendy aren't
even dating anymore.

230
00:08:34,813 --> 00:08:36,614
She's moving back to Canada.

231
00:08:36,699 --> 00:08:38,483
- What?
- Uh-oh. Oh, God.

232
00:08:38,567 --> 00:08:39,984
Okay, hey.
What's happening here?

233
00:08:40,069 --> 00:08:42,370
Ron and I are going
to get a cup of coffee

234
00:08:42,437 --> 00:08:43,371
and talk things out.

235
00:08:43,439 --> 00:08:44,956
It's time for Tammy and me

236
00:08:45,040 --> 00:08:48,042
to bury the hatchet
once and for all.

237
00:08:48,110 --> 00:08:51,112
Everything will be fine.

238
00:08:56,085 --> 00:08:59,337
It's been like this for hours.

239
00:08:59,421 --> 00:09:00,555
Right there.
Right there.

240
00:09:00,622 --> 00:09:02,323
- Yeah. Yeah.
- Hey, Swanson!

241
00:09:04,126 --> 00:09:05,893
Snap out of it!

242
00:09:05,961 --> 00:09:07,461
Leslie.

243
00:09:07,513 --> 00:09:08,796
Congratulate us.

244
00:09:08,847 --> 00:09:12,400
Ron's got one
just like it on his penis.

245
00:09:12,467 --> 00:09:14,301
Oh, yeah.

246
00:09:14,353 --> 00:09:16,070
What the hell happened to you?

247
00:09:16,138 --> 00:09:18,406
Well...

248
00:09:28,083 --> 00:09:29,200
Thank you.

249
00:09:30,986 --> 00:09:33,538
Rock and roll!
Yeah, baby!

250
00:09:33,622 --> 00:09:34,839
- I got it!
- Uh!

251
00:09:34,923 --> 00:09:36,174
You may now kiss the bride.

252
00:09:39,261 --> 00:09:46,551
♪
♪

253
00:09:46,635 --> 00:09:49,053
It's been kind of a crazy night.

254
00:09:49,138 --> 00:09:50,354
Come back, come back.

255
00:09:52,307 --> 00:09:54,892
April, could you
come in here a second?

256
00:09:54,977 --> 00:09:57,378
April, way to come in.
Great initiative.

257
00:09:57,446 --> 00:09:59,346
You called me in.

258
00:09:59,398 --> 00:10:01,048
Listen, could you please
call Ann Perkins

259
00:10:01,116 --> 00:10:02,900
and tell her that I will be
unable to enjoy lunch

260
00:10:02,985 --> 00:10:05,253
with her today,
as I am just swamped.

261
00:10:05,320 --> 00:10:06,287
All day.

262
00:10:06,354 --> 00:10:08,856
Copy that.

263
00:10:08,907 --> 00:10:11,192
Hi, Ann.
This is April.

264
00:10:11,243 --> 00:10:12,860
I'm just calling to let you know

265
00:10:12,928 --> 00:10:14,879
that Chris can't
make lunch today.

266
00:10:14,963 --> 00:10:18,749
I knew you wouldn't care.
Good-bye.

267
00:10:18,834 --> 00:10:20,334
They started having sex
at the court house.

268
00:10:20,402 --> 00:10:21,636
We had no choice.

269
00:10:21,703 --> 00:10:23,604
Oh, well,
I completely understand.

270
00:10:23,672 --> 00:10:25,873
Chief, I need to ask you
a huge favor.

271
00:10:25,924 --> 00:10:27,141
Leslie.

272
00:10:27,209 --> 00:10:28,759
Ron is a good man

273
00:10:28,844 --> 00:10:30,311
who just got caught up
in something terrible.

274
00:10:30,378 --> 00:10:32,814
And I was hoping that maybe
you would

275
00:10:32,881 --> 00:10:36,450
reduce his charges and
release him into my custody.

276
00:10:36,518 --> 00:10:37,902
All right.

277
00:10:37,986 --> 00:10:40,154
But keep him away
from that crazy librarian.

278
00:10:40,222 --> 00:10:41,823
I'll do my best.

279
00:10:41,890 --> 00:10:42,940
In fact, I don't want him

280
00:10:43,025 --> 00:10:45,092
within 500 feet of the library.

281
00:10:45,160 --> 00:10:46,661
That's good advice for all of us.

282
00:10:46,728 --> 00:10:47,912
Nothing but trouble there.

283
00:10:47,996 --> 00:10:50,631
- Thanks a lot, bro.
- Hey, stop.

284
00:10:50,699 --> 00:10:52,617
Sorry.

285
00:10:52,701 --> 00:10:55,453
Take me back to Tammy.

286
00:10:55,537 --> 00:10:57,171
For the millionth time, no.

287
00:10:57,239 --> 00:10:59,423
That was not the favor
we needed to ask for, Leslie.

288
00:10:59,508 --> 00:11:01,509
If we don't get the police
for the harvest festival,

289
00:11:01,576 --> 00:11:03,344
there is no harvest festival.

290
00:11:03,411 --> 00:11:04,962
I know.
I'll figure it out later.

291
00:11:05,047 --> 00:11:07,932
Can you turn the radio off.
This is our song.

292
00:11:08,016 --> 00:11:09,750
Your song is
<i>Dancing on the Ceiling</i>

293
00:11:09,801 --> 00:11:11,769
by Lionel Ritchie?

294
00:11:11,854 --> 00:11:13,354
Oh, wow, look at that.

295
00:11:13,421 --> 00:11:14,555
You shaved off
part of your mustache.

296
00:11:14,623 --> 00:11:15,656
That's lovely.

297
00:11:15,724 --> 00:11:19,193
I didn't shave it off.
It rubbed off...

298
00:11:19,261 --> 00:11:20,561
From friction.

299
00:11:20,629 --> 00:11:21,696
Uhh!

300
00:11:21,763 --> 00:11:23,064
Uhhh!

301
00:11:23,115 --> 00:11:24,148
Uhh!

302
00:11:24,233 --> 00:11:26,267
Ron Swanson,
this is an intervention.

303
00:11:26,318 --> 00:11:28,502
You have been spending
the last 24 hours

304
00:11:28,570 --> 00:11:31,572
sipping on joy juice
and tripping on Tammy.

305
00:11:31,640 --> 00:11:33,824
Well, the people in this room
are your methadone.

306
00:11:33,909 --> 00:11:35,493
And we're here to get you clean.

307
00:11:35,577 --> 00:11:37,962
Congratulations.

308
00:11:38,046 --> 00:11:41,332
Holy matrimony,
there's the man of the hour.

309
00:11:41,416 --> 00:11:43,467
Jerry, what is this?

310
00:11:43,552 --> 00:11:44,585
That is the set of tumblers

311
00:11:44,653 --> 00:11:46,153
that I got from the registry.

312
00:11:46,221 --> 00:11:47,989
This is an intervention.

313
00:11:48,056 --> 00:11:49,307
I thought you said on the phone

314
00:11:49,391 --> 00:11:50,558
it was a reception.

315
00:11:50,626 --> 00:11:52,627
Okay.
All right, let's begin.

316
00:11:52,694 --> 00:11:55,513
I would like to address
the goofy-looking,

317
00:11:55,597 --> 00:11:58,666
dirty-kimono-wearing,
corn-rowed clown in the room.

318
00:11:58,734 --> 00:12:03,271
If you see Ron Swanson,
can you give him this message?

319
00:12:03,322 --> 00:12:05,539
You used to be a man.

320
00:12:05,607 --> 00:12:07,642
You need to get
your house in order.

321
00:12:07,709 --> 00:12:09,610
Look, I love you like a brother

322
00:12:09,678 --> 00:12:11,779
but right now, I hate you

323
00:12:11,830 --> 00:12:13,681
like my actual brother,
Levondrious,

324
00:12:13,749 --> 00:12:15,983
who I hate.

325
00:12:16,051 --> 00:12:17,001
Okay, Tom.

326
00:12:17,085 --> 00:12:18,986
Ron, you look great.

327
00:12:19,037 --> 00:12:21,255
Your skin is glowing.
I've never seen you so happy.

328
00:12:21,323 --> 00:12:23,257
Okay, sit down.

329
00:12:23,325 --> 00:12:25,492
Andy.

330
00:12:25,544 --> 00:12:28,596
Ron, I do not really understand

331
00:12:28,664 --> 00:12:30,598
what is going on right now.

332
00:12:30,666 --> 00:12:35,853
But no matter what,

333
00:12:35,938 --> 00:12:38,372
you must keep going.

334
00:12:38,440 --> 00:12:39,707
No.

335
00:12:39,775 --> 00:12:42,510
You must stop.

336
00:12:42,577 --> 00:12:44,478
I love you, buddy.

337
00:12:44,546 --> 00:12:47,581
Follow your dreams.

338
00:12:47,649 --> 00:12:49,383
Powerful stuff.

339
00:12:51,286 --> 00:12:54,521
April?
What are you doing here?

340
00:12:54,573 --> 00:12:57,591
I work for your boyfriend.
What are you doing here?

341
00:12:57,659 --> 00:13:00,027
I came to see why Chris
stood me up for lunch,

342
00:13:00,078 --> 00:13:01,462
but I think I just
figured it out.

343
00:13:01,513 --> 00:13:02,680
Because he doesn't like you.

344
00:13:02,764 --> 00:13:04,231
No, I'm pretty sure
that's not it.

345
00:13:04,299 --> 00:13:07,234
Actually it's because
I didn't call you deliberately.

346
00:13:07,302 --> 00:13:09,737
Which, if you ask me,
is a fireable offense.

347
00:13:09,804 --> 00:13:11,339
So I would tell him that.

348
00:13:11,406 --> 00:13:13,975
Also, he's not into you sexually.

349
00:13:14,042 --> 00:13:15,776
- Mmm.
- Ann Perkins.

350
00:13:15,827 --> 00:13:16,994
April Ludgate.

351
00:13:17,079 --> 00:13:19,246
Literally two of
my favorite people on earth.

352
00:13:19,314 --> 00:13:20,648
I'm sorry that I had
to cancel lunch.

353
00:13:20,716 --> 00:13:22,983
Not at all.
It's not a problem.

354
00:13:23,035 --> 00:13:25,853
April here was very
apologetic on the phone.

355
00:13:25,921 --> 00:13:27,288
She also sent me flowers.

356
00:13:27,339 --> 00:13:30,091
April.
Nice touch.

357
00:13:33,662 --> 00:13:36,130
Hello, Ron.
It's Ron.

358
00:13:36,198 --> 00:13:39,767
If you're watching this,
it means that once again

359
00:13:39,835 --> 00:13:41,669
you have danced with the devil.

360
00:13:41,737 --> 00:13:43,637
Right now,
you're probably thinking

361
00:13:43,705 --> 00:13:45,406
Tammy's changed.

362
00:13:45,474 --> 00:13:47,008
We'll be happy together.

363
00:13:47,075 --> 00:13:48,442
But you're only thinking that

364
00:13:48,510 --> 00:13:50,644
because she's
a monstrous parasite

365
00:13:50,712 --> 00:13:52,930
who entered through your privates

366
00:13:53,015 --> 00:13:55,316
and lodged herself in your brain.

367
00:13:55,384 --> 00:13:57,234
So you have two choices...

368
00:13:57,319 --> 00:13:59,553
One, get rid of Tammy,

369
00:13:59,604 --> 00:14:02,990
or two, lobotomy and castration.

370
00:14:03,058 --> 00:14:04,859
Choose wisely.

371
00:14:04,926 --> 00:14:07,795
You stupid.

372
00:14:07,863 --> 00:14:10,164
This is a waste of time.

373
00:14:10,231 --> 00:14:13,567
You people have no idea
what you're talking about.

374
00:14:13,635 --> 00:14:16,937
That was you on the tape.
That was you talking.

375
00:14:17,005 --> 00:14:18,706
Ron? Ron!

376
00:14:18,757 --> 00:14:21,876
Ron.
Oh, there you are.

377
00:14:21,927 --> 00:14:25,012
Ah!

378
00:14:25,080 --> 00:14:27,515
Ron. Stop it.
God, please.

379
00:14:27,582 --> 00:14:31,135
- Ron, don't do that.
- Oh, it's done, Leslie.

380
00:14:31,219 --> 00:14:32,820
Tammy and I are in love,

381
00:14:32,888 --> 00:14:34,922
and we're gonna start a family
together.

382
00:14:34,973 --> 00:14:37,191
In fact, she's ovulating.

383
00:14:37,259 --> 00:14:38,726
So if you'll excuse us,

384
00:14:38,777 --> 00:14:40,561
we're heading off
on our honeymoon.

385
00:14:40,629 --> 00:14:43,998
- Wow. Where are you going?
- Jerry.

386
00:14:44,066 --> 00:14:46,801
We're gonna spend 11 days
in my cabin in the woods.

387
00:14:46,868 --> 00:14:48,736
We bought 10 cases of Gatorade

388
00:14:48,804 --> 00:14:51,539
and a 40-pound bag
of peanuts for energy.

389
00:14:51,606 --> 00:14:54,458
- Oh, God.
- Give me it.

390
00:14:54,543 --> 00:14:58,112
All right, okay.
Excuse me.

391
00:14:58,163 --> 00:15:00,114
Oh, yeah.
Oh, God.

392
00:15:01,415 --> 00:15:02,969
Ann and I are headed out

393
00:15:03,089 --> 00:15:04,423
and I just wanted to thank you

394
00:15:04,490 --> 00:15:06,675
for everything today,
and I will see you tomorrow.

395
00:15:08,356 --> 00:15:10,190
Hi, Chris.

396
00:15:10,258 --> 00:15:12,558
Uh, Ron Swanson has sent me over

397
00:15:12,610 --> 00:15:15,328
to deliver to you this document.

398
00:15:15,396 --> 00:15:18,598
"To whom it may concern,
dear Chris,

399
00:15:18,666 --> 00:15:21,401
"there is an emergency regarding
the Parks Department,

400
00:15:21,469 --> 00:15:24,404
"and April may just be
the only person who can help.

401
00:15:24,472 --> 00:15:28,275
"I can't get into details
because it's super-classified.

402
00:15:28,342 --> 00:15:30,443
"Please release April
back to us permanently.

403
00:15:30,511 --> 00:15:32,312
"God bless America.

404
00:15:32,380 --> 00:15:36,416
Love, Bert Macklin, FBI."

405
00:15:36,484 --> 00:15:37,517
What?

406
00:15:37,584 --> 00:15:39,219
Okay. Well, bye.

407
00:15:39,270 --> 00:15:40,753
April, listen.

408
00:15:40,805 --> 00:15:42,589
If you don't want to work for me,

409
00:15:42,657 --> 00:15:43,823
I'm not gonna force you.

410
00:15:43,891 --> 00:15:45,091
You didn't have to do this.

411
00:15:45,142 --> 00:15:46,426
She didn't do that.
That... that was...

412
00:15:46,494 --> 00:15:48,194
Dude, I think it sounds
like it was Macklin's call.

413
00:15:48,262 --> 00:15:49,429
Look, I get it.

414
00:15:49,497 --> 00:15:52,899
You're young,
and trying isn't cool.

415
00:15:52,967 --> 00:15:54,334
But I think you're smart.

416
00:15:54,402 --> 00:15:57,070
I'm gonna be going back
to Indianapolis soon,

417
00:15:57,121 --> 00:15:59,539
and I think you should
come work for me.

418
00:15:59,606 --> 00:16:02,342
Wow, move all the way
to Indianapolis,

419
00:16:02,410 --> 00:16:05,145
so I can pick up your
vitamins and supplements.

420
00:16:05,212 --> 00:16:07,080
Everybody starts somewhere.

421
00:16:07,131 --> 00:16:08,315
You'll travel,

422
00:16:08,382 --> 00:16:10,684
and you will meet
interesting people.

423
00:16:10,751 --> 00:16:12,585
Think about it.

424
00:16:12,653 --> 00:16:15,855
Okay. I will.

425
00:16:15,923 --> 00:16:17,757
Apparently he wants April

426
00:16:17,825 --> 00:16:20,760
to move to Indianapolis with him.

427
00:16:20,811 --> 00:16:23,229
So that's something.

428
00:16:25,966 --> 00:16:27,367
I hope you're happy.

429
00:16:27,435 --> 00:16:30,737
Look, I just wanted to
show him what it felt like

430
00:16:30,804 --> 00:16:33,239
to have your ex-wife
date someone you know.

431
00:16:33,307 --> 00:16:35,775
Whatever happened after that
is not my fault.

432
00:16:35,826 --> 00:16:36,860
Yes, it is.

433
00:16:36,944 --> 00:16:39,162
Tom, you did
a really crappy thing,

434
00:16:39,246 --> 00:16:41,448
and I think deep down,
you know that.

435
00:16:41,515 --> 00:16:45,618
Whatever happens
to Ron Swanson is on you.

436
00:16:51,559 --> 00:16:53,793
Excuse me, chief.
Sorry to interrupt.

437
00:16:53,844 --> 00:16:55,762
Hey, calzone boy, what's up?

438
00:16:55,830 --> 00:16:58,831
Well, here's hoping that
that nickname doesn't stick.

439
00:16:58,883 --> 00:17:01,601
Right?

440
00:17:01,668 --> 00:17:03,636
But that's not why I'm here.

441
00:17:03,704 --> 00:17:07,540
Uh, Leslie Knope asked you
for a favor the other day,

442
00:17:07,608 --> 00:17:09,542
but the real favor we need
is much bigger.

443
00:17:09,610 --> 00:17:12,145
You mean,
like calzone-sized?

444
00:17:12,196 --> 00:17:15,415
Ah! Sure.

445
00:17:15,483 --> 00:17:17,484
We need the Pawnee police force

446
00:17:17,551 --> 00:17:18,752
to volunteer as security

447
00:17:18,819 --> 00:17:20,853
during the upcoming
harvest festival.

448
00:17:20,905 --> 00:17:22,989
Now, the city won't let us
throw the festival unless...

449
00:17:23,040 --> 00:17:24,591
Say no more.

450
00:17:24,658 --> 00:17:26,393
Just send me a schedule of how
many officers you need and when.

451
00:17:26,460 --> 00:17:28,762
Really?
Just like that?

452
00:17:28,829 --> 00:17:31,131
Leslie Knope gets
as many favors as she needs.

453
00:17:31,198 --> 00:17:33,066
Can I ask why?

454
00:17:33,134 --> 00:17:35,385
Because she's
the kind of a person

455
00:17:35,469 --> 00:17:37,237
who uses favors
to help other people.

456
00:17:37,304 --> 00:17:39,172
And also, my buddy Dave

457
00:17:39,223 --> 00:17:40,607
was the crankiest bastard
in the department

458
00:17:40,674 --> 00:17:41,975
till he started dating Leslie.

459
00:17:42,042 --> 00:17:45,711
Huh. So she...
Okay.

460
00:17:45,763 --> 00:17:47,547
Are they still dating or...?

461
00:17:47,615 --> 00:17:50,583
No. Moved to San Diego
a year ago.

462
00:17:50,651 --> 00:17:52,485
Oh.

463
00:17:52,553 --> 00:17:54,487
Was it a serious thing, or...?

464
00:17:54,555 --> 00:17:56,656
What do you care?
You a pervert?

465
00:17:56,724 --> 00:17:58,091
Nope, nope.
I'm all good.

466
00:17:58,159 --> 00:17:59,692
Um, everything's fine.

467
00:17:59,744 --> 00:18:01,694
This is fine.

468
00:18:03,397 --> 00:18:04,564
Don't think I won't do it.

469
00:18:04,632 --> 00:18:05,598
I'll wear this to work.

470
00:18:07,501 --> 00:18:09,068
All right, we got a little one.

471
00:18:09,120 --> 00:18:10,920
Ooh, what's in here, huh?

472
00:18:11,005 --> 00:18:13,590
A new library card?

473
00:18:13,674 --> 00:18:14,841
Read the name.

474
00:18:14,908 --> 00:18:16,342
"Tammy Swanson Swanson."

475
00:18:17,411 --> 00:18:19,679
You guys, that's amazing.

476
00:18:21,849 --> 00:18:23,767
Well, you about ready
to go, my love?

477
00:18:23,851 --> 00:18:25,251
- Yeah.
- Wait, Ron!

478
00:18:25,302 --> 00:18:26,419
Stop!

479
00:18:26,487 --> 00:18:28,354
Jerry, what the hell?

480
00:18:28,422 --> 00:18:30,756
Don't do it.
She doesn't love you.

481
00:18:30,808 --> 00:18:32,642
You don't know what
you're talking about.

482
00:18:32,726 --> 00:18:33,693
What are you doing here?

483
00:18:33,760 --> 00:18:35,929
Yes, I do, okay?

484
00:18:35,996 --> 00:18:36,946
When I asked her to be my date,

485
00:18:37,031 --> 00:18:38,064
she cackled for a full minute

486
00:18:38,115 --> 00:18:40,667
and said, and I quote,

487
00:18:40,734 --> 00:18:43,570
"anything to make Ron
miserable."

488
00:18:43,621 --> 00:18:45,605
It's not real, man.

489
00:18:45,656 --> 00:18:46,790
She's just messing with you.

490
00:18:46,874 --> 00:18:49,542
He's lying, Ron.
Stay out of this, Glenn.

491
00:18:49,610 --> 00:18:51,478
It's Tom.
And I'm not lying.

492
00:18:51,545 --> 00:18:52,996
I'm not gonna let you
destroy Ron.

493
00:18:53,080 --> 00:18:54,664
You're just gonna
have to... Ow!

494
00:18:54,748 --> 00:18:56,382
Ow! Aah!

495
00:18:56,450 --> 00:18:58,485
Oh!
He's attacking me, Ron!

496
00:18:58,552 --> 00:19:00,587
Kill him!

497
00:19:08,095 --> 00:19:11,064
Yeah!
Do your worst, Glenn!

498
00:19:11,131 --> 00:19:12,098
You're gonna have to do
better than that.

499
00:19:12,149 --> 00:19:13,533
- Aaaah!
- Aah!

500
00:19:15,536 --> 00:19:16,820
Why are you
hitting yourself, Glenn?

501
00:19:16,904 --> 00:19:18,354
Stop hitting yourself.

502
00:19:18,439 --> 00:19:20,406
Tammy, that's enough!

503
00:19:20,474 --> 00:19:21,574
What?

504
00:19:21,642 --> 00:19:23,359
Hey, baby.

505
00:19:23,444 --> 00:19:25,678
You almost had me.

506
00:19:25,746 --> 00:19:27,814
Again.

507
00:19:27,882 --> 00:19:29,883
But seeing you pick
on this pathetic,

508
00:19:29,950 --> 00:19:31,117
defenseless little man...

509
00:19:31,168 --> 00:19:32,352
Hey.

510
00:19:32,419 --> 00:19:34,988
Reminded me what kind
of a monster you are.

511
00:19:35,055 --> 00:19:36,155
You're a joke.

512
00:19:36,223 --> 00:19:38,124
You're not even a man anymore.

513
00:19:38,175 --> 00:19:40,176
Oh, and by the way, last night,

514
00:19:40,261 --> 00:19:42,212
I faked four out of the seven.

515
00:19:42,296 --> 00:19:45,231
Hmm. So did I.

516
00:19:45,299 --> 00:19:47,183
Let's go, son.

517
00:19:54,058 --> 00:19:55,742
- Hey.
- Hey.

518
00:19:55,809 --> 00:19:56,976
Sorry about the whole mess.

519
00:19:57,027 --> 00:19:58,695
The crisis has been averted,

520
00:19:58,779 --> 00:20:00,813
so let's go talk to the chief.

521
00:20:00,881 --> 00:20:02,115
Oh, I already talked to him.

522
00:20:02,182 --> 00:20:04,017
They'll give us the hours
we need, no problem.

523
00:20:04,084 --> 00:20:06,553
- Great. What a relief.
- Yeah.

524
00:20:08,222 --> 00:20:10,740
Hey, are you hungry?
I haven't eaten.

525
00:20:10,824 --> 00:20:11,891
You know, yeah.

526
00:20:11,959 --> 00:20:12,892
There's a really great
calzone place

527
00:20:12,960 --> 00:20:14,127
over in Idiotville.

528
00:20:14,194 --> 00:20:15,862
- Oh, really?
- Down on Terrible Idea Avenue.

529
00:20:15,930 --> 00:20:17,330
Okay. That's weird.

530
00:20:17,398 --> 00:20:21,601
'Cause I thought it was on
beating-a-dead-horse boulevard.

531
00:20:21,669 --> 00:20:23,236
Calzones are pointless.

532
00:20:23,304 --> 00:20:25,371
They're just pizza
that's harder to eat.

533
00:20:25,439 --> 00:20:27,574
No one likes them.

534
00:20:27,641 --> 00:20:28,975
Good day, sir.

535
00:20:29,042 --> 00:20:31,895
Leslie, I...
you know, I...

536
00:20:31,979 --> 00:20:33,646
I'm just kidding.
Let's go.

537
00:20:33,714 --> 00:20:34,880
Oh.

538
00:20:34,932 --> 00:20:36,983
Okay. That was funny.

539
00:20:40,022 --> 00:20:42,440
High and tight.

540
00:20:42,525 --> 00:20:44,526
Do you think
it's gonna leave a scar?

541
00:20:44,594 --> 00:20:46,261
Tom, women like scars.

542
00:20:46,312 --> 00:20:48,063
Shows you survived an attack,

543
00:20:48,114 --> 00:20:51,099
and they'll assume
the attack was from a man.

544
00:20:51,150 --> 00:20:54,236
Sorry, Ron, about everything.

545
00:20:54,287 --> 00:20:55,604
Ah.

546
00:20:55,671 --> 00:20:57,138
To true love.

547
00:20:57,206 --> 00:20:58,957
May we both find it.

548
00:20:59,041 --> 00:21:01,476
I'll drink to that.

549
00:21:02,545 --> 00:21:03,645
Nice glasses.

550
00:21:03,713 --> 00:21:05,780
Wedding present from Jerry.

551
00:21:05,848 --> 00:21:06,781
You're not gonna return them?

552
00:21:06,832 --> 00:21:09,017
Nah.
Too much hassle.

553
00:21:09,217 --> 00:21:19,417
<font color="#ec14bd">Sync by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
