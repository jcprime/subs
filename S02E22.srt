1
00:00:02,085 --> 00:00:03,085
Hi, there.

2
00:00:03,450 --> 00:00:04,629
What's going on here?

3
00:00:04,797 --> 00:00:06,877
Puppy.
Can we keep him?

4
00:00:07,002 --> 00:00:08,988
My neighbors
are looking for people to adopt.

5
00:00:09,113 --> 00:00:12,011
- I love him.
- I wish he had tiny puppy shoes.

6
00:00:12,639 --> 00:00:14,988
I would totally shine
his little shoes for free.

7
00:00:15,475 --> 00:00:17,275
I do say the cutest stuff.

8
00:00:17,560 --> 00:00:19,600
I love a good dog
as much as the next guy,

9
00:00:19,725 --> 00:00:21,636
but this building
doesn't allow animals.

10
00:00:21,761 --> 00:00:23,147
Andy, take him outside.

11
00:00:23,482 --> 00:00:24,649
And shoot him?

12
00:00:24,818 --> 00:00:26,920
No. Just keep him outside.

13
00:00:27,700 --> 00:00:28,527
Come on.

14
00:00:28,980 --> 00:00:32,130
I'm just a little puppy.
I ain't done nothing wrong.

15
00:00:32,469 --> 00:00:33,699
I'm just a puppy.

16
00:00:34,855 --> 00:00:36,152
I like your mustache.

17
00:00:36,703 --> 00:00:39,103
I wish I could have one,
but I can't.

18
00:00:39,498 --> 00:00:41,155
'Cause I'm just a little puppy.

19
00:00:44,402 --> 00:00:46,148
Okay, take him out and shoot him.

20
00:00:46,339 --> 00:00:47,880
- Feygnasse Team -

21
00:00:48,048 --> 00:00:49,498
.:: La Fabrique ::.

22
00:00:50,716 --> 00:00:52,569
Episode 221
<i>Telethon</i>

23
00:00:53,716 --> 00:00:54,932
mpm - lestat

24
00:01:06,556 --> 00:01:08,359
- Fire! Fire! Fire!
- What?

25
00:01:08,527 --> 00:01:10,236
- Where?
- In my belly.

26
00:01:10,404 --> 00:01:13,412
Because the "24-hour Pawnee
cares diabetes telethon"

27
00:01:13,537 --> 00:01:14,782
starts tonight.

28
00:01:14,950 --> 00:01:17,050
Goodie. Let us know how it went.

29
00:01:17,286 --> 00:01:21,092
Every year, Pawnee cares teams up
with the local cable access station

30
00:01:21,217 --> 00:01:23,196
to raise money
for diabetes research.

31
00:01:23,321 --> 00:01:25,199
And it's important, because Pawnee

32
00:01:25,324 --> 00:01:27,774
is the fourth fattest town
in the U.S.

33
00:01:28,262 --> 00:01:29,262
Goes us,

34
00:01:29,798 --> 00:01:30,998
Dallas, Tulsa,

35
00:01:31,512 --> 00:01:33,718
and certain parts
of the mall of America.

36
00:01:33,886 --> 00:01:36,930
Well, this year, I get to program
my own four-hour block.

37
00:01:37,591 --> 00:01:39,306
I know. Exciting.

38
00:01:39,819 --> 00:01:42,418
So, I've signed you all up
for multiple shifts.

39
00:01:42,637 --> 00:01:45,020
Just to answer phones
and provide moral support.

40
00:01:45,189 --> 00:01:47,102
- From when to when?
- Tonight.

41
00:01:47,473 --> 00:01:49,045
From 2:00 am to 6:00 am.

42
00:01:49,170 --> 00:01:49,900
What?

43
00:01:50,448 --> 00:01:51,618
Leslie, please.

44
00:01:52,374 --> 00:01:53,988
Tonight's kind of a big night.

45
00:01:54,961 --> 00:01:57,159
All my kids are away and...

46
00:01:57,284 --> 00:01:59,660
Gross! No!
It's Jerry's sex night!

47
00:01:59,828 --> 00:02:01,162
That ruins sex and tonight.

48
00:02:01,467 --> 00:02:03,664
Put on these t-shirts.
It'll get you in the mood.

49
00:02:03,872 --> 00:02:05,958
I stayed up all night, last night,
making these.

50
00:02:06,126 --> 00:02:09,194
You stayed up all night
the night before an all-night telethon.

51
00:02:09,713 --> 00:02:11,313
And here's why.
Boom.

52
00:02:11,575 --> 00:02:13,725
"Diabetes.
Let's dia-beat-this."

53
00:02:14,525 --> 00:02:16,317
Four hours
to come up with the slogan.

54
00:02:16,442 --> 00:02:18,252
Four hours to embroider them.

55
00:02:18,639 --> 00:02:19,597
Time well spent.

56
00:02:20,146 --> 00:02:21,148
Salad sucks.

57
00:02:21,392 --> 00:02:23,214
- There. I said it.
- Feel better?

58
00:02:23,936 --> 00:02:25,489
I've been doing some thinking.

59
00:02:25,614 --> 00:02:27,480
I'm not gonna ask Ann
to move in with me.

60
00:02:27,649 --> 00:02:29,732
- Why?
- No. I'm gonna ask her to marry me.

61
00:02:30,608 --> 00:02:32,008
I love her. And...

62
00:02:32,361 --> 00:02:33,927
- I want a partner.
- Horseback.

63
00:02:34,322 --> 00:02:37,470
You should ask her on horseback.
No, in a hot air balloon.

64
00:02:38,200 --> 00:02:40,673
She, on a hot air balloon
and you, on horseback.

65
00:02:40,798 --> 00:02:42,414
Wait.
She's in a balloon.

66
00:02:42,539 --> 00:02:45,956
You, on horseback. You point to the sky.
Up there, skywriting "Marry me, Ann."

67
00:02:46,124 --> 00:02:48,501
I think I can figure out
the right way to ask her.

68
00:02:48,669 --> 00:02:51,420
How you ask someone to marry you
is a very big deal.

69
00:02:52,073 --> 00:02:54,715
They have to repeat that story
for the rest of their lives.

70
00:02:54,884 --> 00:02:57,384
- So you think I should do it, though?
- Yeah. Definitely.

71
00:02:57,553 --> 00:02:59,178
Can you get 5 eagles?
No, get 10.

72
00:02:59,846 --> 00:03:02,097
No, it's you're life.
Get as many eagles as you want.

73
00:03:02,369 --> 00:03:04,365
So, Tom, you're in for tonight?

74
00:03:04,490 --> 00:03:06,643
I forgot to tell you I can't make it

75
00:03:06,923 --> 00:03:09,357
because I have no interest
in being there.

76
00:03:09,482 --> 00:03:12,726
Okay, you're in charge
of the V.I.P. special telethon guest.

77
00:03:12,977 --> 00:03:15,276
You'll never believe who I got.
People are gonna freak out.

78
00:03:15,445 --> 00:03:16,654
- Rihanna.
- No.

79
00:03:16,822 --> 00:03:17,780
Dr. Oz.

80
00:03:17,948 --> 00:03:19,240
You're never gonna guess.

81
00:03:19,408 --> 00:03:20,882
Justin Bieber.

82
00:03:22,292 --> 00:03:25,579
Ex-Indiana Pacers small forward
Detlef Schrempf.

83
00:03:26,748 --> 00:03:28,415
The Detlef Schrempf?

84
00:03:29,501 --> 00:03:30,424
I know!

85
00:03:30,752 --> 00:03:33,405
So it's really exciting.
And I need you to be his body man.

86
00:03:33,530 --> 00:03:35,933
Pick him up at the airport at 7:00 pm
and then,

87
00:03:36,058 --> 00:03:39,051
entertain him because he doesn't need
to be in hair and makeup until 2:00 am.

88
00:03:39,219 --> 00:03:41,409
Does Pawnee cable access
even have hair and makeup?

89
00:03:41,534 --> 00:03:44,431
Well, they have a communal lipstick
and a box of combs.

90
00:03:45,736 --> 00:03:47,726
As a nurse and as your friend,
I highly suggest

91
00:03:47,894 --> 00:03:49,937
you don't stay up
for the next 24 hours.

92
00:03:50,106 --> 00:03:53,399
I can definitely do it.
I've already been up for 24 hours.

93
00:03:53,568 --> 00:03:55,418
But I have a secret weapon.

94
00:03:57,656 --> 00:04:00,322
- Nutriyum bars? Leslie.
- I know. They're terrible.

95
00:04:01,051 --> 00:04:03,547
But they give me
an insane 15 minute burst.

96
00:04:03,672 --> 00:04:06,051
Plus my nana used to tell me
"you'll sleep when you're dead."

97
00:04:06,176 --> 00:04:07,872
She used to say the best stuff.

98
00:04:08,311 --> 00:04:10,118
"Don't work yourself into a lather."

99
00:04:10,243 --> 00:04:12,921
"Look where it is and you'll find it."
"Don't put me in a home."

100
00:04:13,046 --> 00:04:16,324
"Tell the truth and shame the devil."
"The devil knows where you're hiding."

101
00:04:16,449 --> 00:04:19,620
"If you take enough rides with the devil
pretty soon he's gonna drive."

102
00:04:20,035 --> 00:04:21,510
She was really into the devil.

103
00:04:21,778 --> 00:04:23,529
I haven't checked in
with you lately.

104
00:04:23,654 --> 00:04:26,015
I'm randomly and casually asking
how are things going with Mark?

105
00:04:26,815 --> 00:04:27,683
Good.

106
00:04:28,182 --> 00:04:29,268
He's the one?

107
00:04:29,544 --> 00:04:31,145
The one? I don't know.

108
00:04:31,477 --> 00:04:32,827
- Maybe.
- Good.

109
00:04:33,451 --> 00:04:34,472
I'm here.

110
00:04:34,597 --> 00:04:38,553
- What do I have to do?
- I have you on phone answering duty

111
00:04:38,776 --> 00:04:40,696
from 4:00 to 6:00 am.

112
00:04:40,864 --> 00:04:41,960
That's horrible.

113
00:04:42,342 --> 00:04:43,490
I have to nap up.

114
00:04:43,659 --> 00:04:46,744
If I don't get a solid five,
it kills my sunny disposition.

115
00:04:47,389 --> 00:04:49,113
Got enough leg room back there?

116
00:04:49,325 --> 00:04:51,415
Yeah.
You don't have to sit so far up.

117
00:04:51,583 --> 00:04:52,583
Yeah, I do.

118
00:04:53,666 --> 00:04:55,502
So... Detlef Schrempf...

119
00:04:55,812 --> 00:04:57,338
Three-time N.B.A. all-star.

120
00:04:57,506 --> 00:04:58,995
Two-time six-man award winner.

121
00:04:59,120 --> 00:05:01,769
Must've been pretty cool to be traded
to the pacers in exchange

122
00:05:01,894 --> 00:05:04,051
for veteran center Herb Williams.

123
00:05:04,176 --> 00:05:06,171
So you looked me up on Wikipedia?

124
00:05:07,801 --> 00:05:09,087
Everybody knows that.

125
00:05:09,699 --> 00:05:11,575
Just like everybody knows
you appeared

126
00:05:11,700 --> 00:05:14,048
in two episodes
of the German soap opera

127
00:05:14,314 --> 00:05:16,190
Gute zeiten, schlechte zeiten.

128
00:05:16,405 --> 00:05:18,943
- What, you a big fan of that show?
- Huge fan.

129
00:05:19,953 --> 00:05:20,953
6'10".

130
00:05:21,146 --> 00:05:23,503
I'm 5'6" and three-quarters.

131
00:05:25,327 --> 00:05:27,159
All right, team, you guys psyched?

132
00:05:27,328 --> 00:05:29,820
Remember, you have to take down
everybody's name and address

133
00:05:29,945 --> 00:05:31,694
and ask them how much they donate.

134
00:05:32,124 --> 00:05:34,500
- Do these phones dial out?
- Yes. Why?

135
00:05:34,767 --> 00:05:35,749
No reason.

136
00:05:36,575 --> 00:05:39,369
My phone has 25 lines.
I think it might be a switchboard.

137
00:05:39,756 --> 00:05:41,465
Congratulations.
You got the most lines.

138
00:05:41,634 --> 00:05:42,633
Figure it out.

139
00:05:42,801 --> 00:05:43,759
Come on.

140
00:05:44,118 --> 00:05:45,636
Everyone else has one line.

141
00:05:47,180 --> 00:05:48,138
So, Leslie,

142
00:05:48,490 --> 00:05:50,474
You'll be
in front of the camera this year.

143
00:05:50,643 --> 00:05:52,452
- It's exciting.
- Cut the chatter.

144
00:05:52,577 --> 00:05:55,437
Telehosting not as easy as it looks.

145
00:05:55,605 --> 00:05:56,897
This isn't c-span.

146
00:05:57,065 --> 00:05:58,983
This is local access 46.

147
00:05:59,805 --> 00:06:01,005
Don't blow it.

148
00:06:01,915 --> 00:06:02,987
Good evening

149
00:06:03,155 --> 00:06:06,907
and welcome to the 10th annual
Pawnee cares diabetes telethon.

150
00:06:07,075 --> 00:06:09,795
I'm Pawnee today's Joan Callamezzo.

151
00:06:09,920 --> 00:06:11,619
And I am not a crook.

152
00:06:12,876 --> 00:06:14,126
No, he's not.

153
00:06:14,501 --> 00:06:16,417
What time is it?
I'm tired already.

154
00:06:16,836 --> 00:06:18,314
It's 8:03.

155
00:06:18,439 --> 00:06:20,629
Your shift doesn't start
for another six hours.

156
00:06:22,049 --> 00:06:23,049
Diabetes.

157
00:06:23,425 --> 00:06:24,225
Yuck.

158
00:06:24,544 --> 00:06:27,386
Tonight we're hoping
the people of Pawnee dig their big,

159
00:06:27,554 --> 00:06:30,465
chubby hands
into their plus-sized pockets

160
00:06:30,590 --> 00:06:31,911
and donate generously.

161
00:06:32,101 --> 00:06:33,764
We're wacky stuff.

162
00:06:34,530 --> 00:06:35,435
Stop it.

163
00:06:36,005 --> 00:06:36,854
Coming up,

164
00:06:37,022 --> 00:06:39,572
a very special
video presentation called

165
00:06:39,970 --> 00:06:42,818
"Even my tongue is fat,
the story of Pawnee."

166
00:06:42,987 --> 00:06:44,672
But right now, to begin with,

167
00:06:44,797 --> 00:06:47,364
let's start things off on our telethon
with a song.

168
00:06:47,533 --> 00:06:50,841
From Pawnee's
most bookable personality,

169
00:06:50,966 --> 00:06:52,244
Denise Yermley!

170
00:07:00,129 --> 00:07:01,462
Pawnee cares, hello.

171
00:07:01,630 --> 00:07:03,796
- Here we go. Head up there.
- $50 is great.

172
00:07:04,961 --> 00:07:06,606
Who... Pawnee...

173
00:07:07,677 --> 00:07:09,258
Who's next?
I feel great.

174
00:07:09,383 --> 00:07:11,221
Everything is running smoothly.

175
00:07:16,386 --> 00:07:17,686
Clipboard mouth.

176
00:07:19,076 --> 00:07:20,397
There's a secret ingredient

177
00:07:20,565 --> 00:07:22,764
in these Nutriyum bars
that make me feel so good.

178
00:07:22,943 --> 00:07:23,943
Sugar.

179
00:07:24,244 --> 00:07:25,844
It's a block of sugar.

180
00:07:31,243 --> 00:07:32,152
Tigers.

181
00:07:32,619 --> 00:07:34,524
You told me to wake you up at 1:45.

182
00:07:34,649 --> 00:07:36,037
- It's 1:45 now.
- I'm up.

183
00:07:36,350 --> 00:07:38,551
I need to pull it together.
My slot's coming up.

184
00:07:38,676 --> 00:07:41,460
Before you go on,
can I just talk to you about something?

185
00:07:41,629 --> 00:07:43,045
Wait a minute. Sorry.

186
00:07:44,030 --> 00:07:46,131
It's Tom.
He may have a Schrempf problem.

187
00:07:46,300 --> 00:07:48,258
- You understand.
- I do. Take it.

188
00:07:48,426 --> 00:07:49,927
To be continued.
I promise.

189
00:07:50,095 --> 00:07:51,095
We'll talk later.

190
00:07:52,752 --> 00:07:55,231
- Are you on your way?
- <i>The snakehole is</i>

191
00:07:55,356 --> 00:07:56,308
<i>booming!</i>

192
00:07:56,773 --> 00:07:59,144
People are loving Detlef Schrempf.

193
00:07:59,313 --> 00:08:00,813
Professional athletes

194
00:08:00,982 --> 00:08:03,148
- are so popular.
<i>- That's awesome.</i>

195
00:08:03,317 --> 00:08:05,818
- But you promised to get him here.
- Don't worry.

196
00:08:05,986 --> 00:08:08,056
We're leaving soon, all right?
Bye.

197
00:08:08,435 --> 00:08:11,782
You can't leave. This is the most
business the club's done in months.

198
00:08:11,951 --> 00:08:14,785
- But I got to get him to the telethon.
- He's not going anywhere.

199
00:08:18,516 --> 00:08:19,368
Problem!

200
00:08:20,417 --> 00:08:22,935
Andy, I told you
that you were going on at 3:00 am

201
00:08:23,060 --> 00:08:25,539
after the Detlef Schrempf interview
and career retrospective,

202
00:08:25,664 --> 00:08:28,130
but you're going on now.
You're kicking things off.

203
00:08:28,361 --> 00:08:29,671
Sweet. Headlining.

204
00:08:30,093 --> 00:08:32,301
At 2:00 on cable access.

205
00:08:33,983 --> 00:08:34,983
Let's go.

206
00:08:38,126 --> 00:08:39,768
How are things going with you two?

207
00:08:40,232 --> 00:08:43,438
Really well. We're gonna get married.
And I'm pregnant with his child.

208
00:08:45,401 --> 00:08:46,701
Thank you, Joan.

209
00:08:46,986 --> 00:08:48,981
There are two types of diabetes,

210
00:08:49,106 --> 00:08:50,902
but only one type of caring.

211
00:08:51,249 --> 00:08:52,449
Type-1 caring.

212
00:08:53,572 --> 00:08:55,322
And tonight, God willing,

213
00:08:55,541 --> 00:08:57,301
we will all be stricken with that.

214
00:08:57,496 --> 00:08:59,056
A lot of fun stuff coming up.

215
00:08:59,331 --> 00:09:01,618
Indiana Pacers legend
Detlef Schrempf

216
00:09:02,102 --> 00:09:04,501
will be joining us
in the studio later on.

217
00:09:04,734 --> 00:09:06,816
But until then,
I would like to introduce

218
00:09:06,941 --> 00:09:09,485
one of the hottest bands in Pawnee.

219
00:09:10,056 --> 00:09:12,968
Ladies and gentlemen,
I give you Mouse Rat.

220
00:09:17,528 --> 00:09:19,099
Well, slight hiccup,

221
00:09:19,267 --> 00:09:20,656
but we're back on track.

222
00:09:21,038 --> 00:09:21,977
See the board?

223
00:09:22,571 --> 00:09:24,855
When my shift is done,
that board is gonna read

224
00:09:25,023 --> 00:09:26,023
$20,000.

225
00:09:30,451 --> 00:09:31,684
<i>Pawnee cares.</i>

226
00:09:31,809 --> 00:09:34,359
Hi, yes,
I'd like to donate $50, please.

227
00:09:38,662 --> 00:09:40,495
<i>You got sex hair</i>

228
00:09:40,663 --> 00:09:42,868
<i>You got it from me, girl</i>

229
00:09:42,993 --> 00:09:44,288
<i>Sex hair</i>

230
00:09:44,709 --> 00:09:47,698
God, we're not getting
any donations coming in.

231
00:09:47,823 --> 00:09:50,755
I'm tired of waiting. Let's call them.
Everybody call somebody.

232
00:09:50,924 --> 00:09:53,425
- It's the middle of the night.
- Then good. They'll be home.

233
00:09:53,593 --> 00:09:56,512
- Someone good?
- No, I'm talking to my brother George.

234
00:09:56,684 --> 00:09:58,430
You can't receive personal calls.

235
00:09:58,743 --> 00:09:59,790
I called him.

236
00:09:59,915 --> 00:10:02,768
He's in Liberia visiting my uncle.
Wants to know what happened on <i>Lost.</i>

237
00:10:03,716 --> 00:10:06,010
- Thank God.
- There you go.

238
00:10:07,916 --> 00:10:11,109
- Did you put cream in this?
- Yeah. Did I screw up?

239
00:10:11,277 --> 00:10:12,409
No. I can drink it.

240
00:10:12,534 --> 00:10:14,196
Guys, has anyone seen Tom?

241
00:10:14,519 --> 00:10:17,353
The ultimate celebrity,
I think, to hang out with for a night

242
00:10:17,478 --> 00:10:20,325
would be Criss Angel.

243
00:10:20,745 --> 00:10:23,525
Like, you'll be talking to him
and then he would just turn into fire.

244
00:10:25,208 --> 00:10:28,168
Brooks brothers boys
is, like, the cuts are slimmer

245
00:10:28,336 --> 00:10:30,570
and it's cheaper.
Win, win.

246
00:10:31,005 --> 00:10:33,604
You ever talk to someone
and you're just, like,

247
00:10:33,729 --> 00:10:36,593
"We're gonna be best friends"?
I'm getting that, like, right now.

248
00:10:36,877 --> 00:10:38,227
He had two beers.

249
00:10:38,807 --> 00:10:39,907
Light beers.

250
00:10:40,932 --> 00:10:43,617
I need you to make
that out to Wendy.

251
00:10:43,742 --> 00:10:45,852
"Tom is an amazing guy.

252
00:10:46,020 --> 00:10:47,888
"You never should have left him.

253
00:10:48,013 --> 00:10:51,531
"You made a huge mistaken in your life.
And you're probably gonna die alone.

254
00:10:51,656 --> 00:10:52,856
"Love, Detlef."

255
00:10:53,236 --> 00:10:55,362
I think
we should be heading to that telethon.

256
00:10:55,712 --> 00:10:58,143
Yes. We will definitely
go to the telethon soon.

257
00:10:58,268 --> 00:10:59,590
After this song!

258
00:11:10,626 --> 00:11:11,932
Ron, wake up.

259
00:11:12,596 --> 00:11:13,797
Wake up. It's Leslie.

260
00:11:15,967 --> 00:11:17,288
Were you having a bad dream?

261
00:11:17,413 --> 00:11:20,304
No. I suffer from a disorder
called "sleep fighting".

262
00:11:20,745 --> 00:11:23,115
- That must be terrible.
- Only when I'm losing.

263
00:11:23,316 --> 00:11:25,161
Look, I'm freaking out, okay?

264
00:11:25,286 --> 00:11:27,978
Tom is not here.
And he's got Detlef Schrempf.

265
00:11:28,146 --> 00:11:29,688
And I have three hours to fill.

266
00:11:30,271 --> 00:11:31,690
You'll figure something out.

267
00:11:31,858 --> 00:11:34,091
Don't go back to sleep, okay?
I need you to wake up!

268
00:11:34,216 --> 00:11:35,360
Come on! Help me!

269
00:11:36,531 --> 00:11:37,531
Attaboy.

270
00:11:37,718 --> 00:11:41,150
I am only here because I owe Leslie
a thousand favors.

271
00:11:41,275 --> 00:11:43,025
I'm not big on charities.

272
00:11:43,771 --> 00:11:46,571
Give a man a fish
and you feed him for a day.

273
00:11:47,334 --> 00:11:50,284
Don't teach a man to fish
and you feed yourself.

274
00:11:51,010 --> 00:11:53,660
He's a grown man.
Fishing's not that hard.

275
00:11:57,379 --> 00:12:00,049
Okay, that was the national
anthem of Canada.

276
00:12:01,367 --> 00:12:05,366
And now I'm going to see
how long I can hold a d-chord.

277
00:12:07,018 --> 00:12:08,418
Call the rec center teachers

278
00:12:08,543 --> 00:12:11,063
and see if they want
to show off their special skills?

279
00:12:11,231 --> 00:12:13,523
- Yes. Nod your head "yes".
- I can do magic.

280
00:12:16,024 --> 00:12:16,943
Egg, milady?

281
00:12:17,217 --> 00:12:20,238
- Jerry, that's disgusting and fake!
- Oh, my God. It's real.

282
00:12:20,406 --> 00:12:22,157
- Go do that.
- You broke my egg.

283
00:12:22,325 --> 00:12:24,893
- You don't have a second egg?
- No, but I'm a very good piano player.

284
00:12:25,018 --> 00:12:26,203
I'd be happy to play.

285
00:12:26,371 --> 00:12:28,997
Why are you wasting my time?
This is really serious!

286
00:12:30,134 --> 00:12:32,000
You go keep your eye out for Tom.

287
00:12:32,504 --> 00:12:33,752
I got this covered.

288
00:12:34,064 --> 00:12:36,129
When re-caning an old chair,

289
00:12:36,436 --> 00:12:40,175
one needs to make sure that one
has all the right elements in place.

290
00:12:40,427 --> 00:12:41,427
Over.

291
00:12:41,689 --> 00:12:42,594
Under.

292
00:12:44,921 --> 00:12:46,264
And you guessed it.
Over.

293
00:12:46,608 --> 00:12:48,392
God, he's actually losing money.

294
00:12:48,791 --> 00:12:50,435
Over. Under.

295
00:12:50,764 --> 00:12:52,938
This chair's almost caning itself.

296
00:12:53,846 --> 00:12:54,856
No, sir.

297
00:12:55,024 --> 00:12:57,272
If you want to make a donation,
you have to...

298
00:12:57,538 --> 00:13:00,160
Yeah, that sounds really cool.

299
00:13:00,587 --> 00:13:03,596
You sound cool.
You sound really strong.

300
00:13:04,200 --> 00:13:06,910
Why don't you forget about the donation
and just come down here

301
00:13:07,078 --> 00:13:08,635
and meet me in person.

302
00:13:08,997 --> 00:13:09,997
What?

303
00:13:12,375 --> 00:13:14,008
You're so... funny

304
00:13:14,133 --> 00:13:16,440
and you're... funny. Bye.

305
00:13:19,197 --> 00:13:20,590
Funny girl you were talking to?

306
00:13:21,056 --> 00:13:23,176
When it comes to preparing taxes...

307
00:13:23,485 --> 00:13:25,420
Oh, my God.
This is a disaster.

308
00:13:25,545 --> 00:13:27,764
The only thing that's taxing

309
00:13:27,932 --> 00:13:30,225
is deciding which software to buy.

310
00:13:30,477 --> 00:13:32,098
I am completely screwed.

311
00:13:32,270 --> 00:13:34,218
I have no Schrempf, no backup plan,

312
00:13:34,343 --> 00:13:35,730
no more nutriyum bars.

313
00:13:36,223 --> 00:13:39,450
What do people want to watch?
Cute animals on a bike?

314
00:13:39,711 --> 00:13:42,722
No time to train one.
Hot people kissing?

315
00:13:43,204 --> 00:13:45,272
I don't know,
maybe Mark and Ann would...

316
00:13:47,662 --> 00:13:49,161
Have you ever seen this man sleep?

317
00:13:49,329 --> 00:13:51,204
It's like underwater ballet.

318
00:13:51,416 --> 00:13:53,582
- I have a great idea.
- What's that?

319
00:13:53,813 --> 00:13:56,581
I think you should propose to Ann
tonight...

320
00:13:56,706 --> 00:13:58,809
today...
Whatever it is...

321
00:13:58,934 --> 00:14:00,942
On camera for diabetes.

322
00:14:01,364 --> 00:14:04,203
- Really?
- Yes. I would be so cool.

323
00:14:04,956 --> 00:14:07,971
Don't you remember when Ahmad Rashad
proposed to Mrs. Cosby on TV?

324
00:14:08,292 --> 00:14:10,440
- No.
- I do. And most women do.

325
00:14:11,222 --> 00:14:12,772
On television though.

326
00:14:13,615 --> 00:14:15,228
- Yes, but...
- Oh, my God.

327
00:14:15,396 --> 00:14:17,653
- We talked about something big.
- Right.

328
00:14:17,778 --> 00:14:20,275
And I don't think we can get
a hot air balloon this time of night.

329
00:14:20,583 --> 00:14:22,433
I have my grandma's ring.

330
00:14:23,765 --> 00:14:25,906
I guess I could go home and get it.

331
00:14:26,145 --> 00:14:28,862
Is there a story behind it?
Was she on the Titanic?

332
00:14:29,377 --> 00:14:30,619
Was she on the Titanic?

333
00:14:31,010 --> 00:14:32,954
No.
She was just my grandma.

334
00:14:33,249 --> 00:14:35,470
It would make a great story.

335
00:14:35,595 --> 00:14:36,996
I trust Leslie.

336
00:14:37,121 --> 00:14:38,271
I don't know.

337
00:14:39,081 --> 00:14:41,081
What do you think I should do?

338
00:14:41,714 --> 00:14:43,340
And that concludes

339
00:14:43,508 --> 00:14:44,549
a quick look

340
00:14:44,732 --> 00:14:46,718
at quickbook... s pro.

341
00:14:48,414 --> 00:14:49,471
Quickbook... s!

342
00:14:50,182 --> 00:14:51,723
Thank you so much, Barney,

343
00:14:51,891 --> 00:14:53,153
for... that.

344
00:14:55,103 --> 00:14:56,032
Go.

345
00:14:56,569 --> 00:15:00,573
We'd like to make a quick announcement.
Detlef Schrempf is temporary delayed.

346
00:15:01,228 --> 00:15:04,903
But, coming soon, a really
amazing thing is going to happen.

347
00:15:05,071 --> 00:15:08,448
So get your wallets out
or get your handkerchiefs out.

348
00:15:09,129 --> 00:15:12,410
Or get your tissues out if like me
you think handkerchiefs are gross.

349
00:15:12,999 --> 00:15:15,995
For now, please, let's take
a look at this moving video

350
00:15:16,120 --> 00:15:19,162
entitled <i>One butt, two seats:</i>

351
00:15:19,513 --> 00:15:21,002
<i>the widening of America</i>.

352
00:15:23,975 --> 00:15:24,840
Wait!

353
00:15:25,486 --> 00:15:27,842
Where are you going?
Just stay a little while longer.

354
00:15:28,010 --> 00:15:31,232
- Leslie, I'm so beat.
- I'm so sorry that I've been crazy.

355
00:15:31,357 --> 00:15:34,015
- What did you want to talk to me about?
- No worries. It can wait.

356
00:15:34,183 --> 00:15:35,183
What is it?

357
00:15:36,545 --> 00:15:38,478
I think
I need to break up with Mark.

358
00:15:42,700 --> 00:15:45,569
I've just been feeling for a while
like something's missing.

359
00:15:45,883 --> 00:15:47,112
And I kept thinking about

360
00:15:47,559 --> 00:15:50,949
the question you asked me today
about whether or not he's the one.

361
00:15:51,247 --> 00:15:53,577
- And he's not.
- Mark really loves you.

362
00:15:54,308 --> 00:15:56,580
I think he's ready to take this
to the next step.

363
00:15:56,748 --> 00:15:59,916
He doesn't know what he's ready for,
but I think I do.

364
00:16:00,084 --> 00:16:02,544
Guess you're just gonna have
to marry him and figure it out.

365
00:16:04,047 --> 00:16:05,412
Why would you do that?

366
00:16:05,674 --> 00:16:07,074
Good for you, Ann.

367
00:16:08,533 --> 00:16:12,387
Hey, Mark, it's Leslie.
Change of plans. Can you call me back?

368
00:16:12,555 --> 00:16:14,180
Tom, get here! Now!
Call me! Bye!

369
00:16:14,741 --> 00:16:16,149
Hey, Leslie, it's Leslie.

370
00:16:16,397 --> 00:16:18,059
Hang in there.
I love you. Bye.

371
00:16:20,290 --> 00:16:21,940
Please be April. April?

372
00:16:24,491 --> 00:16:25,949
It's Joe. From the phone.

373
00:16:26,074 --> 00:16:27,944
I made you laugh.
You said come down.

374
00:16:29,197 --> 00:16:30,697
My van's out back, let's roll.

375
00:16:31,518 --> 00:16:34,273
- No. Please leave.
- Where you going?

376
00:16:35,136 --> 00:16:37,030
- What's up?
- Is this guy bothering you?

377
00:16:37,300 --> 00:16:39,831
No. I'm bothering you
for bothering her.

378
00:16:40,294 --> 00:16:42,918
Are you her bodyguard?
I mean, she's an adult.

379
00:16:43,086 --> 00:16:45,754
I think she can decide on her own
what train she wants to ride.

380
00:16:46,035 --> 00:16:47,589
- It's time to go.
- Whatever, man.

381
00:16:47,782 --> 00:16:51,214
I work for the sewage department.
I'm up to my waist in hot snizz.

382
00:16:51,807 --> 00:16:52,989
Take a walk. Bye-bye.

383
00:17:00,613 --> 00:17:02,131
Heads.
Oh, boy.

384
00:17:02,663 --> 00:17:04,230
What is gonna happen next?

385
00:17:04,355 --> 00:17:06,565
Okay, my second favorite episode,

386
00:17:06,852 --> 00:17:08,437
Monica's making dinner.

387
00:17:08,562 --> 00:17:11,947
Joey is mad at Chandler because Chandler
made out with his girlfriend.

388
00:17:12,119 --> 00:17:15,675
So Joey says, "get in the box".
I forgot to tell you there's a box.

389
00:17:15,868 --> 00:17:17,494
And it's Thanksgiving day.

390
00:17:17,944 --> 00:17:21,054
What is Ross and Rachel doing?
They're fighting.

391
00:17:21,179 --> 00:17:22,728
So Ross, Rachel, Joey, Chandler,

392
00:17:22,853 --> 00:17:24,959
Monica, and Phoebe are all together.

393
00:17:25,348 --> 00:17:26,711
Although is phoebe there?

394
00:17:31,213 --> 00:17:32,175
Yes. So...

395
00:17:33,033 --> 00:17:36,097
I will now drink eight glasses of milk
in three minutes.

396
00:17:36,222 --> 00:17:37,790
No, you won't.
No, you won't.

397
00:17:37,915 --> 00:17:39,849
Because if you do that,
you will die.

398
00:17:56,408 --> 00:17:58,576
All right. Okay.
Enough of that racket.

399
00:17:59,489 --> 00:18:01,287
Pawnee, it's...

400
00:18:02,045 --> 00:18:03,497
almost 6:00 in the morning.

401
00:18:03,622 --> 00:18:06,376
And, we need to keep
those donations coming in.

402
00:18:07,156 --> 00:18:09,713
I know we promised
a special event coming up and...

403
00:18:11,549 --> 00:18:13,099
Should I come up now?

404
00:18:15,094 --> 00:18:17,265
What we're gonna do
is we're gonna pull our pants down.

405
00:18:17,390 --> 00:18:20,837
Everyone's gonna pull their pants down
for diabetes, okay? On 1, 2.

406
00:18:29,683 --> 00:18:32,527
Are you sure you don't want me to drive?
I'm good to drive.

407
00:18:32,864 --> 00:18:34,320
You want to play video games?

408
00:18:35,187 --> 00:18:38,482
Pull over, man. That place
has good chicken. I'm hungry.

409
00:18:41,579 --> 00:18:44,706
- You love flashing your ass, don't you?
- When it's for a good cause, Joan.

410
00:18:44,874 --> 00:18:47,333
- What's up?
- Look who's here.

411
00:18:47,653 --> 00:18:50,753
Wake up, Pawnee,
Tommy Timberlake's in the house.

412
00:18:50,878 --> 00:18:52,960
And you know who else is with me?

413
00:18:56,327 --> 00:18:58,495
Tell them what they won, son!

414
00:18:59,965 --> 00:19:02,147
Well, on behalf of the Detlef
Schrempf foundation,

415
00:19:02,767 --> 00:19:06,311
I'd like to present this check
for $5,000 for diabetes research.

416
00:19:09,887 --> 00:19:11,387
Thank you very much.

417
00:19:12,792 --> 00:19:14,242
Big man, big check.

418
00:19:14,517 --> 00:19:15,403
By the way,

419
00:19:16,458 --> 00:19:18,281
I'm drunk!

420
00:19:20,779 --> 00:19:22,702
- I don't understand what happened.
- I'm sorry.

421
00:19:22,870 --> 00:19:25,405
I think I was a little tired
when I told you that you should do that.

422
00:19:25,530 --> 00:19:27,665
I don't think
that that's something Ann wants.

423
00:19:28,249 --> 00:19:29,249
Not on TV.

424
00:19:29,713 --> 00:19:32,313
So that's why
you pull your pants down.

425
00:19:33,507 --> 00:19:35,007
Wait. What did I do?

426
00:19:35,668 --> 00:19:39,649
It's 6:04 am and I'm Perd Hapley
of channel four eyewitness news.

427
00:19:39,948 --> 00:19:41,926
And the story of this next dance

428
00:19:42,051 --> 00:19:43,598
is that it's called "the worm".

429
00:19:51,239 --> 00:19:53,525
- Good morning, Knope.
- Morning, Ron.

430
00:19:53,814 --> 00:19:55,235
Everything running smoothly?

431
00:19:55,930 --> 00:19:59,113
You know, I'm happy to go back out there
and demonstrate more of my skills.

432
00:19:59,601 --> 00:20:02,951
How to start a fire without matches.
How to build a cribbage board.

433
00:20:03,158 --> 00:20:04,410
I'm okay. Thanks.

434
00:20:05,296 --> 00:20:07,746
- Get some sleep.
- Aye, aye, captain.

435
00:20:12,128 --> 00:20:15,046
What the *** are you doing,
Perd Hapley?

436
00:20:19,008 --> 00:20:21,553
Leslie, what are you doing here?
Why aren't you sleeping?

437
00:20:21,721 --> 00:20:24,126
I just... I thought
maybe you'd want to talk more

438
00:20:24,251 --> 00:20:26,349
about all the crazy stuff going on.

439
00:20:26,517 --> 00:20:28,448
I do. I really do.
Thanks.

440
00:20:29,937 --> 00:20:31,865
I just got to tell you
I'm a little tired.

441
00:20:31,990 --> 00:20:34,440
So I may have parked
on your front lawn.

442
00:20:36,167 --> 00:20:37,360
- You did.
- I did.

443
00:20:41,007 --> 00:20:42,849
Just come in before anybody...

444
00:20:44,910 --> 00:20:47,954
Leslie and I had an amazing talk.
It was so great to come over here.

445
00:20:48,560 --> 00:20:51,081
Even though she was exhausted
beyond belief.

446
00:20:51,206 --> 00:20:54,369
Anyway, after we talked,
she fell asleep on the couch.

447
00:20:55,736 --> 00:20:58,952
And she's been asleep for...

448
00:20:59,849 --> 00:21:01,176
22 hours.

449
00:21:02,040 --> 00:21:04,750
It's amazing what she slept through.

450
00:21:05,676 --> 00:21:07,406
At one point,
I thought she was up...

451
00:21:07,531 --> 00:21:08,881
Hold on. Hi.

452
00:21:10,755 --> 00:21:12,979
But then she went right back
to sleep.

453
00:21:14,207 --> 00:21:17,025
I've been monitoring her vital signs.
She's totally fine.

454
00:21:17,265 --> 00:21:18,715
I love her so much.

455
00:21:20,260 --> 00:21:22,906
But I think I'm gonna draw
a mustache on her face.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
