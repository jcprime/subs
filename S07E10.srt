﻿1
00:00:04,081 --> 00:00:07,750
It's <i>The Johnny Karate Super</i>
<i>Awesome Musical Explosion Show.</i>

2
00:00:09,065 --> 00:00:11,499
Now, to introduce your host,
Johnny Karate,

3
00:00:11,534 --> 00:00:15,170
here is me, Perd Hapley!

4
00:00:16,472 --> 00:00:18,073
Silence!

5
00:00:18,107 --> 00:00:20,842
Welcome to the only show
that's all about learning,

6
00:00:20,877 --> 00:00:23,445
music, animals, fireworks,
water skis,

7
00:00:23,479 --> 00:00:26,648
and above all, ice cream, pizza,

8
00:00:26,682 --> 00:00:28,416
ninjas, getting stronger,

9
00:00:28,451 --> 00:00:30,785
sharks versus bears,
and above all...

10
00:00:30,820 --> 00:00:32,988
- karate!
- Hiya!

11
00:00:33,022 --> 00:00:35,924
Now, why don't you kids
help me sing <i>The Welcome Song!</i>

12
00:00:35,958 --> 00:00:37,592
Let me just grab my guitar.

13
00:00:39,395 --> 00:00:41,229
Someone has stolen my guitar.

14
00:00:42,765 --> 00:00:46,601
This seems like a case
for Special Agent...

15
00:00:46,636 --> 00:00:47,569
Burt Macklin.

16
00:00:47,603 --> 00:00:53,308
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

17
00:00:53,342 --> 00:00:55,744
I don't give a crap, Batman.
You work for me.

18
00:00:55,778 --> 00:00:59,448
Increase the perimeter!

19
00:00:59,482 --> 00:01:01,650
Macklin, the President's
called six times.

20
00:01:01,684 --> 00:01:03,084
If we don't get
that guitar back,

21
00:01:03,119 --> 00:01:04,719
the peace in Iraq
will be canceled.

22
00:01:04,754 --> 00:01:06,354
Get off my back, Chief!

23
00:01:06,389 --> 00:01:07,756
I'm doing the best I can.

24
00:01:07,790 --> 00:01:09,991
I don't play by the rules.

25
00:01:10,026 --> 00:01:11,593
But I get results.

26
00:01:11,627 --> 00:01:13,195
Dang it!
You're right again, Macklin.

27
00:01:13,229 --> 00:01:14,462
And I'm sorry.

28
00:01:14,497 --> 00:01:15,897
You're the best agent I've seen.

29
00:01:15,932 --> 00:01:18,300
And I've worked with James Bond.

30
00:01:18,334 --> 00:01:19,401
- This clue may help.
- Mm.

31
00:01:19,435 --> 00:01:21,536
It was left at the crime scene.

32
00:01:21,571 --> 00:01:23,471
You're dismissed.

33
00:01:23,506 --> 00:01:27,776
This case just keeps getting
deeper and deeper.

34
00:01:27,810 --> 00:01:29,911
I'm drowning
in the adult end of the pool,

35
00:01:29,946 --> 00:01:32,581
and the lifeguard's off
in the bathroom pooping.

36
00:01:35,551 --> 00:01:37,285
That's good writing.

37
00:01:37,320 --> 00:01:39,321
Johnny Karate's guitar
has been stolen.

38
00:01:39,355 --> 00:01:42,224
That instrument is worth
literally $900 million.

39
00:01:42,258 --> 00:01:44,793
But more importantly, he can't
play<i> The Good-bye Song</i>

40
00:01:44,827 --> 00:01:45,760
without it.

41
00:01:45,795 --> 00:01:47,329
So, by the time this show
is over,

42
00:01:47,363 --> 00:01:49,531
I will find who stole it,
and they will pay

43
00:01:49,565 --> 00:01:51,032
the ultimate price.

44
00:01:51,067 --> 00:01:54,870
A one-way ticket
to the Funky Monkey Dunk Tank!

45
00:01:54,904 --> 00:01:56,338
All that and more,

46
00:01:56,372 --> 00:02:01,409
on<i> The Johnny Karate Super</i>
<i>Awesome Musical Explosion Show!</i>

47
00:02:01,444 --> 00:02:04,713
♪ It's time to punch boredom
in its stupid face ♪

48
00:02:04,747 --> 00:02:07,015
♪ Jump, kick, punch, and jump ♪

49
00:02:07,049 --> 00:02:10,151
♪ And drop-kick sadness
into outer space ♪

50
00:02:10,186 --> 00:02:12,420
♪ Punch, kick till you drop ♪

51
00:02:12,455 --> 00:02:15,590
♪ It's the Johnny Karate
Super Awesome ♪

52
00:02:15,625 --> 00:02:19,928
♪ Musical Explosion Show ♪

53
00:02:19,962 --> 00:02:21,563
So, karate masters, you are here

54
00:02:21,597 --> 00:02:24,466
for an extra-awesome
and super-special episode

55
00:02:24,500 --> 00:02:25,767
of<i> Johnny Karate.</i>

56
00:02:27,570 --> 00:02:30,005
And that is because
it's my final show.

57
00:02:30,039 --> 00:02:31,873
Aw.

58
00:02:31,908 --> 00:02:33,308
Don't look glum.

59
00:02:33,342 --> 00:02:36,244
In a couple of weeks, I'll be
moving to Washington, D.C.

60
00:02:36,279 --> 00:02:39,681
It's the capital
of the entire world.

61
00:02:39,715 --> 00:02:42,851
They have things there
like this white house

62
00:02:42,885 --> 00:02:46,188
and this mental institution

63
00:02:46,222 --> 00:02:48,824
and my very favorite,
the Lincoln Memorial.

64
00:02:48,858 --> 00:02:51,993
It's this crazy statue
of this giant monster

65
00:02:52,028 --> 00:02:53,161
sitting on a chair
that represents

66
00:02:53,196 --> 00:02:54,596
all of America's enemies.

67
00:02:54,630 --> 00:02:55,897
I'll be moving
to Washington, D.C.,

68
00:02:55,932 --> 00:02:57,966
because my wife,
April Ludgate-Karate-Dwyer,

69
00:02:58,000 --> 00:02:59,501
has accepted
an amazing job there.

70
00:02:59,535 --> 00:03:01,937
I'm very proud of her
and totally in love.

71
00:03:01,971 --> 00:03:04,773
April, why don't you come on out
and talk to the children?

72
00:03:04,807 --> 00:03:06,174
Take a bow for being so cool.

73
00:03:06,209 --> 00:03:08,477
Hey, Johnny!

74
00:03:08,511 --> 00:03:10,946
So, since it's your final show,
I wanted to do

75
00:03:10,980 --> 00:03:14,349
one last extra-special
<i>April's Animal Corner.</i>

76
00:03:15,651 --> 00:03:17,152
That's right.

77
00:03:17,186 --> 00:03:19,955
This week
I brought one of the scariest,

78
00:03:19,989 --> 00:03:22,624
weirdest animals
in the whole world--

79
00:03:22,658 --> 00:03:25,427
a goliath
bird-eating tarantula.

80
00:03:25,461 --> 00:03:27,229
Wow!

81
00:03:27,263 --> 00:03:28,764
Ah!

82
00:03:28,798 --> 00:03:31,233
Now, the name, "goliath"
means "giant."

83
00:03:31,267 --> 00:03:34,503
And you can see that--

84
00:03:34,537 --> 00:03:36,638
- Andy, where is it?
- What?

85
00:03:36,672 --> 00:03:38,173
- Uh-oh.
- Where is it?

86
00:03:38,207 --> 00:03:39,441
Don't look at me.
I don't know what happened.

87
00:03:39,475 --> 00:03:41,243
I took it out to play
hide-and-seek.

88
00:03:41,277 --> 00:03:42,477
I couldn't find it,
and then--

89
00:03:42,512 --> 00:03:43,845
Oh, that's what happened.

90
00:03:43,880 --> 00:03:46,681
Okay, well, once again,
<i>April's Animal Corner</i>

91
00:03:46,716 --> 00:03:49,651
has turned into one
of our favorite segments--

92
00:03:49,685 --> 00:03:51,486
<i>Loose Animal in the Studio.</i>

93
00:03:51,521 --> 00:03:52,754
Yay!

94
00:03:52,789 --> 00:03:54,756
And as always,
when we accidentally end up

95
00:03:54,791 --> 00:03:56,324
doing
<i>Loose Animal in the Studio,</i>

96
00:03:56,359 --> 00:03:58,927
we have to show you
this disclaimer.

97
00:03:58,961 --> 00:04:00,629
Boring!

98
00:04:00,663 --> 00:04:02,564
Here's a disclaimer,
I didn't read this disclaimer.

99
00:04:04,133 --> 00:04:06,234
Ooh, good point.

100
00:04:06,269 --> 00:04:08,069
Okay. Done.

101
00:04:08,104 --> 00:04:10,172
Okay, kids,
so be on the lookout for a--

102
00:04:10,206 --> 00:04:11,339
What's it called?

103
00:04:11,374 --> 00:04:13,875
A goliath bird-eating
tarantula,

104
00:04:13,910 --> 00:04:17,379
known locally to Venezuelans
as "the devil's fist."

105
00:04:17,413 --> 00:04:20,182
Bye, kids!

106
00:04:20,216 --> 00:04:22,384
Okay, kids, well,
we've got a lot of work to do,

107
00:04:22,418 --> 00:04:25,253
performing the Five Karate
Moves to Success.

108
00:04:25,288 --> 00:04:27,022
Sing it with me!

109
00:04:27,056 --> 00:04:29,357
♪ Today we're gonna make
something ♪

110
00:04:29,392 --> 00:04:30,959
♪ Learn something ♪

111
00:04:30,993 --> 00:04:33,929
♪ Karate chop something ♪

112
00:04:33,963 --> 00:04:35,664
♪ And then we'll try
something new ♪

113
00:04:35,698 --> 00:04:37,265
♪ Even if it's scary to you ♪

114
00:04:37,300 --> 00:04:39,134
♪ And finally
we'll have some fun ♪

115
00:04:39,168 --> 00:04:40,469
♪ Being nice to someone ♪

116
00:04:40,503 --> 00:04:46,274
♪ Because that's
the Johnny Karate way ♪

117
00:04:46,309 --> 00:04:48,477
Step one--
let's go make something.

118
00:04:53,316 --> 00:04:54,749
Hello, Carpenter Ron.

119
00:04:54,784 --> 00:04:55,984
How are you doing?

120
00:04:56,018 --> 00:04:57,486
My contract is very specific.

121
00:04:57,520 --> 00:04:59,855
I do not have to answer
that question.

122
00:04:59,889 --> 00:05:02,390
This week I am making
a shadowbox frame,

123
00:05:02,425 --> 00:05:06,027
which can be used to display
an object of great value.

124
00:05:06,062 --> 00:05:08,897
This one is constructed
with American cherry wood.

125
00:05:08,931 --> 00:05:11,666
Cherry, huh?
Now, I have just one question--

126
00:05:11,701 --> 00:05:13,401
No, the wood does not taste
like cherry.

127
00:05:13,436 --> 00:05:14,536
You cannot eat it.

128
00:05:14,570 --> 00:05:16,505
I was not going to eat it,
Carpenter Ron.

129
00:05:16,539 --> 00:05:18,573
I was going to lick it.

130
00:05:18,608 --> 00:05:20,876
Always remember, kids,
when you find something new,

131
00:05:20,910 --> 00:05:23,445
you must lick it
before you eat it.

132
00:05:23,479 --> 00:05:25,040
That is incorrect
in a number of ways.

133
00:05:26,382 --> 00:05:29,184
I sure have had some fun
making things with you, son.

134
00:05:29,218 --> 00:05:30,819
Thank you
for everything you've done

135
00:05:30,853 --> 00:05:32,220
for the children of this area.

136
00:05:32,255 --> 00:05:33,522
You're welcome, Carpenter Ron.

137
00:05:33,556 --> 00:05:36,625
Hey, kids, this, to me,
seems like a...

138
00:05:36,659 --> 00:05:38,326
♪ Hug moment ♪

139
00:05:38,361 --> 00:05:41,897
You are mistaken.
Remove the graphic.

140
00:05:41,931 --> 00:05:44,966
Well, kids, it's time for our
next "Karate Move to Success."

141
00:05:45,001 --> 00:05:46,501
Let's learn something.

142
00:05:51,107 --> 00:05:53,275
Hello, Professor Smartbrain.

143
00:05:53,309 --> 00:05:54,576
Hi, Johnny.

144
00:05:54,610 --> 00:05:56,044
Listen, I don't know
if you got my email,

145
00:05:56,078 --> 00:05:58,080
but I thought that
since this is our final show,

146
00:05:58,114 --> 00:05:59,614
maybe we not use the--

147
00:06:00,917 --> 00:06:01,850
Boring buzzer.

148
00:06:05,221 --> 00:06:08,290
Okay, kids, today we're gonna
learn about geography.

149
00:06:08,324 --> 00:06:11,426
Now, you, Johnny, are moving
to Washington, D.C.

150
00:06:11,461 --> 00:06:13,862
I thought what we could talk
about is how fast

151
00:06:13,896 --> 00:06:19,134
you could get there using
an airplane, a train, or a car.

152
00:06:19,168 --> 00:06:22,838
Now, as you can see,
the airplane is the fastest.

153
00:06:22,872 --> 00:06:25,207
Not the fastest.
What about teleporting?

154
00:06:25,241 --> 00:06:27,943
Well, sure,
that would be really fun,

155
00:06:27,977 --> 00:06:29,578
but it's impossible.

156
00:06:29,612 --> 00:06:32,681
Nothing is impossible, kids.

157
00:06:32,715 --> 00:06:35,150
Teleportation is impossible.

158
00:06:35,184 --> 00:06:36,318
It's about theoretical physics.

159
00:06:38,221 --> 00:06:40,222
You know, you asked me
to do this. This was a favor--

160
00:06:44,127 --> 00:06:45,861
Morning, Johnny.
Mail call.

161
00:06:48,865 --> 00:06:50,131
Ya!

162
00:06:50,166 --> 00:06:51,533
Hi, Mailman Barry.

163
00:06:51,567 --> 00:06:52,734
Who brought me mail today?

164
00:06:52,769 --> 00:06:55,704
Well, Johnny,
you got over 500 letters

165
00:06:55,738 --> 00:06:58,039
from kids who love you, asking
for you and April to stay.

166
00:06:58,074 --> 00:07:00,776
Aw, babe, did you hear that?

167
00:07:00,810 --> 00:07:03,478
Now, Johnny, you also got
one very special letter.

168
00:07:03,513 --> 00:07:04,846
It's from me.

169
00:07:04,881 --> 00:07:06,915
And on the last day
of your show,

170
00:07:06,949 --> 00:07:08,083
I thought I could read it
to you.

171
00:07:08,117 --> 00:07:10,152
Aw.

172
00:07:10,186 --> 00:07:14,055
"Dearest Andy, I have never
had a son of my own,

173
00:07:14,090 --> 00:07:16,258
and I just want you to know
that for the past ten years"--

174
00:07:16,292 --> 00:07:18,126
It's time to karate chop something!

175
00:07:22,699 --> 00:07:24,866
Ninjas, attack!

176
00:07:24,901 --> 00:07:26,902
Oh, jeez.

177
00:07:30,173 --> 00:07:32,040
Disperse!

178
00:07:32,075 --> 00:07:34,409
Remember, never attack
a real postal employee.

179
00:07:34,444 --> 00:07:35,911
We're allowed
to attack Mailman Barry

180
00:07:35,945 --> 00:07:38,413
because he volunteered to
help us with our karate moves.

181
00:07:38,448 --> 00:07:40,182
I kind of thought
I'd be holding a piece of wood

182
00:07:40,216 --> 00:07:42,184
or something, but, you know...

183
00:07:42,218 --> 00:07:43,451
always happy to help.

184
00:07:43,486 --> 00:07:45,126
Next up, we're going
to try something new,

185
00:07:45,154 --> 00:07:46,434
with my good friend
Leslie Knope.

186
00:07:50,893 --> 00:07:52,661
Hey, Johnny Karate,
I'm so happy to be here

187
00:07:52,695 --> 00:07:54,596
on your last show ever.

188
00:07:54,630 --> 00:07:55,897
Hi, Leslie.
I'm glad you're here

189
00:07:55,932 --> 00:07:58,266
to teach us all how to be brave.

190
00:07:58,301 --> 00:08:00,402
So, Leslie, what are you going
to be trying new this week?

191
00:08:00,436 --> 00:08:03,972
I am going to be taking over
hosting my friend's TV show.

192
00:08:04,006 --> 00:08:04,973
Huh?

193
00:08:05,007 --> 00:08:06,475
Isn't that right, April?

194
00:08:06,509 --> 00:08:08,276
That is right.

195
00:08:08,311 --> 00:08:10,445
Johnny, you have made
so many kids

196
00:08:10,480 --> 00:08:13,248
and so many parents so happy,
that we wanted to give you

197
00:08:13,282 --> 00:08:14,616
a proper send-off.

198
00:08:14,650 --> 00:08:17,352
Andy Dwyer/Johnny Karate,

199
00:08:17,387 --> 00:08:21,289
this is your life.

200
00:08:21,324 --> 00:08:25,594
Oh, my gosh, Leslie,
this is amazing.

201
00:08:25,628 --> 00:08:27,863
I don't know what to say...

202
00:08:27,897 --> 00:08:29,765
literally, because normally
I have a script

203
00:08:29,799 --> 00:08:32,401
that I have to stick to, but...

204
00:08:32,435 --> 00:08:35,570
I don't know what I'm supposed
to say right now.

205
00:08:35,605 --> 00:08:36,805
It's kind of a nightmare.

206
00:08:36,839 --> 00:08:38,940
Okay, you leave that
to us, Johnny.

207
00:08:38,975 --> 00:08:41,009
Uh, when we come back,
we will take a look

208
00:08:41,044 --> 00:08:42,978
at the journey of Andy Dwyer.

209
00:08:43,012 --> 00:08:44,413
Stay tuned.

210
00:08:44,447 --> 00:08:47,282
"Now listen to some words
from our sponsors,"

211
00:08:47,316 --> 00:08:51,286
are the words I'm saying
right now.

212
00:08:51,320 --> 00:08:53,789
Hire Very Good Building Company

213
00:08:53,823 --> 00:08:55,223
for your construction needs.

214
00:08:55,258 --> 00:08:56,224
Or do not.

215
00:08:56,259 --> 00:08:58,393
I am not a beggar.

216
00:09:13,242 --> 00:09:14,643
End of commercial.

217
00:09:17,826 --> 00:09:19,858
The Wamapoke people
have a saying--

218
00:09:19,893 --> 00:09:22,461
"Only one who listens
hears the cry of the wolf."

219
00:09:23,897 --> 00:09:27,432
That advice has been passed down
from generation to generation.

220
00:09:27,467 --> 00:09:30,235
Today I give that advice to you.

221
00:09:30,270 --> 00:09:33,372
Listen for Coinsy the Wolf
at Wamapoke Casino.

222
00:09:33,406 --> 00:09:36,675
Twice a day, one lucky visitor
will hit the mega-wolf jackpot.

223
00:09:36,709 --> 00:09:39,077
And Coinsy will howl for you!

224
00:09:51,691 --> 00:09:53,258
The thing we return to now

225
00:09:53,293 --> 00:09:56,361
is my<i> Johnny Karate Show</i>
return message.

226
00:09:57,630 --> 00:09:59,331
Welcome back.
When I first met the man

227
00:09:59,365 --> 00:10:01,767
standing next to me, he was not
a cultural phenomenon.

228
00:10:01,801 --> 00:10:05,604
"Phenomenon" means
"to explore a cave."

229
00:10:05,638 --> 00:10:07,172
He was going through
a tough time.

230
00:10:07,207 --> 00:10:10,108
He had just broken both legs
from falling into a pit.

231
00:10:10,143 --> 00:10:11,443
I was super wasted.

232
00:10:11,478 --> 00:10:12,878
- It's a kids' show, Andy.
- Yes, good call.

233
00:10:12,912 --> 00:10:14,279
- Kids' show.
- Good call.

234
00:10:14,314 --> 00:10:18,150
That event set in motion many
wonderful things in my life,

235
00:10:18,184 --> 00:10:19,785
including the building
of the Pawnee Commons.

236
00:10:19,819 --> 00:10:24,189
So, in a weird way, Andy,
I owe it all to you.

237
00:10:24,224 --> 00:10:25,224
And now that the show is ending,

238
00:10:25,258 --> 00:10:26,658
I'm gonna have to start
all over again.

239
00:10:26,693 --> 00:10:28,827
Hopefully there's a pit
in Washington, D.C.,

240
00:10:28,862 --> 00:10:30,062
that I could fall into.

241
00:10:30,096 --> 00:10:32,798
Andy pulled his life together,
and he got himself a job

242
00:10:32,832 --> 00:10:35,134
at the shoe-shine stand
in City Hall.

243
00:10:35,168 --> 00:10:37,703
He then became
a bureaucratic assistant.

244
00:10:37,737 --> 00:10:39,004
- Hi, honey.
- Hey. Hi.

245
00:10:39,038 --> 00:10:40,939
He was always enthusiastic.

246
00:10:40,974 --> 00:10:42,674
But he also kept track
of his appointments

247
00:10:42,709 --> 00:10:44,710
by writing them on his arm.

248
00:10:44,744 --> 00:10:46,145
Still do.

249
00:10:46,179 --> 00:10:47,446
To commemorate that time
and to make sure

250
00:10:47,480 --> 00:10:49,515
that you never miss
another appointment again,

251
00:10:49,549 --> 00:10:53,318
we have brought you
a monogrammed day planner.

252
00:10:54,521 --> 00:10:55,554
Guys...

253
00:10:55,588 --> 00:10:57,156
So sweet.

254
00:10:57,190 --> 00:10:58,157
Thank you.

255
00:10:58,191 --> 00:10:59,892
Aw.

256
00:10:59,926 --> 00:11:01,660
Oh, no.
I lost it.

257
00:11:01,694 --> 00:11:02,995
The day planner's gone.
I can't find it anywhere.

258
00:11:03,029 --> 00:11:06,131
What?

259
00:11:06,166 --> 00:11:07,866
Fitting, really.

260
00:11:07,901 --> 00:11:10,736
From then, Andy went
to England for a few months

261
00:11:10,770 --> 00:11:12,371
to work for a nonprofit.

262
00:11:12,405 --> 00:11:13,906
It was there that he befriended

263
00:11:13,940 --> 00:11:16,708
the 14th Earl
of Cornwall-upon-Thames,

264
00:11:16,743 --> 00:11:17,709
Sir Edgar Covington.

265
00:11:17,744 --> 00:11:19,411
Ta-da!

266
00:11:19,446 --> 00:11:21,580
Oh, my God, Eddie,
this is amazing!

267
00:11:24,083 --> 00:11:27,619
Andy, I am here in this weird,
flat place,

268
00:11:27,654 --> 00:11:32,057
to name you a loyal and true
karate defender

269
00:11:32,092 --> 00:11:33,058
of the Queen's Realm.

270
00:11:36,463 --> 00:11:38,764
Please get down on one knee.

271
00:11:38,798 --> 00:11:42,201
This honor is bestowed on those
who have shown great achievement

272
00:11:42,235 --> 00:11:46,205
in fields of the arts, learning,
literature, and science.

273
00:11:46,239 --> 00:11:49,308
Therefore, I hereby dub thee

274
00:11:49,342 --> 00:11:52,811
a knight of the Order
of the British Empire.

275
00:11:52,846 --> 00:11:55,280
Hang on,
can you actually do this?

276
00:11:55,315 --> 00:11:57,282
Well, technically, only
the Queen can knight somebody,

277
00:11:57,317 --> 00:12:00,219
but I'm so rich that she lets me
do what I want.

278
00:12:00,253 --> 00:12:01,787
Would you like to be knighted?

279
00:12:01,821 --> 00:12:03,222
Oh, no, no, no, no.

280
00:12:03,256 --> 00:12:05,190
No, of course not.
It's Andy's day.

281
00:12:05,225 --> 00:12:06,225
Dude, I don't mind.

282
00:12:06,259 --> 00:12:07,539
Oh, then, yes,
knight me, please.

283
00:12:07,560 --> 00:12:09,628
Oh, my God, this is amazing!

284
00:12:09,662 --> 00:12:12,498
Winter is coming
for Sir Ben Lightstorm.

285
00:12:12,532 --> 00:12:13,799
Okay, you're both knights.

286
00:12:13,833 --> 00:12:14,800
Cool.

287
00:12:14,834 --> 00:12:17,236
Guys, this was so much fun.

288
00:12:17,270 --> 00:12:20,672
Friend, coworker, musician,
husband, Andy Dwyer--

289
00:12:20,707 --> 00:12:21,874
Sir Andy Dwyer.

290
00:12:21,908 --> 00:12:23,776
Sir Andy Dwyer had become
so many things

291
00:12:23,810 --> 00:12:24,877
to so many people.

292
00:12:24,911 --> 00:12:26,912
But his greatest creation
was yet to come.

293
00:12:26,946 --> 00:12:30,015
A salute to Johnny Karate
when we come back.

294
00:12:33,084 --> 00:12:34,285
We know what you want--

295
00:12:34,319 --> 00:12:36,720
healthy, natural food
that still tastes great.

296
00:12:36,755 --> 00:12:37,822
And we're here to tell you...

297
00:12:38,857 --> 00:12:41,459
It doesn't exist.

298
00:12:41,493 --> 00:12:43,093
Healthy food is for suckers.

299
00:12:43,128 --> 00:12:44,528
It tastes like garbage,

300
00:12:44,563 --> 00:12:47,031
and if you say you like it,
you're a chump and a liar.

301
00:12:47,065 --> 00:12:49,366
Be honest.
This is what you want to eat...

302
00:12:52,170 --> 00:12:53,637
It tastes amazing.

303
00:12:53,672 --> 00:12:55,372
What's in it?
Who cares?

304
00:12:55,407 --> 00:12:56,964
How many calories?
Shut up.

305
00:12:57,084 --> 00:12:58,943
It's awesome.

306
00:13:05,172 --> 00:13:07,006
One of Andy's
greatest achievements

307
00:13:07,041 --> 00:13:09,082
was the success of his band,
Mouse Rat.

308
00:13:09,202 --> 00:13:11,025
No one was doing
what we were doing.

309
00:13:11,060 --> 00:13:12,160
Well, that's not really true.

310
00:13:12,194 --> 00:13:13,395
I mean, we were basically
playing covers

311
00:13:13,429 --> 00:13:14,829
of Dave Matthews songs.

312
00:13:14,864 --> 00:13:17,198
So I guess you could say
Dave Matthews

313
00:13:17,233 --> 00:13:18,767
was doing what we were doing.

314
00:13:18,801 --> 00:13:21,169
Oh, to play in a band
like Mouse Rat

315
00:13:21,203 --> 00:13:23,838
that brought such joy to so many

316
00:13:23,873 --> 00:13:26,508
truly was a mitzvah.

317
00:13:26,542 --> 00:13:28,710
I should mention
I'm a rabbi now.

318
00:13:28,744 --> 00:13:30,412
For the full rockumentary, go to

319
00:13:30,446 --> 00:13:33,782
TheJohnnyKarateSuperAwesome
MusicalExplosion.com.

320
00:13:33,816 --> 00:13:35,583
And now, back to the show.

321
00:13:39,555 --> 00:13:41,689
Every hero struggles
with failure,

322
00:13:41,724 --> 00:13:43,958
faces something they cannot do.

323
00:13:43,993 --> 00:13:46,227
Andy faced one of those moments
just a few years ago.

324
00:13:46,262 --> 00:13:48,530
And I would've chugged
that entire gallon of milk

325
00:13:48,564 --> 00:13:50,465
had I not started puking
out of nowhere.

326
00:13:50,499 --> 00:13:52,133
No, you did not achieve
your dream

327
00:13:52,168 --> 00:13:54,269
of becoming
a Pawnee police officer.

328
00:13:54,303 --> 00:13:55,303
Oh, right.

329
00:13:55,337 --> 00:13:56,705
But you did create Burt Macklin,

330
00:13:56,739 --> 00:13:59,307
a character who exemplifies
everything that is great

331
00:13:59,341 --> 00:14:00,308
about law enforcement.

332
00:14:00,342 --> 00:14:01,843
Take a look.

333
00:14:01,877 --> 00:14:06,214
My name is rt Tyra.

334
00:14:12,722 --> 00:14:14,289
I don't know
what the problem is, Sergeant.

335
00:14:14,323 --> 00:14:16,391
Just drain the ocean.

336
00:14:28,804 --> 00:14:31,339
Andy, on behalf of the entire
Pawnee Police Department,

337
00:14:31,373 --> 00:14:33,241
I'd like to present you
with this genuine

338
00:14:33,275 --> 00:14:34,809
- Pawnee police badge.
- Wow.

339
00:14:34,844 --> 00:14:37,011
It's ensconced forever
in the shadowbox

340
00:14:37,046 --> 00:14:38,646
- we made earlier.
- How about that?

341
00:14:38,681 --> 00:14:41,249
No.

342
00:14:41,283 --> 00:14:44,018
Son, we spent four days
making that.

343
00:14:44,053 --> 00:14:47,188
This means so, so much to me.

344
00:14:47,223 --> 00:14:48,323
I'm a cop.

345
00:14:48,357 --> 00:14:49,597
I should get some handcuffs too.

346
00:14:49,625 --> 00:14:51,026
- Give me your cuffs, Randy.
- No, no, no.

347
00:14:51,060 --> 00:14:52,260
- Give me your cuffs.
- It's an honorary title only.

348
00:14:52,294 --> 00:14:53,962
- Stand down, sir.
- Stop it! Quit!

349
00:14:53,996 --> 00:14:55,363
Andy, now that you're
an official police officer...

350
00:14:55,398 --> 00:14:56,965
He's not. I want to make
it clear--he's not.

351
00:14:56,999 --> 00:14:59,834
You should be able to solve
the case of the missing guitar.

352
00:14:59,869 --> 00:15:03,772
So grab your clue and head
on over to Professor Smartbrain.

353
00:15:04,940 --> 00:15:07,375
Professor Smartbrain,
this piece of fabric

354
00:15:07,410 --> 00:15:08,743
was found at the scene.

355
00:15:08,778 --> 00:15:10,312
Look at it under your telescope.

356
00:15:10,346 --> 00:15:11,412
Hmm.

357
00:15:11,447 --> 00:15:13,448
It looks like there are
some tiny initials.

358
00:15:13,482 --> 00:15:16,584
"J.C."?

359
00:15:16,619 --> 00:15:19,721
President Jimmy Carter
stole my guitar?

360
00:15:19,755 --> 00:15:20,922
But why?

361
00:15:20,956 --> 00:15:23,725
I don't think
it's Jimmy Carter, buddy.

362
00:15:23,759 --> 00:15:26,194
The champ is here!

363
00:15:28,030 --> 00:15:29,998
Hi, kids, is everybody
having a great time?

364
00:15:31,600 --> 00:15:33,034
I know I am!

365
00:15:33,068 --> 00:15:34,536
John Cena in the house!

366
00:15:34,570 --> 00:15:36,438
John Cena, you're my hero.

367
00:15:36,472 --> 00:15:38,773
- Wait, you're Andy.
- Yeah, I'm Andy.

368
00:15:38,808 --> 00:15:40,075
From the information I got,

369
00:15:40,109 --> 00:15:41,810
I just assumed you'd be, like,
ten years old.

370
00:15:41,844 --> 00:15:44,345
- Thank you.
- Okay, great. Thank you.

371
00:15:44,380 --> 00:15:46,948
Thank you so much, man,
for bringing martial arts

372
00:15:46,982 --> 00:15:49,117
and music to all the children--

373
00:15:49,151 --> 00:15:50,385
Wait, wait,
what are you doing, man?

374
00:15:50,419 --> 00:15:51,352
- Got you.
- Got me?

375
00:15:51,387 --> 00:15:52,487
- What?
- He got my cuffs.

376
00:15:52,521 --> 00:15:53,888
Wait.
What the hell is this?

377
00:15:53,923 --> 00:15:55,690
John Cena, you're a great
inspiration, a true hero,

378
00:15:55,725 --> 00:15:57,559
and this is the greatest moment
of my life.

379
00:15:57,593 --> 00:15:58,526
But you're going down.

380
00:15:58,561 --> 00:15:59,561
Going down?
What are you--

381
00:15:59,562 --> 00:16:00,562
Whoa!
Hey, stop--

382
00:16:00,596 --> 00:16:01,629
You stole Johnny Karate's guitar.

383
00:16:01,664 --> 00:16:02,944
I didn't steal his guitar, man.

384
00:16:02,965 --> 00:16:04,065
I'm just doing this as a favor.

385
00:16:04,100 --> 00:16:05,540
You're going
in the dunk tank, bud.

386
00:16:05,568 --> 00:16:06,601
I don't want to go
in the dunk tank.

387
00:16:06,635 --> 00:16:08,169
- Shut up.
- Is anybody in charge?

388
00:16:08,204 --> 00:16:09,738
A little help?

389
00:16:09,772 --> 00:16:12,540
And now it's time to celebrate
Andy Dwyer's greatest creation--

390
00:16:12,575 --> 00:16:13,508
Johnny Karate.

391
00:16:15,911 --> 00:16:18,880
He has brought so much happiness
to so many children

392
00:16:18,914 --> 00:16:22,317
in southern Indiana that we
thought a piece of Johnny Karate

393
00:16:22,351 --> 00:16:24,352
should stay here in Pawnee.

394
00:16:30,092 --> 00:16:34,963
♪ Everybody was
Kung Fu fighting ♪

395
00:16:34,997 --> 00:16:36,765
Raise it up.

396
00:16:36,799 --> 00:16:40,969
♪ Those kicks were fast
as lightning ♪

397
00:16:41,003 --> 00:16:43,405
This is awesome.
Can you believe it, babe?

398
00:16:43,439 --> 00:16:47,108
♪ In fact, it was
a little bit fritening ♪

399
00:16:47,143 --> 00:16:50,178
That karate gi
will stay here forever.

400
00:16:50,212 --> 00:16:52,747
Well, not<i> here</i> here,
because this studio

401
00:16:52,782 --> 00:16:54,783
actually has a show
where divorced couples

402
00:16:54,817 --> 00:16:56,217
work out their problems.

403
00:16:56,252 --> 00:16:57,686
So that might be weird if that
thing was hanging over them.

404
00:16:57,720 --> 00:16:58,853
But it'll be somewhere.

405
00:16:58,888 --> 00:17:03,758
And the spirit will stay
with us forever.

406
00:17:03,793 --> 00:17:06,561
Andy Dwyer's final good-bye
when we come back.

407
00:17:10,800 --> 00:17:12,100
What powers us?

408
00:17:12,134 --> 00:17:14,069
What gives us the tools
to attain our goals,

409
00:17:14,103 --> 00:17:17,572
face our fears, take
that next step into the unknown?

410
00:17:17,606 --> 00:17:19,340
It's energy.

411
00:17:19,375 --> 00:17:22,143
Verizon, Exxon, and Chipotle
are proud to announce

412
00:17:22,178 --> 00:17:25,513
a cross-platform merger
of our three great brands.

413
00:17:25,548 --> 00:17:27,916
Whether it's extending
your 6G coverage,

414
00:17:27,950 --> 00:17:29,818
drilling natural gas
out of shale,

415
00:17:29,852 --> 00:17:31,853
or perfecting
the chicken quesadilla,

416
00:17:31,887 --> 00:17:35,223
we will give you the energy
to get you to the next level.

417
00:17:35,257 --> 00:17:39,427
And with enough energy, America,
nothing can stop us.

418
00:17:39,462 --> 00:17:42,564
The all-new
Verizon-Chipotle-Exxon--

419
00:17:42,598 --> 00:17:45,808
proud to be one
of America's 8 companies.

420
00:17:52,614 --> 00:17:54,615
I'd like to begin by saying
thank you

421
00:17:54,649 --> 00:17:56,050
to anyone who has ever worked

422
00:17:56,084 --> 00:17:59,653
on the<i> Johnny Karate</i>
<i>Musical Explosion Show.</i>

423
00:17:59,688 --> 00:18:02,623
This has been the greatest job
I've ever had,

424
00:18:02,657 --> 00:18:05,659
and today has been
the best day of my life.

425
00:18:05,694 --> 00:18:07,795
And that's saying something.

426
00:18:07,829 --> 00:18:10,431
I once found a rock that looked
exactly like Santa Claus--

427
00:18:10,465 --> 00:18:13,033
hat and everything, so...

428
00:18:13,068 --> 00:18:15,469
Before I say good-bye,
let's review our checklist

429
00:18:15,504 --> 00:18:16,604
one last time.

430
00:18:16,638 --> 00:18:18,839
We made something
with Carpenter Ron.

431
00:18:18,874 --> 00:18:21,142
We learned something
with Professor Smartbrain.

432
00:18:21,176 --> 00:18:22,810
We karate chopped something--

433
00:18:22,844 --> 00:18:24,745
old Mailman Barry's face.

434
00:18:24,780 --> 00:18:27,681
And we tried something new,
even though it was scary to us,

435
00:18:27,716 --> 00:18:29,617
with Leslie Knope.

436
00:18:29,651 --> 00:18:32,153
That leaves just one more thing,

437
00:18:32,187 --> 00:18:34,822
the most important one.

438
00:18:34,856 --> 00:18:37,625
Be nice to someone.

439
00:18:37,659 --> 00:18:40,094
And I think I know right now
who needs it the most--

440
00:18:40,128 --> 00:18:44,899
my wife,
April Ludgate-Karate-Dwyer.

441
00:18:44,933 --> 00:18:47,001
Honey, come on out here.

442
00:18:47,035 --> 00:18:48,269
Come here.

443
00:18:48,303 --> 00:18:50,771
Babe.

444
00:18:50,806 --> 00:18:52,206
Hey, babe?

445
00:18:52,240 --> 00:18:55,042
Wait!

446
00:18:55,076 --> 00:18:56,644
Honey, where are you going?

447
00:18:56,678 --> 00:18:58,446
Babe, hey, will you--

448
00:18:58,480 --> 00:18:59,547
Hey, come here.

449
00:18:59,581 --> 00:19:02,616
April, what's going on?

450
00:19:02,651 --> 00:19:06,053
It's...

451
00:19:06,087 --> 00:19:09,323
Every week, this show
is an amazing train wreck,

452
00:19:09,357 --> 00:19:12,393
and you love doing it,
and you're the best host,

453
00:19:12,427 --> 00:19:14,762
like, even better
than my all-time favorite host,

454
00:19:14,796 --> 00:19:16,497
Tom Snyder, which is crazy.

455
00:19:16,531 --> 00:19:18,432
And all those kids love you,

456
00:19:18,467 --> 00:19:21,936
and you're doing
what you're meant to do.

457
00:19:21,970 --> 00:19:24,271
And I can't be the person that
makes you lose what you love.

458
00:19:24,306 --> 00:19:25,506
Yeah, but you're what I love.

459
00:19:25,540 --> 00:19:27,675
You're the only reason
I have any of this.

460
00:19:27,709 --> 00:19:30,544
You believed in me,
and you supported me.

461
00:19:30,579 --> 00:19:32,780
You make me happy...

462
00:19:32,814 --> 00:19:35,449
happier than I ever thought
I could be.

463
00:19:35,484 --> 00:19:38,319
Without you,
I wouldn't be anything.

464
00:19:38,353 --> 00:19:40,888
You're what keeps me going.

465
00:19:40,922 --> 00:19:44,225
You're my
Verizon-Chipotle-Exxon.

466
00:19:44,259 --> 00:19:47,361
As long as I'm with you,
I'm gonna be happy.

467
00:19:47,396 --> 00:19:49,830
So we go to Washington, D.C.

468
00:19:49,865 --> 00:19:51,065
And then we figure out

469
00:19:51,099 --> 00:19:54,268
the next cool and awesome thing
from there, okay?

470
00:19:56,905 --> 00:19:58,539
Okay.

471
00:19:58,573 --> 00:20:00,920
That was a very good
"Being Nice to Someone."

472
00:20:01,518 --> 00:20:02,679
I did it.

473
00:20:03,177 --> 00:20:04,177
I can go finish the show.

474
00:20:06,888 --> 00:20:10,923
♪ Well it's time for us to go ♪

475
00:20:11,284 --> 00:20:16,181
♪ But I want you all to know ♪

476
00:20:16,216 --> 00:20:18,383
♪ That karate's
not about fighting ♪

477
00:20:18,418 --> 00:20:20,619
♪ It's about knowing
who you are ♪

478
00:20:20,653 --> 00:20:22,821
♪ And being kind and honest ♪

479
00:20:22,856 --> 00:20:24,490
♪ While you're kicking
for the stars ♪

480
00:20:24,524 --> 00:20:30,496
♪ Yeah, that's
the Johnny Karate way ♪

481
00:20:33,800 --> 00:20:36,268
♪ Keep karate in your heart ♪

482
00:20:36,302 --> 00:20:39,071
♪ And aspire to your dreams ♪

483
00:20:39,105 --> 00:20:40,639
♪ And always remember ♪

484
00:20:40,673 --> 00:20:42,741
♪ You're forever on my team ♪

485
00:20:42,776 --> 00:20:48,647
♪ Yeah, that's
the Johnny Karate way ♪

486
00:20:48,681 --> 00:20:49,848
Karate yell!

487
00:20:49,883 --> 00:20:51,450
Hiya!

488
00:20:51,484 --> 00:20:53,685
Good-bye, Pawnee.
I will miss you.

489
00:20:55,789 --> 00:20:56,755
Guys?

490
00:20:56,789 --> 00:20:57,756
Guys?

491
00:20:57,791 --> 00:20:59,658
Guys!

492
00:20:59,692 --> 00:21:00,993
Tarantula.

493
00:21:03,062 --> 00:21:05,097
Could somebody get me
out of here, please?

494
00:21:05,131 --> 00:21:06,565
I'll help you, John Cena.

495
00:21:06,599 --> 00:21:08,300
No--

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
