1
00:00:01,457 --> 00:00:04,208
Okay, everybody,
time for the hummingbird lottery.

2
00:00:06,486 --> 00:00:09,062
You know how it works.
Write your name down on a paper.

3
00:00:09,187 --> 00:00:10,718
No, they don't win a hummingbird.

4
00:00:10,886 --> 00:00:13,512
I installed hummingbird feeders
in all of the parks.

5
00:00:13,680 --> 00:00:16,859
So the winner
gets to refill those feeders.

6
00:00:17,187 --> 00:00:18,184
Scientifically,

7
00:00:18,352 --> 00:00:20,652
hummingbirds
are the world's cutest animals.

8
00:00:21,119 --> 00:00:22,605
They're so small,

9
00:00:22,773 --> 00:00:24,021
they have tiny beaks.

10
00:00:24,146 --> 00:00:25,858
And they only eat sugar water.

11
00:00:26,190 --> 00:00:27,889
I mean, what beats that?

12
00:00:28,014 --> 00:00:31,091
Come on.
Baby monkeys in diapers?

13
00:00:32,562 --> 00:00:35,242
They do.
Baby monkeys in diapers are the cutest.

14
00:00:35,473 --> 00:00:37,787
- Okay, who's it gonna be?
- Not me. Not me.

15
00:00:40,720 --> 00:00:42,741
You can stop by tomorrow morning,
before work.

16
00:00:42,866 --> 00:00:44,335
This is my 3rd time in a row.

17
00:00:44,862 --> 00:00:48,047
It's just a bad luck streak. Next time,
I'm sure it'll definitely be one of us.

18
00:00:48,310 --> 00:00:51,709
But it won't be me
because I always write...

19
00:00:56,682 --> 00:00:58,362
No. I always write my own name.

20
00:00:59,028 --> 00:00:59,793
But,

21
00:01:00,176 --> 00:01:02,461
just to be safe, I do add

22
00:01:02,758 --> 00:01:04,318
20 extra Jerrys.

23
00:01:05,190 --> 00:01:06,816
.:: La Fabrique ::.

24
00:01:06,984 --> 00:01:08,618
- Feygnasse Team -

25
00:01:09,626 --> 00:01:11,441
Episode 219
<i>Park safety</i>

26
00:01:12,605 --> 00:01:13,860
mpm

27
00:01:14,397 --> 00:01:15,574
lestat

28
00:01:25,464 --> 00:01:27,013
Finally, we're going to kick off

29
00:01:27,138 --> 00:01:29,012
the children's concert series
this weekend

30
00:01:29,137 --> 00:01:31,298
with a performance
by Freddy Spaghetti.

31
00:01:31,538 --> 00:01:34,543
- I thought Freddy Spaghetti O.D.'d.
- No. That's Mr. Funny Noodle.

32
00:01:34,668 --> 00:01:36,429
He didn't O.D.
His drummer shot him.

33
00:01:37,221 --> 00:01:38,583
Where's Jerry, by the way?

34
00:01:38,708 --> 00:01:41,082
Why isn't he back? How long does it take
to fill bird feeders?

35
00:01:41,207 --> 00:01:44,270
Maybe he fell into the toilet.
Remember when he fell into the toilet?

36
00:01:45,579 --> 00:01:46,827
"Sorry, guys. I'm late.

37
00:01:46,952 --> 00:01:50,317
"I got confused and took a shower
after I got dressed 'cause I'm Jerry."

38
00:01:51,632 --> 00:01:55,240
David Meyers,
the Jewish guy who works at city hall,

39
00:01:55,575 --> 00:01:56,873
once told me something.

40
00:01:56,998 --> 00:01:59,431
A <i>schlemiel</i>, is the guy

41
00:01:59,953 --> 00:02:01,905
who spills soup at a fancy party.

42
00:02:02,030 --> 00:02:03,107
A <i>schlimazel</i>,

43
00:02:03,498 --> 00:02:05,026
is the guy he spills it on.

44
00:02:06,668 --> 00:02:10,357
Jerry is both the <i>schlemiel</i> and
the <i>schlimazel</i> of our office.

45
00:02:11,586 --> 00:02:14,633
"And then I put my underwear on my head
instead of my butt."

46
00:02:16,594 --> 00:02:18,394
Okay, guys, that's enough.

47
00:02:19,187 --> 00:02:20,723
Unless somebody has another one.

48
00:02:29,196 --> 00:02:31,984
Jerry, are you okay? Ann, is Jerry okay?
What's wrong with your arm?

49
00:02:32,152 --> 00:02:34,153
What's wrong with his arm.
Jerry, talk to me.

50
00:02:34,321 --> 00:02:36,685
- Ann, get Jerry to talk to me.
- He's okay.

51
00:02:36,810 --> 00:02:39,867
He's got a couple of scrapes
and a dislocated shoulder.

52
00:02:40,035 --> 00:02:41,035
What happened?

53
00:02:41,203 --> 00:02:43,412
- You guys are just gonna laugh.
- Why?

54
00:02:43,580 --> 00:02:46,290
Did you throw it out trying to swing
a honey pot off your hand?

55
00:02:46,458 --> 00:02:47,416
I was mugged.

56
00:02:48,705 --> 00:02:51,045
My God.
I'm so sorry, Jerry.

57
00:02:51,287 --> 00:02:53,714
I was on my way
to the hummingbird feeders,

58
00:02:53,882 --> 00:02:55,424
and I was walking Lord Sheldon.

59
00:02:55,686 --> 00:02:57,963
Is that code
for some kind of weird sex act?

60
00:02:58,136 --> 00:03:00,596
Lord Sheldon is my dog.
My wife named him.

61
00:03:02,099 --> 00:03:04,535
These kids came out of nowhere.
They pinned my arm back,

62
00:03:04,660 --> 00:03:06,761
grabbed my wallet,
and knocked me to the ground.

63
00:03:06,886 --> 00:03:09,315
How did you counterattack?
Fist to the throat?

64
00:03:09,440 --> 00:03:10,798
Hit in the beanbag?

65
00:03:10,923 --> 00:03:13,458
There's no shame
in attacking a criminal's beanbag.

66
00:03:13,583 --> 00:03:15,903
No, I just curled up
and laid still till they left.

67
00:03:17,160 --> 00:03:19,460
Well, that's another way to play it.

68
00:03:19,908 --> 00:03:22,285
Did any of them have weird tattoos
or scars or anything?

69
00:03:22,410 --> 00:03:25,120
If even one of them had a unique scar,
we got 'em.

70
00:03:25,619 --> 00:03:27,832
- I didn't get a good look.
- Damn it, Jerry.

71
00:03:28,914 --> 00:03:30,084
You're the victim.

72
00:03:30,252 --> 00:03:31,043
Sorry.

73
00:03:32,250 --> 00:03:34,057
I feel we're responsible for this.

74
00:03:34,182 --> 00:03:35,982
Why?
We didn't mug Jerry.

75
00:03:36,883 --> 00:03:38,336
Why was he in the park?

76
00:03:38,461 --> 00:03:40,344
'Cause we tricked him.
I don't see the link.

77
00:03:41,596 --> 00:03:43,341
This is on us. It's karma.

78
00:03:43,466 --> 00:03:45,387
Wouldn't it be karma
if we got mugged?

79
00:03:45,512 --> 00:03:48,644
That's how pathetic Jerry is.
He can't even get karma right.

80
00:03:49,859 --> 00:03:51,063
That's not funny.

81
00:03:54,186 --> 00:03:57,820
Our friend got mugged this morning.
And we will not let that happen in vain.

82
00:03:57,988 --> 00:03:59,405
He doesn't have a black eye.

83
00:03:59,573 --> 00:04:01,671
The whole department
has a black eye.

84
00:04:01,796 --> 00:04:03,421
This is our wakeup call, guys.

85
00:04:03,546 --> 00:04:05,327
Jerry's face
is the symbol of failure.

86
00:04:06,997 --> 00:04:08,873
Our failure.
To keep the parks safe.

87
00:04:09,099 --> 00:04:11,723
I have some folders.
Inside are some assignments.

88
00:04:11,848 --> 00:04:14,044
And some homemade taffy.

89
00:04:16,631 --> 00:04:18,647
April, check in
with our police liaison.

90
00:04:18,772 --> 00:04:20,426
Donna, go to Ramsett park.

91
00:04:20,769 --> 00:04:23,198
- Tom, you're with me.
- So when you say,

92
00:04:23,323 --> 00:04:26,640
check in with the police liaison,
you mean hook up with him, right?

93
00:04:27,810 --> 00:04:29,685
Just check in with him.
Everybody dismissed.

94
00:04:30,050 --> 00:04:31,600
Wait. I want to help.

95
00:04:32,757 --> 00:04:36,556
I'm gonna teach everybody self-defense,
so you can defend yourselves.

96
00:04:36,902 --> 00:04:39,488
We need it 'cause we certainly
are a bunch of weaklings.

97
00:04:39,613 --> 00:04:41,965
- Especially Tom.
- I am not a weakling.

98
00:04:42,090 --> 00:04:44,390
- Arm-wrestle me right now.
- Okay.

99
00:04:45,701 --> 00:04:47,856
I think I'm more
than holding my own here.///

100
00:04:56,274 --> 00:04:58,339
- Hey. How you doing?
- Not too bad.

101
00:04:58,666 --> 00:05:02,092
I was dropping off Jerry .
He was mugged in the park.

102
00:05:02,260 --> 00:05:03,725
No. Jerry?

103
00:05:04,257 --> 00:05:07,222
The black guy
with the looney tunes ties? I love him.

104
00:05:07,525 --> 00:05:09,099
Jerry who works with Leslie.

105
00:05:10,626 --> 00:05:12,476
That Jerry?
He got mugged?

106
00:05:15,234 --> 00:05:16,645
That's kind of a bummer too.

107
00:05:16,770 --> 00:05:18,681
While you here,
what do you think of this?

108
00:05:21,571 --> 00:05:22,905
Scrotation Marks.

109
00:05:23,073 --> 00:05:25,950
I don't know what you're talking about,
but my gut says no.

110
00:05:26,256 --> 00:05:27,368
New band name.

111
00:05:28,098 --> 00:05:31,369
'Cause, Mouse Rat, it's a great name,
but, at the same time,

112
00:05:31,889 --> 00:05:32,778
it sucks.

113
00:05:32,903 --> 00:05:34,708
So we have to change it
one more time.

114
00:05:35,001 --> 00:05:37,006
Dude, you've got to stop doing that.

115
00:05:37,131 --> 00:05:39,296
How are people gonna become fans
if they don't know

116
00:05:39,464 --> 00:05:41,377
the name of the band
they're listening to?

117
00:05:41,951 --> 00:05:42,967
Good point.

118
00:05:43,764 --> 00:05:45,700
I always had fun with Andy.

119
00:05:46,132 --> 00:05:49,254
But, when you're his girlfriend,
you're also his mother,

120
00:05:49,379 --> 00:05:51,313
and his maid, and his nurse.

121
00:05:52,062 --> 00:05:55,762
He's completely helpless.
He's like a baby in a straightjacket.

122
00:05:56,530 --> 00:05:58,686
Baby in a straightjacket.
That's a good band name.

123
00:05:59,048 --> 00:06:00,311
I should tell him that.

124
00:06:01,920 --> 00:06:05,030
- Really funny. Where's the real banner?
- We only had an hour.

125
00:06:05,198 --> 00:06:07,048
So did I.
Look what I did.

126
00:06:10,204 --> 00:06:11,203
Welcome back.

127
00:06:12,843 --> 00:06:15,165
Really. You shouldn't have gone
to all this trouble.

128
00:06:15,912 --> 00:06:17,918
It's no trouble for our buddy.
Here you go.

129
00:06:19,011 --> 00:06:20,461
Today is Jerry day.

130
00:06:20,606 --> 00:06:22,548
We'll do whatever you want to do.

131
00:06:22,716 --> 00:06:26,068
Honestly, what I would like to do
is have everything go back to normal.

132
00:06:26,193 --> 00:06:27,565
That sounds good to me.

133
00:06:27,690 --> 00:06:29,812
I believe you were going
to do a presentation?

134
00:06:29,937 --> 00:06:31,858
Yes, sir, I was.
I will go set up.

135
00:06:34,286 --> 00:06:36,956
Remember, you guys,
no jokes, no comments,

136
00:06:37,081 --> 00:06:38,343
nothing but support.

137
00:06:38,612 --> 00:06:40,025
He needs a lot of support.

138
00:06:41,693 --> 00:06:43,357
I'm talking about a bra for a man.

139
00:06:45,083 --> 00:06:46,998
Seriously, that was the last one.

140
00:06:47,651 --> 00:06:49,033
So as we know,

141
00:06:49,201 --> 00:06:50,944
spring hunting season is upon us.

142
00:06:52,728 --> 00:06:54,378
Anyway, here's the info

143
00:06:55,074 --> 00:06:56,727
about the new licensing system.

144
00:06:59,495 --> 00:07:01,462
I don't think
your computer's plugged in.

145
00:07:01,630 --> 00:07:02,421
Sorry.

146
00:07:03,066 --> 00:07:05,567
Just gotta power up.
First thing we should have

147
00:07:05,960 --> 00:07:07,910
is my graph about the season.

148
00:07:08,280 --> 00:07:09,803
What? Wait a minute.
No.

149
00:07:10,442 --> 00:07:13,482
That's not the graph. That's a picture
from my vacation in Muncie.

150
00:07:14,226 --> 00:07:16,769
You went on a vacation,
and you chose Muncie, Indiana?

151
00:07:16,937 --> 00:07:18,604
My wife and I have a timeshare.

152
00:07:19,426 --> 00:07:20,426
In Muncie?

153
00:07:21,362 --> 00:07:23,062
Muncie is a lovely city.

154
00:07:24,339 --> 00:07:26,735
Anyway, hunting and fishing season
is winding down.

155
00:07:26,860 --> 00:07:29,893
And we all know that it's already
closed season on twout.

156
00:07:30,018 --> 00:07:30,741
So...

157
00:07:31,872 --> 00:07:32,993
I said "twout",

158
00:07:33,270 --> 00:07:34,495
instead of "trout".

159
00:07:34,663 --> 00:07:35,996
It happens to everyone.

160
00:07:36,164 --> 00:07:38,123
My marbles are full of mouth today.

161
00:07:38,915 --> 00:07:40,141
You know what,

162
00:07:40,544 --> 00:07:43,165
has anybody seen my glasses?
I don't even think I can...

163
00:07:51,675 --> 00:07:54,056
It says here 1:00 meeting.
Who are we meeting with?

164
00:07:54,224 --> 00:07:56,809
- Don't worry about it.
- Please no.

165
00:07:56,977 --> 00:07:58,887
It's important to meet
with the park rangers.

166
00:07:59,012 --> 00:08:00,896
They are the first line of defense.

167
00:08:05,360 --> 00:08:06,772
What's up, pencil pushers?

168
00:08:07,195 --> 00:08:08,570
Good to see you, man.

169
00:08:10,156 --> 00:08:12,741
Is it hot in here? I feel hot.
Are you guys hot?

170
00:08:12,909 --> 00:08:14,410
How you guys doing? I'm good.

171
00:08:14,578 --> 00:08:15,953
You guys got any snacks?

172
00:08:17,205 --> 00:08:19,206
Carl is the head
of all outdoor security.

173
00:08:19,331 --> 00:08:21,952
Why was he transferred
from his indoor desk job, you ask.

174
00:08:22,077 --> 00:08:23,467
- Listen.
- It's so sweet.

175
00:08:23,592 --> 00:08:25,514
Hey, Leslie, have you seen Avatar?

176
00:08:25,639 --> 00:08:27,239
I never saw Avatar.

177
00:08:27,364 --> 00:08:29,323
I wanted to read the book first,

178
00:08:29,718 --> 00:08:32,466
but then I realized
there's no book version of Avatar.

179
00:08:32,591 --> 00:08:34,726
What'd you guys do
for St. Patty's day?

180
00:08:34,851 --> 00:08:37,723
I was wearing this t-shirt that said :
"Kiss me. I'm Irish."

181
00:08:37,848 --> 00:08:39,518
But no one would kiss me.

182
00:08:40,971 --> 00:08:41,774
So...

183
00:08:43,179 --> 00:08:46,275
you're too important for me
until one of your own gets attacked.

184
00:08:46,540 --> 00:08:50,434
I just feel like there's more we can do
to keep the parks safe.

185
00:08:50,653 --> 00:08:52,806
You think you know how to do my job?

186
00:08:52,931 --> 00:08:55,335
You might not be so confident
once you've walked a mile

187
00:08:55,460 --> 00:08:56,860
in my size sevens.

188
00:08:58,299 --> 00:08:59,547
Kind of small feet.

189
00:08:59,831 --> 00:09:01,540
Seven is the worldwide average.

190
00:09:04,788 --> 00:09:07,538
Welcome to the emergency
self-defense class.

191
00:09:07,663 --> 00:09:11,049
I'll be showing you how to escape
from a variety of situations

192
00:09:11,364 --> 00:09:14,470
while inflicting maximum damage
on your attackers.

193
00:09:14,709 --> 00:09:17,330
Do you think that maybe
I should put Mark in a headlock?

194
00:09:17,455 --> 00:09:20,655
That way I can show everyone
how to escape a pervert.

195
00:09:21,107 --> 00:09:24,131
In the scenario you just laid out,
you're the pervert.

196
00:09:24,256 --> 00:09:26,106
You understand that, right?

197
00:09:26,440 --> 00:09:27,691
- You wish.
- Enough.

198
00:09:27,947 --> 00:09:29,290
I'm gonna start off simple

199
00:09:29,415 --> 00:09:33,409
and demonstrate how to extract
oneself from a wrist grab.

200
00:09:34,179 --> 00:09:36,481
Andy, Ann, step up here.

201
00:09:36,846 --> 00:09:38,795
I watch a lot of lifetime movies.

202
00:09:38,920 --> 00:09:41,040
There was this one,
How far is too far enough:

203
00:09:41,418 --> 00:09:43,298
the Teri Palliber Lonergan story.

204
00:09:43,538 --> 00:09:46,472
This woman had agoraphobia,
and her therapist was obsessed with her.

205
00:09:46,597 --> 00:09:48,717
And he hid in her house,
and then he attacked her

206
00:09:48,842 --> 00:09:50,505
and tried to eat her toes.

207
00:09:50,902 --> 00:09:53,091
Also, her daughter
was having sex way too young.

208
00:09:53,834 --> 00:09:57,235
So yeah, free
self-defense class, I'm there.

209
00:09:58,765 --> 00:10:00,488
And just twist your weight.

210
00:10:01,135 --> 00:10:02,768
Very good.
Very good. Well done.

211
00:10:02,936 --> 00:10:04,819
What's up now, mugger?

212
00:10:04,944 --> 00:10:06,772
That was awesome.
That was really good.

213
00:10:09,366 --> 00:10:11,944
I don't know, Leslie.
I'd rather be back at the office.

214
00:10:12,242 --> 00:10:15,667
I know this is painful for you, Jerry,
but you have to be strong.

215
00:10:17,109 --> 00:10:18,408
You guys ready?

216
00:10:19,792 --> 00:10:22,092
Oh, boy, yeah.
Okay, we're ready.

217
00:10:22,496 --> 00:10:25,123
I'm gonna show you guys
all the problems we've been facing.

218
00:10:25,357 --> 00:10:27,668
I'm gonna show you
we've been doing everything we can.

219
00:10:27,836 --> 00:10:29,857
I'm looking forward
to working together, Carl.

220
00:10:29,982 --> 00:10:32,214
And after that, I'm gonna
show you this log I found.

221
00:10:32,584 --> 00:10:34,684
It's got, like, 50 worms on it.

222
00:10:35,580 --> 00:10:37,030
I call it worm log.

223
00:10:39,013 --> 00:10:41,132
Yeah, I've always been
a bit of an outdoorsman.

224
00:10:41,257 --> 00:10:43,317
When I was a kid,
my parents used to make me hang out

225
00:10:43,442 --> 00:10:46,602
in the backyard a lot
and just run around till I got tired.

226
00:10:47,186 --> 00:10:50,879
But if there's any criminals
out there watching, I never get tired.

227
00:10:51,866 --> 00:10:53,116
And ladies too.

228
00:10:56,322 --> 00:10:59,292
- This thing is a mess.
- We used to have three carts actually.

229
00:10:59,417 --> 00:11:02,449
The first one got pushed into the creek
by some kids.

230
00:11:02,574 --> 00:11:06,343
The second one raccoons got onto.
There was urine everywhere.

231
00:11:06,606 --> 00:11:08,667
And the third one
was recently stolen.

232
00:11:08,835 --> 00:11:10,806
- What's this one?
- This is the second one.

233
00:11:10,931 --> 00:11:12,481
The raccoon piss one.

234
00:11:13,589 --> 00:11:16,383
All right, so we're gonna
just head out.

235
00:11:20,508 --> 00:11:22,431
I think we got too much weight.

236
00:11:24,523 --> 00:11:25,851
That's Tom probably.

237
00:11:26,186 --> 00:11:29,335
- Are you serious?
- Tom, can you get off please?

238
00:11:29,522 --> 00:11:31,722
Just run alongside the cart, okay?

239
00:11:31,920 --> 00:11:33,025
Okay, here we go.

240
00:11:34,774 --> 00:11:37,149
So let me tell you
a little bit about the park.

241
00:11:37,274 --> 00:11:38,417
Up here on the left

242
00:11:38,542 --> 00:11:41,192
is one of our most beautiful
grass fields.

243
00:11:42,394 --> 00:11:43,944
It's primarily grass.

244
00:11:44,435 --> 00:11:47,585
I'm gonna make a hard left here.
Stick with us, Tom.

245
00:11:48,124 --> 00:11:50,088
Okay.
Lesson learned.

246
00:11:51,268 --> 00:11:52,294
Next, Andy.

247
00:11:55,242 --> 00:11:56,292
Impressive.

248
00:11:56,638 --> 00:12:00,260
I'm gonna engage Andy in an attack hold,
and he's gonna try and break free.

249
00:12:01,032 --> 00:12:03,263
Cool.
Now I don't want to hurt you, Ron.

250
00:12:03,431 --> 00:12:05,599
Don't worry about that.
Just try to escape my attack.

251
00:12:05,767 --> 00:12:08,864
Now when I get out,
am I allowed to counterstrike?

252
00:12:09,729 --> 00:12:12,504
Sure.
When you get out,

253
00:12:12,629 --> 00:12:14,115
you may counterattack.

254
00:12:14,240 --> 00:12:17,390
But just promise me you'll be ready
because, I mean,

255
00:12:18,066 --> 00:12:19,913
I don't want to destroy you.

256
00:12:21,866 --> 00:12:23,907
I see where we're going with this.

257
00:12:25,457 --> 00:12:27,743
Now when your arms
are pinned to your sides,

258
00:12:27,868 --> 00:12:29,247
use your legs to break free

259
00:12:29,415 --> 00:12:31,708
instead of your neck,
which is what Andy's trying to do.

260
00:12:34,232 --> 00:12:35,453
Okay, hold on.

261
00:12:36,820 --> 00:12:38,079
Let him go.

262
00:12:47,507 --> 00:12:48,892
Any of this looking familiar?

263
00:12:49,517 --> 00:12:51,635
Yeah, it happened right over there.

264
00:12:53,165 --> 00:12:54,565
I'm not surprised.

265
00:12:54,791 --> 00:12:56,108
Take a look at this path.

266
00:12:56,604 --> 00:12:59,319
With budget cuts,
we can't afford a single safety light.

267
00:12:59,538 --> 00:13:01,573
There's been ten assaults
already this year.

268
00:13:01,698 --> 00:13:02,706
Really?

269
00:13:02,831 --> 00:13:05,037
- Can't you station a park ranger?
- We have.

270
00:13:05,162 --> 00:13:07,232
Who do you think they're assaulting?

271
00:13:07,462 --> 00:13:09,496
I'm sorry.
I didn't mean to yell.

272
00:13:09,902 --> 00:13:13,014
One way or another, I'm gonna get money
so you can protect Jerry

273
00:13:13,139 --> 00:13:15,961
and all the other helpless,
pathetic people in this town.

274
00:13:19,055 --> 00:13:20,632
You guys got to slow down.

275
00:13:20,757 --> 00:13:23,191
- Can I just take a rest for a minute?
- Sorry. No can do.

276
00:13:23,316 --> 00:13:24,814
Sun's going down.
It's dangerous.

277
00:13:24,939 --> 00:13:27,339
- Let's roll, Carl.
- Okay, going fast.

278
00:13:31,252 --> 00:13:32,644
What day is it today?

279
00:13:33,551 --> 00:13:36,481
- I don't know.
- Okay, but to be fair, you never know.

280
00:13:36,669 --> 00:13:39,881
That's kind of true.
I'm super bad at days.

281
00:13:41,338 --> 00:13:43,029
But honestly I'm fine.

282
00:13:43,599 --> 00:13:45,781
Sorry I squeezed
your lights out there, son.

283
00:13:45,906 --> 00:13:48,651
No worries. Will you show me
how to do that move, though?

284
00:13:48,776 --> 00:13:50,584
Sure. I can teach you right now.

285
00:13:51,122 --> 00:13:52,768
You shouldtake it easy, I think.

286
00:13:52,893 --> 00:13:55,993
Do you want some more water
or maybe some pancakes?

287
00:13:56,409 --> 00:13:57,753
No, no, no.
I'm fine.

288
00:13:59,207 --> 00:14:02,174
- What's the first move in any fight?
- Punch to the balls.

289
00:14:02,625 --> 00:14:05,135
We've all heard the old saying,

290
00:14:05,303 --> 00:14:07,350
"Parks are supposed to be fun,"

291
00:14:07,495 --> 00:14:09,723
but sometimes muggers
have their own ideas.

292
00:14:10,095 --> 00:14:11,613
Leslie Knope is with us again

293
00:14:11,738 --> 00:14:13,732
from the parks department.

294
00:14:14,854 --> 00:14:16,304
Tell us your story.

295
00:14:16,446 --> 00:14:19,024
This is my coworker Jerry Gergich,

296
00:14:19,510 --> 00:14:20,510
diabetic,

297
00:14:21,367 --> 00:14:22,694
sloppily out of shape,

298
00:14:23,282 --> 00:14:24,237
friend.

299
00:14:24,468 --> 00:14:26,107
He was mugged this morning

300
00:14:26,282 --> 00:14:27,491
in Ramsett park.

301
00:14:27,776 --> 00:14:30,744
Who's next?
Your frumpy uncle?

302
00:14:31,021 --> 00:14:32,571
Your simple neighbor?

303
00:14:32,893 --> 00:14:34,959
Your unpopular coworker?

304
00:14:35,763 --> 00:14:38,197
Head of security Carl Lorthner
is doing his best

305
00:14:38,322 --> 00:14:40,504
to keep the parks safe,
but he's failing.

306
00:14:40,672 --> 00:14:43,006
So what is the solution to fix this?

307
00:14:43,250 --> 00:14:45,155
To make it right

308
00:14:45,585 --> 00:14:47,134
and not bad?

309
00:14:48,118 --> 00:14:51,306
We need money from city hall,
and it's not coming through.

310
00:14:51,551 --> 00:14:53,811
And, Pawnee, I am sorry to say this

311
00:14:53,936 --> 00:14:55,977
but your government is failing you.

312
00:14:56,145 --> 00:14:59,808
Up next, ten objects
you didn't know you can eat.

313
00:15:01,848 --> 00:15:03,717
What possessed you to do that?

314
00:15:04,070 --> 00:15:06,963
I'm sorry, Paul, but one of my guys
got mugged in the park.

315
00:15:07,156 --> 00:15:09,288
I don't care how upset you are,

316
00:15:09,458 --> 00:15:12,744
you do not badmouth
your own government on TV.

317
00:15:14,010 --> 00:15:16,831
I'm sure you'll be happy to know
your little stunt worked.

318
00:15:17,134 --> 00:15:19,584
The mayor's going to divert $2,500

319
00:15:19,755 --> 00:15:22,546
to the parks for security upgrades.

320
00:15:23,000 --> 00:15:26,736
There'll be an announcement tomorrow
at 10:00 in the press room,

321
00:15:27,231 --> 00:15:29,802
so make sure you bring the doofus
that got his ass kicked.

322
00:15:30,138 --> 00:15:33,507
I don't know who you're referring to.
We treat everyone with respect.

323
00:15:37,419 --> 00:15:39,604
Good morning.
As many of you know,

324
00:15:39,772 --> 00:15:43,666
there was an incident
involving a government employee

325
00:15:44,483 --> 00:15:45,620
in Ramsett park.

326
00:15:45,745 --> 00:15:49,114
Jerry, are you nervous?
Just talk about how hard it was for you.

327
00:15:49,282 --> 00:15:51,408
Speak from the heart.
You'll be fine.

328
00:15:53,977 --> 00:15:55,203
I wasn't mugged.

329
00:15:59,666 --> 00:16:03,289
- Jerry, why don't you step on up here?
- No, no, I will get up here.

330
00:16:05,829 --> 00:16:07,924
I'm gonna speak for Jerry.

331
00:16:08,092 --> 00:16:11,465
He can't talk right now
because he has hysterical muteness.

332
00:16:12,458 --> 00:16:14,222
- From trauma.
- Correct.

333
00:16:17,328 --> 00:16:19,119
What about this?
Is this fake?

334
00:16:19,244 --> 00:16:21,688
This is real.
I really dislocated my shoulder.

335
00:16:21,881 --> 00:16:23,523
Were you even in the park yesterday?

336
00:16:23,691 --> 00:16:25,859
Look, I was on my way
to feed the hummingbirds,

337
00:16:26,027 --> 00:16:28,036
and I stopped
for a breakfast burrito.

338
00:16:28,619 --> 00:16:29,719
The farting.

339
00:16:30,184 --> 00:16:32,176
And, lord Sheldon,
he lunged at a bird,

340
00:16:32,301 --> 00:16:34,993
I dropped the burrito, and it
lands on a log in the creek.

341
00:16:35,118 --> 00:16:37,040
So I go to reach for it,
I lose my balance,

342
00:16:37,165 --> 00:16:38,997
and I fall on my shoulder
really weird.

343
00:16:39,165 --> 00:16:41,163
Why didn't you
just tell everybody the truth?

344
00:16:41,288 --> 00:16:44,586
Are you kidding me?
Imagine what Tom would have said.

345
00:16:45,671 --> 00:16:48,589
Damn, Jerry,
you jumped in a creek for a burrito?

346
00:16:48,714 --> 00:16:51,024
What would you do for a Klondike bar?
Kill your wife?

347
00:16:51,958 --> 00:16:55,096
Block the opponent's punch
and counterpunch to the jaw.

348
00:16:56,448 --> 00:16:57,448
Shoeshine?

349
00:17:00,263 --> 00:17:02,234
Ann Perkins in the shoe shouse.

350
00:17:02,765 --> 00:17:05,647
I wanted to check in.
I brought some stuff from the hospital.

351
00:17:05,772 --> 00:17:08,805
Water, compress,
aspirin, some lollipops.

352
00:17:08,930 --> 00:17:10,990
- I know you love those.
- You know, thank you,

353
00:17:11,115 --> 00:17:13,990
but I'm really... I'm feeling great.
I took a couple of aspirin

354
00:17:14,158 --> 00:17:16,483
and slept right through the night and...
Hold on a second.

355
00:17:16,608 --> 00:17:18,620
Ludgate!
What the hell?

356
00:17:19,156 --> 00:17:21,331
I got you
those muffins that you're always eating.

357
00:17:21,537 --> 00:17:22,537
Score.

358
00:17:23,209 --> 00:17:25,218
- Tastes like a rug.
- Shut up.

359
00:17:27,470 --> 00:17:30,674
- Good, I'm glad you're feeling better.
- I am. Thank you for that.

360
00:17:31,208 --> 00:17:32,458
That's so cool.

361
00:17:32,705 --> 00:17:35,575
Since when did he start doing stuff
for other people?

362
00:17:35,700 --> 00:17:38,732
Now I actually am worried
that something happened to his brain.

363
00:17:40,130 --> 00:17:41,780
People change, I guess.

364
00:17:43,229 --> 00:17:45,814
Wouldn't you rather the money
go to keeping the parks safe?

365
00:17:45,982 --> 00:17:48,358
Yes, but now we have the money
under false pretences.

366
00:17:48,526 --> 00:17:50,819
I actually think
that you have a bigger problem.

367
00:17:51,110 --> 00:17:51,987
What?

368
00:17:52,112 --> 00:17:54,367
There is someone in your department
who is willing to lie

369
00:17:54,492 --> 00:17:57,492
about being mugged because
he's afraid of his coworkers.

370
00:18:05,498 --> 00:18:07,335
Carl, what are you doing here?

371
00:18:09,466 --> 00:18:12,007
Well, since you decided
to drag my name through the mud on TV,

372
00:18:12,175 --> 00:18:13,906
I figured why not return the favor?

373
00:18:14,031 --> 00:18:15,802
Carl, could you quietly quiet down

374
00:18:16,076 --> 00:18:18,944
for a second and please tell me
what's going on quietly?

375
00:18:19,643 --> 00:18:21,407
In a quiet voice.

376
00:18:22,253 --> 00:18:25,101
A citizen handed in
a very interesting home video

377
00:18:25,226 --> 00:18:29,065
that I think the people of Pawnee
will find very interesting.

378
00:18:29,364 --> 00:18:30,864
Take a peek at this.

379
00:18:33,837 --> 00:18:35,737
Keep your eyes on the creek.

380
00:18:38,011 --> 00:18:39,208
There he is.

381
00:18:39,535 --> 00:18:41,244
This is my favorite part.

382
00:18:41,972 --> 00:18:44,456
Not willing to share with a dog.

383
00:18:44,624 --> 00:18:48,293
So it would appear that park security
was not to blame after all.

384
00:18:48,833 --> 00:18:51,533
Okay, look,
I have a proposition for you.

385
00:18:51,839 --> 00:18:54,199
Fine. I'll have sex with you
in exchange for the tape.

386
00:18:54,634 --> 00:18:56,854
That's not what I'm saying!
Okay?

387
00:18:57,321 --> 00:19:00,288
Just listen to what I have to say.

388
00:19:01,571 --> 00:19:04,872
The Ramsett park mugging story
continues

389
00:19:04,997 --> 00:19:06,936
with a shiny new twist.

390
00:19:07,928 --> 00:19:10,858
Ranger Carl Lorthner is here today.

391
00:19:11,297 --> 00:19:15,028
Now I understand
that you have a bombshell

392
00:19:15,196 --> 00:19:18,049
to drop
that you are just sitting on.

393
00:19:19,410 --> 00:19:20,575
Yes, that's correct.

394
00:19:20,743 --> 00:19:24,662
I came on today because I have
some important information to share.

395
00:19:24,839 --> 00:19:26,704
I finally saw Avatar,

396
00:19:27,399 --> 00:19:29,793
and I thought it lived up
to the hype.

397
00:19:31,128 --> 00:19:33,613
I'm not sure
what that has to do with the mugging.

398
00:19:35,137 --> 00:19:36,887
Leslie, counterpoint.

399
00:19:37,194 --> 00:19:38,510
I disagree with Carl.

400
00:19:39,243 --> 00:19:41,477
I have seen avatar as well.

401
00:19:41,602 --> 00:19:43,699
And I think it exceeded the hype.

402
00:19:44,100 --> 00:19:44,972
What?

403
00:19:45,385 --> 00:19:46,338
Oh, boy.

404
00:19:46,804 --> 00:19:48,954
- We...
- Disagreement.

405
00:19:50,078 --> 00:19:52,733
Well, things are
really heating up in here. Aren't they?

406
00:19:53,108 --> 00:19:54,984
We should probably take some calls.

407
00:19:55,789 --> 00:19:57,689
That segment was a disaster!

408
00:19:58,472 --> 00:20:02,359
Don't you ever **** me like that again.
This is Pawnee ***** today.

409
00:20:02,743 --> 00:20:06,462
Do you know that for you I bumped a cat
that can stand up on its hinders?

410
00:20:06,747 --> 00:20:08,915
You disgust me, Knope.
Get out of my sight.

411
00:20:09,205 --> 00:20:10,705
- Yes, ma'am.
- Go.

412
00:20:13,636 --> 00:20:16,422
Don't make me chase you.
You want to go eat something.

413
00:20:18,259 --> 00:20:19,717
I am part of a great team.

414
00:20:20,125 --> 00:20:23,122
And sometimes the best way
to maintain a strong team

415
00:20:23,247 --> 00:20:26,490
is just by letting someone know
that he is still a part of it.

416
00:20:27,059 --> 00:20:28,549
This is the only copy.

417
00:20:28,674 --> 00:20:30,674
And I am going to destroy it.

418
00:20:32,231 --> 00:20:34,631
Right after I watch it
one more time.

419
00:20:35,991 --> 00:20:38,691
I'm sorry, but it is so good.

420
00:20:41,741 --> 00:20:44,367
- I got you a peppermint latte.
- Seriously?

421
00:20:45,682 --> 00:20:48,052
- My goodness. I love a pepp...
- I know.

422
00:20:50,333 --> 00:20:51,330
Jeez.

423
00:20:52,209 --> 00:20:53,001
Go ahead.

424
00:20:53,169 --> 00:20:55,917
We should just directly
apply the food to your clothes.

425
00:20:57,817 --> 00:20:59,667
Making fun of Jerry's back.

426
00:21:00,634 --> 00:21:04,154
They can laugh at me all they want.
Because two more years until I retire

427
00:21:04,279 --> 00:21:06,180
with full benefits and pension.

428
00:21:06,552 --> 00:21:09,100
And my wife and I, we have bought
a little cottage on a lake.

429
00:21:09,268 --> 00:21:12,192
And I'm gonna make myself
a stack of mystery novels,

430
00:21:12,317 --> 00:21:16,092
a box of cigars and I could sit back
and enjoy my life.

431
00:21:18,247 --> 00:21:22,133
April was double-checked the lunch order
you want the salmon or the twout?

432
00:21:22,490 --> 00:21:24,481
Twout! Twout! Twout!

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
