1
00:00:02,364 --> 00:00:03,886
Here's the situation.

2
00:00:04,296 --> 00:00:06,088
Your parents went away on  vacation.

3
00:00:06,584 --> 00:00:08,466
They left the keys
to the brand new Porsche.

4
00:00:08,879 --> 00:00:10,146
Would they mind?

5
00:00:10,577 --> 00:00:11,802
Of course not.

6
00:00:57,370 --> 00:00:58,920
Thank you, thank you.

7
00:00:59,719 --> 00:01:01,268
Just a little something I know.

8
00:01:01,795 --> 00:01:05,043
- So what's up?
- Someone is on fire in Ramsett park.

9
00:01:05,168 --> 00:01:07,108
They need you to get
down there right away.

10
00:01:08,035 --> 00:01:09,085
Oh, my God.

11
00:01:11,446 --> 00:01:12,746
- La Fabrique - 

12
00:01:13,490 --> 00:01:14,636
- Feygnasse Team -

13
00:01:14,761 --> 00:01:16,075
Episode 201
<i>Pawnee Zoo</i>

14
00:01:16,200 --> 00:01:18,500
Synchro: Albator1932, Lestat78, mpm

15
00:01:29,939 --> 00:01:32,591
I've been spending the summer
doing a lot of zoo promotions.

16
00:01:34,393 --> 00:01:36,011
Parrots live a very long time,

17
00:01:36,179 --> 00:01:37,847
so we had a birthday party

18
00:01:38,015 --> 00:01:39,563
for a parrot that turned 60.

19
00:01:43,437 --> 00:01:45,167
Chimpanzees are very smart,

20
00:01:45,404 --> 00:01:47,356
so we had them graduate

21
00:01:47,524 --> 00:01:48,509
from college.

22
00:01:48,634 --> 00:01:51,360
And they like to fling their feces,
so we hoped they'd fling their hats,

23
00:01:51,669 --> 00:01:53,779
but they...
they just flung their feces.

24
00:01:55,584 --> 00:01:58,913
The Pawnee zoo recently purchased
two South African black-footed penguins,

25
00:01:59,038 --> 00:02:00,065
Tux and Flipper.

26
00:02:00,190 --> 00:02:02,136
And as part of our zoo promotion,

27
00:02:02,261 --> 00:02:04,540
we gonna give them
a marriage ceremony...

28
00:02:05,568 --> 00:02:07,279
because they mate for life.

29
00:02:07,816 --> 00:02:08,616
Tux...

30
00:02:08,957 --> 00:02:10,286
do you take Flipper

31
00:02:10,411 --> 00:02:12,330
to be your lawfully wedded wife?

32
00:02:15,303 --> 00:02:16,303
I do. I do.

33
00:02:17,023 --> 00:02:18,673
By the power vested in me

34
00:02:18,798 --> 00:02:21,043
from the department
of Parks and Recreation,

35
00:02:21,168 --> 00:02:23,406
I now pronounce you
husband and wife.

36
00:02:35,267 --> 00:02:37,363
Okay, well at least they're married.

37
00:02:37,756 --> 00:02:39,406
Are they making babies?

38
00:02:39,869 --> 00:02:42,620
No, not those two.
Those are both boy penguins.

39
00:02:43,260 --> 00:02:44,080
Sorry?

40
00:02:44,471 --> 00:02:46,277
Tux and Flipper are both boys.

41
00:02:46,402 --> 00:02:49,502
So you should have pronounced them
husband and husband, technically.

42
00:02:49,862 --> 00:02:51,112
That's awesome.

43
00:02:54,054 --> 00:02:56,550
Still... you couldn't have
asked for better weather.

44
00:02:59,170 --> 00:03:00,691
You big sandwich eater.

45
00:03:01,859 --> 00:03:03,109
Cut it out now.

46
00:03:03,684 --> 00:03:05,804
Just to be clear,
that was a friend punch.

47
00:03:05,929 --> 00:03:07,843
There was no flirtatious meaning

48
00:03:07,968 --> 00:03:09,939
behind that playful punch
I gave your arm.

49
00:03:10,107 --> 00:03:11,213
I do understand.

50
00:03:11,338 --> 00:03:12,746
You've made it abundantly clear

51
00:03:12,871 --> 00:03:15,486
that there's no romantic element
to our relationship in any way.

52
00:03:15,958 --> 00:03:16,772
Good.

53
00:03:17,080 --> 00:03:19,782
Isn't it good to be able
to horse around like this as friends?

54
00:03:19,950 --> 00:03:21,226
- It is...
- Yeah.

55
00:03:21,351 --> 00:03:23,202
I really hit rock bottom that night.

56
00:03:23,846 --> 00:03:25,746
And I mean that I literally fell

57
00:03:25,914 --> 00:03:28,127
to the bottom of a pit
and hit a rock.

58
00:03:28,378 --> 00:03:30,204
I remember laying there, thinking,

59
00:03:30,806 --> 00:03:33,286
"There's probably a good reason
why I'm down here...

60
00:03:33,675 --> 00:03:34,778
"and single."

61
00:03:36,771 --> 00:03:39,135
And then I started thinking
that I need morphine.

62
00:03:39,419 --> 00:03:40,803
I hear you made

63
00:03:40,971 --> 00:03:43,264
two male penguins very happy today.

64
00:03:43,781 --> 00:03:46,016
You're making history.
I like that, sticking your neck out.

65
00:03:46,293 --> 00:03:48,394
I didn't stick my neck out.
It was an accident.

66
00:03:48,562 --> 00:03:50,104
Out of the mainstream, that's cool.

67
00:03:50,272 --> 00:03:51,791
- I'm in the mainstream.
- I know.

68
00:03:51,916 --> 00:03:53,856
- Not out of it.
- Social activism.

69
00:03:55,277 --> 00:03:58,571
People in this town don't really like
government employees being activists.

70
00:03:59,137 --> 00:04:00,947
Last year, a garbage man
was suspended

71
00:04:01,072 --> 00:04:03,159
for wearing a livestrong bracelet.

72
00:04:04,393 --> 00:04:06,996
Some guy who owns a gay bar
sent you a cake.

73
00:04:07,228 --> 00:04:08,726
Pawnee has a gay bar?

74
00:04:09,732 --> 00:04:10,583
The Bulge.

75
00:04:12,242 --> 00:04:13,803
It's behind my house.

76
00:04:13,928 --> 00:04:15,578
The Bulge is a gay bar?

77
00:04:16,291 --> 00:04:17,795
The nights I've wasted there.

78
00:04:17,920 --> 00:04:20,570
"Leslie, hey girrrrrrrrl."

79
00:04:21,043 --> 00:04:22,245
With eight "r"s.

80
00:04:22,621 --> 00:04:24,782
"Thank you for supporting
the gay marriage.

81
00:04:24,907 --> 00:04:27,107
You rock!
The boys at the Bulge."

82
00:04:27,476 --> 00:04:29,226
They thought
that was a political gesture?

83
00:04:29,782 --> 00:04:32,374
Nobody eat that cake.
Tom, step into my office.

84
00:04:32,499 --> 00:04:34,273
- That's also my office.
- Whatever.

85
00:04:34,398 --> 00:04:36,265
- I know that you are not gay.
- I'm not.

86
00:04:36,390 --> 00:04:38,012
- But you're effeminate.
- What?

87
00:04:38,137 --> 00:04:40,613
You're wearing a peach shirt
with a coiled snake on it.

88
00:04:40,781 --> 00:04:43,699
- Yes, 'cause it was featured
in <I>Details</I> magazine and it's awesome.

89
00:04:44,294 --> 00:04:46,519
Effeminate.
Anyway, so the point is...

90
00:04:46,644 --> 00:04:49,997
do you think that marrying penguins
made some kind of statement?

91
00:04:50,165 --> 00:04:52,831
Yes, the statement
was that you're very lonely,

92
00:04:52,956 --> 00:04:54,001
and you need a pet.

93
00:04:55,308 --> 00:04:56,997
They sent you t-shirts too.

94
00:04:57,345 --> 00:04:59,105
Look they sent us a t-shirt.

95
00:05:02,719 --> 00:05:04,178
Mine has a collar on it.

96
00:05:06,348 --> 00:05:07,491
There she is.

97
00:05:09,333 --> 00:05:12,280
This is my boyfriend, Derek,
and this is Derek's boyfriend, Ben.

98
00:05:15,153 --> 00:05:16,815
Wait, sorry.
What's the situation?

99
00:05:17,324 --> 00:05:19,393
- What do you mean?
- How does this work?

100
00:05:19,821 --> 00:05:21,623
Derek is gay.
But he's straight for me,

101
00:05:21,748 --> 00:05:23,700
but he's gay for Ben,
and Ben's gay for Derek.

102
00:05:23,825 --> 00:05:24,881
And I hate ben.

103
00:05:25,006 --> 00:05:26,408
It's not that complicated.

104
00:05:27,120 --> 00:05:28,369
Yeah. Sure.

105
00:05:28,537 --> 00:05:32,001
The thing about youth culture
is I don't understand it.

106
00:05:33,236 --> 00:05:36,169
- So what can I do for you guys?
- They just really wanted to meet you,

107
00:05:36,294 --> 00:05:38,545
because you're kinda
like their hero now.

108
00:05:40,832 --> 00:05:42,801
Please, it was just penguins.

109
00:05:42,926 --> 00:05:44,219
I'm a public servant,

110
00:05:44,344 --> 00:05:46,178
and I can't take a political stance.

111
00:05:46,346 --> 00:05:48,150
But you did, and it was awesome.

112
00:05:48,275 --> 00:05:50,396
None of the other politicians
ever take a stand,

113
00:05:50,521 --> 00:05:52,518
and it means a lot
to the gay community.

114
00:05:52,686 --> 00:05:56,658
It does. And we're gonna have this party
tomorrow night at the Bulge,

115
00:05:56,880 --> 00:05:58,602
and we had something to ask you.

116
00:05:58,727 --> 00:06:00,734
We really want you
to be the guest of honor.

117
00:06:01,058 --> 00:06:01,934
Oh, my!

118
00:06:02,445 --> 00:06:05,061
- It'd be amazing if you came.
- Yeah, you have to come.

119
00:06:05,186 --> 00:06:07,481
- Who made this?
- We did.

120
00:06:07,606 --> 00:06:08,867
- How?
- Photoshop.

121
00:06:09,035 --> 00:06:10,364
- What?
- Computers.

122
00:06:14,514 --> 00:06:15,874
Why are you all dolled up?

123
00:06:16,042 --> 00:06:17,604
It's a long story.

124
00:06:17,729 --> 00:06:19,982
I'm the guest of honor
at this gay bar.

125
00:06:20,163 --> 00:06:22,499
I guess gay men
are starting to like me.

126
00:06:22,999 --> 00:06:25,433
I don't know,
they think I'm fabulous or something.

127
00:06:25,977 --> 00:06:27,396
You look good, girl.

128
00:06:27,521 --> 00:06:29,138
You gonna turn somebody tonight.

129
00:06:34,019 --> 00:06:35,860
That was, hands down,
the best interaction

130
00:06:35,985 --> 00:06:37,438
I've ever had with Donna.

131
00:06:39,173 --> 00:06:41,259
This is Marcia Langman

132
00:06:41,610 --> 00:06:45,070
from the Society
for Family Stability Foundation.

133
00:06:45,752 --> 00:06:48,305
- Hello. I love your top.
- Thank you so much.

134
00:06:48,430 --> 00:06:51,785
I was hoping you had a moment to discuss
the events at the zoo yesterday.

135
00:06:51,910 --> 00:06:54,371
Well, I have nothing
to do with this, so...

136
00:06:54,982 --> 00:06:58,604
What can I do for you
and those fine people at...

137
00:06:59,108 --> 00:07:00,276
the SFSF?

138
00:07:00,629 --> 00:07:01,430
Well...

139
00:07:02,223 --> 00:07:04,631
you could resign.
If you're up for it.

140
00:07:06,338 --> 00:07:07,342
You're serious?

141
00:07:07,539 --> 00:07:10,662
When you performed
a marriage for gay penguins

142
00:07:11,018 --> 00:07:13,719
using taxpayer money
on government property,

143
00:07:13,844 --> 00:07:16,073
you were symbolically taking a stand

144
00:07:16,198 --> 00:07:18,548
in favor of the gay marriage agenda.

145
00:07:19,396 --> 00:07:21,303
I'm sorry,
but hold on a second there.

146
00:07:21,428 --> 00:07:24,247
Marcia, that was
not my intention at all.

147
00:07:24,819 --> 00:07:26,737
Why else would you marry penguins?

148
00:07:27,494 --> 00:07:29,988
Because I firmly believed
that it would be cute.

149
00:07:30,113 --> 00:07:31,113
And it was.

150
00:07:31,486 --> 00:07:32,534
Are you married?

151
00:07:33,898 --> 00:07:35,316
Not yet, Marcia.

152
00:07:35,916 --> 00:07:37,331
Soon.
Probably.

153
00:07:37,499 --> 00:07:40,034
I have a plan, but not...
no, not now.

154
00:07:40,159 --> 00:07:43,253
Not dating anyone yet.
Focusing on my career, but...

155
00:07:43,596 --> 00:07:44,777
I thought so.

156
00:07:44,902 --> 00:07:47,252
So you couldn't possibly understand.

157
00:07:47,377 --> 00:07:49,129
But when gays marry,

158
00:07:49,254 --> 00:07:51,845
it ruins marriage
for the rest of us.

159
00:07:52,264 --> 00:07:54,364
So either you annul the wedding

160
00:07:54,766 --> 00:07:56,975
or I'll publicly
ask for your resignation.

161
00:07:59,396 --> 00:08:00,348
You know what?

162
00:08:00,473 --> 00:08:02,754
I'm terrible with directions.
If I'm heading to the parking,

163
00:08:02,879 --> 00:08:05,109
- do I make a left, or I go right?
- Left.

164
00:08:05,832 --> 00:08:07,611
- You're welcome.
- Annul the wedding!

165
00:08:14,511 --> 00:08:16,332
I'm supposed
to meet Leslie for lunch,

166
00:08:16,457 --> 00:08:18,664
but she actually works, so...

167
00:08:20,532 --> 00:08:23,223
Mark was brought to my hospital
the night of the accident.

168
00:08:23,348 --> 00:08:24,753
He was here for a week.

169
00:08:24,921 --> 00:08:26,964
I think when his head hit the ground

170
00:08:27,146 --> 00:08:28,831
it must've knocked something loose,

171
00:08:28,956 --> 00:08:31,706
because he's actually
a pretty nice guy now.

172
00:08:32,951 --> 00:08:34,805
And Andy, after we broke up,

173
00:08:34,973 --> 00:08:37,415
he told me he was going to Kansas

174
00:08:37,623 --> 00:08:38,976
to climb mountains.

175
00:08:40,689 --> 00:08:43,589
So, I don't... I don't
really know where he is.

176
00:08:44,056 --> 00:08:46,358
Have you seen
that documentary about food yet?

177
00:08:46,526 --> 00:08:49,417
No, I haven't. But I heard
it's really good. I want to see it.

178
00:08:49,793 --> 00:08:51,128
We should go together.

179
00:08:52,481 --> 00:08:53,481
What?

180
00:08:54,050 --> 00:08:55,663
I don't...
I don't think so.

181
00:08:55,788 --> 00:08:57,953
That would be like a date.

182
00:08:59,470 --> 00:09:00,570
Say no more.

183
00:09:02,715 --> 00:09:03,865
I hate salad.

184
00:09:05,951 --> 00:09:07,880
I don't want to ever keep secrets
from you.

185
00:09:08,048 --> 00:09:08,993
Me neither.

186
00:09:09,118 --> 00:09:11,513
Let's invent our own secret language
that only we understand,

187
00:09:11,638 --> 00:09:14,679
and then we can use it around people
and no one will know what we'resaying.

188
00:09:16,755 --> 00:09:19,874
But in the meantime,
I'm just gonna tell you in English,

189
00:09:19,999 --> 00:09:21,977
Mark kind of asked me out.

190
00:09:23,256 --> 00:09:24,480
He weirdly asked me

191
00:09:24,648 --> 00:09:26,364
to go see a movie with him.

192
00:09:26,489 --> 00:09:27,941
And I said no, of course.

193
00:09:28,109 --> 00:09:29,136
But I just...

194
00:09:29,261 --> 00:09:31,069
I just wanted to let you know.

195
00:09:33,951 --> 00:09:36,033
I'm feeling a lot of confusing things
right now.

196
00:09:36,880 --> 00:09:38,351
No, of course you are.

197
00:09:38,536 --> 00:09:41,466
And your friendship
is the most important thing to me.

198
00:09:42,332 --> 00:09:43,929
And he's off-limits.

199
00:09:47,712 --> 00:09:48,712
Thank you.

200
00:09:49,839 --> 00:09:51,984
I was really serious
about that secret language.

201
00:09:52,109 --> 00:09:52,841
I know.

202
00:09:56,332 --> 00:09:57,939
I can't believe this is a gay bar.

203
00:09:58,064 --> 00:10:01,038
Especially with that heterosexual cowboy
greeting us on the way in.

204
00:10:01,163 --> 00:10:02,392
Where should I drink now?

205
00:10:02,560 --> 00:10:05,062
There's a bar called
Pitchers and Catchers. You can go there.

206
00:10:06,426 --> 00:10:07,606
Here's the plan.

207
00:10:07,774 --> 00:10:09,359
Our position is we have no position.

208
00:10:09,484 --> 00:10:11,210
So let's say
thank you for the party,

209
00:10:11,335 --> 00:10:14,738
but we regretfully decline
your offer to honor me.

210
00:10:15,058 --> 00:10:16,406
I can't believe you came.

211
00:10:17,978 --> 00:10:20,285
- It's Leslie Knope!
- Leslie Knope, you're my hero.

212
00:10:21,080 --> 00:10:21,828
Nice.

213
00:10:21,996 --> 00:10:23,960
- You are looking hot.
- Really?

214
00:10:24,984 --> 00:10:26,020
Thank you, Ben!

215
00:10:26,145 --> 00:10:28,345
I'm really enjoying this hug.

216
00:10:29,136 --> 00:10:31,046
It's so nice, but I need
to tell you something.

217
00:10:31,214 --> 00:10:33,408
- This one is on us.
- Well, if it's free.

218
00:10:43,118 --> 00:10:46,186
She's Leslie Knope
and she wants to recruit you.

219
00:10:48,982 --> 00:10:51,056
Oh, my. Okay, please...

220
00:10:52,844 --> 00:10:54,691
gentlemen, first of all...

221
00:10:55,035 --> 00:10:58,249
I would like to say thank you so much
for throwing me this party.

222
00:10:58,374 --> 00:11:00,492
Especially on a night
when the Colts are playing.

223
00:11:02,413 --> 00:11:03,913
We love you, Leslie!

224
00:11:07,843 --> 00:11:09,053
This is green.

225
00:11:19,304 --> 00:11:23,143
I wanted to tell you that I wasn't
really trying to take a stand

226
00:11:23,543 --> 00:11:25,879
when I married those...

227
00:11:26,418 --> 00:11:27,418
penguins.

228
00:11:29,419 --> 00:11:30,772
You're chanting my name.

229
00:11:34,394 --> 00:11:35,868
I just have one thing to say.

230
00:11:35,993 --> 00:11:38,320
Together we can
change Pawnee forever!

231
00:11:38,445 --> 00:11:39,545
Let's dance!

232
00:11:43,289 --> 00:11:46,330
Leslie Knope's in the building, y'all.
Let's get wild!

233
00:11:53,004 --> 00:11:55,287
- I've seen so many dudes
from city hall here tonight,

234
00:11:55,412 --> 00:11:56,462
it's crazy.

235
00:11:57,332 --> 00:11:59,126
But I guess
they've seen me here too.

236
00:11:59,251 --> 00:12:00,870
So, that's not great.

237
00:12:03,945 --> 00:12:05,640
I got your text, is everything okay?

238
00:12:06,216 --> 00:12:08,812
They're having a party in my honor.
Go to the bar.

239
00:12:08,937 --> 00:12:10,977
If you're my friend,
you can drink for free.

240
00:12:12,532 --> 00:12:15,734
And maybe next time, don't use the words
"medical emergency."

241
00:12:16,280 --> 00:12:17,944
I'm so glad you're here!

242
00:12:18,112 --> 00:12:20,489
- I'm really sober.
- Okay, I'll get you a drink.

243
00:12:27,956 --> 00:12:29,696
You know why tonight's fun?

244
00:12:29,821 --> 00:12:31,571
'cause everyone's so gay.

245
00:12:32,207 --> 00:12:35,266
And they know how to have fun,
and the dancing!

246
00:12:36,305 --> 00:12:37,305
Just...

247
00:12:37,660 --> 00:12:39,955
it's everyone is just who they are,

248
00:12:40,080 --> 00:12:42,844
and who they are
is just stone-cold gay.

249
00:12:58,247 --> 00:12:59,695
I'm having such a nice time.

250
00:12:59,999 --> 00:13:01,958
I've met many interesting people.

251
00:13:02,083 --> 00:13:04,641
And there's two bisexual guys here,

252
00:13:04,766 --> 00:13:06,735
and I got both
of their phone numbers.

1
00:13:17,377 --> 00:13:18,407
All morning.

2
00:13:24,729 --> 00:13:25,887
Have fun last night?

3
00:13:26,055 --> 00:13:29,299
I had three drinks named after me,
so that's pretty fun.

4
00:13:29,424 --> 00:13:31,868
Plus, Ben and Derek
are taking me shopping on Saturday.

5
00:13:31,993 --> 00:13:34,104
And we're gonna find out
my actual bra size.

6
00:13:35,432 --> 00:13:37,566
I guess I'm kind of like
queen of the gays.

7
00:13:38,373 --> 00:13:39,532
Bully for you.

8
00:13:39,657 --> 00:13:42,571
I just got a phone call.
They want you to go on <i>Pawnee Today.</i>

9
00:13:42,903 --> 00:13:44,781
That's huge!
What's the topic?

10
00:13:45,896 --> 00:13:49,327
That Marcia from the family thing
is calling for your resignation.

11
00:13:50,133 --> 00:13:51,872
You gotta go on and defend yourself.

12
00:13:52,066 --> 00:13:55,125
Why? I haven't even officially
taken a stand on gay marriage.

13
00:13:56,122 --> 00:13:59,552
That's funny, somebody just told me
you were queen of the gays.

14
00:14:01,202 --> 00:14:02,340
That was me.

15
00:14:04,806 --> 00:14:06,842
<i>Pawnee Today,</i> with Joan Callamezzo.

16
00:14:06,967 --> 00:14:08,999
It's kind of like
the <i>Meet The Press</i> of our town.

17
00:14:09,124 --> 00:14:10,473
It's the big time.

18
00:14:10,598 --> 00:14:12,392
I know, it is.
I'm nervous.

19
00:14:12,560 --> 00:14:15,189
I just wish I was here
under different circumstances.

20
00:14:15,523 --> 00:14:16,523
Guess who!

21
00:14:17,134 --> 00:14:19,316
Megan Fox?
One of the Desperate Housewives?

22
00:14:20,234 --> 00:14:21,526
Joan Callamezzo.

23
00:14:22,520 --> 00:14:23,953
What's up?
Good to see you!

24
00:14:24,078 --> 00:14:26,072
Tom comes all the time.
Joan loves him.

25
00:14:26,240 --> 00:14:28,241
You have the softest skin
of any woman in Pawnee.

26
00:14:29,379 --> 00:14:30,633
I wish you could reach

27
00:14:30,758 --> 00:14:33,394
from your TV screen
and just touch Joan's skin for a second.

28
00:14:33,519 --> 00:14:36,041
That's sweet.
You're pretty soft yourself.

29
00:14:36,245 --> 00:14:38,376
- How are your kids doing?
- They're pretty good.

30
00:14:38,644 --> 00:14:41,213
Is it tough for them
to have a mother that is so beautiful?

31
00:14:41,381 --> 00:14:43,215
What's it like being
the most attractive ?

32
00:14:43,421 --> 00:14:46,635
Keep up those funnies.
I'll have to invite you over for supper.

33
00:14:46,803 --> 00:14:48,428
I'll have to come over for supper.

34
00:14:48,772 --> 00:14:49,804
You well seem.

35
00:14:54,972 --> 00:14:55,934
My God!

36
00:14:56,396 --> 00:14:57,771
Ann Perkins.
How you doing?

37
00:14:58,400 --> 00:15:01,024
I'm good.
How was mountain climbing?

38
00:15:02,803 --> 00:15:03,693
What?

39
00:15:06,741 --> 00:15:09,241
Can I come in?
I wanted to talk to you for a second.

40
00:15:09,943 --> 00:15:10,824
Sure.

41
00:15:14,641 --> 00:15:17,290
<i>We have a controversial topic today.</i>

42
00:15:17,458 --> 00:15:20,008
<i>The gay penguin marriage
at the Pawnee Zoo.</i>

43
00:15:20,586 --> 00:15:23,463
So Marcia,
what is all the fuss about?

44
00:15:23,856 --> 00:15:26,156
The fuss is that Miss Knope

45
00:15:26,384 --> 00:15:29,761
claimed that she was not advocating
for this gay cause.

46
00:15:30,361 --> 00:15:31,867
And then that very night,

47
00:15:31,992 --> 00:15:35,262
she was the guest of honor
at a pro-gay marriage rally

48
00:15:35,387 --> 00:15:37,575
at a bar called The Bulge.

49
00:15:38,146 --> 00:15:39,312
How do you respond?

50
00:15:39,480 --> 00:15:42,232
I'd first like to say that I wasn't
trying to advocate for anyone.

51
00:15:42,604 --> 00:15:45,441
I did not know
that both of the penguins were males,

52
00:15:45,634 --> 00:15:48,834
and I was just trying
to perform a cute, fun ceremony

53
00:15:49,056 --> 00:15:50,490
to promote our local zoo.

54
00:15:50,658 --> 00:15:52,075
I have to say that

55
00:15:52,250 --> 00:15:54,557
that stunt that you did
with the penguins

56
00:15:54,682 --> 00:15:56,371
was clearly over the line.

57
00:15:56,539 --> 00:15:59,709
What if anything can Miss Knope do

58
00:15:59,834 --> 00:16:01,251
to make it right?

59
00:16:01,639 --> 00:16:04,877
- We don't want to be unreasonable.
- Of course not.

60
00:16:05,193 --> 00:16:08,091
We think
that she should separate the penguins,

61
00:16:08,259 --> 00:16:10,035
annul the marriage,

62
00:16:10,303 --> 00:16:11,469
reimburse the taxpayers

63
00:16:11,637 --> 00:16:13,805
for the cost of the wedding,
of course.

64
00:16:14,278 --> 00:16:15,849
And then resign.

65
00:16:16,089 --> 00:16:17,309
Is that it?

66
00:16:17,477 --> 00:16:19,206
- That would do it.
- Anything else?

67
00:16:19,331 --> 00:16:21,771
Want me to jump off a building?
Perform Harry Caray?

68
00:16:21,939 --> 00:16:23,982
Move to a different town.
No, I kid.

69
00:16:24,642 --> 00:16:25,844
Full of ideas today.

70
00:16:25,969 --> 00:16:28,503
This is the reason
why people don't go into politics.

71
00:16:28,628 --> 00:16:30,064
Because I bust my ass

72
00:16:30,189 --> 00:16:32,177
for the people in this city,
and I can't win.

73
00:16:32,450 --> 00:16:34,057
I have one night of fun

74
00:16:34,182 --> 00:16:36,913
with some of the best dancers
I've ever danced with,

75
00:16:37,038 --> 00:16:38,984
and suddenly
everybody's freaking out?

76
00:16:39,207 --> 00:16:41,170
Look at that!
We're lighting up here.

77
00:16:41,295 --> 00:16:43,501
Good, great, let's go for it.
Bring it on.

78
00:16:43,669 --> 00:16:44,848
How does this work?

79
00:16:44,973 --> 00:16:47,862
- There, you press that one...
- You're on the air.

80
00:16:48,530 --> 00:16:50,281
<i>I think that lady should resign.</i>

81
00:16:50,406 --> 00:16:52,365
Good, thank you.
Next caller.

82
00:16:52,869 --> 00:16:55,377
<i>You should resign
and repay your salary.</i>

83
00:16:55,502 --> 00:16:56,839
Two for resign.

84
00:16:56,964 --> 00:16:58,344
Thank you, next caller.

85
00:16:58,469 --> 00:17:01,478
<i>I just want to say that I love the zoo
and the zoo is really fun.</i>

86
00:17:03,326 --> 00:17:05,560
Thank you.
That's really sweet.

87
00:17:05,722 --> 00:17:07,442
<i>And I think you should resign.</i>

1
00:17:10,465 --> 00:17:12,364
You look great.
You look amazing.

2
00:17:12,532 --> 00:17:14,934
Thanks.
You look fancy.

3
00:17:15,827 --> 00:17:17,062
Yeah. The monkey suit.

4
00:17:17,187 --> 00:17:19,287
It cost 3,000 bucks,
but totally worth it.

5
00:17:19,617 --> 00:17:20,667
I sold out.

6
00:17:21,460 --> 00:17:24,969
I got a boring office job in town,
so I gotta dress up.

7
00:17:25,094 --> 00:17:26,510
You know, the grind.

8
00:17:26,635 --> 00:17:29,422
But I'm really happy,
and I really feel like I've matured...

9
00:17:30,287 --> 00:17:31,142
a lot.

10
00:17:31,768 --> 00:17:33,040
Good, that's...

11
00:17:33,165 --> 00:17:34,701
I'm happy for you.

12
00:17:35,096 --> 00:17:37,455
I was sitting
in the cubicle the other day,

13
00:17:37,580 --> 00:17:38,922
doing some thinking,

14
00:17:39,047 --> 00:17:40,183
some growing...

15
00:17:40,351 --> 00:17:43,144
- And some maturing.
- Yes, maturing.

16
00:17:45,371 --> 00:17:47,315
Darn it if I don't just miss you.

17
00:17:48,805 --> 00:17:50,955
And... "a"-cakes, just let me...

18
00:17:51,993 --> 00:17:53,467
I love you, and...

19
00:17:54,499 --> 00:17:57,784
I'm sorry for the way I treated you.
I was the worst boyfriend ever.

20
00:17:57,952 --> 00:17:59,869
I know that.
And I...

21
00:18:00,311 --> 00:18:02,706
I think it would behoove us
to give it another shot.

22
00:18:03,903 --> 00:18:05,053
Yeah, look...

23
00:18:05,417 --> 00:18:07,560
I'm really happy
for you about your job

24
00:18:07,685 --> 00:18:10,059
and that you've learned
some new words,

25
00:18:10,184 --> 00:18:12,590
but I'm sure about my decision.

26
00:18:15,717 --> 00:18:17,364
OK. Say no more.

27
00:18:17,638 --> 00:18:19,888
Listen, hit me up on my cell phone.

28
00:18:20,013 --> 00:18:21,516
I'll be around.

29
00:18:21,999 --> 00:18:24,352
And if you wanna talk
or grab coffee,

30
00:18:24,520 --> 00:18:26,354
- or something like that, OK?
- OK.

31
00:18:26,522 --> 00:18:28,356
Gotta get back to the office.

32
00:18:28,797 --> 00:18:30,901
- Have a good one. Good seeing you.
- Bye.

33
00:18:45,082 --> 00:18:47,638
The hardest part
about living in this pit?

34
00:18:48,045 --> 00:18:50,495
It's probably keeping
my suit pressed.

35
00:18:54,420 --> 00:18:55,570
And the rats.

36
00:18:56,427 --> 00:18:59,095
It's like a freakin' rat parade
every night.

37
00:18:59,487 --> 00:19:02,948
I just want to be close to her house
because I need to protect her.

38
00:19:03,073 --> 00:19:06,436
'cause there are some weird people
that live around here.

39
00:19:06,604 --> 00:19:09,484
<i>If you let penguins get married,
where does it end?</i>

40
00:19:09,609 --> 00:19:12,177
<i>I mean, would you let me
marry my guinea pig?</i>

41
00:19:12,302 --> 00:19:16,071
<i>Because I want to marry my guinea pig.
I'll take my answer off the air.</i>

42
00:19:16,239 --> 00:19:19,708
- That's a valuable point.
- Attendance is up 30% at the zoo.

43
00:19:19,833 --> 00:19:21,042
You're welcome!

44
00:19:21,167 --> 00:19:24,454
And that penguin wedding was cute.
And I'm not gonna annul it.

45
00:19:24,622 --> 00:19:25,997
I'd ask you to reconsider.

46
00:19:26,165 --> 00:19:28,792
Well, I'd ask you to stop asking me.

47
00:19:29,038 --> 00:19:31,419
Because It's not gonna happen,
Marcia

48
00:19:31,587 --> 00:19:33,932
- Is that right?
- Last time I checked, I don't think...

49
00:19:34,057 --> 00:19:35,507
I murdered anyone,

50
00:19:35,675 --> 00:19:38,303
or had an affair, or did drugs!

51
00:19:38,551 --> 00:19:40,119
But I apologize.

52
00:19:40,244 --> 00:19:43,473
I apologize for having fun
and for making something cute.

53
00:19:46,142 --> 00:19:49,542
- Where are you from originally?
- What did I do?

54
00:19:51,384 --> 00:19:54,943
- So you will not be annulling...
- No, I will not be annulling.

55
00:19:59,953 --> 00:20:01,103
Look at them.

56
00:20:01,825 --> 00:20:03,887
They're just in their own little...

57
00:20:04,012 --> 00:20:05,512
penguin love bubble.

58
00:20:07,005 --> 00:20:08,191
That's what it's like

59
00:20:08,316 --> 00:20:11,089
when you meet your mate
and know you're gonna be together.

60
00:20:17,015 --> 00:20:19,519
You know what I realized?
We're just animals.

61
00:20:19,644 --> 00:20:21,654
We don't know anything about love.

62
00:20:23,055 --> 00:20:24,739
You should go with Mark.

63
00:20:25,705 --> 00:20:27,331
Really, I thought about it.

64
00:20:27,456 --> 00:20:30,854
Look, he might not be my gay penguin,
but he could be yours.

65
00:20:31,216 --> 00:20:34,149
Look, I told you, I'm not
going on a date with him.

66
00:20:34,317 --> 00:20:37,821
If you don't wanna go out with him,
don't. But don't do it because of me.

67
00:20:37,946 --> 00:20:39,112
I'm fine with it.

68
00:20:40,171 --> 00:20:42,198
All that's important
is we're friends.

69
00:20:42,516 --> 00:20:44,264
- Me too.
- Good, great.

70
00:20:47,776 --> 00:20:51,171
Plus, I already called and told him
that you were dying to go out, have fun.

71
00:20:53,755 --> 00:20:56,488
So I transferred
the penguins to a zoo in Iowa.

72
00:20:56,756 --> 00:20:59,483
Gay marriage is legal there,
so hopefully they'll be happy.

73
00:20:59,692 --> 00:21:01,457
At least they'll be together.

74
00:21:06,420 --> 00:21:09,359
Look! Six Flags!
I should take them on a water slide.

75
00:21:09,798 --> 00:21:12,348
They might die.
But it would be so cute!

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
