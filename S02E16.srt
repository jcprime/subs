1
00:00:01,168 --> 00:00:04,086
If you look inside your bags,
you'll find a few things.

2
00:00:04,427 --> 00:00:07,590
A bouquet of handcrochet
flower pens...

3
00:00:08,774 --> 00:00:11,427
A mosaic portrait of each of you

4
00:00:11,595 --> 00:00:14,139
made from the crush bottles
of your favorite diet soda,

5
00:00:14,264 --> 00:00:16,514
and a personalized 5,000-word essay

6
00:00:16,652 --> 00:00:19,351
of why you're all so awesome.

7
00:00:19,728 --> 00:00:21,053
What's Galentine's day?

8
00:00:21,178 --> 00:00:23,397
It's only the best day of the year.

9
00:00:23,565 --> 00:00:24,982
Every february 13th,

10
00:00:25,150 --> 00:00:28,069
my lady friends and I leave our husbands
and our boyfriends at home,

11
00:00:28,237 --> 00:00:30,446
and we just come and kick it,
breakfast-style.

12
00:00:30,614 --> 00:00:32,331
Ladies celebrating ladies.

13
00:00:32,803 --> 00:00:35,428
It's like Lilith fair,
minus the angst.

14
00:00:36,025 --> 00:00:37,301
Plus frittatas.

15
00:00:37,426 --> 00:00:40,372
So to conclude our
Galentine's day breakfast,

16
00:00:41,009 --> 00:00:43,751
it is time
for the greatest story ever told.

17
00:00:45,086 --> 00:00:46,545
What's the story?

18
00:00:46,670 --> 00:00:48,797
It's the most romantic story ever.

19
00:00:48,922 --> 00:00:52,100
It makes The Notebook
look like Saw V.

20
00:00:52,817 --> 00:00:53,917
Go, mom, go.

21
00:00:54,263 --> 00:00:55,610
It was 1968.

22
00:00:55,735 --> 00:00:59,225
I was 18, and our family
went on a vacation to Bermuda.

23
00:00:59,800 --> 00:01:01,936
And on the first day,
I went for a swim

24
00:01:02,104 --> 00:01:03,254
in the ocean,

25
00:01:04,023 --> 00:01:07,984
and I got caught
in a very strong current.

26
00:01:09,054 --> 00:01:11,643
- You've heard this story before, right?
- Yes, but the drama.

27
00:01:12,239 --> 00:01:14,163
The next thing I felt

28
00:01:14,288 --> 00:01:17,827
were two very powerful arms

29
00:01:18,402 --> 00:01:20,246
whisking me to safety.

30
00:01:20,569 --> 00:01:22,373
And he looked down at me,
and he said,

31
00:01:24,376 --> 00:01:25,918
"Are you okay?"

32
00:01:26,086 --> 00:01:28,379
and I looked up at him, and I said,

33
00:01:28,677 --> 00:01:29,727
"I am now."

34
00:01:31,417 --> 00:01:33,978
So they sneak out to meet each other
and walk on the beach,

35
00:01:34,103 --> 00:01:36,259
and, two weeks later,
he asked her to marry him

36
00:01:36,384 --> 00:01:37,934
and move to Illinois.

37
00:01:38,059 --> 00:01:40,850
But her parents thought
she was too young, so she said no.

38
00:01:41,181 --> 00:01:43,894
And then they lost touch,
and, a few years later, she met my dad.

39
00:01:44,469 --> 00:01:47,348
I got to take lifeguard courses.
Those guys get all the action.

40
00:01:47,473 --> 00:01:49,385
- Am I right, Justin?
- We got to find this guy.

41
00:01:49,510 --> 00:01:51,777
Frank?
He's probably married. Or dead.

42
00:01:52,280 --> 00:01:54,238
What's the difference?
Am I right, Justin?

43
00:01:54,406 --> 00:01:56,217
At least let me try and find him.

44
00:01:56,342 --> 00:01:58,678
Imagine how much better
that story would be

45
00:01:58,803 --> 00:02:00,244
if we actually reunited them.

46
00:02:01,952 --> 00:02:04,261
I think Justin's right.
You should let him do this.

47
00:02:04,386 --> 00:02:05,486
Okay. Do it.

48
00:02:05,633 --> 00:02:07,332
But if you find him and he's weird,

49
00:02:07,457 --> 00:02:09,186
like a ventriloquist or a puppeteer

50
00:02:09,311 --> 00:02:11,761
or anyone who pretends toys
are people,

51
00:02:12,079 --> 00:02:13,257
then abort the mission.

52
00:02:13,425 --> 00:02:14,475
Absolutely.

53
00:02:17,512 --> 00:02:19,096
- Feygnasse Team -

54
00:02:19,264 --> 00:02:20,750
.:: La Fabrique ::.

55
00:02:21,903 --> 00:02:23,803
Episode 216
Galentine's Day

56
00:02:24,880 --> 00:02:26,054
mpm

57
00:02:26,692 --> 00:02:27,897
lestat78

58
00:02:35,213 --> 00:02:37,406
Transcript: www.addic7ed.com

59
00:02:37,823 --> 00:02:39,909
You.
You're the best girlfriend ever.

60
00:02:41,055 --> 00:02:42,825
Happy valentine's day.

61
00:02:43,205 --> 00:02:44,891
I knew, eventually, somehow,

62
00:02:45,016 --> 00:02:47,374
being in a relationship with you
would totally pay off.

63
00:02:47,813 --> 00:02:49,794
I bought him some actual towels.

64
00:02:49,962 --> 00:02:51,512
He was using a bathrobe.

65
00:02:51,637 --> 00:02:54,366
And I bought him some other things
that humans usually use,

66
00:02:54,491 --> 00:02:55,641
like shampoo.

67
00:02:55,968 --> 00:02:58,295
Troops, gather around.
Great news.

68
00:02:58,420 --> 00:03:01,931
The senior center valentine's dance
is tomorrow from 5:00 to 9:00.

69
00:03:02,099 --> 00:03:03,767
Are we talking a.m. or p.m.?

70
00:03:03,892 --> 00:03:06,492
Those people are old!
Am I right, Justin?

71
00:03:07,419 --> 00:03:09,105
Justin's not here.

72
00:03:09,273 --> 00:03:12,274
It isn't just a job, gang.
We'll learn a lot from these seniors.

73
00:03:12,802 --> 00:03:16,142
Some of them
have been married for half a century.

74
00:03:16,698 --> 00:03:19,490
And, no offense, but everybody here
is terrible at love.

75
00:03:19,658 --> 00:03:20,658
Divorced,

76
00:03:20,965 --> 00:03:22,284
dating a gay guy,

77
00:03:22,517 --> 00:03:23,767
divorced twice,

78
00:03:24,155 --> 00:03:25,904
jury's still out on you two.

79
00:03:26,227 --> 00:03:27,607
And, Jerry, who knows?

80
00:03:27,732 --> 00:03:31,585
I've been happily married for 28 years.
You've met my wife Gayle many times.

81
00:03:31,753 --> 00:03:35,047
Whatever. Finally, Ann and
Mark are our special volunteers,

82
00:03:35,408 --> 00:03:37,508
and they will be available
whenever needed.

83
00:03:37,836 --> 00:03:39,051
I didn't volunteer.

84
00:03:39,219 --> 00:03:40,678
Yeah, too bad. You got drafted.

85
00:03:41,076 --> 00:03:44,805
Ask not what
your old people could do for you.

86
00:03:44,930 --> 00:03:47,059
Ask what you could do
for your old people.

87
00:03:47,634 --> 00:03:50,062
- Terminator.
- What? No. JFK.

88
00:03:50,795 --> 00:03:53,038
This meeting has been terminated.

89
00:03:53,163 --> 00:03:54,541
That is the Terminator.

90
00:03:54,666 --> 00:03:55,666
Nice.

91
00:03:56,779 --> 00:03:58,129
Where am I going?

92
00:04:00,488 --> 00:04:03,503
- Well, hello there.
- Well, hey. I got your message.

93
00:04:03,628 --> 00:04:05,077
You wanted to talk?

94
00:04:05,408 --> 00:04:06,408
Yes, I do.

95
00:04:07,633 --> 00:04:09,032
Have a seat.

96
00:04:11,276 --> 00:04:13,419
- Some champagne?
- No, thanks.

97
00:04:13,736 --> 00:04:15,963
Come on. This is Armand de Brignac.

98
00:04:16,131 --> 00:04:19,383
- Jay-z drinks this.
- He doesn't have to perform surgery.

99
00:04:19,551 --> 00:04:21,719
You don't know Jay-z's schedule.
He's a renaissance man.

100
00:04:23,092 --> 00:04:24,096
Okay, fine.

101
00:04:24,400 --> 00:04:25,681
This is for you.

102
00:04:26,896 --> 00:04:28,759
It's almost valentine's day,

103
00:04:28,884 --> 00:04:31,729
so I thought it would be
a good day to tell you...

104
00:04:33,058 --> 00:04:35,649
Let me just stop you right there.

105
00:04:36,910 --> 00:04:37,910
Look...

106
00:04:42,388 --> 00:04:44,241
I just want you to know
I'm so grateful

107
00:04:44,409 --> 00:04:46,403
for everything you did for me.

108
00:04:46,828 --> 00:04:49,830
- But I only see us as friends.
- For now.

109
00:04:49,998 --> 00:04:53,364
But think about how much better
our friendship would be

110
00:04:53,489 --> 00:04:55,502
if we added doing it.

111
00:04:57,020 --> 00:05:01,024
Seriously. I know our marriage was fake,
but there's something between us,

112
00:05:01,281 --> 00:05:03,068
and we should give it
a shot for real.

113
00:05:03,193 --> 00:05:06,847
I just don't feel
that way about you.

114
00:05:08,671 --> 00:05:09,671
I'm sorry.

115
00:05:33,081 --> 00:05:34,583
So there's that one, I guess.

116
00:05:34,751 --> 00:05:36,710
You sound ready
for the dance tomorrow.

117
00:05:36,878 --> 00:05:38,879
I don't know
about these old fogey songs, Leslie.

118
00:05:39,141 --> 00:05:42,660
Why can't we just do our originals?
I just wrote a new song, okay.

119
00:05:42,785 --> 00:05:44,837
Bottom line: it's called sex hair.

120
00:05:45,012 --> 00:05:47,158
It's about how youknow
when someone's just had sex

121
00:05:47,283 --> 00:05:50,889
'cause of how their hair gets
matted up in the back. It's awesome.

122
00:05:51,137 --> 00:05:52,265
Think of it this way.

123
00:05:52,390 --> 00:05:55,594
These song are exactly like
the songs you usually play,

124
00:05:55,719 --> 00:05:59,608
except instead of modern rock,
they're jazzy standards from the '40s.

125
00:06:03,589 --> 00:06:06,156
- You got a point.
- Yeah. You're gonna do great.

126
00:06:06,431 --> 00:06:07,731
All right, guys.

127
00:06:08,417 --> 00:06:11,161
I've been here for a half an hour.
Can I get my shoeshine?

128
00:06:11,329 --> 00:06:14,164
Kyle, I'm gonna lose my **** on you
if you ask me one more time.

129
00:06:16,554 --> 00:06:17,654
I found him.

130
00:06:18,908 --> 00:06:20,208
Frank Beckerson.

131
00:06:21,119 --> 00:06:23,919
63, lives in a little town
called Bridgeport.

132
00:06:24,083 --> 00:06:25,083
Illinois.

133
00:06:25,479 --> 00:06:28,711
Divorced, no kids,
worked as a lifeguard when he was 20.

134
00:06:31,266 --> 00:06:34,560
I need a half a day off
for a secret mission of love.

135
00:06:35,347 --> 00:06:37,563
You're asking my permission
to take a nooner?

136
00:06:37,731 --> 00:06:39,119
Sure. I don't know.

137
00:06:39,244 --> 00:06:42,359
Maybe. Justin and I need to go
on a romantically inspired road trip.

138
00:06:42,527 --> 00:06:44,009
So it is a nooner.

139
00:06:44,134 --> 00:06:46,986
We're planning to leave around noon,
so I'm not quite sure...

140
00:06:47,111 --> 00:06:48,282
It's not a nooner.

141
00:06:48,604 --> 00:06:50,532
I tracked down
this old flame of Leslie's mom.

142
00:06:50,657 --> 00:06:53,742
We're gonna go pick him up,
and reunite them on valentine's day.

143
00:06:54,372 --> 00:06:55,831
A great story or what?

144
00:06:56,406 --> 00:06:58,039
Yeah, great story indeed.

145
00:06:58,164 --> 00:06:59,914
Enjoy your half day off.

146
00:07:00,616 --> 00:07:04,006
- Pick you up tomorrow around noon.
- For our nooner, which is a cute word.

147
00:07:04,263 --> 00:07:07,009
- Explain it to her later.
- Explain what?

148
00:07:07,795 --> 00:07:09,395
Happy valentine's day.

149
00:07:09,920 --> 00:07:12,329
First off, a bear in a paper bag

150
00:07:12,454 --> 00:07:14,433
and a heart-shaped balloon.

151
00:07:14,665 --> 00:07:16,456
I love bears and bags.

152
00:07:16,949 --> 00:07:18,727
Well, you'll like this then,

153
00:07:18,852 --> 00:07:20,983
because this is a giant teddy bear

154
00:07:21,108 --> 00:07:22,858
holding a red heart.

155
00:07:23,887 --> 00:07:25,569
Festive.
What's this?

156
00:07:25,737 --> 00:07:28,072
I don't know.
It's a heart-shaped box of chocolates.

157
00:07:28,390 --> 00:07:31,489
That's a dozen red roses
in a heart-shaped arrangement.

158
00:07:31,614 --> 00:07:33,575
See that. Perfume.

159
00:07:34,127 --> 00:07:35,317
For the lady.

160
00:07:36,164 --> 00:07:38,729
Yearning, by Dennis Feinstein.

161
00:07:43,288 --> 00:07:45,798
Finally, the heart of the ocean.

162
00:07:46,841 --> 00:07:48,176
Gorgeous.

163
00:07:50,929 --> 00:07:52,710
I never had a chance to get a girl

164
00:07:52,835 --> 00:07:55,793
a cliched valentine's day
gift before,

165
00:07:56,267 --> 00:07:57,684
so I got you all of 'em.

166
00:08:00,168 --> 00:08:01,939
Thank you. That was very sweet.

167
00:08:02,542 --> 00:08:05,440
Do you think they'll get married?
My God. What if they get married?

168
00:08:05,565 --> 00:08:08,904
- That would be amazing.
- Would I call him dad then?

169
00:08:09,422 --> 00:08:13,292
No, that'd be too much.
Maybe I'll call him pappy. Pop.

170
00:08:13,417 --> 00:08:15,114
Poppy. Papa.

171
00:08:16,067 --> 00:08:18,080
Look at me,
I'm getting ahead of myself.

172
00:08:18,248 --> 00:08:19,649
You never know.

173
00:08:20,452 --> 00:08:22,302
I'm gonna call him poo-paw.

174
00:08:22,881 --> 00:08:25,546
How often do you get
to reunite soul mates?

175
00:08:25,714 --> 00:08:28,557
What if I told you that you
could reunite romeo and juliet?

176
00:08:28,682 --> 00:08:31,138
Or Brad Pitt and Jennifer Aniston?

177
00:08:32,876 --> 00:08:35,284
Jen, I really want you to be happy.

178
00:08:37,260 --> 00:08:38,976
Stay away from John Mayer.

179
00:08:40,254 --> 00:08:42,360
I cannot believe I'm gonna
meet Frank Beckerson.

180
00:08:42,485 --> 00:08:44,639
I feel like
I've known him my whole life.

181
00:08:48,091 --> 00:08:49,182
My God!

182
00:08:49,988 --> 00:08:52,438
- Marlene.
- No, poo-paw. Poo-paw, no.

183
00:08:52,825 --> 00:08:53,949
I'm not marlene.

184
00:08:56,014 --> 00:08:58,169
I'm so sorry.
I should really wear my glasses.

185
00:08:58,294 --> 00:08:59,344
Come on in.

186
00:09:04,961 --> 00:09:08,005
I was wondering, my mom always said
that you wanted to be a lawyer.

187
00:09:08,173 --> 00:09:10,671
- Did that work out?
- No. Never did.

188
00:09:10,796 --> 00:09:12,885
Though I did once act
as my own lawyer.

189
00:09:14,346 --> 00:09:17,014
You know what,
I found some pictures of Marlene and me.

190
00:09:18,308 --> 00:09:19,308
Look.

191
00:09:21,102 --> 00:09:22,699
Look how young she is.

192
00:09:22,901 --> 00:09:26,451
- These are amazing.
- Probably shouldn't show you this one.

193
00:09:28,538 --> 00:09:29,738
No, thank you.

194
00:09:30,612 --> 00:09:32,519
You mom was such a firecracker.

195
00:09:32,644 --> 00:09:36,158
Smart, funny, sharp as a tack.
Such firm breasts.

196
00:09:38,139 --> 00:09:39,775
That was inappropriate.

197
00:09:41,011 --> 00:09:43,196
I'm just, all of a sudden,

198
00:09:43,321 --> 00:09:45,584
so nervous to see her again.

199
00:09:47,000 --> 00:09:49,379
I'm gonna throw up real quick,
and then we can leave.

200
00:09:53,343 --> 00:09:56,428
Valentine's bash at the Bulge tonight.
We can get you a drink bracelet.

201
00:09:56,860 --> 00:09:58,178
I can't. I've got to work

202
00:09:58,303 --> 00:10:00,891
at this senior center
valentine's day dance thing.

203
00:10:01,059 --> 00:10:03,101
That sounds amazing. Can we come?

204
00:10:03,660 --> 00:10:05,312
I don't know why you would want to.

205
00:10:05,722 --> 00:10:08,440
- Because old people are funny.
- It'll be like <i>The Golden Girls</i>.

206
00:10:08,792 --> 00:10:10,442
I have to go get ready.

207
00:10:13,238 --> 00:10:15,280
So, where have you been
for the past 40 years?

208
00:10:16,232 --> 00:10:19,266
You know, here and there.
You know, Grenada for a while.

209
00:10:19,391 --> 00:10:22,916
Then Panama. Then Afghanistan.

210
00:10:23,295 --> 00:10:25,082
- So you were in the military?
- Nope.

211
00:10:25,530 --> 00:10:27,980
You know,
I always wonder how different

212
00:10:28,253 --> 00:10:31,011
my life would have been
if your mother had married me.

213
00:10:31,136 --> 00:10:33,966
It would have been better.
I'll tell you that much.

214
00:10:37,178 --> 00:10:40,038
- Frank, are you okay?
- I'm fine. I'm good...

215
00:10:42,236 --> 00:10:45,269
I am just so nervous to see her.

216
00:10:45,437 --> 00:10:49,146
- There is nothing to be nervous about.
- Have you got a bag or something?

217
00:10:49,315 --> 00:10:51,275
- Do you have a bag?
- I have a purse.

218
00:10:51,443 --> 00:10:53,599
- Give him your purse.
- All right.

219
00:11:15,861 --> 00:11:17,609
I mean, that sucked, didn't it?

220
00:11:17,734 --> 00:11:20,260
Maybe if you sang it
like Louis Armstrong?

221
00:11:20,385 --> 00:11:21,794
Maybe. Yeah.

222
00:11:21,919 --> 00:11:23,730
I mean, here's the thing, though.

223
00:11:24,172 --> 00:11:25,272
Who is that?

224
00:11:27,103 --> 00:11:29,434
My problem is I don't know

225
00:11:30,145 --> 00:11:31,804
how to tell if we're doing good

226
00:11:31,929 --> 00:11:34,657
because, when you play rock,
it's easy to know if you're doing great

227
00:11:34,782 --> 00:11:37,158
because chicks
will flash their boobs at you

228
00:11:37,611 --> 00:11:39,145
when you're up on stage.

229
00:11:39,270 --> 00:11:42,104
You're like, oh, that must
have sounded pretty good.

230
00:11:42,229 --> 00:11:45,067
But I can't... If that happens here,
my eyes will fall out of my head,

231
00:11:45,192 --> 00:11:46,342
and I'll die.

232
00:11:53,188 --> 00:11:54,718
Frank, are you okay?

233
00:11:58,763 --> 00:12:00,093
Maybe we should...

234
00:12:11,523 --> 00:12:14,197
I might be crazy,
but I have this weird suspicion

235
00:12:14,322 --> 00:12:16,616
that things are going well with us.

236
00:12:16,741 --> 00:12:19,613
You know,
I have that same suspicion.

237
00:12:21,340 --> 00:12:25,158
But, having never been in something
like this before, I need to ask,

238
00:12:25,950 --> 00:12:27,410
how am I doing?

239
00:12:27,950 --> 00:12:28,950
Come on.

240
00:12:29,327 --> 00:12:31,958
No, I'm sort of serious.

241
00:12:33,105 --> 00:12:34,955
All right, fine. Forget it.

242
00:12:37,328 --> 00:12:40,215
I'm just saying, you know,
it seems to me on paper

243
00:12:40,468 --> 00:12:44,096
- that this thing we've is pretty great.
- Yes, nerd, on paper,

244
00:12:44,264 --> 00:12:46,313
this thing we have is pretty great.

245
00:12:46,438 --> 00:12:47,808
Mark is a great boyfriend.

246
00:12:47,976 --> 00:12:49,899
I have no complaints at all.

247
00:12:50,024 --> 00:12:52,419
Everything is good.

248
00:12:56,865 --> 00:12:59,359
I don't know.
I don't feel right about this.

249
00:12:59,484 --> 00:13:01,984
Are you kidding me?
This is so much fun.

250
00:13:02,454 --> 00:13:04,731
Look, we took a road trip today.

251
00:13:05,279 --> 00:13:07,502
We chased your mom's
long-lost love down a freeway.

252
00:13:07,627 --> 00:13:08,894
He's a loon, Justin.

253
00:13:09,019 --> 00:13:11,553
So he freaked out a little bit.
He got nervous. You would too.

254
00:13:11,678 --> 00:13:13,171
And he was fine inside the car.

255
00:13:13,298 --> 00:13:16,776
Yeah, well, he cried himself to sleep.
I don't know why you're pushing.

256
00:13:16,901 --> 00:13:20,046
Why do you want him to spend time
with my mom? How would that help her?

257
00:13:20,171 --> 00:13:22,884
I don't think this is gonna work.
I'm calling this off.

258
00:13:23,052 --> 00:13:25,804
Come on. Look, we're here.
Just see what happens.

259
00:13:25,977 --> 00:13:28,340
- What happens is I drive him home.
- Now, you need to breathe.

260
00:13:28,465 --> 00:13:30,065
I'm breathing, okay...

261
00:13:39,216 --> 00:13:40,485
There you are.

262
00:13:40,658 --> 00:13:42,508
So what's the big surprise?

263
00:13:42,777 --> 00:13:44,823
You gonna try to check me
into an old folks' home?

264
00:13:45,396 --> 00:13:48,575
It's a long story. One that we'd love
to tell you over a cup of coffee

265
00:13:48,700 --> 00:13:50,700
somewhere far from this place.

266
00:14:00,551 --> 00:14:03,383
I can't believe it's really you.

267
00:14:04,935 --> 00:14:06,520
Want to catch up?

268
00:14:08,498 --> 00:14:09,498
Sure.

269
00:14:14,350 --> 00:14:16,409
- Oh, boy.
- Oh, boy.

270
00:14:18,514 --> 00:14:21,145
Do you think we should stand by...

271
00:14:22,153 --> 00:14:23,203
She's fine.

272
00:14:23,821 --> 00:14:25,671
Let's just let this unfold.

273
00:14:28,326 --> 00:14:31,569
Come here, sonny.
Let me tell you about the civil war.

274
00:14:31,694 --> 00:14:34,247
Grandpa, leave me alone.
You smell like death.

275
00:14:39,223 --> 00:14:40,973
I'm gonna get some punch.

276
00:14:46,631 --> 00:14:48,723
You guys are really adorable.

277
00:14:49,332 --> 00:14:50,782
That's really nice.

278
00:14:51,567 --> 00:14:54,394
So Leslie's like, "that's it.
I'm taking him home."

279
00:14:54,538 --> 00:14:56,946
So we look over,
and he's not even in the car anymore.

280
00:14:57,071 --> 00:14:59,947
- You're kidding.
- No. It's insane.

281
00:15:01,160 --> 00:15:03,318
What does Leslie
think about all this?

282
00:15:03,613 --> 00:15:07,375
Oh, God, Leslie, I don't even know.
I lost track of her in all the chaos.

283
00:15:07,500 --> 00:15:10,784
Anyway, so now he and Marlene
are off somewhere, doing God knows what.

284
00:15:11,681 --> 00:15:13,231
I don't want to know.

285
00:15:19,043 --> 00:15:20,710
You're suing me for alimony?

286
00:15:22,829 --> 00:15:24,987
When we were married,
I got accustomed

287
00:15:25,112 --> 00:15:28,510
to a certain lifestyle, and I'm entitled
to money to maintain that lifestyle.

288
00:15:28,832 --> 00:15:30,178
This is insane.

289
00:15:30,346 --> 00:15:32,150
Yeah, it's insane,
but it's all I got.

290
00:15:32,275 --> 00:15:35,600
And you could make it all go away
if you just gave me a chance.

291
00:15:35,895 --> 00:15:37,435
- What?
- Three dates.

292
00:15:39,699 --> 00:15:40,699
One date.

293
00:15:42,400 --> 00:15:43,233
Coffee?

294
00:15:43,519 --> 00:15:45,916
So your plan was to sue me

295
00:15:46,245 --> 00:15:49,489
and then to use that to blackmail me
into falling in love with you?

296
00:16:00,116 --> 00:16:01,619
You guys sound good.

297
00:16:02,092 --> 00:16:03,592
You really think so?

298
00:16:04,248 --> 00:16:07,315
It's impossible to tell.
It's like bizarro land out there.

299
00:16:07,440 --> 00:16:09,801
As soon as we finish a song,
it's dead silence.

300
00:16:09,969 --> 00:16:12,971
What do you expect, you know?
They're like a million years old.

301
00:16:13,400 --> 00:16:16,558
Well, I'm glad you like it.
You were a big supporter of the band.

302
00:16:17,342 --> 00:16:18,742
Hey, break's over.

303
00:16:18,943 --> 00:16:20,937
Ludgate, cracking the whip.

304
00:16:21,190 --> 00:16:24,134
Yeah, well, these old bags
paid for some entertainment,

305
00:16:24,259 --> 00:16:25,259
so get...

306
00:16:26,152 --> 00:16:27,488
- You're right.
- Up there.

307
00:16:27,613 --> 00:16:31,364
You should play <i>The Way
You Look Tonight</i>. That's a good one.

308
00:16:31,617 --> 00:16:34,545
<i>Next one's going out
to a special little lady</i>

309
00:16:34,670 --> 00:16:36,995
<i>named April Ludgate.</i>

310
00:16:47,468 --> 00:16:48,718
Are you guys...

311
00:16:49,259 --> 00:16:50,309
Never mind.

312
00:16:51,955 --> 00:16:53,955
- Why do you care?
- I don't.

313
00:17:00,978 --> 00:17:03,396
Now what about you?
What do you do for a living?

314
00:17:05,104 --> 00:17:07,361
Well, it's been an interesting ride.

315
00:17:07,903 --> 00:17:11,132
I got a job at a grocery store
right out of college.

316
00:17:11,379 --> 00:17:13,198
Just for a way to make money.

317
00:17:14,186 --> 00:17:16,409
Here I am, 40 years later,

318
00:17:16,656 --> 00:17:18,286
completely unemployed.

319
00:17:23,256 --> 00:17:24,656
What do you say...

320
00:17:25,459 --> 00:17:27,299
we pick up where we left off?

321
00:17:28,090 --> 00:17:29,923
You're not serious, right?

322
00:17:30,091 --> 00:17:31,891
Serious as a heart attack.

323
00:17:32,215 --> 00:17:33,718
Of which I've had four.

324
00:17:57,493 --> 00:18:01,371
- Are you okay? Did Frank leave?
- Yeah, he stepped away for a second.

325
00:18:01,539 --> 00:18:03,959
- I'm so sorry we brought him here.
- Oh, it's okay.

326
00:18:04,084 --> 00:18:05,875
It was very thoughtful, sweetheart.

327
00:18:06,043 --> 00:18:07,919
It's not your fault
that he turned out to be...

328
00:18:12,749 --> 00:18:14,759
<i>My name is Frank Beckerson.</i>

329
00:18:15,898 --> 00:18:18,403
<i>Marlene, you...</i>

330
00:18:20,214 --> 00:18:21,391
<i>blew it.</i>

331
00:18:22,609 --> 00:18:25,141
<i>Take one last look, Marlene,</i>

332
00:18:25,266 --> 00:18:29,270
<i>because you'll never see
this body again.</i>

333
00:18:31,447 --> 00:18:33,274
What happened between you two?

334
00:18:33,429 --> 00:18:35,280
Tell me everything.
I want every detail.

335
00:18:36,782 --> 00:18:38,646
We're almost done,
so we can leave soon.

336
00:18:38,947 --> 00:18:42,951
I'll take you for an ice cream malt,
and then we can go choose our caskets.

337
00:18:43,799 --> 00:18:47,292
Why does everything we do
must be cloaked in 15 layers of irony?

338
00:18:48,252 --> 00:18:51,755
Here's something unironic.
Since you know that meathead,

339
00:18:51,923 --> 00:18:53,923
you've become completely lame.

340
00:18:54,663 --> 00:18:55,925
We're breaking up.

341
00:18:57,643 --> 00:19:01,639
- You can't make out with me anymore.
- Fine. Then I'll make out with Ben.

342
00:19:02,086 --> 00:19:03,952
- Pass.
- No, he's my boyfriend.

343
00:19:04,077 --> 00:19:06,181
You can either make out
with both of us or none of us.

344
00:19:06,306 --> 00:19:08,256
- Fine. None of you.
- Fine.

345
00:19:22,546 --> 00:19:24,412
Everything okay, Knope?

346
00:19:25,231 --> 00:19:26,939
My boyfriend is a lawyer,

347
00:19:27,064 --> 00:19:28,970
and he's smart and interesting,

348
00:19:29,095 --> 00:19:31,622
and there's a lot
of things about him I like.

349
00:19:31,747 --> 00:19:34,047
But he acted like a real jerk today.

350
00:19:34,559 --> 00:19:36,758
I don't know.
There's something about the way

351
00:19:36,926 --> 00:19:39,135
he treats people or something.

352
00:19:40,008 --> 00:19:41,221
He's a tourist.

353
00:19:41,852 --> 00:19:44,183
He vacations in people's lives,

354
00:19:44,308 --> 00:19:47,683
takes pictures,
puts 'em in a scrapbook, and moves on.

355
00:19:48,271 --> 00:19:50,438
All he's interested in are stories.

356
00:19:53,499 --> 00:19:56,092
Basically, Leslie, he's selfish.

357
00:19:57,323 --> 00:20:00,282
And you're not.
And that's why you don't like him.

358
00:20:02,572 --> 00:20:05,245
I told you so.
It's Duke Silver.

359
00:20:06,020 --> 00:20:08,081
Duke, can I have your autograph?

360
00:20:08,249 --> 00:20:09,649
I love your music.

361
00:20:10,545 --> 00:20:13,169
You're mistaken, ladies.
Move along.

362
00:20:20,679 --> 00:20:22,137
Yeah, go Mouse Rat!

363
00:20:25,658 --> 00:20:27,517
I thought you were just terrific.

364
00:20:27,770 --> 00:20:28,810
Seriously?

365
00:20:28,978 --> 00:20:30,520
You sound like Dean Martin.

366
00:20:31,373 --> 00:20:34,152
If I were 50 years younger...

367
00:20:37,226 --> 00:20:38,778
Wait, who's Dean Martin?

368
00:20:38,946 --> 00:20:42,249
If I'm not mistaken, that was
the old lady version of flashing.

369
00:20:42,849 --> 00:20:44,159
Nailed the gig.

370
00:20:44,717 --> 00:20:46,367
Look, it's sad. I know.

371
00:20:46,878 --> 00:20:48,496
But we'll still be friends.

372
00:20:48,854 --> 00:20:50,727
We're gonna see
each other all the time.

373
00:20:51,083 --> 00:20:54,365
- I come into town every other weekend.
- No. You guys can't break up.

374
00:20:54,490 --> 00:20:56,754
- We can fix this. Let me talk to her.
- Tom, it's over.

375
00:20:57,066 --> 00:21:00,141
- She doesn't want to see me anymore.
- Is it my fault? Did I do something?

376
00:21:01,311 --> 00:21:03,770
Seriously,
this has nothing to do with you.

377
00:21:04,086 --> 00:21:06,556
- Can we still go suit shopping?
- Of course.

378
00:21:06,839 --> 00:21:08,433
Armani's having a sale right now.

379
00:21:09,408 --> 00:21:11,728
- Time to get you a pocket square.
- All right.

380
00:21:13,514 --> 00:21:15,398
www.sous-titres.eu

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
