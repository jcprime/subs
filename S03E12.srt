﻿1
00:00:01,320 --> 00:00:02,988
Good morning, Ron.

2
00:00:03,031 --> 00:00:05,499
Good morning.

3
00:00:05,566 --> 00:00:08,568
I know how much you enjoy
paperwork, but don't hover.

4
00:00:08,620 --> 00:00:10,120
Nice bench.
Is that new?

5
00:00:10,205 --> 00:00:12,806
No, that's been there
since the '90s.

6
00:00:12,874 --> 00:00:14,541
What are you doing
for your birthday on Friday?

7
00:00:14,592 --> 00:00:17,211
Nothing.
I never d...

8
00:00:17,262 --> 00:00:19,446
- Aha!
- Oh, God.

9
00:00:19,514 --> 00:00:21,215
♪ Birthday,
birthday, birthday ♪

10
00:00:21,282 --> 00:00:23,217
♪ it's your birthday ♪

11
00:00:23,284 --> 00:00:25,152
♪ it's your birthday,
and I know when it is ♪

12
00:00:25,220 --> 00:00:28,255
Ron refuses to tell anyone
when his birthday is.

13
00:00:28,323 --> 00:00:31,158
He's even had it redacted
on all government documents.

14
00:00:31,225 --> 00:00:34,077
Three years of investigations,
phone calls,

15
00:00:34,162 --> 00:00:36,230
Freedom of Information Act
requests,

16
00:00:36,297 --> 00:00:37,731
and still, I had nothing...

17
00:00:37,799 --> 00:00:42,369
Until a well-placed bribe to
a gentleman at Baskin-Robbins

18
00:00:42,437 --> 00:00:44,204
revealed...

19
00:00:44,272 --> 00:00:46,156
Ron's birthday is on Friday.

20
00:00:46,224 --> 00:00:48,025
Damn it.
I was so careful.

21
00:00:48,092 --> 00:00:49,176
Well, you blew it.

22
00:00:49,244 --> 00:00:51,428
All for a free scoop
of rum raisin.

23
00:00:51,513 --> 00:00:53,814
Was it worth it, Ron?
Was it?

24
00:00:53,882 --> 00:00:55,716
I command you to do nothing.

25
00:00:55,783 --> 00:00:57,718
I'm not gonna do nothing.
I'm gonna do something.

26
00:00:57,769 --> 00:00:58,936
And it's gonna be really big.

27
00:00:59,020 --> 00:01:00,888
I have a lot of years
to make up for.

28
00:01:00,939 --> 00:01:04,691
I don't like loud noises
and people making a fuss,

29
00:01:04,759 --> 00:01:07,327
and I especially
don't like people celebrating

30
00:01:07,395 --> 00:01:11,498
because they know a piece
of private information about me.

31
00:01:11,566 --> 00:01:13,634
Plus,
the whole thing is a scam.

32
00:01:13,701 --> 00:01:17,437
Birthdays were invented
by Hallmark to sell cards.

33
00:01:17,505 --> 00:01:27,440
<font color="#FF8C00">Sync & corrections by Alice</font>
<font color="##FF8C00">www.addic7ed.com</font>

34
00:01:38,760 --> 00:01:41,361
- They put up a fence.
- Who?

35
00:01:41,429 --> 00:01:42,846
No!

36
00:01:42,931 --> 00:01:44,765
- What's that?
- There's a small park

37
00:01:44,816 --> 00:01:46,833
on the line between Pawnee
and Eagleton, and...

38
00:01:46,901 --> 00:01:50,203
last night Eagleton put up
a fence around their side

39
00:01:50,271 --> 00:01:53,139
to keep us disgusting Pawnee
hobos off their precious land.

40
00:01:53,191 --> 00:01:54,641
There's even a security guard.

41
00:01:54,692 --> 00:01:56,877
You got to show
Eagleton I.D. to get in.

42
00:01:56,945 --> 00:01:58,545
Eagleton is
a bunch of rich snobs.

43
00:01:58,613 --> 00:02:00,981
And that's coming from someone
who has a Mercedes...

44
00:02:01,032 --> 00:02:04,151
With a Harman Kardon Logic 7
surround-sound system.

45
00:02:04,218 --> 00:02:06,420
Who builds a fence to keep
kids out of a playground?

46
00:02:06,487 --> 00:02:09,006
Three words...
Lindsay Carlisle Shay.

47
00:02:11,590 --> 00:02:12,492
Who?

48
00:02:12,543 --> 00:02:16,730
Lindsay Carlisle Shay and I
used to be best friends.

49
00:02:16,798 --> 00:02:19,166
We worked together
at the Pawnee Parks Department.

50
00:02:19,233 --> 00:02:21,268
Then she went to work
in Eagleton

51
00:02:21,336 --> 00:02:25,339
and "fixed her deviated septum"
and lost 35 pounds

52
00:02:25,406 --> 00:02:27,174
and lost something else.

53
00:02:27,241 --> 00:02:28,442
What was it again?

54
00:02:28,509 --> 00:02:30,177
Oh, yeah.
Her soul.

55
00:02:30,244 --> 00:02:31,979
Is the Eagleton side
really that much better

56
00:02:32,046 --> 00:02:34,681
- than the Pawnee side?
- To be fair, yeah.

57
00:02:34,732 --> 00:02:36,850
Our side is
this scrappy piece of land

58
00:02:36,918 --> 00:02:39,519
where kids go to smash
fluorescent light tubes.

59
00:02:39,570 --> 00:02:40,887
But it has a lot of heart.

60
00:02:40,955 --> 00:02:43,189
That's what people always say
when something sucks.

61
00:02:43,241 --> 00:02:44,791
We should stand up
for our town, okay?

62
00:02:44,859 --> 00:02:47,527
Pawneeans are just as good
as Eagletonians,

63
00:02:47,578 --> 00:02:49,997
although on average,
we are several inches shorter

64
00:02:50,064 --> 00:02:51,465
and 80 pounds heavier.

65
00:02:51,532 --> 00:02:54,301
People in Eagleton
are straight up mean to us.

66
00:02:54,369 --> 00:02:56,036
I would never set foot
over there.

67
00:02:56,104 --> 00:02:58,205
But it's the only place
that I can get

68
00:02:58,272 --> 00:02:59,873
my bumble and bumble
hair care products,

69
00:02:59,924 --> 00:03:01,742
so I'm there every eight days.

70
00:03:01,809 --> 00:03:04,227
Okay, well, I am
on "Operation No More Fence,"

71
00:03:04,312 --> 00:03:06,563
so I'm putting you in charge
of "Operation Ron's Party...

72
00:03:06,648 --> 00:03:08,982
- colon... Shock And Awe."
- I am all over it.

73
00:03:09,050 --> 00:03:10,901
I just need you
to do what's on that list.

74
00:03:10,985 --> 00:03:14,221
Leslie has a lot of qualities
I find horrifying.

75
00:03:14,288 --> 00:03:18,759
But the worst one by far
is how thoughtful she can be.

76
00:03:18,826 --> 00:03:20,160
It's the only park
in our neighborhood.

77
00:03:20,228 --> 00:03:22,079
I mean, where are
my kids supposed to play...

78
00:03:22,163 --> 00:03:24,414
the rock quarry?
There's rocks in there.

79
00:03:24,499 --> 00:03:26,166
Why don't we just set fire
to the fence?

80
00:03:26,234 --> 00:03:27,918
You know, set it ablaze.

81
00:03:28,002 --> 00:03:29,569
That's arson.

82
00:03:29,620 --> 00:03:31,088
Well, let's leave that
up to the lawyers.

83
00:03:31,172 --> 00:03:32,673
The point is, it would work.

84
00:03:32,740 --> 00:03:36,093
Why don't we build a fence
around their fence?

85
00:03:36,177 --> 00:03:38,512
- Why?
- It would give us...

86
00:03:38,579 --> 00:03:39,746
Two fences,

87
00:03:39,797 --> 00:03:42,516
so if they needed to get
to their fence

88
00:03:42,583 --> 00:03:45,602
for maintenance and whatnot,
their pants might get caught.

89
00:03:45,687 --> 00:03:47,320
My son Joey
tried to scale that fence

90
00:03:47,388 --> 00:03:49,473
to play on the Eagleton side,

91
00:03:49,557 --> 00:03:51,358
and he fell and hurt his arm.

92
00:03:51,425 --> 00:03:53,960
You need to get those people
to tear that fence down.

93
00:03:54,028 --> 00:03:55,195
This woman's right.

94
00:03:55,262 --> 00:03:58,865
I promise you,
citizens of Pawnee

95
00:03:58,933 --> 00:04:00,701
and sweet, brave Joey,

96
00:04:00,768 --> 00:04:02,569
I will tear down that fence.

97
00:04:04,155 --> 00:04:06,123
Okay, I-I'm just
gonna suggest one more time

98
00:04:06,207 --> 00:04:07,374
that we burn it down.

99
00:04:07,441 --> 00:04:09,209
But whatever you guys think.

100
00:04:09,277 --> 00:04:10,711
Yeah. Great.

101
00:04:10,778 --> 00:04:12,079
Yes, hi.

102
00:04:12,146 --> 00:04:15,348
I have a question about your
inflatable saxophones.

103
00:04:15,416 --> 00:04:18,351
Do those come
in different sizes?

104
00:04:18,419 --> 00:04:20,120
I'm gonna need
about 40 dozen of those.

105
00:04:20,171 --> 00:04:23,289
Also, what about your
neon gangster fedora hats?

106
00:04:23,341 --> 00:04:24,825
Um...

107
00:04:24,892 --> 00:04:26,643
- That was rude.
- Whatever's going on here,

108
00:04:26,728 --> 00:04:27,794
stop it immediately.

109
00:04:27,845 --> 00:04:28,962
I was just talking
on the phone.

110
00:04:29,013 --> 00:04:30,731
- To whom?
- It was personal.

111
00:04:30,798 --> 00:04:32,399
I would never make
a work-related call.

112
00:04:32,466 --> 00:04:35,168
You know that.

113
00:04:36,571 --> 00:04:39,339
- Lindsay!
- Leslie Knope.

114
00:04:39,407 --> 00:04:40,473
Hi.

115
00:04:40,525 --> 00:04:42,743
Has it really been five years?

116
00:04:42,810 --> 00:04:45,746
It has.
You look amazing.

117
00:04:45,813 --> 00:04:47,781
Thank you. This place
hasn't changed a bit...

118
00:04:47,832 --> 00:04:50,083
still loveable, but grimy.

119
00:04:50,151 --> 00:04:51,668
Hello, there.
I'm Ben Wyatt.

120
00:04:51,753 --> 00:04:53,320
I'm assistant city manager

121
00:04:53,387 --> 00:04:55,005
hi, Dan.

122
00:04:55,089 --> 00:04:56,656
- Ben.
- It's not important.

123
00:04:56,707 --> 00:04:57,874
It won't come up again.

124
00:04:57,959 --> 00:05:00,510
Uh, this is Ann.
She is my best friend...

125
00:05:00,595 --> 00:05:01,845
Now.

126
00:05:01,929 --> 00:05:04,264
And, uh, she's a nurse,
and she works at a hospital.

127
00:05:04,332 --> 00:05:06,133
- Ooh.
- Nice to meet you.

128
00:05:06,184 --> 00:05:08,368
Oh, I'm sorry.
It's just, nursing...

129
00:05:08,436 --> 00:05:10,937
You must be so tired.
It's sad.

130
00:05:11,005 --> 00:05:14,307
It's really nice outside.
Shall we speak in my courtyard?

131
00:05:14,375 --> 00:05:16,676
Yeah.

132
00:05:16,744 --> 00:05:18,845
Well, thank you so much
for stopping by.

133
00:05:18,896 --> 00:05:20,680
I was passing through
Pawnee anyway.

134
00:05:20,731 --> 00:05:22,783
I do a lot
of charity work here.

135
00:05:22,850 --> 00:05:25,368
There's
only so much you can do.

136
00:05:25,453 --> 00:05:30,257
You know, I found a picture
of you from back in the day.

137
00:05:30,324 --> 00:05:32,292
Look me at that.

138
00:05:32,359 --> 00:05:33,794
Let me get a closer look.

139
00:05:35,696 --> 00:05:37,998
So let's talk
about Lafayette park.

140
00:05:38,049 --> 00:05:39,916
Oh, yes.
The fence.

141
00:05:40,001 --> 00:05:42,202
I'm only trying to protect
our children.

142
00:05:42,253 --> 00:05:44,070
Come on, Lindsay,
this isn't you.

143
00:05:44,138 --> 00:05:46,006
When we used to work here
together,

144
00:05:46,057 --> 00:05:48,175
we loved the fact
that parks were for everyone.

145
00:05:48,226 --> 00:05:49,542
That's what
makes them so great.

146
00:05:49,594 --> 00:05:50,844
You know what's really great?

147
00:05:50,895 --> 00:05:53,430
A private park
that's not for everyone.

148
00:05:53,514 --> 00:05:55,515
- Wowsers.
- What do you care?

149
00:05:55,583 --> 00:05:57,117
It's just a crappy little park.

150
00:05:57,185 --> 00:05:58,685
Well, someone once told me

151
00:05:58,753 --> 00:06:00,787
that there's no such thing
as small parks,

152
00:06:00,855 --> 00:06:02,772
- just small ideas.
- Mm.

153
00:06:02,857 --> 00:06:04,608
And that someone...

154
00:06:04,692 --> 00:06:06,610
Was this woman.

155
00:06:06,694 --> 00:06:08,111
You can take it if you want.

156
00:06:08,196 --> 00:06:10,113
I have many copies
and the negatives.

157
00:06:10,198 --> 00:06:11,615
And I have a jpeg.

158
00:06:11,699 --> 00:06:14,251
The fence stays up.

159
00:06:14,335 --> 00:06:15,802
- Lindsay, if we...
- Sweetie...

160
00:06:15,870 --> 00:06:17,570
And I mean this
in the nicest possible way,

161
00:06:17,622 --> 00:06:20,457
Pawnee is and always will be
a dirty, little nightmare

162
00:06:20,541 --> 00:06:22,626
from which
you will never wake up.

163
00:06:24,178 --> 00:06:27,681
But it was good seeing you.

164
00:06:27,748 --> 00:06:29,983
What?
I'm not asleep. I'm awake.

165
00:06:30,051 --> 00:06:32,469
I'm wide awake,
and I got my eyes on you.

166
00:06:32,553 --> 00:06:33,720
That's what I would've said

167
00:06:33,771 --> 00:06:36,973
if I had thought of it
in the moment.

168
00:06:37,058 --> 00:06:38,758
What did I say instead?

169
00:06:41,358 --> 00:06:43,758
Wow! This is where they have
their Public Forums?

170
00:06:43,807 --> 00:06:46,609
- Yeah, it's not that great.
- They had a valet.

171
00:06:46,676 --> 00:06:48,594
Yes, Eagleton
is nicer than Pawnee.

172
00:06:48,678 --> 00:06:50,712
And, yes, because
of their cupcake factory,

173
00:06:50,764 --> 00:06:52,214
the air always
smells like vanilla.

174
00:06:52,265 --> 00:06:53,882
Oh, yeah.

175
00:06:53,934 --> 00:06:55,651
- Wow.
- But...

176
00:06:55,719 --> 00:06:57,886
Their people are not inherently
better than our people.

177
00:06:57,938 --> 00:06:59,221
The only thing they beat us in

178
00:06:59,272 --> 00:07:01,223
is life expectancy,
beauty pageants,

179
00:07:01,274 --> 00:07:02,908
and average income...
who cares?

180
00:07:02,993 --> 00:07:04,226
Factoid alert...

181
00:07:04,294 --> 00:07:06,595
Eagleton was founded
by former Pawneeans.

182
00:07:06,913 --> 00:07:10,149
Pawnee was established
in May of 1817.

183
00:07:10,216 --> 00:07:11,483
And by July,

184
00:07:11,534 --> 00:07:14,153
finding the smell unpleasant
and the soil untenable,

185
00:07:14,220 --> 00:07:18,824
all the wealthy people
evacuated to Eagleton.

186
00:07:18,892 --> 00:07:20,259
Hello, Pawnee.

187
00:07:20,326 --> 00:07:22,928
Welcome to our public forum.

188
00:07:22,996 --> 00:07:25,164
Hi, I'm
Thomas Montgomery Haverford.

189
00:07:25,231 --> 00:07:27,566
- Okay.
- Well, nice outfit.

190
00:07:27,634 --> 00:07:29,051
What, did you just come
from the stables?

191
00:07:29,135 --> 00:07:30,335
Yes.

192
00:07:30,386 --> 00:07:32,237
- I was just at the stables.
- Oh.

193
00:07:32,305 --> 00:07:34,339
You look like
you've been working hard.

194
00:07:34,407 --> 00:07:36,275
You have
a million flyaways right now.

195
00:07:36,342 --> 00:07:38,610
Would you like to borrow
a mirror or a self-help book?

196
00:07:38,678 --> 00:07:39,611
I would not.

197
00:07:39,679 --> 00:07:40,979
I would like you to get off
your high horse,

198
00:07:41,047 --> 00:07:43,015
Lindsay... pun intended.

199
00:07:43,082 --> 00:07:45,984
You know if I had your job,
there would be no fence there.

200
00:07:46,052 --> 00:07:47,202
Well, you don't have my job,

201
00:07:47,287 --> 00:07:49,020
because you knew
you couldn't handle it.

202
00:07:49,072 --> 00:07:51,256
Oh, the forum is beginning.

203
00:07:51,324 --> 00:07:53,292
Uh, we've got a little bit
of Eagleton business,

204
00:07:53,359 --> 00:07:55,377
- and then I'll introduce you.
- Great.

205
00:07:55,461 --> 00:07:57,763
Look how pretty the people are.

206
00:07:57,831 --> 00:07:59,698
Hey, Ron...

207
00:07:59,749 --> 00:08:01,500
How's the street parking
at your house?

208
00:08:01,551 --> 00:08:03,569
- What?
- Can you handle, like, 20 cars

209
00:08:03,636 --> 00:08:05,037
or a double-Decker party bus?

210
00:08:05,104 --> 00:08:07,539
There is no street parking
at my house.

211
00:08:07,607 --> 00:08:09,942
My house
is not even on a street.

212
00:08:10,009 --> 00:08:14,713
Do you have space
for, like, a huge circus tent?

213
00:08:14,764 --> 00:08:16,215
Hey, Ron.
Have you seen...

214
00:08:16,282 --> 00:08:17,716
what the hell?

215
00:08:17,784 --> 00:08:19,768
No!

216
00:08:19,853 --> 00:08:21,053
Oh!

217
00:08:21,120 --> 00:08:22,654
Well, looks like
there wont be any balloons

218
00:08:22,722 --> 00:08:24,056
for the birthday boy.

219
00:08:24,123 --> 00:08:26,058
These were for a sick child
at the hospital.

220
00:08:26,125 --> 00:08:27,492
Ah.

221
00:08:27,560 --> 00:08:28,827
My office, now.

222
00:08:28,895 --> 00:08:31,063
- I don't work for you.
- Don't care.

223
00:08:31,130 --> 00:08:32,831
Decisions, decisions.

224
00:08:32,899 --> 00:08:34,466
Uh, I think I'm gonna go

225
00:08:34,534 --> 00:08:37,002
with the porcini mushrooms
and boursin, <i>s'il vous plait.</i>

226
00:08:37,069 --> 00:08:38,420
...that the Eagleton-Pawnee fence

227
00:08:38,504 --> 00:08:41,340
does a lot more harm
than it does good.

228
00:08:41,407 --> 00:08:45,244
Hello. I'm Bertram Rolands,
a citizen of Eagleton.

229
00:08:48,081 --> 00:08:49,848
With all due respect,
Ms. Knope,

230
00:08:49,916 --> 00:08:52,284
can't you just clean up
your side of the park,

231
00:08:52,352 --> 00:08:53,752
put some new equipment there?

232
00:08:53,803 --> 00:08:55,053
Well, we would love to do that,

233
00:08:55,104 --> 00:08:58,690
but, unfortunately,
money is tight right now.

234
00:08:58,758 --> 00:09:00,442
Hello.
I'm Christine Porter.

235
00:09:03,863 --> 00:09:06,815
I want to respectfully say
that I'm in favor of the fence.

236
00:09:06,900 --> 00:09:09,785
I see it as a kind
of punishment for Pawnee

237
00:09:09,869 --> 00:09:13,071
that might inspire your town
to clean up its act.

238
00:09:14,540 --> 00:09:16,775
Well, I would like
to respectfully say

239
00:09:16,826 --> 00:09:19,678
that any child should be able
to play in any park,

240
00:09:19,746 --> 00:09:23,148
regardless of wealth or status.

241
00:09:26,352 --> 00:09:29,638
Uh, anyway,
this is Joey Plunkett.

242
00:09:29,722 --> 00:09:31,423
Joey, wave to the audience.

243
00:09:31,474 --> 00:09:34,192
Oh, you can't,
because you broke your arm

244
00:09:34,260 --> 00:09:35,794
climbing that fence.

245
00:09:35,845 --> 00:09:38,180
- Uh, I can wave with this arm.
- No, you can't.

246
00:09:38,264 --> 00:09:40,399
Both your arms are broken.

247
00:09:40,466 --> 00:09:42,234
All due respect,
I recognize that boy.

248
00:09:42,302 --> 00:09:45,470
He was caught selling
fireworks to Eagleton kids.

249
00:09:45,521 --> 00:09:47,973
Well, with more due respect,

250
00:09:48,041 --> 00:09:49,691
a lot of boys do that
in our town.

251
00:09:49,776 --> 00:09:51,243
You cannot be sure
that he is the one.

252
00:09:51,311 --> 00:09:54,680
This is a perfect example
of how we're going to help you.

253
00:09:54,747 --> 00:09:57,082
Here's to you, Joey,
and your mother

254
00:09:57,150 --> 00:10:00,886
and to every Pawnee citizen
who might have a bright future

255
00:10:00,954 --> 00:10:04,039
if they fundamentally change
everything about themselves.

256
00:10:04,123 --> 00:10:06,758
- Thanks to Leslie Knope.
- I'm not done.

257
00:10:06,826 --> 00:10:08,877
Isn't she trying her hardest?

258
00:10:10,930 --> 00:10:14,683
So cute and so good.

259
00:10:14,767 --> 00:10:16,852
I'm only gonna ask you
this once.

260
00:10:16,936 --> 00:10:18,537
What is going on
with my birthday?

261
00:10:18,604 --> 00:10:20,639
Oh, my God.
Ron, it's your birthday.

262
00:10:20,690 --> 00:10:22,941
- Happy Birthday.
- Shut your damn mouth.

263
00:10:23,009 --> 00:10:24,576
This is a fun conversation.

264
00:10:24,644 --> 00:10:26,678
Just tell me
what Leslie is planning.

265
00:10:26,729 --> 00:10:29,748
Honestly, I-I don't know.
I haven't heard anything.

266
00:10:29,816 --> 00:10:32,284
Why don't you tell me what
she did for your last birthday?

267
00:10:32,351 --> 00:10:34,236
Oh, well, that was intense.

268
00:10:34,320 --> 00:10:35,854
She totally surprised me.

269
00:10:35,922 --> 00:10:37,372
She kidnapped me from work,

270
00:10:37,457 --> 00:10:39,391
and then she took me
to that place, <i>Senor Vega's</i>,

271
00:10:39,459 --> 00:10:41,093
you know, where
the mariachi band comes out.

272
00:10:41,160 --> 00:10:42,728
They put a big sombrero on you,

273
00:10:42,795 --> 00:10:44,096
and then everybody sings
Happy Birthday?

274
00:10:44,163 --> 00:10:45,230
Damn it.

275
00:10:45,298 --> 00:10:46,565
And then we went
back to my house,

276
00:10:46,632 --> 00:10:49,267
and she invited
basically everyone I knew,

277
00:10:49,335 --> 00:10:51,903
and she had this great guy
doing face painting,

278
00:10:51,971 --> 00:10:54,389
and I had my face painted
like a fairy tiger.

279
00:10:55,875 --> 00:10:57,559
Also, she did it, like,
a week before my birthday,

280
00:10:57,643 --> 00:11:00,145
which is genius, 'cause
I had no idea it was coming.

281
00:11:00,212 --> 00:11:02,614
And then there was
a bouncy castle.

282
00:11:02,682 --> 00:11:04,349
Did you know
they made those for adults?

283
00:11:04,417 --> 00:11:05,650
Mm-hmm.

284
00:11:05,718 --> 00:11:07,736
Maybe I'll change it up
this time.

285
00:11:07,820 --> 00:11:09,287
Can I try
the andouille sausage?

286
00:11:09,355 --> 00:11:11,990
- Tom, let's go.
- What? No.

287
00:11:12,058 --> 00:11:14,593
Hey, not you too.

288
00:11:14,660 --> 00:11:16,361
It's gross.
I don't like it.

289
00:11:16,412 --> 00:11:18,497
It's disgusting.

290
00:11:18,564 --> 00:11:20,449
Did you guys get
your public forum gift bag?

291
00:11:20,516 --> 00:11:22,134
There's an iPod touch in here.

292
00:11:22,201 --> 00:11:23,869
Man, she used
to not be like this.

293
00:11:23,920 --> 00:11:25,370
Eagleton really changed her.

294
00:11:25,421 --> 00:11:27,773
Yeah, what exactly happened
between the two of you, anyway?

295
00:11:27,840 --> 00:11:30,625
Five years ago,
Eagleton offered me that job.

296
00:11:30,710 --> 00:11:33,178
And you said no?
Are you insane?

297
00:11:33,245 --> 00:11:35,263
I talked it over with Lindsay,
and we made a pact

298
00:11:35,348 --> 00:11:37,382
that we would stay
in Pawnee together

299
00:11:37,433 --> 00:11:40,218
and work hard
and fight to make Pawnee

300
00:11:40,269 --> 00:11:41,887
a better place to live.

301
00:11:41,938 --> 00:11:43,755
And then
they offered her the job,

302
00:11:43,823 --> 00:11:46,224
and she took it
and disappeared.

303
00:11:46,275 --> 00:11:47,309
Wow.

304
00:11:47,393 --> 00:11:51,262
Ooh, verbena-scented
soy candles!

305
00:11:51,314 --> 00:11:53,865
You want me to do what, now?

306
00:11:53,933 --> 00:11:56,201
Send Leslie somewhere
on an assignment,

307
00:11:56,269 --> 00:11:57,636
a conference or something,

308
00:11:57,703 --> 00:12:00,939
and make her take April
and freeze their bank accounts.

309
00:12:01,007 --> 00:12:01,940
I don't understand.

310
00:12:02,008 --> 00:12:04,042
Is Leslie's work
unsatisfactory?

311
00:12:04,110 --> 00:12:05,977
No, it has nothing to do
with her work.

312
00:12:06,045 --> 00:12:08,213
I don't want to get into it.

313
00:12:08,281 --> 00:12:10,799
Ron, look, this is me, okay?

314
00:12:10,883 --> 00:12:13,218
You know you can talk to me
about anything.

315
00:12:13,285 --> 00:12:17,022
Well, it's... it's
my birthday on Friday.

316
00:12:17,090 --> 00:12:19,290
Hey!
Happy Birthday.

317
00:12:22,595 --> 00:12:23,962
I'm ending this right now.

318
00:12:24,013 --> 00:12:27,365
I'm just gonna leave early
and go home...

319
00:12:27,433 --> 00:12:29,367
Unless...

320
00:12:29,435 --> 00:12:32,971
That's exactly
what she wants me to do.

321
00:12:33,039 --> 00:12:35,073
All right,
everybody just grab a bag

322
00:12:35,141 --> 00:12:36,641
and open it up

323
00:12:36,692 --> 00:12:38,944
and then try to find
some dirty stuff in there.

324
00:12:38,995 --> 00:12:40,028
What are you doing?

325
00:12:40,113 --> 00:12:41,363
Eagleton treats us
like garbage.

326
00:12:41,447 --> 00:12:42,781
We're gonna treat them
like garbage.

327
00:12:42,832 --> 00:12:44,416
We're gonna take
these bags of trash,

328
00:12:44,483 --> 00:12:46,201
and we're gonna throw it over
to their side of the park.

329
00:12:46,285 --> 00:12:47,669
And then we're just gonna let
the stink

330
00:12:47,753 --> 00:12:49,320
and the raccoons run wild.

331
00:12:49,372 --> 00:12:50,622
Well, isn't that

332
00:12:50,690 --> 00:12:54,176
just playing right
into what they think of you?

333
00:12:54,260 --> 00:12:56,494
Oh, my God.
What am I thinking?

334
00:12:56,546 --> 00:12:57,829
Let's just stop this.

335
00:12:57,880 --> 00:12:59,264
Put everything
back in the truck.

336
00:12:59,331 --> 00:13:00,999
Let's back it up.
No, no, no, no.

337
00:13:01,067 --> 00:13:03,034
Leslie, you promised we could
throw garbage everywhere.

338
00:13:03,102 --> 00:13:04,553
Wow.

339
00:13:04,637 --> 00:13:07,556
I didn't expect to see
the whole Parks Department here,

340
00:13:07,640 --> 00:13:08,857
although it is, sadly,

341
00:13:08,941 --> 00:13:10,942
one of the nicest spots
in town.

342
00:13:11,010 --> 00:13:12,978
I am so sick of this, Lindsay.

343
00:13:13,045 --> 00:13:15,197
Wait, Leslie.
I've got this.

344
00:13:15,281 --> 00:13:18,450
You listen to me,
Lindsay Carlisle Shay.

345
00:13:18,517 --> 00:13:20,185
Why don't you take
your fancy dog,

346
00:13:20,236 --> 00:13:21,586
get in your escalade?

347
00:13:21,654 --> 00:13:23,521
And if you have
any job openings,

348
00:13:23,573 --> 00:13:24,823
maybe you should let me
know about 'em.

349
00:13:24,874 --> 00:13:26,524
- Come on, man.
- No!

350
00:13:26,576 --> 00:13:28,426
I'm sick of being treated

351
00:13:28,494 --> 00:13:32,363
like I'm not willing to relocate
to Eagleton, because I am!

352
00:13:32,415 --> 00:13:35,217
So here's
what you could do, lady.

353
00:13:35,301 --> 00:13:37,368
Take this resume

354
00:13:37,420 --> 00:13:39,537
and shove it
into your human resources slot.

355
00:13:41,557 --> 00:13:43,892
Oh, yeah!
Shove it there!

356
00:13:45,344 --> 00:13:49,431
You might have a fancy car
and a mahogany purse,

357
00:13:49,515 --> 00:13:50,899
or whatever rich people have,

358
00:13:50,983 --> 00:13:53,268
but I remember something
that you're trying to forget.

359
00:13:53,352 --> 00:13:55,220
- You're a Pawnee girl.
- Mm-hmm.

360
00:13:55,271 --> 00:13:57,656
No, Leslie, I'm not.

361
00:13:57,723 --> 00:13:59,524
Well, then why do you
come here at dinnertime

362
00:13:59,575 --> 00:14:01,743
and get takeout
from the legendary JJ's Diner?

363
00:14:01,827 --> 00:14:03,695
It's not for me.

364
00:14:03,746 --> 00:14:06,665
These waffles make
great dog laxatives.

365
00:14:06,732 --> 00:14:08,833
Don't you dare

366
00:14:08,901 --> 00:14:11,369
feed that waffle to that dog
to get it to poop.

367
00:14:13,239 --> 00:14:14,539
Sambuca need to make?

368
00:14:14,590 --> 00:14:15,924
There you go.

369
00:14:16,008 --> 00:14:18,043
- Leslie.
- How dare you?

370
00:14:18,110 --> 00:14:19,511
- Get her!
- Get her, Leslie!

371
00:14:19,578 --> 00:14:21,379
- Get off of me!
- Get her!

372
00:14:23,082 --> 00:14:25,150
Garbage fight!

373
00:14:29,218 --> 00:14:31,417
I want her arrested for
attempted murder.

374
00:14:31,780 --> 00:14:32,781
For God's sake, Knope.
Get a grip.

375
00:14:32,848 --> 00:14:34,799
Thank God you're here!

376
00:14:34,884 --> 00:14:36,451
I want her arrested!

377
00:14:36,519 --> 00:14:37,836
She attacked me,

378
00:14:37,920 --> 00:14:40,255
and then she hurled my
Kate Spade headband somewhere.

379
00:14:40,306 --> 00:14:42,173
I think my Eagleton
colleagues would agree

380
00:14:42,241 --> 00:14:43,791
that we don't want to make
a federal case out of this,

381
00:14:43,843 --> 00:14:45,694
so I suggest you both apologize
to each other,

382
00:14:45,761 --> 00:14:47,762
and we pretend
this never happened.

383
00:14:47,813 --> 00:14:49,648
I will never apologize to her.

384
00:14:49,732 --> 00:14:51,700
- Nor I her.
- "Nor I her."

385
00:14:51,767 --> 00:14:54,903
I doth proclaim to be
a stupid fartface.

386
00:14:54,971 --> 00:14:57,322
Nice retort. Did G.B. Shaw
write that for you?

387
00:14:57,406 --> 00:14:59,157
Did G.B. Shaw write
your stupid fartface?

388
00:14:59,241 --> 00:15:01,159
Ladies, if you don't apologize,

389
00:15:01,243 --> 00:15:03,311
we're gonna have to toss
the both of you in jail.

390
00:15:03,379 --> 00:15:06,848
So just swallow your pride...

391
00:15:06,916 --> 00:15:09,217
Say you're sorry.

392
00:15:16,459 --> 00:15:17,842
We hope you enjoy
your evening here

393
00:15:17,927 --> 00:15:19,260
at the Eagleton holding cell.

394
00:15:19,578 --> 00:15:22,146
Can I offer you anything...
herbal tea, Greek yogurt?

395
00:15:22,214 --> 00:15:23,414
No.

396
00:15:27,152 --> 00:15:28,269
Morning, Ron.

397
00:15:28,354 --> 00:15:31,522
Ooh, dude, you forgot
to put a shirt on.

398
00:15:31,590 --> 00:15:32,824
I do it all the time.
It's fine.

399
00:15:32,891 --> 00:15:35,143
- I slept here.
- Sweet.

400
00:15:35,227 --> 00:15:38,763
So a little birdie told me
it's your birthday coming up.

401
00:15:38,831 --> 00:15:40,231
How about
a free birthday shoe shine?

402
00:15:40,282 --> 00:15:41,899
What did this
little birdie tell you

403
00:15:41,967 --> 00:15:44,135
is going to happen
for my birthday?

404
00:15:44,203 --> 00:15:46,271
Oh, nice try, Ron. You're not
getting anything out of me.

405
00:15:46,338 --> 00:15:47,839
Andrew, please.

406
00:15:47,906 --> 00:15:51,576
Ron, look,
I love you like a father

407
00:15:51,644 --> 00:15:53,578
who's not that much
older than me...

408
00:15:53,646 --> 00:15:54,779
like a young Uncle...

409
00:15:54,847 --> 00:15:57,048
or like, uh, you were
my camp counselor,

410
00:15:57,116 --> 00:15:58,916
but we're adults,
so we hang out,

411
00:15:58,968 --> 00:16:00,184
and it's not weird, you know.

412
00:16:00,252 --> 00:16:01,803
Or, actually,
here's what it is.

413
00:16:01,887 --> 00:16:03,988
You're my Lacrosse coach.
I get it. I get it.

414
00:16:04,056 --> 00:16:05,223
What's the point?

415
00:16:05,291 --> 00:16:07,292
Well, coach,

416
00:16:07,359 --> 00:16:09,677
Leslie swore me to secrecy,
so I can't say anything.

417
00:16:09,762 --> 00:16:11,679
I owe her so much.
I can't ruin it for her.

418
00:16:11,764 --> 00:16:15,433
- Well, I respect that.
- Mm-hmm.

419
00:16:15,501 --> 00:16:16,684
- See you later.
- Okay...

420
00:16:16,769 --> 00:16:19,604
Not if I see you first,
Uncle Ron.

421
00:16:19,672 --> 00:16:21,272
And I probably will,

422
00:16:21,323 --> 00:16:25,076
'cause Leslie assigned me
to the kidnap squad.

423
00:16:28,646 --> 00:16:29,681
Hey, jailbird.

424
00:16:29,748 --> 00:16:32,667
Beautiful Ann, thank you
for coming to get me.

425
00:16:32,751 --> 00:16:34,035
Oh, my God, are you kidding?

426
00:16:34,119 --> 00:16:35,653
Are you okay?

427
00:16:35,704 --> 00:16:39,340
It was a rough night,
but I survived.

428
00:16:39,425 --> 00:16:41,158
Scone?

429
00:16:41,210 --> 00:16:44,796
- They only have maple walnut.
- Yeah. Thank you.

430
00:16:44,863 --> 00:16:47,065
Why did you get arrested?
What did you do?

431
00:16:47,132 --> 00:16:49,834
The only thing I'm guilty of
is loving Pawnee...

432
00:16:49,902 --> 00:16:51,803
And punching Lindsay
in the face

433
00:16:51,870 --> 00:16:54,022
and shoving a coffee filter
down her pants.

434
00:16:54,106 --> 00:16:56,808
But in my defense, I believe
that assault should be legal

435
00:16:56,875 --> 00:16:58,142
if a person is a jerk.

436
00:16:58,210 --> 00:17:00,228
Why are you letting her
get to you like this?

437
00:17:00,312 --> 00:17:02,680
Because she's a stupid jerk...

438
00:17:02,748 --> 00:17:04,382
Because we were best friends

439
00:17:04,450 --> 00:17:06,651
and then she sold out
everything that we believed in.

440
00:17:06,719 --> 00:17:07,785
And the worst part?

441
00:17:07,853 --> 00:17:09,287
To her, our friendship...

442
00:17:09,355 --> 00:17:10,722
it's like it never happened.

443
00:17:10,789 --> 00:17:13,124
Well, first of all,
this color looks amazing on you.

444
00:17:13,192 --> 00:17:14,325
Thank you.

445
00:17:14,393 --> 00:17:16,060
Second of all,
the whole fence thing...

446
00:17:16,111 --> 00:17:18,129
she's obviously trying
to get a rise out of you.

447
00:17:18,197 --> 00:17:20,998
Third of all, she knows
she only got the job

448
00:17:21,066 --> 00:17:23,167
because you turned it down,
which must drive her nuts.

449
00:17:23,235 --> 00:17:26,254
But most importantly,
you say the word,

450
00:17:26,338 --> 00:17:29,474
and I will beat her senseless
with a baseball bat.

451
00:17:29,541 --> 00:17:31,642
Thank you.

452
00:17:31,710 --> 00:17:33,911
Wait a minute.

453
00:17:33,979 --> 00:17:35,763
Oh, my God. Okay, we have
a lot of work to do.

454
00:17:35,848 --> 00:17:38,082
Ann, put these scones
in your bra.

455
00:17:38,150 --> 00:17:39,267
- Let's go.
- Okay.

456
00:17:39,351 --> 00:17:41,219
Can we just stop off
at the, um, lobby?

457
00:17:41,270 --> 00:17:43,638
'Cause the prison gift bags
are amazing.

458
00:17:43,722 --> 00:17:45,189
Hey, batter, batter,
batter, batter, batter.

459
00:17:45,257 --> 00:17:46,858
Hey, batter, batter, batter,
batter, batter, batter, batter.

460
00:17:46,925 --> 00:17:47,975
Hey, batter, bah.

461
00:17:49,428 --> 00:17:51,996
Lucas with a crazy base hit!

462
00:17:52,064 --> 00:17:54,098
Dude, stop rooting
for the other team.

463
00:17:54,149 --> 00:17:56,451
You're just mad 'cause
they are lighting you up.

464
00:17:56,535 --> 00:17:58,669
- What's going on here?
- Hi, Lindsay.

465
00:17:58,737 --> 00:18:00,872
Introducing the Pawnee
wiffle ball league.

466
00:18:00,939 --> 00:18:02,407
It's an idea
that I came up with

467
00:18:02,458 --> 00:18:03,875
after my best friend, Ann,
over there

468
00:18:03,942 --> 00:18:05,827
said she wanted to bash your
head in with a baseball bat.

469
00:18:05,911 --> 00:18:07,995
And I want to thank you
for the fence.

470
00:18:08,080 --> 00:18:11,048
There's no way we could afford
that high-quality wood.

471
00:18:11,116 --> 00:18:12,583
What if someone
hits a home run?

472
00:18:12,634 --> 00:18:14,001
How will you get the ball back?

473
00:18:14,086 --> 00:18:16,187
One of the many
Eagleton kids who signed up

474
00:18:16,255 --> 00:18:19,841
will just pop over and grab it.

475
00:18:19,925 --> 00:18:21,342
Hey, what are you doing
tonight?

476
00:18:21,427 --> 00:18:23,177
- Why?
- I don't know.

477
00:18:23,262 --> 00:18:25,463
I thought maybe you could bust
out your old jazz sweatshirt,

478
00:18:25,514 --> 00:18:27,899
and you and I could go
to Sullivan's for a beer.

479
00:18:27,966 --> 00:18:30,184
Yeah, I do still have
that sweatshirt.

480
00:18:30,269 --> 00:18:31,502
I know you do.

481
00:18:31,570 --> 00:18:33,571
Foul ball!

482
00:18:33,638 --> 00:18:36,073
- You did all this in a day?
- Yeah.

483
00:18:36,141 --> 00:18:37,241
All right.

484
00:18:37,309 --> 00:18:38,693
I work with some
really great people.

485
00:18:41,663 --> 00:18:44,115
Good job, guys.
Great job.

486
00:18:45,417 --> 00:18:47,535
Hey, did you hear the news?

487
00:18:47,619 --> 00:18:50,455
Ben and Chris want us to go
into Conference Room "C"

488
00:18:50,522 --> 00:18:51,522
for a meeting.

489
00:18:51,590 --> 00:18:54,208
Let's get this over with.

490
00:19:02,167 --> 00:19:05,203
Happy Birthday, Ron.

491
00:19:05,270 --> 00:19:07,438
Ann said you had a big party...

492
00:19:07,506 --> 00:19:09,173
sombreros, karaoke.

493
00:19:09,241 --> 00:19:10,575
Yeah, I did that for Ann.

494
00:19:10,642 --> 00:19:13,511
Why would I throw Ron Swanson
an Ann Perkins party?

495
00:19:13,579 --> 00:19:16,680
What about the giant list
of things April was doing?

496
00:19:16,732 --> 00:19:18,950
That was just a list
of ways to mess with you.

497
00:19:19,017 --> 00:19:21,686
- She do 'em all?
- She did indeed.

498
00:19:23,322 --> 00:19:26,657
So I have rented
<i>Bridge on the River Kwai</i>

499
00:19:26,708 --> 00:19:27,792
and <i>The Dirty Dozen.</i>

500
00:19:29,044 --> 00:19:30,962
Artie from security
is outside the door,

501
00:19:31,029 --> 00:19:32,663
so no one will bother you.

502
00:19:32,731 --> 00:19:34,999
And a cab will be here
whenever you're ready

503
00:19:35,067 --> 00:19:36,334
to take you home.

504
00:19:38,203 --> 00:19:39,637
Thank you.

505
00:19:39,705 --> 00:19:42,673
Do you remember what you
said to me five years ago

506
00:19:42,741 --> 00:19:44,308
when Eagleton offered me
that job,

507
00:19:44,376 --> 00:19:45,943
and I asked you
for your advice?

508
00:19:46,011 --> 00:19:47,945
Uh, "do whatever
the hell you want.

509
00:19:48,013 --> 00:19:48,980
What do I care?"

510
00:19:49,047 --> 00:19:50,848
Right, but then, after,

511
00:19:50,916 --> 00:19:53,150
when I pressed you,
what did you say?

512
00:19:54,218 --> 00:19:55,486
I believe I said

513
00:19:55,554 --> 00:19:57,438
that I thought
we worked well together

514
00:19:57,523 --> 00:20:00,892
and that I might disagree
with your philosophy,

515
00:20:00,959 --> 00:20:02,894
but I respected you.

516
00:20:02,961 --> 00:20:04,428
And I said that you'll get

517
00:20:04,496 --> 00:20:07,198
a lot of job offers
in your life,

518
00:20:07,266 --> 00:20:09,267
but you only have one hometown.

519
00:20:09,334 --> 00:20:10,801
Yes.

520
00:20:10,869 --> 00:20:13,271
That's how I remember it.

521
00:20:16,575 --> 00:20:19,410
This, by the way,
is a one-time-only situation.

522
00:20:19,478 --> 00:20:22,813
Next year, your birthday party
is gonna be a rager.

523
00:20:29,136 --> 00:20:30,469
Mmm!

524
00:20:33,874 --> 00:20:37,543
♪ ♪

525
00:20:37,611 --> 00:20:38,594
Mmm.

526
00:20:45,089 --> 00:20:55,024
<font color="#FF8C00">Sync & corrections by Alice</font>
<font color="##FF8C00">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
