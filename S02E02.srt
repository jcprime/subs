1
00:00:00,778 --> 00:00:02,278
I just want to check

2
00:00:02,460 --> 00:00:05,707
one last time that you're okay
about this date with Mark.

3
00:00:05,832 --> 00:00:07,579
I am so fine with it,
Ann, seriously.

4
00:00:07,704 --> 00:00:08,799
It's so fine.

5
00:00:08,967 --> 00:00:10,592
As long as you and I are cool.

6
00:00:10,759 --> 00:00:12,109
You know my code.

7
00:00:12,452 --> 00:00:13,597
Hos before bros.

8
00:00:14,421 --> 00:00:16,057
- Uteruses before duderuses.
- Got it.

9
00:00:16,322 --> 00:00:18,072
Ovaries before brovaries.

10
00:00:19,992 --> 00:00:22,188
- You make such good coffee.
- Look,

11
00:00:22,356 --> 00:00:25,232
I know you're saying you're okay
but I've been in this position before.

12
00:00:25,425 --> 00:00:26,985
And I had a friend who dated an ex,

13
00:00:27,483 --> 00:00:29,988
and I said I was okay,
but I wasn't actually.

14
00:00:30,230 --> 00:00:33,120
- It was kind of weird.
- The thing is, Mark isn't my ex.

15
00:00:33,245 --> 00:00:34,950
We slept together six years ago.

16
00:00:35,498 --> 00:00:37,828
Anyway, I'm over it.
Or am I? I'm just kidding.

17
00:00:42,743 --> 00:00:44,919
This is one
of my greatest brainstorms ever.

18
00:00:45,191 --> 00:00:48,047
A community garden.
Right in the middle of the pit.

19
00:00:48,450 --> 00:00:51,465
We're encouraging people to come out
and plant whatever they want.

20
00:00:51,590 --> 00:00:53,845
And Tom and I sometimes
get here early and help out.

21
00:00:54,691 --> 00:00:56,931
It's so hot.
I had to get some more iced tea.

22
00:00:57,436 --> 00:01:01,018
People have just really embraced this
and planted the coolest stuff.

23
00:01:01,419 --> 00:01:04,631
And Tom is our master horticulturist.
He knows all the scientific names.

24
00:01:04,756 --> 00:01:05,806
Right, Tom?

25
00:01:06,235 --> 00:01:09,318
- Like this. What's this?
- Those are, of course, tomatoes.

26
00:01:09,583 --> 00:01:11,237
Or souljaboy tellems.

27
00:01:11,708 --> 00:01:14,783
Whenever Leslie asks me
for the Latin names of our plants,

28
00:01:14,908 --> 00:01:16,826
I just give her
the names of rappers.

29
00:01:17,341 --> 00:01:19,859
- And those over there?
- Those are some diddies.

30
00:01:20,026 --> 00:01:22,749
There's some bonethugs
and harmoniums right here.

31
00:01:22,980 --> 00:01:25,395
- Growing beautifully.
- Those ludacrises are coming in great.

32
00:01:25,520 --> 00:01:27,194
Look. Someone planted something new.

33
00:01:27,319 --> 00:01:28,419
What's this?

34
00:01:29,150 --> 00:01:30,571
What do you think?
Carrots?

35
00:01:30,696 --> 00:01:33,796
If that's true,
we have a garden pest on our hands.

36
00:01:35,127 --> 00:01:36,373
Some kind of spice?

37
00:01:36,498 --> 00:01:38,372
You know, the best way to figure out

38
00:01:38,497 --> 00:01:41,636
what kind of spice that is,
is roll it up into a joint and smoke it.

39
00:01:45,969 --> 00:01:47,142


40
00:01:49,077 --> 00:01:50,270


41
00:01:50,590 --> 00:01:52,245


42
00:01:52,370 --> 00:01:54,046


43
00:02:05,764 --> 00:02:07,293
Ron, did you get my text?

44
00:02:08,454 --> 00:02:09,794
Did you get my emails?

45
00:02:10,319 --> 00:02:11,927
Did you see that I paged you?

46
00:02:12,192 --> 00:02:13,097
I did not.

47
00:02:13,222 --> 00:02:14,899
Did you check your voicemail?

48
00:02:15,602 --> 00:02:16,451
I didn't.

49
00:02:16,576 --> 00:02:18,370
We have a criminal emergency
on our hands.

50
00:02:18,495 --> 00:02:20,890
Someone planted a gateway drug
in the community garden.

51
00:02:21,678 --> 00:02:24,018
- Call the cops.
- That'll leak to the press.

52
00:02:24,549 --> 00:02:27,646
Then there'll be an investigation. And 
they'll find my fingerprints on the manure.

53
00:02:27,814 --> 00:02:29,912
- And then we'll lose our funding.
- You don't have.

54
00:02:30,037 --> 00:02:32,242
And we never will if this gets out.

55
00:02:32,409 --> 00:02:33,991
Look, I will call the cops,

56
00:02:34,116 --> 00:02:36,447
but give me one day to deal
with this internally, okay?

57
00:02:36,948 --> 00:02:38,365
- Sure.
- Thank you.

58
00:02:39,087 --> 00:02:41,577
And listen, if this thing blows up,

59
00:02:42,515 --> 00:02:43,813
I was never here.

60
00:02:48,884 --> 00:02:50,184
I have a hernia.

61
00:02:50,759 --> 00:02:52,459
I've had it for a while.

62
00:02:53,313 --> 00:02:55,213
And I've been ignoring it...

63
00:02:55,872 --> 00:02:58,740
successfully, but this morning,

64
00:03:00,555 --> 00:03:02,097
I made the mistake of sneezing.

65
00:03:06,050 --> 00:03:07,520
But as long as I sit still

66
00:03:07,688 --> 00:03:09,265
and don't move my head

67
00:03:10,535 --> 00:03:11,535
or torso,

68
00:03:13,655 --> 00:03:15,205
I'm good, I got this.

69
00:03:16,338 --> 00:03:18,292
We need to find out
who we're dealing with.

70
00:03:18,417 --> 00:03:19,657
Who is this kingpin?

71
00:03:20,335 --> 00:03:22,886
It's a 13-year-old kid named Stevie

72
00:03:23,011 --> 00:03:25,066
who likes to get high
and make his transformers

73
00:03:25,191 --> 00:03:26,824
look like they're having sex.

74
00:03:26,949 --> 00:03:30,627
I would like to be president some day,
so, no, I have not smoked marijuana.

75
00:03:31,628 --> 00:03:33,178
I ate a brownie once.

76
00:03:33,748 --> 00:03:35,266
At a party in college.

77
00:03:35,433 --> 00:03:36,683
It was intense.

78
00:03:36,993 --> 00:03:38,998
It was kind of indescribable,
actually.

79
00:03:39,289 --> 00:03:40,678
I felt like I was floating.

80
00:03:41,737 --> 00:03:43,298
Turns out there wasn't any pot in it.

81
00:03:43,423 --> 00:03:45,683
It was just
an insanely good brownie.

82
00:03:45,981 --> 00:03:47,876
You know, if we catch him,

83
00:03:48,001 --> 00:03:50,472
then we avoid a scandal,
'cause we flip the headline.

84
00:03:50,597 --> 00:03:53,233
And it's not "community garden
infested with pot",

85
00:03:53,466 --> 00:03:55,192
it's "brave public servants

86
00:03:55,360 --> 00:03:56,898
"rid neighborhood of drugs,

87
00:03:57,023 --> 00:03:59,405
"earn praise from mayor,
comma, everyone."

88
00:03:59,995 --> 00:04:01,351
We gotta catch this guy.

89
00:04:01,518 --> 00:04:03,518
Get the van, meet me at 7:00.

90
00:04:04,949 --> 00:04:06,399
It's stakeout time.

91
00:04:10,584 --> 00:04:12,376
Get my lunch for me, please.

92
00:04:12,544 --> 00:04:14,128
Okay, like order you something?

93
00:04:14,560 --> 00:04:15,810
No, get it...

94
00:04:16,384 --> 00:04:17,484
from there.

95
00:04:18,687 --> 00:04:20,928
Is this some kind
of weird power trip?

96
00:04:21,388 --> 00:04:22,303
Please.

97
00:04:28,847 --> 00:04:30,645
Thank you.
That'll be all.

98
00:04:31,219 --> 00:04:32,569
You're welcome.

99
00:04:44,538 --> 00:04:47,756
I have stakeout supplies. This is stuff
that we're probably gonna need.

100
00:04:47,881 --> 00:04:49,797
We have notepads,

101
00:04:50,478 --> 00:04:52,082
pencils, and pencil case.

102
00:04:52,374 --> 00:04:54,126
Shakable whipped cream.

103
00:04:54,294 --> 00:04:55,292
Cameras.

104
00:04:55,417 --> 00:04:56,360
Gorp.

105
00:04:57,187 --> 00:04:58,067
And...

106
00:04:59,488 --> 00:05:00,788
candy necklaces.

107
00:05:01,984 --> 00:05:03,438
It's like we're real police.

108
00:05:03,605 --> 00:05:05,617
And I made us a mix CD.

109
00:05:06,403 --> 00:05:09,493
It's all filled with songs
about people watching people.

110
00:05:09,660 --> 00:05:11,060
It's mostly Sting.

111
00:05:11,472 --> 00:05:13,812
- I put our faces on there.
- It's really cool.

112
00:05:13,980 --> 00:05:15,731
Are you gonna wear
that shirt?

113
00:05:15,856 --> 00:05:17,245
Yeah. You like it?

114
00:05:17,787 --> 00:05:18,913
Tommy Hilfiger?

115
00:05:19,038 --> 00:05:22,263
No, Tom Haverford.
I spent 120 bucks to get it monogrammed.

116
00:05:22,388 --> 00:05:23,822
Everyone thinks it's Hilfiger.

117
00:05:24,204 --> 00:05:26,508
You should be wearing
something like this. Take this.

118
00:05:26,633 --> 00:05:29,371
Wear black.
Black is what you wear in a stakeout.

119
00:05:29,496 --> 00:05:31,422
I have to figure out how this works.

120
00:05:31,547 --> 00:05:34,083
- Just let me focus up on this.
- Pretty bummed this fits.

121
00:05:34,251 --> 00:05:36,386
Okay, now we just need
to take a test shot

122
00:05:36,553 --> 00:05:38,671
- and see how this goes.
- Take a test shot. Ready?

123
00:05:38,861 --> 00:05:40,734
- Focus up on the pit. All right.
- Stop.

124
00:05:40,859 --> 00:05:42,595
- Focus up right where the...
- Stop.

125
00:05:42,720 --> 00:05:44,352
Focus up right where the weed...

126
00:05:44,477 --> 00:05:46,011
Stop.
Stop.

127
00:05:46,785 --> 00:05:48,223
Leslie swears it's okay.

128
00:05:48,348 --> 00:05:50,248
So I'm going out with Mark.

129
00:05:50,780 --> 00:05:52,267
I got to get back out there.

130
00:05:52,435 --> 00:05:54,545
When Andy and I used
to go to the movies,

131
00:05:54,670 --> 00:05:57,256
he would always try to guess
the ending of the movie.

132
00:05:57,381 --> 00:05:59,225
And he would always guess

133
00:05:59,350 --> 00:06:02,236
that the main character
had been dead the whole time.

134
00:06:03,259 --> 00:06:05,209
Even when we saw <i>Ratatouille.</i>

135
00:06:09,619 --> 00:06:10,919
Look, it's Mark.

136
00:06:14,749 --> 00:06:15,867
What are you doing?

137
00:06:15,992 --> 00:06:18,856
What? I'm just checking
to make sure this lens works.

138
00:06:18,981 --> 00:06:19,878
Good.

139
00:06:26,282 --> 00:06:27,471
Are you ready?

140
00:06:27,596 --> 00:06:30,050
Oh, no, I need another hour
to get ready.

141
00:06:30,217 --> 00:06:32,684
Seriously? 'Cause I think
the movie starts in like...

142
00:06:34,477 --> 00:06:35,894
You're joking around right now.

143
00:06:36,997 --> 00:06:38,418
This is gonna be fun.

144
00:06:38,586 --> 00:06:40,514
You know, with a body like this,

145
00:06:40,639 --> 00:06:42,589
you don't get the brains too.

146
00:06:43,012 --> 00:06:44,688
That was silly.

147
00:06:46,775 --> 00:06:48,032
Are they dating now?

148
00:06:48,200 --> 00:06:49,571
Brendanawicz is the man.

149
00:06:50,035 --> 00:06:53,117
This is Brendanawicz's life:
hot chick from the newspaper,

150
00:06:53,242 --> 00:06:56,133
hot chick from the post office,
hot chick from the hospital.

151
00:06:56,258 --> 00:06:58,299
Her name's Ann.
You know her name. Her name's Ann.

152
00:06:58,424 --> 00:07:00,378
I'm just saying,
nobody turns him down.

153
00:07:01,106 --> 00:07:02,046
I did.

154
00:07:03,632 --> 00:07:04,536
Yeah, I did.

155
00:07:04,925 --> 00:07:06,805
We were drunk,
and he tried to kiss me,

156
00:07:06,930 --> 00:07:10,934
and I said, "nuh-uh,
no, sir, Mark Brendanawicz.

157
00:07:11,374 --> 00:07:12,848
'I don't need your business here."

158
00:07:13,217 --> 00:07:14,217
Get down!

159
00:07:28,402 --> 00:07:29,406
Working late?

160
00:07:44,937 --> 00:07:46,221
<i>Shovel guitar</i>

161
00:07:46,346 --> 00:07:47,428
<i>Shovel guitar</i>

162
00:07:47,595 --> 00:07:49,886
<i>Somebody wants to play shovel guitar</i>

163
00:07:50,464 --> 00:07:54,014
Leslie's been playing shovel guitar
for about an hour now.

164
00:07:54,459 --> 00:07:55,676
<i>Bucket drum</i>

165
00:07:56,450 --> 00:07:57,601
<i>Bucket drum</i>

166
00:07:57,769 --> 00:08:00,145
- You're not from here, right?
- No, I'm from South Carolina.

167
00:08:00,313 --> 00:08:02,279
But you moved
to South Carolina from where?

168
00:08:02,404 --> 00:08:06,023
- My mother's uterus.
- But you were conceived in Libya?

169
00:08:07,779 --> 00:08:09,871
I was conceived in America.
My parents are Indian.

170
00:08:09,996 --> 00:08:11,804
Where did the name Haverford
come from?

171
00:08:11,929 --> 00:08:13,909
My birth name is...

172
00:08:14,077 --> 00:08:16,196
Darwish Zubair Ismail Gani.

173
00:08:16,524 --> 00:08:18,129
Then I changed it to Tom Haverford

174
00:08:18,254 --> 00:08:21,583
because, you know, brown guys
with funny-sounding Muslim names

175
00:08:21,789 --> 00:08:23,816
don't make it really far
in politics.

176
00:08:24,785 --> 00:08:27,218
- What about Barack Obama?
- Yeah, fine, Barack Obama.

177
00:08:27,343 --> 00:08:31,023
If I knew a dude named Barack Obama
was gonna be elected president,

178
00:08:31,148 --> 00:08:33,595
- Maybe I wouldn't have changed it.
- Did you hear that?

179
00:08:34,269 --> 00:08:35,556
It's the kingpin.

180
00:08:36,203 --> 00:08:37,503
Write this down.

181
00:08:39,703 --> 00:08:42,531
White male, light brown hair.

182
00:08:42,740 --> 00:08:44,190
Just take pictures.

183
00:08:44,332 --> 00:08:46,066
Oh, my God.
It looks like Andy.

184
00:08:46,424 --> 00:08:47,359
That is Andy.

185
00:08:48,566 --> 00:08:49,716
That is Andy.

186
00:08:50,304 --> 00:08:51,804
Andy is the kingpin.

187
00:08:53,301 --> 00:08:54,533
Andy, come here.

188
00:08:54,701 --> 00:08:56,434
Oh, hey, you guys.

189
00:08:56,851 --> 00:08:58,981
What are you doing here?
It's so good to see you.

190
00:08:59,106 --> 00:09:00,539
- Quietly.
- What happened?

191
00:09:00,832 --> 00:09:03,709
- You're living in the pit now?
- For now, yeah. It's awesome.

192
00:09:03,877 --> 00:09:06,476
Somebody planted a garden down there
with fruits and vegetables,

193
00:09:06,601 --> 00:09:09,399
- so I'm getting a lot of vitamins.
- That's our community garden.

194
00:09:09,524 --> 00:09:13,000
Yeah, and someone planted
a whole mess of weed

195
00:09:13,125 --> 00:09:14,303
next to the tomatoes.

196
00:09:14,471 --> 00:09:16,872
Don't look at me.
There's weed down there?

197
00:09:16,997 --> 00:09:18,432
I thought that was 
the tops of carrots.

198
00:09:18,600 --> 00:09:21,233
Right, I know. Listen,
we're on a stakeout, okay?

199
00:09:21,358 --> 00:09:22,827
Why don't you come helping us?

200
00:09:22,952 --> 00:09:25,147
To catch the guy.
You could tell us what you've seen.

201
00:09:25,440 --> 00:09:27,859
I'm supposed to have a rock fight
with this crazy guy.

202
00:09:29,936 --> 00:09:32,362
He's like 20 minutes late.
All right. Let's do it.

203
00:09:33,363 --> 00:09:34,813
Do you live here?

204
00:09:38,837 --> 00:09:40,412
Yeah, do you live here?

205
00:09:42,899 --> 00:09:43,790
Catch.

206
00:09:45,727 --> 00:09:46,960
Yeah, I thought so.

207
00:09:47,965 --> 00:09:50,042
I went home,
but I had this strange feeling

208
00:09:50,209 --> 00:09:52,382
that there was something wrong
with you, so I came back.

209
00:09:53,112 --> 00:09:55,093
It's just a minor medical issue.

210
00:09:55,327 --> 00:09:56,200
AIDS?

211
00:09:56,727 --> 00:09:57,795
No, I'm safe.

212
00:09:57,920 --> 00:09:58,889
Blindness?

213
00:10:00,107 --> 00:10:03,060
Is it like a parasite or a virus
or something you get from a bee?

214
00:10:04,437 --> 00:10:05,562
I have a hernia.

215
00:10:05,730 --> 00:10:08,039
- Do you have syphilis?
- I said it's a hernia.

216
00:10:08,164 --> 00:10:09,208
I know.

217
00:10:09,564 --> 00:10:11,151
It's possible to have two things.

218
00:10:11,774 --> 00:10:13,631
Do you need a ride to the hospital?

219
00:10:13,756 --> 00:10:14,998
Yes, please.

220
00:10:16,213 --> 00:10:19,076
But I rode my bike, so I have to go home
and get my dad's station wagon.

221
00:10:20,108 --> 00:10:21,120
Thank you.

222
00:10:28,198 --> 00:10:31,589
- You still here?
- I wanted to see if you could tell.

223
00:10:38,508 --> 00:10:40,058
Are you still here?

224
00:10:40,984 --> 00:10:42,955
It is really great
to have somebody to talk to.

225
00:10:43,080 --> 00:10:44,309
Man, oh, man.

226
00:10:44,477 --> 00:10:46,061
So how's Ann?
She's doing good?

227
00:10:46,249 --> 00:10:48,700
I miss her so much, it's ridiculous.

228
00:10:48,825 --> 00:10:51,775
- How's she doing?
- Doing good tonight. She's out on a...

229
00:10:51,943 --> 00:10:53,386
Ann is great.

230
00:10:54,269 --> 00:10:55,821
And I bet she really misses you.

231
00:10:57,240 --> 00:10:59,157
I'm gonna need more
if I'm gonna stay up.

232
00:10:59,325 --> 00:11:01,380
- Is that candy?
- It's a necklace made out of candy.

233
00:11:01,505 --> 00:11:03,579
- Want one?
- Oh, all right.

234
00:11:03,885 --> 00:11:06,485
Great. All I've had
is fruits and vegetables...

235
00:11:06,652 --> 00:11:07,852
for weeks now.

236
00:11:10,295 --> 00:11:11,216
Oh, my God.

237
00:11:15,127 --> 00:11:16,970
You know there's a string in there.

238
00:11:17,713 --> 00:11:18,927
Not in this one.

239
00:11:19,672 --> 00:11:23,181
Instant sugar high.
Sugar high. Sugar hit high.

240
00:11:23,349 --> 00:11:24,498
Sugar high. High.

241
00:11:26,821 --> 00:11:27,971
Are you okay?

242
00:11:29,104 --> 00:11:30,633
Sugar slam.

243
00:11:30,842 --> 00:11:33,346
Maybe we should get you 
something more substantial to eat.

244
00:11:34,016 --> 00:11:34,860
I'm in.

245
00:11:36,041 --> 00:11:38,770
Bring me back two cheeseburgers
and a green tea.

246
00:11:43,092 --> 00:11:45,342
Come on.
You got to be kidding me.

247
00:11:50,012 --> 00:11:51,538
So that was fun.

248
00:11:51,705 --> 00:11:53,305
Yeah, it really was.

249
00:11:53,630 --> 00:11:55,881
- Surprisingly fun.
- You know what,

250
00:11:56,049 --> 00:11:58,463
I have been a perfect gentleman
this evening.

251
00:11:58,588 --> 00:11:59,962
In fact, watch this.

252
00:12:00,129 --> 00:12:03,264
I would like you to now please
invite me in for a nightcap.

253
00:12:03,473 --> 00:12:05,680
- Not a chance.
- No. Just watch...

254
00:12:05,805 --> 00:12:07,352
watch what I'm gonna do.

255
00:12:07,477 --> 00:12:08,810
Ask me in for a nightcap.

256
00:12:09,076 --> 00:12:11,237
- Will you come in for a nightcap?
- Yes, I will.

257
00:12:11,571 --> 00:12:12,785
No, no, damn.

258
00:12:13,169 --> 00:12:15,042
Drats, I was gonna plan to not.

259
00:12:15,167 --> 00:12:16,068
Wait.

260
00:12:16,236 --> 00:12:17,193
Look.

261
00:12:18,129 --> 00:12:20,621
That guy's trying
to break into that van.

262
00:12:21,367 --> 00:12:23,617
It does look like
he's breaking into that van.

263
00:12:26,659 --> 00:12:28,132
<i>- 911.</i>
- Hi, there.

264
00:12:28,448 --> 00:12:30,862
So, do you like spy on Ann?

265
00:12:31,143 --> 00:12:32,293
From the pit?

266
00:12:35,509 --> 00:12:37,214
I just like being nearby.

267
00:12:37,382 --> 00:12:39,024
That way if she wants me back,

268
00:12:39,149 --> 00:12:42,746
I could be at her house in two seconds,
before she changes her mind.

269
00:12:43,571 --> 00:12:45,854
I don't really know
how healthy that is.

270
00:12:45,979 --> 00:12:49,393
You aren't even dating anymore.
She might be dating somebody else.

271
00:12:50,958 --> 00:12:52,358
- Is she?
- What?

272
00:12:52,606 --> 00:12:54,440
- Is she dating somebody else?
- What?

273
00:12:54,667 --> 00:12:56,957
- She's dating somebody else?
- I didn't say that.

274
00:12:57,082 --> 00:12:59,236
- Who is she dating?
- Nobody. Mark.

275
00:13:00,655 --> 00:13:01,655
It's cool.

276
00:13:01,780 --> 00:13:02,780
My God!

277
00:13:04,575 --> 00:13:07,048
641 to base style.
I'm at the vehicle now.

278
00:13:12,824 --> 00:13:14,292
How are you doing tonight?

279
00:13:14,460 --> 00:13:16,367
I'm all right, officer.
How are you?

280
00:13:16,492 --> 00:13:17,921
I'm responding to a 911 call

281
00:13:18,089 --> 00:13:20,716
about a suspicious person
breaking into a van.

282
00:13:20,925 --> 00:13:23,141
I need you to step out
and show me some ID.

283
00:13:23,308 --> 00:13:26,367
It's okay, my name's Tom Haverford.
I work for the Parks Department.

284
00:13:26,598 --> 00:13:29,224
I got locked out of the van.
I had to jimmy my way back in.

285
00:13:29,392 --> 00:13:31,810
Why don't you jimmy your way out
and show me some ID?

286
00:13:32,856 --> 00:13:35,611
I just told you my ID,
so what's the crime here?

287
00:13:35,886 --> 00:13:37,324
Parking while Indian?

288
00:13:37,609 --> 00:13:40,817
There's no stereotypes
about Indians sitting in vehicles.

289
00:13:41,321 --> 00:13:42,602
All right, fine.

290
00:13:42,906 --> 00:13:44,306
Here's my ID.

291
00:13:46,106 --> 00:13:47,656
Nice job, Paul Blart.

292
00:13:48,261 --> 00:13:52,186
Why don't you head back to the mall, make 
sure nobody's breaking into lady-foot locker?

293
00:13:52,390 --> 00:13:55,500
I'm very close to placing you
under arrest for disorderly conduct.

294
00:13:55,757 --> 00:13:58,444
- Step out of the van.
- I'll step out of your mama's van.

295
00:14:00,238 --> 00:14:02,459
I didn't do anything!
I'm a city employee.

296
00:14:02,584 --> 00:14:04,176
- Come on!
- Calm down.

297
00:14:09,838 --> 00:14:11,088
Motion sensors.

298
00:14:13,011 --> 00:14:14,011
My God.

299
00:14:15,268 --> 00:14:17,215
Excuse me.
What are you doing?

300
00:14:17,509 --> 00:14:19,593
What happened to the guy
that was in that van?

301
00:14:19,718 --> 00:14:20,737
Cops took him.

302
00:14:20,904 --> 00:14:21,904
What?

303
00:14:22,278 --> 00:14:23,990
- My God.
- Ann and Mark!

304
00:14:24,917 --> 00:14:26,278
It's Ann and Mark.

305
00:14:31,647 --> 00:14:33,914
We caught a criminal.
Do you know about this?

306
00:14:34,082 --> 00:14:35,707
He was breaking into this van,

307
00:14:36,287 --> 00:14:38,001
and they dragged him away.

308
00:14:38,230 --> 00:14:39,712
It was crazy.

309
00:14:39,837 --> 00:14:40,837
Right here.

310
00:14:41,005 --> 00:14:42,297
What are you doing here?

311
00:14:45,738 --> 00:14:48,845
If you could just stop writing for 
a second and focus here, 'cause...

312
00:14:49,013 --> 00:14:50,013
Ma'am.

313
00:14:50,138 --> 00:14:53,012
As I've already told you,
this is a police matter.

314
00:14:53,559 --> 00:14:56,109
It doesn't seem
to matter to the police.

315
00:14:57,389 --> 00:15:00,315
- Nice job with that, but listen...
- Let me ask you a question.

316
00:15:00,483 --> 00:15:02,600
Is it the policy
of the Pawnee Police Department

317
00:15:02,725 --> 00:15:04,986
to arrest people when they try 
to get into their own van?

318
00:15:06,083 --> 00:15:07,767
That's not what happened.

319
00:15:07,949 --> 00:15:09,366
Could you please go home,

320
00:15:09,534 --> 00:15:11,592
get some sleep
and let this matter be resolved?

321
00:15:11,717 --> 00:15:13,134
Let's just go home.

322
00:15:13,259 --> 00:15:14,630
Let's go home.

323
00:15:14,755 --> 00:15:16,512
- No!
- Let's just go home.

324
00:15:18,897 --> 00:15:19,624
Easy.

325
00:15:21,170 --> 00:15:23,196
- Come with me.
- I'd like to come with you.

326
00:15:23,321 --> 00:15:24,990
Your friend, he acted like an ass.

327
00:15:25,115 --> 00:15:26,675
And he didn't give me a choice.

328
00:15:27,368 --> 00:15:29,135
Your name's Dave?
Can I call you Dave?

329
00:15:30,330 --> 00:15:32,764
My mother likes David,
but I'm pretty split on...

330
00:15:32,932 --> 00:15:34,285
I hate to break it to you.

331
00:15:34,410 --> 00:15:35,850
You really stepped in it.

332
00:15:36,251 --> 00:15:37,018
What?

333
00:15:37,186 --> 00:15:39,896
I'm a government employee,
and so is your prisoner.

334
00:15:40,206 --> 00:15:42,232
If I wanted to,
I could get on the horn,

335
00:15:42,599 --> 00:15:44,584
and I could have
Ron Swanson down here.

336
00:15:44,709 --> 00:15:46,069
Kicking down your doors.

337
00:15:46,237 --> 00:15:47,696
That's right, you heard me.

338
00:15:52,869 --> 00:15:54,536
What branch of government
are you in?

339
00:15:57,119 --> 00:15:58,331
Parks and Recreation.

340
00:16:00,434 --> 00:16:01,584
Do I stutter?

341
00:16:04,800 --> 00:16:06,520
It wasn't just his behavior.

342
00:16:06,645 --> 00:16:09,009
I think your friend
might be a pervert.

343
00:16:09,177 --> 00:16:11,540
That's what people think
when they first meet him

344
00:16:11,665 --> 00:16:14,054
- but he's all talk.
- I'm being serious.

345
00:16:14,179 --> 00:16:16,725
We searched the van,
and we found a lot of disturbing things.

346
00:16:17,121 --> 00:16:19,311
Some professional
photography equipment,

347
00:16:19,479 --> 00:16:21,689
and these pictures
of some people on a date.

348
00:16:22,004 --> 00:16:24,704
We also found
an enormous amount of manure,

349
00:16:25,002 --> 00:16:27,389
and a very inappropriate
amount of candy.

350
00:16:29,509 --> 00:16:31,267
Tom and I were in that van together.

351
00:16:31,392 --> 00:16:33,228
We were staking out
the community garden

352
00:16:33,353 --> 00:16:35,251
because somebody
planted marijuana in it.

353
00:16:35,376 --> 00:16:37,037
I brought the candy, a lot of it

354
00:16:37,205 --> 00:16:38,377
because I love it.

355
00:16:38,502 --> 00:16:39,773
I took the pictures.

356
00:16:40,848 --> 00:16:43,502
My best friend was going on a date
with my ex-lover.

357
00:16:44,712 --> 00:16:46,326
It's been a very long night.

358
00:16:46,558 --> 00:16:49,215
But Tom is not a pervert,
and he shouldn't be in jail.

359
00:16:51,719 --> 00:16:54,012
Well, they're gonna release him.
In a little while.

360
00:16:54,390 --> 00:16:55,671
Thank God.

361
00:16:56,352 --> 00:16:58,152
Should I get you home right now?

362
00:16:58,277 --> 00:17:00,977
You two go ahead.
I'm gonna stay here and wait for Tom.

363
00:17:01,398 --> 00:17:03,206
- Are you sure?
- Yeah.

364
00:17:03,331 --> 00:17:04,881
Prison changes a man.

365
00:17:05,383 --> 00:17:08,276
I think he'll wanna see a familiar face
when he gets back on the outside.

366
00:17:09,009 --> 00:17:11,370
Well, OK, then.
See you, Leslie.

367
00:17:18,537 --> 00:17:20,872
Yo. I had to wait
till my dad fell asleep

368
00:17:21,040 --> 00:17:22,840
so I could steal his keys.

369
00:17:23,118 --> 00:17:24,125
You ready?

370
00:17:24,610 --> 00:17:26,019
I was born ready.

371
00:17:28,622 --> 00:17:30,321
I'm Ron [beep] Swanson.

372
00:17:32,885 --> 00:17:34,160
Easy. Care...

373
00:17:35,711 --> 00:17:36,711
Careful!

374
00:17:42,410 --> 00:17:44,810
Ann, I really don't think
you should invite me in.

375
00:17:44,977 --> 00:17:47,227
Terrible things happened last time.

376
00:17:49,026 --> 00:17:50,676
So I guess I'll just...

377
00:17:51,685 --> 00:17:53,035
see you tomorrow.

378
00:17:55,754 --> 00:17:56,554
Bye.

379
00:17:59,893 --> 00:18:01,493
You kind of kissed me.

380
00:18:01,876 --> 00:18:04,418
Yes, I did,
and now we both have herpes.

381
00:18:05,071 --> 00:18:06,521
I'll see you later.

382
00:18:22,018 --> 00:18:24,602
- Thanks for waiting. Appreciate it.
- Mother Teresa.

383
00:18:25,277 --> 00:18:28,356
It's not your chocolate.
Don't... get your hands off that.

384
00:18:28,616 --> 00:18:30,043
- Wake up.
- What?

385
00:18:30,559 --> 00:18:32,164
Is this your house?
Where are we?

386
00:18:32,289 --> 00:18:33,570
You're awake?

387
00:18:36,833 --> 00:18:39,242
- Thanks for the coat.
- It's no problem, you can keep it.

388
00:18:39,433 --> 00:18:40,285
Really?

389
00:18:41,401 --> 00:18:44,330
Actually, no.
I need that for my uniform.

390
00:18:45,069 --> 00:18:46,583
But I can give you a ride.

391
00:18:48,501 --> 00:18:49,964
We'll talk about the coat.

392
00:18:50,166 --> 00:18:52,297
- I can't give it to you. Understand?
- OK.

393
00:18:54,291 --> 00:18:56,445
So where's that marijuana at?

394
00:18:57,455 --> 00:18:59,000
It's down in the garden.

395
00:18:59,224 --> 00:19:01,973
Good meeting you, man.
Should grab a drink some time.

396
00:19:03,663 --> 00:19:05,143
I hope Ann and Mark got home okay.

397
00:19:05,311 --> 00:19:08,761
That's all that matters.
That Ann is home, and she's okay.

398
00:19:09,118 --> 00:19:11,567
And that Mark is also okay,
and he is in his home.

399
00:19:12,235 --> 00:19:14,897
Just important that they're both
in their homes.

400
00:19:15,022 --> 00:19:16,237
Mark's an idiot.

401
00:19:16,614 --> 00:19:19,616
You could do a lot better than him.
Forget about him.

402
00:19:20,888 --> 00:19:21,988
Thanks, Tom.

403
00:19:23,425 --> 00:19:26,206
I didn't mean me.
Keep it in your pants, Knope.

404
00:19:27,752 --> 00:19:29,751
I swear to God,
it actually... it was there.

405
00:19:29,919 --> 00:19:31,019
These are...

406
00:19:31,494 --> 00:19:32,494
carrots.

407
00:19:33,576 --> 00:19:36,818
You don't think I know the difference
between carrots and marijuana?

408
00:19:39,245 --> 00:19:41,504
Is that the house there
where your best friend lives?

409
00:19:41,629 --> 00:19:43,172
Where she saw the van from?

410
00:19:45,347 --> 00:19:46,267
I get it.

411
00:19:46,435 --> 00:19:47,236
What?

412
00:19:47,468 --> 00:19:49,229
You wanted to check up
on your friend

413
00:19:49,397 --> 00:19:51,022
and that guy, your ex,

414
00:19:51,461 --> 00:19:55,026
so you made up a story about weed
so you could stake out their date.

415
00:19:55,194 --> 00:19:56,029
I did not.

416
00:19:56,154 --> 00:19:58,321
- There actually was weed here.
- It's okay.

417
00:19:59,410 --> 00:20:01,074
I think it's kind of cute.

418
00:20:05,381 --> 00:20:06,625
I like Miss Knope.

419
00:20:06,750 --> 00:20:07,885
I gotta say

420
00:20:08,551 --> 00:20:10,998
when I first met her,
I didn't care much for her,

421
00:20:11,123 --> 00:20:14,963
because, like 99% of the people
in any given day of my life,

422
00:20:15,131 --> 00:20:16,797
she was very belligerent

423
00:20:16,922 --> 00:20:18,272
and disagreeable.

424
00:20:18,551 --> 00:20:21,699
Miss Knope was attractive to me
as a man.

425
00:20:22,340 --> 00:20:25,809
I was attracted to her
in her demeanor.

426
00:20:25,934 --> 00:20:29,561
I was attracted to her
in a sexual manner that was appropriate.

427
00:20:31,775 --> 00:20:33,606
I don't want to talk
about this anymore.

428
00:20:35,766 --> 00:20:37,953
What a crazy night.
Partner?

429
00:20:38,079 --> 00:20:39,573
Want to go get some breakfast?

430
00:20:39,698 --> 00:20:41,739
What? No.
Take me home.

431
00:20:41,907 --> 00:20:43,308
What is wrong with you?

432
00:20:44,786 --> 00:20:47,453
Oh, my God,
I can't believe this is on!

433
00:20:48,778 --> 00:20:50,600
I wonder if mini golf is open.

434
00:20:51,056 --> 00:20:52,056
Home!

435
00:20:55,822 --> 00:20:57,088
Ann-danawicz,

436
00:20:57,482 --> 00:20:59,630
what are you guys doing?
Eating? Love it.

437
00:20:59,808 --> 00:21:02,092
Just wanted to thank you
for having me arrested

438
00:21:02,217 --> 00:21:04,178
as a pervert the other night.
That was cool.

439
00:21:04,346 --> 00:21:05,207
Peace.

440
00:21:05,999 --> 00:21:08,808
We didn't think you were a pervert.
We thought you were a criminal.

441
00:21:10,273 --> 00:21:13,575
For the record, I still actually
kind of think he's a pervert.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
