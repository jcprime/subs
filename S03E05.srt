﻿1
00:00:05,010 --> 00:00:07,945
I found this typewriter
next to the courtyard dumpster...

2
00:00:07,946 --> 00:00:11,615
An old Underwood Five
with original carriage return.

3
00:00:11,666 --> 00:00:13,183
Ah.

4
00:00:13,251 --> 00:00:15,786
I took her home, polished her up,

5
00:00:16,726 --> 00:00:20,729
and bought a brand-new ribbon
off of electronicbay.com.

6
00:00:20,796 --> 00:00:23,715
Okay, somebody's got
to do something.

7
00:00:23,783 --> 00:00:25,266
I'm getting a cluster headache.

8
00:00:25,334 --> 00:00:28,269
Oh, whoa, whoa, he's leaving.

9
00:00:28,337 --> 00:00:29,002
Let's go.

10
00:00:29,055 --> 00:00:31,474
I'm gonna throw it away.

11
00:00:31,525 --> 00:00:33,409
No, I'm not.
It weighs a billion tons.

12
00:00:33,477 --> 00:00:34,877
What is he typing anyway?

13
00:00:34,945 --> 00:00:37,813
"If you sons of bitches
try to remove this typewriter,

14
00:00:37,864 --> 00:00:40,816
I'll kill you."

15
00:00:40,884 --> 00:00:44,987
I'm gonna type every word I know.

16
00:00:45,038 --> 00:00:46,222
Rectangle!

17
00:00:46,289 --> 00:00:47,990
America!

18
00:00:48,041 --> 00:00:49,575
Megaphone!

19
00:00:49,659 --> 00:00:52,328
Monday!

20
00:00:52,379 --> 00:00:53,863
Butthole.

21
00:00:56,933 --> 00:01:06,873
<font color="#ec14bd">Sync by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

22
00:01:14,017 --> 00:01:17,069
Well, I hate to be the bearer
of bad tidings,

23
00:01:17,154 --> 00:01:19,605
but these interviews are
more important than we thought.

24
00:01:19,689 --> 00:01:22,074
Harvest fest is two weeks away,

25
00:01:22,159 --> 00:01:24,326
and awareness is still
pretty low.

26
00:01:24,394 --> 00:01:25,728
Boy, 35%.

27
00:01:25,795 --> 00:01:27,496
It's actually 34.2%.

28
00:01:27,564 --> 00:01:28,781
34.2%.

29
00:01:28,865 --> 00:01:30,633
I am Ben, the numbers robot.

30
00:01:30,700 --> 00:01:32,284
It's just
an exact, uh, calculation.

31
00:01:32,369 --> 00:01:33,786
It is just an exact calculation.

32
00:01:33,870 --> 00:01:34,837
All right.

33
00:01:34,905 --> 00:01:36,739
So we're doing a huge media blitz

34
00:01:36,806 --> 00:01:38,591
to publicize
the Harvest Festival,

35
00:01:38,675 --> 00:01:41,544
and we've got TV,
newspaper, radio,

36
00:01:41,611 --> 00:01:44,680
and I even called Oprah.

37
00:01:44,748 --> 00:01:46,615
Well, I tried to call Oprah.

38
00:01:46,683 --> 00:01:48,083
I-I couldn't get her number.

39
00:01:48,134 --> 00:01:51,220
I'm putting it out there
like <i>the secret...</i>

40
00:01:51,288 --> 00:01:53,923
And hopefully she'll call me.

41
00:01:53,990 --> 00:01:56,058
Our first interview
is at 93.7 F.M.

42
00:01:56,126 --> 00:01:57,259
With Crazy Ira and the Douche.

43
00:01:57,327 --> 00:02:00,429
Crazy Ira and the Douche.

44
00:02:00,480 --> 00:02:03,315
They are Pawnee's most
hilarious drive-time radio guys.

45
00:02:03,400 --> 00:02:04,817
So much better than
Tubby Tony and the Papaya.

46
00:02:04,901 --> 00:02:05,951
Am I right?

47
00:02:06,036 --> 00:02:07,736
They're seriously so funny.

48
00:02:07,804 --> 00:02:09,271
Is this right?

49
00:02:09,322 --> 00:02:11,073
This feels almost perfect,

50
00:02:11,124 --> 00:02:15,244
but I don't think your core
has maximized elasticity.

51
00:02:15,295 --> 00:02:17,663
Okay, um, I'll come back
if you guys are being weird.

52
00:02:17,747 --> 00:02:19,114
No, no, no, no!

53
00:02:19,165 --> 00:02:20,666
You're exactly the person
that I most want to talk to

54
00:02:20,750 --> 00:02:21,967
right at this moment.

55
00:02:22,052 --> 00:02:23,785
- And Lunge.
- Oh.

56
00:02:23,837 --> 00:02:25,621
Have you given my offer
any more thought?

57
00:02:25,672 --> 00:02:28,557
What exactly would happen
if I said yes?

58
00:02:28,625 --> 00:02:30,626
Well, you would work
at the State House.

59
00:02:30,677 --> 00:02:31,961
You would coordinate

60
00:02:32,028 --> 00:02:33,295
my professional
and personal schedule,

61
00:02:33,363 --> 00:02:34,980
and we would give you
a small relocation fee

62
00:02:35,065 --> 00:02:36,148
for your move to Indianapolis.

63
00:02:36,233 --> 00:02:37,900
Oh, that sounds boring.

64
00:02:37,968 --> 00:02:40,736
But I have
nothing keeping me here.

65
00:02:40,804 --> 00:02:42,771
Do you have Internet
in your office?

66
00:02:42,822 --> 00:02:44,807
Yes.

67
00:02:44,874 --> 00:02:46,742
Fine. I'll do it.

68
00:02:46,809 --> 00:02:48,143
That is literally the best news

69
00:02:48,211 --> 00:02:49,745
that I've heard all day.

70
00:02:49,812 --> 00:02:52,648
Unlike April, I still do not know

71
00:02:52,716 --> 00:02:54,683
what my future holds with Chris,

72
00:02:54,751 --> 00:02:57,620
and it's starting to bum me out.

73
00:02:57,687 --> 00:02:59,988
I need some more vitamin "D."

74
00:03:00,040 --> 00:03:01,823
Yeah.

75
00:03:01,875 --> 00:03:05,377
Oh, hey, crazy.
What was that meeting about?

76
00:03:05,462 --> 00:03:07,630
Oh, it turns out
Chris is my real dad.

77
00:03:07,681 --> 00:03:08,864
I think you're lying.

78
00:03:08,932 --> 00:03:10,832
I think I know
what that meeting was about.

79
00:03:10,884 --> 00:03:12,768
You can't go to Indianapolis.

80
00:03:12,835 --> 00:03:14,737
There's got to be something
that I can do to convince you

81
00:03:14,804 --> 00:03:16,772
how much I care about you.

82
00:03:16,840 --> 00:03:19,241
Tell me
your least favorite things

83
00:03:19,309 --> 00:03:21,610
you have to do every day,
and I'll do them for a month.

84
00:03:21,678 --> 00:03:23,362
Fine.

85
00:03:23,446 --> 00:03:25,414
If you do everything I hate
for a month,

86
00:03:25,482 --> 00:03:28,117
then I might begin to think
about the possibility

87
00:03:28,184 --> 00:03:31,553
of thinking about maybe staying.

88
00:03:31,621 --> 00:03:34,623
That's all I have to hear.

89
00:03:34,691 --> 00:03:37,859
You won't live to regret this.

90
00:03:37,911 --> 00:03:40,462
Unless Andy can un-kiss Ann,

91
00:03:40,530 --> 00:03:43,365
then I'm not gonna change
my mind.

92
00:03:43,433 --> 00:03:45,134
But if he wants to take
my dumb sister

93
00:03:45,201 --> 00:03:46,468
to her dumb dance class,

94
00:03:46,536 --> 00:03:50,472
then I'm not gonna dumb stop him.

95
00:03:50,540 --> 00:03:53,475
Crazy Ira and the Douche...
93.7.

96
00:03:53,543 --> 00:03:56,111
You know why
I'd never be an astronaut?

97
00:03:56,179 --> 00:03:58,897
Two words... space farts.

98
00:03:58,982 --> 00:04:01,684
You can't roll down
the window of the shuttle.

99
00:04:01,751 --> 00:04:02,718
Am I right, folks?

100
00:04:02,769 --> 00:04:04,436
Houston, we have a...

101
00:04:04,521 --> 00:04:06,689
Now, what do you think
about that, Neil Armstrong?

102
00:04:06,740 --> 00:04:09,858
That's one small...
for man.

103
00:04:09,909 --> 00:04:13,262
One giant...
for mankind.

104
00:04:13,330 --> 00:04:15,230
All right, switching gears
here now.

105
00:04:15,298 --> 00:04:18,083
We got Leslie Knope
and Tom Haverfart...

106
00:04:18,168 --> 00:04:20,569
And, uh, Ben Wyatt,
and they're in the hizzy

107
00:04:20,620 --> 00:04:24,707
to talk about an upcoming event
called the Harvest Festival.

108
00:04:24,774 --> 00:04:26,759
Well, the Douche,
it's a Pawnee tradition,

109
00:04:26,843 --> 00:04:30,279
and it's where fun
meets awesome...

110
00:04:30,347 --> 00:04:31,964
Meets agriculture.

111
00:04:32,048 --> 00:04:34,767
And it is going to be
next month right here in Pawnee.

112
00:04:34,851 --> 00:04:35,951
And spoiler alert...

113
00:04:36,019 --> 00:04:38,287
It's gonna have
the best corn maze ever.

114
00:04:38,355 --> 00:04:40,723
You lost your virginity in
a corn maze, didn't you, Douche?

115
00:04:40,774 --> 00:04:42,157
Oh, that's right, to your mom.

116
00:04:43,860 --> 00:04:46,595
Crazy Ira, clean your room!

117
00:04:46,663 --> 00:04:48,947
There's also gonna be hayrides.

118
00:04:49,032 --> 00:04:51,433
"Hey, ride me"...is what
Crazy Ira's mom said.

119
00:04:51,484 --> 00:04:53,101
Stop it!

120
00:04:54,537 --> 00:04:56,955
China Joe, you are a poet.

121
00:04:58,708 --> 00:05:00,142
My bird is missing.

122
00:05:00,210 --> 00:05:01,810
I need a permit to post signs.

123
00:05:01,878 --> 00:05:04,496
Oh, let me just look
for that form.

124
00:05:04,581 --> 00:05:06,915
There's no time!
He can fly!

125
00:05:06,966 --> 00:05:09,184
Nine things April hates to do.

126
00:05:09,252 --> 00:05:11,503
"Number one, run
the permits desk for an hour."

127
00:05:11,588 --> 00:05:12,988
That's no problem.

128
00:05:13,056 --> 00:05:15,591
I eat running the permits desk
for an hour for breakfast.

129
00:05:15,658 --> 00:05:17,593
Ow, that's... ow.

130
00:05:17,660 --> 00:05:19,194
- Henry, stop it.
- Ow.

131
00:05:19,262 --> 00:05:22,097
Okay, Henry.
Cute kid.

132
00:05:22,148 --> 00:05:25,000
Andy, got a sec?

133
00:05:25,068 --> 00:05:27,969
- Yeah.
- Henry.

134
00:05:28,021 --> 00:05:30,105
Why are you working
the permits desk?

135
00:05:30,173 --> 00:05:33,992
And why was that child
clubbing your nuggets?

136
00:05:34,077 --> 00:05:37,179
I'm trying to stop April
from taking a job with Chris,

137
00:05:37,247 --> 00:05:40,616
so I'm doing everything she
hates doing for the next month.

138
00:05:40,683 --> 00:05:43,368
- That's going to work?
- She said it might.

139
00:05:43,453 --> 00:05:45,704
But it's all I got, man.

140
00:05:47,190 --> 00:05:49,007
Give me the list.

141
00:05:49,092 --> 00:05:50,459
I'll help you.

142
00:05:50,527 --> 00:05:53,495
Oh, my God.
Thank you.

143
00:05:55,865 --> 00:05:57,533
You got it.

144
00:05:57,600 --> 00:05:59,334
No, I don't care
about their relationship.

145
00:05:59,402 --> 00:06:01,904
I just don't want to lose April.

146
00:06:01,971 --> 00:06:06,108
I would never be able to find
a worse assistant.

147
00:06:06,176 --> 00:06:07,609
Okay, thanks, Leslie.

148
00:06:07,677 --> 00:06:10,245
But you know what
we really want to do today

149
00:06:10,313 --> 00:06:13,849
is have a chat
with Ben Wyatt here.

150
00:06:13,900 --> 00:06:15,250
- Come on, Ben.
- Come on, Ben!

151
00:06:15,318 --> 00:06:17,085
I'm more of a behind
the scenes kind of guy.

152
00:06:17,153 --> 00:06:20,255
Well, it's either come with us
or get in the spank chair.

153
00:06:20,323 --> 00:06:22,057
So maybe it's time
for an interview.

154
00:06:22,125 --> 00:06:23,826
Come on, everybody,
give him a hand.

155
00:06:23,877 --> 00:06:26,295
- Come on over, Ben.
- Oh, yeah.

156
00:06:26,362 --> 00:06:28,547
So this guy, Ben Wyatt,

157
00:06:28,631 --> 00:06:30,716
we're looking him up
on Altavista.

158
00:06:30,800 --> 00:06:35,571
He's from some hick town...
Partridge, Minnesota...

159
00:06:35,638 --> 00:06:40,142
and when he's 18,
he's elected mayor.

160
00:06:43,212 --> 00:06:44,880
♪
♪

161
00:06:48,217 --> 00:06:50,486
That's funny.
That's funny.

162
00:06:50,553 --> 00:06:52,588
He's 18, becomes the mayor,

163
00:06:52,655 --> 00:06:56,024
and then blows the whole budget
on an ice-skating rink.

164
00:06:56,075 --> 00:06:57,192
City goes bankrupt.

165
00:06:57,243 --> 00:06:59,828
You're out of there.

166
00:06:59,896 --> 00:07:01,396
What did you call it again, Ben?

167
00:07:01,447 --> 00:07:04,233
What was it called? Say it.

168
00:07:04,300 --> 00:07:05,834
Ice Town.

169
00:07:08,237 --> 00:07:09,738
I was just a...
I was a kid,

170
00:07:09,806 --> 00:07:13,342
and, um, when you... you...

171
00:07:13,409 --> 00:07:16,778
you end up getting out there
and, uh... and there's a...

172
00:07:16,846 --> 00:07:19,081
Oh, I really wish you guys
could see this guy right now.

173
00:07:19,148 --> 00:07:21,099
He is "draunched" is sweat.

174
00:07:21,184 --> 00:07:24,186
There's... there's also gonna be
ponies at the Harvest Festival.

175
00:07:24,253 --> 00:07:27,272
All right, let's take a caller
from Douche nation.

176
00:07:27,357 --> 00:07:29,191
- All right, talk to me, caller.
- What's up, guys?

177
00:07:29,258 --> 00:07:30,926
Just douchein' over here
in Eagleton.

178
00:07:30,977 --> 00:07:32,294
Sweet.

179
00:07:32,362 --> 00:07:34,763
Seems like 18
is pretty young for a mayor.

180
00:07:34,831 --> 00:07:37,766
What were you, like, 12?

181
00:07:37,817 --> 00:07:39,868
The funny, um...

182
00:07:39,936 --> 00:07:41,269
When...

183
00:07:41,321 --> 00:07:44,039
I guess...

184
00:07:44,107 --> 00:07:46,458
The fortunate, um...

185
00:07:46,543 --> 00:07:49,111
Can we just sort of...

186
00:07:50,997 --> 00:07:53,181
Game over, man.
Game over.

187
00:07:54,464 --> 00:07:55,387
Nice job, man.

188
00:07:55,507 --> 00:07:56,975
Was that your first time
talking to other people?

189
00:07:57,042 --> 00:07:58,142
'Cause it came off that way.

190
00:07:58,210 --> 00:07:59,577
You embarrassed me
in front of the Douche.

191
00:07:59,628 --> 00:08:01,346
I'm sorry, you guys.
I just... I haven't had to deal

192
00:08:01,413 --> 00:08:02,847
with that mayor stuff
for 17 years.

193
00:08:02,915 --> 00:08:04,182
I guess I'm not totally over it.

194
00:08:04,249 --> 00:08:06,367
No worries. Look, it was just
a bump in the road.

195
00:08:06,435 --> 00:08:07,819
Okay.

196
00:08:07,886 --> 00:08:09,754
Fortunately, the Leslie-mobile
is an all-terrain vehicle.

197
00:08:09,822 --> 00:08:12,023
So everything involving Ben
is fine.

198
00:08:12,090 --> 00:08:13,675
I just need to talk to you
in private

199
00:08:13,742 --> 00:08:16,861
about something different
that is not Ben related.

200
00:08:16,928 --> 00:08:19,113
Ben, good job.

201
00:08:19,198 --> 00:08:22,200
- Your boy's a nightmare.
- I know, but we need him.

202
00:08:22,267 --> 00:08:24,636
If they ask us about the budget,
he's our numbers guy.

203
00:08:24,703 --> 00:08:25,870
Well, we should just slap

204
00:08:25,937 --> 00:08:27,205
a pair of Ray-Bans
on a calculator,

205
00:08:27,272 --> 00:08:28,539
'cause that would be
way more charming.

206
00:08:28,607 --> 00:08:30,208
I'll do the newspaper interview.

207
00:08:30,276 --> 00:08:32,543
You and Ben do Channel 4.

208
00:08:32,611 --> 00:08:35,113
Perd Hapley is a big softy.
It'll be a puff piece.

209
00:08:35,180 --> 00:08:36,214
He can do it.

210
00:08:36,282 --> 00:08:38,282
So how are you liking Pawnee?

211
00:08:38,334 --> 00:08:40,518
Um, there are a lot of...
A lot of cars.

212
00:08:40,586 --> 00:08:42,887
I mean, not too many...

213
00:08:42,955 --> 00:08:44,722
Trucks and stuff.

214
00:08:44,790 --> 00:08:46,957
But, uh, yeah.

215
00:08:47,009 --> 00:08:48,826
Cool.

216
00:08:48,894 --> 00:08:51,012
- Good talk, Ice Town.
- Okay.

217
00:08:55,283 --> 00:08:58,002
Wow. Who knew that
watercress, garbanzo beans,

218
00:08:58,086 --> 00:08:59,787
and walnuts could complement
each other so well?

219
00:08:59,855 --> 00:09:01,822
Good dressing too, right?

220
00:09:01,890 --> 00:09:03,190
Flaxseed and agave...

221
00:09:03,258 --> 00:09:05,810
same basic ingredients
as bird feed.

222
00:09:07,562 --> 00:09:10,965
I'm just gonna come right out
and say it.

223
00:09:11,033 --> 00:09:12,833
I want to define your <i>bagua.</i>

224
00:09:12,901 --> 00:09:16,637
- Okay. What's that?
- It's a feng shui term.

225
00:09:16,705 --> 00:09:19,973
The energy in this house
is a little stale,

226
00:09:20,025 --> 00:09:22,493
but I think
I could redefine your flow

227
00:09:22,577 --> 00:09:25,212
in a very positive way.

228
00:09:25,280 --> 00:09:27,365
Great.
I'm not really attached

229
00:09:27,449 --> 00:09:29,166
to the way this stuff is arranged

230
00:09:29,251 --> 00:09:33,087
or the stuff itself
or this house even, really.

231
00:09:33,155 --> 00:09:34,338
Great.
Let's do it.

232
00:09:34,423 --> 00:09:37,008
Am I not being obvious enough?

233
00:09:37,092 --> 00:09:38,893
I feel like I'm being obvious.

234
00:09:38,960 --> 00:09:40,528
This just in.

235
00:09:40,595 --> 00:09:43,030
Harvest Festival?
More like Harvest "Bestival."

236
00:09:43,098 --> 00:09:44,932
The Parks Department
has planted the seeds,

237
00:09:45,000 --> 00:09:47,001
and now they're
harvesting the rewards.

238
00:09:47,069 --> 00:09:48,502
Great, you done?

239
00:09:48,553 --> 00:09:50,388
They'll put the "fun"
in funnel cake.

240
00:09:50,472 --> 00:09:51,505
Okay, now I'm done.

241
00:09:51,556 --> 00:09:53,674
- Tell me about Ben Wyatt.
- Sure.

242
00:09:53,742 --> 00:09:55,109
He works
for the state government,

243
00:09:55,177 --> 00:09:57,194
and he's been coming to advise
us about our budget.

244
00:09:57,279 --> 00:09:58,479
Oh, come on, Leslie.

245
00:09:58,547 --> 00:10:00,231
The Douche blew the story
wide open.

246
00:10:00,315 --> 00:10:02,383
Ben Wyatt bankrupts a town
and then comes to Pawnee

247
00:10:02,451 --> 00:10:03,884
to tell us
how to spend our money?

248
00:10:03,952 --> 00:10:05,286
There's no story here, Shauna.

249
00:10:05,354 --> 00:10:07,204
He's just
a dedicated civil servant

250
00:10:07,289 --> 00:10:09,073
who's doing whatever he can
to make sure

251
00:10:09,157 --> 00:10:12,543
that this is the best
Harvest Festival ever.

252
00:10:12,627 --> 00:10:16,030
And he's easy on the eyes too.

253
00:10:16,098 --> 00:10:18,566
Um, what exactly is the nature
of your relationship with him?

254
00:10:18,633 --> 00:10:20,468
Strictly professional,
just friends.

255
00:10:20,535 --> 00:10:21,902
So are you colleagues,
or are you friends?

256
00:10:21,970 --> 00:10:25,973
We are colleagues with benefits.

257
00:10:26,041 --> 00:10:29,076
We're colleagues
who benefit from the fact

258
00:10:29,144 --> 00:10:30,745
that we're also friends.

259
00:10:30,812 --> 00:10:33,564
I'm sorry, Shauna.
I think I need to go.

260
00:10:33,648 --> 00:10:35,249
But, um, thank you so much.

261
00:10:35,317 --> 00:10:36,934
And as always, everything
I said is off the record.

262
00:10:37,018 --> 00:10:38,052
Okay? Bye.

263
00:10:38,120 --> 00:10:40,604
What's the next task?

264
00:10:40,689 --> 00:10:42,323
Oh, we have to write her
grandmother a thank-you note

265
00:10:42,390 --> 00:10:43,941
for a birthday check.

266
00:10:44,025 --> 00:10:45,726
It has to be really good, too,
'cause it's five months late.

267
00:10:45,794 --> 00:10:47,328
Never written
a thank-you note before.

268
00:10:47,395 --> 00:10:48,496
This will be fun.

269
00:10:48,563 --> 00:10:50,331
What does she call her?

270
00:10:50,399 --> 00:10:52,399
Gamma.
No, Nannaw.

271
00:10:52,451 --> 00:10:54,301
Or Nanna...
Gizmo.

272
00:10:54,369 --> 00:10:56,137
Something like that.

273
00:10:59,124 --> 00:11:01,041
This is for your Gizmo.

274
00:11:01,093 --> 00:11:03,477
I think you'll find
it's pretty good.

275
00:11:03,545 --> 00:11:05,513
"Dear April's grandmother."

276
00:11:05,580 --> 00:11:07,181
I said grandfather.

277
00:11:07,248 --> 00:11:09,550
Oh. Oops.
Okay.

278
00:11:09,618 --> 00:11:11,585
"You are a beautiful
and amazing woman... " Man.

279
00:11:11,636 --> 00:11:13,821
"I hope someday I can become
half the woman you are."

280
00:11:13,889 --> 00:11:14,955
He's a man.

281
00:11:15,023 --> 00:11:16,090
"Thank you for the $500."

282
00:11:16,158 --> 00:11:17,258
It was $5.

283
00:11:17,325 --> 00:11:18,976
"Enjoy the mouse rat CD."

284
00:11:19,060 --> 00:11:21,979
He is deaf.

285
00:11:22,063 --> 00:11:25,533
Okay, do you want me to make
those changes, or is it good?

286
00:11:25,600 --> 00:11:27,067
Before we do this next interview,

287
00:11:27,119 --> 00:11:28,953
there's a little secret
I want to let you in on.

288
00:11:29,037 --> 00:11:31,271
It's called
the Haverford Schmooze.

289
00:11:31,323 --> 00:11:32,840
I was just caught off guard.
I'm fine.

290
00:11:32,908 --> 00:11:34,041
Three easy steps...

291
00:11:34,108 --> 00:11:35,743
Smile...

292
00:11:35,794 --> 00:11:39,246
Friendly physical contact,
and flattery.

293
00:11:39,297 --> 00:11:42,216
Hey, Perd, is your dad Robocop?
'Cause you're arms are guns.

294
00:11:42,283 --> 00:11:44,251
Robocop didn't have
guns for arms.

295
00:11:44,302 --> 00:11:46,387
Oh, my God. That's so
not the point, you nerd.

296
00:11:46,454 --> 00:11:48,839
I'm just saying.
Why are we going to a mall?

297
00:11:48,924 --> 00:11:50,508
I'm getting you a new suit.

298
00:11:50,592 --> 00:11:52,176
I've let this go on long enough.

299
00:11:52,260 --> 00:11:54,345
- I like this suit.
- You shouldn't...

300
00:11:54,429 --> 00:11:56,347
'Cause it looks like garbage.

301
00:11:56,431 --> 00:11:58,566
Your suit looks
more like garbage.

302
00:11:58,633 --> 00:12:00,017
- Oh, really?
- Yeah.

303
00:12:00,101 --> 00:12:02,636
'Cause Brooks Brothers Boys
doesn't make garbage.

304
00:12:02,704 --> 00:12:05,573
Okay, this next thing is
a photography-class assignment.

305
00:12:05,640 --> 00:12:06,740
Can you figure that out?

306
00:12:06,808 --> 00:12:09,743
"Encapsulate the spirit
of melancholy."

307
00:12:09,811 --> 00:12:11,212
- Melancholy.
- Give me this.

308
00:12:11,279 --> 00:12:12,480
Easy.

309
00:12:12,547 --> 00:12:14,481
Boom... a sad desk.

310
00:12:14,533 --> 00:12:17,718
Boom... sad wall.

311
00:12:17,786 --> 00:12:20,204
It's art.
Anything is anything.

312
00:12:20,288 --> 00:12:21,622
Oh, right on.

313
00:12:21,690 --> 00:12:23,090
Uh, I'm gonna go drop this off
and pick up the mail.

314
00:12:23,157 --> 00:12:25,526
Sad floor.

315
00:12:25,594 --> 00:12:28,212
It does look sad...
Kind of.

316
00:12:28,296 --> 00:12:31,549
Sorry for stepping on you, floor.

317
00:12:34,302 --> 00:12:35,569
Hey.

318
00:12:35,637 --> 00:12:37,204
You got Andy
doing all your work for you?

319
00:12:37,272 --> 00:12:39,907
Yes. But he said
he wanted to do it.

320
00:12:39,975 --> 00:12:41,742
I'm only doing it
because I'm getting...

321
00:12:41,810 --> 00:12:42,860
Yeah, I don't care.

322
00:12:42,944 --> 00:12:44,445
I got something to add
to your list.

323
00:12:44,512 --> 00:12:47,548
Yep.
There we go.

324
00:12:47,616 --> 00:12:49,250
That's the stuff.

325
00:12:49,317 --> 00:12:51,869
April really does this every day?

326
00:12:51,953 --> 00:12:53,888
Yes.
Work the heel.

327
00:12:53,955 --> 00:12:55,206
Andy, can I be next?

328
00:12:55,290 --> 00:12:57,190
Yeah.

329
00:12:57,242 --> 00:13:00,628
I've got a corn so big
you could slap butter on it.

330
00:13:00,695 --> 00:13:03,196
Welcome to <i>Ya' Heard with Perd.</i>

331
00:13:03,248 --> 00:13:04,532
I'm Perd Hapley.

332
00:13:04,599 --> 00:13:06,700
The story of our guests today
is that they are

333
00:13:06,768 --> 00:13:08,702
from the Department
of Parks and Recreation...

334
00:13:08,770 --> 00:13:10,304
Ben Wyatt and Tom Haverford.

335
00:13:10,372 --> 00:13:12,473
What up, Perd?
Big fan.

336
00:13:12,541 --> 00:13:15,309
Me too, Perd.
What up?

337
00:13:15,376 --> 00:13:16,977
The thing about
this first question is...

338
00:13:17,045 --> 00:13:19,379
I'd like to ask you
about the Harvest Festival.

339
00:13:19,431 --> 00:13:21,382
Perd, it's gonna be amazing...

340
00:13:21,449 --> 00:13:22,816
Carnival rides, games.

341
00:13:22,884 --> 00:13:26,153
Sweetums is even building
a plus-sized roller coaster

342
00:13:26,221 --> 00:13:28,522
for some of Pawnee's obese
thrill seekers.

343
00:13:28,590 --> 00:13:30,057
You must be this wide to ride.

344
00:13:30,125 --> 00:13:32,927
Wow. That's gonna be
a pretty big roller coaster.

345
00:13:32,994 --> 00:13:34,895
Yep.

346
00:13:34,963 --> 00:13:36,396
Hey, guys, how'd it go?

347
00:13:36,448 --> 00:13:38,398
Well, there were
some sticky moments.

348
00:13:38,450 --> 00:13:40,768
Let's go to the videotape.

349
00:13:40,835 --> 00:13:42,570
Ah! Look...

350
00:13:42,637 --> 00:13:44,405
Who hasn't had gay thoughts?

351
00:13:44,472 --> 00:13:45,673
Who?

352
00:13:45,740 --> 00:13:46,957
- You okay?
- Yeah, I'm fine, I'm fine.

353
00:13:47,042 --> 00:13:48,309
I mean, you know,
sometimes I feel like

354
00:13:48,376 --> 00:13:49,476
I might need glasses.

355
00:13:49,544 --> 00:13:52,012
Is there a bird in here?

356
00:13:52,080 --> 00:13:54,465
I swear I keep seeing a bird
in the studio.

357
00:13:54,549 --> 00:13:56,016
How did this happen?

358
00:13:56,084 --> 00:13:57,685
He was fine until Perd
started asking him

359
00:13:57,752 --> 00:13:59,086
about the boy-mayor stuff.

360
00:13:59,137 --> 00:14:01,188
What's wrong with you?
You look psychotic.

361
00:14:01,256 --> 00:14:03,691
I was 18
when I was elected mayor, okay?

362
00:14:03,758 --> 00:14:05,859
So excuse me for that.

363
00:14:05,927 --> 00:14:08,028
Cindy Eckert had just turned me
down for senior prom.

364
00:14:08,096 --> 00:14:09,863
Do you know how that feel...
I should call her.

365
00:14:09,931 --> 00:14:11,699
I should. I...
No, I shouldn't.

366
00:14:11,766 --> 00:14:14,785
And I'm not going to, and
I'm proud of myself for that.

367
00:14:14,869 --> 00:14:17,538
And then he talked
about feeling up Cindy Eckert

368
00:14:17,606 --> 00:14:19,873
for the first time
for about five minutes.

369
00:14:19,941 --> 00:14:21,542
And then the show ended...

370
00:14:21,610 --> 00:14:24,044
As did our careers
and probably Harvest Fest.

371
00:14:25,880 --> 00:14:28,449
Boom... sadness.
That's the one.

372
00:14:32,342 --> 00:14:34,156
Natalie. Hey.

373
00:14:34,276 --> 00:14:35,443
It's me, Andy.

374
00:14:35,510 --> 00:14:37,028
You're an hour and half late.

375
00:14:37,096 --> 00:14:38,096
I know. I know.

376
00:14:38,164 --> 00:14:39,998
I had to massage a ton of feet...

377
00:14:40,049 --> 00:14:41,600
Super sorry.

378
00:14:41,668 --> 00:14:43,702
Come on, get in. I-I need to do
everything on April's list,

379
00:14:43,770 --> 00:14:45,537
or she's not gonna
go out with me.

380
00:14:45,605 --> 00:14:47,272
You're into April?

381
00:14:47,340 --> 00:14:48,807
Ah.
Come on.

382
00:14:48,858 --> 00:14:50,359
We'll talk about it in the car.

383
00:14:50,443 --> 00:14:52,010
Hold on.
I'll get my books.

384
00:14:52,061 --> 00:14:53,445
Okay.

385
00:14:53,513 --> 00:14:55,898
Okay, great.
Call me.

386
00:14:55,965 --> 00:14:58,617
That should be
some damage control.

387
00:14:58,685 --> 00:14:59,918
I'm having dinner
with Perd Hapley.

388
00:14:59,986 --> 00:15:01,103
Tom, what do we have?

389
00:15:01,170 --> 00:15:03,055
I've gotten calls
from a dozen businesses

390
00:15:03,122 --> 00:15:04,590
thinking about pulling
their sponsorships.

391
00:15:04,657 --> 00:15:06,041
Sweetums might cancel
the fat-coaster.

392
00:15:06,125 --> 00:15:07,125
Oh, my God.

393
00:15:07,193 --> 00:15:08,727
Okay, look, we need to focus.

394
00:15:08,795 --> 00:15:10,462
We still have
the <i>Pawnee Today </i>interview.

395
00:15:10,529 --> 00:15:12,030
Well, is it too late to cancel?

396
00:15:12,098 --> 00:15:15,050
Yeah, yeah,
it's too late to cancel.

397
00:15:15,134 --> 00:15:17,202
Joan Callamezzo runs this town.

398
00:15:17,253 --> 00:15:18,437
But, Ben, don't worry about it.

399
00:15:18,504 --> 00:15:19,705
You're not going
in front of the camera.

400
00:15:19,772 --> 00:15:21,390
Fine with me.

401
00:15:21,474 --> 00:15:23,592
Are you trying to lure
this young lady into your van?

402
00:15:23,676 --> 00:15:25,227
Yeah.

403
00:15:25,311 --> 00:15:27,379
But she's being
really difficult about it.

404
00:15:27,447 --> 00:15:29,748
Uh, and it's actually not my van.

405
00:15:29,816 --> 00:15:31,750
Uh, I stole it
from a friend of mine.

406
00:15:31,818 --> 00:15:33,551
I technically
shouldn't be even driving,

407
00:15:33,603 --> 00:15:37,122
because, uh, my license
is crazy expired.

408
00:15:38,358 --> 00:15:39,725
That's, uh, Dwyer.

409
00:15:39,792 --> 00:15:42,194
D-w-y-e-r.

410
00:15:42,245 --> 00:15:43,528
Oh, dude!

411
00:15:43,580 --> 00:15:45,764
Come on,
I got to get out of here!

412
00:15:45,832 --> 00:15:48,333
Natalie, tell this guy
you know me.

413
00:15:51,237 --> 00:15:52,371
Joan!

414
00:15:52,438 --> 00:15:54,306
Oh, I thought
you were Jennifer Aniston

415
00:15:54,374 --> 00:15:56,008
filming a movie here.

416
00:15:57,343 --> 00:15:59,077
Joan, let's make a pact, okay?

417
00:15:59,128 --> 00:16:01,680
If we're both still single
in an hour, let's get married.

418
00:16:01,748 --> 00:16:03,432
Tom, I'm already married.

419
00:16:03,516 --> 00:16:04,933
Oh, that's right, to Seal.

420
00:16:05,018 --> 00:16:06,818
Oh, I confused you
with Heidi Klum again.

421
00:16:06,886 --> 00:16:09,605
- Oh!
- See you later, Joan.

422
00:16:09,689 --> 00:16:11,256
That's how it's done.

423
00:16:11,307 --> 00:16:12,791
Okay, five, four...

424
00:16:12,859 --> 00:16:15,727
Hello, and welcome
to <i>Pawnee Today.</i>

425
00:16:15,795 --> 00:16:17,562
I'm Joan Callamezzo.

426
00:16:17,630 --> 00:16:19,398
Today's guest is Leslie Knope,

427
00:16:19,465 --> 00:16:21,900
who is here to tell us
how this year's Harvest Festival

428
00:16:21,968 --> 00:16:24,069
is going to bankrupt the city.

429
00:16:24,137 --> 00:16:26,038
Well, Joan, actually,
there's a lot

430
00:16:26,105 --> 00:16:28,040
of false information
flying around.

431
00:16:28,107 --> 00:16:30,275
- The Harvest Festival is...
- Just jumping right in.

432
00:16:30,343 --> 00:16:31,677
That's rude.

433
00:16:31,744 --> 00:16:33,211
I just want everyone to know

434
00:16:33,279 --> 00:16:35,080
what an extraordinary event
this is going to be.

435
00:16:35,131 --> 00:16:37,316
But at what cost?

436
00:16:37,383 --> 00:16:40,135
How many cities
does Ben Wyatt need to destroy

437
00:16:40,219 --> 00:16:42,087
before he's put behind bars?

438
00:16:42,155 --> 00:16:43,655
Ben Wyatt has done nothing wrong.

439
00:16:43,723 --> 00:16:47,225
You know, if you want to ask
him questions,

440
00:16:47,293 --> 00:16:48,560
let's go for it, huh?

441
00:16:48,628 --> 00:16:50,362
Just ask any question
about his past or present.

442
00:16:50,430 --> 00:16:52,147
Just get it over with.
Ben, let's get up here.

443
00:16:52,231 --> 00:16:54,066
Yeah, yeah, come on.

444
00:16:54,133 --> 00:16:55,567
We'll just ask him
a bunch of questions,

445
00:16:55,635 --> 00:16:57,402
and then we'll get everything
cleared and out of the way.

446
00:16:57,470 --> 00:16:58,704
Sound good?
Yeah?

447
00:16:58,771 --> 00:16:59,821
- Great.
- Good.

448
00:16:59,906 --> 00:17:01,440
Uh, let's take some calls, Joan.

449
00:17:01,491 --> 00:17:02,808
Why don't you?

450
00:17:02,875 --> 00:17:04,276
Okay.
Uh, caller, are you there?

451
00:17:04,327 --> 00:17:07,379
Yeah, how can you justify
raising taxes

452
00:17:07,447 --> 00:17:09,982
for something
that will hurt the town?

453
00:17:10,049 --> 00:17:13,218
I...

454
00:17:13,286 --> 00:17:15,554
For...

455
00:17:15,621 --> 00:17:18,423
Uh, just to add
to what Ben's stammering about,

456
00:17:18,491 --> 00:17:20,892
um, we aren't going
to raise taxes,

457
00:17:20,960 --> 00:17:23,328
so that's that.

458
00:17:23,379 --> 00:17:24,463
Next caller.

459
00:17:26,099 --> 00:17:28,567
Do you fish, April?

460
00:17:28,634 --> 00:17:30,335
No.
Fish are gross.

461
00:17:30,386 --> 00:17:32,270
Let me give you
a piece of fishing advice.

462
00:17:32,338 --> 00:17:33,438
I said I don't...

463
00:17:33,506 --> 00:17:34,723
When you have a fish on the line,

464
00:17:34,807 --> 00:17:36,108
you don't just drag it
behind the boat.

465
00:17:36,175 --> 00:17:39,244
You either reel it in,
or you cut him loose,

466
00:17:39,312 --> 00:17:41,279
especially if he's a nice fish

467
00:17:41,347 --> 00:17:43,048
with a big, lovable fish heart.

468
00:17:43,116 --> 00:17:44,983
You don't know
what you're talking about.

469
00:17:45,034 --> 00:17:48,253
Maybe not.
Maybe you really do hate Andy.

470
00:17:48,321 --> 00:17:51,857
Maybe moving to Indianapolis
just to get revenge on him

471
00:17:51,924 --> 00:17:54,192
is a really good idea.

472
00:17:54,260 --> 00:17:56,762
What do I know?

473
00:18:03,202 --> 00:18:05,237
You are going to love this.

474
00:18:05,304 --> 00:18:08,774
Verosian tea really supercharges
the bacteria in your colon.

475
00:18:08,841 --> 00:18:11,042
Plus, it smells interesting.
Waft it.

476
00:18:11,094 --> 00:18:13,378
Are we actually ever
gonna drink it,

477
00:18:13,446 --> 00:18:15,013
or we're just gonna sniff it?

478
00:18:15,064 --> 00:18:17,048
I love sniffing.
Don't get me wrong.

479
00:18:17,100 --> 00:18:20,018
You are hilarious.

480
00:18:20,069 --> 00:18:22,070
So you're leaving soon.

481
00:18:22,155 --> 00:18:24,456
Um, back to Indianapolis briefly

482
00:18:24,524 --> 00:18:26,908
and then on to a town
called Snerling, Indiana,

483
00:18:26,993 --> 00:18:28,093
for several months.

484
00:18:28,161 --> 00:18:29,494
Never heard of it.

485
00:18:29,562 --> 00:18:30,829
It's quite small.

486
00:18:30,896 --> 00:18:32,831
The cows outnumber the people
40 to 1.

487
00:18:32,899 --> 00:18:34,866
It sounds amazing.

488
00:18:34,934 --> 00:18:36,734
I like you a lot.

489
00:18:36,786 --> 00:18:39,771
I love spending time with you.

490
00:18:39,839 --> 00:18:42,340
And I thoroughly
enjoy you, Ann Perkins.

491
00:18:42,408 --> 00:18:46,511
I just think we need to talk
about what that means for us.

492
00:18:46,579 --> 00:18:48,013
I don't want to be clingy.
I...

493
00:18:48,080 --> 00:18:50,048
Please, it is something
that we need to figure out,

494
00:18:50,099 --> 00:18:52,150
and we should do that right now.

495
00:18:52,218 --> 00:18:56,521
I'm so happy you said that.

496
00:18:56,589 --> 00:18:58,190
Wow, that's disgusting.

497
00:18:58,257 --> 00:19:01,626
Yes, it's very hard to drink.

498
00:19:03,296 --> 00:19:05,297
Yeah, why should we trust
this Ben Wyatt guy?

499
00:19:05,364 --> 00:19:07,532
Because I'm trusty.

500
00:19:07,600 --> 00:19:09,334
Trust me.
I'm trustworthy.

501
00:19:09,402 --> 00:19:13,872
And I am working very hard to...

502
00:19:13,940 --> 00:19:17,175
Make sure that this town
gets back on its feet.

503
00:19:17,243 --> 00:19:18,776
There we go.
Okay, great.

504
00:19:18,828 --> 00:19:19,911
Next caller.

505
00:19:19,962 --> 00:19:21,713
So I looked you up on Altavista,

506
00:19:21,781 --> 00:19:23,381
and I found out
that the last seven towns

507
00:19:23,449 --> 00:19:25,550
you've gone to ended up bankrupt.

508
00:19:25,618 --> 00:19:26,802
Okay, first of all,

509
00:19:26,886 --> 00:19:30,055
why does everyone in this town
use Altavista?

510
00:19:30,123 --> 00:19:32,190
Is it 1997?

511
00:19:32,258 --> 00:19:34,626
And second,
I am a budget specialist.

512
00:19:34,694 --> 00:19:37,496
I went to those towns
because they were bankrupt,

513
00:19:37,563 --> 00:19:39,231
and now they aren't.

514
00:19:39,298 --> 00:19:41,266
And, yeah, I screwed up
when I was 18,

515
00:19:41,317 --> 00:19:44,402
but who doesn't do dumb stuff
when they're 18?

516
00:19:44,470 --> 00:19:46,571
- Joan?
- I stole my...

517
00:19:46,639 --> 00:19:49,841
gym teacher's husband.

518
00:19:49,909 --> 00:19:52,577
So there you go.

519
00:19:52,645 --> 00:19:55,447
Well, what else
do you got, callers?

520
00:19:55,498 --> 00:19:56,515
Going once, going twi...

521
00:19:56,582 --> 00:19:58,783
- Oh, here we go.
- Yeah, hi.

522
00:19:58,835 --> 00:20:00,418
Um, can you tell me more
about the corn maze

523
00:20:00,486 --> 00:20:01,753
at the Harvest Festival?

524
00:20:01,821 --> 00:20:03,221
Are dogs allowed?

525
00:20:06,259 --> 00:20:07,893
- Hello.
- Hello.

526
00:20:07,960 --> 00:20:09,394
How did it go today?

527
00:20:09,462 --> 00:20:10,795
It was super fun.

528
00:20:10,863 --> 00:20:12,330
- It was?
- Yeah.

529
00:20:12,398 --> 00:20:14,766
Do you have a list for tomorrow?

530
00:20:16,335 --> 00:20:18,103
You want to do this again
tomorrow?

531
00:20:18,171 --> 00:20:19,771
Yes, I do.
I mean, that's the deal, right?

532
00:20:19,839 --> 00:20:21,139
I got to do it for a whole month

533
00:20:21,207 --> 00:20:24,910
and then you're not gonna move
to Indianapolis...

534
00:20:25,030 --> 00:20:30,515
<font color="#ec14bd">Sync by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

535
00:20:31,183 --> 00:20:32,984
I better be the only person
you kissed today.

536
00:20:33,052 --> 00:20:34,953
I am...

537
00:20:35,021 --> 00:20:36,988
I'm positive that you are.

538
00:20:40,242 --> 00:20:41,345
If I thought you were serious,

539
00:20:41,346 --> 00:20:42,812
I'd be offended, Perd,
but I know that we're buddies,

540
00:20:42,879 --> 00:20:44,996
and I know
you wouldn't do that to me.

541
00:20:44,997 --> 00:20:47,816
But, no, this is a birthmark,
Perd, okay?

542
00:20:47,883 --> 00:20:49,984
This is the little scar I got
when I was nine

543
00:20:50,052 --> 00:20:53,638
and I fell off my bike,
so, no, I'm not perfect!

544
00:20:53,722 --> 00:20:55,807
- I can't look away.
- It's a... it's amazing.

545
00:20:55,891 --> 00:20:57,625
Okay, wait, wait, wait, no.
Here's the best part.

546
00:20:57,693 --> 00:21:01,162
Okay, uh, that's all the time
we have here on <i>Ya' Heard.</i>

547
00:21:01,230 --> 00:21:03,314
I'm Perd Hapley, Channel 4.

548
00:21:03,399 --> 00:21:05,850
More like "Turd Crapley."

549
00:21:05,935 --> 00:21:07,535
- Yes, yes.
- Yeah?

550
00:21:07,603 --> 00:21:09,804
- Bravo.
- Thank you very much.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
