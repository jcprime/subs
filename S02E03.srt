1
00:00:01,335 --> 00:00:03,127
Good morning.
Couple announcements.

2
00:00:03,295 --> 00:00:06,124
As we all know, Ron is recovering
from his hernia surgery,

3
00:00:06,249 --> 00:00:08,164
so I got him flowers from all of us.

4
00:00:08,289 --> 00:00:10,018
So everybody needs to pitch in $90.

5
00:00:10,143 --> 00:00:11,330
$90?

6
00:00:12,346 --> 00:00:15,943
Yes, because I ordered
a beautiful bouquet of daffodils

7
00:00:16,411 --> 00:00:19,185
from a website
after a few glasses of wine.

8
00:00:19,817 --> 00:00:22,523
So, Tom, I think you might be
getting some daffodils too.

9
00:00:22,648 --> 00:00:24,437
Donna,
you're definitely getting some.

10
00:00:24,562 --> 00:00:27,193
Jerry, I don't know, I'm not sure.
Time will tell.

11
00:00:28,076 --> 00:00:29,599
Also, I'm leaving early tonight

12
00:00:29,724 --> 00:00:32,791
because I am a judge
in the Miss Pawnee beauty pageant.

13
00:00:33,534 --> 00:00:34,992
You get to be a judge?

14
00:00:35,160 --> 00:00:37,662
Yes, and it's a responsibility
I take very seriously.

15
00:00:37,830 --> 00:00:39,038
I want to be a judge.

16
00:00:39,206 --> 00:00:41,103
I can't believe
you like beauty pageants.

17
00:00:41,708 --> 00:00:42,996
Whoever Miss Pawnee is,

18
00:00:43,121 --> 00:00:45,461
is gonna be the representative
of womanhood in our town.

19
00:00:45,921 --> 00:00:47,868
And let me assure you
that this year's

20
00:00:47,993 --> 00:00:50,622
Miss Pawnee will be chosen
for her talent and poise.

21
00:00:50,747 --> 00:00:53,008
The girls from Talent and Poise
are gonna be there?

22
00:00:53,133 --> 00:00:54,773
- What?
- Talent and Poise.

23
00:00:54,898 --> 00:00:56,799
It's a strip club
by the V.A. Hospital.

24
00:00:56,924 --> 00:00:59,266
- I have some meetings there.
- That's disgusting.

25
00:00:59,434 --> 00:01:01,977
No, what's disgusting
is the Glitter Factory.

26
00:01:02,102 --> 00:01:04,021
Do not go to the Glitter Factory.

27
00:01:39,893 --> 00:01:42,984
So I was just at the grind,
and I thought you might want

28
00:01:43,109 --> 00:01:46,063
an iced mocha with extra,
extra whipped cream.

29
00:01:46,574 --> 00:01:48,905
Oh, my.
Thank you so much, April.

30
00:01:49,533 --> 00:01:50,694
You're welcome.

31
00:01:50,819 --> 00:01:51,944
By the way,

32
00:01:52,379 --> 00:01:53,833
completely unrelated,

33
00:01:53,958 --> 00:01:57,116
I just signed up
for the Miss Pawnee beauty pageant.

34
00:01:57,325 --> 00:01:58,617
That's wonderful.

35
00:01:58,982 --> 00:02:01,513
That is why I decided
to become a judge.

36
00:02:01,863 --> 00:02:03,609
So that awesome girls like you,

37
00:02:03,734 --> 00:02:05,916
who are not, you know,
classically hot,

38
00:02:06,084 --> 00:02:08,836
can be rewarded
for their intelligence and savvy.

39
00:02:09,004 --> 00:02:11,005
Beauty pageants are idiotic,

40
00:02:11,173 --> 00:02:13,719
but I found out that the winner
of the Miss Pawnee pageant

41
00:02:13,844 --> 00:02:15,301
gets $600.

42
00:02:15,469 --> 00:02:17,538
I can be idiotic for $600.

43
00:02:17,888 --> 00:02:20,300
So are you gonna vote
for me, sister?

44
00:02:20,523 --> 00:02:23,408
April, it's unethical for me
to show you favoritism.

45
00:02:23,922 --> 00:02:25,352
You and I are like family.

46
00:02:26,884 --> 00:02:29,315
- The coffee's $7.
- Yes, right, of course.

47
00:02:29,955 --> 00:02:33,180
Just bump that clown. Tell him
they already have an Asian judge.

48
00:02:34,363 --> 00:02:35,939
Awesome.
Thank you so much.

49
00:02:36,064 --> 00:02:38,258
I owe you.
A'ight, peace.

50
00:02:38,743 --> 00:02:41,619
Guess who's also gonna be
a judge in the beauty pageant.

51
00:02:41,879 --> 00:02:44,038
- What? How?
- I know a guy.

52
00:02:44,206 --> 00:02:45,831
I had to call in a few favors.

53
00:02:45,999 --> 00:02:48,226
But if you don't do it
to look at women in bikinis

54
00:02:48,351 --> 00:02:51,545
and assign them numerical grades,
what the hell do you call in favors for?

55
00:02:56,483 --> 00:02:58,218
What you doing in these parts?

56
00:02:58,459 --> 00:03:01,388
I just... I came by
to see the murals.

57
00:03:01,842 --> 00:03:03,328
This one's pretty amazing.

58
00:03:03,677 --> 00:03:05,017
Yeah, this one's a beauty.

59
00:03:05,442 --> 00:03:07,686
In the 1880s, there were a few years

60
00:03:07,854 --> 00:03:10,021
that were pretty rough
and tumble in Pawnee.

61
00:03:10,315 --> 00:03:12,194
This depicts kind of a famous fight

62
00:03:12,319 --> 00:03:16,185
between reverend Bradley
and Anna Beth Stevenson,

63
00:03:16,676 --> 00:03:18,030
a widowed mother of seven.

64
00:03:18,451 --> 00:03:21,367
The original title of this
was "a lively fisting".

65
00:03:22,575 --> 00:03:25,204
But they had to change it.
For obvious reasons.

66
00:03:25,485 --> 00:03:27,441
She's got him
by the hair pretty good there.

67
00:03:28,052 --> 00:03:29,431
Leslie's really cool.

68
00:03:29,556 --> 00:03:30,898
And she's smart.

69
00:03:32,093 --> 00:03:33,212
It's a little intimidating.

70
00:03:34,277 --> 00:03:37,200
To tell you the truth, I didn't
come here to look at the murals.

71
00:03:37,325 --> 00:03:39,043
I came to ask her on a date.

72
00:03:44,890 --> 00:03:46,475
I really...
I really like you.

73
00:03:47,315 --> 00:03:48,894
And I was wondering

74
00:03:49,062 --> 00:03:51,814
if you maybe want to get
a cup of coffee some time.

75
00:03:52,672 --> 00:03:53,902
Okay. Yeah, sure.

76
00:03:54,941 --> 00:03:56,193
Yeah, absolutely.

77
00:03:56,361 --> 00:03:58,101
Let me check my schedule.

78
00:03:59,114 --> 00:04:00,146
Let's see...

79
00:04:01,589 --> 00:04:03,325
Is that your grandma?

80
00:04:10,255 --> 00:04:11,542
It's Madeleine Albright.

81
00:04:12,045 --> 00:04:13,269
That's her name?

82
00:04:14,026 --> 00:04:15,295
I call mine nana.

83
00:04:15,599 --> 00:04:17,424
No, that's Madeleine Albright.

84
00:04:17,866 --> 00:04:19,789
First female Secretary of State.

85
00:04:22,031 --> 00:04:24,232
OK, so not...
That's not your grandma then.

86
00:04:24,357 --> 00:04:25,695
All right.
I got it.

87
00:04:27,186 --> 00:04:29,732
- How's that schedule looking?
- Good. A couple days are free.

88
00:04:29,857 --> 00:04:31,186
Maybe we could just...

89
00:04:31,354 --> 00:04:34,415
- Lock it in later? Talk later about it?
- Yeah, that's a good idea.

90
00:04:38,063 --> 00:04:38,944
Ladies.

91
00:04:43,426 --> 00:04:44,798
Wanna hear something awful?

92
00:04:45,288 --> 00:04:47,119
He didn't even know
Madeleine Albright.

93
00:04:47,675 --> 00:04:49,201
- Who?
- Not you too.

94
00:04:49,326 --> 00:04:51,290
Madeleine Albright,
1st female Secretary of State.

95
00:04:51,458 --> 00:04:52,624
No, who didn't know?

96
00:04:53,342 --> 00:04:55,571
Dave, that cop that I met.

97
00:04:56,284 --> 00:04:59,506
I just don't know if I can date
someone who doesn't share my interests.

98
00:05:00,180 --> 00:05:03,135
I mean, could you date
someone who doesn't

99
00:05:03,536 --> 00:05:05,220
love giving vaccinations?

100
00:05:05,567 --> 00:05:08,223
I've never dated anyone
who loves giving vaccinations.

101
00:05:13,105 --> 00:05:15,428
Weird question for you.
Are you handy?

102
00:05:15,553 --> 00:05:17,191
Like, can you fix things?

103
00:05:17,359 --> 00:05:19,132
- What's broken?
- My shower.

104
00:05:19,257 --> 00:05:22,357
It's leaky, low pressure,
just all around terrible.

105
00:05:23,045 --> 00:05:24,990
Easy.
You want me to come over after work?

106
00:05:26,474 --> 00:05:27,492
Yes, amazing.

107
00:05:27,846 --> 00:05:29,988
I will cook you
a cheap, quick dinner

108
00:05:30,113 --> 00:05:32,372
- that will be no trouble at all for me.
- Great.

109
00:05:32,679 --> 00:05:35,626
Well, I'll see you tonight
for the weirdest second date ever.

110
00:05:35,794 --> 00:05:36,794
Okay.

111
00:05:37,420 --> 00:05:38,587
Hello, fellow judges.

112
00:05:39,380 --> 00:05:42,150
I'm judge Leslie Knope,
and this is my colleague,

113
00:05:42,275 --> 00:05:44,051
Tom Haverford,
who's gonna be a judge.

114
00:05:44,219 --> 00:05:46,910
Hi, Jessica Wicks, miss Pawnee 1994.

115
00:05:47,110 --> 00:05:48,652
I recognized you right away.

116
00:05:48,777 --> 00:05:52,559
And may I say that you look
more beautiful now than when you won.

117
00:05:53,561 --> 00:05:54,561
I like you.

118
00:05:54,937 --> 00:05:56,980
Ray Holstead, Ray's Sandwich Place.

119
00:05:57,149 --> 00:05:58,816
Yes, of course, Ray.
How do you do?

120
00:05:58,941 --> 00:06:00,103
I'm Charles Woliner.

121
00:06:00,228 --> 00:06:03,411
I've judged every Miss Pawnee pageant
for the last 30 years.

122
00:06:03,703 --> 00:06:05,271
I made Jessica.

123
00:06:06,035 --> 00:06:08,718
Well, Tom and I
are very happy to be here.

124
00:06:08,843 --> 00:06:10,827
And I'm sure,
between the five of us,

125
00:06:10,996 --> 00:06:14,414
we will choose the most well-rounded,
intelligent, modern,

126
00:06:14,583 --> 00:06:17,376
and forward-thinking woman
to represent our fine town.

127
00:06:18,017 --> 00:06:19,017
Right, Tom?

128
00:06:19,583 --> 00:06:21,171
You don't believe me?
Watch.

129
00:06:21,887 --> 00:06:23,956
34c, 36b,

130
00:06:24,274 --> 00:06:26,051
34b, 34d.

131
00:06:26,852 --> 00:06:28,720
32a?
How'd you get in here?

132
00:06:29,596 --> 00:06:30,597
I'm kidding.

133
00:06:30,765 --> 00:06:33,016
You're perfect, each and every 
one of you. God bless.

134
00:06:34,454 --> 00:06:37,653
Here they are,
your candidates for Miss Pawnee.

135
00:06:45,655 --> 00:06:47,030
Let's meet the girls.

136
00:06:47,199 --> 00:06:49,719
First up,
please welcome April Ludgate.

137
00:06:53,705 --> 00:06:56,251
Hello, I'm April Ludgate.

138
00:07:02,008 --> 00:07:03,755
I'm 20 years old,

139
00:07:04,493 --> 00:07:05,340
I like...

140
00:07:07,036 --> 00:07:08,036
people...

141
00:07:08,561 --> 00:07:09,561
places...

142
00:07:10,675 --> 00:07:11,725
and things.

143
00:07:12,317 --> 00:07:15,434
And Pawnee
is my favorite place in the world.

144
00:07:19,064 --> 00:07:21,272
Next up,
please welcome Susan Gleever.

145
00:07:21,712 --> 00:07:23,399
Hi, everyone, I'm Susan.

146
00:07:23,939 --> 00:07:26,400
I'm a history major
at Indiana state,

147
00:07:26,719 --> 00:07:28,279
I play classical piano,

148
00:07:28,595 --> 00:07:29,780
and I volunteer

149
00:07:30,023 --> 00:07:31,657
at the children's hospital.

150
00:07:35,288 --> 00:07:36,750
- She's good.
- Next,

151
00:07:37,290 --> 00:07:38,410
Trish Ianetta.

152
00:07:43,681 --> 00:07:44,681
Hi, y'all.

153
00:07:45,064 --> 00:07:46,046
I'm Trish.

154
00:07:46,436 --> 00:07:47,685
I'm 22 years old.

155
00:07:47,810 --> 00:07:49,360
I've been on <i>Youtube</i>.

156
00:07:49,581 --> 00:07:51,790
I just love to hang out
with my friends.

157
00:07:51,915 --> 00:07:53,012
I love to laugh.

158
00:07:53,181 --> 00:07:55,048
I love the summertime

159
00:07:55,173 --> 00:07:56,766
and going to the beach,

160
00:07:57,310 --> 00:07:59,354
and I love wearing bikinis
at the beach

161
00:07:59,479 --> 00:08:00,811
with everyone there.

162
00:08:01,765 --> 00:08:03,980
They can't all be winners.

163
00:08:04,105 --> 00:08:05,932
Are y'all having a good time?

164
00:08:12,408 --> 00:08:14,450
Looks like we got a frontrunner.

165
00:08:18,122 --> 00:08:20,831
And now it's time
for our ever-popular talent competition

166
00:08:21,339 --> 00:08:23,288
where our ladies
will each demonstrate

167
00:08:23,413 --> 00:08:24,919
their excellent skills.

168
00:08:25,437 --> 00:08:27,885
Here we have,
Leslie's custom score card

169
00:08:28,283 --> 00:08:30,176
with categories such as

170
00:08:30,503 --> 00:08:31,581
presentation,

171
00:08:31,706 --> 00:08:32,805
intelligence,

172
00:08:32,930 --> 00:08:34,531
knowledge of "herstory,"

173
00:08:34,876 --> 00:08:36,276
fruitful gestures,

174
00:08:36,504 --> 00:08:37,719
<i>je ne sais quol,</i>

175
00:08:37,980 --> 00:08:39,400
and something called

176
00:08:39,525 --> 00:08:41,439
the Naomi Wolf Factor.

177
00:08:41,979 --> 00:08:44,157
And our first one up
is Trish Ianetta,

178
00:08:44,282 --> 00:08:45,814
whose talent is baton.

179
00:08:55,576 --> 00:08:57,345
My girl Trish is talented.

180
00:08:58,705 --> 00:09:00,470
She's not even twirling the baton.

181
00:09:04,252 --> 00:09:05,960
I'm going to do impressions.

182
00:09:06,433 --> 00:09:07,750
Celebrity impressions.

183
00:09:07,875 --> 00:09:09,506
That's wild, wild stuff.

184
00:09:14,328 --> 00:09:16,578
This is an impression of my sister.

185
00:09:18,259 --> 00:09:19,456
Hi, I'm Natalie.

186
00:09:19,581 --> 00:09:21,878
I love Ritalin
and have low self-esteem.

187
00:09:29,610 --> 00:09:32,361
This is an impression of my boss,
Leslie Knope.

188
00:09:33,990 --> 00:09:36,607
Women should do everything.
Check out my four-color pen.

189
00:09:36,732 --> 00:09:39,444
Listen up while I talk
about some really important stuff.

190
00:09:39,569 --> 00:09:41,606
Parks, parks...
Michelle Obama, parks.

191
00:09:41,731 --> 00:09:43,516
Gay penguins, parks, sugar, parks.

192
00:09:44,574 --> 00:09:46,531
She got me.
She got me good.

193
00:10:08,556 --> 00:10:11,151
I'm texting Trish
to tell her how good she did earlier.

194
00:10:13,112 --> 00:10:14,749
No, Susan isn't a perfect 10.

195
00:10:15,072 --> 00:10:17,698
But, in my mind,
Susan is the perfect Miss Pawnee.

196
00:10:17,994 --> 00:10:19,490
Her values are strong.

197
00:10:19,615 --> 00:10:22,001
Her commitment to her job
is very admirable.

198
00:10:22,126 --> 00:10:23,926
She has a real sense of...

199
00:10:26,791 --> 00:10:27,666
Over here.

200
00:10:30,763 --> 00:10:31,879
How's it going?

201
00:10:33,138 --> 00:10:36,092
I hooked your toilet up to your shower.
That's what you wanted, right?

202
00:10:37,133 --> 00:10:38,293
Excellent.

203
00:10:47,053 --> 00:10:48,203
Is that Andy?

204
00:10:58,454 --> 00:11:01,325
I know you're in there
'cause I can see you through the screen.

205
00:11:09,502 --> 00:11:11,168
What's up?
What are you doing?

206
00:11:12,834 --> 00:11:14,171
Do you live down here?

207
00:11:15,291 --> 00:11:16,119
What?

208
00:11:20,541 --> 00:11:21,428
Oh, my God.

209
00:11:22,051 --> 00:11:23,222
You live down here.

210
00:11:24,045 --> 00:11:26,316
I can't believe this is happening.
What?

211
00:11:26,441 --> 00:11:28,810
I can't be at my house
and see my ex-boyfriend

212
00:11:29,148 --> 00:11:32,235
living in a hole
in my backyard like a gopher.

213
00:11:32,514 --> 00:11:34,056
That's so weird.

214
00:11:34,610 --> 00:11:36,735
I have been trying really hard
not to bother you.

215
00:11:37,586 --> 00:11:38,957
When you had that barbecue,

216
00:11:39,082 --> 00:11:41,698
I didn't come up,
even though it smelled so good.

217
00:11:41,867 --> 00:11:43,538
You've been here a whole week?

218
00:11:45,123 --> 00:11:46,588
One week only.

219
00:11:48,490 --> 00:11:51,520
I guess that's the office
that you were telling me about

220
00:11:51,645 --> 00:11:54,169
- that you go to every day.
- Listen.

221
00:11:54,338 --> 00:11:56,880
Are we gonna talk about anything
other than the lies?

222
00:11:57,049 --> 00:11:58,287
I can't do this now.

223
00:11:58,412 --> 00:12:00,176
I've a guy fixing my bathroom.

224
00:12:00,301 --> 00:12:01,401
I got to go.

225
00:12:01,667 --> 00:12:04,555
I got to zoom out too
'cause I have some people coming over,

226
00:12:04,724 --> 00:12:07,018
so why don't you call first,
next time?

227
00:12:07,397 --> 00:12:08,707
You know the way out.

228
00:12:09,938 --> 00:12:12,956
It's time now for the dreaded Q&A.

229
00:12:13,355 --> 00:12:15,942
And the first question goes
to Tom Haverford.

230
00:12:16,152 --> 00:12:17,734
I have one for the hot one.

231
00:12:19,441 --> 00:12:22,441
First off, I just want to say
I'm a little bit surprised

232
00:12:22,809 --> 00:12:24,866
because I didn't think
angels could fly so low.

233
00:12:27,305 --> 00:12:30,243
You truly are a beautiful, 
beautiful woman.

234
00:12:31,795 --> 00:12:32,916
You're so funny.

235
00:12:33,285 --> 00:12:34,435
You're funny.

236
00:12:36,372 --> 00:12:37,629
Do you have a question?

237
00:12:37,798 --> 00:12:39,798
I actually have a real question.

238
00:12:39,967 --> 00:12:41,136
If you don't mind.

239
00:12:42,415 --> 00:12:45,444
Alexis De Tocqueville
called America the great experiment.

240
00:12:45,835 --> 00:12:48,859
What can we do as citizens
to improve on that experiment?

241
00:12:52,216 --> 00:12:54,243
I think that America

242
00:12:54,817 --> 00:12:57,056
is the land of the free,

243
00:12:57,536 --> 00:12:59,402
which is a wonderful thing.

244
00:12:59,908 --> 00:13:01,499
And also the brave,

245
00:13:01,624 --> 00:13:03,224
where people can live.

246
00:13:03,699 --> 00:13:06,875
And no one
can ever take that away from you.

247
00:13:07,187 --> 00:13:08,714
And it never gives up.

248
00:13:09,341 --> 00:13:12,832
But the high birthing rate of immigrants
frightens me.

249
00:13:13,263 --> 00:13:14,916
No offense to anyone out there,

250
00:13:15,085 --> 00:13:17,670
but if it were
up to me and my family,

251
00:13:18,075 --> 00:13:21,257
I would actually call it our America
and not their America.

252
00:13:24,967 --> 00:13:26,094
Don't applaud that.

253
00:13:26,263 --> 00:13:28,513
Don't.
She didn't answer my question.

254
00:13:29,829 --> 00:13:31,308
You don't think it's weird

255
00:13:31,477 --> 00:13:35,004
that my ex-boyfriend lives in a tent
in the pit outside my house?

256
00:13:35,627 --> 00:13:36,634
It's not...

257
00:13:37,279 --> 00:13:38,099
ideal.

258
00:13:39,548 --> 00:13:41,552
Maybe you should try to relax,
maybe...

259
00:13:41,857 --> 00:13:44,155
Take one of those Atavins
I saw in the medicine cabinet.

260
00:13:44,802 --> 00:13:45,511
Dude.

261
00:13:45,636 --> 00:13:47,699
I peeked.
I also didn't see any toothpaste.

262
00:13:47,868 --> 00:13:50,911
- Do you not use toothpaste?
- I always knew he was lazy,

263
00:13:51,080 --> 00:13:53,866
but this is like a new low for him.

264
00:13:55,793 --> 00:13:57,335
We should invite him inside.

265
00:13:57,887 --> 00:13:59,987
Have you not been listening
to anything?

266
00:14:00,112 --> 00:14:02,297
I don't know.
It's raining outside.

267
00:14:02,587 --> 00:14:04,675
And he's living in a pit.

268
00:14:08,306 --> 00:14:12,016
Folks, just a couple more contestants,
and then the judges will decide

269
00:14:12,184 --> 00:14:13,382
our next Miss Pawnee,

270
00:14:13,507 --> 00:14:16,285
a winner of $600
in gift certificates

271
00:14:16,410 --> 00:14:19,649
to Big Archie's Sporting Goods
and Emerson Fencing Company.

272
00:14:20,067 --> 00:14:22,763
What? We don't get cash?
This is for a fence?

273
00:14:22,945 --> 00:14:26,155
It won't cover a whole fence,
but it'll defray the cost considerably.

274
00:14:26,366 --> 00:14:27,548
Oh, my God.

275
00:14:29,856 --> 00:14:30,743
I quit.

276
00:14:31,505 --> 00:14:32,370
I quit.

277
00:14:35,458 --> 00:14:37,738
Okay.
I guess she really is quitting.

278
00:14:37,905 --> 00:14:39,001
No, I didn't win.

279
00:14:39,675 --> 00:14:42,839
But at least I didn't make
any new friendships.

280
00:14:43,348 --> 00:14:45,801
So how long do we have
to pretend to deliberate

281
00:14:45,926 --> 00:14:47,725
till we get back out there?

282
00:14:50,084 --> 00:14:52,372
- What do you mean?
- We're all in agreement.

283
00:14:52,790 --> 00:14:55,587
- The hot one, by a landslide.
- Well, her name is Trish,

284
00:14:55,983 --> 00:14:57,833
and I don't think
we should rush this.

285
00:14:57,958 --> 00:15:01,639
What is there to talk about?
I thought Trish was just adorable.

286
00:15:02,067 --> 00:15:05,697
- Take Susan, for example. I think...
- Susan "boring stories"?

287
00:15:05,864 --> 00:15:08,214
No, it's Trish.
Let's go back out there.

288
00:15:09,091 --> 00:15:10,366
Okay, hold on, everyone.

289
00:15:10,534 --> 00:15:12,616
Hold on.
Everyone, wait, wait.

290
00:15:13,419 --> 00:15:16,156
Look, whoever we choose
is gonna represent

291
00:15:16,281 --> 00:15:18,174
the ideal woman for a year.

292
00:15:18,341 --> 00:15:21,877
She'll be someone that little girls
in south-central Indiana look up to.

293
00:15:22,131 --> 00:15:25,339
Now nobody leaves this room
until we discuss all of it, okay?

294
00:15:25,955 --> 00:15:27,908
Consider yourselves sequestered.

295
00:15:28,177 --> 00:15:30,822
I'm a judge,
so I don't want to sound partial,

296
00:15:31,224 --> 00:15:34,088
but Trish will win this pageant
over my dead body.

297
00:15:37,519 --> 00:15:38,936
Look, I'm the only one here

298
00:15:39,377 --> 00:15:42,189
who has entered and won this contest
in the past.

299
00:15:42,928 --> 00:15:44,734
And I think Trish is a no-brainer.

300
00:15:44,998 --> 00:15:47,069
Jessica, may I ask,
what was your talent?

301
00:15:47,237 --> 00:15:48,728
I packed a suitcase.

302
00:15:49,100 --> 00:15:51,699
I have to say,
Leslie does make a good point.

303
00:15:52,032 --> 00:15:54,241
Trish is not the brightest bulb
in the bunch.

304
00:15:54,408 --> 00:15:57,538
Ray, good. I like what you're saying.
Let's keep talking.

305
00:15:57,706 --> 00:16:00,993
Just stand over there,
and don't drip on anything.

306
00:16:07,700 --> 00:16:10,217
Is Mark the guy
who's fixing your shower?

307
00:16:10,879 --> 00:16:13,773
I don't know about you, Mark,
but I've seen a ton of porn,

308
00:16:13,898 --> 00:16:16,307
and I know what "fixing
your shower" means.

309
00:16:17,476 --> 00:16:19,324
Sorry, you are on a date.
That's cool.

310
00:16:19,618 --> 00:16:20,901
No, it's all right.

311
00:16:21,240 --> 00:16:22,813
Don't let me interrupt your date.

312
00:16:23,450 --> 00:16:25,547
- Dig in.
- I already ate. I'm super full.

313
00:16:25,672 --> 00:16:27,672
I don't...
I didn't want any.

314
00:16:29,029 --> 00:16:31,703
Okay, so it's still 3-2 for Trish.

315
00:16:32,030 --> 00:16:33,859
Guys, all I ask

316
00:16:34,396 --> 00:16:36,362
is that you look into your hearts

317
00:16:36,529 --> 00:16:38,996
and think, really think,
about what you've seen

318
00:16:40,540 --> 00:16:41,957
and ask yourselves,

319
00:16:42,125 --> 00:16:44,293
who is the most impressive woman
here tonight?

320
00:16:46,296 --> 00:16:48,514
It's the hot one, Trish Ianetta.

321
00:17:00,491 --> 00:17:02,314
These are amazing.
Do you...

322
00:17:02,439 --> 00:17:04,694
Do you put a lot of hot sauce
on these or something?

323
00:17:04,861 --> 00:17:07,001
A good chef never reveals
her secrets.

324
00:17:07,126 --> 00:17:08,234
It's hot sauce.

325
00:17:10,924 --> 00:17:13,185
Ann is an extraordinary cook.

326
00:17:16,469 --> 00:17:17,527
Memories.

327
00:17:18,281 --> 00:17:20,273
Do you want some more?
Are you hungry still?

328
00:17:20,398 --> 00:17:21,645
You know what's funny?

329
00:17:21,770 --> 00:17:24,292
I was sitting over here on the couch,
and I was thinking

330
00:17:24,417 --> 00:17:26,502
that there was once a time
when Mark used to be

331
00:17:26,670 --> 00:17:28,128
the stranger in the house.

332
00:17:28,520 --> 00:17:29,547
And now it's me.

333
00:17:35,971 --> 00:17:37,400
Excuse me, everyone.

334
00:17:37,697 --> 00:17:40,303
Why don't we just take a moment
to give a round of applause

335
00:17:40,428 --> 00:17:42,544
to all the contestants this evening?

336
00:17:44,181 --> 00:17:45,563
Especially Susan.

337
00:17:47,041 --> 00:17:51,045
This isn't the first time that
Susans have lost to Trishes.

338
00:17:51,972 --> 00:17:53,553
And it won't be the last.

339
00:17:54,281 --> 00:17:56,379
Susan and I will continue on

340
00:17:56,651 --> 00:18:00,655
until the women of Pawnee are judged
not by the flatness of their tummies,

341
00:18:01,214 --> 00:18:03,831
but by the contents of their brains.

342
00:18:05,285 --> 00:18:06,387
And, Trish,

343
00:18:07,696 --> 00:18:09,295
I may not have voted for you,

344
00:18:09,546 --> 00:18:11,212
but now is a time

345
00:18:11,337 --> 00:18:12,893
for us to come together.

346
00:18:13,650 --> 00:18:16,300
I hope you honor
this crown with dignity

347
00:18:16,470 --> 00:18:17,826
and a devotion to...

348
00:18:19,367 --> 00:18:21,480
One, two, three.

349
00:18:27,404 --> 00:18:29,398
As usual, Ann, delicious meal.

350
00:18:30,421 --> 00:18:33,027
Are we doing anything in the way
of dessert or coffee?

351
00:18:33,530 --> 00:18:34,780
We are leaving.

352
00:18:36,025 --> 00:18:37,275
Okay, I get it.

353
00:18:38,742 --> 00:18:41,648
Don't worry. I'm very good
at picking up signals.

354
00:18:42,577 --> 00:18:45,331
- Let's go, Mark.
- No, dummy, just you.

355
00:18:45,697 --> 00:18:46,415
Okay.

356
00:18:46,758 --> 00:18:47,758
All right.

357
00:18:49,787 --> 00:18:51,170
Take care of yourself.

358
00:18:53,308 --> 00:18:55,564
Just don't worry about 
me, Ann. Okay?

359
00:18:57,409 --> 00:18:58,420
I'll be fine.

360
00:19:02,098 --> 00:19:05,123
That went really well!

361
00:19:06,039 --> 00:19:07,185
We had dinner,

362
00:19:07,352 --> 00:19:08,911
I got to see her.

363
00:19:09,036 --> 00:19:11,281
Oh, God, I was so tempted
to look back at her.

364
00:19:11,406 --> 00:19:12,706
Was she looking?

365
00:19:13,059 --> 00:19:14,589
I had to walk so slow.

366
00:19:16,586 --> 00:19:18,015
A good day.

367
00:19:21,034 --> 00:19:22,596
Well, well, look who's here.

368
00:19:22,763 --> 00:19:24,158
It's officer John McClane.

369
00:19:24,325 --> 00:19:26,080
Welcome to the party, pal.

370
00:19:26,331 --> 00:19:28,121
- Who's that?
- <I>Die Hard.</I>

371
00:19:28,709 --> 00:19:31,085
- Battery?
- Hey, what are you doing here?

372
00:19:31,252 --> 00:19:34,171
The other day I asked you
if you wanted to have coffee?

373
00:19:34,724 --> 00:19:35,706
You said yes.

374
00:19:36,049 --> 00:19:38,462
And then afterwards,
I saw you again,

375
00:19:39,341 --> 00:19:41,274
you acted like
you didn't want to anymore.

376
00:19:41,399 --> 00:19:43,013
We left it kind of open-ended.

377
00:19:43,181 --> 00:19:45,290
- And that's made me uncomfortable.
- OK.

378
00:19:45,415 --> 00:19:48,628
So I thought I'd come in and ask you.
Just get some clearance on this.

379
00:19:48,753 --> 00:19:50,771
- Dave, I like you.
- I like you too.

380
00:19:50,939 --> 00:19:52,898
Okay, so, we'll get coffee then.

381
00:19:53,152 --> 00:19:54,552
But I'm just busy.

382
00:19:55,198 --> 00:19:57,041
Right now, my schedule's just so...

383
00:19:58,839 --> 00:20:00,311
Okay.
Okay, I get it.

384
00:20:00,865 --> 00:20:02,969
Look, I'm not a guy who plays games.

385
00:20:03,136 --> 00:20:06,157
Okay, I just came to tell you
that I like you, and I like coffee,

386
00:20:06,604 --> 00:20:08,546
and if you want to get
some coffee, just...

387
00:20:08,754 --> 00:20:09,998
Here's my number.

388
00:20:12,050 --> 00:20:13,433
Coming through, buddy.

389
00:20:22,426 --> 00:20:23,804
I got your message.

390
00:20:25,116 --> 00:20:26,330
Thanks for coming.

391
00:20:26,455 --> 00:20:28,517
Look, I'd love to go out with you.

392
00:20:28,769 --> 00:20:31,579
- How about Friday for dinner?
- Yeah? Great.

393
00:20:32,590 --> 00:20:36,556
Will Sandra Day
O'Connor and Michelle Obama

394
00:20:37,022 --> 00:20:39,572
and Condoleezza Rice
and Nancy Pelosi...

395
00:20:40,701 --> 00:20:42,054
are they gonna join us?

396
00:20:42,573 --> 00:20:43,948
No, they won't.

397
00:20:44,073 --> 00:20:47,273
Good, 'cause I don't happen to agree
with miss Pelosi's views

398
00:20:47,398 --> 00:20:50,581
about the troubled assets relief
program.

399
00:20:52,183 --> 00:20:54,924
- I looked that up to impress you.
- I figured.

400
00:20:58,089 --> 00:21:00,465
Use my grill, let me do you a favor.

401
00:21:00,633 --> 00:21:03,547
Take a key, just come by,
grill up whatever you want.

402
00:21:03,752 --> 00:21:05,637
This is one of my favorite
pick-up strategies.

403
00:21:05,805 --> 00:21:07,510
I'm constantly giving women my keys.

404
00:21:07,635 --> 00:21:10,100
- Is this your house key?
- I just happen to have a spare.

405
00:21:10,268 --> 00:21:11,532
Come by whenever you want.

406
00:21:11,796 --> 00:21:13,437
So far, none of 'em have shown up.

407
00:21:13,605 --> 00:21:16,815
That's a very appealing offer.
Thank you.

408
00:21:17,232 --> 00:21:18,233
Matter of time.

409
00:21:18,660 --> 00:21:19,660
Hey, Craig.

410
00:21:21,219 --> 00:21:23,739
- 50 more copies, please.
- You got it.

411
00:21:24,842 --> 00:21:26,283
I have been robbed twice.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
