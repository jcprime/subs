1
00:00:00,282 --> 00:00:02,399
<i>We noticed abnormal activity
on your credit card,</i>

2
00:00:02,524 --> 00:00:04,266
<i>so we need you to confirm or deny</i>

3
00:00:04,391 --> 00:00:07,006
<i>- some of the recent charges.</i>
- Okay, thank you.

4
00:00:07,422 --> 00:00:08,758
<i>$20 to Netflix.</i>

5
00:00:09,853 --> 00:00:13,012
- <i>$20 to Blockbuster Online.</i>
- Both?

6
00:00:13,347 --> 00:00:15,723
I needed all 11 discs
of <I>gossip girl</I> at the same time.

7
00:00:16,290 --> 00:00:17,933
<i>$120 in tuition</i>

8
00:00:18,058 --> 00:00:20,908
<i>to Hogwarts School
of Witchcraft and Wizardry.</i>

9
00:00:22,940 --> 00:00:25,816
They give you a little wand
and a diploma, it's fun.

10
00:00:26,047 --> 00:00:27,844
- What'd you major in?
- Potions.

11
00:00:28,631 --> 00:00:31,049
I'm gonna take you off speakerphone.
Go ahead.

12
00:00:31,298 --> 00:00:33,336
<i>Jessica Simpson
clip-in hair extensions.</i>

13
00:00:34,835 --> 00:00:36,148
I wore those once.

14
00:00:36,273 --> 00:00:38,626
It was a money-back guarantee,
but I forgot to return them.

15
00:00:38,751 --> 00:00:40,873
<i>Man-Pillow,
the pillow shaped like a man.</i>

16
00:00:41,883 --> 00:00:43,668
<i>Also something called
"bucket of cake".</i>

17
00:00:43,836 --> 00:00:45,099
You know what?

18
00:00:45,224 --> 00:00:47,963
I think someone stole my credit card
so cancel it.

19
00:00:48,262 --> 00:00:50,192
<i>Do you wanna hear
what else they purchased?</i>

20
00:00:50,317 --> 00:00:52,464
Don't refund anything.
Just cancel the credit card

21
00:00:52,589 --> 00:00:54,652
and we'll all go on with our lives.
Thank you.

22
00:00:55,646 --> 00:00:58,182
So...
What does a man-pillow look like?

23
00:00:58,508 --> 00:00:59,658
Daniel Craig.

24
00:01:01,290 --> 00:01:02,645
It's for my lower back.

25
00:01:02,813 --> 00:01:04,163
- Feygnasse Team -

26
00:01:04,731 --> 00:01:06,315
.:: La Fabrique ::.

27
00:01:07,215 --> 00:01:08,953
Episode 206
<i>Kaboom</i>

28
00:01:09,974 --> 00:01:11,254
mpm

29
00:01:11,947 --> 00:01:13,145
albator1932

30
00:01:13,626 --> 00:01:14,740
Lestat78

31
00:01:23,159 --> 00:01:24,298
My name is Keith,

32
00:01:24,423 --> 00:01:26,085
and I am your friend.

33
00:01:26,253 --> 00:01:27,670
Are you guys my friends?

34
00:01:29,368 --> 00:01:30,339
All right.

35
00:01:30,609 --> 00:01:31,588
Awesome!

36
00:01:31,713 --> 00:01:33,885
It is playground time!

37
00:01:36,402 --> 00:01:38,931
We're here in Eagletown,
it's two towns over.

38
00:01:39,099 --> 00:01:40,933
And we're all volunteering
for Kaboom!

39
00:01:41,229 --> 00:01:44,793
A service organization that says
it builds a playground in a day.

40
00:01:45,077 --> 00:01:45,991
High five.

41
00:01:46,857 --> 00:01:50,151
I never trust anything that quickly.
That's why I don't eat minute rice.

42
00:01:51,403 --> 00:01:53,607
So this whole
build-a-playground-in-a-day thing,

43
00:01:53,732 --> 00:01:55,543
- That's just a slogan.
- Nope

44
00:01:55,831 --> 00:01:57,632
One day, 24 hours.

45
00:01:57,757 --> 00:01:59,357
That is so incredible.

46
00:01:59,482 --> 00:02:02,705
I work in parks, and I know
how hard it is to get something done.

47
00:02:02,873 --> 00:02:04,832
Not if you have the <i>kaboom</i> spirit.

48
00:02:05,000 --> 00:02:07,081
You can look at a problem
and you can either go

49
00:02:07,206 --> 00:02:09,526
"This is a problem",
or you can <i>kaboom</i>!

50
00:02:09,651 --> 00:02:12,381
Blow it up
and turn it into something great.

51
00:02:12,549 --> 00:02:14,849
You literally <i>kaboom</i> the problem.

52
00:02:15,706 --> 00:02:16,802
Come on, people!

53
00:02:16,970 --> 00:02:19,082
I see more ground than playground.

54
00:02:19,596 --> 00:02:20,973
- Kaboom!
- Don't do that.

55
00:02:21,641 --> 00:02:22,947
One, two, three.

56
00:02:24,532 --> 00:02:25,728
This is tough.

57
00:02:25,896 --> 00:02:28,439
But think of all the kids
that will swing on this swing.

58
00:02:28,708 --> 00:02:31,483
Fat kids, skinny kids,
brainiacs, sluts,

59
00:02:31,652 --> 00:02:33,728
the gay drama kids, goths, jocks,

60
00:02:33,853 --> 00:02:35,226
the alternative crowd.

61
00:02:40,145 --> 00:02:42,755
- What are you doing here?
- I'm volunteering.

62
00:02:43,260 --> 00:02:44,783
I love to volunteer.

63
00:02:45,241 --> 00:02:46,681
The key to volunteering?

64
00:02:47,092 --> 00:02:48,274
A lot of pockets.

65
00:02:48,399 --> 00:02:49,878
For putting all the food in.

66
00:02:50,003 --> 00:02:51,889
The red cross has amazing cookies.

67
00:02:52,014 --> 00:02:53,577
I go there all the time.

68
00:02:53,702 --> 00:02:55,621
Meals on wheels was a bonanza.

69
00:02:55,833 --> 00:02:56,926
Suicide hotline,

70
00:02:57,215 --> 00:02:58,887
surprisingly lame spread.

71
00:03:04,075 --> 00:03:07,144
Thought I'd give back
to those less fortunate than myself.

72
00:03:08,113 --> 00:03:09,812
- You live in a pit.
- Not anymore.

73
00:03:09,982 --> 00:03:12,500
Living with the drummer of my band.
Living indoors.

74
00:03:12,830 --> 00:03:13,803
Pretty cool.

75
00:03:13,928 --> 00:03:15,695
Not to brag.
Kinda hard not to.

76
00:03:16,304 --> 00:03:17,541
Anyhow, gotta go.

77
00:03:17,666 --> 00:03:20,775
Me and an old Asian lady
are double-teaming some monkey bars, so.

78
00:03:23,328 --> 00:03:25,434
I got a remote control
and I'm controlling you.

79
00:03:25,559 --> 00:03:26,583
Go faster!

80
00:03:27,541 --> 00:03:29,291
"Go faster, Ron's the master."

81
00:03:34,672 --> 00:03:36,674
You have been officially <i>kaboomed</i>.

82
00:03:37,133 --> 00:03:39,270
Look at you two and your big <i>kaboom</i>!

83
00:03:39,395 --> 00:03:40,511
Great job!

84
00:03:40,787 --> 00:03:41,789
I got gifts.

85
00:03:42,532 --> 00:03:44,976
There you go for you.
And one more for you.

86
00:03:45,929 --> 00:03:48,001
So you guys gonna join us tomorrow
in Muncie?

87
00:03:48,126 --> 00:03:51,148
No, I wish,
but back to the real world.

88
00:03:51,273 --> 00:03:53,433
At least you guys will be
<i>kabooming</i> somewhere.

89
00:03:53,558 --> 00:03:54,540
Remember...

90
00:03:54,877 --> 00:03:57,421
Take a man <i>kabooming</i>,
he <i>kabooms</i> for a day.

91
00:03:57,614 --> 00:03:59,478
But you teach a man how to <i>kaboom</i>...

92
00:04:00,072 --> 00:04:01,615
Kaboom! Kaboom!

93
00:04:03,348 --> 00:04:04,829
I'm so pumped!

94
00:04:05,038 --> 00:04:06,687
God, I could take on the world.

95
00:04:07,039 --> 00:04:09,400
I could prep, like,
1,000 diagnostic tests.

96
00:04:10,184 --> 00:04:12,022
- In an hour!
- That's the spirit.

97
00:04:13,492 --> 00:04:15,336
There it is, the pit.

98
00:04:17,090 --> 00:04:18,991
There's something
I wish I could <i>kaboom</i>.

99
00:04:19,116 --> 00:04:20,087
Well, we are.

100
00:04:20,212 --> 00:04:22,087
It's just a slow boom.

101
00:04:22,968 --> 00:04:24,949
A slower, more deliberate <i>kaboom</i>.

102
00:04:25,074 --> 00:04:26,083
Totally.

103
00:04:26,538 --> 00:04:27,807
See you tomorrow.

104
00:04:37,315 --> 00:04:38,694
You know what this mess is?

105
00:04:38,862 --> 00:04:40,954
This is a list of things
we have to complete

106
00:04:41,079 --> 00:04:43,407
before we fill in
the pit behind Ann's house.

107
00:04:43,816 --> 00:04:47,152
You know what this is?
This is a <i>kaboomer</i>.

108
00:04:54,087 --> 00:04:56,028
Jerry, did you use
permanent marker again?

109
00:04:56,153 --> 00:04:57,396
I'm sorry, guys...

110
00:05:01,020 --> 00:05:03,177
Forget it.
Let's all pretend Jerry wasn't born.

111
00:05:04,304 --> 00:05:05,598
And this is clean.

112
00:05:06,380 --> 00:05:08,430
How do we speed up the process?

113
00:05:09,340 --> 00:05:11,378
How do we <i>kaboom</i> it?

114
00:05:11,770 --> 00:05:14,098
Rules, codes, permits, red tape,

115
00:05:14,223 --> 00:05:17,356
I never realized how frustrating it is
to be in the government.

116
00:05:17,659 --> 00:05:20,340
In my next life,
I'm going into private industry.

117
00:05:21,624 --> 00:05:23,030
Maybe strip mining.

118
00:05:23,385 --> 00:05:25,376
Everybody take out
their thinking caps.

119
00:05:25,764 --> 00:05:26,867
And rip them up.

120
00:05:27,848 --> 00:05:31,026
Then take out your doing caps,
'Cause we're gonna do something today.

121
00:05:31,151 --> 00:05:32,748
I have a couple in my wallet.

122
00:05:32,916 --> 00:05:35,334
- That's what I call condoms.
- Come on, Tom. Focus.

123
00:05:36,376 --> 00:05:39,463
How do we cut through the red tape?
How do we get this pit filled in? Ideas?

124
00:05:39,631 --> 00:05:42,232
We need to cut through the red tape
and get the pit filled in.

125
00:05:42,357 --> 00:05:43,090
Good.

126
00:05:43,260 --> 00:05:45,729
Everybody else needs to participate.
Come on, guys.

127
00:05:45,854 --> 00:05:48,263
These suggestions
aren't gonna suggest themselves.

128
00:05:50,223 --> 00:05:51,973
This idea better be good.

129
00:05:55,021 --> 00:05:56,412
- I support you.
- Good.

130
00:05:56,537 --> 00:05:59,650
You could petition to expedite
the architectural review board process.

131
00:05:59,818 --> 00:06:01,366
Mark, buddy...

132
00:06:01,715 --> 00:06:03,245
You're not listening to me.

133
00:06:03,732 --> 00:06:05,714
I want to <i>kaboom</i> it.

134
00:06:06,905 --> 00:06:08,873
- Can I talk to you outside?
- Sure.

135
00:06:09,244 --> 00:06:11,231
Mark's gonna help me,
so thanks for nothing.

136
00:06:11,356 --> 00:06:12,472
Good job, Ann.

137
00:06:13,947 --> 00:06:16,336
So you really wanna get
this pit filled in?

138
00:06:16,461 --> 00:06:20,228
- And we'll do whatever it takes?
- I am so sick of moving like a slug.

139
00:06:20,353 --> 00:06:21,930
I wanna move like a cheetah.

140
00:06:22,529 --> 00:06:25,104
Or a slug driving
a remote controlled car.

141
00:06:25,229 --> 00:06:27,219
Something more plausible than that,
but fast.

142
00:06:27,527 --> 00:06:30,305
- Would you break the rules?
- I won't murder.

143
00:06:31,111 --> 00:06:32,556
That's good to know.

144
00:06:33,066 --> 00:06:34,916
But it's actually very simple.

145
00:06:35,233 --> 00:06:37,688
If you wanna fill in the pit,
just go fill it in.

146
00:06:37,856 --> 00:06:39,608
Don't ask for permission,

147
00:06:39,733 --> 00:06:41,217
ask for forgiveness.

148
00:06:42,302 --> 00:06:43,699
I like that.

149
00:06:44,334 --> 00:06:47,406
So who gives me the go ahead
to not ask for permission,

150
00:06:47,574 --> 00:06:48,890
because...
Is it Ron?

151
00:06:49,015 --> 00:06:49,976
It's nobody.

152
00:06:50,101 --> 00:06:52,575
It's you.
You have to be bold.

153
00:06:55,841 --> 00:06:57,374
You wanna get the pit filled in?

154
00:06:57,653 --> 00:07:00,907
Then go rent a bulldozer
and fill in the freaking pit.

155
00:07:06,579 --> 00:07:07,551
What's up?

156
00:07:07,719 --> 00:07:10,762
Say you had a friend who wanted to do
something good, but a little risky,

157
00:07:10,930 --> 00:07:12,264
and she was kind of nervous.

158
00:07:12,441 --> 00:07:15,350
And this friend is me.
What should I do?

159
00:07:16,373 --> 00:07:17,562
You should do it.

160
00:07:17,687 --> 00:07:19,243
And ask your friend to help you

161
00:07:19,368 --> 00:07:21,376
because your friend
totally has your back.

162
00:07:21,501 --> 00:07:23,400
- And that friend is me.
- You'll help me?

163
00:07:23,673 --> 00:07:26,294
Of course, I'll do anything I can
to help.

164
00:07:26,419 --> 00:07:27,494
Let's do it!

165
00:07:30,026 --> 00:07:31,876
What are we doing?
Is it dangerous?

166
00:07:32,121 --> 00:07:33,744
We're not gonna murder anyone.

167
00:07:35,455 --> 00:07:37,589
It was crazy.
I didn't get a single permit.

168
00:07:37,714 --> 00:07:41,014
Nobody stamped anything,
all I did was order a backhoe,

169
00:07:41,145 --> 00:07:43,069
and it came with a guy who runs it.

170
00:07:44,686 --> 00:07:46,381
I've never rented a guy before.

171
00:07:47,207 --> 00:07:48,875
Wish I knew about that during prom.

172
00:07:49,000 --> 00:07:51,255
There was a girl at my prom
who was known as the backhoe.

173
00:07:51,380 --> 00:07:53,763
Mary Dunbar,
she'd let anyone massage her back.

174
00:07:53,931 --> 00:07:55,560
I'm so excited we're doing this!

175
00:07:55,685 --> 00:07:57,045
And without permission.

176
00:07:57,170 --> 00:07:58,768
We're giving ourselves permission.

177
00:07:59,181 --> 00:08:01,487
Look!
I had these hard hats made.

178
00:08:01,612 --> 00:08:03,412
Check it out..."kick ass".

179
00:08:03,737 --> 00:08:05,151
I'll take that one.

180
00:08:05,276 --> 00:08:07,415
Oh, my God!
Ann, this is so cool!

181
00:08:07,540 --> 00:08:08,852
Do you feel giddy?

182
00:08:08,977 --> 00:08:10,405
I'm so proud of us.

183
00:08:10,530 --> 00:08:12,908
Ms. Montegue, you ready?

184
00:08:13,951 --> 00:08:16,585
I didn't wanna tell him
my real name, you know?

185
00:08:18,336 --> 00:08:21,036
First speech, easy...
important moment here.

186
00:08:24,220 --> 00:08:26,318
We are about to fill this hole now.

187
00:08:26,574 --> 00:08:27,774
Not with dirt,

188
00:08:28,280 --> 00:08:30,967
but with the courage
of a thousand lions.

189
00:08:31,336 --> 00:08:33,687
And the solemn memory
of all of our friends

190
00:08:33,812 --> 00:08:36,417
who have fallen in this cursed hole.

191
00:08:38,138 --> 00:08:38,975
Dump it!

192
00:08:49,139 --> 00:08:50,153
What the ***?

193
00:08:52,491 --> 00:08:54,371
I'm okay.

194
00:09:04,502 --> 00:09:07,419
That beeping was sure annoying,
but this is even more annoying.

195
00:09:07,714 --> 00:09:08,797
I'll handle this.

196
00:09:09,626 --> 00:09:11,549
The monitor is not a toy, Andy.

197
00:09:11,741 --> 00:09:14,061
Anything is a toy
if you play with it.

198
00:09:14,186 --> 00:09:15,929
Chalk that up to your concussion.

199
00:09:16,682 --> 00:09:19,482
Well, the good news
is your cat scan's clear.

200
00:09:20,290 --> 00:09:21,101
Cool.

201
00:09:21,300 --> 00:09:24,355
Andy, I'm really happy you're okay.
Leslie was worried too.

202
00:09:24,480 --> 00:09:27,691
We should've checked before
to see if you were... home.

203
00:09:28,040 --> 00:09:31,258
I moved out. I had to go back
'cause I forgot my headphones.

204
00:09:31,383 --> 00:09:33,342
And I layed down
on this really comfy tarp

205
00:09:33,467 --> 00:09:35,865
I saw an old cereal box
I hadn't read before,

206
00:09:36,033 --> 00:09:37,534
so I must've drifted off.

207
00:09:37,702 --> 00:09:39,018
Next thing you know...

208
00:09:41,768 --> 00:09:42,701
Dirt.

209
00:09:43,516 --> 00:09:45,166
Scoot up for one sec.

210
00:09:46,299 --> 00:09:47,512
And back down.

211
00:09:48,228 --> 00:09:49,378
There you go.

212
00:09:50,047 --> 00:09:51,347
That is perfect.

213
00:09:51,823 --> 00:09:53,873
It's like I'm lying on a cloud.

214
00:09:54,461 --> 00:09:55,761
This is awesome.

215
00:09:56,807 --> 00:09:59,057
Ann and Andy, just like old times.

216
00:10:00,560 --> 00:10:04,321
The pit works in mysterious ways.

217
00:10:06,248 --> 00:10:09,279
I'm sorry about Andy,
but I'm not sorry about what I did.

218
00:10:09,970 --> 00:10:11,270
What's my crime?

219
00:10:11,467 --> 00:10:12,467
I got bold?

220
00:10:13,535 --> 00:10:14,949
And a little negligent?

221
00:10:16,713 --> 00:10:18,241
My office, now!

222
00:10:20,536 --> 00:10:22,662
- Can I smoke in here?
- You don't smoke.

223
00:10:22,830 --> 00:10:24,375
Just asking if I can.

224
00:10:24,868 --> 00:10:25,971
Are you high?

225
00:10:26,320 --> 00:10:27,770
I'm high on kaboom!

226
00:10:28,311 --> 00:10:30,596
Don't ask for permission,
ask for forgiveness.

227
00:10:30,721 --> 00:10:33,869
That's right, you never did
ask me for permission, did you?

228
00:10:33,994 --> 00:10:35,862
I'm sorry to burst your ka-bubble,

229
00:10:35,987 --> 00:10:39,982
but I just had my ass ka-handed
to me by the city manager,

230
00:10:40,174 --> 00:10:42,474
and now this entire
department is ka-screwed.

231
00:10:44,950 --> 00:10:46,728
Ron, I'm so, so, so sorry.

232
00:10:47,354 --> 00:10:49,814
What the ka-*** were you thinking?

233
00:10:50,054 --> 00:10:53,004
I would prefer that
she ask me for my permission

234
00:10:53,276 --> 00:10:54,577
so I can say no.

235
00:10:54,702 --> 00:10:56,101
I like saying no.

236
00:10:57,706 --> 00:10:59,556
It lowers their enthusiasm.

237
00:11:01,760 --> 00:11:05,163
So I brought you some clean clothes
that I found at my house.

238
00:11:05,331 --> 00:11:08,249
My God, my sexy sweater!
It has a hole cut out over the bicep.

239
00:11:08,565 --> 00:11:10,084
- Thank you.
- No problem.

240
00:11:10,771 --> 00:11:13,460
No, a-cakes, seriously.

241
00:11:14,354 --> 00:11:15,354
Thank you.

242
00:11:15,479 --> 00:11:16,508
You're welcome.

243
00:11:22,284 --> 00:11:25,350
What do you... what do you
think is happening right now?

244
00:11:25,475 --> 00:11:26,976
A tragic accident...

245
00:11:27,667 --> 00:11:28,969
reignited feelings

246
00:11:29,094 --> 00:11:32,644
you thought were long dead
and we are finally back together.

247
00:11:32,963 --> 00:11:35,276
Andy...
we're not back together.

248
00:11:35,988 --> 00:11:38,404
You know that
I'm kind of with Mark now.

249
00:11:38,890 --> 00:11:40,490
God, is that still happening?

250
00:11:40,963 --> 00:11:42,033
I don't get it.

251
00:11:42,491 --> 00:11:45,161
- What does he have that I don't have?
- Are you serious?

252
00:11:46,539 --> 00:11:47,418
Everything.

253
00:11:47,564 --> 00:11:50,041
He has literally everything
that you don't have:

254
00:11:50,247 --> 00:11:51,944
a job, a car,

255
00:11:52,069 --> 00:11:53,670
a steady income,

256
00:11:53,795 --> 00:11:54,796
an address,

257
00:11:55,179 --> 00:11:57,629
a second pair of shoes,
table manners,

258
00:11:57,799 --> 00:12:00,893
the ability to say
"tampon" without giggling.

259
00:12:01,726 --> 00:12:04,197
Mark has his life together.

260
00:12:07,408 --> 00:12:09,126
I'll get you a different nurse.

261
00:12:12,526 --> 00:12:14,252
It's awful what happened to Andy.

262
00:12:14,567 --> 00:12:17,752
But sometimes when you make an omelet,
you gotta break a few eggs.

263
00:12:17,877 --> 00:12:20,314
What's the alternative,
no omelets at all?

264
00:12:20,715 --> 00:12:22,814
Who wants to live in
that kind of world?

265
00:12:22,944 --> 00:12:24,044
Maybe birds.

266
00:12:24,169 --> 00:12:25,894
Then all their babies would live.

267
00:12:29,254 --> 00:12:31,685
Hi, Scott Braddock, city attorney.

268
00:12:33,008 --> 00:12:36,044
- I didn't know that you were friends.
- I never met him.

269
00:12:36,169 --> 00:12:38,545
What I do know is that he could sue us
at the drop of a hat.

270
00:12:38,670 --> 00:12:41,551
I mean, right now
he is the most dangerous man in Pawnee.

271
00:12:41,719 --> 00:12:43,517
Is that all you lawyers think about?

272
00:12:43,642 --> 00:12:46,283
Lawsuits, and laws, and legalese?

273
00:12:47,657 --> 00:12:48,725
You can relax.

274
00:12:49,128 --> 00:12:51,713
All I'm gonna do
is go in and just say,

275
00:12:51,838 --> 00:12:54,083
"We're so sorry,
it's entirely our fault"...

276
00:12:54,815 --> 00:12:57,161
You can't say any of that.
It admits liability.

277
00:12:57,286 --> 00:13:00,653
You can't say "I'm sorry",
or "I apologize." It implies guilt.

278
00:13:01,025 --> 00:13:04,365
That's insane, I have to apologize.
Andy was the victim...

279
00:13:04,533 --> 00:13:06,617
- Can't say "victim".
- Of an unfortunate...

280
00:13:06,785 --> 00:13:08,661
Can't say "unfortunate"
and "situation".

281
00:13:08,956 --> 00:13:12,290
- I can't say the word "situation"?
- No, it implies there was a situation.

282
00:13:13,365 --> 00:13:15,013
Can I give him the pig?

283
00:13:16,132 --> 00:13:17,045
Pig's fine.

284
00:13:17,769 --> 00:13:18,629
Hi, Andy.

285
00:13:19,917 --> 00:13:21,193
Got this for you.

286
00:13:21,318 --> 00:13:22,981
Hope you squeal better.

287
00:13:29,976 --> 00:13:31,715
This is my friend, Scott.

288
00:13:34,879 --> 00:13:37,942
Andy, I just wanted to say,
I am so, so, so...

289
00:13:39,276 --> 00:13:41,652
filled... with emotions.

290
00:13:42,449 --> 00:13:43,613
As any person would.

291
00:13:46,200 --> 00:13:47,034
What?

292
00:13:47,827 --> 00:13:48,827
No miming.

293
00:13:50,843 --> 00:13:53,831
- You were in the pit.
- We're not conceding that point.

294
00:13:54,649 --> 00:13:56,209
You were in a place...

295
00:13:57,066 --> 00:13:58,753
we're both here now.

296
00:13:59,014 --> 00:14:00,166
The government...

297
00:14:02,088 --> 00:14:03,455
- We gotta go.
- What?

298
00:14:03,913 --> 00:14:05,343
- Come on.
- Wendell Adams.

299
00:14:05,511 --> 00:14:07,111
I'm Andrew's attorney.

300
00:14:08,802 --> 00:14:11,009
- It's my only option...
- Andy, stop.

301
00:14:11,293 --> 00:14:13,686
I've advised my client
not to speak with you.

302
00:14:13,811 --> 00:14:15,686
He's suing the city of Pawnee.

303
00:14:16,015 --> 00:14:17,774
You're suing your home town?

304
00:14:19,884 --> 00:14:22,634
- Can we just talk about this?
- Leslie, no.

305
00:14:23,754 --> 00:14:25,917
- This is terrible.
- Don't say "terrible".

306
00:14:26,042 --> 00:14:27,301
Don't look back.

307
00:14:33,424 --> 00:14:35,299
Andy, it's Leslie.

308
00:14:36,500 --> 00:14:37,980
Hello.
Andy, it's Leslie.

309
00:14:38,289 --> 00:14:40,555
<i>Psych!
Leave a message after the beep.</i>

310
00:14:40,913 --> 00:14:42,161
Andy, it's Leslie.

311
00:14:42,286 --> 00:14:46,082
Look, what did you mean
when you said it's your only option?

312
00:14:46,268 --> 00:14:47,792
I think we should talk.

313
00:14:48,028 --> 00:14:49,461
Without lawyers present.

314
00:14:49,586 --> 00:14:53,241
If you wanna meet, just put
a white chalk'x'on the mailbox

315
00:14:53,366 --> 00:14:55,164
across the street from city hall.

316
00:14:55,818 --> 00:14:58,150
Or call me back...
just call me back.

317
00:14:58,635 --> 00:15:00,569
Why aren't you returning my calls?

318
00:15:01,212 --> 00:15:03,067
Is it because of your lawyer?

319
00:15:03,864 --> 00:15:06,351
Hey, Andy, it's your aunt.

320
00:15:06,786 --> 00:15:08,684
Your mom or dad's sister.

321
00:15:09,388 --> 00:15:12,171
I don't know how to tell you this,
but...

322
00:15:12,715 --> 00:15:14,315
your uncle has passed.

323
00:15:15,427 --> 00:15:17,168
He's with Jesus now.

324
00:15:18,536 --> 00:15:21,519
So we're having a memorial
in 30 minutes

325
00:15:21,644 --> 00:15:23,071
at city hall.

326
00:15:23,969 --> 00:15:26,340
Hey, free guitars at city hall!
Everybody run!

327
00:15:27,154 --> 00:15:29,518
Because of a local disaster,

328
00:15:29,643 --> 00:15:30,643
you...

329
00:15:30,856 --> 00:15:32,286
Andy Dwyer...

330
00:15:32,752 --> 00:15:35,207
must go to the evacuation center

331
00:15:35,332 --> 00:15:38,395
at Pawnee city hall...

332
00:15:41,857 --> 00:15:42,982
That was weird.

333
00:15:43,688 --> 00:15:45,332
How long have you been there?

334
00:15:47,592 --> 00:15:50,114
I promise that Andy
isn't suing just for the money.

335
00:15:50,426 --> 00:15:52,197
Leslie, the man lived in a pit.

336
00:15:52,322 --> 00:15:54,671
He couldn't find a place to live
on the earth's surface,

337
00:15:54,796 --> 00:15:56,229
so he went under the ground.

338
00:15:56,354 --> 00:15:58,831
You're dealing with a grown man
who thinks like a gofer.

339
00:15:59,401 --> 00:16:01,410
I feel like
if I could get past the lawyers,

340
00:16:01,535 --> 00:16:05,004
and find out what's really going on,
I could get him to drop the lawsuit.

341
00:16:12,473 --> 00:16:13,721
Ann, is that you?

342
00:16:14,167 --> 00:16:15,952
We need to talk somewhere private.

343
00:16:19,245 --> 00:16:20,971
Baby, I'm back.

344
00:16:21,235 --> 00:16:22,485
Thank you so...

345
00:16:23,808 --> 00:16:25,107
Why are you naked?

346
00:16:25,275 --> 00:16:27,349
Because of what you said,
we're getting back together.

347
00:16:27,474 --> 00:16:29,353
- No, we're not.
- Yes, we are.

348
00:16:29,478 --> 00:16:31,363
You should've heard
the sexy stuff she said.

349
00:16:31,611 --> 00:16:34,418
I said "hey, Andy, it's Ann,
can we talk at my house?"

350
00:16:34,543 --> 00:16:37,317
That's not how you said it,
you were like, "Hey, Andy, it's Ann,

351
00:16:37,537 --> 00:16:40,021
"can you talk at my house?"

352
00:16:40,146 --> 00:16:42,708
My God, you're such a baby.
Literally.

353
00:16:42,968 --> 00:16:45,417
Big naked baby, put clothes on.

354
00:16:46,672 --> 00:16:48,881
You know what?
I should've seen this coming.

355
00:16:49,049 --> 00:16:50,902
Has he showed up naked before?

356
00:16:51,385 --> 00:16:53,381
Anniversaries, birthdays, holidays.

357
00:16:54,343 --> 00:16:56,222
- Good luck.
- Where are you...

358
00:16:57,456 --> 00:16:58,891
None of your clothes fit me,

359
00:16:59,059 --> 00:17:01,584
so I'm putting an apron
over the front stuff.

360
00:17:02,104 --> 00:17:03,185
Is she gone?

361
00:17:05,239 --> 00:17:07,566
What did you mean
when you said this was your only option?

362
00:17:07,830 --> 00:17:09,680
Well, I'm completely broke.

363
00:17:09,934 --> 00:17:12,988
And my lawyer said it could be
worth upwards of $100,000.

364
00:17:13,254 --> 00:17:15,449
I can't believe that
you're suing for the money.

365
00:17:15,618 --> 00:17:18,494
I want Ann back, and she said
she needs a guy with a lot of money.

366
00:17:18,662 --> 00:17:21,205
- That doesn't sound like Ann.
- I can't really do her voice.

367
00:17:21,431 --> 00:17:23,540
But she said she likes Mark
because he's a grown-up

368
00:17:23,665 --> 00:17:25,770
with a bank account.
I put two and two together,

369
00:17:25,895 --> 00:17:28,208
$100,000,
I can probably get a bank account.

370
00:17:28,422 --> 00:17:31,799
Sometimes I think the right thing to do
is to not take shortcuts.

371
00:17:32,140 --> 00:17:33,823
I tried to fill in the pit.

372
00:17:34,115 --> 00:17:35,427
That didn't work either.

373
00:17:37,070 --> 00:17:38,018
Fine.

374
00:17:38,935 --> 00:17:40,099
I'll get a job.

375
00:17:40,528 --> 00:17:41,892
Ann will respect that.

376
00:17:42,691 --> 00:17:45,758
I can make six bucks a day
playing guitar in the street.

377
00:17:45,883 --> 00:17:48,010
- I can't make six bucks a day.
- Wait a second.

378
00:17:48,135 --> 00:17:50,771
I know I just said
that shortcuts are bad,

379
00:17:51,238 --> 00:17:53,725
but I kind of just thought
of an awesome shortcut.

380
00:17:55,295 --> 00:17:57,747
Mr. Dwyer, we're very happy
that you're dropping the suit.

381
00:17:57,872 --> 00:17:59,159
It's really terrific.

382
00:17:59,674 --> 00:18:00,870
I have my conditions.

383
00:18:01,067 --> 00:18:03,485
Never, I will not negotiate
with greedy street people.

384
00:18:03,610 --> 00:18:05,960
Wait, Leslie, let's hear him out.

385
00:18:06,418 --> 00:18:07,745
We planned this.

386
00:18:11,077 --> 00:18:12,883
All right, what's it gonna take?

387
00:18:13,162 --> 00:18:14,008
One...

388
00:18:15,090 --> 00:18:17,993
I want that pit filled in, sir.

389
00:18:23,970 --> 00:18:25,060
Is there a two?

390
00:18:25,931 --> 00:18:28,634
A guaranteed starting spot

391
00:18:29,620 --> 00:18:31,572
on the Indianapolis Colts...

392
00:18:32,201 --> 00:18:33,694
inside linebacker.

393
00:18:35,197 --> 00:18:36,674
We can't accomplish that.

394
00:18:36,799 --> 00:18:39,160
I think that Andy seems like

395
00:18:39,285 --> 00:18:42,519
he doesn't care so much
about the second one.

396
00:18:43,108 --> 00:18:47,112
And he would be fine
with us just filling in the pit.

397
00:18:47,693 --> 00:18:49,255
What do you say, Scott?

398
00:18:50,924 --> 00:18:54,270
- Eventually, we're gonna fill it.
- Eventually?

399
00:18:55,722 --> 00:18:57,753
I've already been
injured in that thing twice.

400
00:18:57,878 --> 00:19:01,096
And I can fall in ten more times
by "eventually".

401
00:19:01,535 --> 00:19:03,781
Every time I walk past that pit,
I'm forced to relive

402
00:19:03,906 --> 00:19:07,853
the most traumatic two separate days
of my life and wanna sue you again.

403
00:19:10,846 --> 00:19:13,069
Well, I hate to say it,

404
00:19:13,644 --> 00:19:16,972
but it really seems like Andy has us
over a barrel here, Scott.

405
00:19:20,800 --> 00:19:22,416
This is really happening.

406
00:19:23,247 --> 00:19:24,621
I can't believe it.

407
00:19:26,480 --> 00:19:29,333
Usually in these situations
a person says "pinch me."

408
00:19:32,875 --> 00:19:34,004
Is that Andy?

409
00:19:37,593 --> 00:19:40,469
Just one of his conditions
for the settlement, I guess.

410
00:19:40,594 --> 00:19:41,644
He settled?

411
00:19:43,449 --> 00:19:46,074
Gave up $100,000 probably.

412
00:19:46,199 --> 00:19:47,536
Just for one thing.

413
00:19:48,401 --> 00:19:50,646
The city fill in the pit
behind your house.

414
00:19:51,168 --> 00:19:51,994
Why?

415
00:19:52,607 --> 00:19:53,666
Who knows?

416
00:19:54,497 --> 00:19:56,819
Who knows why that gopher
does what he does?

417
00:19:57,033 --> 00:19:58,883
You always hated this pit!

418
00:20:03,454 --> 00:20:04,838
Good-bye, pit...

419
00:20:05,848 --> 00:20:07,162
hello, lot.

420
00:20:08,124 --> 00:20:10,374
How does taking risks make me feel?

421
00:20:10,936 --> 00:20:11,936
Amazing.

422
00:20:12,945 --> 00:20:16,127
Tingling sensation
throughout my whole body.

423
00:20:16,252 --> 00:20:17,569
I feel flushed.

424
00:20:18,784 --> 00:20:21,256
My muscles feel relaxed,
yet I feel awake.

425
00:20:21,381 --> 00:20:23,463
Just waves of pleasure.

426
00:20:26,189 --> 00:20:29,251
I wish there was something physical
that can make me feel this way.

427
00:20:31,329 --> 00:20:32,379
You did it.

428
00:20:33,120 --> 00:20:34,848
You thinking what I'm thinking?

429
00:20:35,067 --> 00:20:37,281
- There's so much more to do.
- We're so close!

430
00:20:38,477 --> 00:20:41,259
I'm gonna call kaboom! And check on
their availability for next year.

431
00:20:41,384 --> 00:20:42,384
Great.

432
00:20:44,070 --> 00:20:45,033
<i>Information.</i>

433
00:20:45,201 --> 00:20:47,188
Hi, I'd like the number for kaboom!

434
00:20:47,313 --> 00:20:49,371
The national playground
building charity.

435
00:20:49,796 --> 00:20:51,408
I hope they can work us in.

436
00:20:51,736 --> 00:20:53,757
<i>Sorry, there's no listing
for that name.</i>

437
00:20:53,882 --> 00:20:55,448
What? Are you sure?

438
00:20:56,734 --> 00:20:59,610
Kaboom! Is a word I made up.
It's not in any dictionary.

439
00:20:59,735 --> 00:21:03,615
I trick people into building playgrounds
in empty lots in their neighborhood.

440
00:21:03,740 --> 00:21:05,050
It's an elaborate prank.

441
00:21:05,175 --> 00:21:07,609
For my next prank,
I'm gonna build a hospital

442
00:21:07,734 --> 00:21:09,434
in a poor part of China.

443
00:21:09,654 --> 00:21:11,268
They'll never see it coming!

444
00:21:13,291 --> 00:21:14,771
www.sous-titres.eu

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
