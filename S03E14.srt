﻿1
00:00:00,119 --> 00:00:01,402
- Hey, Leslie
- Hey

2
00:00:01,419 --> 00:00:01,992
Thank you for that

3
00:00:01,993 --> 00:00:03,992
"You're over your maintenance"
cost analysis.

4
00:00:04,003 --> 00:00:05,792
- That was really helpful
- You're so welcome.

5
00:00:05,803 --> 00:00:07,002
I think you may wanna
read it.

6
00:00:07,503 --> 00:00:09,902
´Cause I was uhm...

7
00:00:10,203 --> 00:00:12,102
'Cause I told you that
I wanted to read it?

8
00:00:12,203 --> 00:00:13,302
Yeah

9
00:00:13,419 --> 00:00:14,702
That's a good reason.

10
00:00:14,770 --> 00:00:17,639
Oh, anyway, Chris wants to
see us in his office.

11
00:00:17,706 --> 00:00:18,706
I guess we should head
over there.

12
00:00:18,774 --> 00:00:20,041
- All right.
- All right.

13
00:00:20,109 --> 00:00:21,342
- All right, cool.
- Great.

14
00:00:21,410 --> 00:00:22,811
- See you there.
- I'll see you there, Benjamin.

15
00:00:22,878 --> 00:00:24,112
- Ben.
- All right, Leslie-min.

16
00:00:24,180 --> 00:00:25,797
Leslie.

17
00:00:25,881 --> 00:00:28,049
Hi, Ben.

18
00:00:28,117 --> 00:00:30,819
Oh, hey, Ann.

19
00:00:32,388 --> 00:00:35,523
Wow, that was
the most sexual tension

20
00:00:35,591 --> 00:00:37,392
I have ever seen
in a conversation

21
00:00:37,459 --> 00:00:38,643
about documents.

22
00:00:38,727 --> 00:00:40,979
It can't happen.
We could both get fired.

23
00:00:41,063 --> 00:00:43,064
- That's a dumb rule.
- I know!

24
00:00:43,132 --> 00:00:44,616
Well, if you have to follow it,

25
00:00:44,700 --> 00:00:45,984
I would just suggest

26
00:00:46,068 --> 00:00:49,938
avoiding one-on-one,
tension-y situations with him.

27
00:00:50,005 --> 00:00:52,140
You're right.
Good plan.

28
00:00:52,208 --> 00:00:54,325
I am sending you two on a trip.

29
00:00:55,578 --> 00:00:56,845
Really?

30
00:00:56,912 --> 00:00:58,546
The Indiana little league
baseball tournament

31
00:00:58,614 --> 00:01:02,183
is upon us, and Pawnee
hasn't hosted it in 20 years.

32
00:01:02,251 --> 00:01:04,385
So, I would like you two
to go to Indianapolis

33
00:01:04,453 --> 00:01:05,803
and state our case,

34
00:01:05,888 --> 00:01:08,923
because you two
are my dynamic duo.

35
00:01:08,974 --> 00:01:13,595
Put it here.
Eh, eh-eh, eh...

36
00:01:13,662 --> 00:01:14,596
- Go team!
- Yeah.

37
00:01:14,647 --> 00:01:24,599
<font color="#FF8C00">Sync & corrections by Alice</font>
<font color="##FF8C00">www.addic7ed.com</font>

38
00:01:34,850 --> 00:01:37,285
Ann, everything you have
is too sexy.

39
00:01:37,336 --> 00:01:39,454
This is actually the dress
that Julia Roberts wore

40
00:01:39,521 --> 00:01:40,855
as a prostitute
in <i>Pretty Woman.</i>

41
00:01:40,923 --> 00:01:43,007
I know.
I look really good in it.

42
00:01:43,092 --> 00:01:44,676
I need, like,
a sweat suit or something.

43
00:01:44,760 --> 00:01:47,095
I need to send out a signal
that nothing is gonna happen.

44
00:01:47,162 --> 00:01:48,880
Oh, this is insane.

45
00:01:48,964 --> 00:01:51,466
It's so obvious
you're dying to be together.

46
00:01:51,533 --> 00:01:52,867
And now you're going
on a road trip?

47
00:01:52,935 --> 00:01:55,303
I mean, you guys
could literally "get a room."

48
00:01:55,371 --> 00:01:56,971
Yeah, and I could
literally get a-fired.

49
00:01:57,022 --> 00:01:59,357
All right, I will help you
anti-seduce him.

50
00:01:59,441 --> 00:02:01,225
Just tell me
what else you need.

51
00:02:01,310 --> 00:02:04,045
I need to think of unsexy,
boring conversation topics

52
00:02:04,113 --> 00:02:05,313
we can talk about in the car.

53
00:02:05,364 --> 00:02:07,315
I have a few ideas.

54
00:02:07,383 --> 00:02:08,900
We could discuss
the <i>New Yorker</i> article

55
00:02:08,984 --> 00:02:10,702
"The history of the ladder."

56
00:02:10,786 --> 00:02:11,869
Okay.

57
00:02:11,954 --> 00:02:13,371
We could talk
about different dorms

58
00:02:13,455 --> 00:02:14,822
at Johns Hopkins University,

59
00:02:14,890 --> 00:02:17,659
and I could read
from my Sonicare booklet.

60
00:02:17,726 --> 00:02:19,694
- Oh, I have a good idea!
- What?

61
00:02:19,762 --> 00:02:22,363
Why don't you ask him
about his penis?

62
00:02:25,167 --> 00:02:27,335
Pawnee Zoo, monkey speaking.

63
00:02:29,905 --> 00:02:31,839
Okay, everyone stop
what you're doing

64
00:02:31,890 --> 00:02:33,891
and come with me...
all four of you.

65
00:02:33,976 --> 00:02:35,843
- Where are you going?
- Leslie's not here today.

66
00:02:35,911 --> 00:02:37,228
No boss.
We can do whatever we want.

67
00:02:37,313 --> 00:02:38,846
I'm your boss.

68
00:02:38,914 --> 00:02:41,249
That's a good one, Ron.

69
00:02:41,317 --> 00:02:43,434
Let's go, seriously.
Come on.

70
00:02:43,519 --> 00:02:45,019
Here's the sitch.

71
00:02:45,087 --> 00:02:46,621
I developed
a dope new game show

72
00:02:46,689 --> 00:02:48,823
where I ask couples
scandalous questions,

73
00:02:48,890 --> 00:02:50,825
and they have to guess
what the other one answered.

74
00:02:50,893 --> 00:02:53,328
I call this <i>Know ya Boo.</i>

75
00:02:53,395 --> 00:02:55,163
Oh, that sounds like
<i>The Newlywed Game.</i>

76
00:02:55,230 --> 00:02:56,497
Shut up, Jerry!

77
00:02:56,565 --> 00:02:57,865
It's not <i>The Newlywed Game</i> okay?

78
00:02:57,933 --> 00:03:00,601
It is totally
<i>The Newlywed Game.</i>

79
00:03:00,669 --> 00:03:02,570
But big deal.
Everyone steals.

80
00:03:02,621 --> 00:03:04,088
My favorite movie
is <i>Love don't cost a thing,</i>

81
00:03:04,173 --> 00:03:05,306
with Nick Cannon,

82
00:03:05,374 --> 00:03:07,075
which is based on
<i>Can't buy me love,</i>

83
00:03:07,142 --> 00:03:09,310
which is based on
<i>Kramer vs. Kramer</i>

84
00:03:09,378 --> 00:03:10,745
or something,
which I think was Shakespeare.

85
00:03:10,796 --> 00:03:12,764
Don't know, don't care.

86
00:03:12,848 --> 00:03:14,282
And what exactly
do you plan on doing

87
00:03:14,350 --> 00:03:15,383
with this game show idea?

88
00:03:15,451 --> 00:03:17,986
Are you going to
shove it up your butt?

89
00:03:18,053 --> 00:03:20,455
No, I'm gonna test it out
on you four bing-bongs

90
00:03:20,522 --> 00:03:21,956
and work out the kinks,

91
00:03:22,024 --> 00:03:24,192
then hire actual attractive
people, and make a demo.

92
00:03:24,259 --> 00:03:26,527
What network is gonna buy
a game show from you?

93
00:03:26,595 --> 00:03:28,496
There's a million networks
out there,

94
00:03:28,564 --> 00:03:29,764
and they all need programming.

95
00:03:29,832 --> 00:03:32,700
Spike, G4, GSN, Fuse,

96
00:03:32,768 --> 00:03:34,268
WoW, Boom, Zip, Kablam,

97
00:03:34,336 --> 00:03:36,871
Slurp, Slurp Latin, Slurp HD.

98
00:03:36,938 --> 00:03:38,439
I love Slurp HD.

99
00:03:38,507 --> 00:03:40,241
Have you guys seen
<i>Ultimate Battle Smoothie?</i>

100
00:03:40,309 --> 00:03:42,076
- That's a dope show.
- Oh, my God.

101
00:03:42,144 --> 00:03:44,862
Yeah, so, basically
every dorm allows bed lofting,

102
00:03:44,930 --> 00:03:46,664
but the students
have really taken to it

103
00:03:46,749 --> 00:03:47,832
at Wolman and McCoy.

104
00:03:47,916 --> 00:03:49,634
Did you go to Johns Hopkins?

105
00:03:49,718 --> 00:03:51,552
No.

106
00:03:52,620 --> 00:03:54,689
Do you wanna play some music?

107
00:03:54,757 --> 00:03:57,925
Sure! Ann and I burned
an awesome CD for the trip.

108
00:03:57,993 --> 00:04:00,128
Jimmy Carter's
<i>Crisis of Confidence</i> speech,

109
00:04:00,195 --> 00:04:02,163
<i>Learning to speak Mandarin,</i>

110
00:04:02,231 --> 00:04:05,333
<i>16 minutes of
Old timey car horn,</i>

111
00:04:05,401 --> 00:04:09,570
and something called
<i>Banjo Boogie Bonanza.</i>

112
00:04:13,275 --> 00:04:15,326
It's an amazing instrument,
the banjo.

113
00:04:16,411 --> 00:04:17,111
Yeah.

114
00:04:17,162 --> 00:04:19,363
I didn't realize
it... it could be this loud.

115
00:04:22,751 --> 00:04:24,752
Hello?

116
00:04:24,820 --> 00:04:26,621
Hello?

117
00:04:26,672 --> 00:04:28,556
Can I help you?

118
00:04:28,624 --> 00:04:30,091
Hi. My class is here
on a field trip,

119
00:04:30,158 --> 00:04:33,428
and I'm supposed to interview
someone for a school project.

120
00:04:33,495 --> 00:04:35,830
Okay. You can wait
at that table,

121
00:04:35,881 --> 00:04:39,867
and someone
will be here sometime.

122
00:04:39,935 --> 00:04:41,602
But aren't you here now?

123
00:04:43,172 --> 00:04:45,890
No.

124
00:04:45,974 --> 00:04:47,608
♪ Ba na na na na na
na na na na ♪

125
00:04:47,676 --> 00:04:50,511
It's time to <i>know ya...</i>

126
00:04:52,347 --> 00:04:54,181
I don't have the instrumental
backing track yet,

127
00:04:54,233 --> 00:04:55,716
but it's gonna say
"boo" right there.

128
00:04:55,784 --> 00:04:57,118
Hey, everybody!

129
00:04:57,186 --> 00:04:58,703
Welcome <i>Know ya Boo.</i>

130
00:04:58,787 --> 00:05:00,238
I'm your host, TomHaverford,

131
00:05:00,322 --> 00:05:03,758
and with me as always
is my CGI puppy co-host,

132
00:05:03,826 --> 00:05:05,493
Bobby the boo!

133
00:05:05,544 --> 00:05:07,245
Hey, everybody!

134
00:05:07,329 --> 00:05:09,297
All right, let's get to it.

135
00:05:09,364 --> 00:05:11,382
First question...

136
00:05:11,467 --> 00:05:13,751
fellas, which rock star
would your lady bang

137
00:05:13,836 --> 00:05:16,170
if she could bang
one rock star?

138
00:05:16,221 --> 00:05:17,722
Jerry!

139
00:05:17,806 --> 00:05:21,943
I believe I have heard Donna
talk about Prince a lot.

140
00:05:22,010 --> 00:05:23,094
What'd your boo say?

141
00:05:23,178 --> 00:05:26,180
Ah!

142
00:05:26,231 --> 00:05:27,348
- Impressive!
- Hee hee!

143
00:05:27,416 --> 00:05:29,150
All right, Andy,

144
00:05:29,218 --> 00:05:30,751
which rock star
would your lady get with?

145
00:05:30,819 --> 00:05:33,988
Um, this is
almost too easy... me!

146
00:05:34,055 --> 00:05:35,623
Let's check in with ya boo!

147
00:05:35,691 --> 00:05:37,992
- Yay!
- Oh!

148
00:05:38,059 --> 00:05:40,194
Sorry.

149
00:05:40,245 --> 00:05:41,913
Who is Jeff Mangum?

150
00:05:41,997 --> 00:05:43,664
The guy
from Neutral Milk Hotel.

151
00:05:43,732 --> 00:05:45,833
Oh! Oh.

152
00:05:45,901 --> 00:05:48,302
Neutral Milk Hotel.
What is that?

153
00:05:48,370 --> 00:05:49,504
That's my favorite band.

154
00:05:49,571 --> 00:05:51,005
I've told you that,
like, a thousand times.

155
00:05:51,072 --> 00:05:53,608
I don't remember.
Why wouldn't you pick me?

156
00:05:53,675 --> 00:05:56,110
I don't know. You're
not technically a rock star.

157
00:05:56,178 --> 00:05:57,929
Booyah!

158
00:05:58,013 --> 00:05:59,547
Somebody don't know their boo!

159
00:05:59,598 --> 00:06:01,048
That's the sound bite
that's gonna play

160
00:06:01,116 --> 00:06:02,283
when a fight breaks out.

161
00:06:03,351 --> 00:06:04,769
Look, little girl,

162
00:06:04,853 --> 00:06:07,054
can we postpone this
for another day?

163
00:06:07,122 --> 00:06:09,390
It's unsettling
having you just sit there.

164
00:06:09,458 --> 00:06:11,392
But my report's due tomorrow.

165
00:06:11,460 --> 00:06:13,828
- What's it on?
- Why government matters.

166
00:06:13,896 --> 00:06:15,863
Really?

167
00:06:15,931 --> 00:06:17,698
It's never too early to learn

168
00:06:17,766 --> 00:06:19,901
that the government
is a greedy piglet

169
00:06:19,968 --> 00:06:22,069
that suckles
on a taxpayer's teat

170
00:06:22,137 --> 00:06:25,206
until they have
sore, chapped nipples.

171
00:06:27,976 --> 00:06:29,310
I'm gonna need
a different metaphor

172
00:06:29,378 --> 00:06:31,479
to give this nine-year-old.

173
00:06:31,547 --> 00:06:32,880
What's your name, ma'am?

174
00:06:32,948 --> 00:06:34,165
Lauren Berkus.

175
00:06:34,249 --> 00:06:37,335
Lauren, my name is Ron Swanson,

176
00:06:37,419 --> 00:06:40,154
and I'm gonna tell you
everything you need to know

177
00:06:40,222 --> 00:06:41,889
about the miserable,
screwed-up world

178
00:06:41,957 --> 00:06:44,225
of local government.

179
00:06:44,293 --> 00:06:45,893
You have mustard
in your moustache.

180
00:06:45,961 --> 00:06:49,497
Don't sass me, Berkus.
Let's get started.

181
00:06:49,565 --> 00:06:53,701
"Life, liberty, and property."
That's John Locke.

182
00:06:56,605 --> 00:06:57,989
What is this?

183
00:06:58,073 --> 00:06:59,607
Whale sounds.

184
00:06:59,675 --> 00:07:00,942
Okay.

185
00:07:00,993 --> 00:07:02,660
You can change it if you want.

186
00:07:02,744 --> 00:07:04,245
Yeah?

187
00:07:06,648 --> 00:07:07,832
What the hell?

188
00:07:07,916 --> 00:07:09,617
Oh, no, no, no.
This is such a great song.

189
00:07:10,819 --> 00:07:14,422
Yeah, I snuck
an Al Green song in there.

190
00:07:14,489 --> 00:07:16,224
I want them to get together.

191
00:07:16,291 --> 00:07:17,592
Sue me.

192
00:07:17,659 --> 00:07:18,759
Did you see this?

193
00:07:18,827 --> 00:07:20,127
According to their rules,

194
00:07:20,178 --> 00:07:22,213
we may not have enough
hotel rooms within city limits.

195
00:07:22,297 --> 00:07:23,931
I know.
It's a silly rule.

196
00:07:23,999 --> 00:07:26,100
But maybe they'll be
reasonable and not care.

197
00:07:26,167 --> 00:07:27,835
Yeah, maybe we could
just point out to them

198
00:07:27,886 --> 00:07:30,187
there are tons of hotel rooms
just outside city limits.

199
00:07:30,272 --> 00:07:31,522
Uh-huh.

200
00:07:31,607 --> 00:07:33,641
Have you been to
the Grandville Hotel and Spa?

201
00:07:33,692 --> 00:07:36,777
- Uh-uh.
- They have the softest towels.

202
00:07:36,845 --> 00:07:39,113
Oh, I gotta tell ya.
I love a nice towel in my life.

203
00:07:39,181 --> 00:07:40,364
Me too.
I know.

204
00:07:40,449 --> 00:07:42,049
It's... I mean,
it's the simplest luxury,

205
00:07:42,117 --> 00:07:43,401
but it makes
all the difference.

206
00:07:43,485 --> 00:07:44,819
Their bath mats are amazing.

207
00:07:44,870 --> 00:07:46,454
It's like stepping on a lamb.

208
00:07:48,907 --> 00:07:51,993
Oh, hold on.
Hey, come here.

209
00:07:52,044 --> 00:07:55,212
You've got an eyelash
on your face.

210
00:07:55,297 --> 00:07:57,632
I got it.
Make a wish.

211
00:07:57,699 --> 00:07:59,884
<i>Learning to speak Mandarin.
Unit three...</i>

212
00:07:59,968 --> 00:08:01,736
- Get away from me.
- Sorry.

213
00:08:01,803 --> 00:08:03,371
- I'm sorry.
- Sorry.

214
00:08:03,438 --> 00:08:04,705
- Um...
- Um...

215
00:08:04,773 --> 00:08:06,440
I'm allergic to fingers.

216
00:08:06,508 --> 00:08:08,709
Where are you going?

217
00:08:08,777 --> 00:08:10,444
Maybe we should just
not talk to each other

218
00:08:10,512 --> 00:08:11,812
for the rest of the trip,

219
00:08:11,880 --> 00:08:13,898
and I'm just gonna concentrate
on the presentation.

220
00:08:13,982 --> 00:08:15,215
Okay.

221
00:08:15,267 --> 00:08:18,052
This is my little brother.

222
00:08:21,167 --> 00:08:22,152
Next up.

223
00:08:22,167 --> 00:08:25,052
The delegation
from Pawnee, please?.

224
00:08:25,183 --> 00:08:27,601
And, so, as you can see,

225
00:08:27,668 --> 00:08:29,770
Pawnee has 12 beautiful
baseball diamonds,

226
00:08:29,838 --> 00:08:32,306
and our seats
have extra cushioning

227
00:08:32,374 --> 00:08:34,608
due to the massive size
of our average citizen.

228
00:08:34,676 --> 00:08:36,777
I have to bring up
what happened

229
00:08:36,844 --> 00:08:39,046
the last time Pawnee
hosted this tournament.

230
00:08:39,114 --> 00:08:40,314
No, you don't.

231
00:08:40,382 --> 00:08:41,548
Good evening.

232
00:08:41,616 --> 00:08:43,550
We begin with
our first story tonight.

233
00:08:43,618 --> 00:08:45,252
They're cute, they're cuddly,

234
00:08:45,320 --> 00:08:47,020
but now they're wreaking havoc

235
00:08:47,088 --> 00:08:48,722
at the State
Little League Championship.

236
00:08:48,790 --> 00:08:51,125
Pawnee's raccoon infestation.

237
00:08:52,544 --> 00:08:54,044
Have these
little bandits stolen

238
00:08:54,129 --> 00:08:56,029
our sense of safety?

239
00:08:56,081 --> 00:08:58,432
The raccoon problem
is under control.

240
00:08:58,500 --> 00:08:59,716
They have their part
of the town,

241
00:08:59,801 --> 00:09:01,051
and we have ours.

242
00:09:01,136 --> 00:09:04,037
Muncie is larger.
Bloomington is more central.

243
00:09:04,105 --> 00:09:06,540
What's the advantage
of doing it in Pawnee?

244
00:09:06,608 --> 00:09:10,377
The advantage is
that it's a wonderful city.

245
00:09:10,445 --> 00:09:12,846
I mean, look,
I've been to 40 some odd towns

246
00:09:12,914 --> 00:09:14,748
in Indiana,

247
00:09:14,816 --> 00:09:16,717
and Pawnee is special.

248
00:09:16,785 --> 00:09:19,219
I mean, the people
are passionate and kind.

249
00:09:19,287 --> 00:09:21,088
They love their city.

250
00:09:21,156 --> 00:09:24,792
They take pride in their work.

251
00:09:24,859 --> 00:09:28,162
It's a very,
very special place.

252
00:09:32,233 --> 00:09:35,102
Hey, it's Tom Haverford
back here with <i>Know ya Boo.</i>

253
00:09:35,170 --> 00:09:37,004
Let's move
to the next question.

254
00:09:37,071 --> 00:09:40,424
Where is your favorite place
to smush ya boo?

255
00:09:40,508 --> 00:09:43,076
- Donna!
- Back of my Benz?

256
00:09:43,144 --> 00:09:45,379
Let's check in with ya boo!

257
00:09:45,447 --> 00:09:46,547
Her said her Mercedes!

258
00:09:48,583 --> 00:09:49,800
Jerry and Donna on fire.

259
00:09:49,884 --> 00:09:52,686
- April?
- This question is gross.

260
00:09:52,754 --> 00:09:54,922
That's kind of the point.
What's your answer?

261
00:09:54,989 --> 00:09:58,559
- In our bed, I guess.
- Andy?

262
00:09:58,626 --> 00:10:00,427
Where's your boo's
favorite place to smush?

263
00:10:00,478 --> 00:10:02,729
At the Neutral Milk Hotel.

264
00:10:02,797 --> 00:10:04,865
- Get over it.
- No, you get over it.

265
00:10:04,932 --> 00:10:06,150
You like some other dude's
band more than me.

266
00:10:06,234 --> 00:10:07,401
Do you even think
that Mouse Rat

267
00:10:07,469 --> 00:10:08,735
is the greatest band
in the world?

268
00:10:08,786 --> 00:10:10,270
'Cause it's starting
to not feel that way.

269
00:10:10,338 --> 00:10:11,605
That band is
really important to me,

270
00:10:11,673 --> 00:10:13,340
and, honestly, I've asked you
to listen to them,

271
00:10:13,408 --> 00:10:14,908
like, a million times,
and you never have, so...

272
00:10:14,976 --> 00:10:17,127
'cause their music is sad,
and depressing, and weird.

273
00:10:17,212 --> 00:10:20,581
And art is supposed to be
happy and fun,

274
00:10:20,632 --> 00:10:21,999
and everyone knows that.

275
00:10:22,083 --> 00:10:23,950
You know what?
Whatever. Forget it.

276
00:10:25,753 --> 00:10:27,588
Fine, forget it.

277
00:10:27,639 --> 00:10:30,090
Damn!
This game's got juice.

278
00:10:30,158 --> 00:10:33,393
This is your lunch.

279
00:10:33,461 --> 00:10:35,962
Now, you should be able to do
whatever you want to with this,

280
00:10:36,014 --> 00:10:37,064
right?

281
00:10:37,131 --> 00:10:38,899
If you wanna eat
all of it, great.

282
00:10:38,966 --> 00:10:40,467
If you wanna throw it away
in the garbage,

283
00:10:40,535 --> 00:10:41,985
that's your prerogative.

284
00:10:42,070 --> 00:10:44,988
But here I come,
the government...

285
00:10:48,409 --> 00:10:51,812
And I get to take 40 percent
of your lunch.

286
00:10:53,998 --> 00:10:57,017
And that, Lauren,
is how taxes work.

287
00:10:57,085 --> 00:10:59,520
- But that's not fair.
- You're learning.

288
00:10:59,587 --> 00:11:03,357
Uh-oh,
Capital Gains Tax.

289
00:11:04,425 --> 00:11:06,226
Well, Chris says
congratulations

290
00:11:06,294 --> 00:11:08,995
and that together
we're unstoppable.

291
00:11:09,047 --> 00:11:10,997
- Wow, cheers, sir!
- Yup.

292
00:11:11,049 --> 00:11:12,332
- You did it.
- No, you did it.

293
00:11:12,400 --> 00:11:14,334
No, normally I do it.

294
00:11:14,402 --> 00:11:15,669
This time you did it.

295
00:11:15,720 --> 00:11:17,271
- No.
- You should take the praise.

296
00:11:17,338 --> 00:11:19,206
Can I get a shot of you guys?

297
00:11:19,274 --> 00:11:20,607
- Yeah.
- Sure.

298
00:11:22,343 --> 00:11:24,044
Uh, hey,
when this thing's over,

299
00:11:24,112 --> 00:11:25,846
do you wanna grab some dinner?

300
00:11:25,914 --> 00:11:27,447
Uh, Chris recommended a place.

301
00:11:27,515 --> 00:11:30,217
- Sure.
- Yeah?

302
00:11:30,285 --> 00:11:32,286
Um, hey, we were thinking
about going to dinner,

303
00:11:32,353 --> 00:11:33,620
photographer.

304
00:11:33,688 --> 00:11:35,455
You want in?
You wanna grab some grub?

305
00:11:35,523 --> 00:11:38,242
- Uh, no, thanks.
- They have great burgers.

306
00:11:38,326 --> 00:11:40,427
Well, you don't even know
where we're going yet, so...

307
00:11:40,495 --> 00:11:42,379
I'm sure they have
great burgers there, though.

308
00:11:42,463 --> 00:11:44,881
Come on, photographer.
Last chance.

309
00:11:44,966 --> 00:11:46,733
Uh, okay.

310
00:11:46,801 --> 00:11:48,418
Well, then, it's just us then.

311
00:11:48,503 --> 00:11:51,338
- Yeah.
- Sure. Okay. I tried.

312
00:11:51,389 --> 00:11:54,841
And that, Lauren,
is how FDR ruined this country.

313
00:11:54,909 --> 00:11:57,877
Lauren, ready to head back?

314
00:11:57,929 --> 00:12:00,380
Well, I guess it's time
for you to head home.

315
00:12:00,431 --> 00:12:02,549
I've really enjoyed
talking with you.

316
00:12:02,600 --> 00:12:04,735
You are...
and this is not a joke...

317
00:12:04,819 --> 00:12:06,820
much smarter
than most of the people

318
00:12:06,888 --> 00:12:08,188
who work in this building.

319
00:12:08,256 --> 00:12:10,857
I liked talking
with you, too, Mr. Swanson.

320
00:12:10,908 --> 00:12:12,359
Ron.

321
00:12:12,410 --> 00:12:15,228
Hang on, hang on,
I have something for you.

322
00:12:16,831 --> 00:12:19,900
This is a Claymore land mine.

323
00:12:19,968 --> 00:12:22,569
Use that to protect
your property.

324
00:12:22,637 --> 00:12:25,405
- Thanks, Ron.
- You got it.

325
00:12:27,875 --> 00:12:30,510
Tommy Hilfiger
iPhone app finally.

326
00:12:30,578 --> 00:12:33,046
Hey, your stupid
<i>Know your Boo</i> game

327
00:12:33,114 --> 00:12:34,715
made me and Andy
get in a big fight.

328
00:12:34,782 --> 00:12:36,216
He just sold his guitar,

329
00:12:36,284 --> 00:12:37,768
and he's, like,
quitting music now.

330
00:12:37,852 --> 00:12:40,621
What do you want me
to do about it?

331
00:12:40,688 --> 00:12:43,624
Okay, welcome to the super
awesome bonus lightning round.

332
00:12:43,691 --> 00:12:45,125
First question...

333
00:12:45,193 --> 00:12:48,629
Andy, who did your boo say
makes the best mac and cheese

334
00:12:48,696 --> 00:12:49,763
in the universe?

335
00:12:49,831 --> 00:12:52,766
The universe?

336
00:12:52,817 --> 00:12:55,469
Ooh. Kraft?

337
00:12:55,536 --> 00:12:59,473
- April said...Andy!
- Aw.

338
00:12:59,540 --> 00:13:01,441
Best couple ever.
You just won 50 points.

339
00:13:01,492 --> 00:13:02,942
Wait, why?
We got it wrong.

340
00:13:02,994 --> 00:13:04,828
Next lightning round thing.

341
00:13:04,912 --> 00:13:09,166
Who is the number one Colts fan
in the world?

342
00:13:11,252 --> 00:13:12,686
Wait a minute.

343
00:13:12,754 --> 00:13:14,821
You set this whole thing up
so I would be un-mad at you.

344
00:13:14,889 --> 00:13:16,990
Well, guess what,
ain't gonna work.

345
00:13:17,058 --> 00:13:19,125
You figured out
April's trying to trick you.

346
00:13:19,177 --> 00:13:20,594
That's worth 100 points.

347
00:13:20,662 --> 00:13:24,097
Wait, no.

348
00:13:24,148 --> 00:13:26,132
Good-bye.

349
00:13:29,570 --> 00:13:31,772
You were really great
in that presentation today.

350
00:13:31,823 --> 00:13:33,006
Oh, thanks.

351
00:13:33,074 --> 00:13:34,474
I liked the stuff
you said about Pawnee.

352
00:13:34,542 --> 00:13:36,276
That was really nice to hear.

353
00:13:36,344 --> 00:13:38,011
You know, Pawnee
is a really special town.

354
00:13:38,079 --> 00:13:40,747
I, uh, love living there.

355
00:13:40,815 --> 00:13:43,450
And, um...

356
00:13:43,518 --> 00:13:45,519
And I look forward
to the moments in my day

357
00:13:45,586 --> 00:13:49,589
where I... I... where I get
to hang out with the town

358
00:13:49,657 --> 00:13:52,292
and talk to the town
about stuff.

359
00:13:52,360 --> 00:13:55,495
And the town has
really nice blond hair, too,

360
00:13:55,563 --> 00:13:58,265
and has read a shocking number
of political biographies

361
00:13:58,332 --> 00:14:00,133
for a town, which I like.

362
00:14:00,201 --> 00:14:02,803
- Oh, God.
- I'm sorry.

363
00:14:02,870 --> 00:14:04,271
And I know
we can get into trouble,

364
00:14:04,338 --> 00:14:06,773
but I-I... I can't
take this anymore,

365
00:14:06,841 --> 00:14:09,710
and I feel like we have to
at least talk about it.

366
00:14:09,777 --> 00:14:12,279
I mean,
it's not just me, right?

367
00:14:13,346 --> 00:14:15,015
No, it's not just you.

368
00:14:19,570 --> 00:14:20,570
Oh, God.

369
00:14:20,655 --> 00:14:22,205
- What? Are you... are...
- Ugh.

370
00:14:22,290 --> 00:14:23,990
- Are you all right?
- Perfect.

371
00:14:24,041 --> 00:14:25,525
I'm gonna go see a man
about some porcelain,

372
00:14:25,576 --> 00:14:26,893
you know what I mean?

373
00:14:26,961 --> 00:14:28,295
I'm not buying cocaine.
I'm going to the bathroom.

374
00:14:28,362 --> 00:14:29,796
The whiz palace,
as I like to call it.

375
00:14:29,864 --> 00:14:32,198
And I'm not calling Ann, so...

376
00:14:32,250 --> 00:14:34,401
Ann, we have
a serious code Ben.

377
00:14:34,469 --> 00:14:36,136
Well, it's not really code
if you say his name.

378
00:14:36,204 --> 00:14:37,604
He told me that he liked me,

379
00:14:37,672 --> 00:14:39,406
and I'm gonna go make out
with him right now

380
00:14:39,474 --> 00:14:40,741
on his face.

381
00:14:40,808 --> 00:14:43,894
- That's awesome!
- No, no, read me the script.

382
00:14:43,978 --> 00:14:45,812
- Seriously?
- Yes!

383
00:14:45,880 --> 00:14:47,647
All right.

384
00:14:47,715 --> 00:14:49,683
"Leslie, it's Leslie Knope
from the Parks Department

385
00:14:49,734 --> 00:14:51,184
"speaking to you
through Ann Perkins,

386
00:14:51,252 --> 00:14:52,402
friend and beautiful nurse."

387
00:14:52,487 --> 00:14:53,820
Thank you.

388
00:14:53,888 --> 00:14:55,322
"Do not do anything with Ben.

389
00:14:55,389 --> 00:14:57,691
"Be responsible, no matter
how cute his mouth is.

390
00:14:57,759 --> 00:14:59,693
Your job is on the line!"

391
00:14:59,744 --> 00:15:01,495
- Shut up, Ann!
- You wrote that.

392
00:15:01,562 --> 00:15:04,164
No, you...
then Leslie, Leslie,

393
00:15:04,232 --> 00:15:05,532
you don't know
what you're talking about.

394
00:15:05,600 --> 00:15:07,167
I care about him very much,

395
00:15:07,235 --> 00:15:09,336
and I've had 2 1/2 glasses
of red wine,

396
00:15:09,403 --> 00:15:10,754
and what that means is
I'm gonna go make out with him

397
00:15:10,838 --> 00:15:12,239
right now,
and it's gonna be awesome.

398
00:15:12,306 --> 00:15:13,790
Yay!

399
00:15:13,875 --> 00:15:15,575
No, you're supposed
to talk me out of this.

400
00:15:15,626 --> 00:15:17,511
No, don't, stop.

401
00:15:17,578 --> 00:15:19,379
Shut up, Ann,
I'm doing it anyway.

402
00:15:19,447 --> 00:15:21,047
Yay!

403
00:15:23,551 --> 00:15:24,885
Leslie!

404
00:15:27,651 --> 00:15:29,885
When Ben told me
that you had won the bid,

405
00:15:30,079 --> 00:15:31,679
I was wildly ecstatic,

406
00:15:31,747 --> 00:15:33,581
and I had to come up here
and celebrate with you.

407
00:15:33,649 --> 00:15:35,283
There is literally nothing
in this world

408
00:15:35,334 --> 00:15:36,818
that you cannot do.

409
00:15:36,869 --> 00:15:39,354
So, what's the plan now?
Should we take a long walk?

410
00:15:39,421 --> 00:15:41,589
Mini-golf.

411
00:15:41,657 --> 00:15:43,057
I'll probably...
do you...

412
00:15:43,125 --> 00:15:44,592
shouldn't we
just go back to Pawnee?

413
00:15:44,660 --> 00:15:45,927
- Yeah.
- Nonsense.

414
00:15:45,995 --> 00:15:48,263
There's no reason to drive
all the way back home.

415
00:15:48,330 --> 00:15:50,381
I've got a perfectly good condo
right here in the city.

416
00:15:50,466 --> 00:15:52,000
Oh, we couldn't
put you out like that.

417
00:15:52,067 --> 00:15:54,235
Double nonsense. I would love
to have you stay with me.

418
00:15:54,303 --> 00:15:55,720
Leslie, you can take
the guest room,

419
00:15:55,805 --> 00:15:59,174
and, Ben, you <i>have</i> to sleep
on my couch.

420
00:15:59,241 --> 00:16:01,009
It is literally

421
00:16:01,076 --> 00:16:02,677
the comfiest couch
you've ever been on.

422
00:16:06,081 --> 00:16:09,350
This is weird.

423
00:16:09,401 --> 00:16:11,018
We're on Chris' couch.

424
00:16:11,070 --> 00:16:13,738
Yup, we are,
and I'm wearing his clothes.

425
00:16:17,693 --> 00:16:21,729
- Uh, well...
- Well...

426
00:16:21,797 --> 00:16:24,432
Sorry, I keep myself
very well hydrated,

427
00:16:24,500 --> 00:16:26,568
and my bladder
is the size of a thimble.

428
00:16:26,635 --> 00:16:28,870
I urinate
roughly 12 times a night.

429
00:16:30,422 --> 00:16:33,107
I think I might go to bed,
too, actually.

430
00:16:33,175 --> 00:16:34,609
Oh, yeah?

431
00:16:34,677 --> 00:16:35,844
Okay.

432
00:16:35,911 --> 00:16:38,396
Um, well, look...

433
00:16:40,049 --> 00:16:42,934
Good job, again, today.
It was...

434
00:16:43,018 --> 00:16:44,435
- You too.
- Really?

435
00:16:44,520 --> 00:16:45,653
Yeah.

436
00:16:45,721 --> 00:16:47,121
Well, that was quick
and to the point.

437
00:16:47,189 --> 00:16:48,890
- Here's a tip.
- Mm?

438
00:16:48,958 --> 00:16:50,942
The key to
a healthy urethra... radishes.

439
00:16:51,026 --> 00:16:53,127
Good, I'm gonna go to bed.

440
00:16:53,195 --> 00:16:54,929
- Good night.
- Good night.

441
00:16:54,997 --> 00:16:58,833
Well, now I'm up.
You wanna boggle?

442
00:16:59,901 --> 00:17:02,236
- Are you Ron Swanson?
- I am.

443
00:17:02,288 --> 00:17:04,105
Okay, what exactly
did you teach my daughter?

444
00:17:04,173 --> 00:17:06,107
Oh, you must be
Mrs. Berkus.

445
00:17:06,175 --> 00:17:07,575
Lauren was supposed
to do a paper

446
00:17:07,626 --> 00:17:09,377
on why government matters.

447
00:17:09,428 --> 00:17:10,879
This is what she wrote.

448
00:17:12,264 --> 00:17:13,464
"It doesn't."

449
00:17:13,549 --> 00:17:15,383
Well said.

450
00:17:15,434 --> 00:17:17,185
- Is this a joke?
- No, ma'am.

451
00:17:17,252 --> 00:17:19,520
I legitimately believe that.
I'm a Libertarian.

452
00:17:19,588 --> 00:17:21,756
Oh, that's nice.
Well, she is a fourth grader.

453
00:17:21,824 --> 00:17:23,474
And fourth graders
aren't supposed to have

454
00:17:23,559 --> 00:17:25,126
their heads crammed
full of weird ideas.

455
00:17:25,194 --> 00:17:26,778
They're supposed to do
cute reports

456
00:17:26,862 --> 00:17:27,896
and get gold stars.

457
00:17:27,947 --> 00:17:29,263
I'm very sorry.
I was only...

458
00:17:29,315 --> 00:17:32,901
and you ate her lunch?
And you gave her a land mine?

459
00:17:32,952 --> 00:17:34,135
Really?

460
00:17:34,203 --> 00:17:37,655
Well, it seemed appropriate
at the time. I...

461
00:17:37,740 --> 00:17:39,908
How??

462
00:17:44,546 --> 00:17:46,547
You know this is
my house, right?

463
00:17:46,615 --> 00:17:49,300
- Yeah, hi.
- Do you wanna come in?

464
00:17:50,619 --> 00:17:52,820
- You okay?
- No.

465
00:17:52,888 --> 00:17:54,355
Andy is totally mad
at me right now,

466
00:17:54,423 --> 00:17:55,723
and I don't know
how to deal with him,

467
00:17:55,791 --> 00:17:57,492
so I thought I would ask you.

468
00:17:57,559 --> 00:17:59,193
You know, Andy and I
broke up so long ago.

469
00:17:59,261 --> 00:18:01,162
I... I don't think that
I'd be the best source to...

470
00:18:01,230 --> 00:18:03,231
please.

471
00:18:03,299 --> 00:18:04,666
What happened?

472
00:18:04,733 --> 00:18:06,401
He doesn't think
I like Mouse Rat.

473
00:18:06,468 --> 00:18:09,103
And all I said to him was that
this other band was better,

474
00:18:09,171 --> 00:18:10,571
but I don't know
why he got all mad,

475
00:18:10,639 --> 00:18:12,273
because it's
like an indisputable fact

476
00:18:12,341 --> 00:18:13,574
that they're better.
They're a real band.

477
00:18:13,642 --> 00:18:15,026
Oh, boy. Okay.

478
00:18:15,110 --> 00:18:17,912
Well, Andy just wants you to be
proud of him and his music,

479
00:18:17,980 --> 00:18:20,782
so this isn't
really about being right.

480
00:18:20,849 --> 00:18:23,051
It's more about
being supportive.

481
00:18:23,118 --> 00:18:24,452
Oh, wow,

482
00:18:24,520 --> 00:18:27,088
I didn't realize you were
a marriage counselor, Ann.

483
00:18:27,156 --> 00:18:29,590
Sorry.

484
00:18:29,658 --> 00:18:31,526
My instinct
is to be mean to you.

485
00:18:31,593 --> 00:18:34,262
I understand.

486
00:18:34,329 --> 00:18:36,547
O-M-G.

487
00:18:36,632 --> 00:18:38,182
Leslie, I read
that same article...

488
00:18:38,267 --> 00:18:39,467
"The history of the ladder."

489
00:18:39,518 --> 00:18:40,685
It's utterly fascinating.

490
00:18:40,769 --> 00:18:42,303
- Ben, you're gonna love this.
- Really?

491
00:18:42,354 --> 00:18:44,572
Do you know that
the original image of a ladder

492
00:18:44,640 --> 00:18:46,724
is in a cave
in Valencia, Spain,

493
00:18:46,809 --> 00:18:48,175
drawn over 10,000 years ago.

494
00:18:48,227 --> 00:18:49,694
Oh, my God.
How about some music?

495
00:18:56,351 --> 00:18:58,453
That's amazing.
What is this?

496
00:19:05,577 --> 00:19:08,229
Oh, man, I love this song.

497
00:19:09,381 --> 00:19:12,166
♪ The pit, I was in the pit ♪

498
00:19:12,234 --> 00:19:13,835
Wait a minute.

499
00:19:13,886 --> 00:19:16,838
♪ You were in the pit ♪

500
00:19:16,889 --> 00:19:21,592
♪ we all were in the pit ♪

501
00:19:21,677 --> 00:19:23,678
♪ the pit ♪

502
00:19:23,729 --> 00:19:26,514
♪ I was in the pit ♪

503
00:19:26,565 --> 00:19:29,851
♪ you were in the pit ♪

504
00:19:29,902 --> 00:19:35,273
♪ we all were in the pit ♪

505
00:19:37,910 --> 00:19:39,944
- What is this?
- Living out my dream.

506
00:19:40,029 --> 00:19:43,031
Playing a show
with the greatest band ever.

507
00:19:43,098 --> 00:19:44,065
That's my guitar.

508
00:19:44,133 --> 00:19:45,583
You bought it back
from Sewage Joe?

509
00:19:45,667 --> 00:19:48,920
I actually stole it
from his office,

510
00:19:49,004 --> 00:19:50,438
but whatever.

511
00:19:50,506 --> 00:19:51,839
He's a weirdo.

512
00:19:54,209 --> 00:19:56,477
Maybe April doesn't think
that we're the greatest band

513
00:19:56,545 --> 00:19:58,679
in the world,
but, man, she loves me,

514
00:19:58,747 --> 00:20:00,848
and I love her.

515
00:20:00,916 --> 00:20:02,483
So, you know, who cares.

516
00:20:02,551 --> 00:20:05,186
I got the greatest wife
in the world!

517
00:20:05,253 --> 00:20:07,254
- Stop.
- We're married!

518
00:20:08,924 --> 00:20:11,392
We're totally
gonna do it later!

519
00:20:11,460 --> 00:20:13,444
Oh, my God.

520
00:20:14,529 --> 00:20:16,898
Well, actually, I think
it's good that Chris showed up,

521
00:20:16,949 --> 00:20:18,399
'cause I wasn't
thinking clearly.

522
00:20:18,467 --> 00:20:20,968
I love my job,
Ben loves his job,

523
00:20:21,036 --> 00:20:23,004
and it's just
not worth the risk.

524
00:20:24,072 --> 00:20:25,173
- Oh, hey.
- Hey.

525
00:20:25,240 --> 00:20:27,141
Chris just wanted me
to drop off these receipts.

526
00:20:27,209 --> 00:20:29,210
Oh, well, he's not here.
He took off.

527
00:20:29,277 --> 00:20:30,995
Okay.

528
00:20:43,559 --> 00:20:44,492
Uh-oh.

529
00:20:47,559 --> 00:20:50,002
All I'm saying is...
keep an open mind for a while.

530
00:20:50,159 --> 00:20:53,002
Listen to your teachers
and read all the books you can.

531
00:20:53,659 --> 00:20:57,902
Then when you're eighteen, you can
drink, gamble and become a Libertarian.

532
00:20:58,059 --> 00:21:00,002
The drinking age is 21.

533
00:21:00,359 --> 00:21:03,002
I know...
Another stupid government rule.

534
00:21:04,759 --> 00:21:06,902
- So you write a new paper?
- Yeah.

535
00:21:07,759 --> 00:21:10,302
- Can you autograph this one for me?
- Sure.

536
00:21:10,303 --> 00:21:19,902
<font color="#FF8C00">Sync & corrections by Alice</font>
<font color="##FF8C00">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
